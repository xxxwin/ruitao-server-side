package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Logistics;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.LogisticsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Shaka on 2017/1/9.
 */
@Service
public class LogisticsService implements BaseService<Logistics> {
    @Autowired
    private LogisticsMapper logisticsMapper;

    @Override
    public Logistics insert(Logistics logistics) {
        Long time = System.currentTimeMillis();
        logistics.setCtime(time);
        logistics.setMtime(time);
        logisticsMapper.insert(logistics);
        return logistics;
    }

    @Override
    public int delete(Long id) {

        Logistics logistics = new Logistics();
        logistics.setLogisticsId(id);
        logistics.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        logistics.setMtime(time);
        return logisticsMapper.updateByPK(logistics);
    }

    @Override
    public Logistics update(Logistics logistics) {
        Long time = System.currentTimeMillis();
        logistics.setMtime(time);
        logisticsMapper.updateByPK(logistics);
        return logistics;
    }

    @Override
    public Logistics selectByPK(Long id) {
        return logisticsMapper.selectByPK(id);
    }

    @Override
    public List<Logistics> select(Logistics logistics) {
        logistics.setRstatus((byte) 0);

        return logisticsMapper.select(logistics);
    }
}
