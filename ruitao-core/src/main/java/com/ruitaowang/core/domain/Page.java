package com.ruitaowang.core.domain;

import com.ruitaowang.core.utils.StringUtils4RT;

import java.io.Serializable;
import java.util.List;

/**
 * 与具体ORM实现无关的分页查询结果封装.
 * @author neal.ma
 */
public class Page<T> implements Serializable{

    protected transient List<T> result = null;
    /**
     * 总页数
     */
    protected transient long total = -1;
    /**
     * 总记录数
     */
    protected transient long records = -1;
    /**
     * 页码
     */
    protected transient int page = 1;

    /**
     * 每页记录数
     */
    protected transient int rows = Integer.MAX_VALUE;

    /**
     * 偏移量
     */
    protected transient long offset = 0;

    /**
     * 排序字段
     */
    protected transient String orderBy = "ctime desc";

    /**
     * 排序顺序
     */
    protected transient String orderDir = null;

    /**
     * 搜索条件
     */
    protected transient String sSearch = null;

    protected transient String sidx = null;

    protected transient String sord = null;

    /**
     * stime
     * 时间段 起始时间
     * @return
     */
    protected Long stime;

    /**
     * etime
     * 时间段 结束时间
     * @return
     */
    protected Long etime;

    public Long getStime() {
        return stime;
    }

    public void setStime(Long stime) {
        this.stime = stime;
    }

    public Long getEtime() {
        return etime;
    }

    public void setEtime(Long etime) {
        this.etime = etime;
    }

    public String getSidx() {
        return StringUtils4RT.underscoreName(sidx);
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    /**
     * 获得页内的记录列表.
     *
     * @return 记录集
     */
    public List<T> getResult() {
        return result;
    }

    /**
     * 设置页内的记录列表.
     *
     * @param result
     */
    public void setResult(final List<T> result) {
        this.result = result;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {

        this.page = page;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        if (rows > 1000){
            rows = 1000;
        }
        if(rows < 1){
            rows = 10;
        }
        this.rows = rows;
    }

    public long getOffset() {

        int totalPages = (int) Math.ceil((double)records / (double)rows);
        if (totalPages == 0){
            totalPages = 1;
        }

        if(page > totalPages){
           return records;
        }

        if(page < 1){
            page = 1;
        }

        this.setTotal(totalPages);
        return (page - 1) * rows;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrderDir() {
        return orderDir;
    }

    public void setOrderDir(String orderDir) {
        this.orderDir = orderDir;
    }

    public String getsSearch() {
        return sSearch;
    }

    public void setsSearch(String sSearch) {
        this.sSearch = sSearch;
    }

    public long getRecords() {
        return records;
    }

    public void setRecords(long records) {
        this.records = records;
    }
}
