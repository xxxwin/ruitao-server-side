package com.ruitaowang.core.domain;

public class ArticleLibrary extends Page {
    /**
     *  自增编码,所属表字段为article_library.
     */
    private Long id;

    /**
     *  活动标题,所属表字段为article_library.article_title
     */
    private String articleTitle;

    /**
     *  活动链接,所属表字段为article_library.article_url
     */
    private String articleUrl;

    /**
     * 文章点击量,所属表字段为article_library.article_clicks
     */
    private Long articleClicks;

    /**
     *  创建时间,所属表字段为article_library.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为article_library.mtime
     */
    private Long mtime;

    /**
     *  0 正常
1 删除,所属表字段为article_library.rstatus
     */
    private Byte rstatus;

    /**
     *  备注,所属表字段为article_library.remark
     */
    private String remark;

    /**
     *  文章发布者,所属表字段为article_library.activity_author
     */
    private Long articleAuthor;

    /**
     * 文章展示图片，所属表字段为article_library.activity_
     */
    private String articleImageUrl;


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getArticleImageUrl() {
        return articleImageUrl;
    }

    public void setArticleImageUrl(String articleImageUrl) {
        this.articleImageUrl = articleImageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleUrl() {
        return articleUrl;
    }

    public void setArticleUrl(String articleUrl) {
        this.articleUrl = articleUrl;
    }

    public Long getArticleClicks() {
        return articleClicks;
    }

    public void setArticleClicks(Long articleClicks) {
        this.articleClicks = articleClicks;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getArticleAuthor() {
        return articleAuthor;
    }

    public void setActivityAuthor(Long articleAuthor) {
        this.articleAuthor = articleAuthor;
    }
}