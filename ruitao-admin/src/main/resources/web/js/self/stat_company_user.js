/**
 * Created by Administrator on 2017/4/22.
 */
$(document).ready(function () {

    var date = new Date;
    var year = (date.getFullYear()).toString() + "年";
    var month = (date.getMonth() + 1).toString() + "月";
    var day = (date.getDate()).toString() + "日";
    $("#month").html(month);
    $("#year").html(year);
    $("#today").html(day);
    $(".chart").easyPieChart({
        barColor: "#f8ac59",
        scaleLength: 5,
        lineWidth: 4,
        size: 80
    });
    $(".chart2").easyPieChart({
        barColor: "#1c84c6",
        scaleLength: 5,
        lineWidth: 4,
        size: 80
    });
    $.ajax(
        {
            url: "/api/stat/company/user",
            type: "GET",
            data: {days: 365},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                var arr1=[],arr2=[],arr3=[],arr4=[],arr5=[],arr6=[],arr7=[];
                $.each(data.data, function (index, val) {
                    arr1.push(val.userId);
                    arr2.push((val.orderConsumption).toFixed(2));
                    arr3.push(val.orderCount);
                    arr4.push(val.localOrderConsumption);
                    arr5.push(val.localOrderCount);
                    arr6.push(val.dishOrderConsumption);
                    arr7.push(val.dishOrderCount);
                });
                var chart=echarts.init(document.getElementById("chart"));
                var option={
                    title: {
                        text: '用户统计走势图',
                        left:0,
                        top:'3%'
                    },
                    backgroundColor:"#fff" ,
                    tooltip: {
                        trigger:"axis"
                    },
                    legend: {
                        data:['消费总额','订单总额'],
                        right:'13%',
                        top:'4%'
                    },
                    grid:{
                        left:'15%',
                        right:'15%',
                        top:'30%',
                        bottom:'15%'
                    },
                    xAxis: {
                        data:arr1
                    },
                    yAxis:[
                        {
                            type:'value',
                            name:'消费总额(元)'
                        },
                        {
                            type:'value',
                            name:'订单总额(个)'
                        }
                    ],
                    toolbox:{
                        top:'3%',
                        right:0,
                        show:true,
                        feature: {
                            dataZoom: {}
                        }
                    },
                    series: [
                        {
                            name: '消费总额',
                            type: 'bar',
                            data: arr2
                        },
                        {
                            name: '订单总额',
                            type: 'bar',
                            yAxisIndex:1,
                            data: arr3
                        }
                    ]
                };
                chart.setOption(option);

                var chart1=echarts.init(document.getElementById("chart1"));
                var option1={
                    title: {
                        text: '用户统计走势图',
                        left:0,
                        top:'3%'
                    },
                    backgroundColor:"#fff" ,
                    tooltip: {
                        trigger:"axis"
                    },
                    legend: {
                        data:['消费总额','订单总额'],
                        right:'13%',
                        top:'4%'
                    },
                    grid:{
                        left:'15%',
                        right:'15%',
                        top:'30%',
                        bottom:'15%'
                    },
                    xAxis: {
                        data:arr1
                    },
                    yAxis:[
                        {
                            type:'value',
                            name:'消费总额(元)'
                        },
                        {
                            type:'value',
                            name:'订单总额(个)'
                        }
                    ],
                    toolbox:{
                        top:'3%',
                        right:0,
                        show:true,
                        feature: {
                            dataZoom: {}
                        }
                    },
                    series: [
                        {
                            name: '消费总额',
                            type: 'bar',
                            data: arr4
                        },
                        {
                            name: '订单总额',
                            type: 'bar',
                            yAxisIndex:1,
                            data: arr5
                        }
                    ]
                };
                chart1.setOption(option1);

                var chart2=echarts.init(document.getElementById("chart2"));
                var option2={
                    title: {
                        text: '用户统计走势图',
                        left:0,
                        top:'3%'
                    },
                    backgroundColor:"#fff" ,
                    tooltip: {
                        trigger:"axis"
                    },
                    legend: {
                        data:['消费总额','订单总额'],
                        right:'13%',
                        top:'4%'
                    },
                    grid:{
                        left:'15%',
                        right:'15%',
                        top:'30%',
                        bottom:'15%'
                    },
                    xAxis: {
                        data:arr1
                    },
                    yAxis:[
                        {
                            type:'value',
                            name:'消费总额(元)'
                        },
                        {
                            type:'value',
                            name:'订单总额(个)'
                        }
                    ],
                    toolbox:{
                        top:'3%',
                        right:0,
                        show:true,
                        feature: {
                            dataZoom: {}
                        }
                    },
                    series: [
                        {
                            name: '消费总额',
                            type: 'bar',
                            data: arr6
                        },
                        {
                            name: '订单总额',
                            type: 'bar',
                            yAxisIndex:1,
                            data: arr7
                        }
                    ]
                };
                chart2.setOption(option2);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
});