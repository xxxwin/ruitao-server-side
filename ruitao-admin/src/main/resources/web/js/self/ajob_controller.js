var Config = {
    selectUrl: '/api/ajobs/page',
    deleteUrl: '/api/ajobs',
    updateUrlForCellEdit: '/api/ajobs/cellEdit',
};

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#ajobs_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","定时器唯一标识" ,"服务器类型","备注","是否使用", "创建时间" ,"操作"],
        colModel: [{
            name: "ajobId",
            index: "ajobId",
            search: true
        }, {
            name: "ajobSign",
            index: "ajobSign"
        }, {
            name:"ajobIp",
            index: "ajobIp",
            editoptions: {value: "10.29.24.137:bmw线上服务器;10.27.143.116:admin线上服务器;10.171.55.220:测试服务器"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name:"remark",
            index: "remark"
        }, {
            name:"ajobType",
            index: "ajobType",
            autowidth: true,
            editable: true,
            sortable: true,
            editoptions: {value: "1:是;2:否"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        multiselect: true,
        multiboxonly: true,
        onSelectRow: function (id, status, e) {
            console.log($(e.target).parent().parent());
            throw Error("");
        },
        beforeSelectRow: function (rowId, e) {
            return $(e.target).is('input[type=checkbox]');
        },
        pager: "#pager_ajobs_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "ajobId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });

    $("#perform_search").click(function () {
        var postdata = $("#ajobs_list").jqGrid('getGridParam', 'postData');
        postdata._search = true;
        if($('#ajobSign').val() != ""){
            postdata.ajobSign = $('#ajobSign').val();
        }
        if($('#ajobIp option:selected').val() != ""){
            postdata.ajobIp = $('#ajobIp option:selected').val();
        }
        if($('#ajobType option:selected').val() != ""){
            postdata.ajobType = $('#ajobType option:selected').val();
        }
        jQuery("#ajobs_list").trigger("reloadGrid", [{page: 1}]);
    });

    jQuery("#ajobs_list").jqGrid('navGrid', '#pager_ajobs_list', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#ajobs_list").setGridWidth(width);
    });
});

function Delete(id) {
    bootbox.confirm({size:"small",message:"确认要删除id为"+id+"的这条数据吗?",callback: function (result) {
            if (result) {
                updateDialog("DELETE", Config.deleteUrl + '/' + id);
                jQuery("#ajobs_list").jqGrid('delRowData', id);
            }
        }
    });
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#ajobs_list").trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}


function dataWrapper() {
    var ids = jQuery("#ajobs_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#ajobs_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" onclick="industryUpdate(' + id + ')" data-index="0">修改</a>';
        jQuery("#ajobs_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
        });
    }
}

function industryUpdate(id) {
    layer.open({
        title: ["定时器修改","font-size:20px;"],
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:"/ajob/update/" + id,
    })
}