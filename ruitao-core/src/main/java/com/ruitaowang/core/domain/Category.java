package com.ruitaowang.core.domain;

/**
 * category《分类表》 
 * @author yuri.cxy
 *
 */
public class Category extends Page{

//	private static final long serialVersionUID = 1L;
	
	private Long id; //
	private Long parentId; //上级主分类ID
	private Long level; //级别
	private String name; //名称
	private String spell; //简拼
	private Long sid; //子分类ID
	private Long parentSid; //上级子分类ID
	
	/**
	 *实例化一个"分类表"实体模型对象
	 */
	public Category() {
		super();
	}

	public Long getId(){
		return this.id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public Long getParentId(){
		return this.parentId;
	}

	public void setParentId(Long parentId){
		this.parentId = parentId;
	}

	public Long getLevel(){
		return this.level;
	}

	public void setLevel(Long level){
		this.level = level;
	}

	public String getName(){
		return this.name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getSpell(){
		return this.spell;
	}

	public void setSpell(String spell){
		this.spell = spell;
	}

	public Long getSid(){
		return this.sid;
	}

	public void setSid(Long sid){
		this.sid = sid;
	}

	public Long getParentSid(){
		return this.parentSid;
	}

	public void setParentSid(Long parentSid){
		this.parentSid = parentSid;
	}
	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append("Category [")
		.append("id=").append(this.getId())
		.append(",parentId=").append(this.getParentId())
		.append(",level=").append(this.getLevel())
		.append(",name=").append(this.getName())
		.append(",spell=").append(this.getSpell())
		.append(",sid=").append(this.getSid())
		.append(",parentSid=").append(this.getParentSid())
		.append("]");
		return builder.toString();
	}
}