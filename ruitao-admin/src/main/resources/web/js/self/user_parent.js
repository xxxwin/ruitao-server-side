/**
 * Created by Administrator on 2017/3/23.
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
$(document).ready(function(){
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        postData: {uesrId: parseInt(getQueryString("id"))},
        url:"/api/admin/user/parent",
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "昵称", "头像"],
        colModel: [{
            name: "id",
            index: "id",
            autowidth: true,
            sorttype: "int",
            search: true,
            width:15

        },{
            name: "nickname",
            index: "nickname",
            autowidth: true,
            width:15
        }, {
            name: "headimgurl",
            index: "headimgurl",
            autowidth: true,
            width:20
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            dataWrapper();
        }
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});
$('#buttonGoback').click(function() {
    history.go(-1);
    location.reload();
    return false;
});
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id);
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/user_role_update/' + id + '" data-index="0">分配角色</a>';
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 10px" href="/user_member_update/' + id + '" data-index="0">详情</a>';
        var avatar = '<img style="width: 50px; height: 50px;" src="'+dataFromTheRow.headimgurl+'"/>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html, headimgurl: avatar});
    }
}