package com.ruitaowang.core.domain;

public class PcUser extends Page {
    /**
     *  自增主键,所属表字段为pc_user.id
     */
    private Long id;

    /**
     *  用户名,所属表字段为pc_user.username
     */
    private String username;

    /**
     *  密码,所属表字段为pc_user.password
     */
    private String password;

    /**
     *  手机号,所属表字段为pc_user.phone
     */
    private String phone;

    /**
     *  备注,所属表字段为pc_user.remark
     */
    private String remark;

    /**
     *  邮箱,所属表字段为pc_user.email
     */
    private String email;

    /**
     *  创建时间,所属表字段为pc_user.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为pc_user.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为pc_user.rstatus
     */
    private Byte rstatus;

    /**
     *  真实姓名,所属表字段为pc_user.real_name
     */
    private String realName;

    /**
     *  最后登录ip,所属表字段为pc_user.last_ip
     */
    private String lastIp;

    /**
     *  最后登录时间,所属表字段为pc_user.last_time
     */
    private Long lastTime;

    /**
     *  1: 公开       个人隐私信息 
0 :不公开   个人隐私信息,所属表字段为pc_user.locked
     */
    private Boolean locked;

    /**
     *  0： 普通会员 （消费者 两类1 网站消费，2 实体店扫码消费，没有绑定手机号）
1：本地生活 商家用户
2：城市合伙人
4：区代理 ,所属表字段为pc_user.user_type
     */
    private Byte userType;

    /**
     *  头像,所属表字段为pc_user.headimgurl
     */
    private String headimgurl;

    /**
     *  昵称,所属表字段为pc_user.nickname
     */
    private String nickname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp == null ? null : lastIp.trim();
    }

    public Long getLastTime() {
        return lastTime;
    }

    public void setLastTime(Long lastTime) {
        this.lastTime = lastTime;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Byte getUserType() {
        return userType;
    }

    public void setUserType(Byte userType) {
        this.userType = userType;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl == null ? null : headimgurl.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }
}