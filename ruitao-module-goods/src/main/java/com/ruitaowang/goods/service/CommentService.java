package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Comment;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CommentAdMapper;
import com.ruitaowang.goods.dao.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentService implements BaseService<Comment> {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private CommentAdMapper commentAdMapper;
    @Override
    public Comment insert(Comment comment) {
        Long time = System.currentTimeMillis();
        comment.setCtime(time);
        comment.setMtime(time);
        commentMapper.insert(comment);
        return comment;
    }

    @Override
    public int delete(Long id) {
        Comment comment = new Comment();
        comment.setId(id);
        comment.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        comment.setMtime(time);
        return commentMapper.updateByPK(comment);
    }

    @Override
    public Comment update(Comment comment) {
        Long time = System.currentTimeMillis();
        comment.setMtime(time);
        commentMapper.updateByPK(comment);
        return comment;
    }

    @Override
    public Comment selectByPK(Long id) {

        return commentMapper.selectByPK(id);
    }

    @Override
    public List<Comment> select(Comment comment) {
        comment.setRstatus((byte) 0);
        return commentMapper.select(comment);
    }

    public List<Comment> selectForPage(Comment comment) {
        comment.setRstatus((byte) 0);
        comment.setRecords(commentMapper.count(comment));
        return commentMapper.select(comment);
    }

    public List<Comment> selectAd(Comment comment) {
        comment.setRstatus((byte) 0);
        return commentAdMapper.select(comment);
    }
    public List<Comment> selectAdForPage(Comment comment) {
        comment.setRstatus((byte) 0);
        comment.setRecords(commentAdMapper.count(comment));
        return commentAdMapper.select(comment);
    }

    public List<Comment> selectByTwo(Comment comment){
        comment.setRstatus((byte) 0);
        comment.setRecords(commentAdMapper.selectByTwoCount(comment));
        return commentAdMapper.selectByTwo(comment);
    }

    public Long countByTwo(Comment comment){
        comment.setRstatus((byte) 0);
        return commentAdMapper.selectByTwoCount(comment);
    }

    public Long count(Comment comment){
        comment.setRstatus((byte) 0);
        return commentMapper.count(comment);
    }
}
