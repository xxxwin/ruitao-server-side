$(".datepicker").datepicker({
    todayBtn: "linked",
    clearBtn: true,
    autoclose: true,
    language: "zh-CN"
});

var CONST_GLOBAL_COMPANY_ID;
var Config = {
    createUrl: '/api/ajobs/create'
};

// $(function(){
//     /* 初始化控件 */
//     $(".ajobType").bootstrapSwitch({
//         onText: "启动",      // 设置ON文本  
//         offText: "停止",    // 设置OFF文本  
//         onColor: "success",// 设置ON文本颜色     (info/success/warning/danger/primary)  
//         offColor: "info",  // 设置OFF文本颜色        (info/success/warning/danger/primary)  
//         size: "normal",    // 设置控件大小,从小到大  (mini/small/normal/large)  
//         // 当开关状态改变时触发  
//         onSwitchChange : function(event, state) {
//             if (state == true) {
//                 console.log("停止");
//                 $("#ajobType").val("1");
//             } else {
//                 console.log("开始");
//                 $("#ajobType").val("2");
//             }
//         }
//     })
// });

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$().ready(function () {

    $('#submitCreate').click(function () {
        $("#ajobForm").validate();
        if ($('#ajobForm').valid()) {
            if($("#ajobIp").val() == "0"){
                toastr.error("请选择服务器！");
                return false;
            }
            if($("#ajobType").val() == "0"){
                toastr.error("请选择是否使用！");
                return false;
            }
            var postData = $('#ajobForm').serializeArray();
            $.ajax({
                url: Config.createUrl,
                type: "POST",
                data: postData,
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    if (data.type ==="ERROR"){
                        toastr.success(data.msg, data.title);
                    } else {
                        CONST_GLOBAL_COMPANY_ID = data.data.companyId;
                        toastr.success(data.msg, data.title);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        }
        return false;
    });
});





