package com.ruitaowang.account.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;

import com.ruitaowang.core.domain.CompanyStaff;


public interface CompanyStaffMapper {
	
	public static String SELECTBYPK = "select * from company_staff where id=#{id}";
	 
    CompanyStaff selectByPK(Long id);

    List<CompanyStaff> select(CompanyStaff companyStaff); 
    
    long count(CompanyStaff companyStaff);
	
	int insert(CompanyStaff companyStaff);

	@Delete("delete from company_staff where id = #{id}")
    int deleteByPK(Long id);

    int updateByPK(CompanyStaff companyStaff);



}


