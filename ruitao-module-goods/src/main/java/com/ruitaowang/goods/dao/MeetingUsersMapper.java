package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.MeetingUsers;
import java.util.List;

public interface MeetingUsersMapper {
    int insert(MeetingUsers meetingUsers);

    int deleteByPK(Long companyId);

    int updateByPK(MeetingUsers meetingUsers);

    MeetingUsers selectByPK(Long companyId);

    long count(MeetingUsers meetingUsers);

    List<MeetingUsers> select(MeetingUsers meetingUsers);
}