package com.ruitaowang.admin.view.admin;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.utils.PriceUtile;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * 订单状态修改
 */
@RestController
public class PageOrdersCotroller extends BaseController {

    @Autowired
    OrderService orderService;

    @Autowired
    OrderProdService orderProdService;
    @Autowired
    AfterMarketService afterMarketService;
    @Autowired
    CompanyService companyService;
    @Autowired
    ShoppingOrderService shoppingOrderService;
    @Autowired
    GoodsService goodsService;
    @Autowired
    SysUserService sysUserService;
    @Autowired
    CompanyTypeService companyTypeService;
    @Autowired
    CouponService couponService;

    @RequestMapping(value = {"/orders/update/{orderId}"}, method = RequestMethod.GET)
    public ModelAndView update(@PathVariable("orderId") Long orderId, Model model) {
        //订单信息
        Order order = orderService.selectByPK(orderId);
        OrderVO orderVO = new OrderVO();
        BeanUtils.copyProperties(order, orderVO);
        orderVO.setNickName(sysUserService.selectByPK(order.getUserId()).getNickname());
        //订单详情
        List<ShoppingOrderVO> shoppingOrderVOS= new ArrayList<>();
        ShoppingOrder shoppingOrder = new ShoppingOrder();
        shoppingOrder.setOrderId(orderId);
        List<ShoppingOrder>shoppingOrders=shoppingOrderService.select(shoppingOrder);
        for (ShoppingOrder so:shoppingOrders){
            ShoppingOrderVO shoppingOrderVO = new ShoppingOrderVO();
            BeanUtils.copyProperties(so, shoppingOrderVO);
            OrderProd orderProd = new OrderProd();
            orderProd.setOrderId(so.getId());
            orderProd.setOrderType((byte)1);
            List<OrderProd> orderProds = orderProdService.select(orderProd);
            int num=0;
            List<OrderProdVO> orderProdVOList= new ArrayList<>();
            for (OrderProd op:orderProds){
                OrderProdVO orderProdVO = new OrderProdVO();
                BeanUtils.copyProperties(op, orderProdVO);
                orderProdVO.setGoodsScreenPriceVO(PriceUtile.toFixedForPrice(op.getGoodsScreenPrice()));
                orderProdVO.setGoodsRealPriceVO(PriceUtile.toFixedForPrice(op.getGoodsRealPrice()));
                orderProdVOList.add(orderProdVO);
                num+=op.getGoodsNumber();
            }
            Company company=companyService.selectByPK(so.getCompanyId());
            shoppingOrderVO.setCompanyPhone(company.getMobile());
            if (!ObjectUtils.isEmpty(company.getHeadimgurl())){
                shoppingOrderVO.setCompanyHeadimg(company.getHeadimgurl());
            }else {
                shoppingOrderVO.setCompanyHeadimg("http://static.ruitaowang.com/attached/file/2018/07/18/20180718153138439.jpg");
            }
            shoppingOrderVO.setGoodsNumber(num);
            if (so.getDiscountId() != 0){
                shoppingOrderVO.setCouponPrice(couponService.couponPrice(so.getDiscountId()));
            }else {
                shoppingOrderVO.setCouponPrice("无优惠券");
            }
            shoppingOrderVO.setOrderAmount(PriceUtile.toFixedForPrice(so.getAmount()));
            shoppingOrderVO.setLinkman(company.getLinkman());
            shoppingOrderVO.setAddress(company.getAddress());
            //shoppingOrderVO.setIndustryCategory(companyTypeService.industryCategoryi(company.getCompanyTypeId()));
            shoppingOrderVO.setGoodsListVO(orderProdVOList);
            shoppingOrderVOS.add(shoppingOrderVO);
        }
        model.addAttribute("order", orderVO);
        model.addAttribute("shoppingOrderVOS", shoppingOrderVOS);
        return new ModelAndView("web/orders_update");
    }


    @RequestMapping(value = {"/ws/orders/update/{orderId}"}, method = RequestMethod.GET)
    public ModelAndView updateWs(@PathVariable("orderId") Long orderId, Model model) {
        SysUser user = LYSecurityUtil.currentSysUser();
        Order order = orderService.selectByPK(orderId);
        order.setOrderAmount(order.getExPrice() + order.getOrderAmount() - order.getMemberPrice());
        OrderProd orderProd = new OrderProd();
        orderProd.setOrderId(orderId);
        orderProd.setGoodsProviderId(companyService.selectByUserId(user.getId()).getCompanyId());
        List<OrderProd> orderProdList1 = orderProdService.select(orderProd);
        List<OrderProd> orderProdList=new ArrayList<>();
        for (OrderProd op:orderProdList1){
            AfterMarket afterMarket=new AfterMarket();
            afterMarket.setProdId(op.getProdId());
            List<AfterMarket> afterMarket1=afterMarketService.select(afterMarket);
            if (!ObjectUtils.isEmpty(afterMarket1)){
                for (AfterMarket am:afterMarket1){
                    switch (am.getReturnType()){
                        case 0:
                            op.setRemark("退货");
                            break;
                        case 1:
                            op.setRemark("退款");
                            break;
                        case 3:
                            op.setRemark("退货退款");
                            break;
                        case 4:
                            op.setRemark("换货");
                            break;
                    }
                }
            }else {
                op.setRemark("正常");
            }
            orderProdList.add(op);
        }
        model.addAttribute("order", order);
        model.addAttribute("orderProdList", orderProdList);
        return new ModelAndView("web/ws_orders_update");
    }

    @RequestMapping(value = {"/companyOrders/update/{prodId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView updateCompanyOrderProd(@PathVariable("prodId") Long prodId, Model model) {
        OrderProd orderProd = orderProdService.selectByPK(prodId);
        Order order = orderService.selectByPK(orderProd.getOrderId());
        model.addAttribute("orderProd", orderProd);
        model.addAttribute("order", order);
        return new ModelAndView("web/company_order_update");
    }

}
