package com.ruitaowang.admin.view.api;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Activity;
import com.ruitaowang.core.domain.ArticleLibrary;
import com.ruitaowang.core.domain.MzAds;
import com.ruitaowang.core.domain.RelationsVO;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.UserMembers;
import com.ruitaowang.core.domain.UserProfit;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.ActivityService;
import com.ruitaowang.goods.service.ArticleLibraryService;
import com.ruitaowang.goods.service.MzAdsService;
import com.ruitaowang.goods.service.OrderService;
import com.ruitaowang.goods.service.UserMembersService;
import com.ruitaowang.goods.service.UserProfitService;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APIMZController extends BaseController {

    @Autowired
    private ActivityService activityService;
    @Autowired
    private MzAdsService mzAdsService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserProfitService userProfitService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserMembersService userMembersService;
    @Autowired
    private ArticleLibraryService articleLibraryService;

    /**
     * @api {get} /api/mz/article 微传媒-粘贴框-打开
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/mz/article
     * @apiName handleArticle
     * @apiGroup MZ
     * @apiParam {String} url 文章链接.
     * @apiParam {String} f 判断跳转路径.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "activityAuthor":1,
     * "activityId":719,
     * "activityImageUrl":"http://static.ruitaowang.com/attached/file/20170522/20170522185933_400.jpeg",
     * "activitySort":0,
     * "activityTitle":"你旅游我买单",
     * "activityType":0,
     * "activityUrl":"https://www.ruitaowang.com/wap/wx/login?fk=1-15-0-718",
     * "ctime":1495450727557,
     * "goodsId":0,
     * "mtime":1495461867633,
     * "rstatus":0
     * }
     * ],
     * "msg":"操作成功啦",
     * "records":5,
     * "title":"成功",
     * "total":1,
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/mz/article"
     * }
     */
    @RequestMapping("/api/mz/article")
    public String handleArticle(String url, @RequestParam(defaultValue = "0") String f) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Long records = 0L;
        Long num = 7L;
        //未购买用户 限制免费使用7次
        Activity act = new Activity();
        act.setActivityAuthor(user.getId());
        act.setActivityType((byte) 10);
        //2019.1.19 4:30 以后的无权限用户  免费七次
        Long numNow = activityService.selectNoRstatus(act);
        if (!ObjectUtils.isEmpty(numNow)){
            records = num - numNow;
        }
        String userId=userMembersService.getMemberForKZLimit(user.getId());
        if (userId.equals("-1")) {
            LOGGER.info(records.toString());
            if (records < 0L || records == 0L){
                return this.renderJson(MsgOut.error("此用户续费已过期"));
            }
        }else if (userId.equals("-2")) {
            LOGGER.info(records.toString());
            if (records < 0L || records == 0L){
                return this.renderJson(MsgOut.error("此用户无开通权限"));
            }
        }else {
            LOGGER.info(records.toString());
            records = 8L;
        }

        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
            doc.select("div.rich_media_thumb_wrp").remove();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!ObjectUtils.isEmpty(url)){
            ArticleLibrary articleLibrary = new ArticleLibrary();
            articleLibrary.setArticleUrl(url);
            ArticleLibrary where=articleLibraryService.selectUrl(url);
            if (ObjectUtils.isEmpty(where)){
                articleLibrary.setArticleTitle(doc.title());
                articleLibrary.setArticleClicks(1L);
                articleLibrary.setActivityAuthor(user.getId());
                articleLibrary.setArticleImageUrl(doc.select("img").attr("data-src"));
                articleLibraryService.insert(articleLibrary);
                LOGGER.info("articleLibrary1",articleLibrary);
            }else {
                articleLibrary=where;
                LOGGER.info("where",where);
                articleLibrary.setArticleClicks(where.getArticleClicks() + 1);
                articleLibraryService.update(articleLibrary);
                LOGGER.info("articleLibraryN",articleLibrary);
            }
        }
        Element content = doc.getElementById("img-content");//获取id为img-content的dom节点
        String str = stripHtml(content.toString()).substring(0, 50);
        Activity activity = new Activity();
        activity.setActivityAuthor(user.getId());
        activity.setActivityContent(content.toString());
        activity.setActivityTitle(doc.title());
        activity.setActivityType((byte) 10);
        activity.setActivityUrl(str);
        activityService.insert(activity);
        MsgOut o = MsgOut.success(activity);
        o.setRecords(records);
        return this.renderJson(o);
    }

    private String stripHtml(String content) {
// <script> 过滤<script>
        content = content.replaceAll("<script[^>]*?>[\\s\\S]*?<\\/script>", "");
// <p>段落替换为换行
        content = content.replaceAll("<p .*?>", "\r\n");
// <br><br/>替换为换行
        content = content.replaceAll("<br\\s*/?>", "\r\n");
// 去掉其它的<>之间的东西
        content = content.replaceAll("\\<.*?>", "");
// 还原HTML
// content = HTMLDecoder.decode(content);
        return content;
    }

    /**
     * @api {get} /api/mz/activities 微传媒-获取历史记录
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/mz/activities
     * @apiName list
     * @apiGroup MZ
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "activityAuthor":1,
     * "activityId":719,
     * "activityImageUrl":"http://static.ruitaowang.com/attached/file/20170522/20170522185933_400.jpeg",
     * "activitySort":0,
     * "activityTitle":"你旅游我买单",
     * "activityType":0,
     * "activityUrl":"https://www.ruitaowang.com/wap/wx/login?fk=1-15-0-718",
     * "ctime":1495450727557,
     * "goodsId":0,
     * "mtime":1495461867633,
     * "rstatus":0
     * }
     * ],
     * "msg":"操作成功啦",
     * "records":5,
     * "title":"成功",
     * "total":1,
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/mz/activities"
     * }
     */
    @RequestMapping("/api/mz/activities")
    public String list(Activity activity, int page) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        activity.setPage(page);
        if (!ObjectUtils.isEmpty(activity.getActivityId())) {
            activity.setActivityAuthor(activityService.selectByPK(activity.getActivityId()).getActivityAuthor());
        } else if(!ObjectUtils.isEmpty(activity.getActivityAuthor())) {
            activity.setActivityAuthor(activity.getActivityAuthor());
        } else {
            activity.setActivityAuthor(user.getId());
        }
        //activity.setActivityAuthor(user.getId());
        activity.setActivityType((byte) 10);
        MsgOut o = MsgOut.success(activityService.selectForPage(activity));
        o.setRecords(activity.getRecords());
        o.setTotal(activity.getTotal());
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/mz/ads 微传媒-获取历史记录
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/mz/ads
     * @apiName list
     * @apiGroup MZ
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiParam {byte} adType 广告类型.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "adId":1,
     * "adUrl":719,
     * "adTitle":"",
     * "buttonType":0,
     * "adType":0,
     * "ctime":1495461867633,
     * "mtime":1495461867633,
     * "rstatus":0,
     * "linkType":0,
     * "adPosition":0,
     * "adImageUrl":"https://www.ruitaowang.com/wap/wx/login?fk=1-15-0-718",
     * "adAuthor":0,
     * "phone":0,
     * "buttonName":"",
     * "remark":"",
     * "adContent":"{[phone:15910288070,name:"xxx",...............]}",
     * }
     * ],
     * "msg":"操作成功啦",
     * "records":5,
     * "title":"成功",
     * "total":1,
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/mz/ads"
     * }
     */
    @RequestMapping("/api/mz/ads")
    public String list(MzAds activity, int page) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        activity.setPage(page);
        activity.setAdAuthor(user.getId());
        MsgOut o = MsgOut.success(mzAdsService.selectForPage(activity));
        o.setRecords(activity.getRecords());
        o.setTotal(activity.getTotal());
        return this.renderJson(o);
    }

    /**
     * @api {post} /api/mz/ads 微传媒-新建广告
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/mz/ads
     * @apiName create
     * @apiGroup MZ
     * @apiParam {byte} adType 广告类型.
     * @apiParam {String} adTitle 广告title.adUrl
     * @apiParam {String} adUrl 活动链接.
     * @apiParam {byte} buttonType 按钮类型.
     * @apiParam {byte} linkType 链接类型.
     * @apiParam {byte} adPosition 广告位置.
     * @apiParam {String} adImageUrl 广告图片地址.
     * @apiParam {String} buttonName 按钮名称.
     * @apiParam {String} remark 广告备注.
     * @apiParam {String} adContent 广告文本.
     * @apiParam {String} adCompany 公司.
     * @apiParam {String} adHeadimageUrl 头像.
     * @apiParam {String} adEmail 邮件.
     * @apiParam {String} adWechat 微信.
     * @apiParam {String} adAddress 地址.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":
     * {
     * "adId":1,
     * "adUrl":719,
     * "adTitle":"",
     * "buttonType":0,
     * "adType":0,
     * "ctime":1495461867633,
     * "mtime":1495461867633,
     * "rstatus":0,
     * "linkType":0,
     * "adPosition":0,
     * "adImageUrl":"https://www.ruitaowang.com/wap/wx/login?fk=1-15-0-718",
     * "adAuthor":0,
     * "phone":0,
     * "buttonName":"",
     * "remark":"",
     * "adContent":"{"name":"xxx","phone":15842019503}"
     * },
     * "msg":"操作成功啦",
     * "records":5,
     * "title":"成功",
     * "total":1,
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/mz/ads"
     * }
     */
    @RequestMapping(value = "/api/mz/ads", method = RequestMethod.POST)
    public String create(MzAds activity) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        activity.setAdAuthor(user.getId());
        mzAdsService.insert(activity);
        MsgOut o = MsgOut.success(activity);
        return this.renderJson(o);
    }

    /**
     * @api {put} /api/mz/ads 微传媒-修改广告
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/mz/ads
     * @apiName update
     * @apiGroup MZ
     * @apiParam {long} adId 广告ID.
     * @apiParam {byte} adType 广告类型.
     * @apiParam {String} adUrl 活动链接.
     * @apiParam {String} adTitle 广告title.
     * @apiParam {byte} buttonType 按钮类型.
     * @apiParam {byte} linkType 链接类型.
     * @apiParam {byte} adPosition 广告位置.
     * @apiParam {String} adImageUrl 广告图片地址.
     * @apiParam {String} buttonName 按钮名称.
     * @apiParam {String} remark 广告备注.
     * @apiParam {String} adContent 广告文本.
     * @apiParam {String} adCompany 公司.
     * @apiParam {String} adHeadimageUrl 头像.
     * @apiParam {String} adEmail 邮件.
     * @apiParam {String} adWechat 微信.
     * @apiParam {String} adAddress 地址.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "adId":1,
     * "adUrl":719,
     * "adTitle":"",
     * "buttonType":0,
     * "adType":0,
     * "ctime":1495461867633,
     * "mtime":1495461867633,
     * "rstatus":0,
     * "linkType":0,
     * "adPosition":0,
     * "adImageUrl":"https://www.ruitaowang.com/wap/wx/login?fk=1-15-0-718",
     * "adAuthor":0,
     * "phone":0,
     * "buttonName":"",
     * "remark":"",
     * "adContent":"{"name":"xxx","phone":15842019503}",
     * }
     * ],
     * "msg":"操作成功啦",
     * "records":5,
     * "title":"成功",
     * "total":1,
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/mz/ads"
     * }
     */
    @RequestMapping(value = "/api/mz/ads", method = RequestMethod.PUT)
    public String update(MzAds activity) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        activity.setAdAuthor(user.getId());
        mzAdsService.update(activity);
        MsgOut o = MsgOut.success(activity);
        return this.renderJson(o);
    }

    /**
     * @api {delete} /api/mz/ads/{adId} 删除广告
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/mz/ads/{adId}
     * @apiName delete
     * @apiGroup MZ
     * @apiParam {long} activityId 活动编号.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":1,
     * "msg":"操作成功啦",
     * "title":"成功",
     * "total":1,
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/mz/ads/{adId}"
     * }
     */
    @RequestMapping(value = "/api/mz/ads/{adId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("adId") Long adId) {

        MsgOut o = MsgOut.success(mzAdsService.delete(adId));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/mz/ads/{adId} 微传媒-查询单个广告
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/mz/ads/{adId}
     * @apiName findAdId
     * @apiGroup MZ
     * @apiParam {long} adId 广告ID.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "adId":1,
     * "adUrl":719,
     * "adTitle":"",
     * "buttonType":0,
     * "adType":0,
     * "ctime":1495461867633,
     * "mtime":1495461867633,
     * "rstatus":0,
     * "linkType":0,
     * "adPosition":0,
     * "adImageUrl":"https://www.ruitaowang.com/wap/wx/login?fk=1-15-0-718",
     * "adAuthor":0,
     * "phone":0,
     * "buttonName":"",
     * "remark":"",
     * "adContent":"{"name":"xxx","phone":15842019503}",
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/mz/ads/{adId}"
     * }
     */
    @RequestMapping(value = "/api/mz/ads/{adId}", method = RequestMethod.GET)
    public String findAdId(@PathVariable("adId") Long adId) {
        MsgOut o = MsgOut.success(mzAdsService.selectByPK(adId));
        return this.renderJson(o);
    }

    @RequestMapping("/api/mz/limit")
    public String memberLimit() {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Long time = System.currentTimeMillis();
        HashMap map = new HashMap();
        String limit = userMembersService.getMemberForKZLimit(user.getId());
        Long records = 0L;
        Long num = 7L;
        String isRecords = "0";
        //未购买用户 限制免费使用7次
        Activity act = new Activity();
        act.setActivityAuthor(user.getId());
        act.setActivityType((byte) 10);
        Long numNow = activityService.selectNoRstatus(act);
        if (!ObjectUtils.isEmpty(numNow)){
            records = num - numNow;
        }
        if (ObjectUtils.isEmpty(limit)) {
            return this.renderJson(MsgOut.success(limit));
        } else {
            if (limit.equals("-1")) {
                if (records < 0L || records == 0L){
                    return this.renderJson(MsgOut.error("此用户续费已过期"));
                } else {
                    isRecords = "1";
                    limit = String.valueOf(user.getId());
                }
            }
            if (limit.equals("-2")) {
                if (records < 0L || records == 0L){
                    return this.renderJson(MsgOut.error("此用户无开通权限"));
                } else {
                    isRecords = "1";
                    limit = String.valueOf(user.getId());
                }
            }
            UserMembers userMembers = new UserMembers();
            userMembers.setUserId(Long.parseLong(limit));
            userMembers.setMemberType((byte) 1);
            List<UserMembers> userMembersList = userMembersService.select(userMembers);
            //查询个人信息，昵称
//            SysUser sysUser = sysUserService.selectByPK(user.getId());
//            map.put("nickName", sysUser.getNickname());
//            map.put("userId", user.getId());
            map.put("isRecords",isRecords);
            map.put("records",records);
            map.put("expire", time);
            if (ObjectUtils.isEmpty(userMembersList)) {
                userMembers.setStartTime(TimeUtils.getCurrentDate());
                userMembers.setEndTime(TimeUtils.getNextYearCurrentDate());
                userMembersService.insert(userMembers);
                userMembersList = userMembersService.select(userMembers);
            }
            map.put("mtime", userMembersList.get(0).getCtime());
            map.put("date", userMembersList.get(0).getEndTime());
            map.put("permanent",userMembersList.get(0).getPermanent());
        }
        return this.renderJson(MsgOut.success(map));
    }

    @RequestMapping("/api/mz/top/total")
    public String totalTop(UserProfit userProfit) {
        List<UserProfit> userProfits = userProfitService.selectKuaiZhuanTopForTotal(userProfit);
        SysUser user = null;
        if (!ObjectUtils.isEmpty(userProfits)) {
            for (UserProfit up : userProfits) {
                user = sysUserService.selectByPK(up.getUserId());
                up.setRemark(user.getNickname());//nickname
                StringBuffer sb = new StringBuffer(user.getPhone());
                sb.replace(3, 7, "****");
                up.setOrderSn(sb.toString());//phone
            }
        }
        MsgOut o = MsgOut.success(userProfits);
        o.setRecords(userProfit.getRecords());
        o.setTotal(userProfit.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping("/api/mz/top/week")
    public String weekTop(UserProfit userProfit) {
        LocalDate today = LocalDate.now();

        LocalDate monday = today;
        while (monday.getDayOfWeek() != DayOfWeek.MONDAY) {
            monday = monday.minusDays(1);
        }

        LocalDate sunday = today;
        while (sunday.getDayOfWeek() != DayOfWeek.SUNDAY) {
            sunday = sunday.plusDays(1);
        }
        ZoneId zoneId = ZoneId.systemDefault();
        LOGGER.info("zoneId = {}", zoneId);
        Long mondayEpoch = monday.atStartOfDay(zoneId).toEpochSecond();
        Long sundayEpoch = sunday.atStartOfDay(zoneId).toEpochSecond();
        userProfit.setStime(mondayEpoch * 1000);
        userProfit.setEtime((sundayEpoch + 24 * 60 * 60 - 1) * 1000);
        List<UserProfit> userProfits = userProfitService.selectKuaiZhuanTopForTotal(userProfit);
        SysUser user = null;
        if (!ObjectUtils.isEmpty(userProfits)) {
            for (UserProfit up : userProfits) {
                user = sysUserService.selectByPK(up.getUserId());
                up.setRemark(user.getNickname());//nickname
                StringBuffer sb = new StringBuffer(user.getPhone());
                sb.replace(3, 7, "****");
                up.setOrderSn(sb.toString());//phone
            }
        }
        MsgOut o = MsgOut.success(userProfits);
        o.setRecords(userProfit.getRecords());
        o.setTotal(userProfit.getTotal());
        return this.renderJson(o);
    }
    @RequestMapping("/api/mz/ranking/reward")
    public String rankingList(int type) {
        //七天的毫秒数
        Long oneDay = (24L * 60L * 60L *1000L);
        //获取七天前的开始时间
        Long dayBegin = TimeUtils.getLongDayBegin();
        //开始时间结束时间
        Long stime ,etime;
        List list = new ArrayList();
        for (int i=1;i < 8;i++){
            HashMap map = new HashMap();
           RelationsVO relationsVO = new RelationsVO();
           stime = dayBegin;
           etime = dayBegin + oneDay;
           relationsVO.setStime(stime);
           relationsVO.setEtime(etime);
            //查询数据
           List<RelationsVO> relationsVOS = new ArrayList<>();
           if(type == 1){   //直推
               relationsVOS=mzAdsService.selectRanking(relationsVO);
           } else if(type == 2) {   //间推
               relationsVOS=mzAdsService.selectRankingTwo(relationsVO);
           }
           if (!ObjectUtils.isEmpty(relationsVOS) && dayBegin > 1551023999000L){
               map.put("time",dayBegin.toString());
               map.put("data",relationsVOS);
               dayBegin = dayBegin - oneDay;
               list.add(map);
           }
        }
        MsgOut o = MsgOut.success(list);
        return this.renderJson(o);
    }

}
