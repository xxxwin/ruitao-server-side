/**
 * Created by neal on 5/21/2017.
 */

var Config = {
    getWxMpMenus: "/api/wx/menu/list",
    saveWxMpMenus: "/api/wx/menu/create"
};
var buttonAll = {};
var editor = {};
    $(document).ready(function () {
    $.ajax(
    {
        url : Config.getWxMpMenus,
        type: "GET",
        dataType: "json",
        success:function(data)
        {
            if(data && data.type == 'SUCCESS'){
                oneMenuRender(data.data.menu.button);
                $('#code1').text(JSON.stringify(data.data.menu));
                editor = CodeMirror.fromTextArea(document.getElementById("code1"), {
                    mode: "application/json",
                    lineNumbers: true,
                    matchBrackets: true,
                    lineWrapping: true,
                    styleActiveLine: true
                });
                editor.setSize('auto', 'auto');
            }else {
                toastr.error(data.msg, data.title);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            toastr.error(errorThrown, textStatus);
        }
    });
});

$(".save-menu").click(function(){
    $.ajax({
        type:"POST",
        url: Config.saveWxMpMenus,
        dataType: "json",
        data : {"json": editor.getValue()},
        async: false,
        success:function(data){
            toastr.success(data.msg, data.title);
        }
    });
});
$(".rt-test").click(function(){
    //
    // {{first.DATA}}
    // 交易类型：{{keyword1.DATA}}
    // 交易方式：{{keyword2.DATA}}
    // 交易金额：{{keyword3.DATA}}
    // 交易时间：{{keyword4.DATA}}
    // {{remark.DATA}}

    var data = {
        "touser":"o7E6KuHcQAEpTe-ljP6IIhLK15Gg",//"o7E6KuAaaNBKiaqK3S-Dz3L71IQw",//"o7E6KuHcQAEpTe-ljP6IIhLK15Gg",
        "template_id":"ek8Avk33h2GbOAliXrJJgK084X9vVgaESVfmVqmNI2M",
        "url":"http://weixin.qq.com/download",
        "topcolor":"#FF0000",
        "data":{
            "first": {
                "value":"您的排队快要到号啦，请及时来店用餐！",
                "color":"#173177"
            },
            "keyword1":{
                "value":"龙蛙欢乐主题餐厅",
                "color":"#0044BB"
            },
            "keyword2":{
                "value":"8888",
                "color":"#173177"
            },
            "keyword3":{
                "value":"8人桌",
                "color":"#173177"
            },
            "keyword4":{
                "value":"3 位",
                "color":"#173177"
            },
            "remark":{
                "value":"听到叫号请及时就餐，祝您用餐愉快",
                "color":"#FF3333"
            }
        }
    };
    $.ajax({
        type:"GET",
        url: "/api/wx/message/template/send",
        dataType: "json",
        data : {"data": JSON.stringify(data)},
        async: false,
        success:function(data){
            toastr.success(data.msg, data.title);
        }
    });
});
function oneMenuRender(buttons) {
    buttonAll = buttons;
    var size = buttons.length;
    if(size <= 0) {
        return "";
    }
    var menus = [];
    for(var i=0; i<size; i++){
        menus.push('<button onclick="twoMenuRender('+i+')" data-toggle="button" class="btn btn-primary btn-outline active" type="button" aria-pressed="true">'+buttons[i].name+'</button>');
    }
    $('#wx_mp_menu_one').html(menus.join(''));
}

function twoMenuRender(index) {
    var buttons = buttonAll[index].sub_button;
    console.log(buttons)
    $('#wx_mp_menu_two').html('');
    var size = buttons.length;
    if(size <= 0) {
        return "";
    }
    var menus = [];
    for(var i=0; i<size; i++){
        menus.push('<button onclick="detailMenuRender('+i+')" data-toggle="button" class="btn btn-primary btn-outline active" type="button" aria-pressed="true">'+buttons[i].name+'</button>');
    }
    $('#wx_mp_menu_two').html(menus.join(''));
}
function detailMenuRender(index) {
    console.log(index)
    $('#wx_mp_menu_two').html('');
    var size = buttonAll[index].length;
    if(size <= 0) {
        return "";
    }
    var menus = [];
    for(var i=0; i<size; i++){
        menus.push('<button onclick="detailMenuRender('+i+')" data-toggle="button" class="btn btn-primary btn-outline active" type="button" aria-pressed="true">'+buttons[i].name+'</button>');
    }
    $('#wx_mp_menu_two').html(menus.join(''));
}