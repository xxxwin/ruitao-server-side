package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CompanyType;

import java.util.List;

public interface CompanyTypeAdMapper {
    int insert(CompanyType companyType);

    int deleteByPK(Long id);

    int updateByPK(CompanyType companyType);

    CompanyType selectByPK(Long id);

    long count(CompanyType companyType);

    List<CompanyType> select(CompanyType companyType);
}