package com.ruitaowang.core.domain;

public class ShoppingCart extends Page {
    /**
     *  编号,所属表字段为shopping_cart.cart_id
     */
    private Long cartId;

    /**
     *  商品分类编号,所属表字段为shopping_cart.goods_id
     */
    private Long goodsId;

    /**
     *  用户编号,所属表字段为shopping_cart.user_id
     */
    private Long userId;

    /**
     *  创建时间,所属表字段为shopping_cart.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为shopping_cart.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常，1 删除,所属表字段为shopping_cart.rstatus
     */
    private Byte rstatus;

    /**
     *  商品属性ID，以逗号分隔.eg: 1，2，3,所属表字段为shopping_cart.goods_attr_link_id
     */
    private String goodsAttrLinkId;

    /**
     *  ,所属表字段为shopping_cart.goods_number
     */
    private Integer goodsNumber;

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getGoodsAttrLinkId() {
        return goodsAttrLinkId;
    }

    public void setGoodsAttrLinkId(String goodsAttrLinkId) {
        this.goodsAttrLinkId = goodsAttrLinkId == null ? null : goodsAttrLinkId.trim();
    }

    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
}