package com.ruitaowang.admin.view.api;

import com.ruitaowang.core.domain.HistoricalRecord;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.HistoricalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class APIHistoricalRecordController extends BaseController {

    @Autowired
    private HistoricalRecordService historicalRecordService;

    /**
     * 获取所有历史搜索记录
     * @param historicalRecord
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/history", method = RequestMethod.GET)
    public String findHistory(HistoricalRecord historicalRecord,int page,@RequestHeader(value = "headId", required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        historicalRecord.setPage(page);
        historicalRecord.setUserId(userId);
        MsgOut o = MsgOut.success(historicalRecordService.selectForPage(historicalRecord));
        o.setRecords(historicalRecord.getRecords());
        o.setTotal(historicalRecord.getTotal());
        return this.renderJson(o);
    }

    /**
     * 首页商品搜索时新增历史记录
     * @param historicalRecord
     * @param userId
     * @param goodsName
     * @return
     */
    @RequestMapping(value = "/api/wap/history/goods", method = RequestMethod.POST)
    public String insertHistoryForGoods(HistoricalRecord historicalRecord,@RequestHeader(value = "headId", required = false) Long userId,String goodsName){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(goodsName)) {
            historicalRecord.setHistoryName(goodsName);
            historicalRecord.setUserId(userId);
            historicalRecord.setHistoryType("1");
            List<HistoricalRecord> historicalRecordList = historicalRecordService.select(historicalRecord);
            if (ObjectUtils.isEmpty(historicalRecordList)) {
                historicalRecordService.insert(historicalRecord);
            }
            MsgOut o = MsgOut.success("操作成功了！");
            return this.renderJson(o);
        }else{
            return this.renderJson(MsgOut.error("进入到异次元空间"));
        }
    }

    /**
     * 首页店铺搜索时新增历史记录
     * @param historicalRecord
     * @param userId
     * @param companyName
     * @return
     */
    @RequestMapping(value = "/api/wap/history/company", method = RequestMethod.POST)
    public String insertHistoryForCompany(HistoricalRecord historicalRecord,@RequestHeader(value = "headId", required = false) Long userId,String companyName){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(companyName)) {
            historicalRecord.setHistoryName(companyName);
            historicalRecord.setUserId(userId);
            historicalRecord.setHistoryType("2");
            List<HistoricalRecord> historicalRecordList = historicalRecordService.select(historicalRecord);
            if (ObjectUtils.isEmpty(historicalRecordList)) {
                historicalRecordService.insert(historicalRecord);
            }
            MsgOut o = MsgOut.success("操作成功了！");
            return this.renderJson(o);
        }else{
            return this.renderJson(MsgOut.error("进入到异次元空间"));
        }
    }

    /**
     * 删除历史记录
     * @param historicalRecord
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/history", method = RequestMethod.DELETE)
    public String deleteHistory(HistoricalRecord historicalRecord,@RequestHeader(value = "headId", required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        String historyType = historicalRecord.getHistoryType();
        MsgOut o = MsgOut.success(historicalRecordService.deleteForAll(userId,historyType));
        return this.renderJson(o);
    }

}
