/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/admin/goodsType/post',
    updateUrl: '/api/admin/goodsType/update',
    deleteUrl: '/api/goodsType/',
    selectUrl: '/api/admin/goodsType/get'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap"
    $("#goods_type_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["类型ID", "类型名称", "类型分组", "创建时间", "最后修改时间", "操作"],
        colModel: [{
            name: "typeId",
            index: "typeId",
            width: 60,
            sorttype: "int",
            search: true
        }, {
            name: "typeName",
            index: "typeName",
            width: 90
        }, {
            name: "typeGroup",
            index: "typeGroup",
            width: 80,
        }, {
            name: "ctime",
            index: "ctime",
            width: 80,
        }, {
            name: "mtime",
            index: "mtime",
            width: 80,
        }, {
            name: "operate",
            index: "operate",
            width: 100,
            sortable: false
        }
        ],
        pager: "#pager_goods_type_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "typeId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#goods_type_list").jqGrid('navGrid', '#pager_goods_type_list', {
        add: false,
        edit: false,
        del: false,
        search: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#goods_type_list").setGridWidth(width);
    });
    // $.ajax(
    //     {
    //         url: Config.selectUrl,
    //         type: "GET",
    //         dataType: "json",
    //         async: false,
    //         success: function (data, textStatus, jqXHR) {
    //             //add operate for data
    //             if (data && data.data && data.data.length > 0) {
    //                 for (var i = 0; i < data.data.length; i++) {
    //                     var id = data.data[i].typeId;
    //                     data.data[i].id=data.data[i].typeId;
    //                     data.data[i].ctime = new Date(data.data[i].ctime).yyyymmddhhmmss();
    //                     data.data[i].mtime = new Date(data.data[i].mtime).yyyymmddhhmmss();
    //                     var  html = '<a style="color:#f60;padding-right: 15px"  href="/goodsType/update/' + id + '">编辑</a>';
    //                     html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + "," + (i + 1) + ")' >删除</a>";
    //                     data.data[i].operate = html;
    //                 }
    //             }
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             console.error(errorThrown);
    //         }
    //     });

});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#goods_type_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#goods_type_list").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#goods_type_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#goods_type_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        dataFromTheRow.mtime = new Date(dataFromTheRow.mtime * 1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/goodsType/update/'+id+'" data-index="0">编辑</a>';
        html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + ","+  (i+1) +")' >删除</a>";
        jQuery("#goods_type_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
            mtime: dataFromTheRow.mtime
        });
    }
}