package com.ruitaowang.account.service;

import com.ruitaowang.account.dao.*;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.core.utils.LYBeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by neal on 11/7/16.
 */
@Service
public class ResourceService implements BaseService<Resource> {

    @Autowired
    private ResourceMapper resourceMapper;
    @Autowired
    private ResourceAdMapper resourceAdMapper;
    @Autowired
    private RoleResourceLinkMapper roleResourceLinkMapper;
    @Autowired
    private RoleResourceLinkAdMapper roleResourceLinkAdMapper;
    @Autowired
    private UserRoleLinkMapper userRoleLinkMapper;
    @Autowired
    private RoleMapper roleMapper;

    public Resource insert(Resource resource){
        Long time = System.currentTimeMillis();
        resource.setCtime(time);
        resource.setMtime(time);
        resourceMapper.insert(resource);
        return resource;
    }

    public Resource update(Resource resource){
        resourceMapper.updateByPK(resource);
        return resource;
    }

    public int delete(Long id){

        return resourceMapper.deleteByPK(id);
    }

    public Resource selectByPK(Long id) {
        return resourceMapper.selectByPK(id);
    }

    public List<Resource> select(Resource resource){
        return resourceMapper.select(resource);
    }

    public List<Resource> selectByRoleId(Long... roleId){
        if(roleId == null || roleId.length == 0){
            return  null;
        }

        List<RoleResourceLink> roleResourceLinks = roleResourceLinkAdMapper.selectBatch(Arrays.asList(roleId));

        if(roleResourceLinks == null || roleResourceLinks.size() == 0){
            return null;
        }
        List<Long> resourceIds = new ArrayList<>();
        roleResourceLinks.forEach(item -> {
            resourceIds.add(item.getResourceId());

        });
        return resourceAdMapper.selectBatch(resourceIds);
    }
    public List<Resource> selectByResourceLink(String... resourceLink) {
        if (resourceLink == null || resourceLink.length == 0) {
            return null;
        }
        for (String rl : resourceLink) {
            if("root".equals(rl)){
                return resourceMapper.select(new Resource());
            }
        }
        return resourceAdMapper.selectBatchByLink(Arrays.asList(resourceLink));
    }

    public List<ResourceMenu> selectMyGroupResources(){
        List<Long> roleIds = new ArrayList<>();
        List<Role> roles=roleMapper.select(new Role());
        for (Role role: roles){
            roleIds.add(role.getId());
        }
        List<Resource> allResources = this.selectByRoleId(roleIds.toArray(new Long[]{}));
        List<Resource> pResources = new ArrayList<>();
        for (Resource resourse : allResources) {
            if(!ObjectUtils.isEmpty(resourse.getPid())  && resourse.getIsShow() == 0 && resourse.getPid() == 0 && resourse.getType() == 1){
                pResources.add(resourse);
            }
        }
        ResourceMenu resourse = null;
        List<ResourceMenu> resourceMenus = new ArrayList<>();
        for (int i=0; i<pResources.size(); i++) {
            resourse = new ResourceMenu();
            LYBeanUtils.copyProperties(pResources.get(i), resourse);
            resourse.setResources(new ArrayList<>());
            for (Resource inner : allResources) {
                if(!ObjectUtils.isEmpty(inner.getPid()) && inner.getIsShow() == 0 && resourse.getId() == inner.getPid() && inner.getType() == 1){
                    resourse.getResources().add(inner);
                }
            }
            resourceMenus.add(resourse);
        }
        return resourceMenus;
    }

    public void grant(Long roleId, String menuIds){

        if (menuIds == null){
            return;
        }
        roleResourceLinkAdMapper.deleteByRoleId(roleId);
        if ("-1".equals(menuIds)){
            return;//清空全部
        }
        Arrays
        .asList(menuIds.split(","))
        .stream()
        .map(item -> Long.valueOf(item))
        .forEach(resourceId ->
        {
            RoleResourceLink roleResourceLink = new RoleResourceLink();
            roleResourceLink.setRoleId(roleId);
            roleResourceLink.setResourceId(resourceId);
            Long time = System.currentTimeMillis();
            roleResourceLink.setCtime(time);
            roleResourceLink.setMtime(time);
            roleResourceLinkMapper.insert(roleResourceLink);

        });
    }
}
