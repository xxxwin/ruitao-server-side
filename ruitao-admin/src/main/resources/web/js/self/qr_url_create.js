var thisImg;
var thisText;
var thisQrColor;
var thisQrContextColor;
var thisUrl;
var qrpx;
$('#createQr').click(function () {
    if ($("input[id=textUrl]").val() == "") {
        alert("请正确输入文本")
        return false;
    }
    console.log(thisQrColor);
    console.log(thisQrContextColor);
    thisUrl = $("input[id=textUrl]").val();

    if (!thisQrColor) {
        thisQrColor = "#000000";
        console.log(thisQrColor);
    }
    if (!thisQrContextColor) {
        thisQrContextColor = "#ffffff"
        console.log(thisQrContextColor);
    }
    // qrpx=$('#qrSelect option:selected').val();
    // if(!qrpx){
    //     qrpx=400;
    // }
    var qrcode = new QRCode("qrcode", {
        text: $("input[id=textUrl]").val(),
        width: 400,
        height: 400,
        colorDark: thisQrContextColor,
        colorLight: thisQrColor,
        correctLevel: QRCode.CorrectLevel.H
    });
});


thisImg = $("#qrcode").find("img").attr('src');
$('#textSubmit').click(function () {
    thisText = $("input[id=text]").val();
    var canvas = document.getElementsByTagName("canvas")[0];
    var ctx = canvas.getContext('2d');
    ctx.font = "30px Arial";
    ctx.fillStyle = "#ffffff";
    var width = ctx.measureText($("input[id=text]").val()).width;
    var x = 200 - width / 2 - 5;
    var y = 180;
    ctx.fillRect(x, y, width + 10, 40);
    ctx.textAlign = "center";
    ctx.fillStyle = '#179d82';
    ctx.textBaseline = 'middle';
    ctx.fillText($("input[id=text]").val(), 200, 200);
    var dataUrl = canvas.toDataURL();
    $("#qrcode").find("img").attr("src", dataUrl);
});


$('.textContextColor').colorpicker().on('changeColor', function (ev) {
    var canvas = document.getElementsByTagName("canvas")[0];
    var ctx = canvas.getContext('2d');
    ctx.font = "30px Arial";
    ctx.fillStyle = ev.color.toHex();
    var width = ctx.measureText(thisText).width;
    var x = 200 - width / 2 - 5;
    var y = 180;
    ctx.fillRect(x, y, width + 10, 40);
    ctx.textAlign = "center";
    ctx.textBaseline = 'middle';
    ctx.fillText(thisText, 200, 200);
    var dataUrl = canvas.toDataURL();
    $("#qrcode").find("img").attr("src", dataUrl);
});
$('.textColor').colorpicker().on('changeColor', function (ev) {
    var canvas = document.getElementsByTagName("canvas")[0];
    var ctx = canvas.getContext('2d');
    ctx.font = "30px Arial";
    var width = ctx.measureText(thisText).width;
    var x = 200 - width / 2 - 5;
    var y = 180;
    ctx.fillRect(x, y, width + 10, 40);
    ctx.textAlign = "center";
    ctx.fillStyle = ev.color.toHex();
    ctx.textBaseline = 'middle';
    ctx.fillText(thisText, 200, 200);
    var dataUrl = canvas.toDataURL();
    $("#qrcode").find("img").attr("src", dataUrl);
});


$('.qrColor').colorpicker().on('changeColor', function (ev) {
    thisQrColor = ev.color.toHex();
});
$('.qrContextColor').colorpicker().on('changeColor', function (ev) {
    thisQrContextColor = ev.color.toHex();
});
