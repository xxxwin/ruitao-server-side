package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserReward;
import com.ruitaowang.core.domain.Wallet;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserRewardAdMapper;
import com.ruitaowang.goods.dao.UserRewardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class UserRewardService implements BaseService<UserReward> {
    @Autowired
    private UserRewardMapper userRewardMapper;
    @Autowired
    private UserRewardAdMapper userRewardAdMapper;

    @Override
    public UserReward insert(UserReward userReward) {
        Long time = System.currentTimeMillis();
        userReward.setCtime(time);
        userReward.setMtime(time);
        userRewardMapper.insert(userReward);
        return userReward;
    }

    @Override
    public int delete(Long id) {
        UserReward userReward = new UserReward();
        userReward.setRewardId(id);
        userReward.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userReward.setMtime(time);
        return userRewardMapper.updateByPK(userReward);
    }

    @Override
    public UserReward update(UserReward userReward) {
        Long time = System.currentTimeMillis();
        userReward.setMtime(time);
        userRewardMapper.updateByPK(userReward);
        return userReward;
    }

    @Override
    public UserReward selectByPK(Long id) {
        return userRewardMapper.selectByPK(id);
    }

    @Override
    public List<UserReward> select(UserReward userReward) {
//        userReward.setRstatus((byte) 0);
        return userRewardMapper.select(userReward);
    }

    public List<UserReward> selectForPage(UserReward userReward) {
//        userReward.setRstatus((byte) 0);
        userReward.setRecords(userRewardMapper.count(userReward));
        return userRewardMapper.select(userReward);
    }
    public List<UserReward> selectUserReward(UserReward userReward) {
        return userRewardAdMapper.select(userReward);
    }

}
