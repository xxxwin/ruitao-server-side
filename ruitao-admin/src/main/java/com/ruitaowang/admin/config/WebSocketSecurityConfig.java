//package com.ruitaowang.admin.config;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
//import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
//import org.springframework.security.messaging.access.intercept.ChannelSecurityInterceptor;
//
//@Configuration
//public class WebSocketSecurityConfig
//		extends AbstractSecurityWebSocketMessageBrokerConfigurer {
//
//	@Override
//	protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
////		messages
////				.simpMessageDestMatchers("/app/**").permitAll()
////				.simpSubscribeDestMatchers("/queue/**", "/topic/**").permitAll()
////				.anyMessage().authenticated();
//	}
//
//	/**
//	 * Disables CSRF for Websockets.
//	 * @return
//	 */
//	@Override
//	protected boolean sameOriginDisabled() {
//		return true;
//	}
//
//	// @formatter:on
//}
