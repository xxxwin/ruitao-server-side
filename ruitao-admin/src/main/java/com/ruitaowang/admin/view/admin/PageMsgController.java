package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.LsMsg;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.LsMsgService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by xinchunting on 18-2-9.
 */
@RestController
public class PageMsgController extends BaseController {
    @Autowired
    private LsMsgService lsMsgService;


    @RequestMapping(value = {"/blessing/update/{id}"}, method = RequestMethod.GET)
    public ModelAndView update(@PathVariable("id") Long id, Model model) {
       LsMsg lsMsg=lsMsgService.selectByPK(id);
        model.addAttribute("lsMsg", lsMsg);
        return new ModelAndView("web/blessing_update");
    }
}
