package com.ruitaowang.goods.service;

import com.ruitaowang.account.dao.CompanyStaffMapper;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CompanyAdMapper;
import com.ruitaowang.goods.dao.CompanyIndustryAdMapper;
import com.ruitaowang.goods.dao.CompanyMapper;
import com.ruitaowang.goods.dao.GoodsSearchMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shaka on 2017/1/4.
 */
@Service
@Transactional
public class CompanyService implements BaseService<Company> {
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private CompanyAdMapper companyAdMapper;
    @Autowired
    private CompanyProductService companyProductService;
    @Autowired
    private GoodsSearchMapper goodsSearchMapper;
    @Autowired
    private CompanyIndustryAdMapper companyIndustryAdMapper;
    @Autowired
    private CompanyStaffMapper companyStaffMapper;

    @Override
    public Company insert(Company company) {
        Long time = System.currentTimeMillis();
        company.setCtime(time);
        company.setMtime(time);
        companyMapper.insert(company);
        return company;
    }

    @Override
    public int delete(Long id) {
        Company company = new Company();
        company.setCompanyId(id);
        company.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        company.setMtime(time);
        return companyMapper.updateByPK(company);
    }

    @Override
    public Company update(Company company) {
        Long time = System.currentTimeMillis();
        company.setMtime(time);
        companyMapper.updateByPK(company);
        return companyMapper.selectByPK(company.getCompanyId());
    }

    public Company updateNew(Company company) {
        Long time = System.currentTimeMillis();
        company.setMtime(time);
        companyAdMapper.updateByPK(company);
        return companyMapper.selectByPK(company.getCompanyId());
    }

    @Override
    public Company selectByPK(Long id) {
        return companyMapper.selectByPK(id);
    }

    @Override
    public List<Company> select(Company company) {
        company.setRstatus((byte) 0);
        return companyMapper.select(company);
    }

    public List<Company> selectForPage(Company company) {
        company.setRstatus((byte) 0);
        company.setRecords(companyMapper.count(company));
        return companyMapper.select(company);
    }

    public List<Company> selectSearchForPage(Company company) {
        company.setRstatus((byte) 0);
        company.setRecords(companyMapper.count(company));
        return companyMapper.select(company);
    }
    /**
     * 根据userId查公司
     * @param userId
     * @return
     */
    public Company selectByUserId(Long userId) {
        Company company =new Company();
        company.setUserId(userId);
        company.setRstatus((byte)0);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            return null;
        }
        return companyList.get(0);
    }

    /**
     * 根据userId和companyType查公司
     * @param userId
     * @return
     */
    public Company selectByUserIdAndCompanyType(Long userId, Byte companyType) {
        Company company =new Company();
        company.setUserId(userId);
        company.setCompanyType(companyType);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            return null;
        }
        return companyList.get(0);
    }

    /**
     * 根据companyId查找是否微商
     * @param companyId
     * @return
     */
    public Company selectMicroUserByCompanyId(Long companyId) {
        Company company = companyMapper.selectByPK(companyId);
        if(ObjectUtils.isEmpty(company) || company.getCompanyType() != 1){
            return null;
        }
        return company;
    }

    /**
     * 根据userId查找微商
     * @param userId
     * @return
     */
    public Company selectMicroUserByUserId(Long userId) {
        Company company = new Company();
        company.setUserId(userId);
        company.setCompanyType((byte) 1);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            return null;
        }
        return companyList.get(0);
    }
    /**
     * 根据userId查找互动商家
     * @param userId
     * @return
     */
    public Company selectInteractionByUserId(Long userId) {
        Company company = new Company();
        company.setUserId(userId);
        company.setCompanyType((byte) 11);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            Company c = new Company();
            c.setUserId(userId);
            c.setCompanyType((byte) 13);
            List<Company> companies = companyMapper.select(c);
            if (ObjectUtils.isEmpty(companies)){
                return null;
            }else {
                return companies.get(0);
            }
        }
        return companyList.get(0);
    }
    /**
     * 根据userId查找餐饮商家
     * @param userId
     * @return
     */
    public Company selectDinnerByUserId(Long userId) {
        Company company = new Company();
        company.setUserId(userId);
        company.setCompanyType((byte) 10);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            return null;
        }
        return companyList.get(0);
    }

    /**
     * 根据userId查找梦想合伙人
     * @param userId
     * @return
     */
    public Company selectBizCoUserByUserIdV1(Long userId) {
        Company company = new Company();
        company.setUserId(userId);
        company.setCompanyType((byte) 4);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            return null;
        }
        return companyList.get(0);
    }

    /**
     * 根据userId查找供货商
     * @param userId
     * @return
     */
    public Company selectProviderByUserId(Long userId) {//快推
        Company company = new Company();
        company.setUserId(userId);
        company.setCompanyType((byte) 8);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            return null;
        }
        return companyList.get(0);
    }

    /**
     * 查询公司优惠信息
     * @param companyId
     * @return
     */
    public CompanyProduct selectProductByCompanyId(Long companyId) {
        CompanyProduct companyProduct = new CompanyProduct();
        companyProduct.setCompanyId(companyId);
        List<CompanyProduct> companyProducts = companyProductService.select(companyProduct);
        if(ObjectUtils.isEmpty(companyProducts)){
            return null;
        }
        return companyProducts.get(0);
    }

//    @Transactional(value="transactionManager", rollbackFor = Exception.class)
    @Transactional
    public Company rollback() {
        Company company = new Company();
        Long time = System.currentTimeMillis();
        company.setCompanyName("neal");
        company.setCtime(time);
        company.setMtime(time);
        companyMapper.insert(company);
        int i = 1/0;
        return company;
    }

    public Company norollback() {
        Company company = new Company();
        Long time = System.currentTimeMillis();
        company.setCompanyName("neal");
        company.setCtime(time);
        company.setMtime(time);
        companyMapper.insert(company);
        return company;
    }


    public List<Company> selectCompanyIndustryIdForPage(CompanyVO companyVO) {
        companyVO.setRecords(companyAdMapper.selectCompanyIndustryIdCount(companyVO));
        return companyAdMapper.selectCompanyIndustryIdForPage(companyVO);
    }


    public List<CompanyVO> findCompanyGoods(Company company) {
        company.setRecords(companyAdMapper.findCompanyGoodsCount(company));
        return companyAdMapper.findCompanyGoods(company);
    }

    public List<CompanyVO> findCompanyGoodsNearBy(Company company) {
        company.setRecords(companyAdMapper.findCompanyGoodsNearByCount(company));
        return companyAdMapper.findCompanyGoodsNearBy(company);
    }

    public List<Company> searchCompany(Company company){
        company.setRecords(companyAdMapper.searchCompanyCount(company));
        return companyAdMapper.searchCompany(company);
    }

    public List<Company> searchCompanyByNear(CompanyVO companyVO){
        companyVO.setRecords(companyAdMapper.searchCompanyByNearCount(companyVO));
        return companyAdMapper.searchCompanyByNear(companyVO);
    }
//    private int pageNum(int page,int rows){
//        if (page == 1 || page == 0){
//            page=0;
//        }else {
//            page=page * rows;
//        }
//
//        return page;
//    }

    public long count(Company company){
        company.setRstatus((byte) 0);
        return companyMapper.count(company);
    }

    public List<CompanyVO> selectForInteractive(Company company){
        company.setRecords(companyAdMapper.selectForInteractiveCount(company));
        return companyAdMapper.selectForInteractive(company);
    }

    public List<CompanyVO> selectForMyInteractive(Company company){
        company.setRecords(companyAdMapper.selectForMyInteractiveCount(company));
        return companyAdMapper.selectForMyInteractive(company);
    }

    public CompanyIndustry selectForCommission(Long id){
        return companyIndustryAdMapper.selectForCommission(id);
    }

    /**
     * 增加员工查询公司
     * @param userId
     * @return
     */
    public Company selectByCompanyStaff(Long userId) {
        Company company =new Company();
        company.setUserId(userId);
        company.setRstatus((byte)0);
        List<Company> companyList = companyMapper.select(company);
        if(ObjectUtils.isEmpty(companyList)){
            CompanyStaff companyStaff = new CompanyStaff();
            companyStaff.setUserId(userId);
            companyStaff.setStatus(1);
            List<CompanyStaff> companyStaffList = companyStaffMapper.select(companyStaff);
            if(ObjectUtils.isEmpty(companyStaffList)){
                return null;
            } else {
                Long companyId = companyStaffList.get(0).getCompanyId();
                Company company1 = companyMapper.selectByPK(companyId);
                return company1;
            }
        }
        return companyList.get(0);
    }

    public List<Company> selectByCZ(Company company) {
        company.setRecords(companyAdMapper.selectByCZCount(company));
        return companyAdMapper.selectByCZ(company);
    }
}
