package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.SNSWXUserinfo;
import java.util.List;

public interface SNSWXUserinfoMapper {
    int insert(SNSWXUserinfo sNSWXUserinfo);

    int deleteByPK(Long userId);

    int updateByPK(SNSWXUserinfo sNSWXUserinfo);

    SNSWXUserinfo selectByPK(Long userId);

    long count(SNSWXUserinfo sNSWXUserinfo);

    List<SNSWXUserinfo> select(SNSWXUserinfo sNSWXUserinfo);
}