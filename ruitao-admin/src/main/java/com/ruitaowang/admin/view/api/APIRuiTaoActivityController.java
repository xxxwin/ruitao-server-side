package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.Order;
import com.ruitaowang.core.domain.OrderProd;
import com.ruitaowang.core.domain.UserProfit;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.RuiTaoActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/5/21.
 */
@RestController
public class APIRuiTaoActivityController extends BaseController {

    @Autowired
    private RuiTaoActivityService ruiTaoActivityService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SysUserService sysUserService;


    /**
     * @api {get} /api/admin/activity/share 后台管理-广告管理-龙蛙活动-分享专区用户消费 ≥300元
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/activity/share
     *
     * @apiName adminShare
     * @apiGroup ruiTaoActivity
     *
     * @apiParam {long} stime 开始时间.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *               "consignee":"梁怀庆",
     *               "exPrice":0,
     *               "memberPrice":0,
     *               "mobile":"13701366281",
     *               "orderAmount":1800000,
     *               "payStatus":1,
     *               "userId":62458
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/activity/share"
     *  }
     */


    @RequestMapping(value = "/api/admin/activity/share", method = RequestMethod.GET)
    public String adminShare(Order order) {
        List<Order> orders=ruiTaoActivityService.selectShare(order);
        List<Order> users= new ArrayList<>();
        for (Order od:orders){
            if (od.getOrderAmount() + od.getExPrice() - od.getMemberPrice() > 30000 || od.getOrderAmount() + od.getExPrice() - od.getMemberPrice() == 30000){
                od.setOrderAmount(od.getExPrice() + od.getOrderAmount() - od.getMemberPrice());
                users.add(od);
            }
        }
        MsgOut o = MsgOut.success(users);
        return this.renderJson(o);
    }


    /**
     * @api {get} /api/admin/activity/tinyShop 后台管理-广告管理-龙蛙活动-微商销量每月前三名
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/activity/tinyShop
     *
     * @apiName adminTinyShop
     * @apiGroup ruiTaoActivity
     *
     * @apiParam {long} stime 开始时间.
     * @apiParam {long} etime 结束时间.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "goodsProviderId":113,
     *                   "goodsProviderType":1,
     *                   "goodsScreenPrice":25500,
     *                   "logisticsSn":"13716273519",
     *                   "remark":"北京中皓国际科贸有限公司"
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/activity/tinyShop"
     *  }
     */


    @RequestMapping(value = "/api/admin/activity/tinyShop", method = RequestMethod.GET)
    public String adminTinyShop(OrderProd orderProd) {
        List<OrderProd> orderProds;
        if (ObjectUtils.isEmpty(orderProd.getStime()) || ObjectUtils.isEmpty(orderProd.getEtime())) {
            LocalDate today = LocalDate.now();
            LocalDate firstmonthday = today;
            firstmonthday = firstmonthday.minusDays(firstmonthday.getDayOfMonth() - 1);

            LocalDate lastmonthdayday = today;
            lastmonthdayday = lastmonthdayday.plusDays(5);
            lastmonthdayday = lastmonthdayday.plusMonths(1);
            lastmonthdayday = lastmonthdayday.minusDays(lastmonthdayday.getDayOfMonth() - 1);

            ZoneId zoneId = ZoneId.systemDefault();
            LOGGER.info("zoneId = {}", zoneId);
            Long mondayEpoch = firstmonthday.atStartOfDay(zoneId).toEpochSecond();
            Long sundayEpoch = lastmonthdayday.atStartOfDay(zoneId).toEpochSecond();
            orderProd.setStime(mondayEpoch);
            orderProd.setEtime(sundayEpoch);
            orderProds = ruiTaoActivityService.selectTinyShop(orderProd);
            for (OrderProd op : orderProds) {
                op.setRemark(companyService.selectByPK(op.getGoodsProviderId()).getCompanyName());
                op.setLogisticsSn(companyService.selectByPK(op.getGoodsProviderId()).getMobile());
            }
        }else {
            orderProds = ruiTaoActivityService.selectTinyShop(orderProd);
            for (OrderProd op : orderProds) {
                op.setRemark(companyService.selectByPK(op.getGoodsProviderId()).getCompanyName());
                op.setLogisticsSn(companyService.selectByPK(op.getGoodsProviderId()).getMobile());
            }
        }
        MsgOut o = MsgOut.success(orderProds);
        return this.renderJson(o);
    }


    /**
     * @api {get} /api/admin/activity/fastTurn 后台管理-广告管理-龙蛙活动-快转收益每周前三名
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/activity/fastTurn
     *
     * @apiName adminFastTurn
     * @apiGroup ruiTaoActivity
     *
     * @apiParam {long} stime 开始时间.
     * @apiParam {long} etime 结束时间.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "orderSn":"13269214438",
     *                   "remark":"梁焕",
     *                   "userId":3955,
     *                   "userProfit":12000
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/activity/fastTurn"
     *  }
     */


    @RequestMapping(value = "/api/admin/activity/fastTurn", method = RequestMethod.GET)
    public String adminFastTurn(UserProfit userProfit) {
        List<UserProfit> userProfits;
        if (ObjectUtils.isEmpty(userProfit.getStime()) || ObjectUtils.isEmpty(userProfit.getEtime())){
            LocalDate today = LocalDate.now();

            LocalDate monday = today;
            while (monday.getDayOfWeek() != DayOfWeek.MONDAY)
            {
                monday = monday.minusDays(1);
            }

            LocalDate sunday = today;
            while (sunday.getDayOfWeek() != DayOfWeek.SUNDAY)
            {
                sunday = sunday.plusDays(1);
            }
            ZoneId zoneId = ZoneId.systemDefault();
            LOGGER.info("zoneId = {}", zoneId);
            Long mondayEpoch = monday.atStartOfDay(zoneId).toEpochSecond();
            Long sundayEpoch = sunday.atStartOfDay(zoneId).toEpochSecond();
            userProfit.setStime(mondayEpoch*1000);
            userProfit.setEtime((sundayEpoch + 24 * 60 * 60 -1 ) * 1000);
            userProfits=ruiTaoActivityService.selectFastTurn(userProfit);
            for (UserProfit up:userProfits){
                up.setRemark(sysUserService.selectByPK(up.getUserId()).getNickname());
                up.setOrderSn(sysUserService.selectByPK(up.getUserId()).getPhone());
            }
        }else {
            userProfits=ruiTaoActivityService.selectFastTurn(userProfit);
            for (UserProfit up:userProfits){
                up.setRemark(sysUserService.selectByPK(up.getUserId()).getNickname());
                up.setOrderSn(sysUserService.selectByPK(up.getUserId()).getPhone());
            }
        }
        MsgOut o = MsgOut.success(userProfits);
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/admin/activity/companyRegister 后台管理-广告管理-龙蛙活动-实体商家注册前20名
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/activity/companyRegister
     *
     * @apiName adminCompanyRegister
     * @apiGroup ruiTaoActivity
     *
     * @apiParam {long} stime 开始时间.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "companyId":139,
     *                   "companyName":"福雅日用塑料制品厂",
     *                   "ctime":1495416319247,
     *                   "linkman":"钟俊佳",
     *                   "mobile":"06638673964"
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/activity/companyRegister"
     *  }
     */


    @RequestMapping(value = "/api/admin/activity/companyRegister", method = RequestMethod.GET)
    public String adminCompanyRegister(Company company) {
        MsgOut o = MsgOut.success(ruiTaoActivityService.selectCompanyRegister(company));
        return this.renderJson(o);
    }
}
