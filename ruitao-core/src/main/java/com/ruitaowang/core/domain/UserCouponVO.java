package com.ruitaowang.core.domain;

public class UserCouponVO extends UserCoupon {


    private String companyName;
    private String goodsName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }


}
