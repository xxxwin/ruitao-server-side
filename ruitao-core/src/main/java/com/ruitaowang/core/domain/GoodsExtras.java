package com.ruitaowang.core.domain;

public class GoodsExtras extends Page {
    /**
     *  商品编号,所属表字段为goods_extras.goods_id
     */
    private Long goodsId;

    /**
     *  商品重量，最小单位g,所属表字段为goods_extras.goods_weight
     */
    private Integer goodsWeight;

    /**
     *  1 免运费
0 要快递费,所属表字段为goods_extras.goods_xp_free
     */
    private Byte goodsXpFree;

    /**
     *  快递费用,所属表字段为goods_extras.goods_xp_price
     */
    private Integer goodsXpPrice;

    /**
     *  促销价格,所属表字段为goods_extras.goods_promote_price
     */
    private Integer goodsPromotePrice;

    /**
     *  创建时间,所属表字段为goods_extras.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为goods_extras.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常
1 删除,所属表字段为goods_extras.rstatus
     */
    private Byte rstatus;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getGoodsWeight() {
        return goodsWeight;
    }

    public void setGoodsWeight(Integer goodsWeight) {
        this.goodsWeight = goodsWeight;
    }

    public Byte getGoodsXpFree() {
        return goodsXpFree;
    }

    public void setGoodsXpFree(Byte goodsXpFree) {
        this.goodsXpFree = goodsXpFree;
    }

    public Integer getGoodsXpPrice() {
        return goodsXpPrice;
    }

    public void setGoodsXpPrice(Integer goodsXpPrice) {
        this.goodsXpPrice = goodsXpPrice;
    }

    public Integer getGoodsPromotePrice() {
        return goodsPromotePrice;
    }

    public void setGoodsPromotePrice(Integer goodsPromotePrice) {
        this.goodsPromotePrice = goodsPromotePrice;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}