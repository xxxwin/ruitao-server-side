import com.alibaba.fastjson.JSON;
import com.ruitaowang.admin.Application;
import com.ruitaowang.core.utils.RandomUtils;
import com.ruitaowang.core.utils.aliyun.OSSHelper;
import com.ruitaowang.dinner.domain.DcQueue;
import com.ruitaowang.dinner.domain.DcRestaurant;
import com.ruitaowang.dinner.domain.DcTableType;
import com.ruitaowang.dinner.service.DcQueueService;
import com.ruitaowang.dinner.service.DcRestanrantService;
import com.ruitaowang.goods.service.CompanyService;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.DigestUtils;
import org.springframework.util.ObjectUtils;

import java.net.URL;
import java.net.URLDecoder;
import java.time.Instant;
import java.util.*;

/**
 * Created by neal on 11/15/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTests {

    @Autowired
    private DcQueueService queueService;

    @Test
    public void hello() {

        System.out.println("Hello , Neal.");

        //获取随机字符串
        String nonceStr = RandomUtils.randomString(10);

        //获取当前时间戳
        Long timeStamp = Instant.now().getEpochSecond();

        //进行签名
        Map<String, Object> signatureMap = new HashMap<>();
/*
        "appId":"wx488ae3f57360b7ea",
                "timeStamp":"1481550914",
                "nonceStr":"6171783448",
                "package":"prepay_id=wx201612122153470fee836f090528755024",
                "signType":"MD5",
                "paySign":"0FC8FFA848BBCC5CD7A141E301044760"

  */
//        signatureMap.put("appId", "wx488ae3f57360b7ea");
//
//        signatureMap.put("timeStamp", timeStamp);
//
//        signatureMap.put("nonceStr", "6171783448");
//
//        signatureMap.put("package", "prepay_id=wx201612122153470fee836f090528755024");
//
//        signatureMap.put("signType", "MD5");
//
//        System.out.println(signatureMap);
//
//        System.out.println(Signature.getSign4PayForJSAPI(signatureMap));

        String s = "2";
        String[] a = s.split("L");
        System.out.println("a.length=" + a.length);

        List<String> l = null;
        System.out.println(ObjectUtils.isEmpty(l));

        Thread t1 = new Thread();

    }

    @Autowired
    private CompanyService companyService;

    @Test
//    @Rollback
    public void testMysql() {
        companyService.rollback();
//        companyService.norollback();
    }

    @Test
    public void testMyBatis() {
        queueService.buildDcQueueIndex((long) 188);
    }

    @Autowired
    DcQueueService dcQueueService;

    @Test
    public void testRedisCache() {
        DcQueue queue = dcQueueService.selectByPK(211L);
        dcQueueService.selectByPK(212L);
//        System.out.println(JSON.toJSON(queue));
//        queue.setNum(100);
//        dcQueueService.update(queue);
//        System.out.println(JSON.toJSON(queue));
//
//        dcQueueService.delete(211L);
//        DcQueue queue1 = dcQueueService.selectByPK(211L);
//        System.out.println(JSON.toJSON(queue1));
    }

    @Test
    public void testLambda() {
        List<DcTableType> tableTypes = new ArrayList<>();
        DcTableType tableType = new DcTableType();
        tableType.setNumMax(5);
        tableType.setId(5L);
        tableTypes.add(tableType);
        tableType = new DcTableType();
        tableType.setNumMax(2);
        tableType.setId(2L);
        tableTypes.add(tableType);
        tableType = new DcTableType();
        tableType.setNumMax(3);
        tableType.setId(3L);
        tableTypes.add(tableType);
//        List<DcTableType> tableTypesChecked = tableTypes.stream().filter(item->item.getNumMax()>diningNumber).collect(Collectors.toList());
        Collections.sort(tableTypes, (o1, o2) -> o1.getNumMax() - o2.getNumMax());
        tableTypes.forEach(item -> System.out.println(item.getNumMax()));
    }

    @Test
    public void testOss() {
        String url = "https://mmbiz.qpic.cn/mmbiz_png/rzDZtXB1s3ibdtlufGkgSC9Axs86lkGhpFnm8JQt7ufNGqd1S8MibK4hXic9zR7G7xSHkzichyib7WjEnO9K1Zta1iag/640?wx_fmt=png";
        try {
            String newUrl = OSSHelper.uploadImageToOSS(url, "demo");
            System.out.println(newUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testOssOk() {
        String imageUrl = "https://mmbiz.qpic.cn/mmbiz_png/rzDZtXB1s3ibdtlufGkgSC9Axs86lkGhpFnm8JQt7ufNGqd1S8MibK4hXic9zR7G7xSHkzichyib7WjEnO9K1Zta1iag/640?wx_fmt=png";
        try {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url(imageUrl).build();

            Response resp = client.newCall(request).execute();
            System.out.println(resp.code());
            String md5 = DigestUtils.md5DigestAsHex(resp.body().bytes());

            String newUrl = OSSHelper.uploadToOSSV2(resp.body().byteStream(), "demo");
            System.out.println(newUrl);
            System.out.println(md5);//1943320bd844545bb926d7c3ff37cfc6
//                                    1943320bd844545bb926d7c3ff37cfc6
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testQuery() throws Exception {
        String imageUrl = "https://mmbiz.qpic.cn/mmbiz_png/rzDZtXB1s3ibdtlufGkgSC9Axs86lkGhpFnm8JQt7ufNGqd1S8MibK4hXic9zR7G7xSHkzichyib7WjEnO9K1Zta1iag/640?wx_fmt=png";
        System.out.println(splitQuery(imageUrl).get("wx_fmt"));
    }

    public static Map<String, String> splitQuery(String imageUrl) throws Exception {
        URL url = new URL(imageUrl);
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    @Autowired
    DcRestanrantService restanrantService;

    @Test
    public void testRestaurant() throws Exception {
        DcRestaurant r = new DcRestaurant();
        r.setCompanyId(189L);
        System.out.println(JSON.toJSON(restanrantService.select(r)));

    }

    @Test
    public void testRestaurantInsert() throws Exception {
        DcRestaurant restaurantWithBLOBs = new DcRestaurant();
        restaurantWithBLOBs.setCompanyId(189L);
        restaurantWithBLOBs.setBrandStory("story");
//        restaurantWithBLOBs.setAboutUs("");
//        restaurantWithBLOBs.setVentureHistory("");
//        restaurantWithBLOBs.setCustomersFeedback("");
//        restaurantWithBLOBs.setDinnersCulture("");
//        restaurantWithBLOBs.setFoodPhilosophy("");
//        restaurantWithBLOBs.setShopSpecialty("");
        restanrantService.insert(restaurantWithBLOBs);
        System.out.println(JSON.toJSON(restaurantWithBLOBs));

    }
    @Test
    public static String getNextYearCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        Date date = new Date(System.currentTimeMillis());
        calendar.setTime(date);
        //下一周的今天
//        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        calendar.add(Calendar.YEAR, -1);
        date = calendar.getTime();
        return date.toString();
    }
}
