package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Comment;
import java.util.List;

public interface CommentMapper {
    int insert(Comment comment);

    int deleteByPK(Long id);

    int updateByPK(Comment comment);

    Comment selectByPK(Long id);

    long count(Comment comment);

    List<Comment> select(Comment comment);
}