package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Activity;
import com.ruitaowang.core.domain.ActivityVo;
import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.ActivityAdMapper;
import com.ruitaowang.goods.dao.ActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class ActivityService implements BaseService<Activity> {
    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private ActivityAdMapper activityAdMapper;

    @Override
    public Activity insert(Activity activity) {
        Long time = System.currentTimeMillis();
        activity.setCtime(time);
        activity.setMtime(time);
        activityMapper.insert(activity);
        return activity;
    }

    @Override
    public int delete(Long id) {
        Activity activity = new Activity();
        activity.setActivityId(id);
        activity.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        activity.setMtime(time);
        return activityMapper.updateByPK(activity);
    }

    @Override
    public Activity update(Activity activity) {
        Long time = System.currentTimeMillis();
        activity.setMtime(time);
        activityMapper.updateByPK(activity);
        return activity;
    }

    @Override
    public Activity selectByPK(Long id) {

        return activityMapper.selectByPK(id);
    }

    @Override
    public List<Activity> select(Activity activity) {
        activity.setRstatus((byte) 0);
        return activityMapper.select(activity);
    }
    //查询微传媒使用记录
    public long selectNoRstatus(Activity activity) {
        return activityAdMapper.selectNoRstatus(activity);
    }
    public Long countUsersWCM(Long stime,Long etime){
        return activityAdMapper.countUsersWCM(stime,etime);
    }
    public List<Activity> selectForPage(Activity activity) {
        activity.setRstatus((byte) 0);
        activity.setRecords(activityMapper.count(activity));
        return activityMapper.select(activity);
    }

    public List<Activity> selectForNextAndPre(Activity activity) {
        List<Activity> activities = new ArrayList<>(3);
        Long id = activity.getActivityId();
        Long preId = activityAdMapper.selectPreId(id, activity.getActivityType());
        Long nextId = activityAdMapper.selectNextId(id, activity.getActivityType());
        if (ObjectUtils.isEmpty(preId)){
            preId = 0L;
        }
        if (ObjectUtils.isEmpty(nextId)){
            nextId = 0L;
        }
        activity = activityAdMapper.selectByPK(preId);
        activities.add(activity);
        activity = activityAdMapper.selectByPK(id);
        activities.add(activity);
        activity = activityAdMapper.selectByPK(nextId);
        activities.add(activity);
        return activities;
    }

    public Long count(Activity activity){
        activity.setRstatus((byte) 0);
        return activityMapper.count(activity);
    }

    public List<Activity> selectDetail(ActivityVo activityVo){
        return activityAdMapper.selectDetail(activityVo);
    }
}

