package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.UserMembers;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.UserMembersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class PageUserMemberController extends BaseController {

    @Autowired
    private UserMembersService userMembersService;


    @RequestMapping(value = "/admin_user_member_update/{memberId}", method = RequestMethod.GET)
    public ModelAndView updateMembers(@PathVariable("memberId") Long memberId, Model model){
        UserMembers userMembers=userMembersService.selectByPK(memberId);
        model.addAttribute("userMembers", userMembers);
        return new ModelAndView("web/admin_user_member_update");
    }
}