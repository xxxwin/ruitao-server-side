package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.UserLock;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.UserLockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by xinchunting on 17-11-2.
 */
@RestController
public class APIUserLockController extends BaseController {

    @Autowired
    private UserLockService userLockService;

    @RequestMapping("/api/admin/userLock/get")
    public String findCommunicationPage(UserLock userLock) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(userLockService.select(userLock));
        return this.renderJson(o);
    }
}
