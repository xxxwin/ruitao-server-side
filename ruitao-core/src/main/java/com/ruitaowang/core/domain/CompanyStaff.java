package com.ruitaowang.core.domain;

import com.ruitaowang.core.domain.Page;

/**
 * company_staff《》
 *
 * @author yuri.cxy
 *
 */
public class CompanyStaff extends Page {

	private Long id;
	private Long companyId; // 公司id
	private Long userId; // 职员的用户id
	private Integer status; // 0 停用 1 正常 2 离职
	private String phone;
	private String headImg;
	private String userName;
	private String realName;
	/**
	 *  创建时间,所属表字段为company_staff.ctime
	 */
	private Long ctime;

	/**
	 *  最后更新时间,所属表字段为company_staff.mtime
	 */
	private Long mtime;

	/**
	 *  状态 0:正常 1:删除,所属表字段为company_staff.rstatus
	 */
	private Byte rstatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCtime() {
		return ctime;
	}

	public void setCtime(Long ctime) {
		this.ctime = ctime;
	}

	public Long getMtime() {
		return mtime;
	}

	public void setMtime(Long mtime) {
		this.mtime = mtime;
	}

	public Byte getRstatus() {
		return rstatus;
	}

	public void setRstatus(Byte rstatus) {
		this.rstatus = rstatus;
	}

	/**
	 * 实例化一个""实体模型对象
	 */
	public CompanyStaff() {
		super();
	}

	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CompanyStaff [").append("companyId=").append(this.getCompanyId()).append(",userId=")
				.append(this.getUserId()).append(",status=").append(this.getStatus()).append("]");
		return builder.toString();
	}
}