var config = {
    updateUrl: '/api/admin/type/update',
    selectIndustryUrl:'/api/admin/industry/selectAll',
    // updateUrl: '/api/activities',
    // deleteUrl: '/api/activities',
    // selectUrl: '/api/activities'
};
//表单验证
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
    var industryId=$("#industry_id").val();
    //给行业类型注入值
    $.ajax(
        {
            url: config.selectIndustryUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                data.data.map(function (industry) {
                    if(industryId==industry.id){
                        categories.push('<option value="');
                        categories.push(industry.id);
                        categories.push('" ');
                        categories.push('hassubinfo="true" selected="selected">');
                        categories.push(industry.name);
                        categories.push('</option>');
                    }else {
                        categories.push('<option value="');
                        categories.push(industry.id);
                        categories.push('" ');
                        categories.push('hassubinfo="true">');
                        categories.push(industry.name);
                        categories.push('</option>');
                    }
                });
                $("#industrySelect").html(categories.join(''));

            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    // $('#activityType').val(activityTypeChecked);
    $('#submitTypeUpdate').click(function () {
        $("#ajaxform").validate();
        //如果验证通过
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            console.log(postData);
            $.ajax(
                {
                    url: config.updateUrl,
                    type: "PUT",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});

$('#buttonGoback').click(function() {
    history.go(-1);
    location.reload();
    return false;
});