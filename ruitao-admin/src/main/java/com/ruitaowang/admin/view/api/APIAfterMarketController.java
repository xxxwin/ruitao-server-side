package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.AfterMarketService;
import com.ruitaowang.goods.service.OrderService;
import com.ruitaowang.goods.service.ShoppingOrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APIAfterMarketController extends BaseController {

    @Autowired
    private AfterMarketService aftermarketService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ShoppingOrderService shoppingOrderService;


    /**
     * @api {get} /api/admin/aftermarkets 退货提醒-退货列表
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/aftermarkets
     *
     * @apiName findAftermarketsPage
     * @apiGroup Aftermarkets
     *
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiParam {int} activityType 活动类型.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "imageId1":0,
     *                   "imageId2":0,
     *                   "imageId3":0,
     *                   "logisticId":0,
     *                   "logisticName":"",
     *                   "logisticSn":"0",
     *                   "mtime":1502870415259,
     *                   "orderId":224,
     *                   "prodId":227,
     *                   "remark":"",
     *                   "returnCause":"……",
     *                   "returnType":2,
     *                   "rstatus":0,
     *                   "userId":6
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/aftermarkets"
     *  }
     */
    @RequestMapping("/api/admin/aftermarkets")
    public String findAftermarketsPage(AfterMarket aftermarket, int page) {
        aftermarket.setPage(page);
        MsgOut o = MsgOut.success(aftermarketService.selectForPage(aftermarket));
        o.setRecords(aftermarket.getRecords());
        o.setTotal(aftermarket.getTotal());
        return this.renderJson(o);
    }
    @RequestMapping("/api/wap/aftermarkets")
    public String wapFindAftermarkets(AfterMarket aftermarket, int page) {
        aftermarket.setPage(page);
        MsgOut o = MsgOut.success(aftermarketService.selectForPage(aftermarket));
        o.setRecords(aftermarket.getRecords());
        o.setTotal(aftermarket.getTotal());
        return this.renderJson(o);
    }

    /**
     * @api {post} /api/wap/aftermarkets 退货提醒-退货提交
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/aftermarkets
     *
     * @apiName createAfterMarket
     * @apiGroup Aftermarkets
     *
     * @apiParam {int} orderId 订单ID.
     * @apiParam {int} prodId 订单流水ID.
     * @apiParam {int} returnType 退货类型.
     * @apiParam {int} imageId1 退货图片.
     * @apiParam {int} imageId2 退货图片.
     * @apiParam {int} imageId3 退货图片.
     * @apiParam {int} userId 退货人ID.
     * @apiParam {string} returnCause 退货原因.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "imageId1":"退货图片1",
     *                   "imageId2":"退货图片2",
     *                   "imageId3":"退货图片3",
     *                   "logisticId":"物流ID",
     *                   "logisticName":"物流公司名称",
     *                   "logisticSn":"物流单号",
     *                   "mtime":1502870415259,
     *                   "orderId":订单ID,
     *                   "prodId":227,
     *                   "remark":"退货备注",
     *                   "returnCause":"退货原因",
     *                   "returnType":"售后类型： 0 退货退款， 1  退款， 2 换货",
     *                   "rstatus":0,
     *                   "userId":"用户ID"
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/aftermarkets"
     *  }
     */

    @RequestMapping(value = "/api/wap/aftermarkets", method = RequestMethod.POST)
    public String createAfterMarket(@RequestHeader(value="headId",required = false) String userId,AfterMarket aftermarket) {
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(aftermarket.getOrderId())) {
            return this.renderJson(MsgOut.error("no orderId"));
        }
        Order order = new Order();
        order.setPayStatus(((byte)3));
        order.setOrderId(aftermarket.getOrderId());
        if (aftermarket.getReturnType() == 0){
            order.setOrderStatus((byte)3);
        }else if (aftermarket.getReturnType() == 1){
            order.setOrderStatus((byte)4);
        }
        orderService.update(order);
        //更新ShoppingOrder
        ShoppingOrder shoppingOrder = new ShoppingOrder();
        shoppingOrder.setOrderId(order.getOrderId());
        List<ShoppingOrder> shoppingOrders=shoppingOrderService.select(shoppingOrder);
        for (ShoppingOrder so:shoppingOrders){
            if (aftermarket.getReturnType() == 0){
                so.setOrderStatus((byte)3);
            }else if (aftermarket.getReturnType() == 1){
                so.setOrderStatus((byte)4);
            }
            so.setPayStatus(((byte)3));
            shoppingOrderService.update(so);
        }
        aftermarket.setUserId(Long.parseLong(userId));
        aftermarketService.insert(aftermarket);
        MsgOut o = MsgOut.success(aftermarket);
        return this.renderJson(o);
    }

    /**
     * @api {put} /api/admin/aftermarkets 退货提醒-退货订单修改
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/aftermarkets
     *
     * @apiName updateAfterMarket
     * @apiGroup Aftermarkets
     *
     * @apiParam {int} orderId 订单ID.
     * @apiParam {int} prodId 订单流水ID.
     * @apiParam {int} returnType 退货类型.
     * @apiParam {int} imageId1 退货图片.
     * @apiParam {int} imageId2 退货图片.
     * @apiParam {int} imageId3 退货图片.
     * @apiParam {int} userId 退货人ID.
     * @apiParam {string} returnCause 退货原因.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "imageId1":0,
     *                   "imageId2":0,
     *                   "imageId3":0,
     *                   "logisticId":0,
     *                   "logisticName":"",
     *                   "logisticSn":"0",
     *                   "mtime":1502870415259,
     *                   "orderId":224,
     *                   "prodId":227,
     *                   "remark":"",
     *                   "returnCause":"……",
     *                   "returnType":2,
     *                   "rstatus":0,
     *                   "userId":6
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/aftermarkets"
     *  }
     */

    @RequestMapping(value = "/api/admin/aftermarkets", method = RequestMethod.PUT)
    public String updateAfterMarket(AfterMarket aftermarket) {
        aftermarketService.update(aftermarket);
        MsgOut o = MsgOut.success(aftermarket);
        return this.renderJson(o);
    }

    /**
     * @api {post} /api/admin/aftermarkets 退货提醒-退货订单修改-cellEdit
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/aftermarkets
     *
     * @apiName updateAfterMarket
     * @apiGroup Aftermarkets
     *
     * @apiParam {int} orderId 订单ID.
     * @apiParam {int} prodId 订单流水ID.
     * @apiParam {int} returnType 退货类型.
     * @apiParam {int} imageId1 退货图片.
     * @apiParam {int} imageId2 退货图片.
     * @apiParam {int} imageId3 退货图片.
     * @apiParam {int} userId 退货人ID.
     * @apiParam {string} returnCause 退货原因.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/aftermarkets"
     *  }
     */

    @RequestMapping(value = "/api/admin/aftermarkets/cellEdit", method = RequestMethod.POST)
    public String updateAfterMarketForCellEdit( AfterMarket aftermarket,long id){
        aftermarket.setId(id);
        aftermarketService.update(aftermarket);
        MsgOut o = MsgOut.success(aftermarket);
        return this.renderJson(o);
    }
    /**
     * @api {delete} /api/admin/aftermarkets 退货提醒-退货订单修改
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/aftermarkets
     *
     * @apiName deleteAfterMarket
     * @apiGroup Aftermarkets
     *
     * @apiParam {int} id 退货订单Id.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":1,
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/aftermarkets"
     *  }
     */
    @RequestMapping(value = "/api/aftermarkets/{aftermarketId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("aftermarketId") Long aftermarketId) {

        MsgOut o = MsgOut.success(aftermarketService.delete(aftermarketId));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/aftermarkets/{aftermarketId} 退货提醒-查询订单ByPK
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/aftermarkets/{aftermarketId}
     *
     * @apiName findAfterMarketByPK
     * @apiGroup Aftermarkets
     *
     * @apiParam {int} orderId 订单ID.
     * @apiParam {int} prodId 订单流水ID.
     * @apiParam {int} returnType 退货类型.
     * @apiParam {int} imageId1 退货图片.
     * @apiParam {int} imageId2 退货图片.
     * @apiParam {int} imageId3 退货图片.
     * @apiParam {int} userId 退货人ID.
     * @apiParam {string} returnCause 退货原因.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/aftermarkets/{aftermarketId}"
     *  }
     */
    @RequestMapping(value = "/api/admin/aftermarkets/{aftermarketId}", method = RequestMethod.GET)
    public String findAfterMarketByPK(@PathVariable("aftermarketId")Long  aftermarketId) {
        MsgOut o = MsgOut.success(aftermarketService.selectByPK(aftermarketId));
        return this.renderJson(o);
    }
//    //'' 无退货订单
//    //0  进行中
//    //1  退款结束 已经转账
//    @RequestMapping(value = "/api/wap/aftermarkets/{aftermarketId}", method = RequestMethod.GET)
//    public String findAfterMarketWap(@PathVariable("aftermarketId")Long  aftermarketId) {
//        MsgOut o = MsgOut.success(aftermarketService.selectByPK(aftermarketId));
//        return this.renderJson(o);
//    }
}
