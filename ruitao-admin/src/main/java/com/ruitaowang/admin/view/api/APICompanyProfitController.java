package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyProfitService;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.UserMembersService;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/5/15.
 */
@RestController
public class APICompanyProfitController extends BaseController {
    @Autowired
    private CompanyProfitService companyProfitService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private CompanyService companyService;

    /**
     * 商家中心  商家流水
     * @param companyProfit
     * @param page
     * @return
     */
    @RequestMapping("/api/companyProfit/get")
    public String apiFindCompanyProfitPage(CompanyProfit companyProfit, int page) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        companyProfit.setPage(page);
        if(!ObjectUtils.isEmpty(companyProfit.getSidx())){
            companyProfit.setOrderBy(companyProfit.getSidx() + " " + companyProfit.getSord() + "," + companyProfit.getOrderBy());
        }
        List<CompanyProfit> list = companyProfitService.selectForPage(companyProfit);
        for (CompanyProfit where:list){
            where.setRemark(sysUserService.selectByPK(where.getConsumerId()).getNickname());
        }
        MsgOut o = MsgOut.success(list);
        o.setRecords(companyProfit.getRecords());
        o.setTotal(companyProfit.getTotal());
        return this.renderJson(o);
    }
    @RequestMapping("/api/wap/companyProfit/get/{companyId}")
    public String apiFindCompanyProfitByPK(@PathVariable("companyId") Long companyId) {
        if (ObjectUtils.isEmpty(companyId)) {
            return this.renderJson(MsgOut.error("参数错误."));
        }
        CompanyProfit companyProfit=new CompanyProfit();
        companyProfit.setCompanyId(companyId);
        MsgOut o = MsgOut.success(companyProfitService.select(companyProfit));
        return this.renderJson(o);
    }
    @RequestMapping("/api/admin/companyProfit/get")
    public String FindCompanyProfitPage(CompanyProfit companyProfit, int page) {
        companyProfit.setPage(page);
        if(!ObjectUtils.isEmpty(companyProfit.getSidx())){
            companyProfit.setOrderBy(companyProfit.getSidx() + " " + companyProfit.getSord() + "," + companyProfit.getOrderBy());
        }
        List<CompanyProfitVO> companyProfitVOS=new ArrayList<>();
        List<CompanyProfit> companyProfitList = companyProfitService.selectForPage(companyProfit);
        for (CompanyProfit cp:companyProfitList){
            CompanyProfitVO companyProfitVO = new CompanyProfitVO();
            BeanUtils.copyProperties(cp,companyProfitVO);
            companyProfitVO.setCompanyName(companyService.selectByPK(cp.getCompanyId()).getCompanyName());
            companyProfitVO.setNickName(sysUserService.selectByPK(cp.getConsumerId()).getNickname());
            companyProfitVOS.add(companyProfitVO);
        }
        MsgOut o = MsgOut.success(companyProfitVOS);
        o.setRecords(companyProfit.getRecords());
        o.setTotal(companyProfit.getTotal());
        return this.renderJson(o);
    }
    @RequestMapping(value = {"/api/admin/companyProfit/excel/{profitId}"})
    public void adminOrderExcel(@PathVariable("profitId") Long profitId) {
        List<CompanyProfit> out =new ArrayList<>();
        CompanyProfit companyProfit=companyProfitService.selectByPK(profitId);
        companyProfit.setRemark(sysUserService.selectByPK(companyProfit.getConsumerId()).getNickname());
        out.add(companyProfit);
        HttpServletResponse response = this.getResponse();
        //设置response头信息
        response.reset();
        response.setContentType("application/vnd.ms-excel");        //改成输出excel文件
        String fileName = "profit";
        response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");

        //创建工作簿并发送到浏览器
        try {
            //创键Excel表格中的第一个表,命名为"第一页"
            WritableWorkbook book = Workbook.createWorkbook(response.getOutputStream());
//第一sheet
            WritableSheet sheet = book.createSheet("订单详情", 0);
            //设置该单元格的表头
            List<String> keys = new ArrayList<>();
            keys.add("编号");
            keys.add("订单ID");
            keys.add("订单SN");
            keys.add("订单类型");
            keys.add("商户ID");
            keys.add("商户名称");
            keys.add("总金额");
            keys.add("收益金额");
            keys.add("余额");
            keys.add("时间");
            // 添加标题
            for (int x = 0; x < keys.size(); x++) {
                sheet.addCell((WritableCell) new Label(x, 0, keys.get(x)));
            }
            // 添加数据
            for (int y = 0; y < out.size(); y++) {
                sheet.addCell(new Label(0, y + 1, out.get(y).getProfitId().toString()));
                sheet.addCell(new Label(1, y + 1, out.get(y).getOrderId().toString()));
                sheet.addCell(new Label(2, y + 1, out.get(y).getOrderSn()));
                sheet.addCell(new Label(3, y + 1, out.get(y).getOrderType().toString()));
                sheet.addCell(new Label(4, y + 1, out.get(y).getCompanyId().toString()));
                sheet.addCell(new Label(5, y + 1, out.get(y).getRemark()));
                sheet.addCell(new Label(6, y + 1, out.get(y).getAmount().toString()));
                sheet.addCell(new Label(7, y + 1, out.get(y).getCompanyProfit().toString()));
                sheet.addCell(new Label(8, y + 1, out.get(y).getCompanyBalance().toString()));
                sheet.addCell(new Label(9, y + 1, out.get(y).getCtime().toString()));
            }
            // 写入数据并关闭文件
            book.write();
            book.close();
        } catch (Exception e) {

        }
    }
}
