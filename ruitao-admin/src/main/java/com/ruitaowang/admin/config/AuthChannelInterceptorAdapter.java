//package com.ruitaowang.admin.config;
//
//import com.ruitaowang.core.utils.RandomUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.messaging.Message;
//import org.springframework.messaging.MessageChannel;
//import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
//import org.springframework.messaging.simp.stomp.StompCommand;
//import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
//import org.springframework.messaging.support.ChannelInterceptorAdapter;
//import org.springframework.messaging.support.MessageHeaderAccessor;
//import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * Created by neal on 17/10/2017.
// */
//public class AuthChannelInterceptorAdapter extends ChannelInterceptorAdapter {
//    private static final Logger LOGGER = LoggerFactory.getLogger(AuthChannelInterceptorAdapter.class);
//    private static final String USERNAME_HEADER = "login";
//    private static final String PASSWORD_HEADER = "companyId";
//
//    @Override
//    public Message<?> preSend(final Message<?> message, final MessageChannel channel) throws AuthenticationException {
//        final StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
//        LOGGER.debug("preSend");
//        LOGGER.debug("companyId: {}", accessor.getFirstNativeHeader(PASSWORD_HEADER));
//        LOGGER.debug("message headers: {}", accessor.getMessageHeaders());
//        if (StompCommand.CONNECT == accessor.getCommand()) {
//              String login = accessor.getFirstNativeHeader(USERNAME_HEADER);
//              String passcode = accessor.getFirstNativeHeader(PASSWORD_HEADER);
//            try {
//                Long userId = RandomUtils.getUserId(login);
//                accessor.setUser(new UsernamePasswordAuthenticationToken(
//                        String.valueOf(userId+"-"+passcode),
//                        null,
//                        Collections.singleton((GrantedAuthority) () -> "WEB-SOCKET-USER")));
//            } catch (Exception e) {
//                accessor.setUser(new UsernamePasswordAuthenticationToken(
//                        "anonymousUser",
//                        null,
//                        Collections.singleton((GrantedAuthority) () -> "WEB-SOCKET-USER")));
//            }
//        } else {
//            LOGGER.debug("SessionAttributes : {}", accessor.getSessionAttributes());
//        }
//        return message;
//    }
//}