/**
 * Created by Administrator on 2017/4/18.
 */
/**
 * Created by neal on 11/22/16.
 */
var CONST_GLOBAL_COMPANY_ID;
var Config = {
    createUrl: '/api/dc/create/company',
    selectDTUrl: '/api/districts/page',
    selectCompanyUrl:'/api/dc/list/company'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$().ready(function () {
    $.ajax(
        {
            url: Config.selectCompanyUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var companys = [];
                companys.push('<option value="" hassubinfo="true">全部门店</option>');
                data.data.map(function (company) {
                    companys.push('<option value="');
                    companys.push(company.companyId);
                    companys.push('" ');
                    companys.push('hassubinfo="true">');
                    companys.push(company.companyName);
                    companys.push('</option>');
                });
                $("#pid").html(companys.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    getDistrictByParentId(0, "#province");
    getDistrictByParentId(1, "#city");
    $("#province").change(function(){
        var id = $(this).val();
        $("input[name=districtId]").val(id);
        $.ajax({
            url: Config.selectDTUrl,
            data: {parentid:id , page: 1, rows: 1000},
            type: "GET",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {


                getDistrictByParentId(id, "#city");
                // refreshGrid(data.data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
    });
    $("#city").change(function(){
        var id = $(this).val();
        $("input[name=districtId]").val(id);
        $.ajax({
            url: Config.selectDTUrl,
            data: {parentid:id , page: 1, rows: 1000},
            type: "GET",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                getDistrictByParentId(id, "#district");
                // refreshGrid(data.data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
    });
    $("#district").change(function(){
        var id = $(this).val();
        $("input[name=districtId]").val(id);
    });
    function getDistrictByParentId(id, selectId) {
        console.log("1");
        $.ajax({
            url: Config.selectDTUrl,
            data: {parentid:id, page: 1, rows: 1000},
            type: "GET",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                var html = [];
                data.data.forEach(function(item){
                    html.push('<option value="'+item.id+'">'+item.name+'</option>');
                });
                $(selectId).html(html.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
    }
    $('#buttonGoback').click(function () {
        history.go(-1);
        location.reload();
        return false;
    });
});
$('#submitCreate').click(function () {
    $("#companyForm").validate();
    if ($('#companyForm').valid()) {
        var postData = $('#companyForm').serializeJson();
        postData.phone = $("input[id=phone]").val();
        postData.pid = $('#pid option:selected').val();
        postData.province = $('#province option:selected').text();
        postData.city = $('#city option:selected').text();
        if ($('#district option:selected').text() != "" || $('#district option:selected').text() != null){
            postData.district = $('#district option:selected').text();
        }
        postData.districtId=$("input[name=districtId]").val();
        $.ajax({
            url: Config.createUrl,
            type: "POST",
            data: postData,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (data.type ==="ERROR"){
                    toastr.success(data.msg, data.title);
                }else {
                    CONST_GLOBAL_COMPANY_ID = data.data.companyId;
                    toastr.success(data.msg, data.title);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    }

    return false;
});





