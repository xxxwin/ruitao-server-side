package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsAlbum;
import java.util.List;

public interface GoodsAlbumMapper {
    int insert(GoodsAlbum goodsAlbum);

    int deleteByPK(Long imageId);

    int updateByPK(GoodsAlbum goodsAlbum);

    GoodsAlbum selectByPK(Long imageId);

    long count(GoodsAlbum goodsAlbum);

    List<GoodsAlbum> select(GoodsAlbum goodsAlbum);
}