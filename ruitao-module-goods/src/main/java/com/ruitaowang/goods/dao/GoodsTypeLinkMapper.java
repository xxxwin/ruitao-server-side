package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsTypeLink;
import java.util.List;

public interface GoodsTypeLinkMapper {
    int insert(GoodsTypeLink goodsTypeLink);

    int deleteByPK(Long id);

    int updateByPK(GoodsTypeLink goodsTypeLink);

    GoodsTypeLink selectByPK(Long id);

    long count(GoodsTypeLink goodsTypeLink);

    List<GoodsTypeLink> select(GoodsTypeLink goodsTypeLink);
}