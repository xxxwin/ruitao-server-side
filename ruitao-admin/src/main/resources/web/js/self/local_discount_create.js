/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl:'/api/localDiscount'
};
$().ready(function () {
    $('#submitCreate').click( function() {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()){
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.createUrl,
                    type: "POST",
                    data : postData,
                    dataType: "json",
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});
