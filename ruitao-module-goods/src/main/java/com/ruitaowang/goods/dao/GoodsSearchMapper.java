package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Goods;

import java.util.List;

public interface GoodsSearchMapper {

    long count(Goods goods);

    List<Goods> select(Goods goods);
}