package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.MeetingUserLink;
import com.ruitaowang.core.domain.Meetings;
import com.ruitaowang.core.domain.MeetingUserLink;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.MeetingUserLinkMapper;
import com.ruitaowang.goods.dao.MeetingsMapper;
import com.ruitaowang.goods.dao.MeetingUserLinkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class MeetingService implements BaseService<Meetings> {
    @Autowired
    private MeetingsMapper meetingMapper;
    @Autowired
    private MeetingUserLinkMapper meetingUserLinkMapper;

    @Override
    public Meetings insert(Meetings meeting) {
        Long time = System.currentTimeMillis();
        meeting.setCtime(time);
        meeting.setMtime(time);
        meetingMapper.insert(meeting);
        return meeting;
    }

    @Override
    public int delete(Long id) {
        Meetings meeting = new Meetings();
        meeting.setMeetingId(id);
        meeting.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        meeting.setMtime(time);
        return meetingMapper.updateByPK(meeting);
    }

    @Override
    public Meetings update(Meetings meeting) {
        Long time = System.currentTimeMillis();
        meeting.setMtime(time);
        meetingMapper.updateByPK(meeting);
        return meeting;
    }

    @Override
    public Meetings selectByPK(Long id) {
        return meetingMapper.selectByPK(id);
    }

    @Override
    public List<Meetings> select(Meetings meeting) {
        meeting.setRstatus((byte) 0);
        return meetingMapper.select(meeting);
    }

    public List<Meetings> selectForPage(Meetings meeting) {
        meeting.setRstatus((byte) 0);
        meeting.setRecords(meetingMapper.count(meeting));
        return meetingMapper.select(meeting);
    }

    public MeetingUserLink insertOrUpdateMeetingForSignUp(MeetingUserLink userMeetingsLink) {//报名
        if(ObjectUtils.isEmpty(userMeetingsLink)){
            return null;
        }
        MeetingUserLink where = new MeetingUserLink();
        where.setPhone(userMeetingsLink.getPhone());
        where.setMeetingId(userMeetingsLink.getMeetingId());

        List<MeetingUserLink> temp = meetingUserLinkMapper.select(where);
        Long time = System.currentTimeMillis();
        if(ObjectUtils.isEmpty(temp)){
            userMeetingsLink.setCtime(time);
            userMeetingsLink.setMtime(time);
            meetingUserLinkMapper.insert(userMeetingsLink);
            userMeetingsLink.setRstatus((byte) 0);
            return userMeetingsLink;
        }
        temp.get(0).setRstatus((byte) -1);
        return temp.get(0);
    }

    public MeetingUserLink meetingForSignIn(MeetingUserLink userMeetingsLink) {//签到
        Long time = System.currentTimeMillis();
        userMeetingsLink.setCtime(time);
        userMeetingsLink.setMtime(time);
        List<MeetingUserLink> userMeetingsLink1 = meetingUserLinkMapper.select(userMeetingsLink);
        if(ObjectUtils.isEmpty(userMeetingsLink1)){
            return userMeetingsLink;
        }
        userMeetingsLink = userMeetingsLink1.get(0);
        userMeetingsLink.setRstatus((byte) 1);
        meetingUserLinkMapper.updateByPK(userMeetingsLink);
        return userMeetingsLink;
    }

    public boolean checkSignUp(MeetingUserLink userMeetingsLink) {//判断是否报名
        if(!ObjectUtils.isEmpty(userMeetingsLink) && !ObjectUtils.isEmpty(meetingUserLinkMapper.select(userMeetingsLink))){
            return true;
        }
        return false;
    }

    /**
     * 我的会议
     */
    public List<MeetingUserLink> selectMyMeetingLinkForPage(MeetingUserLink meeting) {

        meeting.setRecords(meetingUserLinkMapper.count(meeting));
        return meetingUserLinkMapper.select(meeting);
    }

    public List<Meetings> selectMyMeetingsForPage(Meetings meeting, String phone) {
        MeetingUserLink meetingLink = new MeetingUserLink();
        meetingLink.setPage(meeting.getPage());
        meetingLink.setPhone(phone);
        meetingLink.setRows(meeting.getRows());
        List<MeetingUserLink> meetingUserLinks = selectMyMeetingLinkForPage(meetingLink);
        List<Meetings> meetingses = new ArrayList<>();
        if(!ObjectUtils.isEmpty(meetingUserLinks)){
            meetingUserLinks.forEach(item -> {
                Meetings meetings = meetingMapper.selectByPK(item.getMeetingId());
                if(!ObjectUtils.isEmpty(meetings)){
                    meetingses.add(meetings);
                }
            });
        }
        meeting.setRecords(meetingLink.getRecords());
        meeting.setTotal(meetingLink.getTotal());
        meeting.setResult(meetingses);
        return meetingses;
    }
}
