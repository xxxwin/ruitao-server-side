package com.ruitaowang.account.service;

import com.ruitaowang.core.domain.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by neal on 07/01/2017.
 */
@Component
public class CacheService {
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //@Cacheable(value = "reportcache",keyGenerator = "myKeyGenerator")
    public String get() {
        //保存字符串
        stringRedisTemplate.opsForValue().set("aaa", "111");
        LOGGER.info("aaa {}", stringRedisTemplate.opsForValue().get("aaa"));

        return stringRedisTemplate.opsForValue().get("aaa");
    }
}
