package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsAttribute;
import java.util.List;

public interface GoodsAttributeMapper {
    int insert(GoodsAttribute goodsAttribute);

    int deleteByPK(Long attrId);

    int updateByPK(GoodsAttribute goodsAttribute);

    GoodsAttribute selectByPK(Long attrId);

    long count(GoodsAttribute goodsAttribute);

    List<GoodsAttribute> select(GoodsAttribute goodsAttribute);
}