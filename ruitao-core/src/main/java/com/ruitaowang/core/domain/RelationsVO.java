package com.ruitaowang.core.domain;

public class RelationsVO extends Relations {

    private Long count;
    private Long appreciate;
    private String headImgUrl;
    private String nickName;
    private Long id;
    private Byte type;
    private Byte appreciateStatus;
    private String ranking;
    private Byte levelType;

    public Byte getLevelType() {
        return levelType;
    }

    public void setLevelType(Byte levelType) {
        this.levelType = levelType;
    }

    public String getRanking() {
        return ranking;
    }

    public void setRanking(String ranking) {
        this.ranking = ranking;
    }

    public Byte getAppreciateStatus() {
        return appreciateStatus;
    }

    public void setAppreciateStatus(Byte appreciateStatus) {
        this.appreciateStatus = appreciateStatus;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getAppreciate() {
        return appreciate;
    }

    public void setAppreciate(Long appreciate) {
        this.appreciate = appreciate;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

}
