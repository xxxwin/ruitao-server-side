#!/usr/bin/env bash
ssh -t -t root@test.ruitaowang.com << EOF
cd /server/source/ruitao-server-side
echo `pwd`
/usr/bin/git checkout master
/usr/bin/git pull origin master
apidoc -i /server/source/ruitao-server-side/ruitao-admin/src/main/java/com/ruitaowang/admin/view/api -o /root/shihong/ruitao/ -c /server/source/ruitao-server-side/bootstrap/
tail -f /opt/admin/logs/catalina.out
#/opt/tomcat/logs/catalina.out
exit
EOF