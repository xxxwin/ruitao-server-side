package com.ruitaowang.admin.view.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.utils.RedisUtil;
import com.ruitaowang.goods.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.utils.PriceUtile;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APIOrderController extends BaseController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderProdService orderProdService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserProfitService userProfitService;
    @Autowired
    private LogisticsService logisticsService;
    @Autowired
    private OrderAdService orderAdService;
    @Autowired
    private AfterMarketService afterMarketService;
    @Autowired
    private ShoppingOrderService shoppingOrderService;
    @Autowired
    private RedisUtil<String> redisUtil;
    @Value("${host.api}")
    private String host;

    @RequestMapping(value = "/api/orders", method = RequestMethod.GET)
    public String list4admin(Order order, int page) {
        order.setPage(page);
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(order.getSidx())) {
            order.setOrderBy(order.getSidx() + " " + order.getSord() + "," + order.getOrderBy());
        }
        MsgOut o = MsgOut.success(orderService.searchForPage(order));
        o.setRecords(order.getRecords());
        o.setTotal(order.getTotal());
        return this.renderJson(o);
    }


    /**
     * @api {get} /api/admin/orders 后台管理-订单管理-商城订单列表
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/orders
     * @apiName findOrderPage
     * @apiGroup Order
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "amountMoney": 20000,
     * "amountScore": 0,
     * "auditStatus": 1,
     * "auditUserId": 0,
     * "consignee": "钱多多",
     * "ctime": 1503327958120,
     * "exPrice": 0,
     * "exSn": "",
     * "exText": "",
     * "exWay": 0,
     * "goodsPayType": 0,
     * "memberPrice": 0,
     * "mobile": "13637151955",
     * "mtime": 1503327958120,
     * "orderAmount": 20000,
     * "orderId": 375,
     * "orderSn": "20170821230558120l63268",
     * "orderStatus": 0,
     * "payStatus": 1,
     * "payWay": 0,
     * "remark": "1592",
     * "rstatus": 0,
     * "shippingAddress": "安徽省安庆市宜秀区安庆七街商业街",
     * "userId": 63268
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orders"
     * }
     */
    @RequestMapping(value = "/api/admin/orders", method = RequestMethod.GET)
    public String findOrderPage(Order order, int page) {
        order.setPage(page);
        if (!ObjectUtils.isEmpty(order.getSidx())) {
            order.setOrderBy(order.getSidx() + " " + order.getSord() + "," + order.getOrderBy());
        }
        List<OrderVO> orderVOS = new ArrayList<>();
        List<Order> orders=orderAdService.searchForPage(order);
        for (Order o:orders){
            OrderVO orderVO = new OrderVO();
            BeanUtils.copyProperties(o, orderVO);
            if (ObjectUtils.isEmpty(o.getUserId())){
                orderVO.setNickName("");
            }else {
                SysUser sysUser=sysUserService.selectByPK(o.getUserId());
                if (ObjectUtils.isEmpty(sysUser)){
                    orderVO.setNickName("");
                }else {
                    if (!ObjectUtils.isEmpty(sysUser.getNickname())){
                        orderVO.setNickName(sysUser.getNickname());
                    }else {
                        orderVO.setNickName("");
                    }
                }
            }
            orderVOS.add(orderVO);
        }
        MsgOut o = MsgOut.success(orderVOS);
        o.setRecords(order.getRecords());
        o.setTotal(order.getTotal());
        return this.renderJson(o);
    }


    /**
     * @api {get} /api/admin/ws/orders 后台管理-订单管理-微商商城订单列表
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/ws/orders
     * @apiName findOrderPageWs
     * @apiGroup Order
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "amountMoney": 0,
     * "amountScore": 0,
     * "auditStatus": 1,
     * "auditUserId": 0,
     * "consignee": "张杰青",
     * "ctime": 1491530571227,
     * "exPrice": 1000,
     * "exSn": "",
     * "exText": "",
     * "exWay": 0,
     * "goodsPayType": 0,
     * "memberPrice": 0,
     * "mobile": "13065356949",
     * "mtime": 1491530571227,
     * "orderAmount": 3500,
     * "orderId": 78,
     * "orderSn": "20170407100251227l6819",
     * "orderStatus": 1,
     * "payStatus": 1,
     * "payWay": 0,
     * "remark": "",
     * "rstatus": 0,
     * "shippingAddress": "北京市朝阳区四惠金地名京G座104",
     * "userId": 6819
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/ws/orders"
     * }
     */
    @RequestMapping(value = "/api/admin/ws/orders", method = RequestMethod.GET)
    public String findOrderPageWs(Order order, int page) {
        order.setPage(page);
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(order.getSidx())) {
            order.setOrderBy(order.getSidx() + " " + order.getSord() + "," + order.getOrderBy());
        }
        HashSet<Long> set = new HashSet<>();
        List<Order> orders = new ArrayList<>();
        Company company = companyService.selectByUserId(user.getId());
        OrderProd orderProd = new OrderProd();
        orderProd.setGoodsProviderId(company.getCompanyId());
        List<OrderProd> orderProds = orderProdService.select(orderProd);
        for (OrderProd orderProd1 : orderProds) {
            set.add(orderProd1.getOrderId());
        }
        for (Long id : set) {
            Order order1 = orderService.selectByPK(id);
            orders.add(order1);
        }
        MsgOut o = MsgOut.success(orders);
        o.setRecords(order.getRecords());
        o.setTotal(order.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/orderSetUp", method = RequestMethod.GET)
    public String findOrderSetUp() {
        String orderSetUp=redisUtil.get("order_set_up");
        MsgOut o = MsgOut.success(JSON.parseObject(orderSetUp,OrderSetUp.class));
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/admin/orderSetUp", method = RequestMethod.POST)
    public String createOrderSetUp(OrderSetUp orderSetUp) {
        redisUtil.set("order_set_up",JSON.toJSONString(orderSetUp));
        MsgOut o = MsgOut.success(orderSetUp);
        return this.renderJson(o);
    }
    /**
     * @api {get} /api/admin/orders/ByPK 后台管理-订单-amountFormatter
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/orders/ByPK
     * @apiName findOrderByPk
     * @apiGroup Order
     * @apiParam {int} orderId 订单ID.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "amountMoney": 0,
     * "amountScore": 0,
     * "auditStatus": 1,
     * "auditUserId": 0,
     * "consignee": "张杰青",
     * "ctime": 1491530571227,
     * "exPrice": 1000,
     * "exSn": "",
     * "exText": "",
     * "exWay": 0,
     * "goodsPayType": 0,
     * "memberPrice": 0,
     * "mobile": "13065356949",
     * "mtime": 1491530571227,
     * "orderAmount": 3500,
     * "orderId": 78,
     * "orderSn": "20170407100251227l6819",
     * "orderStatus": 1,
     * "payStatus": 1,
     * "payWay": 0,
     * "remark": "",
     * "rstatus": 0,
     * "shippingAddress": "北京市朝阳区四惠金地名京G座104",
     * "userId": 6819
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orders/ByPK"
     * }
     */
    @RequestMapping(value = "/api/admin/orders/ByPK", method = RequestMethod.GET)
    public String findOrderByPk(Long orderId) {
        MsgOut o = MsgOut.success(orderService.selectByPK(orderId));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/admin/logistics/name 后台管理-订单-获取快递公司名称列表
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/logistics/name
     * @apiName adminLogisticsName
     * @apiGroup Order
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "ctime": 0,
     * "logisticsId": 7,
     * "mtime": 0,
     * "name": "天天快递",
     * "remark": "",
     * "rstatus": 0
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/logistics/name"
     * }
     */

    @RequestMapping(value = "/api/admin/logistics/name", method = RequestMethod.GET)
    public String adminLogisticsName(Logistics logistics) {
        List<Logistics> logs = logisticsService.select(logistics);
        if (ObjectUtils.isEmpty(logs)) {
            return this.renderJson(MsgOut.success("no data"));
        }
        MsgOut o = MsgOut.success(logs);
        return this.renderJson(o);
    }
    /**
     * @api {get} /api/wap/logistics/name 获取快递公司名称列表
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/logistics/name
     * @apiName wapLogisticsName
     * @apiGroup Order
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "ctime": 0,
     * "logisticsId": "快递ID",
     * "mtime": 0,
     * "name": "天天快递",
     * "remark": "",
     * "rstatus": 0
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/wap/logistics/name"
     * }
     */
    @RequestMapping(value = "/api/wap/logistics/name", method = RequestMethod.GET)
    public String wapLogisticsName(Logistics logistics) {
        List<Logistics> logs = logisticsService.select(logistics);
        if (ObjectUtils.isEmpty(logs)) {
            return this.renderJson(MsgOut.success("no data"));
        }
        MsgOut o = MsgOut.success(logs);
        return this.renderJson(o);
    }

    /**
     * @api {put} /api/admin/orderProd/logistics 后台管理-订单-更新订单快递信息
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/orderProd/logistics
     * @apiPermission admin
     * @apiName adminLogisticsName
     * @apiGroup Order
     * @apiParam {int} prodId 订单流水ID[必选].
     * @apiParam {int} goodsId 商品ID[必选].
     * @apiParam {int} logisticsId 快递公司ID.
     * @apiParam {string} logisticsSn 快递单号.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "goodsId": 2043,
     * "logisticsId": 5,
     * "prodId": 366
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orderProd/logistics"
     * }
     */
    @RequestMapping(value = "/api/admin/orderProd/logistics", method = RequestMethod.PUT)
    public String updateOrderProdLogistics(OrderProd orderProd) {
        orderProdService.update(orderProd);
        MsgOut o = MsgOut.success(orderProd);
        return this.renderJson(o);
    }

//    /**
//     * 供货商商品订单
//     *
//     * @return
//     */
//    @RequestMapping(value = "/api/admin/orders", method = RequestMethod.GET)
//    public String list4user() {
//        SysUser user = LYSecurityUtil.currentSysUser();
//        if (ObjectUtils.isEmpty(user)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        Company company = new Company();
//        company.setUserId(user.getId());
//        List<Company> companyList = companyService.select(company);
//        if (companyList != null && companyList.size() > 0) {
//            company = companyList.get(0);
//        } else {
//            return this.renderJson(MsgOut.error("您还不是商家用户"));
//        }
//        OrderProd orderProd = new OrderProd();
//        orderProd.setGoodsProviderId(company.getCompanyId());
//        List<OrderProd> list = orderProdService.select(orderProd);
//        if (!ObjectUtils.isEmpty(list)) {
//            list.stream().
//        }
//        MsgOut o = MsgOut.success(list);
//        return this.renderJson(o);
//    }



    private byte profit_type_shop_score_order_reduce = -3;//积分兑换商品剪减此次积分

    private void setUserProfitForReduceScore(String orderSn, Long userId, Integer amount, Long consumerId, Long orderId, Integer profit, byte orderType) {

        UserProfit userProfit = new UserProfit();
        userProfit.setOrderSn(orderSn);
        userProfit.setUserId(userId);
        userProfit.setAmount(amount);
        userProfit.setConsumerId(consumerId);
        userProfit.setOrderId(orderId);
        userProfit.setUserProfit(profit);
        userProfit.setOrderType(orderType);//积分兑换商品
        userProfitService.insert(userProfit);
    }

    @Autowired
    private DistrictService districtService;

    //商品折扣
    private int getProfitByDiscount(int realPrice, int screanPrice) {

        int discount = realPrice * 100 / screanPrice;
        int profit = 0;
        int multiple = screanPrice / 10000;
        if (discount >= 80 && discount <= 90) {
            profit = 100;
        } else if (discount >= 70 && discount < 80) {
            profit = 200;
        } else if (discount >= 50 && discount < 70) {
            profit = 300;
        } else if (discount < 50) {
            profit = 400;
        }
        return profit * 8 * multiple;
    }

    private int getProfitByDiscountNot100(int discount) {
        if (discount >= 80 && discount <= 90) {
            return 100;
        } else if (discount >= 70 && discount < 80) {
            return 200;
        } else if (discount >= 50 && discount < 70) {
            return 300;
        } else if (discount < 50) {
            return 400;
        }
        return 0;
    }


    /**
     * @api {put} /api/admin/orders 后台管理-订单-更新订单信息-[审核状态]
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/orders
     * @apiPermission admin
     * @apiName updateOrder
     * @apiGroup Order
     * @apiParam {int} orderId 订单ID[必选].
     * @apiParam {int} auditStatus 审核状态[必选].
     * @apiParam {int} remark 备注.
     * @apiParam {string} logisticsSn 快递单号.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "auditStatus": 2,
     * "orderId": 363,
     * "remark": ""
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orders"
     * }
     */


    @RequestMapping(value = "/api/admin/orders", method = RequestMethod.PUT)
    public String updateOrder(Order order) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        orderService.update(order);
        MsgOut o = MsgOut.success(order);
        return this.renderJson(o);
    }

    /**
     * @api {post} /api/admin/order/cellEdit 后台管理-订单-更新订单信息-[审核状态CellEdit]
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/order/cellEdit
     * @apiPermission admin
     * @apiName updateOrderForCellEdit
     * @apiGroup Order
     * @apiParam {int} id 订单ID[必选].
     * @apiParam {int} auditStatus 审核状态[必选].
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "auditStatus": 2,
     * "id": 363,
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/order/cellEdit"
     * }
     */


    @RequestMapping(value = "/api/admin/order/cellEdit", method = RequestMethod.POST)
    public String updateOrderForCellEdit(Order order, Long id) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        order.setOrderId(id);
        if (!ObjectUtils.isEmpty(order.getOrderStatus())){
            ShoppingOrder where = new ShoppingOrder();
            where.setOrderId(id);
            List<ShoppingOrder> shoppingOrders=shoppingOrderService.select(where);
            for (ShoppingOrder so:shoppingOrders){
                so.setOrderStatus(order.getOrderStatus());
                shoppingOrderService.update(so);
            }
        }
        MsgOut o = MsgOut.success(orderService.update(order));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/admin/orderProd/companyName 后台管理-订单-订单详情默认商家名称
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/orderProd/companyName
     * @apiName selectGoodsProvidersName
     * @apiGroup Order
     * @apiParam {int} prodId 订单流水ID[必选].
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "address": "黑龙江省哈尔滨市五常市 黑龙江省哈尔滨市五常市小山子镇胜远村",
     * "auditCause": "",
     * "bankAccount": "0",
     * "bankDeposit": "0",
     * "bankId": "",
     * "bankName": "0",
     * "businessLicenseNumber": "",
     * "city": "哈尔滨市",
     * "coPayType": 3,
     * "coType": 0,
     * "companyGoodsCategotyId": 4,
     * "companyId": 166,
     * "companyName": "五常市中梁国米有机水稻农民专业合作社",
     * "companyStatus": 1,
     * "companyType": 1,
     * "ctime": 1498913024644,
     * "district": "五常市",
     * "districtId": 1102,
     * "headimgurl": "",
     * "imgBusinessLicense": "",
     * "imgIdBack": "http://static.ruitaowang.com/attached/file/20170701/20170701202020_636.jpg",
     * "imgIdFront": "http://static.ruitaowang.com/attached/file/20170701/20170701202007_839.jpg",
     * "imgOrganizationCodeCertificate": "http://static.ruitaowang.com/attached/file/20170701/20170701204312_692.jpg",
     * "imgTaxRegistrationCertificate": "",
     * "jmId": "232103197304223117",
     * "linkman": "梁国",
     * "mobile": "13351311208",
     * "mtime": 1499398152576,
     * "pid": 0,
     * "province": "黑龙江省",
     * "rangeSort": 0,
     * "redirectType": 1,
     * "remark": "",
     * "rstatus": 0,
     * "userId": 63078,
     * "xlErCert": ""
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orderProd/companyName"
     * }
     */
    @RequestMapping(value = "/api/admin/orderProd/companyName", method = RequestMethod.GET)
    public String selectGoodsProvidersName(Long prodId) {
        OrderProd orderProd = orderProdService.selectByPK(prodId);
        if (ObjectUtils.isEmpty(orderProd.getGoodsProviderId())) {
            return this.renderJson(MsgOut.success(""));
        }
        MsgOut o = MsgOut.success(companyService.selectByPK(orderProd.getGoodsProviderId()));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/admin/orderProd/ByPK 后台管理-订单-订单详情默认快递公司
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/orderProd/ByPK
     * @apiName findOrderProdByPK
     * @apiGroup Order
     * @apiParam {int} prodId 订单流水ID[必选].
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code":0,
     * "data":[
     * {
     * "companyBalance": 0,
     * "ctime": 1500784171652,
     * "giveScore": 0,
     * "giveType": 1,
     * "goodsAttrLinkId": "0",
     * "goodsAttrValues": "",
     * "goodsGetType": 0,
     * "goodsId": 2043,
     * "goodsName": "精品稻花香米5斤包邮大米",
     * "goodsNumber": 1,
     * "goodsPayType": 0,
     * "goodsProviderId": 166,
     * "goodsProviderType": 1,
     * "goodsRealPrice": 0,
     * "goodsScore": 0,
     * "goodsScreenPrice": 5900,
     * "goodsThum": "http://static.ruitaowang.com/attached/file/20170712/20170712103223_184.jpg",
     * "logisticsId": 5,
     * "logisticsName": "",
     * "logisticsSn": "",
     * "memberPrice": 0,
     * "mtime": 1500784171652,
     * "orderId": 363,
     * "prodId": 366,
     * "remark": "0",
     * "rstatus": 0,
     * "selfSupport": false,
     * "xpPrice": 0
     * }
     * ],
     * "msg":"操作成功啦",
     * "title":"成功",
     * "type":"SUCCESS"
     * }
     * @apiError InternalServerError.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orderProd/ByPK"
     * }
     */
    @RequestMapping(value = "/api/admin/orderProd/ByPK", method = RequestMethod.GET)
    public String findOrderProdByPK(Long prodId) {
        OrderProd orderProd = orderProdService.selectByPK(prodId);
        if (ObjectUtils.isEmpty(orderProd)) {
            return this.renderJson(MsgOut.success(""));
        }
        MsgOut o = MsgOut.success(orderProd);
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/admin/orders/excel/{orderSn} 后台管理-订单管理-商城订单-Excel导出
     * @apiSampleRequest /api/admin/orders/excel/{orderSn}
     * @apiVersion 0.1.0
     * @apiName adminOrderExcel
     * @apiGroup Order
     * @apiParam {string} orderSn 订单编号.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * @apiError Internal Server Error.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orders/excel/{orderSn}"
     * }
     */
    @RequestMapping(value = {"/api/admin/orders/excel/{orderSn}"})
    public void adminOrderExcel(@PathVariable("orderSn") String orderSn) {
        //订单1
        Order order1 = new Order();
        Order order = new Order();
        List<OrderVO> orderVOS = new ArrayList<>();
        order.setOrderSn(orderSn);
        List<Order> orders = orderService.select(order);
        for (Order od : orders) {
            OrderVO orderVO = new OrderVO();
            order1.setOrderId(od.getOrderId());
            orderVO.setStrtime(TimeUtils.getDatetime(TimeUtils.TimeFormat.LONG_DATE_PATTERN_LINE, od.getCtime()));
            orderVO.setPrice(od.getOrderAmount().doubleValue() + od.getExPrice().doubleValue() - od.getMemberPrice().doubleValue());
            orderVO.setExPriceVO(od.getExPrice().doubleValue());
            orderVO.setMemberPriceVO(od.getMemberPrice().doubleValue());
            BeanUtils.copyProperties(od, orderVO);
            orderVOS.add(orderVO);
        }
        //订单2
        List<OrderProdVO> orderProdVOS = new ArrayList<>();
        OrderProd orderProd = new OrderProd();
        OrderProdVO orderProdVO = new OrderProdVO();
        orderProd.setOrderId(order1.getOrderId());
        List<OrderProd> orderProds = orderProdService.select(orderProd);
        for (OrderProd op : orderProds) {
            Company company = companyService.selectByPK(op.getGoodsProviderId());
            if (!ObjectUtils.isEmpty(company)) {
                orderProdVO.setStrcompanyname(company.getCompanyName());
                orderProdVO.setStrlinkman(company.getLinkman());
                orderProdVO.setStrmobile(company.getMobile());
            }
            orderProdVO.setGoodsScreenPriceVO(PriceUtile.toFixedForPrice(op.getGoodsScreenPrice()));
            orderProdVO.setGoodsRealPriceVO(PriceUtile.toFixedForPrice(op.getGoodsRealPrice()));
            BeanUtils.copyProperties(op, orderProdVO);
            AfterMarket thisVal = new AfterMarket();
            thisVal.setProdId(orderProdVO.getProdId());
            List<AfterMarket> afterMarkets = afterMarketService.select(thisVal);
            if (!ObjectUtils.isEmpty(afterMarkets)) {
                switch (afterMarkets.get(0).getReturnType()) {
                    case 0:
                        orderProdVO.setRemark("退货退款");
                        break;
                    case 1:
                        orderProdVO.setRemark("退货");
                        break;
                    case 2:
                        orderProdVO.setRemark("换货");
                        break;
                }
            } else {
                orderProdVO.setRemark("正常");
            }
            orderProdVOS.add(orderProdVO);
        }
        //订单3
        List<UserProfitVO> userProfits = new ArrayList<>();
        UserProfit userProfit = new UserProfit();
        userProfit.setOrderSn(orderSn);
        List<UserProfit> userProfitList = userProfitService.select(userProfit);
        for (UserProfit up : userProfitList) {
            UserProfitVO userProfitVO = new UserProfitVO();
            userProfitVO.setUserId(up.getUserId());
            userProfitVO.setNickname(sysUserService.selectByPK(up.getUserId()).getNickname());
            userProfitVO.setStrtime(TimeUtils.getDatetime(TimeUtils.TimeFormat.LONG_DATE_PATTERN_LINE, up.getCtime()));
            userProfitVO.setMtime(up.getMtime());
            userProfitVO.setRstatus(up.getRstatus());
            userProfitVO.setAmount(up.getAmount());
            userProfitVO.setOrderId(up.getOrderId());
            if (up.getOrderType() == 4) {
                userProfitVO.setUserProfitvo(up.getUserProfit().doubleValue() * 100);
            } else {
                userProfitVO.setUserProfitvo(up.getUserProfit().doubleValue());
            }
            userProfitVO.setConsumerId(up.getConsumerId());
            switch (up.getOrderType()) {
                case 0:
                    userProfitVO.setStrordertrpe("商城供货商订单返利");
                    break;
                case 1:
                    userProfitVO.setStrordertrpe("地面店扫码订单返利");
                    break;
                case 2:
                    userProfitVO.setStrordertrpe("商城微商订单返利");
                    break;
                case 3:
                    userProfitVO.setStrordertrpe("地面店扫码订单返积分");
                    break;
                case 4:
                    userProfitVO.setStrordertrpe("商城微商订单返积分");
                    break;
                case 5:
                    userProfitVO.setStrordertrpe("点餐订单返积分");
                    break;
                case 6:
                    userProfitVO.setStrordertrpe("点餐订单返返利");
                    break;
                case 7:
                    userProfitVO.setStrordertrpe("快转返利");
                    break;
            }
            userProfitVO.setProfitId(up.getProfitId());
            userProfitVO.setOrderSn(up.getOrderSn());
            if (ObjectUtils.isEmpty(companyService.selectByPK(up.getUserId()))) {
                userProfitVO.setStrcompanytype("普通用户");
            } else {
                switch (companyService.selectByPK(up.getUserId()).getCompanyType()) {
                    case 0:
                        userProfitVO.setStrcompanytype("直接邀约人");
                        break;
                    case 1:
                        userProfitVO.setStrcompanytype("普通用户");
                        break;
                    case 3:
                        userProfitVO.setStrcompanytype("城市合伙人");
                        break;
                    case 4:
                        userProfitVO.setStrcompanytype("梦想合伙人");
                        break;
                    case 8:
                        userProfitVO.setStrcompanytype("普通用户");
                        break;
                }
            }
            userProfits.add(userProfitVO);
        }
        HttpServletResponse response = this.getResponse();
        //设置response头信息
        response.reset();
        response.setContentType("application/vnd.ms-excel");        //改成输出excel文件
        String fileName = "Order";
        response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");

        //创建工作簿并发送到浏览器
        try {
            //创键Excel表格中的第一个表,命名为"第一页"
            WritableWorkbook book = Workbook.createWorkbook(response.getOutputStream());
//第一sheet
            WritableSheet sheet = book.createSheet("订单详情", 0);
            //设置该单元格的表头
            List<String> keys = new ArrayList<>();
            keys.add("订单号");
            keys.add("订单编号");
            keys.add("收货人");
            keys.add("收货人电话");
            keys.add("购买时间");
            keys.add("支付状态");
            keys.add("订单总额（包括运费）");
            keys.add("会员优惠");
            keys.add("运费");
            keys.add("订单地址");
            // 添加标题
            for (int x = 0; x < keys.size(); x++) {
                sheet.addCell((WritableCell) new Label(x, 0, keys.get(x)));
            }
            // 添加数据
            for (int y = 0; y < orderVOS.size(); y++) {
                sheet.addCell(new Label(0, y + 1, orderVOS.get(y).getOrderId().toString()));
                sheet.addCell(new Label(1, y + 1, orderVOS.get(y).getOrderSn()));
                sheet.addCell(new Label(2, y + 1, orderVOS.get(y).getConsignee()));
                sheet.addCell(new Label(3, y + 1, orderVOS.get(y).getMobile()));
                sheet.addCell(new Label(4, y + 1, orderVOS.get(y).getStrtime()));
                if (orderVOS.get(y).getPayStatus() == 1){
                    sheet.addCell(new Label(5, y + 1, "已支付"));
                }else if (orderVOS.get(y).getPayStatus() == 0){
                    sheet.addCell(new Label(5, y + 1, "未支付"));
                }else {
                    sheet.addCell(new Label(5, y + 1, "未支付"));
                }
                sheet.addCell(new Label(6, y + 1, orderVOS.get(y).getPrice().toString()));
                sheet.addCell(new Label(7, y + 1, orderVOS.get(y).getMemberPriceVO().toString()));
                sheet.addCell(new Label(8, y + 1, orderVOS.get(y).getExPriceVO().toString()));
                sheet.addCell(new Label(9, y + 1, orderVOS.get(y).getShippingAddress()));
            }
//第二个sheet
            WritableSheet sheet2 = book.createSheet("商品订单", 1);

            //设置该单元格的表头
            List<String> keys2 = new ArrayList<>();
            keys2.add("订单号");
            keys2.add("商品名称");
            keys2.add("购买数量");
            keys2.add("商品规格");
            keys2.add("商品属性");
            keys2.add("卖出价");
            keys2.add("进货价");
            keys2.add("商品积分");
            keys2.add("订单状态");
            keys2.add("物流ID");
            keys2.add("物流公司");
            keys2.add("物流单号");
            keys2.add("供货商");
            keys2.add("联系人");
            keys2.add("电话");
            // 添加标题
            for (int x = 0; x < keys2.size(); x++) {
                sheet2.addCell((WritableCell) new Label(x, 0, keys2.get(x)));
            }
            // 添加数据
            for (int y = 0; y < orderProdVOS.size(); y++) {
                sheet2.addCell(new Label(0, y + 1, orderProdVOS.get(y).getOrderId().toString()));
                sheet2.addCell(new Label(1, y + 1, orderProdVOS.get(y).getGoodsName()));
                sheet2.addCell(new Label(2, y + 1, orderProdVOS.get(y).getGoodsNumber().toString()));
                sheet2.addCell(new Label(3, y + 1, orderProdVOS.get(y).getGoodsAttrValues()));
                sheet2.addCell(new Label(4, y + 1, orderProdVOS.get(y).getGoodsAttrLinkId()));
                sheet2.addCell(new Label(5, y + 1, orderProdVOS.get(y).getGoodsScreenPriceVO().toString()));
                sheet2.addCell(new Label(6, y + 1, orderProdVOS.get(y).getGoodsRealPriceVO().toString()));
                sheet2.addCell(new Label(7, y + 1, orderProdVOS.get(y).getGoodsScore().toString()));
                sheet2.addCell(new Label(8, y + 1, orderProdVOS.get(y).getRemark()));
                sheet2.addCell(new Label(9, y + 1, orderProdVOS.get(y).getLogisticsId().toString()));
                sheet2.addCell(new Label(10, y + 1, orderProdVOS.get(y).getLogisticsName()));
                sheet2.addCell(new Label(11, y + 1, orderProdVOS.get(y).getLogisticsSn()));
                sheet2.addCell(new Label(12, y + 1, orderProdVOS.get(y).getStrcompanyname()));
                sheet2.addCell(new Label(13, y + 1, orderProdVOS.get(y).getStrlinkman()));
                sheet2.addCell(new Label(14, y + 1, orderProdVOS.get(y).getStrmobile()));
            }
//            WritableSheet sheet3 = book.createSheet("返利详情", 2);
//
//            //设置该单元格的表头
//            List<String> keys3 = new ArrayList<>();
//            keys3.add("订单号");
//            keys3.add("订单编号");
//            keys3.add("受益人ID");
//            keys3.add("受益者名字");56666666666666634
//            keys3.add("受益者类型");
//            keys3.add("返利类型");
//            keys3.add("返利金额");
//            keys3.add("订单时间");
//            // 添加标题
//            for (int x = 0; x < keys3.size(); x++) {
//                sheet3.addCell((WritableCell) new Label(x, 0, keys3.get(x)));
//            }
//            // 添加数据
//            for (int y = 0; y < userProfits.size(); y++) {
//                sheet3.addCell(new Label(0, y + 1, userProfits.get(y).getOrderId().toString()));
//                sheet3.addCell(new Label(1, y + 1, userProfits.get(y).getOrderSn()));
//                sheet3.addCell(new Label(2, y + 1, userProfits.get(y).getUserId().toString()));
//                sheet3.addCell(new Label(3, y + 1, userProfits.get(y).getNickname()));
//                sheet3.addCell(new Label(4, y + 1, userProfits.get(y).getStrcompanytype()));
//                sheet3.addCell(new Label(5, y + 1, userProfits.get(y).getStrordertrpe()));
//                sheet3.addCell(new Label(6, y + 1, userProfits.get(y).getUserProfitvo().toString()));
//                sheet3.addCell(new Label(7, y + 1, userProfits.get(y).getStrtime()));
//            }
            // 写入数据并关闭文件
            book.write();
            book.close();
        } catch (Exception e) {

        }
    }

    /**
     * @api {get} /api/admin/orders/excel/list 后台管理-订单管理-商城订单-批量Excel导出
     * @apiSampleRequest /api/admin/orders/excel/list
     * @apiVersion 0.1.0
     * @apiName adminOrderList
     * @apiGroup Order
     * @apiParam {string} stime 开始时间.
     * @apiParam {string} etime 结束时间.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * @apiError Internal Server Error.
     * @apiErrorExample Error-Response:
     * HTTP/1.1 500 Internal Server Error
     * {
     * "timestamp":1504064947603,
     * "status":500,
     * "error":"Internal Server Error",
     * "exception":"java.lang.ArithmeticException",
     * "message":"/ by zero",
     * "path":"/api/admin/orders/excel/list"
     * }
     */
    @RequestMapping(value = {"/api/admin/orders/excel/list"}, method = RequestMethod.GET)
    public void adminOrderExcelList(Order order) {
        //订单1
        order.setPayStatus((byte) 1);
        List<Order> orders = orderAdService.select(order);
        List<OrderVO> orderVOS = new ArrayList<>();
        for (Order od : orders) {
            OrderVO orderVO = new OrderVO();
            orderVO.setStrtime(TimeUtils.getDatetime(TimeUtils.TimeFormat.LONG_DATE_PATTERN_LINE, od.getCtime()));
            orderVO.setPrice(od.getOrderAmount().doubleValue() + od.getExPrice().doubleValue() - od.getMemberPrice().doubleValue());
            orderVO.setExPriceVO(od.getExPrice().doubleValue());
            orderVO.setMemberPriceVO(od.getMemberPrice().doubleValue());
            BeanUtils.copyProperties(od, orderVO);
            orderVOS.add(orderVO);
        }
        //订单2
        List<OrderProdVO> orderProdVOS = new ArrayList<>();
        OrderProd orderProd = new OrderProd();
        for (Order od1 : orders) {
            orderProd.setOrderId(od1.getOrderId());
            List<OrderProd> orderProds = orderProdService.select(orderProd);
            for (OrderProd op : orderProds) {
                OrderProdVO orderProdVO = new OrderProdVO();
                Company company = companyService.selectByPK(op.getGoodsProviderId());
                if (!ObjectUtils.isEmpty(company)) {
                    orderProdVO.setStrcompanyname(company.getCompanyName());
                    orderProdVO.setStrlinkman(company.getLinkman());
                    orderProdVO.setStrmobile(company.getMobile());
                }
                orderProdVO.setGoodsScreenPriceVO(PriceUtile.toFixedForPrice(op.getGoodsScreenPrice()));
                orderProdVO.setGoodsRealPriceVO(PriceUtile.toFixedForPrice(op.getGoodsRealPrice()));
                BeanUtils.copyProperties(op, orderProdVO);
                AfterMarket thisVal = new AfterMarket();
                thisVal.setProdId(orderProdVO.getProdId());
                List<AfterMarket> afterMarkets = afterMarketService.select(thisVal);
                if (!ObjectUtils.isEmpty(afterMarkets)) {
                    switch (afterMarkets.get(0).getReturnType()) {
                        case 0:
                            orderProdVO.setRemark("退货退款");
                            break;
                        case 1:
                            orderProdVO.setRemark("退货");
                            break;
                        case 2:
                            orderProdVO.setRemark("换货");
                            break;
                    }
                } else {
                    orderProdVO.setRemark("正常");
                }
                orderProdVOS.add(orderProdVO);
            }
        }
        //订单3
        List<UserProfitVO> userProfits = new ArrayList<>();
        UserProfit userProfit = new UserProfit();
        for (Order od2 : orders) {
            userProfit.setOrderId(od2.getOrderId());
            List<UserProfit> userProfitList = userProfitService.select(userProfit);
            for (UserProfit up : userProfitList) {
                UserProfitVO userProfitVO = new UserProfitVO();
                userProfitVO.setUserId(up.getUserId());
                userProfitVO.setNickname(sysUserService.selectByPK(up.getUserId()).getNickname());
                userProfitVO.setStrtime(TimeUtils.getDatetime(TimeUtils.TimeFormat.LONG_DATE_PATTERN_LINE, up.getCtime()));
                userProfitVO.setMtime(up.getMtime());
                userProfitVO.setRstatus(up.getRstatus());
                userProfitVO.setAmount(up.getAmount());
                userProfitVO.setOrderId(up.getOrderId());
                if (up.getOrderType() == 4) {
                    userProfitVO.setUserProfitvo(up.getUserProfit().doubleValue() * 100);
                } else {
                    userProfitVO.setUserProfitvo(up.getUserProfit().doubleValue());
                }
                userProfitVO.setConsumerId(up.getConsumerId());
                switch (up.getOrderType()) {
                    case 0:
                        userProfitVO.setStrordertrpe("商城供货商订单返利");
                        break;
                    case 1:
                        userProfitVO.setStrordertrpe("地面店扫码订单返利");
                        break;
                    case 2:
                        userProfitVO.setStrordertrpe("商城微商订单返利");
                        break;
                    case 3:
                        userProfitVO.setStrordertrpe("地面店扫码订单返积分");
                        break;
                    case 4:
                        userProfitVO.setStrordertrpe("商城微商订单返积分");
                        break;
                    case 5:
                        userProfitVO.setStrordertrpe("点餐订单返积分");
                        break;
                    case 6:
                        userProfitVO.setStrordertrpe("点餐订单返返利");
                        break;
                    case 7:
                        userProfitVO.setStrordertrpe("快转返利");
                        break;
                }
                userProfitVO.setProfitId(up.getProfitId());
                userProfitVO.setOrderSn(up.getOrderSn());
                if (ObjectUtils.isEmpty(companyService.selectByPK(up.getUserId()))) {
                    userProfitVO.setStrcompanytype("普通用户");
                } else {
                    switch (companyService.selectByPK(up.getUserId()).getCompanyType()) {
                        case 0:
                            userProfitVO.setStrcompanytype("直接邀约人");
                            break;
                        case 1:
                            userProfitVO.setStrcompanytype("普通用户");
                            break;
                        case 3:
                            userProfitVO.setStrcompanytype("城市合伙人");
                            break;
                        case 4:
                            userProfitVO.setStrcompanytype("梦想合伙人");
                            break;
                        case 8:
                            userProfitVO.setStrcompanytype("普通用户");
                            break;
                    }
                }
                userProfits.add(userProfitVO);
            }
        }

        HttpServletResponse response = this.getResponse();
        //设置response头信息
        response.reset();
        response.setContentType("application/vnd.ms-excel");        //改成输出excel文件
        String fileName = "Order";
        response.setHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");

        //创建工作簿并发送到浏览器
        try {
            //创键Excel表格中的第一个表,命名为"第一页"
            WritableWorkbook book = Workbook.createWorkbook(response.getOutputStream());
//第一sheet
            WritableSheet sheet = book.createSheet("订单详情", 0);
            //设置该单元格的表头
            List<String> keys = new ArrayList<>();
            keys.add("订单号");
            keys.add("订单编号");
            keys.add("收货人");
            keys.add("收货人电话");
            keys.add("购买时间");
            keys.add("支付状态");
            keys.add("订单总额（包括运费）");
            keys.add("会员优惠");
            keys.add("运费");
            keys.add("订单地址");
            // 添加标题
            for (int x = 0; x < keys.size(); x++) {
                sheet.addCell((WritableCell) new Label(x, 0, keys.get(x)));
            }
            // 添加数据
            for (int y = 0; y < orderVOS.size(); y++) {
                sheet.addCell(new Label(0, y + 1, orderVOS.get(y).getOrderId().toString()));
                sheet.addCell(new Label(1, y + 1, orderVOS.get(y).getOrderSn()));
                sheet.addCell(new Label(2, y + 1, orderVOS.get(y).getConsignee()));
                sheet.addCell(new Label(3, y + 1, orderVOS.get(y).getMobile()));
                sheet.addCell(new Label(4, y + 1, orderVOS.get(y).getStrtime()));
                sheet.addCell(new Label(5, y + 1, orderVOS.get(y).getPayStatus().toString()));
                sheet.addCell(new Label(6, y + 1, orderVOS.get(y).getPrice().toString()));
                sheet.addCell(new Label(7, y + 1, orderVOS.get(y).getMemberPriceVO().toString()));
                sheet.addCell(new Label(8, y + 1, orderVOS.get(y).getExPriceVO().toString()));
                sheet.addCell(new Label(9, y + 1, orderVOS.get(y).getShippingAddress()));
            }
//第二个sheet
            WritableSheet sheet2 = book.createSheet("商品订单", 1);

            //设置该单元格的表头
            List<String> keys2 = new ArrayList<>();
            keys2.add("订单号");
            keys2.add("商品名称");
            keys2.add("购买数量");
            keys2.add("商品规格");
            keys2.add("商品属性");
            keys2.add("卖出价");
            keys2.add("进货价");
            keys2.add("商品积分");
            keys2.add("订单状态");
            keys2.add("物流ID");
            keys2.add("物流公司");
            keys2.add("物流单号");
            keys2.add("供货商");
            keys2.add("联系人");
            keys2.add("电话");
            // 添加标题
            for (int x = 0; x < keys2.size(); x++) {
                sheet2.addCell((WritableCell) new Label(x, 0, keys2.get(x)));
            }
            // 添加数据
            for (int y = 0; y < orderProdVOS.size(); y++) {
                sheet2.addCell(new Label(0, y + 1, orderProdVOS.get(y).getOrderId().toString()));
                sheet2.addCell(new Label(1, y + 1, orderProdVOS.get(y).getGoodsName()));
                sheet2.addCell(new Label(2, y + 1, orderProdVOS.get(y).getGoodsNumber().toString()));
                sheet2.addCell(new Label(3, y + 1, orderProdVOS.get(y).getGoodsAttrValues()));
                sheet2.addCell(new Label(4, y + 1, orderProdVOS.get(y).getGoodsAttrLinkId()));
                sheet2.addCell(new Label(5, y + 1, orderProdVOS.get(y).getGoodsScreenPriceVO().toString()));
                sheet2.addCell(new Label(6, y + 1, orderProdVOS.get(y).getGoodsRealPriceVO().toString()));
                sheet2.addCell(new Label(7, y + 1, orderProdVOS.get(y).getGoodsScore().toString()));
                sheet2.addCell(new Label(8, y + 1, orderProdVOS.get(y).getRemark()));
                sheet2.addCell(new Label(9, y + 1, orderProdVOS.get(y).getLogisticsId().toString()));
                sheet2.addCell(new Label(10, y + 1, orderProdVOS.get(y).getLogisticsName()));
                sheet2.addCell(new Label(11, y + 1, orderProdVOS.get(y).getLogisticsSn()));
                sheet2.addCell(new Label(12, y + 1, orderProdVOS.get(y).getStrcompanyname()));
                sheet2.addCell(new Label(13, y + 1, orderProdVOS.get(y).getStrlinkman()));
                sheet2.addCell(new Label(14, y + 1, orderProdVOS.get(y).getStrmobile()));
            }
//            WritableSheet sheet3 = book.createSheet("返利详情", 2);
//
//            //设置该单元格的表头
//            List<String> keys3 = new ArrayList<>();
//            keys3.add("订单号");
//            keys3.add("订单编号");
//            keys3.add("受益人ID");
//            keys3.add("受益者名字");
//            keys3.add("受益者类型");
//            keys3.add("返利类型");
//            keys3.add("返利金额");
//            keys3.add("订单时间");
//            // 添加标题
//            for (int x = 0; x < keys3.size(); x++) {
//                sheet3.addCell((WritableCell) new Label(x, 0, keys3.get(x)));
//            }
//            // 添加数据
//            for (int y = 0; y < userProfits.size(); y++) {
//                sheet3.addCell(new Label(0, y + 1, userProfits.get(y).getOrderId().toString()));
//                sheet3.addCell(new Label(1, y + 1, userProfits.get(y).getOrderSn()));
//                sheet3.addCell(new Label(2, y + 1, userProfits.get(y).getUserId().toString()));
//                sheet3.addCell(new Label(3, y + 1, userProfits.get(y).getNickname()));
//                sheet3.addCell(new Label(4, y + 1, userProfits.get(y).getStrcompanytype()));
//                sheet3.addCell(new Label(5, y + 1, userProfits.get(y).getStrordertrpe()));
//                sheet3.addCell(new Label(6, y + 1, userProfits.get(y).getUserProfitvo().toString()));
//                sheet3.addCell(new Label(7, y + 1, userProfits.get(y).getStrtime()));
//            }
            // 写入数据并关闭文件
            book.write();
            book.close();
        } catch (Exception e) {

        }
    }

    @RequestMapping(value = "/api/admin/order/selectByCompany", method = RequestMethod.GET)
    public String selectByCompany(OrderVO orderVO,String allOrderBegin,String allOrderEnd) {
        Map map = new HashMap();
        orderVO.setAllOrderBegin(allOrderBegin);
        orderVO.setAllOrderEnd(allOrderEnd);
        double sales = 0;
        List<OrderVO> orderProds = orderAdService.selectByCompany(orderVO);
        for(Order o : orderProds){
            sales = o.getExPrice() + o.getOrderAmount() - o.getMemberPrice();
            sales++;
        }
        map.put("orderCount",orderProds.size());
        map.put("sales",sales);
        MsgOut o = MsgOut.success(map);
        return this.renderJson(o);
    }

    /**
     * 商家订单个数（评价）（暂时不需要，用上个接口，这个 接口过滤掉了已评价）
     * @param shoppingOrder
     * @param userId
     * @return
     */
//    @RequestMapping(value = "/api/wap/selectForOrderPjCompanyCount", method = RequestMethod.GET)
//    public String selectForOrderPjCompanyCount(ShoppingOrder shoppingOrder,@RequestHeader(value="headId",required = false) Long userId){
//        if (ObjectUtils.isEmpty(userId)){
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        MsgOut o = MsgOut.success(shoppingOrderService.selectForCompanyPjCount(shoppingOrder));
//        return this.renderJson(o);
//    }


//    @RequestMapping(value = "/api/wap/selectForOrderCompany", method = RequestMethod.GET)
//    public String selectForOrderCompany(OrderVO orderVO,int page,@RequestHeader(value="headId",required = false) Long userId){
//        if (ObjectUtils.isEmpty(userId)){
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        orderVO.setPage(page);
//        List<Order> orderVOList = orderService.selectForOrderCompany(orderVO);
//        List<OrderVO> orderVOS = new ArrayList<>();
//        for (Order o : orderVOList){
//            int goodsNumber = 0;
//            //double totalPrice = 0;
//            int memberPrice = 0;
//            OrderVO orderVO1 = new OrderVO();
//            BeanUtils.copyProperties(o, orderVO1);
//            List<ShoppingOrderVO> shoppingOrderVOS= new ArrayList<>();
//            ShoppingOrder shoppingOrder = new ShoppingOrder();
//            shoppingOrder.setOrderId(o.getOrderId());
//            shoppingOrder.setCompanyId(orderVO.getCompanyId());
//            List<ShoppingOrder> shoppingOrders=shoppingOrderService.select(shoppingOrder);
//            for (ShoppingOrder so : shoppingOrders){
//                //int amount = so.getAmount();
//                ShoppingOrderVO shoppingOrderVO = new ShoppingOrderVO();
//                BeanUtils.copyProperties(so, shoppingOrderVO);
//                OrderProd orderProd = new OrderProd();
//                orderProd.setOrderId(so.getId());
//                orderProd.setOrderType((byte)1);
//                List<OrderProd> orderProds = orderProdService.select(orderProd);
//                for (OrderProd op : orderProds){
//                    goodsNumber = goodsNumber + op.getGoodsNumber();
//                    //totalPrice += totalPrice + op.getGoodsScreenPrice() * op.getGoodsNumber() - op.getMemberPrice();
//                    //memberPrice = memberPrice + op.getMemberPrice();
//                }
//                //amount = so.getAmount() - memberPrice;
//                //shoppingOrderVO.setAmount(amount);
//                shoppingOrderVO.setGoodsList(orderProds);
//                shoppingOrderVOS.add(shoppingOrderVO);
//            }
//            orderVO1.setCompanyList(shoppingOrderVOS);
//            //orderVO1.setPrice(totalPrice);
//            orderVO1.setGoodsNumber(goodsNumber);
//            orderVOS.add(orderVO1);
//        }
//        MsgOut o = MsgOut.success(orderVOS);
//        o.setRecords(orderVO.getRecords());
//        o.setTotal(orderVO.getTotal());
//        return this.renderJson(o);
//    }

    @RequestMapping(value = "/api/adminShoppingOrder/{orderId}")
    public String adminShoppingOrder(@PathVariable("orderId") Long orderId){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        ShoppingOrder shoppingOrder = new ShoppingOrder();
        shoppingOrder.setOrderId(orderId);
        return this.renderJson(shoppingOrderService.select(shoppingOrder));
    }

    @RequestMapping(value = "/api/admin/updateShoppingOrder", method = RequestMethod.POST)
    public String updateOrder(ShoppingOrder shoppingOrder){
        shoppingOrderService.update(shoppingOrder);
        MsgOut o = MsgOut.success(shoppingOrder);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/ShoppingOrder/fjOrder")
    public String fjOrder(ShoppingOrder shoppingOrder,int page){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        shoppingOrder.setPage(page);
        List<ShoppingOrder> shoppingOrderList = shoppingOrderService.fjOrder(shoppingOrder);
        MsgOut o = MsgOut.success(shoppingOrderList);
        o.setRecords(shoppingOrder.getRecords());
        o.setTotal(shoppingOrder.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/Order/fjOrder")
    public String fjOrder(Order order,int page){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        order.setPage(page);
        List<Order> shoppingOrderList = orderService.fjOrder(order);
        MsgOut o = MsgOut.success(shoppingOrderList);
        o.setRecords(order.getRecords());
        o.setTotal(order.getTotal());
        return this.renderJson(o);
    }
}
