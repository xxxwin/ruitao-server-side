package com.ruitaowang.core.domain;

public class LsMsg extends Page {
    /**
     *  自增编码,所属表字段为ls_msg.msg_id
     */
    private Long msgId;

    /**
     *  发送者编码,所属表字段为ls_msg.from_user_id
     */
    private Long fromUserId;

    /**
     *  发送者昵称,所属表字段为ls_msg.from_user_text
     */
    private String fromUserText;

    /**
     *  发送者头像,所属表字段为ls_msg.from_user_avatar
     */
    private String fromUserAvatar;


    /**
     *  公司编码,所属表字段为ls_msg.company_id
     */
    private Long companyId;

    /**
     *  接收者编码,所属表字段为ls_msg.to_user_id
     */
    private Long toUserId;

    /**
     *  接收者昵称,所属表字段为ls_msg.to_user_text
     */
    private String toUserText;

    /**
     *  接收者头像,所属表字段为ls_msg.to_user_avatar
     */
    private String toUserAvatar;

    /**
     *  用户行为
0 普通消息
1 购分享
2 我耀评
3 我耀赞
4 我耀秀
5 趣竞价
6 积分红包
7 赏
8 耀霸屏
9 商家促销
10 商家秀
11 品牌故事
12 后厨通知菜做好了
13 用户延后上菜
14 用户呼叫服务员,所属表字段为ls_msg.pred
     */
    private Byte pred;

    /**
     *  pred说明,所属表字段为ls_msg.pred_text
     */
    private String predText;

    /**
     *  消息类型
0 文本
1 图片
2 语音
3 视频,所属表字段为ls_msg.msg_type
     */
    private Byte msgType;

    /**
     *  互动内容,富文本,所属表字段为ls_msg.msg_content
     */
    private String msgContent;

    /**
     *  霸屏数量,所属表字段为ls_msg.usurp_screen_num
     */
    private Integer usurpScreenNum;

    /**
     *  霸屏主题编码,所属表字段为ls_msg.usurp_screen_theme_id
     */
    private Long usurpScreenThemeId;

    /**
     *  霸屏主题说明,所属表字段为ls_msg.usurp_screen_theme_text
     */
    private String usurpScreenThemeText;

    /**
     *  霸屏时长,所属表字段为ls_msg.usurp_screen_time
     */
    private Integer usurpScreenTime;

    /**
     *  霸屏单价,所属表字段为ls_msg.usurp_screen_price
     */
    private Integer usurpScreenPrice;

    /**
     *  霸屏桌号,所属表字段为ls_msg.usurp_screen_table_no
     */
    private Integer usurpScreenTableNo;

    /**
     *  打赏对象编码,所属表字段为ls_msg.reward_to
     */
    private Long rewardTo;

    /**
     *  打赏说明说明,所属表字段为ls_msg.reward_text
     */
    private String rewardText;

    /**
     *  打赏礼物,所属表字段为ls_msg.reward_gift
     */
    private Integer rewardGift;

    /**
     *  打赏礼物单价,所属表字段为ls_msg.reward_price
     */
    private Integer rewardPrice;

    /**
     *  打赏礼物数量,所属表字段为ls_msg.reward_gift_num
     */
    private Integer rewardGiftNum;

    /**
     *  乐队或歌手名,所属表字段为ls_msg.play_singer
     */
    private String playSinger;

    /**
     *  乐队或歌手编码,所属表字段为ls_msg.play_singer_id
     */
    private Long playSingerId;

    /**
     *  点播音乐编码,所属表字段为ls_msg.play_music_id
     */
    private Long playMusicId;

    /**
     *  点播音乐名字,所属表字段为ls_msg.play_music_text
     */
    private String playMusicText;

    /**
     *  点播音乐单价,所属表字段为ls_msg.play_music_price
     */
    private Integer playMusicPrice;

    /**
     *  点播音乐数量,所属表字段为ls_msg.play_music_num
     */
    private Integer playMusicNum;

    /**
     *  红包金额,所属表字段为ls_msg.lucky_money_price
     */
    private Integer luckyMoneyPrice;

    /**
     *  红包数量,所属表字段为ls_msg.lucky_money_num
     */
    private Integer luckyMoneyNum;

    /**
     *  创建时间,所属表字段为ls_msg.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为ls_msg.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为ls_msg.remark
     */
    private String remark;

    /**
     *  0 正常
1 删除,所属表字段为ls_msg.rstatus
     */
    private Byte rstatus;

    /**
     *  支付状态0 未支付，1 已支付,所属表字段为ls_msg.pay_status
     */
    private Byte payStatus;

    /**
     *  支付单号,所属表字段为ls_msg.pay_sn
     */
    private String paySn;

    /**
     *  经度,所属表字段为longitude
     */
    private String longitude;

    /**
     * 纬度,所属表字段为latitude
     */
    private String latitude;

    /**
     *  红包名称，所属表字段为所属表字段为lucky_money_name
     */
    private String luckyMoneyName;

    public String getLuckyMoneyName() {
        return luckyMoneyName;
    }

    public void setLuckyMoneyName(String luckyMoneyName) {
        this.luckyMoneyName = luckyMoneyName;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getFromUserText() {
        return fromUserText;
    }

    public void setFromUserText(String fromUserText) {
        this.fromUserText = fromUserText == null ? null : fromUserText.trim();
    }

    public String getFromUserAvatar() {
        return fromUserAvatar;
    }

    public void setFromUserAvatar(String fromUserAvatar) {
        this.fromUserAvatar = fromUserAvatar == null ? null : fromUserAvatar.trim();
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getToUserText() {
        return toUserText;
    }

    public void setToUserText(String toUserText) {
        this.toUserText = toUserText == null ? null : toUserText.trim();
    }

    public String getToUserAvatar() {
        return toUserAvatar;
    }

    public void setToUserAvatar(String toUserAvatar) {
        this.toUserAvatar = toUserAvatar == null ? null : toUserAvatar.trim();
    }

    public Byte getPred() {
        return pred;
    }

    public void setPred(Byte pred) {
        this.pred = pred;
    }

    public String getPredText() {
        return predText;
    }

    public void setPredText(String predText) {
        this.predText = predText == null ? null : predText.trim();
    }

    public Byte getMsgType() {
        return msgType;
    }

    public void setMsgType(Byte msgType) {
        this.msgType = msgType;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent == null ? null : msgContent.trim();
    }

    public Integer getUsurpScreenNum() {
        return usurpScreenNum;
    }

    public void setUsurpScreenNum(Integer usurpScreenNum) {
        this.usurpScreenNum = usurpScreenNum;
    }

    public Long getUsurpScreenThemeId() {
        return usurpScreenThemeId;
    }

    public void setUsurpScreenThemeId(Long usurpScreenThemeId) {
        this.usurpScreenThemeId = usurpScreenThemeId;
    }

    public String getUsurpScreenThemeText() {
        return usurpScreenThemeText;
    }

    public void setUsurpScreenThemeText(String usurpScreenThemeText) {
        this.usurpScreenThemeText = usurpScreenThemeText == null ? null : usurpScreenThemeText.trim();
    }

    public Integer getUsurpScreenTime() {
        return usurpScreenTime;
    }

    public void setUsurpScreenTime(Integer usurpScreenTime) {
        this.usurpScreenTime = usurpScreenTime;
    }

    public Integer getUsurpScreenPrice() {
        return usurpScreenPrice;
    }

    public void setUsurpScreenPrice(Integer usurpScreenPrice) {
        this.usurpScreenPrice = usurpScreenPrice;
    }

    public Integer getUsurpScreenTableNo() {
        return usurpScreenTableNo;
    }

    public void setUsurpScreenTableNo(Integer usurpScreenTableNo) {
        this.usurpScreenTableNo = usurpScreenTableNo;
    }

    public Long getRewardTo() {
        return rewardTo;
    }

    public void setRewardTo(Long rewardTo) {
        this.rewardTo = rewardTo;
    }

    public String getRewardText() {
        return rewardText;
    }

    public void setRewardText(String rewardText) {
        this.rewardText = rewardText == null ? null : rewardText.trim();
    }

    public Integer getRewardGift() {
        return rewardGift;
    }

    public void setRewardGift(Integer rewardGift) {
        this.rewardGift = rewardGift;
    }

    public Integer getRewardPrice() {
        return rewardPrice;
    }

    public void setRewardPrice(Integer rewardPrice) {
        this.rewardPrice = rewardPrice;
    }

    public Integer getRewardGiftNum() {
        return rewardGiftNum;
    }

    public void setRewardGiftNum(Integer rewardGiftNum) {
        this.rewardGiftNum = rewardGiftNum;
    }

    public String getPlaySinger() {
        return playSinger;
    }

    public void setPlaySinger(String playSinger) {
        this.playSinger = playSinger == null ? null : playSinger.trim();
    }

    public Long getPlaySingerId() {
        return playSingerId;
    }

    public void setPlaySingerId(Long playSingerId) {
        this.playSingerId = playSingerId;
    }

    public Long getPlayMusicId() {
        return playMusicId;
    }

    public void setPlayMusicId(Long playMusicId) {
        this.playMusicId = playMusicId;
    }

    public String getPlayMusicText() {
        return playMusicText;
    }

    public void setPlayMusicText(String playMusicText) {
        this.playMusicText = playMusicText == null ? null : playMusicText.trim();
    }

    public Integer getPlayMusicPrice() {
        return playMusicPrice;
    }

    public void setPlayMusicPrice(Integer playMusicPrice) {
        this.playMusicPrice = playMusicPrice;
    }

    public Integer getPlayMusicNum() {
        return playMusicNum;
    }

    public void setPlayMusicNum(Integer playMusicNum) {
        this.playMusicNum = playMusicNum;
    }

    public Integer getLuckyMoneyPrice() {
        return luckyMoneyPrice;
    }

    public void setLuckyMoneyPrice(Integer luckyMoneyPrice) {
        this.luckyMoneyPrice = luckyMoneyPrice;
    }

    public Integer getLuckyMoneyNum() {
        return luckyMoneyNum;
    }

    public void setLuckyMoneyNum(Integer luckyMoneyNum) {
        this.luckyMoneyNum = luckyMoneyNum;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Byte getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Byte payStatus) {
        this.payStatus = payStatus;
    }

    public String getPaySn() {
        return paySn;
    }

    public void setPaySn(String paySn) {
        this.paySn = paySn == null ? null : paySn.trim();
    }
}