package com.ruitaowang.admin.view.api;

import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.GoodsSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APISearchController extends BaseController{

    @Autowired
    private GoodsSearchService goodsSearchService;

    @RequestMapping(value = "/api/search", method = RequestMethod.GET)
    public String search(String keyword,int page,int rows){
        List<Goods> list;
        if(!StringUtils.hasLength(keyword)){
            list = new ArrayList<>(0);
            MsgOut o = MsgOut.success(list);
            return this.renderJson(o);
        }
        Goods goods = new Goods();
        goods.setPage(page);
        goods.setRows(rows);
        goods.setGoodsName(keyword.trim());
        goods.setGoodsPayType((byte) 0);
        goods.setOrderBy("goods_recommend_sort desc");
        list = goodsSearchService.searchForPage(goods);
        MsgOut o = MsgOut.success(list);
        o.setRecords(goods.getRecords());
        o.setTotal(goods.getTotal());
        return this.renderJson(o);
    }

}
