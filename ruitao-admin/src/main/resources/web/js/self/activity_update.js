/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/activities',
    updateUrl: '/api/activities',
    deleteUrl: '/api/activities',
    selectUrl: '/api/activities'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
var ue = UE.getEditor('editor');
ue.ready(function() {
    ue.execCommand('serverparam', {
        'isActivity': 1
    });
    ue.setContent($("input[name=activityContent]").val());
});
$().ready(function () {
    $('#activityType').val(activityTypeChecked);
    $('#submitCategoryCreate').click( function() {
        var activityImageUrl = $("input[name=activityImageUrl]").val().indexOf("/ueditor/jsp/upload/image");
        if(activityImageUrl != -1){
            alert("提交失败原因：上传图片地址错误");
            return false;
        }
        // if($("p").attr("name") == "2"){
        //     toastr.error("商品不存在", "ERROR");
        //     return false;
        // }
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()){
            //set content
            $("input[name=activityContent]").val(ue.getContent());
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.updateUrl,
                    type: "PUT",
                    data : postData,
                    dataType: "json",
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });

    $('#buttonGoback').click(function() {
        history.go(-1);
        location.reload();
        return false;
    });

    //uedit监听事件
    ue.addListener("contentChange",function(){
        console.log('内容改变:'+ue.getContent());
        if(ue.getContent().indexOf('/ueditor/jsp/') != -1) {
            toastr.error("改图片存在问题，请重新删除后重新上传.", "ERROR");
        }
    });
});
// $('#btn_temporary').click(function () {
//     //判断上传控件中是否选择了图片
//     var image = $("#file_temporaryImage").val();
//     if ($.trim(image) == "") {
//         alert("请选择图片！");
//         return;
//     }
//     //提交请求处理的url
//     var actionUrl = "/uploadForDish";
//     //开始ajax操作
//     $("#uploadForm").ajaxSubmit({
//         type: "POST",//提交类型
//         dataType: "json",//返回结果格式
//         url: actionUrl,//请求地址
//         success: function (data) {//请求成功后的函数
//             if (data.status == "warning") {//返回警告
//                 alert("上传图片：警告");
//             } else if (data.type == "SUCCESS") {//返回成功
//                 $("#div_temporaryImage").append("<img style='width:300px;height:300px;' src='" + data.msg + "' />");
//                 $("#activityImageUrl").val(data.msg);
//             }
//         },
//         error: function (data) { alert("上传失败原因：可能图片大于1M"); },//请求失败的函数
//         async: true
//     });
// });
var input;
$("input:file").change(function(){
    input = this;
    lrz(this.files[0], {width: 640})
        .then(function (rst) {
            // 把处理的好的图片给用户看看呗
            var img = new Image();
            img.src = rst.base64;
            img.onload = function () {
                $(".div_temporaryImage").html(img);
            };
            return rst;
        })
        .then(function (rst) {
            // 这里该上传给后端啦
            // 额外添加参数
            rst.formData.append('fileLen', rst.fileLen);
            $.ajax({
                url: '/uploadForCertificate', // 这个地址做了跨域处理，可以用于实际调试
                data: rst.formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if(data.type=="SUCCESS"){
                        $(".activityImageUrl").val(data.msg);
                        $(input).parent().siblings("span").html("上传成功");
                        // $(input).parent("div").parent("div").next("div").html('<img src="'+data.msg+'">');
                    }else{
                        alert("上传失败，请稍后重试");
                    }
                }
            });
            return rst;
        })
        .catch(function (err) {
            alert("上传失败，请稍后重试");
        })
        .always(function () {

        });
});