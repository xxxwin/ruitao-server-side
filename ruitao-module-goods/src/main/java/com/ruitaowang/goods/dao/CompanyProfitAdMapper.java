package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CompanyProfit;

public interface CompanyProfitAdMapper {

    String selectMoney(CompanyProfit companyProfit);
}