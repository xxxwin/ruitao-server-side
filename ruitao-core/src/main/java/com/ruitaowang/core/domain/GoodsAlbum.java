package com.ruitaowang.core.domain;

public class GoodsAlbum extends Page {
    /**
     *  相册编号,所属表字段为goods_album.image_id
     */
    private Long imageId;

    /**
     *  商品编号,所属表字段为goods_album.goods_id
     */
    private Long goodsId;

    /**
     *  1 相册
0 详情,所属表字段为goods_album.image_type
     */
    private Byte imageType;

    /**
     *  创建时间,所属表字段为goods_album.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为goods_album.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常
1 删除,所属表字段为goods_album.rstatus
     */
    private Byte rstatus;

    /**
     *  image url,所属表字段为goods_album.url
     */
    private String url;

    /**
     *  原名,所属表字段为goods_album.original
     */
    private String original;

    /**
     *  upload type,所属表字段为goods_album.state
     */
    private String state;

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Byte getImageType() {
        return imageType;
    }

    public void setImageType(Byte imageType) {
        this.imageType = imageType;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original == null ? null : original.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }
}