package com.ruitaowang.core.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class SysUserVO extends SysUser {

    private long directNum;
    private long indirectNum;
    private int userCompanyType;
    private int companyStatus;
    private Long companyId;
    private byte qrPay;
    private Byte buyStatus;
    private String ruitaoCompanyName;
    private String selectForBh;
    private Byte userInfoType = 0;
    private String token;
}
