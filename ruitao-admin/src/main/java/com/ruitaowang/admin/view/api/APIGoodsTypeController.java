package com.ruitaowang.admin.view.api;

import com.ruitaowang.core.domain.GoodsAttribute;
import com.ruitaowang.core.domain.GoodsType;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.GoodsAttributeService;
import com.ruitaowang.goods.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Shaka on 2016/11/23.
 */
@RestController
public class APIGoodsTypeController extends BaseController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    @RequestMapping("/api/goodsType")
    public String list() {
        List<GoodsType> list;
        GoodsType goodsType = new GoodsType();
        list = goodsTypeService.select(goodsType);
        MsgOut o = MsgOut.success(list);
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/admin/rewards/get 后台管理-商品属性-商品属性列表
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/rewards/get
     *
     * @apiName findGoodsType
     * @apiGroup GoodsType
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *               "ctime": 1503458111712,
     *               "enabled": false,
     *               "mtime": 1503458111712,
     *               "rstatus": 0,
     *               "typeGroup": "",
     *               "typeId": 80,
     *               "typeName": "食品"
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/rewards/get"
     *  }
     */
    @RequestMapping("/api/admin/goodsType/get")
    public String findGoodsType(GoodsType goodsType) {
        MsgOut o = MsgOut.success(goodsTypeService.select(goodsType));
        return this.renderJson(o);
    }
    /**
     * @api {get} /api/admin/goodsType/ByPk/get/:goodsTypeId 后台管理-商品属性-查询商品属性ByPK
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/goodsType/ByPk/get/:goodsTypeId
     *
     * @apiName findGoodsType
     * @apiGroup GoodsType
     *
     * @apiParam {int} goodsTypeId 商品分类Id.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *               "ctime": 1503458111712,
     *               "enabled": false,
     *               "mtime": 1503458111712,
     *               "rstatus": 0,
     *               "typeGroup": "",
     *               "typeId": 80,
     *               "typeName": "食品"
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/goodsType/ByPk/get/{goodsTypeId}"
     *  }
     */
    @RequestMapping("/api/admin/goodsType/ByPk/get/{goodsTypeId}")
    public String list(@PathVariable("goodsTypeId") Long goodsTypeId){
        MsgOut o=MsgOut.success(goodsTypeService.selectByPK(goodsTypeId));
        return  this.renderJson(o);
    }
    /**
     * @api {post} /api/admin/goodsType/post 后台管理-商品属性-添加商品属性
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/goodsType/post
     *
     * @apiName createGoodsType
     * @apiGroup GoodsType
     *
     * @apiParam {string} typeGroup 商品类型组.
     * @apiParam {string} typeName 商品类型名称.
     * @apiParam {Boolean} enabled 启用.
     * @apiParam {byte} rstatus 行状态.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *               "ctime": 1503458111712,
     *               "enabled": false,
     *               "mtime": 1503458111712,
     *               "rstatus": 0,
     *               "typeGroup": "",
     *               "typeId": 80,
     *               "typeName": "食品"
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/goodsType/post"
     *  }
     */
    @RequestMapping(value = "/api/admin/goodsType/post",method = RequestMethod.POST)
    public String createGoodsType(GoodsType goodsType){
        goodsTypeService.insert(goodsType);
        MsgOut o=MsgOut.success(goodsType);
        return this.renderJson(o);
    }
    /**
     * @api {put} /api/admin/goodsType/update 后台管理-商品属性-修改商品属性
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/goodsType/update
     *
     * @apiName updateGoodsType
     * @apiGroup GoodsType
     *
     * @apiParam {string} typeGroup 商品类型组.
     * @apiParam {string} typeName 商品类型名称.
     * @apiParam {Boolean} enabled 启用.
     * @apiParam {byte} rstatus 行状态.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *               "ctime": 1503458111712,
     *               "enabled": false,
     *               "mtime": 1503458111712,
     *               "rstatus": 0,
     *               "typeGroup": "",
     *               "typeId": 80,
     *               "typeName": "食品"
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/goodsType/update"
     *  }
     */
    @RequestMapping(value="/api/admin/goodsType/update",method =RequestMethod.PUT)
    public String updateGoodsType(GoodsType goodsType){
        goodsTypeService.update(goodsType);
        MsgOut o=MsgOut.success(goodsType);
        return this.renderJson(o);
    }
    /**
     * @api {delete} /api/admin/goodsType/delete 后台管理-商品属性-删除商品属性
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/admin/goodsType/delete
     * @apiName findGoodsType
     * @apiGroup GoodsType
     *
     * @apiParam {int} goodsTypeId 商品分类Id.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":1,
     *       "msg":"操作成功啦",
     *       "title":"成功",
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/admin/goodsType/delete"
     *  }
     */
    @RequestMapping(value = "/api/goodsType/{goodsTypeId}" ,method = RequestMethod.DELETE)
    public String deleteGoodsType(@PathVariable("goodsTypeId") Long goodsTypeId){
        MsgOut o=MsgOut.success(goodsTypeService.delete(goodsTypeId));
        return this .renderJson(o);
    }






}
