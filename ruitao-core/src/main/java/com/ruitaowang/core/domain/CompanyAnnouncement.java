package com.ruitaowang.core.domain;

public class CompanyAnnouncement extends Page {
    /**
     *  ,所属表字段为company_announcement.id
     */
    private Long id;

    /**
     *  店铺id,所属表字段为company_announcement.company_id
     */
    private Long companyId;

    /**
     *  公告内容,所属表字段为company_announcement.notice
     */
    private String notice;

    /**
     *  创建时间,所属表字段为company_announcement.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为company_announcement.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为company_announcement.remark
     */
    private String remark;

    /**
     *  状态 0:正常 1:删除,所属表字段为company_announcement.rstatus
     */
    private Byte rstatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice == null ? null : notice.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}