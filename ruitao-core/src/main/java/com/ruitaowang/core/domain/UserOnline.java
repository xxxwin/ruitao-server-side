package com.ruitaowang.core.domain;

public class UserOnline extends Page {
    /**
     *  自增主键,所属表字段为user_online.id
     */
    private Long id;

    /**
     *  用户编码,所属表字段为user_online.user_id
     */
    private Long userId;

    /**
     *  房间编码，即店铺companyId,所属表字段为user_online.room_id
     */
    private Long roomId;

    /**
     *  备注,所属表字段为user_online.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为user_online.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为user_online.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为user_online.rstatus
     */
    private Byte rstatus;

    /**
     *  最后登录IP,所属表字段为user_online.last_ip
     */
    private String lastIp;

    /**
     *  ,所属表字段为user_online.headimgurl
     */
    private String headimgurl;

    /**
     *  昵称,所属表字段为user_online.nickname
     */
    private String nickname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp == null ? null : lastIp.trim();
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl == null ? null : headimgurl.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }
}