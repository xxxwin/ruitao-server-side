var Config = {
    selectGoodsUrl: '/api/goods/goodsCount',
    selectOrderAmount : '/api/admin/order/selectByCompany',
    selectCompanyUrl: '/api/admin/company/providers'
};

var goodsProviderId;    //店铺商品Id
var companyName;        //店铺名称
var companyCount;       //平台店铺总数
var goodsCount;         //店铺商品总数
var goodsCounts;        //平台商品总数
var sales = 0;              //店铺销售额

$().ready(function () {
    //获取店铺列表
    $.ajax(
        {
            url: Config.selectCompanyUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                data.data.map(function (company) {
                    categories.push('<option value="');
                    categories.push(company.companyId);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(company.companyName);
                    categories.push('</option>');
                });
                $("#goodsProviderId").html(categories.join(''));
                companyCount = data.data.length;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });

    $.ajax(
        {
            url: Config.selectGoodsUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                goodsCounts = data.data.goodsCount;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    getSales();
    getGoods();
    myData();
});

$("#goodsProviderId").change(function () {
    getSales();
    getGoods();
    myData();
});

function getSales() {
    goodsProviderId = $('#goodsProviderId option:selected').val();
    $.ajax(
        {
            url: Config.selectOrderAmount,
            type: "GET",
            data : {companyId:goodsProviderId},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                sales = data.data.sales;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
}

function getGoods() {
    goodsProviderId = $('#goodsProviderId option:selected').val();
    companyName = $('#goodsProviderId option:selected').text();
    $.ajax(
        {
            url: Config.selectGoodsUrl,
            type: "GET",
            data : {goodsProviderId:goodsProviderId},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                goodsCount = data.data.goodsCount;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
}

// echarts图表绘制函数
function myData() {
    var xA = [];
    var yA = [];
    var salesList = [];
    xA.push(companyName);
    yA.push(goodsCount);
    salesList.push(sales);
    var myChartpersonal = echarts.init(document.getElementById('personal'));
    var optionpersonal = {
        grid: {
            bottom: 80
        },
        tooltip : {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                animation: false,
                label: {
                    backgroundColor: '#505765'
                }
            }
        },
        legend: {
            data: ['商品数','销售额']
        },
        toolbox: {
            show : true,
            feature : {
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        grid: {
            y2: 140
        },
        xAxis: {
            data: xA
        },
        yAxis: {},
        series: [{
            name: '商品数',
            type: 'bar',
            data: yA,
        },{
            name: '销售额',
            type: 'bar',
            data: salesList,
        }]
    };
    var myChartmain = echarts.init(document.getElementById('main'));
    var zA = [];
    var sA = [];
    var qA = [];
    zA.push(companyCount);
    sA.push(goodsCounts);
    qA.push('平台店铺总数')
    var optionmain = {
        grid: {
            bottom: 80
        },
        tooltip : {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                animation: false,
                label: {
                    backgroundColor: '#505765'
                }
            }
        },
        legend: {
            data: ['平台店铺总数','平台商品总数']
        },
        toolbox: {
            show : true,
            feature : {
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        grid: {
            y2: 140
        },
        xAxis: {
            data: qA
        },
        yAxis: {},
        series: [{
            name: '平台店铺总数',
            type: 'bar',
            data: zA,
        },{
            name: '平台商品总数',
            type: 'bar',
            data: sA,
        }]
    };
    myChartpersonal.setOption(optionpersonal);
    myChartmain.setOption(optionmain);
};