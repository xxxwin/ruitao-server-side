/**
 *
 * Create by murong on 2018/5/14
 */

var config={
    selectUrl: '/api/admin/type/select',
    deleteUrl: '/api/admin/type/delete',
    selectIndustryUrl:'/api/admin/industry/selectAll',
    // selectIndustryUrl: '/api/admin/industry/select',
};

$(document).ready(function () {
    //给行业类型注入值
    $.ajax(
        {
            url: config.selectIndustryUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="" hassubinfo="true">所有行业</option>');
                data.data.map(function (industry) {
                    categories.push('<option value="');
                    categories.push(industry.id);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(industry.name);
                    categories.push('</option>');
                });
                $("#industrySelect").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    $.jgrid.defaults.styleUI="Bootstrap";

    $("#type_list").jqGrid({
        datatype:"json",
        url:config.selectUrl,
        height:450,
        autowidth:true,
        shrinkTofit:true,
        rowNum:12,
        colNames:["编号","所属行业","商家类型","备注","创建时间","操作"],
        colModel:[{
            name:"id",
            index:"id",
            sorttype:"int",
            authwidth:true,
            search:true
        },{
            name:"industryId",
            index:"industryId",
            autowidth:true,
            sortable: true,
            formatter:myformatter,
        },{
            name:"name",
            index:"name",
            autowidth:true,
            sortable: true,
        },{
            name:"remark",
            index:"remark",
            autowidth:true
        },{
            name: "ctime",
            index: "ctime",
            sortable: true,
        },{
            name: "operate",
            index: "operate",
            autowidth: true,
            sortable: false
        }],
        // celledit:true,
        // cellsubmit: 'remote',
        // cellurl: '/api/admin/company/cellEdit',
        pager: "#pager_type_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#type_list").jqGrid('navGrid',"#pager_type_list",{
        add: false, edit: false, del: false
    });

    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#type_list").setGridWidth(width);
    });

    $("#perform_search").click(function () {
        var postdata = $("#type_list").jqGrid('getGridParam', 'postData');
        postdata._search = true;
        postdata.name = $('#typeName').val();
        postdata.industryId =$('#industrySelect option:selected').val();
        jQuery("#type_list").trigger("reloadGrid", [{page: 1}]);
    });
});

function Delete(id) {
    bootbox.confirm({size:"small",message:"确认要删除id为"+id+"的这条数据吗?",callback: function (result) {
            if (result) {
                updateDialog("DELETE", config.deleteUrl + '/' + id);
                jQuery("#type_list").jqGrid('delRowData', id);
            }
        }
    });
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#type_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}

function dataWrapper() {
    var ids = jQuery("#type_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#type_list').jqGrid('getRowData', id);
        // href="/companytype/update/' + id + '"
        // var html = '<a class="J_men uItem" style="color:#f60;padding-right: 15px" onclick="dishUpdate('+id+')" data-index="0">编辑</a>';
        var html = '<a class="J_men uItem" style="color:#f60;padding-right: 15px" onclick="typeUpdate('+id+')" data-index="0">编辑</a>';
        html += "<a style='color:#f60' onclick='Delete(" + id + ")' >删除</a>";
        if(dataFromTheRow.ctime.indexOf("-")<0){
            dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        }
        jQuery("#type_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
        });
    }
}

function typeUpdate(id) {
    layer.open({
        title: ["商家类型修改","font-size:20px;"],
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:"/companytype/update/" + id,
    })
}
function myformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax(
        {
            url: "/api/admin/industry/selectByPk/" + rowObject.industryId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                name = data.data.name;
            }
        });
    return name;
}
