/*
-- Insert: rtshop.local_discount
-- Date: 2017-01-12 14:25
-- create: Shaka
*/
INSERT INTO `local_discount` (`discount_id`,`discount`,`score`,`remark`,`ctime`,`mtime`,`rstatus`,`limit_price`,`title`) VALUES (1,100,0,'不送积分。',1484202245500,1484202245500,0,0,'不打折');
INSERT INTO `local_discount` (`discount_id`,`discount`,`score`,`remark`,`ctime`,`mtime`,`rstatus`,`limit_price`,`title`) VALUES (2,98,10,'满100元送10积分。',1484201943440,1484202065419,0,10000,'9.8折');
INSERT INTO `local_discount` (`discount_id`,`discount`,`score`,`remark`,`ctime`,`mtime`,`rstatus`,`limit_price`,`title`) VALUES (3,95,25,'满100元送25积分。',1484201922169,1484202050362,0,10000,'9.5折');
INSERT INTO `local_discount` (`discount_id`,`discount`,`score`,`remark`,`ctime`,`mtime`,`rstatus`,`limit_price`,`title`) VALUES (4,90,50,'满100元送50积分。',1484201909419,1484202012431,0,10000,'9折');
INSERT INTO `local_discount` (`discount_id`,`discount`,`score`,`remark`,`ctime`,`mtime`,`rstatus`,`limit_price`,`title`) VALUES (5,85,75,'满100元送75积分。',1484201892431,1484201985368,0,10000,'8.5折');
INSERT INTO `local_discount` (`discount_id`,`discount`,`score`,`remark`,`ctime`,`mtime`,`rstatus`,`limit_price`,`title`) VALUES (6,80,100,'满100元送100积分。',1484201864158,1484201864158,0,10000,'8折');
