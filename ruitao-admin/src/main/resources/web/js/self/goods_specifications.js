/**
 * Created by Shaka on 12/21/16.
 */

var Config = {
    selectUrl: "/api/admin/goodsSpecifications/page",
    // updateUrl:"/api/admin/goodsSpecifications",
    // updateUrlForCellEdit: '/api/admin/order/cellEdit',
    selectExcelUrl: "/api/admin/orders/excel/list",
    deleteUrl:"/api/admin/goodsSpecificationsDelete"
};

$(document).ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        // postData: {payStatus: 1},
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "品牌名称", "备注","创建时间","操作"],
        colModel: [{
            name: "id",
            index: "id",
            sorttype: "int",
            width: 5,
            search: true,
            key: true
        }, {
            name: "name",
            index: "name",
            width: 15,
            search: true
        }, {
            name: "remark",
            index: "remark",
            width: 5
        }, {
            name: "ctime",
            index: "ctime",
            width: 12,
            sortable: false
        }, {
            name: "operate",
            index: "operate",
            width: 5
        }],
        multiselect: false,
        multiboxonly: true,
        pager: "#pager_data_list",
        viewrecords: true,
        hidegrid: false,
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "orderId"
        },
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#data_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        //var html = "<a style='color:#f60;padding-right: 15px' onclick='orderUpdate(" + id + ")'>详情</a>";
        // html += '<a style="color:#f60;padding-right: 15px" onclick="orderProfit(\'' + dataFromTheRow.orderSn + '\')" >返利详情</a>';
        // html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/api/admin/orders/excel/' + dataFromTheRow.orderSn + '" >订单导出</a>';
        var html = "<a style='color:#f60' onclick='Delete(" + id + ")' >删除</a>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html,ctime: dataFromTheRow.ctime});
    }
}
$("#perform_search").click(function () {
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.name = $("input[id=name]").val();
    jQuery("#data_list").trigger("reloadGrid",[{page: 1}]);
});