import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ruitaowang.admin.Application;

/**
 * Created by neal on 11/15/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TimeTest {
    @Test
    public void testNami() throws Exception {
        System.out.println(System.nanoTime());
    }
    @Test
    public static String getNextYearCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        Date date = new Date(System.currentTimeMillis());
        calendar.setTime(date);
        //下一周的今天
//        calendar.add(Calendar.WEEK_OF_YEAR, -1);
        calendar.add(Calendar.YEAR, -1);
        date = calendar.getTime();
        return date.toString();
    }
}
