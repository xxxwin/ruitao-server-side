package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ShoppingOrder;
import java.util.List;

public interface ShoppingOrderMapper {
    int insert(ShoppingOrder shoppingOrder);

    int deleteByPK(Long id);

    int updateByPK(ShoppingOrder shoppingOrder);

    ShoppingOrder selectByPK(Long id);

    long count(ShoppingOrder shoppingOrder);

    List<ShoppingOrder> select(ShoppingOrder shoppingOrder);
}