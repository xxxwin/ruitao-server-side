package com.ruitaowang.wap;

import java.util.List;

import com.ruitaowang.account.security.LYSecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Category;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CategoryService;
import com.ruitaowang.goods.service.CompanyService;


@RestController
public class APIWapCategoryController extends BaseController {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private CompanyService companyService;

    @RequestMapping(value = "/api/wap/categorys", method = RequestMethod.GET)
    public String list(Category category){
        List<Category> list;
        MsgOut o;
        category.setOrderBy(" id ");
        list = categoryService.select(category);
        o = MsgOut.success(list);
        
        return this.renderJson(o);
    }
    
    @RequestMapping(value = "/api/wap/category/{id}", method = RequestMethod.GET)
	public String one(@PathVariable("id") Long id) {
		MsgOut o;
		Category category =  categoryService.selectByPK(id);
		o = MsgOut.success(category);
		return this.renderJson(o);
	}

    @RequestMapping(value = "/api/wap/categorys", method = RequestMethod.POST)
    public String create(Category category){
        MsgOut o;
        LOGGER.debug(renderJson(category));
        categoryService.insert(category);
        o = MsgOut.success(category);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/categorys", method = RequestMethod.PUT)
    public String update(Category category){//@Valid
        categoryService.update(category);
        return this.renderJson(MsgOut.success(category));
    }

    @RequestMapping(value = "/api/wap/category/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id){
        categoryService.delete(id);
        return this.renderJson(MsgOut.success());
    }

    /**
     * 商家管理商品页面分类（所有）
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/companyCates", method = RequestMethod.GET)
    public String list(@RequestHeader(value="headId",required = false) String userId){
    	
    	if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company=companyService.selectByCompanyStaff(Long.valueOf(userId));
        
    	List<Category> cates = categoryService.selectByCompany(company.getCompanyId(),null);
    	return this.renderJson(MsgOut.success(cates));
    }

    /**
     * 带有companyId的商家管理商品页面分类（上架）
     * @return
     */
    @RequestMapping(value = "/api/wap/OnlineCates", method = RequestMethod.GET)
    public String onLinelist(Long companyId){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        List<Category> cates = categoryService.selectByCompany(companyId,1);
        return this.renderJson(MsgOut.success(cates));
    }

    /**
     * 商家管理商品页面分类（上架）
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/companyOnlineCates", method = RequestMethod.GET)
    public String onLinelist(@RequestHeader(value="headId",required = false) String userId){
    	
    	if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company=companyService.selectByCompanyStaff(Long.valueOf(userId));
        
    	List<Category> cates = categoryService.selectByCompany(company.getCompanyId(),1);
    	return this.renderJson(MsgOut.success(cates));
    }

    /**
     * 商家管理商品页面分类（仓库）
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/companyOfflineCates", method = RequestMethod.GET)
    public String offLinelist(@RequestHeader(value="headId",required = false) String userId){
    	
    	if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company=companyService.selectByCompanyStaff(Long.valueOf(userId));
    	
    	List<Category> cates = categoryService.selectByCompany(company.getCompanyId(),0);
    	return this.renderJson(MsgOut.success(cates));
    }

    /**
     * 用户查看商品分类
     * @param goodsProviderId
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/userOnlineCates", method = RequestMethod.GET)
    public String userOnlineCates(Long goodsProviderId, @RequestHeader(value="headId",required = false) String userId){
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        List<Category> cates = categoryService.selectByCompany(goodsProviderId,1);
        return this.renderJson(MsgOut.success(cates));
    }

}