package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.GoodsType;
import com.ruitaowang.core.domain.GoodsTypeCustom;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.GoodsTypeCustomService;
import com.ruitaowang.goods.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;

/**
 *
 * 商品类型信息
 */
@RestController
public class PageGoodsTypeController extends BaseController {

    @Autowired
    private GoodsTypeService goodsTypeService;

    @Autowired
    private GoodsTypeCustomService goodsTypeCustomService;


    @RequestMapping(value = {"/goodsType/update/{goodsTypeId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("goodsTypeId") Long goodsTypeId, Model model) {
        GoodsType goodsType = goodsTypeService.selectByPK(goodsTypeId);
        model.addAttribute("goodsType", goodsType);
        return new ModelAndView("web/goods_type_update");
    }

    @RequestMapping(value = {"/goodsTypeCustom/update/{id}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView goodsTypeCustomUpdate(@PathVariable("id")Long id ,Model model) {
        GoodsTypeCustom goodsTypeCustom = goodsTypeCustomService.selectByPK(id);
        model.addAttribute("goodsTypeCustom", goodsTypeCustom);
        return new ModelAndView("web/goods_type_custom_update");
    }

    @RequestMapping(value = {"/wsGoodsTypeCustom/update/{id}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView wsGoodsTypeCustomUpdate(@PathVariable("id")Long id ,Model model) {
        GoodsTypeCustom goodsTypeCustom = goodsTypeCustomService.selectByPK(id);
        model.addAttribute("goodsTypeCustom", goodsTypeCustom);
        return new ModelAndView("web/ws_goods_type_custom_update");
    }
}