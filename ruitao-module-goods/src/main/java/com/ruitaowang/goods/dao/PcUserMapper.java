package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.PcUser;
import java.util.List;

public interface PcUserMapper {
    int insert(PcUser pcUser);

    int deleteByPK(Long id);

    int updateByPK(PcUser pcUser);

    PcUser selectByPK(Long id);

    long count(PcUser pcUser);

    List<PcUser> select(PcUser pcUser);
}