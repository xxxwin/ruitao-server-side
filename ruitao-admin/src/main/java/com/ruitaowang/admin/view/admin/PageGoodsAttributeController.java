package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.GoodsAttribute;
import com.ruitaowang.core.domain.GoodsType;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.GoodsAttributeService;
import com.ruitaowang.goods.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;

/**
 *
 * 商品类型信息
 */
@RestController
public class PageGoodsAttributeController extends BaseController {

    @Autowired
    private GoodsAttributeService goodsAttributeService;


    @RequestMapping(value = {"/goodsAttribute/update/{goodsAttributeId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("goodsAttributeId") Long goodsTypeId, Model model) {
        GoodsAttribute goodsAttribute = goodsAttributeService.selectByPK(goodsTypeId);
        model.addAttribute("goodsAttribute", goodsAttribute);
        return new ModelAndView("web/goods_attribute_update");
    }

}