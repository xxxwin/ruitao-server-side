package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserOnline;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserOnlineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class UserOnlineService implements BaseService<UserOnline> {
    @Autowired
    private UserOnlineMapper UserOnlineMapper;

    @Override
    public UserOnline insert(UserOnline UserOnline) {
        Long time = System.currentTimeMillis();
        UserOnline.setCtime(time);
        UserOnline.setMtime(time);
        UserOnlineMapper.insert(UserOnline);
        return UserOnline;
    }

    @Override
    public int delete(Long id) {
        return UserOnlineMapper.deleteByPK(id);
    }

    public int deleteBySessionId(String sessionId) {
        UserOnline entity = new UserOnline();
        entity.setRemark(sessionId);
        List<UserOnline> list = select(entity);
        if(ObjectUtils.isEmpty(list)){
            return -1;
        }
        return delete(list.get(0).getId());
    }
    public int deleteByUserIdAndRoomId(long userId, long roomId) {
        UserOnline entity = new UserOnline();
        entity.setUserId(userId);
        entity.setRoomId(roomId);
        List<UserOnline> list = select(entity);
        if(ObjectUtils.isEmpty(list)){
            return -1;
        }
        return delete(list.get(0).getId());
    }

    public void updateByUserIdAndRoomId(long userId, long roomId, Byte rstatus) {
        UserOnline entity = new UserOnline();
        entity.setUserId(userId);
        entity.setRoomId(roomId);
        List<UserOnline> list = select(entity);
        if(ObjectUtils.isEmpty(list)){
            return;
        }
        entity = list.get(0);
        Long time = System.currentTimeMillis();
        entity.setMtime(time);
        entity.setRstatus(rstatus);
        this.update(entity);
    }

    @Override
    public UserOnline update(UserOnline UserOnline) {
        Long time = System.currentTimeMillis();
//        UserOnline.setMtime(time);
        UserOnlineMapper.updateByPK(UserOnline);
        return UserOnline;
    }

    @Override
    public UserOnline selectByPK(Long id) {

        return UserOnlineMapper.selectByPK(id);
    }

    @Override
    public List<UserOnline> select(UserOnline UserOnline) {
//        UserOnline.setRstatus((byte) 0);
        UserOnline.setOrderBy("rstatus asc ,mtime desc");
        return UserOnlineMapper.select(UserOnline);
    }

    public List<UserOnline> selectForPage(UserOnline UserOnline) {
//        UserOnline.setRstatus((byte) 0);
        UserOnline.setOrderBy("rstatus asc ,mtime desc");
        UserOnline.setRecords(UserOnlineMapper.count(UserOnline));
        return UserOnlineMapper.select(UserOnline);
    }

    public long count(long companyId){
        UserOnline where = new UserOnline();
        where.setRoomId(companyId);
        return UserOnlineMapper.count(where);
    }
}

