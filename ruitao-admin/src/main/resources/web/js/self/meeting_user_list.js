/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/meetingusers',
    updateUrl: '/api/meetingusers',
    deleteUrl: '/api/meetingusers',
    auditSignUpUrl: '/api/meetingusers/auditSignIn',
    auditCostUrl: '/api/meetingusers/auditCost',
    showDataUrl: '/api/meetingusers/showData',
    selectUrl: '/api/meetingusers'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl+"?meetingId="+$("#meetingId").val() ,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "姓名", "手机号", "公司名称",  "是/否付款", "是/否签到", "操作"],
        colModel: [{
            name: "meetingUserId",
            index: "meetingUserId",
            width: 15
        }, {
            name: "realName",
            index: "realName",
            width: 20
        }, {
            name: "phone",
            index: "phone",
            width: 20
        }, {
            name: "companyName",
            index: "companyName",
            width: 40
        }, {
            name: "locked",
            index: "locked",
            width: 20
        }, {
            name: "rstatus",
            index: "rstatus",
            width: 20
        }, {
            name: "operate",
            index: "operate",
            width: 30
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "meetingUserId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

    $('#buttonGoback').click(function() {
        history.go(-1);
        location.reload();
        return false;
    });
});
showData();
function showData() {
    $.ajax(
    {
        url: Config.showDataUrl,
        data: {meetingId: $("#meetingId").val()},
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data && data.type == "SUCCESS"){
                $("#showSignUp").text(data.data.mtime);
                $("#showSignIn").text(data.data.ctime);
            }else{
                toastr.error(data.msg);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络错误");
        }
    });
}

function auditSignUp(phone) {
    $.ajax(
    {
        url: Config.auditSignUpUrl,
        data: {phone: phone, rstatus: 1},
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data && data.type == "SUCCESS"){

                jQuery("#data_list").trigger("reloadGrid");
            }else{
                toastr.error(data.msg);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络错误");
        }
    });
}

function auditCost(phone) {
    $.ajax(
    {
        url: Config.auditCostUrl,
        data: {phone: phone, locked: true},
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data && data.type == "SUCCESS"){

            }else{
                toastr.error(data.msg);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络错误");
        }
    });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', ids[i]);
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="javascript:auditSignUp(' + dataFromTheRow.phone + ')" data-index="0">确认签到</a>';
            html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="javascript:auditCost(' + dataFromTheRow.phone + ')" data-index="1">确认付款</a>';
        var locked = '否';
        if(dataFromTheRow.locked == 'true') {
            locked = '是';
        }
        var rstatus = '否';
        if(dataFromTheRow.rstatus == 1){
            rstatus = '是';
        }
        jQuery("#data_list").jqGrid('setRowData', ids[i], {
            operate: html,
            locked: locked,
            rstatus: rstatus
        });
    }
}
setInterval(function () {
   showData();
}, 10000);
