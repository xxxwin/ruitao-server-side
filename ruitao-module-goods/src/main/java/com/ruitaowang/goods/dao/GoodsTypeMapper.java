package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsType;
import java.util.List;

public interface GoodsTypeMapper {
    int insert(GoodsType goodsType);

    int deleteByPK(Long typeId);

    int updateByPK(GoodsType goodsType);

    GoodsType selectByPK(Long typeId);

    long count(GoodsType goodsType);

    List<GoodsType> select(GoodsType goodsType);
}