/**
 * Created by xinchunting on 17-7-7.
 */
/**
 * Created by Administrator on 2017/4/18.
 */
/**
 * Created by neal on 11/22/16.
 */
var table2Y="http://static.ruitaowang.com/attached/file/20170713/20170713141409_351.jpg";
var table4Y="http://static.ruitaowang.com/attached/file/20170713/20170713141633_964.jpg";
var tablesY="http://static.ruitaowang.com/attached/file/20170713/20170713141652_920.jpg";

var table2N="http://static.ruitaowang.com/attached/file/20170713/20170713141819_395.jpg";
var table4N="http://static.ruitaowang.com/attached/file/20170713/20170713141803_686.jpg";
var tablesN="http://static.ruitaowang.com/attached/file/20170713/20170713141744_769.jpg";

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}

$().ready(function () {

    $.ajax({
        url: "/api/admin/dc/tableType",
        type: "GET",
        data:{companyId:parseInt(getQueryString("id")),page:1,rows:1000},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            data.data.forEach(function (item) {
                $("#show").append('<div class="row"><div class="col-sm-4 col-md-2" style="background: #179d82"><h4 style="color: #00E8D7">'+item.name+'</h4></div><div class="col-sm-4 col-md-2 '+item.id+'"></div></div><div class="row" id="'+item.id+'"></div><br>');
            });
            table();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    $('#buttonGoback').click(function () {
        history.go(-1);
        location.reload();
        return false;
    });
});
function table() {
    $.ajax({
        url: "/api/admin/dc/table",
        type: "GET",
        data:{companyId:parseInt(getQueryString("id")),page:1,rows:1000},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            data.data.forEach(function (item) {
                if (item.busy == 1){
                    $("#"+item.tableTypeId+"").append('<div class="col-sm-4 col-md-2" style="color: #fff; background: red;"><a href="#" class="thumbnail"><img src="http://static.ruitaowang.com/attached/file/20170707/20170707155932_405.jpg" class="'+item.tableTypeId+'" alt="通用的占位符缩略图"></a><br> <div class="text-center"><input type="hidden" name="id" class="form-control" value="' + item.id + '"><input type="hidden" name="'+item.id+'" class="form-control" value="' + item.busy + '"><H3>' + item.name + '</H3> </div><br><div class="text-center"> <button  class="btn btn-primary submitY" >使用</button> <button  class="btn btn-primary submitN" >空闲</button></div></div>');
                }else {
                    $("#"+item.tableTypeId+"").append('<div class="col-sm-4 col-md-2" style="color: #fff; background: blue;" ><a href="#" class="thumbnail"><img src="http://static.ruitaowang.com/attached/file/20170707/20170707155932_405.jpg" class="'+item.tableTypeId+'" alt="通用的占位符缩略图"></a><br> <div class="text-center"><input type="hidden" name="id" class="form-control" value="' + item.id + '"><input type="hidden" name="'+item.id+'" class="form-control" value="' + item.busy + '"><H3>' + item.name + '</H3> </div><br><div class="text-center"> <button  class="btn btn-primary submitY" >使用</button> <button  class="btn btn-primary submitN" >空闲</button></div></div>');
                }

            });
            tableType();
            queue();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
}
function queue() {
    $.ajax({
        url: "/api/admin/dc/console/queue",
        type: "GET",
        data:{companyId:parseInt(getQueryString("id")),page:1,rows:1000},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            data.data.forEach(function (item) {
                console.log(item.numMax);
               $("."+item.id+"").html("当前排队人数["+item.numMax+"]")
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
}
function tableType() {
    $.ajax({
        url: "/api/admin/dc/tableType",
        type: "GET",
        data:{companyId:parseInt(getQueryString("id")),page:1,rows:1000},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            data.data.forEach(function (item) {
             if (item.numMax == 1 || item.numMax == 2){
                 if ($("input[name="+item.id+"]").val() == 0) {
                     $("." + item.id + "").attr('src', table2Y)
                 }else {
                     $("."+item.id+"").attr('src',table2N)
                 }

             }
                if (item.numMax == 3 || item.numMax == 4){
                    if ($("input[name="+item.id+"]").val() == 0) {
                        $("." + item.id + "").attr('src', table4Y)
                    }else {
                        $("."+item.id+"").attr('src',table4N)
                    }                }
                if (item.numMax > 4){
                    if ($("input[name="+item.id+"]").val() == 0) {
                        $("." + item.id + "").attr('src', tablesY)
                    }else {
                        $("."+item.id+"").attr('src',tablesN)
                    }                }
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
}



$('.row').on("click",".submitY",function () {
    $(this).parent().parent().css({
        'color': '#fff',
        'background': 'red'
    });
    $.ajax({
        url: "/api/admin/dc/table/consoleY",
        type: "PUT",
        data:{id:$(this).parent().prev().prev().find("input[name=id]").val()},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            toastr.success(data.msg, data.title);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
});
$('.row').on("click",".submitN",function () {
    $(this).parent().parent().css({
        'color': '#fff',
        'background': 'blue'
    });
    $.ajax({
        url: "/api/admin/dc/table/consoleN",
        type: "PUT",
        data:{id:$(this).parent().prev().prev().find("input[name=id]").val()},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            toastr.success(data.msg, data.title);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
});






