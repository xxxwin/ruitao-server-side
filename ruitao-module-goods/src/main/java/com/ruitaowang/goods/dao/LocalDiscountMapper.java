package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LocalDiscount;
import java.util.List;

public interface LocalDiscountMapper {
    int insert(LocalDiscount localDiscount);

    int deleteByPK(Long discountId);

    int updateByPK(LocalDiscount localDiscount);

    LocalDiscount selectByPK(Long discountId);

    long count(LocalDiscount localDiscount);

    List<LocalDiscount> select(LocalDiscount localDiscount);
}