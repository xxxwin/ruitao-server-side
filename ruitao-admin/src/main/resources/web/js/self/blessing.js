/**
 * Created by Shaka on 12/21/16.
 */

var Config = {
    selectUrl: '/api/wap/msgs/zn/get'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#blessing_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "用户姓名", "手机号吗","公司名称","公司职位","祝福分类", "祝福语","操作"],
        colModel: [{
            name: "msgId",
            index: "msgId",
            sorttype: "int",
            autowidth: true,
            search: true
        }, {
            name: "fromUserText",
            index: "fromUserText",
            autowidth: true
        },{
            name: "toUserId",
            index: "toUserId",
            autowidth: true
        },{
            name: "toUserText",
            index: "toUserText",
            autowidth: true
        },{
            name: "toUserAvatar",
            index: "toUserAvatar",
            autowidth: true
        },{
            name: "usurpScreenNum",
            index: "usurpScreenNum",
            editoptions: {value: "1:梦想;2:许愿;3:新年祝词;4:祝福语"},
            editable: true,
            edittype: 'select',
            formatter: 'select',
            autowidth: true
        },{
            name: "msgContent",
            index: "msgContent",
            autowidth: true
        },{
            name: "operate",
            index: "operate",
            autowidth: true
        }],
        pager: "#pager_company_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "msgId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
           dataWrapper();
        }
    });
    jQuery("#blessing_list").jqGrid('navGrid', '#pager_company_list', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#blessing_list").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#blessing_list").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#blessing_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#blessing_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
      //  var dataFromTheRow = jQuery('#blessing_list').jqGrid ('getRowData', id);
     //   dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/blessing/update/' + id + '" data-index="0">详细</a>';
        jQuery("#blessing_list").jqGrid('setRowData', ids[i], {operate: html});
    }
}
