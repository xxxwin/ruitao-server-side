package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.MeetingUser;
import com.ruitaowang.core.domain.MeetingUserLink;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.MeetingUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class MeetingUserService implements BaseService<MeetingUser> {
    @Autowired
    private MeetingUserMapper meetingUserMapper;
    @Autowired
    private MeetingUserLinkService meetingUserLinkService;
    @Override
    public MeetingUser insert(MeetingUser meetingUser) {
        Long time = System.currentTimeMillis();
        meetingUser.setCtime(time);
        meetingUser.setMtime(time);
        meetingUserMapper.insert(meetingUser);
        return meetingUser;
    }

    @Override
    public int delete(Long id) {
        MeetingUser meetingUser = new MeetingUser();
        meetingUser.setMeetingUserId(id);
        meetingUser.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        meetingUser.setMtime(time);
        return meetingUserMapper.updateByPK(meetingUser);
    }

    @Override
    public MeetingUser update(MeetingUser meetingUser) {
        Long time = System.currentTimeMillis();
        meetingUser.setMtime(time);
        meetingUserMapper.updateByPK(meetingUser);
        return meetingUser;
    }

    @Override
    public MeetingUser selectByPK(Long id) {
        return meetingUserMapper.selectByPK(id);
    }

    @Override
    public List<MeetingUser> select(MeetingUser meetingUser) {
//        meetingUser.setRstatus((byte) 0);
        return meetingUserMapper.select(meetingUser);
    }

    public List<MeetingUser> selectForPage(MeetingUser meetingUser) {
//        meetingUser.setRstatus((byte) 0);
        meetingUser.setRecords(meetingUserMapper.count(meetingUser));
        return meetingUserMapper.select(meetingUser);
    }

    public MeetingUser selectByPhone(String phone) {
        MeetingUser meetingUser = new MeetingUser();
        meetingUser.setPhone(phone);
        List<MeetingUser> meetingUsers = meetingUserMapper.select(meetingUser);
        if(ObjectUtils.isEmpty(meetingUsers)){
            return null;
        }
        return meetingUsers.get(0);
    }

    public MeetingUser checkUserSignUp(String phone) {
        MeetingUser meetingUser = selectByPhone(phone);
        if (ObjectUtils.isEmpty(meetingUser)) {//没有录入信息
            return null;
        }
        MeetingUserLink meetingUserLink = meetingUserLinkService.selectByPhone(phone);
        if (ObjectUtils.isEmpty(meetingUserLink)) {//没有报名此会议
            return null;
        }
        return meetingUser;
    }
}
