package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.RoleResourceLink;

import java.util.List;

public interface RoleResourceLinkAdMapper {

    int deleteByRoleId(Long roleId);

    List<RoleResourceLink> selectBatch(List<Long> roleId);
}