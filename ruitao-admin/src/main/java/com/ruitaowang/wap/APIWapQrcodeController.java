package com.ruitaowang.wap;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.QRCodeRecord;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.QRCodeRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class APIWapQrcodeController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private QRCodeRecordService qRCodeRecordService;

    /**
     * 升级高级人
     * @param qRCodeRecor
     * @return
     */
    @RequestMapping(value ="/api/wap/payCount", method = RequestMethod.GET)
    public String payCount(QRCodeRecord qRCodeRecor, @RequestHeader(value = "headId", required = false) String userId){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user) && ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Map map = new HashMap();
        Long payCount = qRCodeRecordService.payCount(qRCodeRecor);
        String afterId = "H00" + (payCount + 1);
        map.put("payCount",payCount);
        map.put("afterCount",afterId);
        MsgOut o = MsgOut.success(map);
        return this.renderJson(o);
    }

}
