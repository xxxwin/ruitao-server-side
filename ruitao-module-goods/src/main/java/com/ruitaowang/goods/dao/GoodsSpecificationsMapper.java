package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsSpecifications;
import com.ruitaowang.core.domain.GoodsTypeCustom;

import java.util.List;

public interface GoodsSpecificationsMapper {
    int insert(GoodsSpecifications goodsSpecifications);

    int deleteByPK(Long id);

    int updateByPK(GoodsSpecifications goodsSpecifications);

    GoodsSpecifications selectByPK(Long id);

    long count(GoodsSpecifications goodsSpecifications);

    List<GoodsSpecifications> select(GoodsSpecifications goodsSpecifications);
}