package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.GoodsSpecifications;
import com.ruitaowang.core.domain.GoodsTypeCustom;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsSpecificationsMapper;
import com.ruitaowang.goods.dao.GoodsTypeCustomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsSpecificationsService implements BaseService<GoodsSpecifications> {

    @Autowired
    private GoodsSpecificationsMapper  goodsSpecificationsMapper;

    @Override
    public GoodsSpecifications insert(GoodsSpecifications goodsSpecifications) {
        Long time = System.currentTimeMillis();
        goodsSpecifications.setCtime(time);
        goodsSpecifications.setMtime(time);
        goodsSpecificationsMapper.insert(goodsSpecifications);
        return goodsSpecifications;
    }

    @Override
    public int delete(Long id) {
        GoodsSpecifications goodsSpecifications = new GoodsSpecifications();
        goodsSpecifications.setId(id);
        goodsSpecifications.setStatus((byte) 1);
        long time = System.currentTimeMillis();
        goodsSpecifications.setMtime(time);
        return goodsSpecificationsMapper.updateByPK(goodsSpecifications);
    }

    @Override
    public GoodsSpecifications update(GoodsSpecifications goodsSpecifications) {
        Long time = System.currentTimeMillis();
        goodsSpecifications.setMtime(time);
        goodsSpecificationsMapper.updateByPK(goodsSpecifications);
        return goodsSpecifications;
    }

    @Override
    public GoodsSpecifications selectByPK(Long id) {

        return goodsSpecificationsMapper.selectByPK(id);
    }

    @Override
    public List<GoodsSpecifications> select(GoodsSpecifications goodsSpecifications) {
        goodsSpecifications.setStatus((byte) 0);
        return goodsSpecificationsMapper.select(goodsSpecifications);
    }

    public List<GoodsSpecifications> selectForPage(GoodsSpecifications goodsSpecifications) {
        goodsSpecifications.setStatus((byte) 0);
        goodsSpecifications.setRecords(goodsSpecificationsMapper.count(goodsSpecifications));
        return goodsSpecificationsMapper.select(goodsSpecifications);
    }
}
