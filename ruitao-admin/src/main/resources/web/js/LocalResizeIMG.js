    /**
     * 获得base64
     * @param {Object} obj
     * @param {Number} [obj.width] 图片需要压缩的宽度，高度会跟随调整
     * @param {Number} [obj.quality=0.8] 压缩质量，不压缩为1
     * @param {Function} [obj.before(this, blob, file)] 处理前函数,this指向的是input:file
     * @param {Function} obj.success(obj) 处理后函数
     * @example
     *
     */
    $.fn.localResizeIMG = function (obj) {
        var angle = '';
        this.on('change', function () {
            var file = this.files[0];
            var URL = window.URL || window.webkitURL;
            var blob = URL.createObjectURL(file);
            input=this;
            //console.log(-2, file)
            //console.log(-1, blob)

            // $("#debug").append("1 " + file);
            // $("#debug").append("2 " + blob);
            // var BinaryAjax = window.BinaryAjax || '',
            //     EXIF = window.EXIF || '';
            // if (BinaryAjax && EXIF) {
            //     console.log(0, BinaryAjax, EXIF);
            //     $("#debug").append("3 " + BinaryAjax);
            //     $("#debug").append("4 " + EXIF);
            //     // get photo orientation and set angle
            //     BinaryAjax(blob, function(o) {
            //         var oExif = EXIF.readFromBinaryFile(o.binaryResponse),
            //             orientation = oExif.Orientation;
            //
            //         switch(orientation) {
            //             case 8:
            //                 angle = radians('90deg');
            //                 break;
            //             case 3:
            //                 angle = radians('180deg');
            //                 break;
            //             case 6:
            //                 angle = radians('270deg');
            //                 break;
            //         }
            //     });
            // }

            // 执行前函数
            if($.isFunction(obj.before)) { obj.before(this, blob, file) };

            _create(blob, file);
            this.value = '';   // 清空临时数据
        });

        // function radians(angle) {
        //     if (typeof angle == 'number') return angle;
        //     return {
        //         rad: function(z) {
        //             return z;
        //         },
        //         deg: function(z) {
        //             return Math.PI / 180 * z;
        //         }
        //     }[String(angle).match(/[a-z]+$/)[0] || 'rad'](parseFloat(angle));
        // }

        /**
         * 生成base64
         * @param blob 通过file获得的二进制
         */
        function _create(blob) {
            var img = new Image();
            img.src = blob;

            //console.log(1, img.width, img.height);
            // $("#debug").append("5 " + img.width + " , " + img.height);
            img.onload = function () {
                var _this = this;

                //生成比例
                var w = _this.width,
                    h = _this.width,
                    scale = w / h;
                console.log(2, w, h, scale);
                // $("#debug").append("6 " + w + " , " + h);
                w = obj.width || w;
                h = w / scale;
                //console.log(3, w, h);
                // $("#debug").append("7 " + w + " , " + h);

                //生成canvas
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                $(canvas).attr({width : w, height : h});
                ctx.drawImage(_this, 0, 0, w, h);

                /**
                 * 生成base64
                 * 兼容修复移动设备需要引入mobileBUGFix.js
                 */
                var base64 = canvas.toDataURL('image/jpeg', obj.quality || 0.8 );

                //console.log(base64);

                // 修复IOS
                //if( navigator.userAgent.match(/iphone/i) ) {
                //    var mpImg = new MegaPixImage(img);
                //    // $("#debug").append("8 " + mpImg + " , " + "\n");
                //    mpImg.render(canvas, { maxWidth: w, maxHeight: h, quality: obj.quality || 0.8, orientation: 0 });
                //    base64 = canvas.toDataURL('image/jpeg', obj.quality || 0.8 );
                //}
                //
                //// 修复android
                //if( navigator.userAgent.match(/Android/i) ) {
                //    var encoder = new JPEGEncoder();
                //    // $("#debug").append("9 " + encoder + " , " + "\n");
                //    base64 = encoder.encode(ctx.getImageData(0,0,w,h), obj.quality * 100 || 80 );
                //}

                // 生成结果
                var result = {
                    blob: blob,
                    base64 : base64,
                    clearBase64: base64.substr( base64.indexOf(',') + 1 )
                };

                // 执行后函数
                obj.success(result);
            };
        }
    };


    // 例子
/*
    $('input:file').localResizeIMG({
        width: 100,
        quality: 0.1,
        //before: function (_this, blob) {},
        success: function (result) {
            var img = new Image();
            img.src = result.base64;

            $('body').append(img);
            console.log(result);
        }
    });
*/
