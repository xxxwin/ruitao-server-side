package com.ruitaowang.core.domain;

public class Wallet extends Page {
    /**
     *  主键,用户编号,所属表字段为wallet.user_id
     */
    private Long userId;
    /**
     *  用户姓名,所属表字段为wallet.remark
     */
    private String userName;

    /**
     *  剩余积分,所属表字段为wallet.user_score
     */
    private Integer userScore;

    /**
     *  余额,所属表字段为wallet.user_balance
     */
    private Integer userBalance;

    /**
     *  备注,所属表字段为wallet.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为wallet.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为wallet.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为wallet.rstatus
     */
    private Byte rstatus;

    /**
     *  总额,所属表字段为wallet.user_amount
     */
    private Integer userAmount;

    /**
     *  总积分,所属表字段为wallet.user_score_total
     */
    private Integer userScoreTotal;

    /**
     *  历史余额,所属表字段为wallet.history_balance
     */
    private Integer historyBalance;

    /**
     *  历史积分,所属表字段为wallet.history_score
     */
    private Integer historyScore;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getUserScore() {
        return userScore;
    }

    public void setUserScore(Integer userScore) {
        this.userScore = userScore;
    }

    public Integer getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(Integer userBalance) {
        this.userBalance = userBalance;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getUserAmount() {
        return userAmount;
    }

    public void setUserAmount(Integer userAmount) {
        this.userAmount = userAmount;
    }

    public Integer getUserScoreTotal() {
        return userScoreTotal;
    }

    public void setUserScoreTotal(Integer userScoreTotal) {
        this.userScoreTotal = userScoreTotal;
    }

    public Integer getHistoryBalance() {
        return historyBalance;
    }

    public void setHistoryBalance(Integer historyBalance) {
        this.historyBalance = historyBalance;
    }

    public Integer getHistoryScore() {
        return historyScore;
    }

    public void setHistoryScore(Integer historyScore) {
        this.historyScore = historyScore;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}