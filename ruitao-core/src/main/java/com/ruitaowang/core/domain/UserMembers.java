package com.ruitaowang.core.domain;

public class UserMembers extends Page {
    /**
     *  ,所属表字段为user_members.member_id
     */
    private Long memberId;

    /**
     *  主键,所属表字段为user_members.user_id
     */
    private Long userId;

    /**
     *  备注,所属表字段为user_members.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为user_members.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为user_members.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为user_members.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为user_members.start_time
     */
    private String startTime;

    /**
     *  ,所属表字段为user_members.end_time
     */
    private String endTime;

    /**
     *  消费金额,所属表字段为user_members.consumer
     */
    private Long consumer;

    /**
     *  ,所属表字段为user_members.comsumer_id
     */
    private Long comsumerId;

    /**
     *  0 龙蛙会员
1 快转·微传媒会员
2 扫码点餐会员,所属表字段为user_members.member_type
     */
    private Byte memberType;

    /**
     *  ,所属表字段为permanent
     */
    private Byte permanent;

    public Byte getPermanent() {
        return permanent;
    }

    public void setPermanent(Byte permanent) {
        this.permanent = permanent;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime == null ? null : startTime.trim();
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime == null ? null : endTime.trim();
    }

    public Long getConsumer() {
        return consumer;
    }

    public void setConsumer(Long consumer) {
        this.consumer = consumer;
    }

    public Long getComsumerId() {
        return comsumerId;
    }

    public void setComsumerId(Long comsumerId) {
        this.comsumerId = comsumerId;
    }

    public Byte getMemberType() {
        return memberType;
    }

    public void setMemberType(Byte memberType) {
        this.memberType = memberType;
    }
}