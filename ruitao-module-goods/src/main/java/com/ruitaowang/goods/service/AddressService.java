package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Address;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.AddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class AddressService implements BaseService<Address> {
    @Autowired
    private AddressMapper addressMapper;

    @Override
    public Address insert(Address address) {
        Long time = System.currentTimeMillis();
        address.setCtime(time);
        address.setMtime(time);
        addressMapper.insert(address);
        return address;
    }

    @Override
    public int delete(Long id) {
        Address address = new Address();
        address.setAddressId(id);
        address.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        address.setMtime(time);
        return addressMapper.updateByPK(address);
    }

    @Override
    public Address update(Address address) {
        Long time = System.currentTimeMillis();
        address.setMtime(time);
        addressMapper.updateByPK(address);
        return address;
    }
    public int updateBatchForDefault(Long userId) {
        Long time = System.currentTimeMillis();
        Address address = new Address();
        address.setRstatus((byte) 0);
        address.setUserId(userId);
        List<Address> addresses = addressMapper.select(address);
        if (addresses.size()>0){
            for (Address a : addresses){
                a.setDefaultt((byte) 0);
                addressMapper.updateByPK(a);
            }
        }
        return 0;
    }
    @Override
    public Address selectByPK(Long id) {
        return addressMapper.selectByPK(id);
    }

    @Override
    public List<Address> select(Address address) {
        address.setRstatus((byte) 0);
        return addressMapper.select(address);
    }

    public List<Address> selectForPage(Address address) {
        address.setRstatus((byte) 0);
        address.setRecords(addressMapper.count(address));
        return addressMapper.select(address);
    }

}
