
var Config = {
    updateUrl: '/api/admin/categories/update',
};

$().ready(function () {

    //选择服务器
    if($("#levelMap").val() == "1" || $("#levelMap").val() == "2" || $("#levelMap").val() == "3"){
        $("#level").val($("#levelMap").val());
    }

    $('#submitCateUpdate').click(function () {

        $("#ajaxform").validate();
        //如果验证通过
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url: Config.updateUrl,
                    type: "PUT",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});