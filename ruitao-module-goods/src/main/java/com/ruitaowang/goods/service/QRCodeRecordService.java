package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.QRCodeRecord;
import com.ruitaowang.core.domain.QRCodeRecordVO;
import com.ruitaowang.core.domain.Relations;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.QRCodeRecordAdMapper;
import com.ruitaowang.goods.dao.QRCodeRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class QRCodeRecordService implements BaseService<QRCodeRecord>{

    @Autowired
    private QRCodeRecordMapper qrCodeRecordMapper;
    @Autowired
    private RelationsService relationsService;
    @Autowired
    private QRCodeRecordAdMapper qrCodeRecordAdMapper;

    @Override
    public QRCodeRecord insert(QRCodeRecord qrCodeRecord) {
        Long time = System.currentTimeMillis();
        qrCodeRecord.setCtime(time);
        qrCodeRecord.setMtime(time);
        qrCodeRecordMapper.insert(qrCodeRecord);
        return qrCodeRecord;
    }

    @Override
    public int delete(Long id) {
        QRCodeRecord qrCodeRecord = new QRCodeRecord();
        qrCodeRecord.setId(id);
        qrCodeRecord.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        qrCodeRecord.setMtime(time);
        return qrCodeRecordMapper.updateByPK(qrCodeRecord);
    }

    @Override
    public QRCodeRecord update(QRCodeRecord qrCodeRecord) {
        Long time = System.currentTimeMillis();
        qrCodeRecord.setMtime(time);
        qrCodeRecordMapper.updateByPK(qrCodeRecord);
        return qrCodeRecord;
    }

    @Override
    public QRCodeRecord selectByPK(Long id) {

        return qrCodeRecordMapper.selectByPK(id);
    }

    @Override
    public List<QRCodeRecord> select(QRCodeRecord qrCodeRecord) {
        qrCodeRecord.setRstatus((byte) 0);
        return qrCodeRecordMapper.select(qrCodeRecord);
    }

    public List<QRCodeRecord> selectForPage(QRCodeRecord qrCodeRecord) {
        qrCodeRecord.setRstatus((byte) 0);
        qrCodeRecord.setRecords(qrCodeRecordMapper.count(qrCodeRecord));
        return qrCodeRecordMapper.select(qrCodeRecord);
    }
    public List<QRCodeRecord> selectForUserId(Long userId) {
        QRCodeRecord qrCodeRecord=new QRCodeRecord();
        qrCodeRecord.setUserId(userId);
        qrCodeRecord.setRstatus((byte) 0);
        return qrCodeRecordMapper.select(qrCodeRecord);
    }
    public QRCodeRecord setQRCodeRecord(int price,long userId){

        QRCodeRecord qrCodeRecord= new QRCodeRecord();
        qrCodeRecord.setUserId(userId);
        qrCodeRecord.setRstatus((byte) 0);
        List<QRCodeRecord> qrCodeRecords=qrCodeRecordMapper.select(qrCodeRecord);
        if (!ObjectUtils.isEmpty(qrCodeRecords)){
            qrCodeRecords.get(0).setBuyStatus((byte)2);
            qrCodeRecordMapper.updateByPK(qrCodeRecords.get(0));
            return qrCodeRecords.get(0);
        }
        qrCodeRecord.setBuyStatus((byte)2);
        qrCodeRecord.setPrice(price);

        Long time = System.currentTimeMillis();
        qrCodeRecord.setCtime(time);
        qrCodeRecord.setMtime(time);
        qrCodeRecordMapper.insert(qrCodeRecord);
        return qrCodeRecord;
    }

    public boolean buyQRCodeRecord(long userId,long uesrId){
        QRCodeRecord qrCodeRecord= new QRCodeRecord();
        qrCodeRecord.setUserId(userId);
        qrCodeRecord.setBuyStatus((byte)2);
        List<QRCodeRecord> qrCodeRecords=qrCodeRecordMapper.select(qrCodeRecord);
        if (ObjectUtils.isEmpty(qrCodeRecords)){
            return false;
        }
        List<Long> ids=relationsService.selectAllParents(uesrId);
        for (int i=0;i<ids.size();i++){
            if (ids.get(i) == userId){
                Relations relations=relationsService.selectByPK(ids.get(i+1));
                if (ObjectUtils.isEmpty(relations)) {
                    return false;
                }
                if (relations.getCtime() > qrCodeRecords.get(0).getCtime() || relations.getCtime() == qrCodeRecords.get(0).getCtime()){
                    return true;
                }else {
                    //这个是以前的一个规则，说起来比较麻烦，总之就是以前方便多锁定下级而设置，现在已取消
//                    return false;
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 高级合伙人人数
     * @param qRCodeRecor
     * @return
     */
    public Long payCount(QRCodeRecord qRCodeRecor){
        qRCodeRecor.setBuyStatus((byte)2);
        qRCodeRecor.setRstatus((byte)0);
        return qrCodeRecordMapper.count(qRCodeRecor);
    }

    public List<QRCodeRecordVO> selectForBh(QRCodeRecord qRCodeRecor){
        return qrCodeRecordAdMapper.selectForBh(qRCodeRecor);
    }
}
