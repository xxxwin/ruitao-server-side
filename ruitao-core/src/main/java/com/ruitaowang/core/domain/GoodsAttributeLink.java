package com.ruitaowang.core.domain;

public class GoodsAttributeLink extends Page {
    /**
     *  ,所属表字段为goods_attribute_link.id
     */
    private Long id;

    /**
     *  商品编号,所属表字段为goods_attribute_link.goods_id
     */
    private Long goodsId;

    /**
     *  属性编号,所属表字段为goods_attribute_link.attr_id
     */
    private Long attrId;

    /**
     *  额外加的钱,所属表字段为goods_attribute_link.extra_price
     */
    private Integer extraPrice;

    /**
     *  1， 隐藏不显示
2， 前端显示,所属表字段为goods_attribute_link.attr_value
     */
    private String attrValue;

    /**
     *  创建时间,所属表字段为goods_attribute_link.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为goods_attribute_link.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常，1 删除,所属表字段为goods_attribute_link.rstatus
     */
    private Byte rstatus;

    /**
     *  此属性拥有的商品数量,所属表字段为goods_attribute_link.attr_count
     */
    private Integer attrCount;

    /**
     *  是否显示在 商品详情页的“请选择”
0 不显示
1 显示,所属表字段为goods_attribute_link.attr_show
     */
    private Byte attrShow;

    /**
     *  商品属性名,所属表字段为goods_attribute_link.attr_name
     */
    private String attrName;

    /**
     *  进货价增量,所属表字段为goods_attribute_link.real_extra_price
     */
    private Integer realExtraPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getAttrId() {
        return attrId;
    }

    public void setAttrId(Long attrId) {
        this.attrId = attrId;
    }

    public Integer getExtraPrice() {
        return extraPrice;
    }

    public void setExtraPrice(Integer extraPrice) {
        this.extraPrice = extraPrice;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue == null ? null : attrValue.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getAttrCount() {
        return attrCount;
    }

    public void setAttrCount(Integer attrCount) {
        this.attrCount = attrCount;
    }

    public Byte getAttrShow() {
        return attrShow;
    }

    public void setAttrShow(Byte attrShow) {
        this.attrShow = attrShow;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName == null ? null : attrName.trim();
    }

    public Integer getRealExtraPrice() {
        return realExtraPrice;
    }

    public void setRealExtraPrice(Integer realExtraPrice) {
        this.realExtraPrice = realExtraPrice;
    }
}