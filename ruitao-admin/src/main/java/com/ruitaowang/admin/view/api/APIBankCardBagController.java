package com.ruitaowang.admin.view.api;

import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class APIBankCardBagController extends BaseController{


    @Autowired
    private BankCardBagService bankCardBagService;
    @Autowired
    private VerifyCodeService verifyCodeService;

    @RequestMapping("/api/wap/bankCardBag")
    public String list(BankCardBag bankCardBag,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        bankCardBag.setUserId(Long.parseLong(userId));
        MsgOut o = MsgOut.success(bankCardBagService.select(bankCardBag));
        return this.renderJson(o);
    }
    @RequestMapping("/api/wap/bankCardBag/page")
    public String listForPage(BankCardBag bankCardBag, int page,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        bankCardBag.setPage(page);
        if (!ObjectUtils.isEmpty(bankCardBag.getSidx())) {
            bankCardBag.setOrderBy(bankCardBag.getSidx() + " " + bankCardBag.getSord() + "," + bankCardBag.getOrderBy());
        }
        bankCardBag.setUserId(Long.parseLong(userId));
        MsgOut o = MsgOut.success(bankCardBagService.selectForPage(bankCardBag));
        o.setRecords(bankCardBag.getRecords());
        o.setTotal(bankCardBag.getTotal());
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/wap/bankCardBag", method = RequestMethod.POST)
    public String create(BankCardBag bankCardBag,String checkcode,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        //效验验证码
        Verifycode v = new Verifycode();
        v.setMobile(bankCardBag.getBankUserPhone());
        List<Verifycode> verifycodes = verifyCodeService.select(v);
        if (ObjectUtils.isEmpty(verifycodes)) {
            return this.renderJson(MsgOut.error("请输入验证码！"));
        }
        v = verifycodes.get(0);
        //检测全局缓存中的code与传过来的code是否匹配
        if (!checkcode.equals(v.getVerifycode())) {
            return this.renderJson(MsgOut.error("验证码错误！"));
        }
        BankCardBag bcb=new BankCardBag();
        bcb.setUserId(Long.parseLong(userId));
        bcb.setBankId(bankCardBag.getBankId());
        List<BankCardBag> bankCardBags=bankCardBagService.select(bcb);
        if (!ObjectUtils.isEmpty(bankCardBags)){
            return this.renderJson(MsgOut.error("该银行卡已经提交过绑定"));
        }
        bankCardBag.setUserId(Long.parseLong(userId));
        bankCardBagService.insert(bankCardBag);
        MsgOut o = MsgOut.success(bankCardBag);
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/wap/bankCardBag", method = RequestMethod.PUT)
    public String update(BankCardBag bankCardBag,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        bankCardBagService.update(bankCardBag);
        MsgOut o = MsgOut.success(bankCardBag);
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/wap/bankCardBag/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(bankCardBagService.delete(id));
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/wap/bankCardBag/{id}", method = RequestMethod.GET)
    public String wapBankCardBagId(@PathVariable("id")Long  id) {
        MsgOut o = MsgOut.success(bankCardBagService.selectByPK(id));
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/admin/bankCardBag/{id}", method = RequestMethod.GET)
    public String adminBankCardBagId(@PathVariable("id")Long  id) {
        List<BankCardBag> outList=new ArrayList<>();
        outList.add(bankCardBagService.selectByPK(id));
        MsgOut o = MsgOut.success(outList);
        return this.renderJson(o);
    }
}
