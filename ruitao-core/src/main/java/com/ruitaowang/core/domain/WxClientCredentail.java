package com.ruitaowang.core.domain;

public class WxClientCredentail extends Page {
    /**
     *  ,所属表字段为wx_client_credentail.id
     */
    private Long id;

    /**
     *  ,所属表字段为wx_client_credentail.access_token
     */
    private String accessToken;

    /**
     *  ,所属表字段为wx_client_credentail.expires_in
     */
    private Integer expiresIn;

    /**
     *  ,所属表字段为wx_client_credentail.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为wx_client_credentail.mtime
     */
    private Long mtime;

    /**
     *  ,所属表字段为wx_client_credentail.rstatus
     */
    private Byte rstatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken == null ? null : accessToken.trim();
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}