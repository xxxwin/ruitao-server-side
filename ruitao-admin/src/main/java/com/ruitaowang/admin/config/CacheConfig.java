//package com.ruitaowang.admin.config;
//
//import com.fasterxml.jackson.annotation.JsonAutoDetect;
//import com.fasterxml.jackson.annotation.PropertyAccessor;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.ruitaowang.core.service.queue.RedisMessagePublisher;
//import com.ruitaowang.core.service.queue.RedisMessageSubscriber;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.cache.CacheManager;
//import org.springframework.cache.annotation.CachingConfigurerSupport;
//import org.springframework.cache.annotation.EnableCaching;
//import org.springframework.cache.interceptor.KeyGenerator;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.cache.RedisCacheManager;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.listener.ChannelTopic;
//import org.springframework.data.redis.listener.RedisMessageListenerContainer;
//import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
//import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
///**
// * Created by neal on 11/15/16.
// */
//@Configuration
//@EnableCaching
//public class CacheConfig extends CachingConfigurerSupport{
//    public static final Logger LOGGER = LoggerFactory.getLogger(CacheConfig.class);
//    @Value("${spring.redis.host}")
//    private String redisHostName = "localhost";
//    @Value("${spring.redis.port}")
//    private  int redisPort = 6379;
//    @Value("${spring.redis.password}")
//    private String redisPasswd = "******";
//
//    @Bean
//    public JedisConnectionFactory jedisConnectionFactory() {
//        JedisConnectionFactory factory = new JedisConnectionFactory();
//        factory.setHostName(redisHostName);
//        factory.setPort(redisPort);
//        factory.setPassword(redisPasswd);
//        factory.setUsePool(true);
//        return factory;
//    }
//
//    @Bean
//    public KeyGenerator keyGenerator() {
//        return (o, method, objects) -> {
//            // This will generate a unique key of the class name, the method name,
//            // and all method parameters appended.
//            StringBuilder sb = new StringBuilder();
//            sb.append(o.getClass().getName());
//            sb.append(":");
//            sb.append(method.getName());
//            for (Object obj : objects) {
//                sb.append(":");
//                sb.append(obj.toString());
//            }
//            return sb.toString();
//        };
//    }
//
//    @Bean
//    public RedisTemplate<Object, Object> redisTemplate(
//            RedisConnectionFactory factory) {
//        RedisTemplate<Object, Object> template = new RedisTemplate<>();
//        template.setConnectionFactory(factory);
//        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
//        ObjectMapper om = new ObjectMapper();
//        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//        jackson2JsonRedisSerializer.setObjectMapper(om);
//        //使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值
//        template.setValueSerializer(jackson2JsonRedisSerializer);
//
//        //使用StringRedisSerializer来序列化和反序列化redis的key值
//        template.setKeySerializer(new StringRedisSerializer());
//        template.afterPropertiesSet();
//        return template;
//    }
//
//    @Bean
//    public CacheManager cacheManager(RedisTemplate redisTemplate) {
//        RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);
//
//        // Number of seconds before expiration. Defaults to unlimited (0)
//        cacheManager.setDefaultExpiration(300);
//        return cacheManager;
//    }
//
//    /********************************************************************************
//     *********************** pub / sub message with redis  **************************
//     ********************************************************************************/
//    //1 listener
//    @Bean
//    public MessageListenerAdapter messageListener() {
//        return new MessageListenerAdapter(new RedisMessageSubscriber());
//    }
//
//    //2 container
//    @Bean
//    public RedisMessageListenerContainer redisContainer() {
//        RedisMessageListenerContainer container
//                = new RedisMessageListenerContainer();
//        container.setConnectionFactory(jedisConnectionFactory());
//        container.addMessageListener(messageListener(), channelTopic());
//        return container;
//    }
//
//    //3 publisher
//    @Bean
//    public RedisMessagePublisher redisMessagePublisher() {
//        return new RedisMessagePublisher(redisTemplate(jedisConnectionFactory()), channelTopic());
//    }
//
//    //4 set up topic
//    @Bean
//    public ChannelTopic channelTopic() {
//        return new ChannelTopic("messageQueue");
//    }
//    //message end.
//}