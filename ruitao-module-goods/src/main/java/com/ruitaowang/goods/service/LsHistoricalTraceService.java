package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.LsHistoricalTrace;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.LsHistoricalTraceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class LsHistoricalTraceService implements BaseService<LsHistoricalTrace> {
    @Autowired
    private LsHistoricalTraceMapper LsHistoricalTraceMapper;

    @Override
    public LsHistoricalTrace insert(LsHistoricalTrace LsHistoricalTrace) {
        Long time = System.currentTimeMillis();
        LsHistoricalTrace.setCtime(time);
        LsHistoricalTrace.setMtime(time);
        LsHistoricalTraceMapper.insert(LsHistoricalTrace);
        return LsHistoricalTrace;
    }

    @Override
    public int delete(Long id) {
        LsHistoricalTrace LsHistoricalTrace = new LsHistoricalTrace();
        LsHistoricalTrace.setTraceId(id);
        LsHistoricalTrace.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        LsHistoricalTrace.setMtime(time);
        return LsHistoricalTraceMapper.updateByPK(LsHistoricalTrace);
    }

    @Override
    public LsHistoricalTrace update(LsHistoricalTrace LsHistoricalTrace) {
        Long time = System.currentTimeMillis();
        LsHistoricalTrace.setMtime(time);
        LsHistoricalTraceMapper.updateByPK(LsHistoricalTrace);
        return LsHistoricalTrace;
    }

    @Override
    public LsHistoricalTrace selectByPK(Long id) {

        return LsHistoricalTraceMapper.selectByPK(id);
    }

    @Override
    public List<LsHistoricalTrace> select(LsHistoricalTrace LsHistoricalTrace) {
        LsHistoricalTrace.setRstatus((byte) 0);
        return LsHistoricalTraceMapper.select(LsHistoricalTrace);
    }

    public List<LsHistoricalTrace> selectForPage(LsHistoricalTrace LsHistoricalTrace) {
        LsHistoricalTrace.setRstatus((byte) 0);
        LsHistoricalTrace.setRecords(LsHistoricalTraceMapper.count(LsHistoricalTrace));
        return LsHistoricalTraceMapper.select(LsHistoricalTrace);
    }
}

