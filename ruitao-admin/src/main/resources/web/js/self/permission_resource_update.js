/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/resources',
    updateUrl: '/api/resources',
    deleteUrl: '/api/resources',
    selectUrl: '/api/resources'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$().ready(function () {
    $.ajax(
        {
            url: Config.selectUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var resources = [];
                resources.push('<option value="0" hassubinfo="true">顶级菜单</option>');
                data.data.map(function(item){
                    resources.push('<option value="');
                    resources.push(item.id);
                    resources.push('" ');
                    if ($("#ppid").val() == item.id){
                        resources.push('selected="selected">');
                    }else{
                        resources.push('>');
                    }
                    resources.push(item.name);
                    resources.push('</option>');
                });
                $("#pid").html(resources.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });

    $('#submitForSave').click( function() {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()){
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.updateUrl,
                    type: "PUT",
                    data : postData,
                    dataType: "json",
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });

    $('#buttonGoback').click(function() {
        history.go(-1);
        location.reload();
        return false;
    });
});
