package com.ruitaowang.account.security;

import com.alibaba.fastjson.JSON;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Role;
import com.ruitaowang.core.domain.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by neal on 02/05/2017.
 */
public class LYSecurityUtil {

    @Autowired
    private SysUserService sysUserService;

    protected static final Logger LOGGER = LoggerFactory.getLogger("LYSecurityUtil");


    public static SysUser currentSysUser(){
        ServletRequestAttributes attr=(ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request =attr.getRequest();
        String user = (String) request.getSession().getAttribute("user");
        return JSON.parseObject(user,SysUser.class);
    }
}
