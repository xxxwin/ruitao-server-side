package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.GoodsType;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class GoodsTypeService implements BaseService<GoodsType> {

    @Autowired
    GoodsTypeMapper goodsTypeMapper;

    @Override
    public GoodsType insert(GoodsType goodsType) {
        Long time = System.currentTimeMillis();
        goodsType.setCtime(time);
        goodsType.setMtime(time);
        goodsTypeMapper.insert(goodsType);
        return goodsType;
    }

    @Override
    public int delete(Long id) {
        GoodsType goodsType =new GoodsType();
        goodsType.setTypeId(id);
        goodsType.setRstatus((byte)1);
        Long time = System.currentTimeMillis();
        goodsType.setMtime(time);
        return goodsTypeMapper.updateByPK(goodsType);
    }

    @Override
    public GoodsType update(GoodsType goodsType) {
        Long time = System.currentTimeMillis();
        goodsType.setMtime(time);
        goodsTypeMapper.updateByPK(goodsType);
        return goodsType;
    }

    @Override
    public GoodsType selectByPK(Long id) {
        return goodsTypeMapper.selectByPK(id);
    }

    @Override
    public List<GoodsType> select(GoodsType goodsType) {

        goodsType.setRstatus((byte) 0);

        return goodsTypeMapper.select(goodsType);
    }
}
