package com.ruitaowang.core.domain;

public class GoodsCategory extends Page {
	/**
	 * 商品分类编号,所属表字段为goods_category.category_id
	 */
	private Long categoryId;

	/**
	 * 商品分类名称,所属表字段为goods_category.category_name
	 */
	private String categoryName;

	/**
	 * 商品上级分类ID,所属表字段为goods_category.parent_category_id
	 */
	private Long parentCategoryId;

	private Integer level;

	/**
	 * 分类排序,所属表字段为goods_category.category_sort
	 */
	private Integer categorySort;

	/**
	 * 1， 隐藏不显示 0， 前端显示,所属表字段为goods_category.category_hidden
	 */
	private Byte categoryHidden;

	/**
	 * 分类重量单位,所属表字段为goods_category.category_unit
	 */
	private String categoryUnit;

	/**
	 * 显示在导航栏 1 是 0 否,所属表字段为goods_category.nav_show
	 */
	private Byte navShow;

	/**
	 * 商品数量,所属表字段为goods_category.category_amount
	 */
	private Integer categoryAmount;

	/**
	 * ,所属表字段为goods_category.category_small_icon
	 */
	private String categorySmallIcon;

	/**
	 * ,所属表字段为goods_category.category_desc
	 */
	private String categoryDesc;

	/**
	 * 创建时间,所属表字段为goods_category.ctime
	 */
	private Long ctime;

	/**
	 * 最后修改时间,所属表字段为goods_category.mtime
	 */
	private Long mtime;

	/**
	 * 行状态 0 正常，1 删除,所属表字段为goods_category.rstatus
	 */
	private Byte rstatus;

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName == null ? null : categoryName.trim();
	}

	public Long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(Long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public Integer getCategorySort() {
		return categorySort;
	}

	public void setCategorySort(Integer categorySort) {
		this.categorySort = categorySort;
	}

	public Byte getCategoryHidden() {
		return categoryHidden;
	}

	public void setCategoryHidden(Byte categoryHidden) {
		this.categoryHidden = categoryHidden;
	}

	public String getCategoryUnit() {
		return categoryUnit;
	}

	public void setCategoryUnit(String categoryUnit) {
		this.categoryUnit = categoryUnit == null ? null : categoryUnit.trim();
	}

	public Byte getNavShow() {
		return navShow;
	}

	public void setNavShow(Byte navShow) {
		this.navShow = navShow;
	}

	public Integer getCategoryAmount() {
		return categoryAmount;
	}

	public void setCategoryAmount(Integer categoryAmount) {
		this.categoryAmount = categoryAmount;
	}

	public String getCategorySmallIcon() {
		return categorySmallIcon;
	}

	public void setCategorySmallIcon(String categorySmallIcon) {
		this.categorySmallIcon = categorySmallIcon == null ? null : categorySmallIcon.trim();
	}

	public String getCategoryDesc() {
		return categoryDesc;
	}

	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc == null ? null : categoryDesc.trim();
	}

	public Long getCtime() {
		return ctime;
	}

	public void setCtime(Long ctime) {
		this.ctime = ctime;
	}

	public Long getMtime() {
		return mtime;
	}

	public void setMtime(Long mtime) {
		this.mtime = mtime;
	}

	public Byte getRstatus() {
		return rstatus;
	}

	public void setRstatus(Byte rstatus) {
		this.rstatus = rstatus;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

}