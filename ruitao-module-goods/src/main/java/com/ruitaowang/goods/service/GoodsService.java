package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.domain.GoodsAlbum;
import com.ruitaowang.core.domain.GoodsVO;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.goods.dao.GoodsAdMapper;
import com.ruitaowang.goods.dao.GoodsAlbumMapper;
import com.ruitaowang.goods.dao.GoodsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class GoodsService implements BaseService<Goods>{

    @Autowired
    private GoodsMapper goodsMapper;
    @Autowired
    private GoodsAlbumMapper goodsAlbumMapper;
    @Autowired
    private GoodsAdMapper goodsAdMapper;

    @Override
    public Goods insert(Goods goods) {
        Long time = System.currentTimeMillis();
        goods.setCtime(time);
        goods.setMtime(time);
        goodsMapper.insert(goods);
        return goods;
    }

    @Override
    public int delete(Long id) {
        Goods goods = new Goods();
        goods.setGoodsId(id);
        goods.setRstatus((byte) 1);
        return goodsMapper.updateByPK(goods);
    }

    @Override
    public Goods update(Goods goods) {
        if (!ObjectUtils.isEmpty(goods.getGoodsOnline())){
            if (goods.getGoodsOnline() == 1){
                goods.setStartDate(TimeUtils.getCurrentDatetime());
            }
        }
        goodsMapper.updateByPK(goods);
        return goods;
    }

    @Override
    public Goods selectByPK(Long id) {
        return goodsMapper.selectByPK(id);
    }

    @Override
    public List<Goods> select(Goods goods) {
        goods.setRstatus((byte) 0);
        return goodsMapper.select(goods);
    }
    public List<Goods> selectForPage(Goods goods) {
        goods.setRstatus((byte) 0);
        goods.setRecords(goodsMapper.count(goods));
        return goodsMapper.select(goods);
    }

    public GoodsAlbum insertAlbum(GoodsAlbum goodsAlbum) {
        goodsAlbumMapper.insert(goodsAlbum);
        return goodsAlbum;
    }

    public GoodsAlbum updateAlbum(GoodsAlbum goodsAlbum) {
        goodsAlbumMapper.updateByPK(goodsAlbum);
        return goodsAlbum;
    }
    public void setUpWarning(int val) {
        goodsAdMapper.setUpWarning(val);
    }

    public int deleteAlbum(Long albumId) {
        GoodsAlbum goodsAlbum = new GoodsAlbum();
        goodsAlbum.setImageId(albumId);
        goodsAlbum.setRstatus((byte) 1);
        return goodsAlbumMapper.updateByPK(goodsAlbum);
    }

    public List<GoodsAlbum> selectAlbum(GoodsAlbum goodsAlbum) {
        goodsAlbum.setRstatus((byte) 0);
        return goodsAlbumMapper.select(goodsAlbum);
    }

    public Long goodscount(Goods goods){
        goods.setRstatus((byte) 0);
        return goodsMapper.count(goods);
    }

    public List<Goods> goodsSearch(Goods goods){
        goods.setRstatus((byte) 0);
        goods.setRecords(goodsAdMapper.count(goods));
        goods.setGoodsOnline((byte)1);
        return goodsAdMapper.select(goods);
    }
    public List<Goods> goodsAdminSearch(Goods goods){
        goods.setRstatus((byte) 0);
        goods.setRecords(goodsAdMapper.count(goods));
        return goodsAdMapper.select(goods);
    }
    public List<Goods> selectForWarning(Goods goods){
        goods.setRstatus((byte) 0);
        goods.setRecords(goodsAdMapper.selectForWarningCount(goods));
        return goodsAdMapper.selectForWarning(goods);
    }

    public List<Goods> goodsForHomepage(Goods goods){
        goods.setRstatus((byte) 0);
        return goodsAdMapper.selectForHomepage(goods);
    }

    public List<GoodsVO> seaElection(GoodsVO goodsVO){
        goodsVO.setRecords(goodsAdMapper.seaElectionCount(goodsVO));
        return goodsAdMapper.seaElection(goodsVO);
    }

    public Long count(Goods goods) {
        goods.setRstatus((byte)0);
        return goodsMapper.count(goods);
    }
}
