package com.ruitaowang.core.domain;

import java.util.List;

public class CompanyVO extends Company {

    private long collectionId;

    private byte companyIndustryId;

    private List<GoodsVO> goodsList;

    private String goodsName;

    private List<Coupon> couponList;

    private String companyCommision;  //返点比例

    private String industryName;  //返点类型

    private double distance;    //距离

    private int isCompany;

    private int isRedEnvelopes;     //是否有红包

    private int isCoupon;       //是否有优惠劵

    private int isReward;       //是否有打赏

    private int iskefu;         //是否显示客服

    private int isme;

    public int getIsme() {
        return isme;
    }

    public void setIsme(int isme) {
        this.isme = isme;
    }

    public int getIskefu() {
        return iskefu;
    }

    public void setIskefu(int iskefu) {
        this.iskefu = iskefu;
    }

    public int getIsRedEnvelopes() {
        return isRedEnvelopes;
    }

    public void setIsRedEnvelopes(int isRedEnvelopes) {
        this.isRedEnvelopes = isRedEnvelopes;
    }

    public int getIsCoupon() {
        return isCoupon;
    }

    public void setIsCoupon(int isCoupon) {
        this.isCoupon = isCoupon;
    }

    public int getIsReward() {
        return isReward;
    }

    public void setIsReward(int isReward) {
        this.isReward = isReward;
    }

    public int getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(int isCompany) {
        this.isCompany = isCompany;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getIndustryName() {
        return industryName;
    }

    public void setIndustryName(String industryName) {
        this.industryName = industryName;
    }

    public String getCompanyCommision() {
        return companyCommision;
    }

    public void setCompanyCommision(String companyCommision) {
        this.companyCommision = companyCommision;
    }

    public List<Coupon> getCouponList() {
        return couponList;
    }

    public void setCouponList(List<Coupon> couponList) {
        this.couponList = couponList;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public List<GoodsVO> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<GoodsVO> goodsList) {
        this.goodsList = goodsList;
    }

    public byte getCompanyIndustryId() {
        return companyIndustryId;
    }

    public void setCompanyIndustryId(byte companyIndustryId) {
        this.companyIndustryId = companyIndustryId;
    }

    public long getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(long collectionId) {
        this.collectionId = collectionId;
    }
}
