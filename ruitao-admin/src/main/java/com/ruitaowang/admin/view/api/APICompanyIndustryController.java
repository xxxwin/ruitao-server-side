package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.goods.service.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class APICompanyIndustryController extends BaseController {

    @Autowired
    private CompanyIndustryService industryService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private CouponService couponService;
    @Autowired
    private CompanyTypeService companyTypeService;
    @Autowired
    private GoodsAlbumService goodsAlbumService;

    @RequestMapping("/api/admin/industry/select")
    public String select(CompanyIndustry companyIndustry, int page) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(companyIndustry.getSidx())) {
            companyIndustry.setOrderBy(companyIndustry.getSidx() + " "
                    + companyIndustry.getSord() + "," + companyIndustry.getOrderBy());
        }
        System.out.println("page=" + page);
        companyIndustry.setPage(page);
        MsgOut msgOut = MsgOut.success(industryService.selectForPage(companyIndustry));
        msgOut.setRecords(companyIndustry.getRecords());
        msgOut.setTotal(companyIndustry.getTotal());
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/admin/industry/selectAll")
    public String select(CompanyIndustry companyIndustry) {
        companyIndustry.setOrderBy("id asc");
        List<CompanyIndustry> industries = industryService.select(companyIndustry);
        if (ObjectUtils.isEmpty(industries)) {
            return this.renderJson(MsgOut.success("nodata"));
        }
        MsgOut msgOut = MsgOut.success(industries);
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/wap/industry/selectAll")
    public String wapSelectIndustry(CompanyIndustry companyIndustry) {
        companyIndustry.setOrderBy("id asc");
        List<CompanyIndustry> industries = industryService.select(companyIndustry);
        if (ObjectUtils.isEmpty(industries)) {
            return this.renderJson(MsgOut.success("nodata"));
        }
        MsgOut msgOut = MsgOut.success(industries);
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/wap/industry/onlyOne/{id}")
    public String wapSelectIndustryOne(@PathVariable("id") Long id) {
        CompanyIndustry companyIndustry = industryService.selectByPK(id);
        MsgOut msgOut = MsgOut.success(companyIndustry);
        if (ObjectUtils.isEmpty(companyIndustry)) {
            return this.renderJson(MsgOut.success("nodata"));
        }
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/wap/industry/select")
    public String wapSelect(CompanyIndustry companyIndustry) {
        if (ObjectUtils.isEmpty(companyIndustry)) {
            return this.renderJson(MsgOut.success("nodata"));
        }
        companyIndustry.setOrderBy("id asc");
        List<CompanyIndustry> industries = industryService.select(companyIndustry);
        List listOut = new ArrayList();
        for (CompanyIndustry ci : industries) {
            CompanyIndustryVO companyIndustryVO = new CompanyIndustryVO();
            BeanUtils.copyProperties(ci, companyIndustryVO);
            CompanyType companyType = new CompanyType();
            companyType.setIndustryId(ci.getId());
            List<CompanyType> companyTypeList = companyTypeService.select(companyType);
            companyIndustryVO.setCompanyTypes(companyTypeList);
            listOut.add(companyIndustryVO);
        }
        MsgOut msgOut = MsgOut.success(listOut);
        return this.renderJson(msgOut);
    }

    @RequestMapping(value = {"/api/admin/industry/selectByPk/{id}"}, method = RequestMethod.GET)
    public String selectByPK(@PathVariable("id") Long industryId) {
        MsgOut msgOut = MsgOut.success(industryService.selectByPK(industryId));
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/admin/industry/insert")
    public String insert(CompanyIndustry companyIndustry) {
        MsgOut msgOut = MsgOut.success(industryService.insert(companyIndustry));
        return this.renderJson(msgOut);
    }

    /**
     * 百万折扣 行业选择  点击小分类调取商品
     *
     * @param companyType
     * @return
     */
    @RequestMapping("/api/wap/company/industry")
    public String industrySelectGoods(CompanyType companyType) {
        if (ObjectUtils.isEmpty(companyType)) {
            return this.renderJson(MsgOut.error("参数错误"));
        }
        Company company = new Company();
        company.setCompanyTypeId(companyType.getId());
        List listOut = new ArrayList();
        List<Company> companies = companyService.select(company);
        for (Company c : companies) {
            Goods goods = new Goods();
            goods.setGoodsProviderId(c.getCompanyId());
            goods.setGoodsOnline((byte) 1);
            List<Goods> goodsList = goodsService.select(goods);
            for (Goods g : goodsList) {
                HashMap map = new HashMap();
                Coupon coupon = new Coupon();
                coupon.setGoodsId(g.getGoodsId());
                coupon.setCouponType((byte) 2);
                List<Coupon> coupons = couponService.select(coupon);
                if (!ObjectUtils.isEmpty(coupons)) {
                    map.put("coupon", coupons.get(0));
                    map.put("goods", g);
                    GoodsAlbum goodsAlbum = new GoodsAlbum();
                    goodsAlbum.setGoodsId(g.getGoodsId());
                    goodsAlbum.setImageType((byte) 1);
                    List<GoodsAlbum> goodsAlbums = goodsAlbumService.select(goodsAlbum);
                    if (!ObjectUtils.isEmpty(goodsAlbums)) {
                        map.put("goodsAlbum", goodsAlbums.get(0));
                    } else {
                        map.put("goodsAlbum", goodsAlbums.get(0));
                    }
                }
                if (!ObjectUtils.isEmpty(map)) {
                    listOut.add(map);
                }
            }
        }
        MsgOut msgOut = MsgOut.success(listOut);
        msgOut.setRecords(company.getRecords());
        msgOut.setTotal(company.getTotal());
        return this.renderJson(msgOut);
    }

    /**
     * 百万折扣 行业选择  点击行业调取商品
     *
     * @param id
     * @return
     */
    @RequestMapping("/api/wap/company/industry/goodsAll")
    public String insert(Long id) {
        if (ObjectUtils.isEmpty(id)) {
            return this.renderJson(MsgOut.error("参数错误"));
        }
        CompanyType companyType = new CompanyType();
        companyType.setIndustryId(id);
        List<CompanyType> companyTypes = companyTypeService.select(companyType);
        List listOut = new ArrayList();
        for (CompanyType ct : companyTypes) {
            Company company = new Company();
            company.setCompanyTypeId(ct.getId());
            List<Company> companies = companyService.select(company);
            if (!ObjectUtils.isEmpty(companies)) {
                for (Company c : companies) {
                    Goods goods = new Goods();
                    goods.setGoodsProviderId(c.getCompanyId());
                    goods.setGoodsOnline((byte) 1);
                    List<Goods> goodsList = goodsService.select(goods);
                    for (Goods g : goodsList) {
                        HashMap map = new HashMap();
                        Coupon coupon = new Coupon();
                        coupon.setGoodsId(g.getGoodsId());
                        coupon.setCouponType((byte) 2);
                        List<Coupon> coupons = couponService.select(coupon);
                        if (!ObjectUtils.isEmpty(coupons)) {
                            map.put("coupon", coupons.get(0));
                            map.put("goods", g);
                            GoodsAlbum goodsAlbum = new GoodsAlbum();
                            goodsAlbum.setGoodsId(g.getGoodsId());
                            goodsAlbum.setImageType((byte) 1);
                            List<GoodsAlbum> goodsAlbums = goodsAlbumService.select(goodsAlbum);
                            if (!ObjectUtils.isEmpty(goodsAlbums)) {
                                map.put("goodsAlbum", goodsAlbums.get(0));
                            } else {
                                map.put("goodsAlbum", goodsAlbums.get(0));
                            }
                        }
                        if (!ObjectUtils.isEmpty(map)) {
                            listOut.add(map);
                        }
                    }
                }
            }
        }

        MsgOut msgOut = MsgOut.success(listOut);
        return this.renderJson(msgOut);
    }

    /**
     * 行业选择  点击小分类调取商家及 三个商品 1个满减券
     *
     * @param company
     * @return
     */
    @RequestMapping("/api/wap/type/goods3/coupon")
    public String syIndustryCompany(Company company,int page) {
        if (ObjectUtils.isEmpty(company)) {
            return this.renderJson(MsgOut.error("参数错误"));
        }
        List listOut = new ArrayList();
        company.setOrderBy("ctime asc");
        company.setPage(page);
        List<Company> companies = companyService.selectForPage(company);
        for (Company c : companies) {
            HashMap map = new HashMap();
            Goods goods = new Goods();
            goods.setGoodsProviderId(c.getCompanyId());
            goods.setGoodsOnline((byte) 1);
            goods.setRows(3);
            goods.setPage(1);
            List<Goods> goodsList = goodsService.selectForPage(goods);
            if (!ObjectUtils.isEmpty(goodsList)) {
                Coupon coupon = new Coupon();
                coupon.setCompanyId(c.getCompanyId());
                coupon.setCouponType((byte) 0);
                List<Coupon> coupons = couponService.select(coupon);
                for (Goods g : goodsList) {
                    GoodsAlbum goodsAlbum = new GoodsAlbum();
                    goodsAlbum.setGoodsId(g.getGoodsId());
                    goodsAlbum.setImageType((byte) 1);
                    List<GoodsAlbum> goodsAlbums = goodsAlbumService.select(goodsAlbum);
                    if (!ObjectUtils.isEmpty(goodsAlbums)) {
                        g.setGoodsThum(goodsAlbums.get(0).getUrl());
                    }
                }
                map.put("goods", goodsList);
                map.put("company", c);
                if (!ObjectUtils.isEmpty(coupons)) {
                    map.put("coupon", coupons.get(0));
                } else {
                    map.put("coupon", "");
                }
                if (!ObjectUtils.isEmpty(map)) {
                    listOut.add(map);
                }
            }
        }
        MsgOut msgOut = MsgOut.success(listOut);
        msgOut.setRecords(company.getRecords());
        msgOut.setTotal(company.getTotal());
        return this.renderJson(msgOut);
    }

    /**
     * 行业选择  点击行业调取商家及 三个商品 1个满减券
     *
     * @param companyVO
     * @param page
     * @return
     */
    @RequestMapping("/api/wap/industry/goods3/coupon")
    public String syTypeCompany(CompanyVO companyVO, int page) {
        if (ObjectUtils.isEmpty(companyVO.getCompanyIndustryId())) {
            return this.renderJson(MsgOut.error("参数错误"));
        }

        List listOut = new ArrayList();

        companyVO.setPage(page);
        List<Company> companies = companyService.selectCompanyIndustryIdForPage(companyVO);
        for (Company c : companies) {
            HashMap map = new HashMap();
            Goods goods = new Goods();
            goods.setGoodsProviderId(c.getCompanyId());
            goods.setGoodsOnline((byte) 1);
            goods.setPage(1);
            goods.setRows(3);
            List<Goods> goodsList = goodsService.selectForPage(goods);
            if (!ObjectUtils.isEmpty(goodsList)) {
                Coupon coupon = new Coupon();
                coupon.setCompanyId(c.getCompanyId());
                coupon.setCouponType((byte) 0);
                List<Coupon> coupons = couponService.select(coupon);
                for (Goods g : goodsList) {
                    GoodsAlbum goodsAlbum = new GoodsAlbum();
                    goodsAlbum.setGoodsId(g.getGoodsId());
                    goodsAlbum.setImageType((byte) 1);
                    List<GoodsAlbum> goodsAlbums = goodsAlbumService.select(goodsAlbum);
                    if (!ObjectUtils.isEmpty(goodsAlbums)) {
                        g.setGoodsThum(goodsAlbums.get(0).getUrl());
                    }
                }
                map.put("goods", goodsList);
                map.put("company", c);
                if (!ObjectUtils.isEmpty(coupons)) {
                    map.put("coupon", coupons.get(0));
                } else {
                    map.put("coupon", "");
                }
                if (!ObjectUtils.isEmpty(map)) {
                    listOut.add(map);
                }
            }

        }
        MsgOut msgOut = MsgOut.success(listOut);
        msgOut.setRecords(companyVO.getRecords());
        msgOut.setTotal(companyVO.getTotal());
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/admin/industry/update")
    public String update(CompanyIndustry companyIndustry) {
        companyIndustry.setMtime(System.currentTimeMillis());
        MsgOut msgOut = MsgOut.success(industryService.update(companyIndustry));
        return this.renderJson(msgOut);
    }

    @RequestMapping(value = {"/api/admin/industry/delete/{id}"}, method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id) {
        MsgOut msgOut = MsgOut.success(industryService.delete(id));
        return this.renderJson(msgOut);
    }
}
