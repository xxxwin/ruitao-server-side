package com.ruitaowang.core.service.queue;

/**
 * Created by neal on 12/09/2017.
 */
public interface MessagePublisher {
    void publish(final String message);
}
