/**
 * Created by Shaka on 12/21/16.
 */
var Config = {
    selectUrl: '/api/admin/dc/table',
    deleteUrl: '/api/admin/dc/table'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#dc_table").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["餐桌ID","公司", "餐桌名称","餐桌类型", "使用状态",  "备注", "时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            sorttype: "int",
            search: true
        }, {
            name: "companyId",
            index: "companyId"
        },{
            name: "name",
            index: "name"
        }, {
            name: "tableTypeId",
            index: "tableTypeId",
            formatter:mydateformatter
        }, {
            name: "busy",
            index: "busy",
            editoptions: {value: "0:未使用;1:已使用"},
            editable: false,
            edittype: 'select',
            formatter: 'select'
        },{
            name: "remark",
            index: "remark"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate",
            autowidth: true,
            sortable: false
        }],
        pager: "#pager_dc_table",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#dc_table").jqGrid('navGrid', '#pager_dc_table', {add: false, edit: false, del: false,search: false,
        refresh: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#dc_table").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#dc_table").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#dc_table").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#dc_table").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#dc_table').jqGrid ('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/web/dc_table_create.html?id='+dataFromTheRow.companyId+'" data-index="0">添加</a>';
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/admin/dc/table/update/'+ id +'" data-index="0">修改</a>';
        jQuery("#dc_table").jqGrid('setRowData', ids[i], {operate: html,ctime: dataFromTheRow.ctime});
    }
}
function mydateformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax({
        url: "/api/admin/dc/tableType/only",
        type: "GET",
        data:{tableTypeId:rowObject.tableTypeId},
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            name = data.data.name;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return name;
}
