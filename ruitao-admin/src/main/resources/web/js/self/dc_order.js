/**
 *
 */
var num =0;
var Config = {
    createUrl: '/api/admin/dc/orders',
    updateUrl: '/api/admin/dc/orders',
    deleteUrl: '/api/admin/dc/orders',
    selectUrl: '/api/admin/dc/orders'
};
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
    $.ajax({
        url: "/api/admin/dc/order/setInterval",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
           num=data.data
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 550,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["订单ID", "日期", "流水号","桌号", "总价(元)", "返红包", "公司ID", "备注", "时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: false
        }, {
            name: "day",
            index: "day"
        }, {
            name: "dayIndex",
            index: "dayIndex"
        }, {
            name: "tableId",
            index: "tableId",
            formatter:mydateformatter,
        },{
            name: "amount",
            index: "amount"
        }, {
            name: "score",
            index: "score"
        }, {
            name: "companyId",
            index: "companyId"
        }, {
            name: "remark",
            index: "remark"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate"
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false,id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
   // setInterval(show,5000);
});
function show(){
    var v = $('#data_list').jqGrid('getRowData', rowId);
    $.ajax({
        url: "/api/admin/dc/order/setInterval",
        type: "GET",
        data:{companyId:v.companyId},
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            if (data.data > num){
                console.log("1"+data.data);
                jQuery("#data_list").trigger("reloadGrid");
            }
            console.log("2"+data.data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='orderUpdate(" + id + ")' >详情</a>";
        html +="<a style='color:#f60;padding-right: 15px' onclick='print1(" + id + ")' >打印</a>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html,ctime: dataFromTheRow.ctime,amount:toFixedForPrice(dataFromTheRow.amount)});
    }
}
function orderUpdate(id) {
    layer.open({
        type: 2,
        //   title: '请选择区域',
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content: "/web/dc_order_update.html?id=" + id
    })
}

// function toFixedForNumber(number) {
//     return (number * 1 / 10000).toFixed(4);
// }

// function print1(rowId) {
//     var v = $('#data_list').jqGrid('getRowData', rowId);
//     console.log(v.tableId);
//     console.log(v.amount);
//     console.log(v.remark);
//     console.log(toFixedForNumber(v.dayIndex).substring(2,6));
//     console.log(v.day);
//     $("#print_dayIndex").html("订单号：" + toFixedForNumber(v.dayIndex).substring(2,6));
//     $("#print_tableId").html("餐座号： " + v.tableId + "桌");
//     $("#dish_time").html("时间:" + v.day);
//     $("#print_pay").html("合计：¥ " + toFixedForPrice(v.amount));
//     $("#print_remark").html("备注：" + v.remark);
//     var dishHtml = "<hr/><tr style=font-size:xx-small><td>菜品</td><td>数量</td><td>单价</td></tr><hr/>";
//     $.ajax({
//         url: "/api/wap/dc/orderProds",
//         type: "GET",
//         data: {id: v.id},
//         dataType: "json",
//         async: false,
//         success: function (data, textStatus, jqXHR) {
//             for (var i = 0; i < data.data.length; i++) {
//                 dishHtml += "<tr style=font-size:xx-small><td>" + data.data[i].name + "</td><td>*" + data.data[i].num + "</td><td>" + toFixedForPrice(data.data[i].price) + "</td></tr>"
//             }
//             $(".dish_table").find("tbody").html(dishHtml);
//         },
//         error: function (jqXHR, textStatus, errorThrown) {
//             toastr.error(errorThrown, textStatus);
//         }
//     });
//    $("#div_print").printMe();
// }
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    $("#data_list").trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown);
            }
        });
}
function mydateformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax({
        url: "/api/admin/dc/table/"+rowObject.tableId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            if (data.data){
                name = data.data.name;
            }else {
                name=""
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return name;
}