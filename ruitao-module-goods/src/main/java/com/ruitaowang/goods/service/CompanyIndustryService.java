package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyIndustry;
import com.ruitaowang.goods.dao.CompanyIndustryAdMapper;
import com.ruitaowang.goods.utils.RStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 行业Service
 */
@Service
public class CompanyIndustryService {
    @Autowired
    private CompanyIndustryAdMapper companyIndustryAdMapper;

    public List<CompanyIndustry> select(CompanyIndustry companyIndustry){
        companyIndustry.setRstatus(RStatus.Exist);
        return companyIndustryAdMapper.select(companyIndustry);
    }

    public List<CompanyIndustry> selectForPage(CompanyIndustry companyIndustry){
        companyIndustry.setRstatus(RStatus.Exist);
        companyIndustry.setRecords(companyIndustryAdMapper.count(companyIndustry));
        return companyIndustryAdMapper.select(companyIndustry);
    }

    public CompanyIndustry selectByPK(Long id){
       return companyIndustryAdMapper.selectByPK(id);
    }

    public CompanyIndustry insert(CompanyIndustry companyIndustry){
        Long time=System.currentTimeMillis();
        companyIndustry.setCtime(time);
        companyIndustry.setMtime(time);
        companyIndustryAdMapper.insert(companyIndustry);
        return companyIndustry;
    }

    public int delete(Long id){
        CompanyIndustry companyIndustry= new CompanyIndustry();
        companyIndustry.setMtime(System.currentTimeMillis());
        companyIndustry.setRstatus(RStatus.Deleted);
        companyIndustry.setId(id);
        return companyIndustryAdMapper.updateByPK(companyIndustry);
    }

    public CompanyIndustry update(CompanyIndustry companyIndustry){
        companyIndustry.setMtime(System.currentTimeMillis());
        companyIndustryAdMapper.updateByPK(companyIndustry);
        return companyIndustryAdMapper.selectByPK(companyIndustry.getId());
    }

}
