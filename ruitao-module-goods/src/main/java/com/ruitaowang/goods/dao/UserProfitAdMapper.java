package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.RelationsVO;
import com.ruitaowang.core.domain.UserProfit;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserProfitAdMapper {

    List<UserProfit> select(UserProfit userProfit);

    long count(UserProfit userProfit);

    List<UserProfit> selectKuaiZhuanTopForTotal(UserProfit userProfit);

    List<UserProfit> selectUserProfit(UserProfit userProfit);

    long selectUserProfitCount(UserProfit userProfit);

    @Select("SELECT * FROM rtshop.user_profit where ctime > #{stime} and ctime < #{etime} and user_id = #{userId} and order_type=28 and rstatus =0")
    List<UserProfit> selectRankingRewardStatus(UserProfit userProfit);


    @Select("SELECT * FROM rtshop.user_profit where ctime > #{stime} and ctime < #{etime} and user_id = #{userId} and order_type=29 and rstatus =0")
    List<UserProfit> selectRankingRewardStatusTwo(UserProfit userProfit);
}