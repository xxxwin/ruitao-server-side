#!/usr/bin/env bash

/usr/bin/git checkout master

/usr/bin/git pull

workDir=/home/ruitao/server/source/ruitao-server-side

activeFile1=$workDir/ruitao-rest-mobile/src/main/resources/application.properties

activeFile2=$workDir/ruitao-admin/src/main/resources/application.properties
#
#
#echo 'Before ...'
#
#cat $activeFile1
#
#cat $activeFile2
#

sed -i 's/dev/live/1' $activeFile1

sed -i 's/dev/live/1' $activeFile2

echo "Live ..."

cat $activeFile1

cat $activeFile2

/home/ruitao/mvn/bin/mvn clean install -DskipTests


echo 'After ...'

sed -i 's/live/dev/1' $activeFile1

sed -i 's/live/dev/1' $activeFile2

cat $activeFile1

cat $activeFile2

sudo /usr/bin/cp /home/ruitao/server/source/ruitao-server-side/ruitao-rest-mobile/target/ruitao-rest-mobile-1.0.0.war /home/ruitao/ROOT.war
