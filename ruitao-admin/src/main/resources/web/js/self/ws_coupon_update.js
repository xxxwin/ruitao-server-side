

var Config = {
    selectCompanyUrl: '/api/admin/company/providers',
    submitUpdate:'/api/ws/coupon',
    selectGoods:"/api/admin/goods"
};

$(".datepicker").datepicker({
    todayBtn: "linked",
    clearBtn: true,
    autoclose: true,
    language: "zh-CN"
});

$(document).ready(function () {
    $.ajax(
        {
            url:'/api/admin/companyInfo',
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data) {
                var comapnyes = [];
                data.data.map(function (comapny) {
                    comapnyes.push('<option value="');
                    comapnyes.push(comapny.companyId);
                    comapnyes.push('" ');
                    comapnyes.push('hassubinfo="true">');
                    comapnyes.push(comapny.companyName);
                    comapnyes.push('</option>');
                    //console.log(comapny);
                });
                //console.log(comapnyes);
                $("#companyId").html(comapnyes.join(''));
                $("#companyId").val($("input[id=cid]").val());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //console.error(errorThrown);
            }
        }
    );

    $.ajax(
        {
            url: Config.selectGoods,
            type: "GET",
            data:{goodsProviderId:$("#companyId").val(),page:1,rows:1000},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="" hassubinfo="true">选择商品</option>');
                data.data.map(function (goods) {
                    categories.push('<option value="');
                    categories.push(goods.goodsId);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(goods.goodsName);
                    categories.push('</option>');
                });
                $("#goodsId").html(categories.join(''));
                $("#goodsId").val($("#goodsIds").val());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });

    $("#companyId").change(function () {
        $.ajax(
            {
                url: Config.selectGoods,
                type: "GET",
                data:{goodsProviderId:$(this).val(),page:1,rows:1000},
                dataType: "json",
                async: false,
                success: function (data, textStatus, jqXHR) {
                    var categories = [];
                    categories.push('<option value="" hassubinfo="true">选择商品</option>');
                    data.data.map(function (goods) {
                        categories.push('<option value="');
                        categories.push(goods.goodsId);
                        categories.push('" ');
                        categories.push('hassubinfo="true">');
                        categories.push(goods.goodsName);
                        categories.push('</option>');
                    });
                    $("#goodsId").html(categories.join(''));
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
    });

    $("#submitCreate").click(function () {
        var id = $("input[id=id]").val();
        var couponType = $("#coupon_type").val();
        var reach1 = $("input[id=reach1]").val() * 100;
        var score1 = $("input[id=score1]").val() * 100;
        var goodsId = $("#goodsId").val();
        var price = $("input[id=price]").val() * 100;
        var couponTime =  dateToEpoch($("#beginDateOne").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
        var couponEndtime =  dateToEpoch($("#endDateOne").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
        var remark=$("input[id=remark]").val();
        var companyId=$('#companyId option:selected').val();
        if (!companyId){
            toastr.error("请选择商户");
            return false;
        }
        if (!couponType){
            toastr.error("请选择分类");
            return false;
        }
        var data = "";
        if(couponType == 0){
            if(reach1 == "" || reach1 == null){
                toastr.error("请填写满减价钱");
                return false;
            }
            if(score1 == "" || score1 == null){
                toastr.error("请填写打折价钱");
                return false;
            }
            data = {
                id : id,
                companyId : companyId,
                couponType : couponType,
                goodsId : 0,
                price : 0,
                reach1 : reach1,
                score1 : score1,
                couponTime : couponTime,
                couponEndtime : couponEndtime,
                remark : remark
            }
        }else if(couponType == 2){
            if (!goodsId){
                toastr.error("请选择商品");
                return false;
            }
            if (price == "" || price == null){
                toastr.error("请填写折扣价钱");
                return false;
            }
            data = {
                id : id,
                companyId : companyId,
                couponType : couponType,
                goodsId : goodsId,
                price : price,
                reach1 : 0,
                score1 : 0,
                couponTime : couponTime,
                couponEndtime : couponEndtime,
                remark : remark
            }
        }
        if(isNaN(couponTime)){
            toastr.error("请选择起始时间");
            return false;
        }
        if(isNaN(couponEndtime)){
            toastr.error("请选择结束时间");
            return false;
        }
        $.ajax(
            {
                url: Config.submitUpdate,
                type: "PUT",
                data:data,
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
        });
    });
});

$('.sj').hide();
$("#coupon_type").change(function () {
    if($(this).val() == 0){
        $('.sj').hide();
        $('.bl').show();
    }

    if($(this).val() == 2){
        $('.sj').show();
        $('.bl').hide();
    }
});

function load() {
    if($("#couponType").val() == 0){
        $("#coupon_type").val("0");
        $('.sj').hide();
        $('.bl').show();
    }else if($("#couponType").val() == 2){
        $("#coupon_type").val("2");
        $('.sj').show();
        $('.bl').hide();
    }
    var couponTime = new Date($("#couponTime").val() * 1).yyyymmddhhmmss();
    var newCouponTime = couponTime.substring(0,10);
    var couponEndtime = new Date($("#couponEndtime").val() * 1).yyyymmddhhmmss();
    var newCouponEndtime = couponEndtime.substring(0,10);
    $("#beginDateOne").val(newCouponTime);
    $("#endDateOne").val(newCouponEndtime);

    var price = $("#price").val() / 100;
    var reach1 = $("#reach1").val() / 100;
    var score1 = $("#score1").val() /100;
    $("#price").val(price);
    $("#reach1").val(reach1);
    $("#score1").val(score1);
}

