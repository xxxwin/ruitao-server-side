
var Config = {
    selectUrl: "/api/localOrder/fjOrder",
    updateUrl:"/api/admin/orders",
    updateUrlForCellEdit: '/api/admin/order/cellEdit',
    selectExcelUrl: "/api/admin/orders/excel/list"
};

$(document).ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "订单总额","购买状态", "订单时间"],
        colModel: [{
            name: "orderId",
            index: "orderId",
            sorttype: "int",
            width: 5,
            search: true,
            key: true
        }, {
            name: "amount",
            index: "amount",
            width: 7
        },{
            name: "payStatus",
            index: "payStatus",
            sorttype: "int",
            width: 7,
            editoptions: {value: "0:未支付;1:已支付;11:支付未完成"},
            editable: false,
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "ctime",
            index: "ctime",
            width: 7,
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        multiselect: false,
        multiboxonly: true,
        pager: "#pager_data_list",
        viewrecords: true,
        hidegrid: false,
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "orderId"
        },
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});


function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='orderUpdate(" + dataFromTheRow.orderId + ")'>详情</a>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {amount:toFixedForPrice(dataFromTheRow.amount),ctime: dataFromTheRow.ctime,operate: html});
    }
}

function orderUpdate(id) {
    layer.open({
        type: 2,
        //   title: '请选择区域',
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content: "/orders/update/"+id
    })
}

$("#perform_search").click(function () {
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    if($("#beginDate").val() != ""){
        postdata.stime = dateToEpoch($("#beginDate").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    }
    if($("#endDate").val() != ""){
        postdata.etime = dateToEpoch($("#endDate").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    }
    postdata.remark = $("#buyType") .val();
    jQuery("#data_list").trigger("reloadGrid",[{page: 1}]);
});