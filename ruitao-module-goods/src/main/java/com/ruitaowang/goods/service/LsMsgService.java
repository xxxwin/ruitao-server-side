package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.LsMsg;
import com.ruitaowang.core.domain.UserProfit;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.core.utils.RandomUtils;
import com.ruitaowang.goods.dao.LsMsgAdMapper;
import com.ruitaowang.goods.dao.LsMsgMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class LsMsgService implements BaseService<LsMsg> {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private LsMsgMapper LsMsgMapper;
    @Autowired
    private LsMsgAdMapper LsMsgAdMapper;
    @Autowired
    private UserProfitService userProfitService;

    @Override
    public LsMsg insert(LsMsg LsMsg) {
        Long time = System.currentTimeMillis();
        LsMsg.setCtime(time);
        LsMsg.setMtime(time);
        LsMsg.setPaySn(RandomUtils.genOrderSN(LsMsg.getFromUserId()));
        LsMsgMapper.insert(LsMsg);
        return LsMsg;
    }

    public LsMsg insertMsg(LsMsg LsMsg, Byte pred, String predText, Byte msgType) {
        LsMsg.setPred(pred);
        LsMsg.setPredText(predText);
        LsMsg.setMsgType(msgType);
        this.insert(LsMsg);
        return LsMsg;
    }

    public LsMsg insertCommon(LsMsg LsMsg, Byte msgType) {
        return insertMsg(LsMsg, (byte) 0, "common msg", msgType);
    }

    public LsMsg insert4Text(LsMsg LsMsg) {
        return insertCommon(LsMsg, (byte) 0);
    }
    public LsMsg insert4Photo(LsMsg LsMsg) {
        return insertCommon(LsMsg, (byte) 1);
    }
    public LsMsg insert4Video(LsMsg LsMsg) {
        return insertCommon(LsMsg, (byte) 3);
    }

    public LsMsg insertBaping(LsMsg LsMsg, Byte msgType) {
        return insertMsg(LsMsg, (byte) 1, "baping", msgType);
    }
    public LsMsg insertText4Baping(LsMsg LsMsg) {
        return insertBaping(LsMsg, (byte) 0);
    }
    public LsMsg insertPhoto4Baping(LsMsg LsMsg) {
        return insertBaping(LsMsg, (byte) 1);
    }
    public LsMsg insertVideo4Baping(LsMsg LsMsg) {
        return insertBaping(LsMsg, (byte) 3);
    }

    public LsMsg insertDashang(LsMsg LsMsg) {
        return insertMsg(LsMsg, (byte) 2, "dashang", (byte) 0);
    }

    public LsMsg insertDianbo(LsMsg LsMsg, Byte msgType) {
        return insertMsg(LsMsg, (byte) 3, "dianbo", msgType);
    }
    public LsMsg insertText4Dianbo(LsMsg LsMsg) {
        return insertDianbo(LsMsg, (byte) 0);
    }
    public LsMsg insertPhoto4Dianbo(LsMsg LsMsg) {
        return insertDianbo(LsMsg, (byte) 1);
    }
    public LsMsg insertVideo4Dianbo(LsMsg LsMsg) {
        return insertDianbo(LsMsg, (byte) 3);
    }
    public LsMsg insertHongbao(LsMsg LsMsg, Byte msgType) {
        return insertMsg(LsMsg, (byte) 4, "hongbao", msgType);
    }
    public LsMsg insertText4Hongbao(LsMsg LsMsg) {
        return insertHongbao(LsMsg, (byte) 0);
    }
    @Override
    public int delete(Long id) {
        LsMsg LsMsg = new LsMsg();
        LsMsg.setMsgId(id);
        LsMsg.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        LsMsg.setMtime(time);
        return LsMsgMapper.updateByPK(LsMsg);
    }

    @Override
    public LsMsg update(LsMsg LsMsg) {
        Long time = System.currentTimeMillis();
        LsMsg.setMtime(time);
        LsMsgMapper.updateByPK(LsMsg);
        return LsMsg;
    }

    @Override
    public LsMsg selectByPK(Long id) {
        return LsMsgMapper.selectByPK(id);
    }

    @Override
    public List<LsMsg> select(LsMsg LsMsg) {
        LsMsg.setRstatus((byte) 0);
        return LsMsgMapper.select(LsMsg);
    }

    public List<LsMsg> select4New(LsMsg LsMsg) {
        return LsMsgAdMapper.select4New(LsMsg);
    }

    public List<LsMsg> selectForPage(LsMsg LsMsg) {
        LsMsg.setRstatus((byte) 0);
        LsMsg.setPayStatus((byte) 0);
        LsMsg.setRecords(LsMsgMapper.count(LsMsg));
        return LsMsgMapper.select(LsMsg);
    }
    public Long selectCount(LsMsg LsMsg) {
        return LsMsgMapper.count(LsMsg);
    }
    @Autowired
    RedisTemplate<Object, Object> redisTemplate;

    public void faLuckyMoneySQ(Long msgId) {
        LsMsg msg = LsMsgMapper.selectByPK(msgId);
        if(msg.getPred() == 6){//积分红包，没有奖励
            String key = "lucky_money:"+msgId;
            ListOperations<Object, Object> operations = redisTemplate.opsForList();
            if(operations.size(key)==0){
                Arrays.stream(RandomUtils.luckyMoney(msg.getLuckyMoneyPrice(), msg.getLuckyMoneyNum())).forEach(item->{operations.leftPush(key, item);});
                UserProfit userProfit = new UserProfit();
                userProfit.setOrderSn(msg.getPaySn());
                userProfit.setUserId(msg.getFromUserId());
                userProfit.setAmount(msg.getLuckyMoneyPrice());
                userProfit.setConsumerId(msg.getFromUserId());
                userProfit.setOrderId(msg.getMsgId());
                userProfit.setUserProfit(-msg.getLuckyMoneyPrice());
                userProfit.setOrderType((byte) -4);//发 lucky money score
                userProfitService.insert(userProfit);
            }
        }
    }

    public void faLuckyMoneyPT(Long msgId) {
        LsMsg msg = LsMsgMapper.selectByPK(msgId);
        if(msg.getPred() == 6){//积分红包，没有奖励
            String key = "lucky_money:"+msgId;
            ListOperations<Object, Object> operations = redisTemplate.opsForList();
            if(operations.size(key)==0){
//                for (int i=1; i<msg.getLuckyMoneyNum(); i++){
                for (int i=0; i<msg.getLuckyMoneyNum(); i++){
                    operations.leftPush(key, msg.getLuckyMoneyPrice());
                }
                UserProfit userProfit = new UserProfit();
                userProfit.setOrderSn(msg.getPaySn());
                userProfit.setUserId(msg.getFromUserId());
                userProfit.setAmount(msg.getLuckyMoneyPrice());
                userProfit.setConsumerId(msg.getFromUserId());
                userProfit.setOrderId(msg.getMsgId());
                userProfit.setUserProfit(-msg.getLuckyMoneyPrice());
                userProfit.setOrderType((byte) -4);//发 lucky money score
                userProfit.setMoneyType((byte)1);
                userProfit.setRemark("塞入红包金额");
                userProfitService.insert(userProfit);
            }
        }
    }

    public void faLuckyMoneyBP(Long msgId) {
        LsMsg msg = LsMsgMapper.selectByPK(msgId);
        if(msg.getPred() == 6){//积分红包，没有奖励
            String key = "lucky_money:"+msgId;
            ListOperations<Object, Object> operations = redisTemplate.opsForList();
            if(operations.size(key)==0){
                for (int i=0; i<msg.getLuckyMoneyNum(); i++){
                    operations.leftPush(key, msg.getLuckyMoneyPrice() / 5);
                }
            }
        }
    }

    public int chaiLuckyMoney(Long msgId, Long userId) {
        LsMsg msg = LsMsgMapper.selectByPK(msgId);
        if(msg.getPred() == 6){//积分红包，没有奖励

            UserProfit where = new UserProfit();
            where.setOrderSn(msg.getPaySn());
            where.setOrderType((byte) 19);
            List<UserProfit> userProfits = userProfitService.select(where);
            where.setUserId(userId);
            List<UserProfit> myQiang = userProfitService.select(where);

            if(userProfits.size() <= msg.getLuckyMoneyNum() && ObjectUtils.isEmpty(myQiang)){
                String key = "lucky_money:"+msgId;
                ListOperations<Object, Object> operations = redisTemplate.opsForList();
                LOGGER.info("listLength == " + redisTemplate.opsForList().size(key));
                Object money = operations.leftPop(key);//取一次 少一个
                if(money == null){
                    return 0;
                }
                UserProfit userProfit = new UserProfit();
                userProfit.setOrderSn(msg.getPaySn());
                userProfit.setUserId(userId);
                userProfit.setAmount(msg.getLuckyMoneyPrice());
                userProfit.setConsumerId(msg.getFromUserId());
                userProfit.setOrderId(msg.getMsgId());
                userProfit.setUserProfit((int) money);
                userProfit.setOrderType((byte) 19);//lucky money score
                userProfit.setMoneyType((byte)2);
                userProfitService.insert(userProfit);
                return (int) money;
            }
        }
        return 0;
    }

    public int selectyMoney(Long msgId, Long userId) {
        LsMsg msg = LsMsgMapper.selectByPK(msgId);
        if(msg.getPred() == 6){//积分红包，没有奖励

            UserProfit where = new UserProfit();
            where.setOrderSn(msg.getPaySn());
            where.setOrderType((byte) 19);
            List<UserProfit> userProfits = userProfitService.select(where);
            where.setUserId(userId);
            List<UserProfit> myQiang = userProfitService.select(where);

            if(!ObjectUtils.isEmpty(myQiang)){
                return 2;
            }

            if(userProfits.size() <= msg.getLuckyMoneyNum() && ObjectUtils.isEmpty(myQiang)){
                String key = "lucky_money:"+msgId;
                Long size = redisTemplate.opsForList().size(key);
                if(size == 0){
                    return 0;
                } else {
                    return 1;
                }
            }
        }
        return 0;
    }

    public int lingHb(Long msgId, Long userId) {
        LsMsg msg = LsMsgMapper.selectByPK(msgId);
        if(msg.getPred() == 6){//积分红包，没有奖励

            UserProfit where = new UserProfit();
            where.setOrderSn(msg.getPaySn());
            where.setOrderType((byte) 19);
            List<UserProfit> userProfits = userProfitService.select(where);
            where.setUserId(userId);
            List<UserProfit> myQiang = userProfitService.select(where);

            if(!ObjectUtils.isEmpty(myQiang)){
                return 2;
            }

            if(userProfits.size() <= msg.getLuckyMoneyNum() && ObjectUtils.isEmpty(myQiang)){
                String key = "lucky_money:"+msgId;
                Long size = redisTemplate.opsForList().size(key);
                if(size == 0){
                    return 3;
                } else {
                    return 2;
                }
            }
        }
        return 0;
    }

    public List<LsMsg> selectBySearch(LsMsg lsMsg){
        lsMsg.setRstatus((byte) 0);
        lsMsg.setRecords(LsMsgAdMapper.countBySearch(lsMsg));
        return LsMsgAdMapper.selectBySearch(lsMsg);
    }
}

