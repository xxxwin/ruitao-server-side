#!/usr/bin/env bash

cp /home/ruitao/server/source/ruitao-server-side/ruitao-rest-mobile/target/ruitao-rest-mobile-1.0.0.war /home/ruitao/ROOT.war
scp /home/ruitao/server/source/ruitao-server-side/ruitao-admin/target/ruitao-admin-1.0.0.war ruitao@admin.ruitaowang.com:/home/ruitao/ROOT.war
scp /home/ruitao/server/source/ruitao-server-side/ruitao-rest-mobile/target/ruitao-rest-mobile-1.0.0.war ruitao@int.bak.bmw.ruitaowang.com:/home/ruitao/ROOT.war
scp /home/ruitao/server/source/ruitao-server-side/ruitao-admin/target/ruitao-admin-1.0.0.war ruitao@int.bak.admin.ruitaowang.com:/home/ruitao/ROOT.war


echo 'Uploaded...'