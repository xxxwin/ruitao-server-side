/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/admin/users',
    updateUrl: '/api/admin/users',
    deleteUrl: '/api/admin/users',
    selectUrl: '/api/admin/users',

    resetPasswdUrl: '/api/accounts/resetPasswd'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "用户名","真实姓名","手机号","手机号验证状态","邮箱验证状态","最后登录时间"],
        colModel: [{
            name: "id",
            index: "id",
            autowidth: true,
            sorttype: "int",
            width: 5,
            search: true,
        }, {
            name: "nickname",
            index: "nickname",
            width: 10,
            autowidth: true,
        }, {
            name: "realName",
            index: "realName",
            width: 10,
            autowidth: true,
        },{
            name: "phone",
            index: "phone",
            width: 10,
            autowidth: true,
        }, {
            name: "locked",
            index: "locked",
            editoptions: {value: "true:已认证;false:未认证"},
            editable: false,
            edittype: 'select',
            formatter: 'select',
            width: 5,
            autowidth: true,
        }, {
            name: "email",
            index: "email",
            width: 5,
            autowidth: true,
        },{
            name: "lastTime",
            index: "lastTime",
            formatter:lastTimeformatter,
            width: 5,
            autowidth: true,
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: '/api/admin/Qrcode/cellEdit',
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            //dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {
        add: false,
        edit: false,
        del: false,
        search: false,
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

});
function lastTimeformatter(cellvalue, options, rowObject) {
    var date = new Date(rowObject.lastTime * 1).yyyymmddhhmmss();
    return date;
}
$("#perform_search").click(function() {
    clean();
    var postdata = $("#data_list").jqGrid('getGridParam','postData');
    postdata._search = true;
    if($("#id").val()){
        postdata.id = $("#id").val();
    }
    if ($("#phone").val()){
        postdata.phone = $("#phone").val();
    }
    if ($("#nickname").val()){
        postdata.nickname = $("#nickname").val();
    }
    jQuery("#data_list").trigger("reloadGrid", [{page: 1}]);
    //jQuery("#data_list").trigger("reloadGrid");
});
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS"){
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    }else{
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("该角色下有关联的权限，请先删除.");
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id);
        dataFromTheRow.lastTime = new Date(dataFromTheRow.lastTime * 1).yyyymmddhhmmss();
       // var html =  '<a class="J_menuItem" style="color:#f60;padding-right: 10px" href="/user_member_update/' + id + '" data-index="0">详情</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {lastTime: dataFromTheRow.lastTime});
    }
}

function resetPasswd(userId) {
    $.ajax(
        {
            url: Config.resetPasswdUrl,
            type: "POST",
            data: {userId: userId},
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (data && data.type == "SUCCESS") {
                    toastr.success("重置成功。");
                } else {
                    toastr.error(data.msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络异常");
            }
        });
}
function clean() {
    var postData = $('#data_list').jqGrid("getGridParam", "postData");
    console.log(postData);
    if(postData.id){
        console.log(postData.id);
        delete postData.id;
    }
    if (postData.phone){
        console.log(postData.phone);
        delete postData.phone;
    }
    if (postData.nickname){
        console.log(postData.nickname);
        delete postData.nickname;
    }
}