/**
 * Created by neal on 11/22/16.
 */
var pendingRequests = {};
jQuery.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
    var key = originalOptions.url+"_"+originalOptions.type.toUpperCase();
    // console.log(key)
    if (!pendingRequests[key]
        || (pendingRequests[key] && originalOptions.type.toUpperCase() !='POST')) {
        pendingRequests[key] = jqXHR;
        // console.log('start');
    }else{
        jqXHR.abort();    //放弃后触发的提交
        //pendingRequests[key].abort();   // 放弃先触发的提交
        // console.log('abort');
    }

    var complete = options.complete;
    options.complete = function(jqXHR, textStatus) {
        // console.log('complete');
        //TODO: pendingRequests[key] = null;
        setTimeout(function () {
            pendingRequests[key] = null;
        }, 10000);
        if (jQuery.isFunction(complete)) {
            complete.apply(this, arguments);
        }
    }
});
(function($){
    $.fn.serializeJson=function(){
        var serializeObj={};
        var array=this.serializeArray();
        $(array).each(function(){
            if(serializeObj[this.name]){
                if($.isArray(serializeObj[this.name])){
                    serializeObj[this.name].push(this.value);
                }else{
                    serializeObj[this.name]=[serializeObj[this.name],this.value];
                }
            }else{
                serializeObj[this.name]=this.value;
            }
        });
        return serializeObj;
    };
})(jQuery);
Date.prototype.yyyymmddhhmmss = function() {
    var yyyy = this.getFullYear();
    var mm = this.getMonth() < 9 ? "0" + (this.getMonth() + 1) : (this.getMonth() + 1); // getMonth() is zero-based
    var dd  = this.getDate() < 10 ? "0" + this.getDate() : this.getDate();
    var hh = this.getHours() < 10 ? "0" + this.getHours() : this.getHours();
    var min = this.getMinutes() < 10 ? "0" + this.getMinutes() : this.getMinutes();
    var ss = this.getSeconds() < 10 ? "0" + this.getSeconds() : this.getSeconds();
    return "".concat(yyyy).concat("-").concat(mm).concat("-").concat(dd).concat(" ").concat(hh).concat(":").concat(min).concat(":").concat(ss);
};
function toFixedForPrice(price) {
    return (price * 1/ 100).toFixed(2);
}
function toFixedForScore(score) {
    return (score * 1/ 100).toFixed(2);
}
function dateToEpoch(date) {
    return new Date(date).valueOf();
}

function memberPrice(realPrice, screenPrice) {
    // var discount = (realPrice * 100 / screenPrice).toFixed(0);
    // var profit = 0;
    // var multiple = (screenPrice * 1 / 10000).toFixed(0) * 1;
    // if(discount>=80 && discount <= 90){
    //     profit = 100;
    // }else if(discount>=70 && discount < 80){
    //     profit = 200;
    // }else if(discount>=50 && discount<70){
    //     profit = 300;
    // }else if(discount<50){
    //     profit = 400;
    // }
    // return screenPrice * 1 - profit * 8 * multiple;

    var profit = screenPrice * 1 - realPrice * 1;

    return ((screenPrice * 1 - (profit * 70 / 100).toFixed(0)) / 100).toFixed(2);
}

