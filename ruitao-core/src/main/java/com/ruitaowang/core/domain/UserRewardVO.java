package com.ruitaowang.core.domain;

public class UserRewardVO extends UserReward {

    private Long companyId;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
