/**
 * Created by Administrator on 2017/4/22.
 */
$(document).ready(function () {

    var date = new Date;
    var year = (date.getFullYear()).toString() + "年";
    var month = (date.getMonth() + 1).toString() + "月";
    var day = (date.getDate()).toString() + "日";
    $("#month").html(month);
    $("#year").html(year);
    $("#today").html(day);
    $(".chart").easyPieChart({
        barColor: "#f8ac59",
        scaleLength: 5,
        lineWidth: 4,
        size: 80
    });
    $(".chart2").easyPieChart({
        barColor: "#1c84c6",
        scaleLength: 5,
        lineWidth: 4,
        size: 80
    });
    //data
    var dateorderCount = [];
    var dateorderIncome = [];
    var datelocalOrderCount = [];
    var datelocalOrderIncome = [];
    var datedishOrderCount = [];
    var datedishOrderIncome = [];
    //data.data
    var datadataorderCount = [];
    var datadataorderIncome = [];
    var datadatalocalOrderCount = [];
    var datadatalocalOrderIncome = [];
    var datadatadishOrderCount = [];
    var datadatadishOrderIncome = [];
    //count
    var orderc = 0;
    var orderi = 0;
    var orderlc = 0;
    var orderli = 0;
    var orderdc = 0;
    var orderdi = 0;
    console.log(orderc);
    console.log(orderi);
    $.ajax(
        {
            url: "/api/stat/company/income",
            type: "GET",
            data: {days: 365},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                $.each(data.data, function (index, val) {
                    orderc = val.orderCount+orderc;
                    orderi = val.orderIncome+orderi;
                    orderlc = val.localOrderCount+orderlc;
                    orderli = val.localOrderIncome+orderli;
                    orderdc = val.dishOrderCount+orderdc;
                    orderdi = val.dishOrderIncome+orderdi;
                    console.log(orderc);
                    console.log(orderi);
                    datadataorderCount = [cd(val.date), val.orderCount];
                    datadataorderIncome = [cd(val.date), val.orderIncome];
                    datadatalocalOrderCount = [cd(val.date), val.localOrderCount];
                    datadatalocalOrderIncome = [cd(val.date), val.localOrderIncome];
                    datadatadishOrderCount = [cd(val.date), val.dishOrderCount];
                    datadatadishOrderIncome = [cd(val.date), val.dishOrderIncome];
                    dateorderCount.push(datadataorderCount);
                    dateorderIncome.push(datadataorderIncome);
                    datelocalOrderCount.push(datadatalocalOrderCount);
                    datelocalOrderIncome.push(datadatalocalOrderIncome);
                    datedishOrderCount.push(datadatadishOrderCount);
                    datadatadishOrderIncome.push(datadatadishOrderIncome);
                });
                $("#orderCounts").html(orderc);
                $("#orderCount").html(orderc);
                $("#orderIncome").html(orderi);
                $("#localOrderCounts").html(orderlc);
                $("#localOrderCount").html(orderlc);
                $("#localOrderIncome").html(orderli);
                $("#dishOrderCounts").html(orderdc);
                $("#dishOrderCount").html(orderdc);
                $("#dishOrderIncome").html(orderdi)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    console.log(typeof (date4));
    console.log(typeof (date5));
    var datasetstore = [{
        label: "订单数",
        data: dateorderCount,
        color: "#1ab394",
        bars: {show: true, align: "center", barWidth: 24 * 60 * 60 * 600, lineWidth: 0}
    }, {
        label: "收入额",
        data: dateorderIncome,
        color: "#464f88",
        bars: {show: true, align: "center", barWidth: 24 * 60 * 60 * 600, lineWidth: 0}
    }
    ];
    var datasetshop = [{
        label: "订单数",
        data: datelocalOrderCount,
        color: "#1ab394",
        bars: {show: true, align: "center", barWidth: 24 * 60 * 60 * 600, lineWidth: 0}
    }, {
        label: "收入额",
        data: datelocalOrderIncome,
        color: "#464f88",
        bars: {show: true, align: "center", barWidth: 24 * 60 * 60 * 600, lineWidth: 0}
    }
    ];
    var datasetordering = [{
        label: "订单数",
        data: datedishOrderCount,
        color: "#1ab394",
        bars: {show: true, align: "center", barWidth: 24 * 60 * 60 * 600, lineWidth: 0}
    }, {
        label: "收入额",
        data: datadatadishOrderIncome,
        color: "#464f88",
        bars: {show: true, align: "center", barWidth: 24 * 60 * 60 * 600, lineWidth: 0}
    }
    ];
    var options = {
        xaxis: {
            mode: "time",
            tickSize: [1, "day"],
            tickLength: 0,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: "Arial",
            axisLabelPadding: 10,
            color: "#838383",
        },
        yaxes: [
            {
                position: "left",
                max: 10000,
                color: "#838383",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: "Arial",
                axisLabelPadding: 3
            },
            {
                position: "right",
                clolor: "#838383",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: " Arial",
                axisLabelPadding: 67
            }],
        legend: {
            noColumns: 1,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: false,
            borderWidth: 0,
            color: "#838383"
        }
    };

    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime()
    }

    function cd(date) {
        return new Date(date).getTime()
    }

    var previousPoint = null,
        previousLabel = null;
    $.plot($("#flot-store-chart"), datasetstore, options);
    $.plot($("#flot-shop-chart"), datasetshop, options);
    $.plot($("#flot-ordering-chart"), datasetordering, options);
    var mapData = {
        "US": 298,
        "SA": 200,
        "DE": 220,
        "FR": 540,
        "CN": 120,
        "AU": 760,
        "BR": 550,
        "IN": 200,
        "GB": 120,
    };
    $("#world-map").vectorMap({
        map: "world_mill_en",
        backgroundColor: "transparent",
        regionStyle: {
            initial: {
                fill: "#e4e4e4",
                "fill-opacity": 0.9,
                stroke: "none",
                "stroke-width": 0,
                "stroke-opacity": 0
            }
        },
        series: {regions: [{values: mapData, scale: ["#1ab394", "#22d6b1"], normalizeFunction: "polynomial"}]},
    })
});