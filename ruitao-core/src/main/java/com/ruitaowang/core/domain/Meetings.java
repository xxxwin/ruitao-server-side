package com.ruitaowang.core.domain;

public class Meetings extends Page {
    /**
     *  主键,所属表字段为meetings.meeting_id
     */
    private Long meetingId;

    /**
     *  会议标题,所属表字段为meetings.meeting_title
     */
    private String meetingTitle;

    /**
     *  备注,所属表字段为meetings.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为meetings.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为meetings.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为meetings.rstatus
     */
    private Byte rstatus;

    /**
     *  会议价格 单位 ： 分,所属表字段为meetings.meeting_price
     */
    private Integer meetingPrice;

    /**
     *  0 免费
1 收费,所属表字段为meetings.meeting_type
     */
    private Byte meetingType;

    /**
     *  会议内容，富文本,所属表字段为meetings.meeting_content
     */
    private String meetingContent;

    /**
     *  公司编号,所属表字段为meetings.company_id
     */
    private Long companyId;

    /**
     *  会议报名二维码,所属表字段为meetings.qr_sign_up
     */
    private String qrSignUp;

    /**
     *  会议签到,所属表字段为meetings.qr_sign_in
     */
    private String qrSignIn;

    /**
     *  ,所属表字段为meetings.meeting_address
     */
    private String meetingAddress;

    /**
     *  ,所属表字段为meetings.meeting_date
     */
    private String meetingDate;

    /**
     *  ,所属表字段为meetings.meeting_holder
     */
    private String meetingHolder;

    public Long getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(Long meetingId) {
        this.meetingId = meetingId;
    }

    public String getMeetingTitle() {
        return meetingTitle;
    }

    public void setMeetingTitle(String meetingTitle) {
        this.meetingTitle = meetingTitle == null ? null : meetingTitle.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getMeetingPrice() {
        return meetingPrice;
    }

    public void setMeetingPrice(Integer meetingPrice) {
        this.meetingPrice = meetingPrice;
    }

    public Byte getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(Byte meetingType) {
        this.meetingType = meetingType;
    }

    public String getMeetingContent() {
        return meetingContent;
    }

    public void setMeetingContent(String meetingContent) {
        this.meetingContent = meetingContent == null ? null : meetingContent.trim();
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getQrSignUp() {
        return qrSignUp;
    }

    public void setQrSignUp(String qrSignUp) {
        this.qrSignUp = qrSignUp == null ? null : qrSignUp.trim();
    }

    public String getQrSignIn() {
        return qrSignIn;
    }

    public void setQrSignIn(String qrSignIn) {
        this.qrSignIn = qrSignIn == null ? null : qrSignIn.trim();
    }

    public String getMeetingAddress() {
        return meetingAddress;
    }

    public void setMeetingAddress(String meetingAddress) {
        this.meetingAddress = meetingAddress == null ? null : meetingAddress.trim();
    }

    public String getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(String meetingDate) {
        this.meetingDate = meetingDate == null ? null : meetingDate.trim();
    }

    public String getMeetingHolder() {
        return meetingHolder;
    }

    public void setMeetingHolder(String meetingHolder) {
        this.meetingHolder = meetingHolder == null ? null : meetingHolder.trim();
    }
}