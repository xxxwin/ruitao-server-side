package com.ruitaowang.core.domain;

public class Order extends Page {
    /**
     * 编号,所属表字段为goods_order.order_id
     */
    private Long orderId;

    /**
     * 购买者编号,所属表字段为goods_order.user_id
     */
    private Long userId;
    /**
     * 购买者名称 ,所属表字段为goods_order.user_name
     */
    private String userName;

    /**
     * 创建时间,所属表字段为goods_order.ctime
     */
    private Long ctime;

    /**
     * 最后修改时间,所属表字段为goods_order.mtime
     */
    private Long mtime;

    /**
     * 行状态
     * 0 正常，1 删除,所属表字段为goods_order.rstatus
     */
    private Byte rstatus;

    /**
     * 0 微信支付
     * 1 支付宝支付,所属表字段为goods_order.pay_way
     */
    private Byte payWay;

    /**
     * 0 未支付
     * 1 已支付,所属表字段为goods_order.pay_status
     */
    private Byte payStatus;

    /**
     * 审核状态
     * 0 未审核
     * 1 审核通过
     * 2 审核不通过 （原因）,所属表字段为goods_order.audit_status
     */
    private Byte auditStatus;

    /**
     * 订单状态
     * 0 （买家：等待卖家发货；商家：待发货）
     * 1  （买家：商家已发货；商家：已发货）
     * 2 （买家：确认已收获，；商家：已完成）
     * 3 （买家：申请退货，原因；商家：买家申请退货）
     * 4 (买家：申请退款，原因；）
     * 5 交易关闭
     * 6 以配货（龙蛙已经联系过供货商）,所属表字段为goods_order.order_status
     */
    private Byte orderStatus;

    /**
     * ,所属表字段为goods_order.remark
     */
    private String remark;

    /**
     * ,所属表字段为goods_order.audit_user_id
     */
    private Long auditUserId;

    /**
     * 订单唯一编号,所属表字段为goods_order.order_sn
     */
    private String orderSn;

    /**
     * 收货地址,所属表字段为goods_order.shipping_address
     */
    private String shippingAddress;

    /**
     * 快递方式
     * 0 在线支付
     * 1 到付,所属表字段为goods_order.ex_way
     */
    private Byte exWay;

    /**
     * ,所属表字段为goods_order.ex_price
     */
    private Integer exPrice;

    /**
     * ,所属表字段为goods_order.ex_text
     */
    private String exText;

    /**
     * 物流快递单号,所属表字段为goods_order.ex_sn
     */
    private String exSn;

    /**
     * 收货人联系电话,所属表字段为goods_order.mobile
     */
    private String mobile;

    /**
     * 收货人,所属表字段为goods_order.consignee
     */
    private String consignee;

    /**
     * ,所属表字段为goods_order.goods_pay_type
     */
    private Byte goodsPayType;

    /**
     * 订单总金额，不包括运费,所属表字段为goods_order.order_amount
     */
    private Integer orderAmount;

    /**
     * 送的积分数,所属表字段为goods_order.amount_score
     */
    private Integer amountScore;

    /**
     * 返利金额,所属表字段为goods_order.amount_money
     */
    private Integer amountMoney;

    /**
     * ,所属表字段为goods_order.member_price
     */
    private Integer memberPrice;

    /**
     * ,所属表字段为goods_order.order_type
     */
    private Byte orderType;

    /**
     *  付款时间,所属表字段为goods_order.pay_time
     */
    private Long payTime;

    /**
     * 分享者编号,所属表字段为goods_order.share_id
     */
    private Long shareId;

    /**
     * 是否推送，所属表字段为goods_order.push_id
     */
    private Byte pushId;

    public Byte getPushId() {
        return pushId;
    }

    public void setPushId(Byte pushId) {
        this.pushId = pushId;
    }

    public Long getShareId() {
        return shareId;
    }

    public void setShareId(Long shareId) {
        this.shareId = shareId;
    }

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public Byte getOrderType() {
        return orderType;
    }

    public void setOrderType(Byte orderType) {
        this.orderType = orderType;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Byte getPayWay() {
        return payWay;
    }

    public void setPayWay(Byte payWay) {
        this.payWay = payWay;
    }

    public Byte getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Byte payStatus) {
        this.payStatus = payStatus;
    }

    public Byte getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Byte auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Byte getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Byte orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(Long auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn == null ? null : orderSn.trim();
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress == null ? null : shippingAddress.trim();
    }

    public Byte getExWay() {
        return exWay;
    }

    public void setExWay(Byte exWay) {
        this.exWay = exWay;
    }

    public Integer getExPrice() {
        return exPrice;
    }

    public void setExPrice(Integer exPrice) {
        this.exPrice = exPrice;
    }

    public String getExText() {
        return exText;
    }

    public void setExText(String exText) {
        this.exText = exText == null ? null : exText.trim();
    }

    public String getExSn() {
        return exSn;
    }

    public void setExSn(String exSn) {
        this.exSn = exSn == null ? null : exSn.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee == null ? null : consignee.trim();
    }

    public Byte getGoodsPayType() {
        return goodsPayType;
    }

    public void setGoodsPayType(Byte goodsPayType) {
        this.goodsPayType = goodsPayType;
    }

    public Integer getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Integer orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Integer getAmountScore() {
        return amountScore;
    }

    public void setAmountScore(Integer amountScore) {
        this.amountScore = amountScore;
    }

    public Integer getAmountMoney() {
        return amountMoney;
    }

    public void setAmountMoney(Integer amountMoney) {
        this.amountMoney = amountMoney;
    }

    public Integer getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(Integer memberPrice) {
        this.memberPrice = memberPrice;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}