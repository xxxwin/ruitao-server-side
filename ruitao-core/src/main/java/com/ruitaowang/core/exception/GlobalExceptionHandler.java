//package com.ruitaowang.core.exception;
//
//import com.ruitaowang.core.web.MsgOut;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.authentication.BadCredentialsException;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.validation.BindException;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.multipart.MultipartException;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.IOException;
//
///**
// * 全局异常处理类
// * Created by neal on 9/22/16.
// */
//@ControllerAdvice
//public class GlobalExceptionHandler {
//
//    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
//    private @Value("${spring.http.multipart.max-file-size}") String maxFileSize;
//
//    @ExceptionHandler({IOException.class})
//    public ModelAndView defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
//        LOGGER.error("[error] --> {}", e);
//        ModelAndView mav = new ModelAndView();
//        mav.addObject("exception", e);
//        mav.addObject("url", req.getRequestURL());
//        mav.setViewName("error");
//        return mav;
//    }
//
//    @ExceptionHandler({AccessDeniedException.class, AppException.class, BindException.class})
//    @ResponseBody
//    public MsgOut json(Exception ex) {
//        //TODO:记录日志
//        LOGGER.error("AppException", ex);
//
//        return MsgOut.error(ex);
//    }
//
//    @ExceptionHandler(MultipartException.class)
//    @ResponseBody
//    public MsgOut multipart(MultipartException e) {
//        return MsgOut.error("最大文件: {}, 请检查您的文件大小。", maxFileSize);
//    }
//    @ExceptionHandler({BadCredentialsException.class, AuthenticationException.class, UsernameNotFoundException.class})
//    @ResponseBody
//    public MsgOut login(Exception ex) {
//        //TODO:记录日志
//        LOGGER.error("AuthenticationException", ex);
//
//        return MsgOut.error(ex);
//    }
//}
