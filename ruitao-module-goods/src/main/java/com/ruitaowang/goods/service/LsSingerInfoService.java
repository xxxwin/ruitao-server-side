package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.LsSingerInfo;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.LsSingerInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class LsSingerInfoService implements BaseService<LsSingerInfo> {
    @Autowired
    private LsSingerInfoMapper LsSingerInfoMapper;

    @Override
    public LsSingerInfo insert(LsSingerInfo LsSingerInfo) {
        Long time = System.currentTimeMillis();
        LsSingerInfo.setCtime(time);
        LsSingerInfo.setMtime(time);
        LsSingerInfoMapper.insert(LsSingerInfo);
        return LsSingerInfo;
    }

    @Override
    public int delete(Long id) {
        LsSingerInfo LsSingerInfo = new LsSingerInfo();
        LsSingerInfo.setSingerId(id);
        LsSingerInfo.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        LsSingerInfo.setMtime(time);
        return LsSingerInfoMapper.updateByPK(LsSingerInfo);
    }

    @Override
    public LsSingerInfo update(LsSingerInfo LsSingerInfo) {
        Long time = System.currentTimeMillis();
        LsSingerInfo.setMtime(time);
        LsSingerInfoMapper.updateByPK(LsSingerInfo);
        return LsSingerInfo;
    }

    @Override
    public LsSingerInfo selectByPK(Long id) {

        return LsSingerInfoMapper.selectByPK(id);
    }

    @Override
    public List<LsSingerInfo> select(LsSingerInfo LsSingerInfo) {
        LsSingerInfo.setRstatus((byte) 0);
        return LsSingerInfoMapper.select(LsSingerInfo);
    }

    public List<LsSingerInfo> selectForPage(LsSingerInfo LsSingerInfo) {
        LsSingerInfo.setRstatus((byte) 0);
        LsSingerInfo.setRecords(LsSingerInfoMapper.count(LsSingerInfo));
        return LsSingerInfoMapper.select(LsSingerInfo);

    }
}

