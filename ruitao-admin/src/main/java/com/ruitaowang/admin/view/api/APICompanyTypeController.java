package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.goods.utils.RStatus;
import com.ruitaowang.core.domain.CompanyType;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class APICompanyTypeController extends BaseController {

    @Autowired
    private CompanyTypeService companyTypeService;

    @RequestMapping("/api/admin/type/select")
    public String select(CompanyType companyType,int page){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(companyType.getSidx())) {
            companyType.setOrderBy(companyType.getSidx() + " "
                    + companyType.getSord() + "," + companyType.getOrderBy());
        }
        companyType.setPage(page);
        MsgOut msgOut=MsgOut.success(companyTypeService.selectForPage(companyType));
        msgOut.setTotal(companyType.getTotal());
        msgOut.setRecords(companyType.getRecords());
        return this.renderJson(msgOut);
    }
    @RequestMapping(value = {"/api/admin/type/selectByPk/{id}"}, method = RequestMethod.GET)
    public String selectByPK(@PathVariable("id") Long typeId){
        MsgOut msgOut=MsgOut.success(companyTypeService.selectByPK(typeId));
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/admin/type/selectAll")
    public String select(CompanyType companyType){
        List<CompanyType> types= companyTypeService.select(companyType);
        if(ObjectUtils.isEmpty(types)){
            return this.renderJson(MsgOut.success("nodata"));
        }
        MsgOut msgOut=MsgOut.success(types);
        return this.renderJson(msgOut);
    }
    @RequestMapping("/api/wap/type/onlyOne/{id}")
    public String wapSelectOne(@PathVariable("id") Long id){
        CompanyType companyType= companyTypeService.selectByPK(id);
        if(ObjectUtils.isEmpty(companyType)){
            return this.renderJson(MsgOut.success("nodata"));
        }
        MsgOut msgOut=MsgOut.success(companyType);
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/admin/type/selectByIndustry/{id}")
    public String select(@PathVariable("id") Long id){
        CompanyType companyType =new CompanyType();
        companyType.setIndustryId(id);
        companyType.setOrderBy("ctime desc");
        List<CompanyType> types= companyTypeService.select(companyType);
        if(ObjectUtils.isEmpty(types)){
            return this.renderJson(MsgOut.success("nodata"));
        }
        MsgOut msgOut=MsgOut.success(types);
        return this.renderJson(msgOut);
    }

    /**
     * 行业选择  调取小分类
     * @param id
     * @return
     */
    @RequestMapping("/api/wap/type/selectByIndustry/{id}")
    public String wapSelect(@PathVariable("id") Long id){
        CompanyType companyType =new CompanyType();
        companyType.setIndustryId(id);
        List<CompanyType> types= companyTypeService.select(companyType);
        if(ObjectUtils.isEmpty(types)){
            return this.renderJson(MsgOut.success("nodata"));
        }
        MsgOut msgOut=MsgOut.success(types);
        return this.renderJson(msgOut);
    }
    @RequestMapping("/api/admin/type/insert")
    public String insert(CompanyType companyType){
        MsgOut msgOut=MsgOut.success(companyTypeService.insert(companyType));
        return this.renderJson(msgOut);
    }

    @RequestMapping("/api/admin/type/update")
    public String update(CompanyType companyType){
        companyType.setMtime(System.currentTimeMillis());
        MsgOut msgOut=MsgOut.success(companyTypeService.update(companyType));
        return this.renderJson(msgOut);
    }

    @RequestMapping(value = {"/api/admin/type/delete/{id}"}, method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id){
        MsgOut msgOut = MsgOut.success(companyTypeService.delete(id));
        return this.renderJson(msgOut);
    }
}
