/**
 * Created by Shaka on 12/21/16.
 */
var Config = {
    selectUrl: '/api/admin/dc/dish',
    deleteUrl: '/api/admin/dc/dish',
    updateUrlForCellEdit: '/api/admin/dc/dish/cellEdit',
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#dc_dish").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["菜品编号", "公司", "菜品名称", "菜品图片", "菜品价格(元)", "菜品分类", "推荐菜", "菜品描述", "时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: true
        }, {
            name: "companyId",
            index: "companyId"
        }, {
            name: "name",
            index: "name"
        }, {
            name: "thum",
            index: "thum"
        }, {
            name: "price",
            index: "price",
            sorttype: "int"
        }, {
            name: "categoryId",
            index: "categoryId",
            formatter: mydateformatter,
            autowidth: true,
            sorttype: "int"
        }, {
            name: "isTuijian",
            index: "isTuijian",
            autowidth: true,
            editable: true,
            sortable: true,
            editoptions: {value: "0:未推荐;1:推荐"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "remark",
            index: "remark"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        pager: "#pager_dc_dish",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#dc_dish").jqGrid('navGrid', '#pager_dc_dish', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#dc_dish").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#dc_dish").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#dc_dish").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#dc_dish").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#dc_dish').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        console.log(dataFromTheRow);
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/web/dc_dish_create.html?id=' + dataFromTheRow.companyId + '" data-index="0">添加</a>';
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/admin/dc/dish/update/' + id + '" data-index="0">修改</a>';
        var avatar = '<img style="width: 50px; height: 50px;" src="' + dataFromTheRow.thum + '"/>';
        jQuery("#dc_dish").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
            thum: avatar,
            price: toFixedForPrice(dataFromTheRow.price)
        });
    }
}
function mydateformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax({
        url: "/api/admin/dc/categories/" + rowObject.categoryId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            name = data.data.name;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return name;
}

