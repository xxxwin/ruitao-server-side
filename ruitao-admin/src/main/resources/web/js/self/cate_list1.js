var Config = {
    selectUrl: "/api/admin/categories",
    deleteUrl: "/api/admin/cate/delete",
};

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        postData: {level:1},
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 100,
        colNames: ["编号", "分类名称", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            sorttype: "int",
            width: 5,
            search: true,
            key: true
        }, {
            name: "name",
            index: "name",
            width: 15,
            search: true
        }, {
            name: "operate",
            index: "operate",
            width: 17,
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        multiselect: true,
        multiboxonly: true,
        onSelectRow: function (id, status, e) {
            console.log($(e.target).parent().parent());
            // var item = $(e.target).parent().parent();
            // $(item).attr("aria-selected", "true");
            throw Error("");
        },
        beforeSelectRow: function (rowId, e) {
            // console.log("beforeSelectRow",rowId)
            return $(e.target).is('input[type=checkbox]');
        },
        viewrecords: true,
        hidegrid: false,
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "orderId"
        },
        gridComplete: function () {
            dataWrapper1();
        }
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

    $.ajax({
        url: '/api/admin/categories',
        type: "GET",
        data:{level:1},
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            var categories = [];
            categories.push('<option value="0" hassubinfo="true">请选择分类</option>');
            data.data.map(function (category) {
                categories.push('<option value="');
                categories.push(category.id);
                categories.push('" ');
                categories.push('hassubinfo="true">');
                categories.push(category.name);
                categories.push('</option>');
            });
            $("#cateLevel1").html(categories.join(''));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });

    $("#cateLevel1").change(function () {
        $.ajax({
            url: '/api/admin/categories',
            type: "GET",
            data:{level:2,parentId:$("#cateLevel1 option:selected").val()},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="0" hassubinfo="true">请选择分类</option>');
                data.data.map(function (goodsTypeCustom) {
                    categories.push('<option value="');
                    categories.push(goodsTypeCustom.id);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(goodsTypeCustom.name);
                    categories.push('</option>');
                });
                $("#cateLevel2").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    })


    $("#cateLevel2").change(function () {
        $.ajax({
            url: '/api/admin/categories',
            type: "GET",
            data:{level:3,parentId:$("#cateLevel2 option:selected").val()},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="0" hassubinfo="true">请选择分类</option>');
                data.data.map(function (goodsTypeCustom) {
                    categories.push('<option value="');
                    categories.push(goodsTypeCustom.id);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(goodsTypeCustom.name);
                    categories.push('</option>');
                });
                $("#cateLevel3").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    })
});

function dataWrapper1() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='xiangqing1(" + id + ")'>下一级</a>";
        html += "<a style='color:#f60;padding-right: 15px' onclick='edit1(" + id + ")'>编辑</a>";
        html += "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")'>删除</a>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html});
    }
}

function xiangqing1(id) {
    layer.open({
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content: "/cateList2/"+id
    })
}

function edit1(id) {
    layer.open({
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content: "/cate/update2/"+id
    })
}

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#data_list").setGridParam({url: url}).trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}

function xiangqing11111111111(id) {
    $("#data_list1").jqGrid({
        datatype: "json",
        url: '/api/admin/categories',
        postData: {level:2,parentId:id},
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 100,
        colNames: ["编号", "分类名称", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            sorttype: "int",
            width: 5,
            search: true,
            key: true
        }, {
            name: "name",
            index: "name",
            width: 15,
            search: true
        }, {
            name: "operate",
            index: "operate",
            width: 17,
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        multiselect: true,
        multiboxonly: true,
        onSelectRow: function (id, status, e) {
            console.log($(e.target).parent().parent());
            // var item = $(e.target).parent().parent();
            // $(item).attr("aria-selected", "true");
            throw Error("");
        },
        beforeSelectRow: function (rowId, e) {
            // console.log("beforeSelectRow",rowId)
            return $(e.target).is('input[type=checkbox]');
        },
        pager: "#pager_data_list",
        viewrecords: true,
        hidegrid: false,
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "orderId"
        },
        gridComplete: function () {
            dataWrapper2();
        }
    });
}

function dataWrapper2() {
    var ids = jQuery("#data_list1").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list1').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='xiangqing2(" + id + ")'>详情</a>";
        jQuery("#data_list1").jqGrid('setRowData', ids[i], {operate: html});
    }
}

function xiangqing2(id) {
    $("#data_list2").jqGrid({
        datatype: "json",
        url: '/api/admin/categories',
        postData: {level:3,parentId:id},
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 100,
        colNames: ["编号", "分类名称", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            sorttype: "int",
            width: 5,
            search: true,
            key: true
        }, {
            name: "name",
            index: "name",
            width: 15,
            search: true
        }, {
            name: "operate",
            index: "operate",
            width: 17,
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        multiselect: true,
        multiboxonly: true,
        onSelectRow: function (id, status, e) {
            console.log($(e.target).parent().parent());
            // var item = $(e.target).parent().parent();
            // $(item).attr("aria-selected", "true");
            throw Error("");
        },
        beforeSelectRow: function (rowId, e) {
            // console.log("beforeSelectRow",rowId)
            return $(e.target).is('input[type=checkbox]');
        },
        pager: "#pager_data_list",
        viewrecords: true,
        hidegrid: false,
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "orderId"
        },
        gridComplete: function () {
            dataWrapper3();
        }
    });
}

function dataWrapper3() {
    var ids = jQuery("#data_list2").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list2').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick=''>详情</a>";
        html += "<a style='color:#f60;padding-right: 15px' onclick=''>关闭</a>"
        jQuery("#data_list2").jqGrid('setRowData', ids[i], {operate: html});
    }
}