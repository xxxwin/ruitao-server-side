package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.GoodsAlbum;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.utils.RandomUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.AddressService;
import com.ruitaowang.goods.service.GoodsAlbumService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APIGoodsAlbumController extends BaseController {

    @Autowired
    private GoodsAlbumService goodsAlbumService;

    @RequestMapping("/api/goodsAlbums")
    public String list(GoodsAlbum goodsAlbum, int page) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        goodsAlbum.setPage(page);
        MsgOut o = MsgOut.success(goodsAlbumService.selectForPage(goodsAlbum));
        o.setRecords(goodsAlbum.getRecords());
        o.setTotal(goodsAlbum.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping("/api/goodsAlbums/{addressId}")
    public String selectByPK(@PathVariable Long addressId) {
        return this.renderJson(MsgOut.success(goodsAlbumService.selectByPK(addressId)));
    }

    @RequestMapping(value = "/api/goodsAlbums", method = RequestMethod.POST)
    public String create(GoodsAlbum goodsAlbum) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        goodsAlbumService.insert(goodsAlbum);
        MsgOut o = MsgOut.success(goodsAlbum);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/goodsAlbums", method = RequestMethod.PUT)
    public String update(GoodsAlbum goodsAlbum) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }

        goodsAlbumService.update(goodsAlbum);
        MsgOut o = MsgOut.success(goodsAlbum);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/goodsAlbums/{imageId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("addressId") Long addressId) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        GoodsAlbum goodsAlbum = goodsAlbumService.selectByPK(addressId);
        return this.renderJson(MsgOut.error("denied"));
    }
}
