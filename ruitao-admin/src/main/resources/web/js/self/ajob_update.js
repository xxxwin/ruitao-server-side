
var Config = {
    updateUrl: '/api/ajobs/update',
};

$().ready(function () {

    //选择服务器
    if($("#ajobIpMap").val() == "10.29.24.137"){
        $("#ajobIp").val($("#ajobIpMap").val());
    } else if($("#ajobIpMap").val() == "10.27.143.116"){
        $("#ajobIp").val($("#ajobIpMap").val());
    } else if($("#ajobIpMap").val() == "10.171.55.220"){
        $("#ajobIp").val($("#ajobIpMap").val());
    } else {
        $("#ajobIp").val("0");
    }

    //是否使用
    $("#ajobType").val($("#ajobTypeMap").val());

    $('#submitAjobUpdate').click(function () {

        $("#ajaxform").validate();
        //如果验证通过
        if ($('#ajaxform').valid()) {
            if($("#ajobIp").val() == "0"){
                toastr.error("请选择服务器！");
                return false;
            }
            if($("#ajobType").val() == "0"){
                toastr.error("请选择是否使用！");
                return false;
            }
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url: Config.updateUrl,
                    type: "PUT",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});

$('#buttonGoback').click(function() {
    history.go(-1);
    location.reload();
    return false;
});