package com.ruitaowang.core.domain;

public class GoodsTypeLink extends Page {
    /**
     *  ,所属表字段为goods_type_link.id
     */
    private Long id;

    /**
     *  商品类型名称,所属表字段为goods_type_link.goods_id
     */
    private Long goodsId;

    /**
     *  ,所属表字段为goods_type_link.type_id
     */
    private Long typeId;

    /**
     *  创建时间,所属表字段为goods_type_link.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为goods_type_link.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常
1 删除,所属表字段为goods_type_link.rstatus
     */
    private Byte rstatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}