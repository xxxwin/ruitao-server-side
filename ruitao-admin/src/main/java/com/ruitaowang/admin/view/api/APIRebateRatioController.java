package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.RebateRatio;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.RebateRatioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by xinchunting on 18-1-15.
 */
@RestController
public class APIRebateRatioController extends BaseController {

    @Autowired
    private RebateRatioService rebateRatioService;



    @RequestMapping(value = "/api/admin/rebateRatio", method = RequestMethod.POST)
    public String adminRebateRatioCreate(RebateRatio rebateRatio) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }

        MsgOut o = MsgOut.success(rebateRatioService.insert(rebateRatio));
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/wap/rebateRatio", method = RequestMethod.POST)
    public String wapRebateRatioCreate(RebateRatio rebateRatio) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(rebateRatioService.insert(rebateRatio));
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/admin/rebateRatio", method = RequestMethod.PUT)
    public String adminRebateRatioUpdate(RebateRatio rebateRatio) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(rebateRatio)) {
            return this.renderJson(MsgOut.error("参数错误"));
        }
        if (ObjectUtils.isEmpty(rebateRatio.getGoodsId())) {
            return this.renderJson(MsgOut.error("参数错误"));
        }
        RebateRatio ratio=new RebateRatio();
        ratio.setGoodsId(rebateRatio.getGoodsId());
        List<RebateRatio> rebateRatios=rebateRatioService.select(ratio);


        if (ObjectUtils.isEmpty(rebateRatios)){
            rebateRatioService.insert(rebateRatio);
        }else {
            rebateRatio.setId(rebateRatios.get(0).getId());
            rebateRatioService.update(rebateRatio);
        }

        MsgOut o = MsgOut.success(rebateRatio);
        return this.renderJson(o);
    }
}