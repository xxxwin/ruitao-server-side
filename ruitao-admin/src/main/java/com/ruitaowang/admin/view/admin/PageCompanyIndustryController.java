package com.ruitaowang.admin.view.admin;

import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.CompanyIndustry;
import com.ruitaowang.goods.service.CompanyIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class PageCompanyIndustryController {
    @Autowired
    private CompanyIndustryService companyIndustryService;
    @RequestMapping(value = {"/industry/update/{id}"})
    public ModelAndView update(@PathVariable("id") Long id,Model model){
        CompanyIndustry companyIndustry=companyIndustryService.selectByPK(id);
        model.addAttribute("industry", companyIndustry);
        return new ModelAndView("web/company_industry_update");
    }
}
