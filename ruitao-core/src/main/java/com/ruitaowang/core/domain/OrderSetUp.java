package com.ruitaowang.core.domain;

import lombok.Data;

@Data
public class OrderSetUp extends Page {

    /**
     *  是否支持售后 0 支持 1 不支持
     */
    private String isAfterSale;

    /**
     *  取消订单时间 毫秒数
     */
    private Long cancelTime;

    /**
     *  自动收货时间 毫秒数
     */
    private Long receivingGoodsTime;

    /**
     *  支付类型设置 0 微信支付
     */
    private Long payStatus;

    /**
     *  退货描述
     */
    private String returnDescription;
    /**
     *  退货描述
     */
    private String refundDescription;
}