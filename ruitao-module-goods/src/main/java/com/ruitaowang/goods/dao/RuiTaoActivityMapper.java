package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.*;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RuiTaoActivityMapper  {

    @Select("SELECT  g.user_id,g.pay_status,sum(g.ex_price) as ex_price ,sum(g.order_amount) as order_amount,sum(g.member_price) as member_price,g.mobile,g.consignee,o.goods_provider_type FROM  rtshop.order_prod o,rtshop.goods_order g  where o.order_id=g.order_id and g.pay_status=1 and o.goods_provider_type != 1 and g.ctime>=#{stime,jdbcType=BIGINT} group by g.user_id order by sum(g.order_amount) desc;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "payStatus", column = "pay_status"),
            @Result(property = "mobile", column = "mobile"),
            @Result(property = "consignee", column = "consignee"),
            @Result(property = "orderAmount", column = "order_amount"),
            @Result(property = "exPrice", column = "ex_price"),
            @Result(property = "memberPrice", column = "member_price"),
    })
    List<Order> selectShare(Order order);

    @Select("select o.goods_provider_id,sum(o.goods_screen_price) as goods_screen_price,o.goods_provider_type from rtshop.order_prod o,rtshop.goods_order g where o.order_id=g.order_id and g.pay_status=1 and o.goods_provider_type=1 and o.ctime >= #{stime,jdbcType=BIGINT} and o.ctime <= #{etime,jdbcType=BIGINT} group by o.goods_provider_id order by sum(o.goods_screen_price) desc limit 10")
    @Results({
            @Result(property = "goodsProviderId", column = "goods_provider_id"),
            @Result(property = "goodsProviderType", column = "goods_provider_type"),
            @Result(property = "goodsScreenPrice", column = "goods_screen_price")
    })
    List<OrderProd> selectTinyShop(OrderProd orderProd);

    @Select("select user_id,sum(user_profit) as user_profit from rtshop.user_profit where order_type=7 and ctime>=#{stime,jdbcType=BIGINT} and ctime <= #{etime,jdbcType=BIGINT} group by user_id order by sum(user_profit) desc limit 10")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "userProfit", column = "user_profit")
    })
    List<UserProfit> selectFastTurn(UserProfit userProfit);

    @Select("select company_id, company_name, linkman, ctime, mobile from rtshop.company where ctime >= #{stime,jdbcType=BIGINT} group by company_id order by ctime limit 20")
    @Results({
            @Result(property = "companyId", column = "company_id"),
            @Result(property = "companyName", column = "company_name"),
            @Result(property = "linkman", column = "linkman"),
            @Result(property = "ctime", column = "ctime"),
            @Result(property = "mobile", column = "mobile"),
    })
    List<Company> selectCompanyRegister(Company company);
}