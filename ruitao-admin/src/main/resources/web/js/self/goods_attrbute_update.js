/**
 * Created by Shaka on 11/22/16.
 */

var Config = {
    createUrl: '/api/goodsAttribute',
    updateUrl: '/api/goodsAttribute',
    deleteUrl: '/api/goodsAttribute',
    selectUrl: '/api/goodsAttribute'
};

var ConfigType = {
    selectUrl: '/api/admin/goodsType/get'
};


$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$().ready(function () {


    $.ajax(
        {
            url: ConfigType.selectUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var goodsType = [];
                goodsType.push('<option value="0" hassubinfo="true">所属类型</option>');
                data.data.map(function (category) {
                    goodsType.push('<option value="');
                    goodsType.push(category.typeId);
                    goodsType.push('" ');
                    if (goodsTypeIdChecked == category.typeId) {
                        goodsType.push('selected="selected" ');
                    }
                    goodsType.push('hassubinfo="true">');
                    goodsType.push(category.typeName);
                    goodsType.push('</option>');
                });
                $("#typeId").html(goodsType.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });

    $('#submitUpdate').click( function() {
        $("#ajaxform").validate();
        if ($("#typeId option:selected").val() == 0){
            toastr.warning("请填写所属类型", "警告");
            return false;
        }
        if ($('#ajaxform').valid()){
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.updateUrl,
                    type: "PUT",
                    data : postData,
                    dataType: "json",
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });

    $('#buttonGoback').click(function() {
        history.go(-1);
        location.reload();
        return false;
    });
});
