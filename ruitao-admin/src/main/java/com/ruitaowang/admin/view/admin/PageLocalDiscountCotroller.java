package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.LocalDiscount;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.LocalDiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;


@RestController
public class PageLocalDiscountCotroller extends BaseController {

    @Autowired
    LocalDiscountService localDiscountService;


    @RequestMapping(value = {"/localDiscount/update/{discountId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("discountId") Long discountId, Model model) {
        LocalDiscount localDiscount = localDiscountService.selectByPK(discountId);
        model.addAttribute("localDiscount", localDiscount);
        return new ModelAndView("web/local_discount_update");
    }

}
