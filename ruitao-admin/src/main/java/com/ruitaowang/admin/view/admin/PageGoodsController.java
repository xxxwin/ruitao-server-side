package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.expression.Lists;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;

/**
 * 商品信息
 */
@RestController
public class PageGoodsController extends BaseController {

    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsAttributeLinkService goodsAttributeLinkService;
    @Autowired
    private GoodsAttributeService goodsAttributeService;
    @Autowired
    private RebateRatioService rebateRatioService;
    @Autowired
    private CategoryService categoryService;


    /**
     * 商品更新
     */
    @RequestMapping(value = {"/ws/goods/update/{goodsId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView wsUpdate(@PathVariable("goodsId") Long goodsId, Model model) {
        Goods goods = goodsService.selectByPK(goodsId);
        GoodsAlbum goodsAlbum = new GoodsAlbum();
        goodsAlbum.setGoodsId(goodsId);
        goodsAlbum.setImageType((byte) 1);

        model.addAttribute("goods", goods);
        model.addAttribute("goodsAlbum", goodsService.selectAlbum(goodsAlbum));

        //返利
        RebateRatio rebateRatio=new RebateRatio();
        rebateRatio.setGoodsId(goodsId);
        List<RebateRatio> rebateRatios=rebateRatioService.select(rebateRatio);
        if (!ObjectUtils.isEmpty(rebateRatios)){
            model.addAttribute("rebateRatio",rebateRatios.get(0));
        }else {
            rebateRatio.setOneLevel((byte)0);
            rebateRatio.setTwoLevel((byte)0);
            rebateRatio.setAreaLevel((byte)0);
            rebateRatio.setCityLevel((byte)0);
            rebateRatio.setProvinceLevel((byte)0);
            rebateRatio.setRtLevel((byte)0);
            model.addAttribute("rebateRatio",rebateRatio);
        }


        GoodsAttributeLink goodsAttributeLink = new GoodsAttributeLink();
        goodsAttributeLink.setGoodsId(goodsId);
        goodsAttributeLink.setOrderBy("id");
        goodsAttributeLink.setRstatus((byte)0);
        List<GoodsAttributeLink> goodsAttributeLinkList = goodsAttributeLinkService.select(goodsAttributeLink);
        model.addAttribute("attrbuteLink", goodsAttributeLinkList);

        if (goodsAttributeLinkList != null && goodsAttributeLinkList.size() > 0) {
            GoodsAttribute goodsAttribute = goodsAttributeService.selectByPK(goodsAttributeLinkList.get(0).getAttrId());
            if (goodsAttribute != null )
                model.addAttribute("goodTypeId", goodsAttribute.getTypeId());
            else
                model.addAttribute("goodTypeId", null);
        } else {
            model.addAttribute("goodTypeId", null);
        }

        return new ModelAndView("web/ws_goods_update");
    }
    @RequestMapping(value = {"/goods/update/{goodsId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("goodsId") Long goodsId, Model model) {
        Goods goods = goodsService.selectByPK(goodsId);
        GoodsAlbum goodsAlbum = new GoodsAlbum();
        goodsAlbum.setGoodsId(goodsId);
        goodsAlbum.setImageType((byte) 1);
        model.addAttribute("goods", goods);
        model.addAttribute("goodsAlbum", goodsService.selectAlbum(goodsAlbum));
        //返利
        RebateRatio rebateRatio=new RebateRatio();
        rebateRatio.setGoodsId(goodsId);
        List<RebateRatio> rebateRatios=rebateRatioService.select(rebateRatio);
        if (!ObjectUtils.isEmpty(rebateRatios)){
            model.addAttribute("rebateRatio",rebateRatios.get(0));
        }else {
            rebateRatio.setOneLevel((byte)0);
            rebateRatio.setTwoLevel((byte)0);
            rebateRatio.setAreaLevel((byte)0);
            rebateRatio.setCityLevel((byte)0);
            rebateRatio.setProvinceLevel((byte)0);
            rebateRatio.setRtLevel((byte)0);
            model.addAttribute("rebateRatio",rebateRatio);
        }
        String name = "未选择分类";
        if (!ObjectUtils.isEmpty(goods.getCateId())){
                name=categoryService.selectCategoryName(goods.getCateId());
            if (!ObjectUtils.isEmpty(name)){
                String []s=name.split(",");

                if (s.length ==3){
                    name=s[2]+"===>"+s[1]+"===>"+s[0];
                }
                if (s.length ==2){
                    name=s[1]+"===>"+s[0];
                }
                if (s.length ==1){
                    name=s[0];
                }
            }
        }
        model.addAttribute("categoryName",name);
        GoodsAttributeLink goodsAttributeLink = new GoodsAttributeLink();
        goodsAttributeLink.setGoodsId(goodsId);
        goodsAttributeLink.setOrderBy("id");
        goodsAttributeLink.setRstatus((byte)0);
        List<GoodsAttributeLink> goodsAttributeLinkList = goodsAttributeLinkService.select(goodsAttributeLink);
        model.addAttribute("attrbuteLink", goodsAttributeLinkList);

        if (goodsAttributeLinkList != null && goodsAttributeLinkList.size() > 0) {
            GoodsAttribute goodsAttribute = goodsAttributeService.selectByPK(goodsAttributeLinkList.get(0).getAttrId());
            if (goodsAttribute != null )
                model.addAttribute("goodTypeId", goodsAttribute.getTypeId());
            else
                model.addAttribute("goodTypeId", null);
        } else {
            model.addAttribute("goodTypeId", null);
        }

        return new ModelAndView("web/goods_update");
    }

    @RequestMapping(value = {"/admin/web/set_up_warning"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView setUpWarning() {
        return new ModelAndView("web/set_up_warning");
    }
}