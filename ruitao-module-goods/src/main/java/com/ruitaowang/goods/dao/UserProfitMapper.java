package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserProfit;
import java.util.List;

public interface UserProfitMapper {
    int insert(UserProfit userProfit);

    int deleteByPK(Long profitId);

    int updateByPK(UserProfit userProfit);

    UserProfit selectByPK(Long profitId);

    long count(UserProfit userProfit);

    List<UserProfit> select(UserProfit userProfit);
}