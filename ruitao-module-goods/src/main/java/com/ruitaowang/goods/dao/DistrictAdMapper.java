package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.District;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DistrictAdMapper {

    List<District> selectRepeat(District district);

    List<District> selectByName(District district);
    @Select("SELECT id,name,pinyin FROM rtshop.district where parentid > 4 and parentid < 32")
    List<District> selectByCity(District district);
}