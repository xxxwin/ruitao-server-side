package com.ruitaowang.core.domain;

public class UserLock extends Page {
    /**
     *  主键,所属表字段为user_lock.user_id
     */
    private Long userId;

    /**
     *  申请次数,所属表字段为user_lock.district_id
     */
    private Long districtId;

    /**
     *  备注,所属表字段为user_lock.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为user_lock.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为user_lock.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为user_lock.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为user_lock.province
     */
    private String province;

    /**
     *  ,所属表字段为user_lock.city
     */
    private String city;

    /**
     *  ,所属表字段为user_lock.district
     */
    private String district;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }
}