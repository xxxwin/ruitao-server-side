package com.ruitaowang.goods.service;


import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.Order;
import com.ruitaowang.core.domain.OrderProd;
import com.ruitaowang.core.domain.UserProfit;
import com.ruitaowang.goods.dao.RuiTaoActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/5/21.
 */
@Service
public class RuiTaoActivityService {

    @Autowired
    private RuiTaoActivityMapper ruiTaoActivityMapper;

    public List<Order> selectShare(Order order) {
        return ruiTaoActivityMapper.selectShare(order);
    }
    public List<OrderProd> selectTinyShop(OrderProd orderProd) {
        return ruiTaoActivityMapper.selectTinyShop(orderProd);
    }
    public List<UserProfit> selectFastTurn(UserProfit userProfit) {
        return ruiTaoActivityMapper.selectFastTurn(userProfit);
    }
    public List<Company> selectCompanyRegister(Company company) {
        return ruiTaoActivityMapper.selectCompanyRegister(company);
    }

}
