package com.ruitaowang.admin.view.api;

import com.ruitaowang.core.domain.LocalDiscount;
import com.ruitaowang.core.utils.RandomUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.LocalDiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Shaka on 1/3/17.
 */
@RestController
public class APILocalDiscountController extends BaseController {

    @Autowired
    private LocalDiscountService localDiscountService;

    @RequestMapping("/api/localDiscount")
    public String list() {
        List<LocalDiscount> list;
        LocalDiscount localDiscount = new LocalDiscount();
        list = localDiscountService.select(localDiscount);
        MsgOut o = MsgOut.success(list);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/localDiscount", method = RequestMethod.POST)
    public String create(LocalDiscount localDiscount) {
//        Long userId = RandomUtils.userId(this.getRequest());
//        if(userId == 0l){
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
        localDiscountService.insert(localDiscount);
        MsgOut o = MsgOut.success(localDiscount);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/localDiscount", method = RequestMethod.PUT)
    public String update(LocalDiscount localDiscount) {

        localDiscountService.update(localDiscount);
        MsgOut o = MsgOut.success(localDiscount);
        return this.renderJson(o);
    }


    @RequestMapping(value = "/api/localDiscount/{localDiscountId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Long localDiscountId) {
        MsgOut o = MsgOut.success(localDiscountService.delete(localDiscountId));
        return this.renderJson(o);
    }
}
