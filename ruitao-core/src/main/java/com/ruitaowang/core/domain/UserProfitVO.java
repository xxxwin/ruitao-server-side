package com.ruitaowang.core.domain;

public class UserProfitVO extends UserProfit {
    private String nickname;
    private String headimgurl;
    private String strcompanytype;
    private String strordertrpe;
    private String strtime;
    private Double userProfitvo;
    private Integer userExpenses;
    private Integer userScoreExpenses;
    private Byte rewardStatus;

    public Byte getRewardStatus() {
        return rewardStatus;
    }

    public void setRewardStatus(Byte rewardStatus) {
        this.rewardStatus = rewardStatus;
    }

    public Integer getUserScoreExpenses() {
        return userScoreExpenses;
    }

    public void setUserScoreExpenses(Integer userScoreExpenses) {
        this.userScoreExpenses = userScoreExpenses;
    }

    public Integer getUserExpenses() {
        return userExpenses;
    }

    public void setUserExpenses(Integer userExpenses) {
        this.userExpenses = userExpenses;
    }

    public Double getUserProfitvo() {
        return userProfitvo;
    }

    public void setUserProfitvo(Double userProfitvo) {
        userProfitvo=userProfitvo/100;
        this.userProfitvo = userProfitvo;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }

    public String getStrcompanytype() {
        return strcompanytype;
    }

    public String getStrordertrpe() {
        return strordertrpe;
    }

    public String getStrtime() {
        return strtime;
    }

    public void setStrcompanytype(String strcompanytype) {
        this.strcompanytype = strcompanytype;
    }

    public void setStrordertrpe(String strordertrpe) {
        this.strordertrpe = strordertrpe;
    }

    public void setStrtime(String strtime) {
        this.strtime = strtime;
    }

}