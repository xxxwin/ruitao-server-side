package tech.lingyi.wx.msg.in.card;

import tech.lingyi.wx.utils.XmlHelper;

/**
 * 卡券消息解析接口
 * @author L.cm
 */
public interface ICardMsgParse {
    /**
     * 分而治之
     * @param xmlHelper xml解析工具
     */
    void parse(XmlHelper xmlHelper);
}
