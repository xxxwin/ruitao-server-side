package com.ruitaowang.admin.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

/**
 * Created by neal on 21/09/2017.
 */
@Component
public class AsyncBean {

    public void syncVoid(int i){
        System.out.println(" async void :"+i);
    }

    @Async
    public void asyncVoid(int i){
        System.out.println(" async void :"+i);
    }

    @Async
    public String asyncReturn(int i){
        System.out.println(" async return :"+i);
        return "async return";
    }

    @Async
    public Future<String> asyncReturnFuture(int i){
        try {
            i = i * 2;
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new AsyncResult<>(" async return :"+i);
    }

    @Async
    public CompletableFuture<String> asyncReturnCompletableFuture(int i){
        try {
            i = i * 2;
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return CompletableFuture.completedFuture("async return : " + i);
    }

}
