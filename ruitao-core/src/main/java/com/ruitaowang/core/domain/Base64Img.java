package com.ruitaowang.core.domain;

public class Base64Img extends Page {
    /**
     *  主键,所属表字段为base64_img.image_id
     */
    private Long imageId;

    /**
     *  备注,所属表字段为base64_img.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为base64_img.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为base64_img.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为base64_img.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为base64_img.base64
     */
    private String base64;

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64 == null ? null : base64.trim();
    }
}