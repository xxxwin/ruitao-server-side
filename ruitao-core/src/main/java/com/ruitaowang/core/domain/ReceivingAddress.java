package com.ruitaowang.core.domain;

public class ReceivingAddress extends Page {
    /**
     *  ,所属表字段为receiving_address.id
     */
    private Long id;

    /**
     *  用户ID,所属表字段为receiving_address.user_id
     */
    private Long userId;

    /**
     *  收货地址状态,所属表字段为receiving_address.address_type
     */
    private Byte addressType;

    /**
     *  省,所属表字段为receiving_address.province
     */
    private String province;

    /**
     *  市,所属表字段为receiving_address.city
     */
    private String city;

    /**
     *  区,所属表字段为receiving_address.district
     */
    private String district;

    /**
     *  地区级联所属ID,所属表字段为receiving_address.district_id
     */
    private Long districtId;

    /**
     *  收货详细地址,所属表字段为receiving_address.address
     */
    private String address;

    /**
     *  ,所属表字段为receiving_address.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为receiving_address.mtime
     */
    private Long mtime;

    /**
     *  ,所属表字段为receiving_address.remark
     */
    private String remark;

    /**
     *  ,所属表字段为receiving_address.rstatus
     */
    private Byte rstatus;

    private String userName;

    private String mobile;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Byte getAddressType() {
        return addressType;
    }

    public void setAddressType(Byte addressType) {
        this.addressType = addressType;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}