package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyProduct;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.properties.ConfigProperties;
import com.ruitaowang.core.utils.QRCode;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyProductService;
import com.ruitaowang.goods.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import wx.wechat.common.Configure;

import java.util.List;

/**
 * Created by Shaka on 2017/1/4.
 */
@RestController
public class APICompanyProductController extends BaseController {
    @Autowired
    private CompanyProductService companyProductService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private ConfigProperties configProperties;

    private void init() {
        //init
        Configure.hostAPI = configProperties.getHostAPI();
        Configure.hostOSS = configProperties.getHostOSS();
        Configure.hostWap = configProperties.getHostWap();
        Configure.returnUri = configProperties.getReturnUri();
        Configure.wxpay_notify_url = configProperties.getHostAPI() + "/api/wxs/notify";;
    }

    @RequestMapping("/api/companyProduct")
    public String list(CompanyProduct companyProduct) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company = new Company();
        company.setUserId(user.getId());
        List<Company> companyList = companyService.select(company);
        if (companyList != null && companyList.size() > 0) {
            companyProduct.setCompanyId(companyList.get(0).getCompanyId());
            MsgOut o = MsgOut.success(companyProductService.select(companyProduct));
            return this.renderJson(o);
        } else {
            return this.renderJson(MsgOut.error("您还不是商家用户"));
        }
    }

    @RequestMapping("/api/companyProduct/myCashier")
    public String myCashire() {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company = companyService.selectByUserId(user.getId());
        if (!ObjectUtils.isEmpty(company)) {
            CompanyProduct companyProduct =new CompanyProduct();
            companyProduct.setCompanyId(company.getCompanyId());
            MsgOut o = MsgOut.success(companyProductService.select(companyProduct));
            return this.renderJson(o);
        } else {
            return this.renderJson(MsgOut.error("您还不是商家用户"));
        }
    }

    @RequestMapping(value = "/api/companyProduct", method = RequestMethod.POST)
    public String create(CompanyProduct companyProduct) {
        init();
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company = new Company();
        company.setUserId(user.getId());
        List<Company> companyList = companyService.select(company);
        if (companyList != null && companyList.size() > 0) {
            companyProduct.setCompanyId(companyList.get(0).getCompanyId());
            CompanyProduct insertRes = companyProductService.insert(companyProduct);
            insertRes.setQrcodeUrl(QRCode.rulesQRForSMPay(System.getProperty("catalina.base"), user.getId(), insertRes.getProductId()));
            companyProductService.update(insertRes);
            MsgOut o = MsgOut.success(insertRes);
            return this.renderJson(o);
        } else {
            return this.renderJson(MsgOut.error("您还不是商家用户"));
        }
    }

    @RequestMapping(value = "/api/companyProduct", method = RequestMethod.PUT)
    public String update(CompanyProduct companyProduct) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company = new Company();
        company.setUserId(user.getId());
        List<Company> companyList = companyService.select(company);
        if (companyList != null && companyList.size() > 0) {
            companyProduct.setCompanyId(companyList.get(0).getCompanyId());
            companyProductService.update(companyProduct);
            MsgOut o = MsgOut.success(companyProduct);
            return this.renderJson(o);
        } else {
            return this.renderJson(MsgOut.error("您还不是商家用户"));
        }
    }

    @RequestMapping(value = "/api/companyProduct/{companyProductId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("companyProductId") Long companyProductId) {
        CompanyProduct companyProduct =new CompanyProduct();
        companyProduct.setProductId(companyProductId);
        MsgOut o = MsgOut.success(companyProductService.update(companyProduct));
        return this.renderJson(o);
    }


}
