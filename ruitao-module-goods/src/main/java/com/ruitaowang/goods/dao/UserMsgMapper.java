package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserMsg;
import java.util.List;

public interface UserMsgMapper {
    int insert(UserMsg userMsg);

    int deleteByPK(Long id);

    int updateByPK(UserMsg userMsg);

    UserMsg selectByPK(Long id);

    long count(UserMsg userMsg);

    List<UserMsg> select(UserMsg userMsg);
}