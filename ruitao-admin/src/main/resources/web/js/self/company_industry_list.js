/**
 *
 * Create by murong on 2018/5/14
 */

var config={
    selectUrl: '/api/admin/industry/select',
    deleteUrl: '/api/admin/industry/delete',
    updateUrlForCellEdit: '/api/admin/company/cellEdit',
    // selectTypeUrl:'/api/admin/type/selectByIndustry/'
};

$(document).ready(function () {
    $.jgrid.defaults.styleUI="Bootstrap";
    $("#industry_list").jqGrid({
        datatype:"json",
        url:config.selectUrl,
        height:450,
        autowidth:true,
        shrinkTofit:true,
        rowNum:12,
        colNames:["编号","行业名","抽成比例","备注","创建时间","操作"],
        colModel:[{
            name:"id",
            index:"id",
            sorttype:"int",
            authwidth:true,
            search:true
        },{
            name:"name",
            index:"name",
            autowidth:true,
            sortable: true,
        }, {
            name:"companyCommission",
            index:"companyCommission",
            autowidth:true,
            sortable: true,
        },{
            name:"remark",
            index:"remark",
            autowidth:true
        },{
            name: "ctime",
            index: "ctime",
            sortable: true,
        },{
            name: "operate",
            index: "operate",
            autowidth: true,
            sortable: false
         }],
        celledit:true,
        cellsubmit: 'remote',
        cellurl: '/api/admin/company/cellEdit',
        pager: "#pager_industry_list",
        jsonReader: {
            root: "data", "currPage": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#industry_list").jqGrid('navGrid',"#pager_industry_list",{
        add: false, edit: false, del: false
    });

    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#industry_list").setGridWidth(width);
    });
    $("#refresh_industry_list").click(function () {
        // $("#perform_search").onclick();
        //  var curpagenum= $("#industry_list").jqGrid('getGridParam','page')
        // var postdata={};
        //
        // postdata = $("#industry_list").jqGrid('getGridParam', 'postData');
        var curpagenum =$("#input_pager_industry_list input").val();
        // alert("pagepre="+postdata.page);
        //
        // postdata._search = true;
        // postdata.page=Number(curpagenum);
        // var curPage=Number(curpagenum);
        // // postdata.name = $('#industryName').val();
        // alert("page="+postdata.page);
        // // var curpagenum =$("#industry_list").jqGrid('getGridParam', 'page');
        // alert(Number(curpagenum) );
        // //此方法无效
        // // jQuery("#industry_list").trigger("reloadGrid",[]);
        $("#jqGrid").jqGrid('clearGridData');
        // jQuery("#industry_list").trigger("reloadGrid",[{page : curPage}]);
        jQuery("#industry_list").jqGrid('setGridParam',{
            url:config.selectUrl,
            datatype:'json',
            page:Number(curpagenum)}).trigger("reloadGrid");
        // yu(Number(curpagenum));
        // $("#perform_search").click();
        // var postdata = $("#industry_list").jqGrid('getGridParam', 'postData');
        // postdata._search = true;
        // postdata.name = $('#industryName').val();
        // var page =$("#input_pager_industry_list input").val();
        // jQuery("#industry_list").trigger("reloadGrid", [{page: page}]);
    });

    $("#perform_search").click(function () {
        var postdata = $("#industry_list").jqGrid('getGridParam', 'postData');
        postdata._search = true;
        postdata.name = $('#industryName').val();
        var page =$("#input_pager_industry_list input").val();
        jQuery("#industry_list").trigger("reloadGrid", [{page: page}]);
    });

});

function Delete(id) {
    // $.ajax(
    //     {
    //         url: selectTypeUrl,
    //         type: action,
    //         success: function (data, textStatus, jqXHR) {
    //             if (action == 'DELETE') {
    //                 jQuery("#industry_list").setGridParam({url: url}).trigger("reloadGrid");
    //
    //             }
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             console.error(errorThrown);
    //         }
    //     });
    bootbox.confirm({size:"small",message:"确认要删除id为"+id+"的这条数据吗?",callback: function (result) {
            if (result) {
                updateDialog("DELETE", config.deleteUrl + '/' + id);
                jQuery("#industry_list").jqGrid('delRowData', id);
            }
        }
    });
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#industry_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}

function dataWrapper() {
    var ids = jQuery("#industry_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#industry_list').jqGrid('getRowData', id);
        var html = '<a class="J_men uItem" style="color:#f60;padding-right: 15px" onclick="industryUpdate(' + id + ')" data-index="0">编辑</a>';
        html += "<a style='color:#f60' onclick='Delete(" + id + ")' >删除</a>";
        if(dataFromTheRow.companyCommission.indexOf('%')<0){
            dataFromTheRow.companyCommission=dataFromTheRow.companyCommission+'%';
        }
        if(dataFromTheRow.ctime.indexOf("-")<0){
            dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        }
        jQuery("#industry_list").jqGrid('setRowData', ids[i], {
            operate: html,
            companyCommission: dataFromTheRow.companyCommission,
            ctime: dataFromTheRow.ctime,
            //ctime: new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss(),
        });
    }
}
function industryUpdate(id) {
    layer.open({
        title: ["商家类型修改","font-size:20px;"],
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:"/industry/update/" + id,
    })
}

