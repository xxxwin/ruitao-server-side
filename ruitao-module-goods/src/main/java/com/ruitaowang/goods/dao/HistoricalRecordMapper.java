package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.HistoricalRecord;
import java.util.List;

public interface HistoricalRecordMapper {
    int insert(HistoricalRecord historicalRecord);

    int deleteByPK(Long id);

    int updateByPK(HistoricalRecord historicalRecord);

    HistoricalRecord selectByPK(Long id);

    long count(HistoricalRecord historicalRecord);

    List<HistoricalRecord> select(HistoricalRecord historicalRecord);
}