package com.ruitaowang.admin.view.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ruitaowang.admin.tree.TreeNode;
import com.ruitaowang.core.domain.Category;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CategoryService;

@RestController
public class APICategoryController extends BaseController {

	@Autowired
	private CategoryService categoryService;

	@RequestMapping("/api/admin/categories")
	public String findCategory(Category category) {
		List<Category> list = categoryService.select(category);
		MsgOut o = MsgOut.success(list);
		return this.renderJson(o);
	}

	@RequestMapping(value = "/api/admin/categories/selectByPk/{id}")
	public String selectByPk(@PathVariable("id") long id) {
		MsgOut msgOut = MsgOut.success(categoryService.selectByPK(id));
		return renderJson(msgOut);
	}

	@RequestMapping(value = "/api/admin/categories/create", method = RequestMethod.POST)
	public String create(Category category) {
		if(category.getParentId()!=null){
			Category parent = categoryService.selectByPK(category.getParentId());
			if(parent!=null){
				category.setLevel(parent.getLevel()+1);
			}
		}
		MsgOut msgOut = MsgOut.success(categoryService.insert(category));
		return renderJson(msgOut);
	}

	@RequestMapping(value = "/api/admin/categories/update", method = RequestMethod.PUT)
	public String update(Category category) {
		MsgOut msgOut = MsgOut.success(categoryService.update(category));
		return renderJson(msgOut);
	}

	@RequestMapping(value = "/api/admin/cate/delete/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable("id") Long id) {
		MsgOut msgOut = MsgOut.success(categoryService.delete(id));
		return renderJson(msgOut);
	}

/*	@RequestMapping(value = "/api/cateTree", method = RequestMethod.GET)
	public String cateTree(Category cate) {

		MsgOut out;
		if (cate == null)
			cate = new Category();
		// cate.setParentId(0L);
		List<TreeNode> nodes = new ArrayList<>();
		TreeNode root = new TreeNode();
		root.setId(0L);
		root.setpId(-1L);
		root.setName("根");
		nodes.add(root);
		List<Category> cates = categoryService.select(cate);

		List<Category> level1 = getChildren(cates, 0L);
		
		for (Category c1 : level1) {
			nodes.add(cloneCate(c1, new TreeNode()));
			List<Category> level2 = getChildren(cates, c1.getId());
			for (Category c2 : level2) {
				nodes.add(cloneCate(c2, new TreeNode()));
				List<Category> level3 = getChildren(cates, c2.getId());
				for (Category c3 : level3) {
					nodes.add(cloneCate(c3, new TreeNode()));
				}
			}
		}

		out = MsgOut.success(nodes);

		return this.renderJson(out);
	}

	private List<Category> getChildren(List<Category> cates, Long pid) {

		List<Category> children = new ArrayList<>();
		for (Category c : cates) {
			if(c.getParentId()==pid){
				children.add(c);
			}
		}
		
		cates.removeAll(children);

		return children;
	}
	
	private TreeNode cloneCate(Category c,TreeNode n){
		n.setId(c.getId());
		n.setName(c.getName());
		n.setpId(c.getParentId());
		
		return n;
	}*/

	private List<TreeNode> getNodes(List<Category> cates, Long pid) {
		List<TreeNode> childNodes = new ArrayList<>();
		for (Category c : cates) {
			if (c.getParentId() == pid) {
				TreeNode child = new TreeNode();
				child.setId(c.getId());
				child.setName(c.getName());
				child.setpId(pid);
				childNodes.add(child);
			}
		}
		if (childNodes.size() > 0) {
			for (TreeNode childNode : childNodes) {
				childNode.setChildren(getNodes(cates, childNode.getId()));
			}
		} else {
			childNodes = null;
		}

		return childNodes;
	}

}
