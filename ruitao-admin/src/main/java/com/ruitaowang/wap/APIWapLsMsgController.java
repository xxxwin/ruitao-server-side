package com.ruitaowang.wap;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.LsMsg;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.LsMsgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class APIWapLsMsgController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private LsMsgService lsMsgService;

//    @RequestMapping(value = "/api/wap/lsMsg", method = RequestMethod.GET)
//    public String wapCommentPlPage(int page, LsMsg lsMsg, @RequestHeader(value="headId",required = false) Long userId){
//        if (ObjectUtils.isEmpty(userId)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        lsMsg.setPage(page);
//        Long stime = TimeUtils.getLongDayBegin();
//        Long etime = TimeUtils.getLongDayEnd();
//    }

    @RequestMapping(value = "/api/wap/lsMsg", method = RequestMethod.GET)
    public String wapCommentPlPage(int page, LsMsg lsMsg){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        lsMsg.setPage(page);
        List<LsMsg> lsMsgList = lsMsgService.selectBySearch(lsMsg);
        MsgOut o = MsgOut.success(lsMsgList);
        o.setRecords(lsMsg.getRecords());
        o.setTotal(lsMsg.getTotal());
        return this.renderJson(o);
    }

}
