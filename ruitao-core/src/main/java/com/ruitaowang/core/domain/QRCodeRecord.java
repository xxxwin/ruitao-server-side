package com.ruitaowang.core.domain;

public class QRCodeRecord extends Page {
    /**
     *  ,所属表字段为qrcode_record.id
     */
    private Long id;

    /**
     *  用户ID,所属表字段为qrcode_record.user_id
     */
    private Long userId;

    /**
     *  推广次数,所属表字段为qrcode_record.number
     */
    private Integer number;

    /**
     *  购买次数,所属表字段为qrcode_record.buy_num
     */
    private Integer buyNum;

    /**
     *  购买状态,所属表字段为qrcode_record.buy_status
     */
    private Byte buyStatus;

    /**
     *  购买价格,所属表字段为qrcode_record.price
     */
    private Integer price;

    /**
     *  ,所属表字段为qrcode_record.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为qrcode_record.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为qrcode_record.remark
     */
    private String remark;

    /**
     *  ,所属表字段为qrcode_record.rstatus
     */
    private Byte rstatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getBuyNum() {
        return buyNum;
    }

    public void setBuyNum(Integer buyNum) {
        this.buyNum = buyNum;
    }

    public Byte getBuyStatus() {
        return buyStatus;
    }

    public void setBuyStatus(Byte buyStatus) {
        this.buyStatus = buyStatus;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}