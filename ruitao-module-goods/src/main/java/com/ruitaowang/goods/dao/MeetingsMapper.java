package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Meetings;
import java.util.List;

public interface MeetingsMapper {
    int insert(Meetings meetings);

    int deleteByPK(Long meetingId);

    int updateByPK(Meetings meetings);

    Meetings selectByPK(Long meetingId);

    long count(Meetings meetings);

    List<Meetings> select(Meetings meetings);
}