package com.ruitaowang.wap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Comment;
import com.ruitaowang.core.domain.CommentVO;
import com.ruitaowang.core.domain.Coupon;
import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.domain.GoodsAlbum;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CommentService;
import com.ruitaowang.goods.service.GoodsService;

@RestController
public class APIWapCommentController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private GoodsService goodsService;
    
    @Autowired
    private CommentService commentService;
    
    @Autowired
    private SysUserService sysUserService;
    

    /**
     * 获取评论信息
     * @param page
     * @param comment
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/commentPl/goods", method = RequestMethod.GET)
    public String wapCommentPlPage(int page,Comment comment,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(comment.getGoodsId()) && ObjectUtils.isEmpty(comment.getCompanyId())) {
            return this.renderJson(MsgOut.error("Param Error."));
        }
        comment.setPage(page);
        List<CommentVO> commentVOS = new ArrayList<>();
        List<Comment> comments = commentService.selectByTwo(comment); //获取所有评论数据
        if (!ObjectUtils.isEmpty(comments)) {
            for(Comment c : comments){
                CommentVO commentVO = new CommentVO();
                BeanUtils.copyProperties(c,commentVO);
                if(!ObjectUtils.isEmpty(c.getUserId())){
                    commentVO.setNickName(sysUserService.selectByPK(c.getUserId()).getNickname());
                    commentVO.setHeadImgUrl(sysUserService.selectByPK(c.getUserId()).getHeadimgurl());

                }
                if(!ObjectUtils.isEmpty(c.getGoodsId())){
                    commentVO.setGoodsName(goodsService.selectByPK(c.getGoodsId()).getGoodsName());
                }
                Comment comment1 = new Comment();
                comment1.setParentId(c.getId());
                comment1.setCommentType((byte)3);
                long commentCount = commentService.count(comment1);  //获取二级评论的数量
                commentVO.setCommentTotal(commentCount);
                comment1.setCommentType((byte)4);
                comment1.setUserId(userId);
                List<Comment> comments2 = commentService.select(comment1); //是否点赞
                if (!ObjectUtils.isEmpty(comments2)) {
                    commentVO.setThumpsUpType("Yes");
                } else {
                    commentVO.setThumpsUpType("NO");
                }
                commentVOS.add(commentVO);
            }
        }
        Map map = new HashMap();
        map.put("plsList",commentVOS);
        Comment comment1 = new Comment();
        comment1.setCommentType((byte)2);
        long xxCount = commentService.count(comment1);
        map.put("xxCount",xxCount);
        MsgOut o = MsgOut.success(commentVOS);
        o.setRecords(comment.getRecords());
        o.setTotal(comment.getTotal());
        return this.renderJson(o);
    }

    /**
     * 评论的个数
     * @param comment
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/comment/count", method = RequestMethod.GET)
    public String commentCount(Comment comment,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        comment.setCommentType((byte)1);
        long Count = commentService.count(comment);
        MsgOut o = MsgOut.success(Count);
        return this.renderJson(o);
    }

    /**
     * 买家秀个数
     * @param comment
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/commentXx/count", method = RequestMethod.GET)
    public String commentXxCount(Comment comment,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        comment.setCommentType((byte)2);
        long xxCount = commentService.count(comment);
        MsgOut o = MsgOut.success(xxCount);
        return this.renderJson(o);
    }

    /**
     * 获取买家秀信息
     * @param page
     * @param comment
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/commentXx/goods", method = RequestMethod.GET)
    public String wapCommentXxPage(int page,Comment comment,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        comment.setPage(page);
        comment.setCommentType((byte)2);
        List<CommentVO> commentVOS = new ArrayList<>();
        List<Comment> comments = commentService.selectForPage(comment); //获取所有买家秀数据
        if (!ObjectUtils.isEmpty(comments)) {
            for(Comment c : comments){
                CommentVO commentVO = new CommentVO();
                BeanUtils.copyProperties(c,commentVO);
                if(!ObjectUtils.isEmpty(c.getUserId())){
                    commentVO.setNickName(sysUserService.selectByPK(c.getUserId()).getNickname());
                    commentVO.setHeadImgUrl(sysUserService.selectByPK(c.getUserId()).getHeadimgurl());
                }
                Comment comment1 = new Comment();
                comment1.setParentId(c.getId());
                comment1.setCommentType((byte)3);
                long commentCount = commentService.count(comment1);  //获取二级评论的数量
                commentVO.setCommentTotal(commentCount);
                comment1.setCommentType((byte)4);
                //comment1.setParentId(c.getId());
                comment1.setUserId(userId);
                List<Comment> comments2 = commentService.select(comment1); //是否点赞
                if (!ObjectUtils.isEmpty(comments2)) {
                    commentVO.setThumpsUpType("Yes");
                } else {
                    commentVO.setThumpsUpType("NO");
                }
                commentVOS.add(commentVO);
            }
        }
        MsgOut o = MsgOut.success(commentVOS);
        o.setRecords(comment.getRecords());
        o.setTotal(comment.getTotal());
        return this.renderJson(o);
    }

    /**
     * 买家秀详情
     * @param id
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/commentXx/getOne", method = RequestMethod.GET)
    public String wapCommentXxGetOne(Long id,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Comment comment = commentService.selectByPK(id);
        MsgOut o = MsgOut.success(comment);
        return this.renderJson(o);
    }

    /**
     * 获取评论的评论
     * @param id
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/commentPls/goods", method = RequestMethod.GET)
    public String wapGetCommentPls(Long id,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Map map = new HashMap();
        //评论
        List<CommentVO> commentVOS = new ArrayList<>();
        Comment comment = commentService.selectByPK(id);  //顶级评论
        CommentVO commentVO = new CommentVO();
        BeanUtils.copyProperties(comment,commentVO);
        if(!ObjectUtils.isEmpty(comment.getUserId())){
            commentVO.setNickName(sysUserService.selectByPK(comment.getUserId()).getNickname());
            commentVO.setHeadImgUrl(sysUserService.selectByPK(comment.getUserId()).getHeadimgurl());
        }
        Comment comment2 = new Comment();
        comment2.setParentId(id);
        comment2.setCommentType((byte)3);
        long commentsCount = commentService.count(comment2); //二级评论的数量
        commentVO.setCommentTotal(commentsCount);
        Comment comment3 = new Comment();
        comment3.setCommentType((byte)4);
        comment3.setParentId(id);
        comment3.setUserId(userId);
        List<Comment> comments3 = commentService.select(comment3); //是否点赞
        if (!ObjectUtils.isEmpty(comments3)) {
            commentVO.setThumpsUpType("Yes");
        } else {
            commentVO.setThumpsUpType("NO");
        }
        commentVOS.add(commentVO);
        map.put("commentList",commentVOS);

        //评论中的评论
        Comment comment1 = new Comment();
        comment1.setParentId(id);
        comment1.setCommentType((byte)3);
        List<CommentVO> commentVOS1 = new ArrayList<>();
        List<Comment> comments1 = commentService.select(comment1); //顶级评论下的二级评论
        if (!ObjectUtils.isEmpty(comments1)) {
            for(Comment c : comments1){
                CommentVO commentVO1 = new CommentVO();
                BeanUtils.copyProperties(c,commentVO1);
                if(!ObjectUtils.isEmpty(c.getUserId())){
                    commentVO1.setNickName(sysUserService.selectByPK(c.getUserId()).getNickname());
                    commentVO1.setHeadImgUrl(sysUserService.selectByPK(c.getUserId()).getHeadimgurl());
                }
                Comment comment4 = new Comment();
                comment4.setCommentType((byte)4);
                comment4.setParentId(c.getId());
                comment4.setUserId(userId);
                List<Comment> comments4 = commentService.select(comment4); //是否点赞
                if (!ObjectUtils.isEmpty(comments4)) {
                    commentVO1.setThumpsUpType("Yes");
                } else {
                    commentVO1.setThumpsUpType("NO");
                }
                commentVOS1.add(commentVO1);
            }
        }
        map.put("commentLists",commentVOS1);

        MsgOut o = MsgOut.success(map);
        return this.renderJson(o);
    }

    /**
     * 评论点赞
     * @param id
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/comment/thumbsUp", method = RequestMethod.PUT)
    public String wapthumbsUp(Long id,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Comment comment = commentService.selectByPK(id); //根据id查询数据
        Long thumbsUp = comment.getThumbsUp(); //点赞数量

        //判断用户是否点赞，未点赞+1，已点赞-1。
        Comment comment1 = new Comment();
        comment1.setCommentType((byte)4);
        comment1.setUserId(userId);
        comment1.setParentId(comment.getId());
        List<Comment> comments1 = commentService.select(comment1);
        if (ObjectUtils.isEmpty(comments1)) {
            thumbsUp = thumbsUp + 1;
            comment.setThumbsUp(thumbsUp);
            //根据状态对点赞数量进行更改
            commentService.update(comment);
            comment.setCommentType((byte)4);
            comment.setParentId(comment.getId());
            comment.setUserId(userId);
            commentService.insert(comment);
        } else {
            thumbsUp = thumbsUp - 1;
            comment.setThumbsUp(thumbsUp);
            //根据状态对点赞数量进行更改
            commentService.update(comment);
            comment.setId(comments1.get(0).getId());
            comment.setRstatus((byte)1);
            commentService.update(comment);
        }

        MsgOut o = MsgOut.success("操作成功了！");
        return this.renderJson(o);
    }

    /**
     * 对评论进行评论
     * @param userId
     * @param comment  id,commentContent,commentType,companyId,goodsId,userId
     * @return
     */
    @RequestMapping(value = "/api/wap/commentPls/goods", method = RequestMethod.POST)
    public String wapCommentPls(Comment comment,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        comment.setUserId(userId);
        MsgOut o = MsgOut.success(commentService.insert(comment));
        return this.renderJson(o);
    }

    /**
     * 删除评论
     * @param id
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/commentPls/delete", method = RequestMethod.DELETE)
    public String wapCommentsDelete(Long id,@RequestHeader(value="headId",required = false) String userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(commentService.delete(id));
        return this.renderJson(o);
    }

    
    


}
