package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsAttributeLink;

public interface GoodsAttributeLinkAdMapper {

    int update(GoodsAttributeLink goodsAttributeLink);

}