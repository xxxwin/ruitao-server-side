#!/usr/bin/env bash
ssh -t -t root@test.ruitaowang.com << EOF
cd /server/source/ruitao-server-side
echo `pwd`
/usr/bin/git checkout master
/usr/bin/git pull origin master
cd /server/source/ruitao-server-side/ruitao-core
mvn clean install -DskipTests
cd /server/source/ruitao-server-side/ruitao-admin
mvn clean compile -DskipTests
ps aux |grep tomcat |grep -v 'grep' |awk '{print $2}'|xargs kill -9
rm -rf /opt/admin/webapps/ROOT.war
deployDir=/opt/admin/webapps/ROOT
/usr/bin/cp -r -a /server/source/ruitao-server-side/ruitao-core/target/ruitao-core-1.0.0.jar ${deployDir}/WEB-INF/lib
/usr/bin/cp -r -a /server/source/ruitao-server-side/ruitao-admin/target/classes/* ${deployDir}/WEB-INF/classes
/usr/bin/cp -r -a /server/source/ruitao-server-side/ruitao-admin/src/main/webapp/wap/*  ${deployDir}/wap

#sudo sh /opt/tomcat/bin/startup.sh
sudo sh /opt/admin/bin/startup.sh
echo 'Please waiting 20s ...'
#apidoc -i /server/source/ruitao-server-side/ruitao-admin/src/main/java/com/ruitaowang/admin/view/api -o /root/shihong/ruitao/ -c /server/source/ruitao-server-side/bootstrap/
#apidoc -i /server/source/ruitao-server-side/ruitao-admin/src/main/java/com/ruitaowang/dinner/view/api -o /root/shihong/ruitao/dc/ -c /server/source/ruitao-server-side/bootstrap/
#sleep 20
tail -f /opt/admin/logs/catalina.out
#/opt/tomcat/logs/catalina.out
exit
EOF