package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Coupon;
import com.ruitaowang.goods.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;

/**
 * 优惠卷详情
 */
@RestController
public class PageCouponController {

    @Autowired
    private CouponService couponService;

    @RequestMapping(value = {"coupon/update/{id}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("id") Long id, Model model) {
        Coupon companyCoupon = couponService.selectByPK(id);
        model.addAttribute("companyCoupon",companyCoupon);
        return new ModelAndView("web/admin_coupon_update");
    }

    @RequestMapping(value = {"ws/coupon/update/{id}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView wsUpdate(@PathVariable("id") Long id, Model model) {
        Coupon companyCoupon = couponService.selectByPK(id);
        model.addAttribute("companyCoupon",companyCoupon);
        return new ModelAndView("web/ws_coupon_update");
    }
}
