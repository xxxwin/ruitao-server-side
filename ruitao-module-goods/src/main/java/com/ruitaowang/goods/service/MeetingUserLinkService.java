package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.MeetingUserLink;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.MeetingUserLinkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class MeetingUserLinkService implements BaseService<MeetingUserLink> {
    @Autowired
    private MeetingUserLinkMapper meetingUserLinkMapper;

    @Override
    public MeetingUserLink insert(MeetingUserLink meetingUserLink) {
        Long time = System.currentTimeMillis();
        meetingUserLink.setCtime(time);
        meetingUserLink.setMtime(time);
        meetingUserLinkMapper.insert(meetingUserLink);
        return meetingUserLink;
    }

    @Override
    public int delete(Long id) {
        MeetingUserLink meetingUserLink = new MeetingUserLink();
        meetingUserLink.setLinkId(id);
        meetingUserLink.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        meetingUserLink.setMtime(time);
        return meetingUserLinkMapper.updateByPK(meetingUserLink);
    }

    @Override
    public MeetingUserLink update(MeetingUserLink meetingUserLink) {
        Long time = System.currentTimeMillis();
        meetingUserLink.setMtime(time);
        meetingUserLinkMapper.updateByPK(meetingUserLink);
        return meetingUserLink;
    }

    @Override
    public MeetingUserLink selectByPK(Long id) {
        return meetingUserLinkMapper.selectByPK(id);
    }

    @Override
    public List<MeetingUserLink> select(MeetingUserLink meetingUserLink) {
//        meetingUserLink.setRstatus((byte) 0);
        return meetingUserLinkMapper.select(meetingUserLink);
    }

    public List<MeetingUserLink> selectForPage(MeetingUserLink meetingUserLink) {
//        meetingUserLink.setRstatus((byte) 0);
        meetingUserLink.setRecords(meetingUserLinkMapper.count(meetingUserLink));
        return meetingUserLinkMapper.select(meetingUserLink);
    }

    public MeetingUserLink selectByPhone(String phone) {
        MeetingUserLink meetingUser = new MeetingUserLink();
        meetingUser.setPhone(phone);
        List<MeetingUserLink> meetingUsers = meetingUserLinkMapper.select(meetingUser);
        if(ObjectUtils.isEmpty(meetingUsers)){
            return null;
        }
        return meetingUsers.get(0);
    }

}
