/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/user_role_grant'
};
$(document).ready(function () {
    //grant
    $('#submitForSave').click(function () {
        var checked = $('input[type=checkbox]:checked');
        if(!checked || checked.length == 0){
            toastr.error("请选择角色");
            return;
        }
        var postData = {"userId": $("#userId").val(), "roleIds": menuIdsWrapper(checked) };
        $.ajax(
        {
            url: Config.createUrl,
            type: "POST",
            data: postData,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                toastr.success(data.msg, data.title);
                if (data && data.type == 'SUCCESS'){}
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
        return false;
    });
    $('#buttonGoback').click(function () {
        history.go(-1);
        location.reload();
        return false;
    })
});
function menuIdsWrapper(data) {
    var temp = [];
    for (var i = 0; i < data.length; i++) {
        temp.push($(data[i]).val());
    }
    return temp.join(',');
}

//hello :)~