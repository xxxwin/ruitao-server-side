package com.ruitaowang.core.domain;

public class ActivityUserLinkVO extends ActivityUserLink {

    private String headImgUrl;

    private String nickname;

    private String wechatPhoto;

    private String readSum;

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getWechatPhoto() {
        return wechatPhoto;
    }

    public void setWechatPhoto(String wechatPhoto) {
        this.wechatPhoto = wechatPhoto;
    }

    public String getReadSum() {
        return readSum;
    }

    public void setReadSum(String readSum) {
        this.readSum = readSum;
    }
}
