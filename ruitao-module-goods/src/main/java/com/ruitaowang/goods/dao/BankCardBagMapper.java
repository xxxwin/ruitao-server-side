package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.BankCardBag;
import java.util.List;

public interface BankCardBagMapper {
    int insert(BankCardBag bankCardBag);

    int deleteByPK(Long id);

    int updateByPK(BankCardBag bankCardBag);

    BankCardBag selectByPK(Long id);

    long count(BankCardBag bankCardBag);

    List<BankCardBag> select(BankCardBag bankCardBag);
}