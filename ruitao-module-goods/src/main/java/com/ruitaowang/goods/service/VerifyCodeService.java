package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Verifycode;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.VerifycodeAdMapper;
import com.ruitaowang.goods.dao.VerifycodeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 2017/1/4.
 */
@Service
public class VerifyCodeService implements BaseService<Verifycode>{

    @Autowired
    private VerifycodeMapper verifycodeMapper;
    @Autowired
    private VerifycodeAdMapper verifycodeAdMapper;

    @Override
    public List<Verifycode> select(Verifycode verifycode) {
        verifycode.setRstatus((byte) 0);
        return verifycodeMapper.select(verifycode);
    }

    @Override
    public Verifycode insert(Verifycode verifycode) {
        Long time = System.currentTimeMillis();
        verifycode.setCtime(time);
        verifycode.setMtime(time);
        verifycodeMapper.insert(verifycode);
        return verifycode;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public Verifycode update(Verifycode verifycode) {

        Long time = System.currentTimeMillis();
        verifycode.setMtime(time);
        verifycodeMapper.updateByPK(verifycode);
        return verifycode;
    }

    public int count(Verifycode verifycode) {
        return verifycodeAdMapper.count(verifycode);
    }



    @Override
    public Verifycode selectByPK(Long id) {
        return null;
    }
}
