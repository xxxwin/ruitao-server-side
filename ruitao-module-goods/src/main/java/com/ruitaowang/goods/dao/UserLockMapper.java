package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserLock;
import java.util.List;

public interface UserLockMapper {
    int insert(UserLock userLock);

    int deleteByPK(Long userId);

    int updateByPK(UserLock userLock);

    UserLock selectByPK(Long userId);

    long count(UserLock userLock);

    List<UserLock> select(UserLock userLock);
}