package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.WalletCompany;
import java.util.List;

public interface WalletCompanyMapper {
    int insert(WalletCompany walletCompany);

    int deleteByPK(Long companyId);

    int updateByPK(WalletCompany walletCompany);

    WalletCompany selectByPK(Long companyId);

    long count(WalletCompany walletCompany);

    List<WalletCompany> select(WalletCompany walletCompany);
}