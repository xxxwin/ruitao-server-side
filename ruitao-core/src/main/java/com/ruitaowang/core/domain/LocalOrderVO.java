package com.ruitaowang.core.domain;

/**
 * Created by Administrator on 2017/5/5.
 */
public class LocalOrderVO extends LocalOrder{
    private String strtime;
    private String strnickname;
    private String strcompanyname;
    private String strlinkman;
    private String strphone;

    private Double priceVO;

    public Double getPriceVO() {

        return priceVO;
    }

    public void setPriceVO(Double priceVO) {
        priceVO=priceVO/100;
        this.priceVO = priceVO;
    }

    public void setStrtime(String strtime) {
        this.strtime = strtime;
    }

    public void setStrnickname(String strnickname) {
        this.strnickname = strnickname;
    }

    public void setStrcompanyname(String strcompanyname) {
        this.strcompanyname = strcompanyname;
    }

    public void setStrlinkman(String strlinkman) {
        this.strlinkman = strlinkman;
    }

    public void setStrphone(String strphone) {
        this.strphone = strphone;
    }

    public String getStrtime() {
        return strtime;
    }

    public String getStrnickname() {
        return strnickname;
    }

    public String getStrcompanyname() {
        return strcompanyname;
    }

    public String getStrlinkman() {
        return strlinkman;
    }

    public String getStrphone() {
        return strphone;
    }
}
