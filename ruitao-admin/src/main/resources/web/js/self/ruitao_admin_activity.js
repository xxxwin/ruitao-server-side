/**
 * Created by Administrator on 2017/4/25.
 */
/**
 * Created by Administrator on 2017/4/25.
 */
/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by neal on 11/22/16.
 */

var Config = {
    selectShareUrl: "/api/admin/activity/share",
    selectTinyShopUrl: "/api/admin/activity/tinyShop",
    selectFastTurnUrl: "/api/admin/activity/fastTurn",
    selectCompanyRegisterUrl: "/api/admin/activity/companyRegister"

};

//活动开始时间1495382400001
//2017-5-22 00:00:00 001

//当月日期
$('input[id=tinyShopDate]').change(function(){
    var postdata = $("#tiny_shop").jqGrid('getGridParam', 'postData');
    var yearAll=$(this).val().substring(0,4);
    var monthAll=$(this).val().substring(5,7);
    var dayAll=$(this).val().substring(8,10);
    postdata.stime = new Date(Number(yearAll), Number(monthAll)-1,Number(dayAll)+1-Number(dayAll)).getTime();
    postdata.etime = new Date(Number(yearAll), Number(monthAll), Number(dayAll)+1-Number(dayAll)).getTime();
    jQuery("#tiny_shop").trigger("reloadGrid", [{page: 1}]);
});

//当周日期
$('input[id=fastTurnDate]').change(function(){
    var postdata = $("#fast_turn").jqGrid('getGridParam', 'postData');
    var yearAll=$(this).val().substring(0,4);
    var monthAll=$(this).val().substring(5,7);
    var dayAll=$(this).val().substring(8,10);
    var DayOfWeekAll = new Date(Number(yearAll), Number(monthAll)-1, Number(dayAll));
    if (DayOfWeekAll.getDay()===0){
        postdata.stime = new Date(Number(yearAll), Number(monthAll)-1, Number(dayAll) - (DayOfWeekAll.getDay() + 6)).getTime();
        postdata.etime = new Date(Number(yearAll), Number(monthAll)-1, Number(dayAll) + (8 - (DayOfWeekAll.getDay() + 7))).getTime();
    }else {
        postdata.stime = new Date(Number(yearAll), Number(monthAll)-1, Number(dayAll) - DayOfWeekAll.getDay() + 1).getTime();
        postdata.etime = new Date(Number(yearAll), Number(monthAll)-1, Number(dayAll) + (8 - DayOfWeekAll.getDay())).getTime();
    }
    jQuery("#fast_turn").trigger("reloadGrid", [{page: 1}]);
});

$(document).ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        // closeText: '×',
        // currentText: '今天',
        // hideIfNoPrevNext: true,
        // showButtonPanel: true,
        // showMonthAfterYear: true,
        // showOtherMonths: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#shares").jqGrid({
        datatype: "json",
        postData: {stime: 1495382400001},
        url: Config.selectShareUrl,
        height: 100,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["会员ID", "会员名称", "电话", "消费金额"],
        colModel: [
            {
                name: "userId",
                index: "userId"
            }, {
                name: "consignee",
                index: "consignee"
            }, {
                name: "mobile",
                index: "mobile",
                editable: true
            }, {
                name: "orderAmount",
                index: "orderAmount",
                editable: true
            }],
   //     pager: "#pager_share",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "orderId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            shareWrapper();
        }
    });
 //  jQuery("#shares").jqGrid('navGrid', '#pager_share', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#shares").setGridWidth(width);
    });


    $("#tiny_shop").jqGrid({
        datatype: "json",
        url: Config.selectTinyShopUrl,
        height: 100,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["商家ID", "商家名称", "电话", "销量"],
        colModel: [
            {
                name: "goodsProviderId",
                index: "goodsProviderId"
            }, {
                name: "remark",
                index: "remark"
            }, {
                name: "logisticsSn",
                index: "logisticsSn"
            }, {
                name: "goodsScreenPrice",
                index: "goodsScreenPrice"
            }],
      //  pager: "#pager_tiny_shop",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "goodsProviderId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            tinyShopWrapper();
        }
    });
  //  jQuery("#tiny_shop").jqGrid('navGrid', '#pager_tiny_shop', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#tiny_shop").setGridWidth(width);
    });
    $("#fast_turn").jqGrid({
        datatype: "json",
        url: Config.selectFastTurnUrl,
        height: 100,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["会员ID", "会员名称", "电话", "收益金额"],
        colModel: [
            {
                name: "userId",
                index: "userId"
            }, {
                name: "remark",
                index: "remark"
            }, {
                name: "orderSn",
                index: "orderSn"
            }, {
                name: "userProfit",
                index: "userProfit"
            }],
    //    pager: "#pager_fast_turn",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "userId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            fastTurnWrapper();
        }
    });
   // jQuery("#fast_turn").jqGrid('navGrid', '#pager_fast_turn', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#fast_turn").setGridWidth(width);
    });
    $("#company_register").jqGrid({
        datatype: "json",
        postData: {stime: 1495382400001},
        url: Config.selectCompanyRegisterUrl,
        height: 100,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["商家ID", "商家名称", "负责人", "电话", "时间"],
        colModel: [
            {
                name: "companyId",
                index: "companyId"
            }, {
                name: "companyName",
                index: "companyName"
            }, {
                name: "linkman",
                index: "linkman"
            }, {
                name: "mobile",
                index: "mobile"
            }, {
                name: "ctime",
                index: "ctime"
            }],
      //  pager: "#pager_company_register",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "companyId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            companyRegisterWrapper();
        }
    });
   // jQuery("#company_register").jqGrid('navGrid', '#pager_company_register', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#company_register").setGridWidth(width);
    });

});
function shareWrapper() {
    var ids = jQuery("#shares").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#shares').jqGrid('getRowData', id);
        jQuery("#shares").jqGrid('setRowData', ids[i], {
            orderAmount: toFixedForPrice(dataFromTheRow.orderAmount)
        });
    }
}
function tinyShopWrapper() {
    var ids = jQuery("#tiny_shop").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#tiny_shop').jqGrid('getRowData', id);
        jQuery("#tiny_shop").jqGrid('setRowData', ids[i], {
            goodsScreenPrice: toFixedForPrice(dataFromTheRow.goodsScreenPrice)
        });
    }
}
function fastTurnWrapper() {
    var ids = jQuery("#fast_turn").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#fast_turn').jqGrid('getRowData', id);
        jQuery("#fast_turn").jqGrid('setRowData', ids[i], {
            userProfit: toFixedForPrice(dataFromTheRow.userProfit)
        });
    }
}
function companyRegisterWrapper() {
    var ids = jQuery("#company_register").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#company_register').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        jQuery("#company_register").jqGrid('setRowData', ids[i], {
            price: toFixedForPrice(dataFromTheRow.price),
            ctime: dataFromTheRow.ctime
        });
    }
}


