package com.ruitaowang.core.domain;

public class Verifycode extends Page {
    /**
     *  自增主键,所属表字段为verifycode.verifycode_id
     */
    private Long verifycodeId;

    /**
     *  申请次数,所属表字段为verifycode.invite_count
     */
    private Integer inviteCount;

    /**
     *  验证码,所属表字段为verifycode.verifycode
     */
    private String verifycode;

    /**
     *  手机号,所属表字段为verifycode.mobile
     */
    private String mobile;

    /**
     *  备注,所属表字段为verifycode.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为verifycode.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为verifycode.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为verifycode.rstatus
     */
    private Byte rstatus;

    /**
     *  最后登录IP,所属表字段为verifycode.reg_ip
     */
    private String regIp;

    /**
     *  最后失效时间,所属表字段为verifycode.reg_deadline
     */
    private Long regDeadline;

    public Long getVerifycodeId() {
        return verifycodeId;
    }

    public void setVerifycodeId(Long verifycodeId) {
        this.verifycodeId = verifycodeId;
    }

    public Integer getInviteCount() {
        return inviteCount;
    }

    public void setInviteCount(Integer inviteCount) {
        this.inviteCount = inviteCount;
    }

    public String getVerifycode() {
        return verifycode;
    }

    public void setVerifycode(String verifycode) {
        this.verifycode = verifycode == null ? null : verifycode.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRegIp() {
        return regIp;
    }

    public void setRegIp(String regIp) {
        this.regIp = regIp == null ? null : regIp.trim();
    }

    public Long getRegDeadline() {
        return regDeadline;
    }

    public void setRegDeadline(Long regDeadline) {
        this.regDeadline = regDeadline;
    }
}