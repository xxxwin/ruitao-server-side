package tech.lingyi.wx.msg.out;

import com.alibaba.fastjson.JSON;

/**
 * Created by neal on 18/07/2017.
 */
public class TemplateData {
    private String touser;
    private String template_id;
    private String url;
    private String topcolor;
    private String title;
    private String scene;

    private TemplateItem data;

    public static TemplateData New() {
        return new TemplateData();
    }

    private TemplateData() {
        this.data = new TemplateItem();
    }
    public TemplateData add(String key, String value, String color){
        data.put(key, new Item(value, color));
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getScene() {
        return scene;
    }

    public void setScene(String scene) {
        this.scene = scene;
    }

    public String build() {
        return JSON.toJSON(this).toString();
    }

    public String getTouser() {
        return touser;
    }

    public void setTouser(String touser) {
        this.touser = touser;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTopcolor() {
        return topcolor;
    }

    public void setTopcolor(String topcolor) {
        this.topcolor = topcolor;
    }

    public TemplateItem getData() {
        return data;
    }

    public void setData(TemplateItem data) {
        this.data = data;
    }
}

