/**
 * Created by xinchunting on 18-2-6.
 */
// $.validator.setDefaults({
//     highlight: function (e) {
//         $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
//     }, success: function (e) {
//         e.closest(".form-group").removeClass("has-error").addClass("has-success");
//     }, errorElement: "span", errorPlacement: function (e, r) {
//         e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
//     }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
// });

var Config = {
    selectCompanyUrl: '/api/admin/company/providers',
    selectGoods:"/api/admin/goods",
    someSubmitCreate: '/api/ws/coupon',
    //submitCreate:'/api/ws/companyCoupon/create'
};
$(".datepicker").datepicker({
    todayBtn: "linked",
    clearBtn: true,
    autoclose: true,
    language: "zh-CN"
});
$().ready(function () {
    $.ajax(
        {
            url: Config.selectCompanyUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="" hassubinfo="true">选择商户</option>');
                data.data.map(function (company) {
                    categories.push('<option value="');
                    categories.push(company.companyId);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(company.companyName);
                    categories.push('</option>');
                });
                $("#companyId").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });


    $(".price").click(function () {
        var price="";
        price='<button type="button" onclick="price(this,$(this).val())" value="'+$(this).val()+'" style="position:relative;border-radius:5px;font-size: 20px;background-color: gold">'+$(this).html()+'</button>';
        $(".prices").append(price);
        $(this).hide();
    });
    $(".time").click(function () {
        var time="";
        time='<button type="button" onclick="time(this,$(this).val())" value="'+$(this).val()+'" style="position:relative;border-radius:5px;font-size: 20px;background-color: #00a0e9">'+$(this).html()+'</button>';
        $(".times").append(time);
        $(this).hide();
    });
});

$("#companyId").change(function () {
    $.ajax(
        {
            url: Config.selectGoods,
            type: "GET",
            data:{goodsProviderId:$(this).val(),page:1,rows:1000},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="" hassubinfo="true">选择商品</option>');
                data.data.map(function (goods) {
                    categories.push('<option value="');
                    categories.push(goods.goodsId);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(goods.goodsName);
                    categories.push('</option>');
                });
                $("#goodsId").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
});

$('.sj').hide();
$("#coupon_type").change(function () {
    if($(this).val() == 0){
        $('.sj').hide();
        $('.bl').show();
    }

    // if($(this).val() == 1){
    //     $('.sj').show();
    //     $('.bl').hide();
    // }

    if($(this).val() == 2){
        var companyId=$('#companyId option:selected').val();
        $('.sj').show();
        $('.bl').hide();
    }
});

function price(e,f) {
    console.log("事件触发了");
    $(e).remove();
    $(".xs button[value="+f+"]").show();
}
function time(e,f) {
    console.log("事件触发了");
    $(e).remove();
    $(".sj button[value="+f+"]").show();
}

$("#submitCreate").click(function () {
    var data="";
    var couponTime=dateToEpoch($("#beginDateOne").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    var couponEndTime=dateToEpoch($("#endDateOne").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    var companyId=$('#companyId option:selected').val();
    var goodsId=$('#goodsId option:selected').val();
    //var percentage=$('#percentage').val();
    var price=$('#price').val() * 100;
    var score1=$('#score1').val() * 100;
    var reach1=$('#reach1').val() * 100;
    var couponType=$("#coupon_type option:selected").val();
    if (!companyId){
        toastr.error("请选择商户");
        return false;
    }
    if (!couponType){
        toastr.error("请选择分类");
        return false;
    }
    if(couponType == 0){
        if(reach1 == "" || reach1 == null){
            toastr.error("请填写满减价钱");
            return false;
        }
        if(score1 == "" || score1 == null){
            toastr.error("请填写打折价钱");
            return false;
        }
        data={companyId:companyId,couponType:couponType,reach1:reach1,score1:score1,couponTime:couponTime,couponEndtime:couponEndTime,remark:$("input[id=remark]").val()}
    }
    if(couponType == 2){
        if (!goodsId){
            toastr.error("请选择商品");
            return false;
        }
        if (price == "" || price == null){
            toastr.error("请填写折扣价钱");
            return false;
        }
        data={companyId:companyId,couponType:couponType,goodsId:goodsId,price:price,remark:$("input[id=remark]").val(),couponTime:couponTime,couponEndtime:couponEndTime}
    }
    if(isNaN(couponTime)){
        toastr.error("请选择起始时间");
        return false;
    }
    if(isNaN(couponEndTime)){
        toastr.error("请选择结束时间");
        return false;
    }
    if (data){
        $.ajax(
        {
            url: Config.someSubmitCreate,
            type: "POST",
            data:data,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                toastr.success(data.msg, data.title);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    }
});