
var Config = {
    selectOrder : '/api/admin/order/selectByCompany',
};

var orderCount;
var sales = 0;
$(document).ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
    // getOrder();
     myData();
});

// $("#perform_search").click(function () {
//     getOrder();
//     myData();
// });

function getOrder() {
    $.ajax(
        {
            url: Config.selectOrder,
            data:{
                allOrderBegin : $("#allOrderBegin").val(),
                allOrderEnd : $("#allOrderEnd").val()
            },
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                orderCount = data.data.orderCount;
                sales = data.data.sales;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
}

// echarts图表绘制函数
function myData() {
    // var xA = [];
    // var yA = [];
    // var salesList = [];
    // xA.push(companyName);
    // yA.push(goodsCount);
    // salesList.push(sales);
    // var myChartpersonal = echarts.init(document.getElementById('personal'));
    // var optionpersonal = {
    //     grid: {
    //         bottom: 80
    //     },
    //     tooltip : {
    //         trigger: 'axis',
    //         axisPointer: {
    //             type: 'cross',
    //             animation: false,
    //             label: {
    //                 backgroundColor: '#505765'
    //             }
    //         }
    //     },
    //     legend: {
    //         data: ['商品数','销售额']
    //     },
    //     toolbox: {
    //         show : true,
    //         feature : {
    //             dataView : {show: true, readOnly: false},
    //             magicType : {show: true, type: ['line', 'bar']},
    //             restore : {show: true},
    //             saveAsImage : {show: true}
    //         }
    //     },
    //     calculable : true,
    //     grid: {
    //         y2: 140
    //     },
    //     xAxis: {
    //         data: xA
    //     },
    //     yAxis: {},
    //     series: [{
    //         name: '商品数',
    //         type: 'bar',
    //         data: yA,
    //     },{
    //         name: '销售额',
    //         type: 'bar',
    //         data: salesList,
    //     }]
    // };
    var myChartmain = echarts.init(document.getElementById('main'));
    var xName = [];
    var orderValue = [];
    var salesValue = [];
    var allOrderYear = $("#allOrder").val();
    if(allOrderYear == null || allOrderYear == ""){
        xName.push("所有订单信息");
    }else{
        xName.push(allOrderYear);
    }
    orderValue.push(orderCount);
    salesValue.push(sales);
    var optionmain = {
        grid: {
            bottom: 80
        },
        tooltip : {
            trigger: 'axis',
            axisPointer: {
                type: 'cross',
                animation: false,
                label: {
                    backgroundColor: '#505765'
                }
            }
        },
        legend: {
            data: ['订单数','收入额']
        },
        toolbox: {
            show : true,
            feature : {
                dataView : {show: true, readOnly: false},
                magicType : {show: true, type: ['line', 'bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        grid: {
            y2: 140
        },
        xAxis: {
            data: xName
        },
        yAxis: {},
        series: [{
            name: '订单数',
            type: 'bar',
            data: orderValue,
        },{
            name: '收入额',
            type: 'bar',
            data: salesValue,
        }]
    };
    //myChartpersonal.setOption(optionpersonal);
    myChartmain.setOption(optionmain);
};