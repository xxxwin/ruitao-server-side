package com.ruitaowang.wap;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ruitaowang.account.service.CompanyStaffService;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyStaff;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;

@RestController
public class APIWapCompanyStaffController extends BaseController {

    @Autowired
    private CompanyStaffService companyStaffService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SysUserService userService;

    @RequestMapping(value = "/api/wap/companyStaffs", method = RequestMethod.GET)
    public String list(CompanyStaff companyStaff,@RequestHeader(value="headId",required = false) String headId){
        List<CompanyStaff> list;
        MsgOut o;
        Company company= companyService.selectByUserId(Long.valueOf(headId));
        companyStaff.setCompanyId(company.getCompanyId());
        list = companyStaffService.select(companyStaff);
        o = MsgOut.success(list);
        
        return this.renderJson(o);
    }

    /**
     * 获取员工列表
     * @param companyStaff
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/getStaff", method = RequestMethod.GET)
    public String getStaff(CompanyStaff companyStaff,@RequestHeader(value="headId",required = false) String userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        List<CompanyStaff> companyStaffList = companyStaffService.select(companyStaff);
        MsgOut msgOut = MsgOut.success(companyStaffList);
        return renderJson(msgOut);
    }
    
    @RequestMapping(value = "/api/wap/companyStaff/{id}", method = RequestMethod.GET)
	public String one(@PathVariable("id") Long id) {
		MsgOut o;
		CompanyStaff companyStaff =  companyStaffService.selectByPK(id);
		o = MsgOut.success(companyStaff);
		return this.renderJson(o);
	}

    @RequestMapping(value = "/api/wap/companyStaffs", method = RequestMethod.POST)
    public String create(CompanyStaff companyStaff,@RequestHeader(value="headId",required = false) String headId){
    	
    	MsgOut o;
    	
    	if (ObjectUtils.isEmpty(headId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
    	Company company= companyService.selectByUserId(Long.valueOf(headId));
    	SysUser staff = userService.selectByPK(companyStaff.getUserId());
    	if(staff==null){
    		return this.renderJson(MsgOut.error("员工id不存在!"));
    	}else if(StringUtils.isEmpty(staff.getPhone())){
    		return this.renderJson(MsgOut.error("员工没有设置手机号!"));
    	}else if(!staff.getPhone().equals(companyStaff.getPhone())){
    		return this.renderJson(MsgOut.error("员工id和手机号不匹配"));
    	}
    	
    	CompanyStaff query = new CompanyStaff();
    	query.setUserId(companyStaff.getUserId());
    	query.setCompanyId(company.getCompanyId());
    	query.setOrderBy(" company_id ");
    	List<CompanyStaff> staffs = companyStaffService.select(query);
    	if(!staffs.isEmpty()){
    		o = MsgOut.error("职员已存在");
    	}else{
        	companyStaff.setCompanyId(company.getCompanyId());
            companyStaff.setStatus(1);
            companyStaffService.insert(companyStaff);
            o = MsgOut.success(companyStaff);    		
    	}
    	
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/companyStaffs", method = RequestMethod.PUT)
//    @RolesAllowed({"ROLE_roles:update", "ROLE_root"})
    public String update(CompanyStaff companyStaff){//@Valid
        companyStaffService.update(companyStaff);
        return this.renderJson(MsgOut.success(companyStaff));
    }

    @RequestMapping(value = "/api/wap/companyStaff/{id}", method = RequestMethod.DELETE)
//    @RolesAllowed({"ROLE_roles:delete", "ROLE_root"})
    public String delete(@PathVariable("id") Long id){
        companyStaffService.delete(id);
        return this.renderJson(MsgOut.success());
    }

}