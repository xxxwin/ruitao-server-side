/**
 * Created by Administrator on 2017/4/2.
 */
/**
 * Created by Administrator on 2017/3/23.
 */
var Config={

    selectUrl:'/api/admin/localOrder/Profit'

};


function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        postData: {orderSn:getQueryString("id")},
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","订单编号", "受益者编号","受益者类型","返利类型", "受益者名称", "返利额度", "订单时间"],
        colModel: [{
            name: "profitId",
            index: "profitId",
            autowidth: true,
            sorttype: "int",
            search: true,
        },{
            name: "orderId",
            index: "orderId",
            autowidth: true,
            sorttype: "int",
            search: true,
        }, {
            name: "userId",
            index: "userId",
            autowidth: true,
        },{
            name: "rstatus",
            index: "rstatus",
            autowidth: true,
            editoptions:{value:"0:快省-龙蛙会员;1:快转·微传媒会员;2:快点-扫码点餐会员;3:快联-跨界盈利;4:快销-商家促销;5:快签-扫码签到;6:快飞-梦想;7:快推-城市;100:龙蛙-普通用户"},
            editable: true,
            edittype:'select',
            formatter:'select'
        }, {
            name: "orderType",
            index: "orderType",
            autowidth: true,
            editoptions:{value:"1:地面店扫码订单返利;3:地面店扫码订单返红包"},
            editable: true,
            edittype:'select',
            formatter:'select'
        }, {
            name: "remark",
            index: "remark",
            autowidth: true,
        }, {
            name: "userProfit",
            index: "userProfit",
            sorttype: "int"
        }, {
            name: "ctime",
            index: "ctime",
            autowidth: true,
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            dataWrapper();
        }
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});
$('#buttonGoback').click(function () {
    history.go(-1);
    location.reload();
    return false;
});
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id);
        dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        jQuery("#data_list").jqGrid('setRowData', ids[i], {userProfit: toFixedForPrice(dataFromTheRow.userProfit), ctime: dataFromTheRow.ctime});

    }
}