package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LsMusicInfo;
import java.util.List;

public interface LsMusicInfoMapper {
    int insert(LsMusicInfo lsMusicInfo);

    int deleteByPK(Long musicId);

    int updateByPK(LsMusicInfo lsMusicInfo);

    LsMusicInfo selectByPK(Long musicId);

    long count(LsMusicInfo lsMusicInfo);

    List<LsMusicInfo> select(LsMusicInfo lsMusicInfo);
}