/**
 * Created by xinchunting on 17-9-24.
 */
/**
 * Created by Shaka on 12/21/16.
 */
var width = $(".jqGrid_wrapper").width();
$(".datepicker").datepicker({
    todayBtn: "linked",
    clearBtn: true,
    autoclose: true,
    language: "zh-CN"
});
var Config = {
    selectDishCommentUrl: '/api/admin/dc/dish/comment/get',
    selectCompanyCommentUrl: '/api/admin/dc/company/comment/get',
    deleteDishCommentUrl: '/api/admin/dc/dish/comment/delete',
    deleteCompanyCommentUrl: '/api/admin/dc/company/comment/delete',
    updateUrlForCellEdit: '/api/admin/dc/dish/comment/update'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#dc_dish_comment").jqGrid({
        datatype: "json",
        url: Config.selectDishCommentUrl,
        height: 300,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["用户ID", "订单ID", "菜品名称", "赞/踩", "老板留言", "时间"],
        colModel: [{
            name: "userId",
            index: "userId",
            search: true
        }, {
            name: "orderId",
            index: "orderId"
        }, {
            name: "dishName",
            index: "dishName"
        }, {
            name: "upDown",
            index: "upDown",
            editoptions: {value: "0:赞;1:踩"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "remark",
            index: "remark",
            autowidth: true,
            editable: true,
            sortable: true
        }, {
            name: "ctime",
            index: "ctime"
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        pager: "#pager_dc_dish_comment",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dishCommentWrapper();
        }
    });
    jQuery("#dc_dish_comment").jqGrid('navGrid', '#pager_dc_dish_comment', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        width = $(".jqGrid_wrapper").width();
        $("#dc_dish_comment").setGridWidth(width);
    });


    $("#dc_company_comment").jqGrid({
        datatype: "json",
        url: Config.selectCompanyCommentUrl,
        height: 300,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["用户ID", "订单ID", "评分", "评论", "备注", "时间"],
        colModel: [{
            name: "userId",
            index: "userId",
            search: true
        }, {
            name: "orderId",
            index: "orderId"
        }, {
            name: "score",
            index: "score",
            editoptions: {value: "2:★;4:★★;6:★★★;8:★★★★;10:★★★★★"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "content",
            index: "content",
            sorttype: "int"
        }, {
            name: "remark",
            index: "remark"
        }, {
            name: "ctime",
            index: "ctime"
        }],
        pager: "#pager_dc_company_comment",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            companyCommentWrapper();
        }
    });
    jQuery("#dc_company_comment").jqGrid('navGrid', '#pager_dc_company_comment', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        width = $(".jqGrid_wrapper").width();
        $("#dc_company_comment").setGridWidth(width);
    });
});


// function dishCommentDelete(id, index) {
//     dishCommentUpdateDialog("DELETE", Config.deleteDishCommentUrl + '/' + id);
//     jQuery("#dc_dish_comment").jqGrid('delRowData', id);
// }

$("#dc_dish_comment_search").click(function () {
    var postdata = $("#dc_dish_comment").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.stime = dateToEpoch($("#beginDateOne").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    postdata.etime = dateToEpoch($("#endDateOne").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    jQuery("#dc_dish_comment").trigger("reloadGrid", postdata);
});
$("#dc_company_comment_search").click(function () {
    var postdata = $("#dc_company_comment").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.stime = dateToEpoch($("#beginDateTwo").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    postdata.etime = dateToEpoch($("#endDateTwo").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    jQuery("#dc_company_comment").trigger("reloadGrid", postdata);
});

// function companyCommentDelete(id, index) {
//     companyCommentUpdateDialog("DELETE", Config.deleteCompanyCommentUrl + '/' + id);
//     jQuery("#dc_company_comment").jqGrid('delRowData', id);
// }
//
// function dishCommentUpdateDialog(action, url) {
//     $.ajax(
//         {
//             url: url,
//             type: action,
//             success: function (data, textStatus, jqXHR) {
//                 if (action == 'DELETE') {
//                     jQuery("#dc_dish").trigger("reloadGrid");
//
//                 }
//             },
//             error: function (jqXHR, textStatus, errorThrown) {
//                 console.error(errorThrown);
//             }
//         });
// }
// function companyCommentUpdateDialog(action, url) {
//     $.ajax(
//         {
//             url: url,
//             type: action,
//             success: function (data, textStatus, jqXHR) {
//                 if (action == 'DELETE') {
//                     jQuery("#dc_dish").trigger("reloadGrid");
//
//                 }
//             },
//             error: function (jqXHR, textStatus, errorThrown) {
//                 console.error(errorThrown);
//             }
//         });
// }
function dishCommentWrapper() {
    var ids = jQuery("#dc_dish_comment").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#dc_dish_comment').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        jQuery("#dc_dish_comment").jqGrid('setRowData', ids[i], {
            ctime: dataFromTheRow.ctime
        });
    }
}

function companyCommentWrapper() {
    var ids = jQuery("#dc_company_comment").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#dc_company_comment').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        jQuery("#dc_company_comment").jqGrid('setRowData', ids[i], {
            ctime: dataFromTheRow.ctime
        });
    }
}
$("#ul li").click(function () {
    $("#dc_company_comment").setGridWidth(width);
    $("#dc_dish_comment").setGridWidth(width);
});

