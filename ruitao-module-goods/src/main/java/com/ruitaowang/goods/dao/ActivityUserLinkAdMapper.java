package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ActivityUserLink;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ActivityUserLinkAdMapper {

    long countRead(ActivityUserLink activityUserLink);

    List<ActivityUserLink> selectRead(ActivityUserLink activityUserLink);

    @Select("SELECT count(1) FROM rtshop.activity_user_link where ctime > #{stime} and ctime < #{etime} and author = #{author} and rstatus = 0")
    long todayCount(ActivityUserLink activityUserLink);
}