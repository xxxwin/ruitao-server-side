/**
 * Created by Administrator on 2017/5/17.
 */
var Config = {
    updateUrl: '/api/wap/user/update'
};
$().ready(function () {
    $('#submitCategoryUpdate').click(function () {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()) {
            //set content
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url: Config.updateUrl,
                    type: "PUT",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});