package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Verifycode;
import java.util.List;

public interface VerifycodeMapper {
    int insert(Verifycode verifycode);

    int deleteByPK(Long verifycodeId);

    int updateByPK(Verifycode verifycode);

    Verifycode selectByPK(Long verifycodeId);

    long count(Verifycode verifycode);

    List<Verifycode> select(Verifycode verifycode);
}