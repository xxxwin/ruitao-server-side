package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ReceivingAddress;
import java.util.List;

public interface ReceivingAddressMapper {
    int insert(ReceivingAddress receivingAddress);

    int deleteByPK(Long id);

    int updateByPK(ReceivingAddress receivingAddress);

    ReceivingAddress selectByPK(Long id);

    long count(ReceivingAddress receivingAddress);

    List<ReceivingAddress> select(ReceivingAddress receivingAddress);
}