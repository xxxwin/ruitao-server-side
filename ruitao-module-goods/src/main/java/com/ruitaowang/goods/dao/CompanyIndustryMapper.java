package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CompanyIndustry;
import java.util.List;

public interface CompanyIndustryMapper {
    int insert(CompanyIndustry companyIndustry);

    int deleteByPK(Long id);

    int updateByPK(CompanyIndustry companyIndustry);

    CompanyIndustry selectByPK(Long id);

    long count(CompanyIndustry companyIndustry);

    List<CompanyIndustry> select(CompanyIndustry companyIndustry);
}