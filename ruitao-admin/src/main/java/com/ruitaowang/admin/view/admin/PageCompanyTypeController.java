package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.CompanyType;
import com.ruitaowang.goods.service.CompanyTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
@RestController
public class PageCompanyTypeController {
	@Autowired
    private CompanyTypeService companyTypeService;

    @RequestMapping(value = {"/companytype/update/{id}"})
    public ModelAndView update(@PathVariable("id") Long id, Model model){
        CompanyType companyType=companyTypeService.selectByPK(id);
        model.addAttribute("companyType", companyType);
        return new ModelAndView("web/company_type_update");
    }
}
