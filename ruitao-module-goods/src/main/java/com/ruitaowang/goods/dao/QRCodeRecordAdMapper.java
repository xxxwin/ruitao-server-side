package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.QRCodeRecord;
import com.ruitaowang.core.domain.QRCodeRecordVO;

import java.util.List;

public interface QRCodeRecordAdMapper {

    List<QRCodeRecordVO> selectForBh(QRCodeRecord qRCodeRecord);
}