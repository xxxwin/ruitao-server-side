/**
 * Created by Shaka on 12/21/16.
 */

var Config = {
    selectUrl: "/api/admin/ws/orders",
    updateUrlForCellEdit: '/api/admin/order/cellEdit',
    selectExcelUrl: "/api/admin/orders/excel/list"
};

$(document).ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        postData: {payStatus: 1},
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "订单编号", "消费者", "支付状态", "审核状态", "订单状态", "购买方式", "订单总额", "订单地址", "操作"],
        colModel: [{
            name: "orderId",
            index: "orderId",
            sorttype: "int",
            width: 5,
            search: true,
            key: true
        }, {
            name: "orderSn",
            index: "orderSn",
            width: 20,
            search: true,
        }, {
            name: "userId",
            index: "userId",
            width: 10,
        }, {
            name: "payStatus",
            index: "payStatus",
            sorttype: "int",
            width: 10,
            editoptions: {value: "0:未支付;1:已支付"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "auditStatus",
            index: "auditStatus",
            sorttype: "int",
            width: 10,
            editoptions: {value: "0:未审核;1:审核通过;2:审核未通过"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "orderStatus",
            index: "orderStatus",
            width: 10,
            sortable: true,
            editable: true,
            editoptions: {value: "0:待发货;1:已发货"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "goodsPayType",
            index: "goodsPayType",
            sorttype: "int",
            width: 10,
            editoptions: {value: "0:现金;1:红包;2:红包+现金"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "orderAmount",
            index: "orderAmount",
            width: 10,
            formatter: amountformatter,
            sortable: false

        }, {
            name: "shippingAddress",
            index: "shippingAddress",
            width: 25,
            sortable: false

        }, {
            name: "operate",
            index: "operate",
            width: 25,
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        pager: "#pager_data_list",
        viewrecords: true,
        hidegrid: false,
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "orderId"
        },
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#data_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/ws/orders/update/' + id + '" data-index="0">详细</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html});

    }
}
function amountformatter(cellvalue, options, rowObject) {
    var price;
    $.ajax({
        url: "/api/admin/orders/ByPK",
        type: "GET",
        data: {orderId: rowObject.orderId},
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            price = data.data.exPrice + data.data.orderAmount - data.data.memberPrice;
        }
    });
    return toFixedForPrice(price);
}