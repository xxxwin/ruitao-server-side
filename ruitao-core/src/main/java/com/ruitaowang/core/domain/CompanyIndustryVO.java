package com.ruitaowang.core.domain;

import java.util.List;

public class CompanyIndustryVO extends CompanyIndustry {

    public List<CompanyType> getCompanyTypes() {
        return companyTypes;
    }

    public void setCompanyTypes(List<CompanyType> companyTypes) {
        this.companyTypes = companyTypes;
    }

    private List<CompanyType> companyTypes;
}
