/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/admin/categories/post',
    updateUrl: '/api/admin/categories/put',
    deleteUrl: '/api/admin/categories/delete',
    selectUrl: '/api/admin/categories/get'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$().ready(function () {
    $("#datepicker").datepicker({keyboardNavigation: !1, forceParse: !1, autoclose: !0});
    $.ajax(
        {
            url: Config.selectUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="0" hassubinfo="true">顶级分类</option>');
                data.data.map(function(category){
                    categories.push('<option value="');
                    categories.push(category.id);
                    categories.push('" ');
                    categories.push('hassubinfo="true" ');
                    if (parentCategoryIdChecked == category.id){
                        categories.push('selected="selected">');
                    }else{
                        categories.push('>');
                    }
                    categories.push(category.name);
                    categories.push('</option>');
                });
                $("#parentId").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    
  //为jquery.serializeArray()解决radio,checkbox未选中时没有序列化的问题
    $.fn.ghostsf_serialize = function () {
        var a = this.serializeArray();
        var $radio = $('input[type=radio],input[type=checkbox]', this);
        var temp = {};
        $.each($radio, function () {
            if (!temp.hasOwnProperty(this.name)) {
                if ($("input[name='" + this.name + "']:checked").length == 0) {
                    temp[this.name] = "";
                    a.push({name: this.name, value: ""});
                }
            }
        });
        //console.log(a);
        return jQuery.param(a);
    };

    $('#submitCategoryCreate').click( function() {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()){
            var postData = $('#ajaxform').ghostsf_serialize();
            console.log(postData);
            $.ajax(
                {
                    url : Config.updateUrl,
                    type: "PUT",
                    data : postData,
                    dataType: "json",
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
              		    window.parent.window.queryCates();  
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });

    $('#buttonGoback').click(function() {
        history.go(-1);
        location.reload();
        return false;
    });
});
