package com.ruitaowang.core.domain;

public class Relations extends Page {
    /**
     *  用户编号,所属表字段为relations.uesr_id
     */
    private Long uesrId;

    /**
     *  ,所属表字段为relations.pid
     */
    private Long pid;

    /**
     *  备注,所属表字段为relations.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为relations.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为relations.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为relations.rstatus
     */
    private Byte rstatus;

    public Long getUesrId() {
        return uesrId;
    }

    public void setUesrId(Long uesrId) {
        this.uesrId = uesrId;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}