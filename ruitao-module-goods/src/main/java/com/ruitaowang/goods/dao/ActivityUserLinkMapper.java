package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ActivityUserLink;

import java.util.List;

public interface ActivityUserLinkMapper {
    int insert(ActivityUserLink activityUserLink);

    int updateByPK(ActivityUserLink activityUserLink);

    ActivityUserLink selectByPK(Long userLinkId);

    long count(ActivityUserLink activityUserLink);

    List<ActivityUserLink> select(ActivityUserLink activityUserLink);
}