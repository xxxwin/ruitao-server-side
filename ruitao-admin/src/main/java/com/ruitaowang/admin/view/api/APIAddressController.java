package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.Address;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.utils.RandomUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APIAddressController extends BaseController {

    @Autowired
    private AddressService addressService;

    @RequestMapping("/api/addresses")
    public String list(Address address, int page) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        address.setUserId(user.getId());
        address.setPage(page);
        MsgOut o = MsgOut.success(addressService.selectForPage(address));
        o.setRecords(address.getRecords());
        o.setTotal(address.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping("/api/addresses/{addressId}")
    public String selectByPK(@PathVariable Long addressId) {
        return this.renderJson(MsgOut.success(addressService.selectByPK(addressId)));
    }

    @RequestMapping(value = "/api/addresses", method = RequestMethod.POST)
    public String create(Address address) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if(address.getDefaultt() != null && address.getDefaultt() == 1){
            //取消默认
            addressService.updateBatchForDefault(user.getId());
        }
        address.setUserId(user.getId());
        addressService.insert(address);
        MsgOut o = MsgOut.success(address);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/addresses", method = RequestMethod.PUT)
    public String update(Address address) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }

        if(address.getDefaultt() != null && address.getDefaultt() == 1){
            //取消默认
            addressService.updateBatchForDefault(user.getId());
        }
        addressService.update(address);
        MsgOut o = MsgOut.success(address);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/addresses/{addressId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("addressId") Long addressId) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Address address = addressService.selectByPK(addressId);
        if(user.getId().equals(address.getUserId())){
            addressService.delete(addressId);
            MsgOut o = MsgOut.success();
            return this.renderJson(o);
        }
        return this.renderJson(MsgOut.error("denied"));
    }
}
