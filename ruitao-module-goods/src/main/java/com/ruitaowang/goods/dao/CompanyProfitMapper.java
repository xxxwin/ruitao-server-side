package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CompanyProfit;
import java.util.List;

public interface CompanyProfitMapper {
    int insert(CompanyProfit companyProfit);

    int deleteByPK(Long profitId);

    int updateByPK(CompanyProfit companyProfit);

    CompanyProfit selectByPK(Long profitId);

    long count(CompanyProfit companyProfit);

    List<CompanyProfit> select(CompanyProfit companyProfit);
}