/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/admin/company/wallet',
    updateUrl: '/api/admin/company/wallet',
    deleteUrl: '/api/admin/company/wallet',
    selectUrl: '/api/admin/company/wallet',
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url:Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "名字","商户类型", "余额","总金额", "创建时间","操作"],
        colModel: [{
            name: "companyId",
            index: "companyId",
            width: 5,
            editable: false,
            sorttype: "int",
            search: true
        }, {
            name: "remark",
            index: "remark",
            width: 14,
            editable: true,
        }, {
            name: "companyType",
            index: "companyType",
            width: 10,
            editable: true,
            editoptions: {value: "0:共享通商家;1:共享通商家;2:心理咨询;3:城市合伙人;4:梦想合伙人;5:教育培训;6:联合创始人;7:艺术家;8:供货商;9:产业合伙人;11:共享通商家"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "companyBalance",
            index: "companyBalance",
            width: 10,
            editable: true,
        }, {
            name: "companyAmount",
            index: "companyAmount",
            width: 10,
            editable: true,
        }, {
            name: "ctime",
            index: "ctime",
            width: 10,
            editable: true
        }, {
            name: "operate",
            index: "operate",
            width: 6,
            sortable: false
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "companyId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});

});
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS"){
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    }else{
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
}
$("#perform_search").click(function() {
    var postdata = $("#data_list").jqGrid('getGridParam','postData');
    postdata._search = false;
    postdata.companyType = $('#companyType option:selected') .val();
    postdata.companyName=$('#companyName').val();
    jQuery("#data_list").trigger("reloadGrid",[{page:1}]);
});
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id)
        dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss()
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/web/wallet_company_update.html?id='+ id +'" data-index="0">详细</a>';
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/api/admin/orderProd/excel/'+id+'" >订单导出</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate:html,companyBalance: toFixedForPrice(dataFromTheRow.companyBalance),companyAmount: toFixedForPrice(dataFromTheRow.companyAmount), ctime: dataFromTheRow.ctime});
    }
}