package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Coupon;
import com.ruitaowang.goods.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;

/**
 * 商品规格
 */
@RestController
public class PageGoodsSpecificationsController {

    @Autowired
    private CouponService couponService;

    @RequestMapping(value = {"/admin/web/create_specifications"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView updateSpecifications() {
        return new ModelAndView("web/create_specifications");
    }

    @RequestMapping(value = {"/admin/web/create_discount"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView updateDiscount() {
        return new ModelAndView("web/local_discount_create");
    }
}
