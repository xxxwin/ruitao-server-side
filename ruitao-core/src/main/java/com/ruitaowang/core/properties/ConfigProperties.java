package com.ruitaowang.core.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by neal on 09/01/2017.
 */
@Component
public class ConfigProperties {

    @Value("${host.oss}")
    public String  hostOSS = "http://static.ruitaowang.com";
    @Value("${host.api}")
    public String  hostAPI = "https://api.ruitaowang.com";
    @Value("${host.wap}")
    public String  hostWap = "https://www.ruitaowang.com";
    @Value("${host.return.uri}")
    public String  returnUri = "https://www.ruitaowang.com/wap/enroll";

    public String  wxPayNotifyUrl = getHostAPI() + "/api/wxs/notify";

    public String getWxPayNotifyUrl() {
        return wxPayNotifyUrl;
    }

    public void setWxPayNotifyUrl(String wxPayNotifyUrl) {
        this.wxPayNotifyUrl = wxPayNotifyUrl;
    }

    public String getReturnUri() {
        return returnUri;
    }

    public void setReturnUri(String returnUri) {
        this.returnUri = returnUri;
    }

    public String getHostOSS() {
        return hostOSS;
    }

    public void setHostOSS(String hostOSS) {
        this.hostOSS = hostOSS;
    }

    public String getHostAPI() {
        return hostAPI;
    }

    public void setHostAPI(String hostAPI) {
        this.hostAPI = hostAPI;
    }

    public String getHostWap() {
        return hostWap;
    }

    public void setHostWap(String hostWap) {
        this.hostWap = hostWap;
    }
}
