/**
 * Created by xinchunting on 17-9-25.
 */
var companyId;
$(document).ready(function () {
    $.ajax({
        url: "/api/admin/dc/index/get",
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            console.log(data);
            companyId=data.data[0].companyId;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
});

function nowFunction(name){
    var url="";
    var title=[];
    if (name == "dish"){
        console.log(companyId);
        url="/web/admin_dinner_dish.html?id="+companyId;
        title=["门店菜品管理功能界面","font-size:10px;"];
        skip(url,title);
    }
    if (name == "brand"){
        url="/web/admin_dinner_brand.html?id="+companyId;
        title=["门店品牌管理功能界面","font-size:10px;"];
        skip(url,title);
    }
    if (name == "account"){
        url="/web/admin_dinner_account.html?id="+companyId;
        title=["门店菜品管理功能界面","font-size:10px;"];
        skip(url,title);
    }
    if (name == "consoleTable"){
        url="/web/admin_dinner_console.html?id="+companyId;
        title=["门店餐桌管理功能界面","font-size:10px;"];
        skip(url,title);
    }
    if (name == "sales"){
        url="/web/admin_dinner_sales.html?id="+companyId;
        title=["门店促销管理功能界面","font-size:10px;"];
        skip(url,title);
    }
    if (name == "dishOrder"){
        url="/web/admin_dinner_order.html?id="+companyId;
        title=["门店订单管理功能界面","font-size:10px;"];
        skip(url,title);
    }
    if (name == "mouthMarketing"){
        url="/web/admin_dinner_mouthMarketing.html?id="+companyId;
        title=["门店口碑营销管理功能界面","font-size:10px;"];
        skip(url,title);
    }
}

function skip(url,title) {
    layer.open({
        type: 2,
        title:title,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:url
    })
}