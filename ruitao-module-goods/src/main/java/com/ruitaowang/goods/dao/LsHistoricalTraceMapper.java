package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LsHistoricalTrace;
import java.util.List;

public interface LsHistoricalTraceMapper {
    int insert(LsHistoricalTrace lsHistoricalTrace);

    int deleteByPK(Long traceId);

    int updateByPK(LsHistoricalTrace lsHistoricalTrace);

    LsHistoricalTrace selectByPK(Long traceId);

    long count(LsHistoricalTrace lsHistoricalTrace);

    List<LsHistoricalTrace> select(LsHistoricalTrace lsHistoricalTrace);
}