package com.ruitaowang.core.domain;

public class SNSWXUserinfo extends Page {
    /**
     *  用户编码,所属表字段为sns_wx_userinfo.user_id
     */
    private Long userId;

    /**
     *  微信昵称,所属表字段为sns_wx_userinfo.nickname
     */
    private String nickname;

    /**
     *  活动链接,所属表字段为sns_wx_userinfo.openid
     */
    private String openid;

    /**
     *  互动内容,所属表字段为sns_wx_userinfo.headimgurl
     */
    private String headimgurl;

    /**
     *  活动类型
0 轮播图
1 二级banner,所属表字段为sns_wx_userinfo.sex
     */
    private Byte sex;

    /**
     *  创建时间,所属表字段为sns_wx_userinfo.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为sns_wx_userinfo.mtime
     */
    private Long mtime;

    /**
     *  0 正常
1 删除,所属表字段为sns_wx_userinfo.rstatus
     */
    private Byte rstatus;

    /**
     *  商品编码,所属表字段为sns_wx_userinfo.province
     */
    private String province;

    /**
     *  排序,所属表字段为sns_wx_userinfo.city
     */
    private String city;

    /**
     *  广告图片,所属表字段为sns_wx_userinfo.activity_image_url
     */
    private String activityImageUrl;

    /**
     *  ,所属表字段为sns_wx_userinfo.language
     */
    private String language;

    /**
     *  ,所属表字段为sns_wx_userinfo.country
     */
    private String country;

    /**
     *  ,所属表字段为sns_wx_userinfo.unionid
     */
    private String unionid;

    /**
     *  ,所属表字段为sns_wx_userinfo.access_token
     */
    private String accessToken;

    /**
     *  ,所属表字段为sns_wx_userinfo.expires_in
     */
    private Integer expiresIn;

    /**
     *  ,所属表字段为sns_wx_userinfo.refresh_token
     */
    private String refreshToken;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl == null ? null : headimgurl.trim();
    }

    public Byte getSex() {
        return sex;
    }

    public void setSex(Byte sex) {
        this.sex = sex;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getActivityImageUrl() {
        return activityImageUrl;
    }

    public void setActivityImageUrl(String activityImageUrl) {
        this.activityImageUrl = activityImageUrl == null ? null : activityImageUrl.trim();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language == null ? null : language.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid == null ? null : unionid.trim();
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken == null ? null : accessToken.trim();
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken == null ? null : refreshToken.trim();
    }
}