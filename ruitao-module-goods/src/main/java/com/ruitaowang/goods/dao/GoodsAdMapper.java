package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.domain.GoodsRecommend;
import com.ruitaowang.core.domain.GoodsVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface GoodsAdMapper {
    List<Goods> select(Goods goods);

    List<Goods> selectForWarning(Goods goods);

    List<Goods> selectForHomepage(Goods goods);

    long count(Goods goods);

    long selectForWarningCount(Goods goods);

    List<GoodsVO> seaElection(GoodsVO goodsVO);

    long seaElectionCount(GoodsVO goodsVO);

    List<GoodsVO> selectForSort(GoodsVO goodsVO);

    long selectForSortCount(GoodsVO goodsVO);


    @Update("update rtshop.goods set give_score = #{val} where rstatus = 0")
    void setUpWarning(@Param("val") int val);
}
