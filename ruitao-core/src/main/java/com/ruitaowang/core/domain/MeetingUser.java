package com.ruitaowang.core.domain;

public class MeetingUser extends Page {
    /**
     *  自增主键,所属表字段为meeting_users.meeting_user_id
     */
    private Long meetingUserId;

    /**
     *  部门,所属表字段为meeting_users.department
     */
    private String department;

    /**
     *  手机号,所属表字段为meeting_users.phone
     */
    private String phone;

    /**
     *  职位,所属表字段为meeting_users.position
     */
    private String position;

    /**
     *  备注,所属表字段为meeting_users.remark
     */
    private String remark;

    /**
     *  邮箱,所属表字段为meeting_users.email
     */
    private String email;

    /**
     *  创建时间,所属表字段为meeting_users.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为meeting_users.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为meeting_users.rstatus
     */
    private Byte rstatus;

    /**
     *  真实姓名,所属表字段为meeting_users.real_name
     */
    private String realName;

    /**
     *  1 已签到 
0 未签到,所属表字段为meeting_users.locked
     */
    private Boolean locked;

    /**
     *  身份证号,所属表字段为meeting_users.id_card
     */
    private String idCard;

    /**
     *  ,所属表字段为meeting_users.meeting_id
     */
    private Long meetingId;

    /**
     *  公司名字,所属表字段为meeting_users.company_name
     */
    private String companyName;

    public Long getMeetingUserId() {
        return meetingUserId;
    }

    public void setMeetingUserId(Long meetingUserId) {
        this.meetingUserId = meetingUserId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public Long getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(Long meetingId) {
        this.meetingId = meetingId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }
}