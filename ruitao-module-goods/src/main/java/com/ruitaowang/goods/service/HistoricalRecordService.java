package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.HistoricalRecord;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.HistoricalRecordAdMapper;
import com.ruitaowang.goods.dao.HistoricalRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoricalRecordService implements BaseService<HistoricalRecord> {

    @Autowired
    private HistoricalRecordMapper historicalRecordMapper;
    @Autowired
    private HistoricalRecordAdMapper historicalRecordAdMapper;

    @Override
    public HistoricalRecord insert(HistoricalRecord historicalRecord){
        Long time = System.currentTimeMillis();
        historicalRecord.setCtime(time);
        historicalRecord.setMtime(time);
        historicalRecordMapper.insert(historicalRecord);
        return historicalRecord;
    }

    @Override
    public int delete(Long userId) {
        HistoricalRecord historicalRecord = new HistoricalRecord();
        historicalRecord.setUserId(userId);
        historicalRecord.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        historicalRecord.setMtime(time);
        return historicalRecordMapper.updateByPK(historicalRecord);
    }

    @Override
    public HistoricalRecord update(HistoricalRecord historicalRecord) {
        Long time = System.currentTimeMillis();
        historicalRecord.setMtime(time);
        historicalRecordMapper.updateByPK(historicalRecord);
        return null;
    }

    @Override
    public HistoricalRecord selectByPK(Long id) {
        return historicalRecordMapper.selectByPK(id);
    }

    @Override
    public List<HistoricalRecord> select(HistoricalRecord historicalRecord) {
        return historicalRecordMapper.select(historicalRecord);
    }

    public List<HistoricalRecord> selectForPage(HistoricalRecord historicalRecord) {
        historicalRecord.setRstatus((byte) 0);
        historicalRecord.setRecords(historicalRecordMapper.count(historicalRecord));
        return historicalRecordMapper.select(historicalRecord);
    }

    public int deleteForAll(Long userId,String historyType) {
        HistoricalRecord historicalRecord = new HistoricalRecord();
        historicalRecord.setUserId(userId);
        historicalRecord.setRstatus((byte) 1);
        historicalRecord.setHistoryType(historyType);
        Long time = System.currentTimeMillis();
        historicalRecord.setMtime(time);
        return historicalRecordAdMapper.update(historicalRecord);
    }
}
