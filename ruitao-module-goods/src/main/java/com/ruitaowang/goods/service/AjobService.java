package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Ajob;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.AjobMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AjobService implements BaseService<Ajob> {

    @Autowired
    private AjobMapper ajobMapper;

    @Override
    public Ajob insert(Ajob ajob) {
        Long time = System.currentTimeMillis();
        ajob.setCtime(time);
        ajob.setMtime(time);
        ajobMapper.insert(ajob);
        return ajob;
    }

    @Override
    public int delete(Long id) {
        Ajob ajob = new Ajob();
        ajob.setAjobId(id);
        ajob.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        ajob.setMtime(time);
        return ajobMapper.updateByPK(ajob);
    }

    @Override
    public Ajob update(Ajob ajob) {
        Long time = System.currentTimeMillis();
        ajob.setMtime(time);
        ajobMapper.updateByPK(ajob);
        return ajob;
    }

    @Override
    public Ajob selectByPK(Long id) {
        return ajobMapper.selectByPK(id);
    }

    @Override
    public List<Ajob> select(Ajob ajob) {
        ajob.setRstatus((byte) 0);
        return ajobMapper.select(ajob);
    }

    public List<Ajob> selectForPage(Ajob ajob) {
        ajob.setRstatus((byte) 0);
        ajob.setRecords(ajobMapper.count(ajob));
        return ajobMapper.select(ajob);
    }
}
