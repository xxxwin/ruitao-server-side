package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.AfterMarket;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.AfterMarketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class AfterMarketService implements BaseService<AfterMarket> {
    @Autowired
    private AfterMarketMapper activityMapper;

    @Override
    public AfterMarket insert(AfterMarket activity) {
        Long time = System.currentTimeMillis();
        activity.setCtime(time);
        activity.setMtime(time);
        activityMapper.insert(activity);
        return activity;
    }

    @Override
    public int delete(Long id) {
        AfterMarket activity = new AfterMarket();
        activity.setId(id);
        activity.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        activity.setMtime(time);
        return activityMapper.updateByPK(activity);
    }

    @Override
    public AfterMarket update(AfterMarket activity) {
        Long time = System.currentTimeMillis();
        activity.setMtime(time);
        activityMapper.updateByPK(activity);
        return activity;
    }

    @Override
    public AfterMarket selectByPK(Long id) {
        return activityMapper.selectByPK(id);
    }

    @Override
    public List<AfterMarket> select(AfterMarket activity) {
        activity.setRstatus((byte) 0);
        return activityMapper.select(activity);
    }
    public List<AfterMarket> selectRstatus(AfterMarket activity) {
        return activityMapper.select(activity);
    }
    public List<AfterMarket> selectForPage(AfterMarket activity) {
//        activity.setRstatus((byte) 0);
        activity.setRecords(activityMapper.count(activity));
        return activityMapper.select(activity);
    }
}

