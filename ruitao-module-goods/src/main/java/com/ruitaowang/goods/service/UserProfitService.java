package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserProfit;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserProfitAdMapper;
import com.ruitaowang.goods.dao.UserProfitMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class UserProfitService implements BaseService<UserProfit> {
    @Autowired
    private UserProfitMapper userProfitMapper;
    @Autowired
    private UserProfitAdMapper userProfitAdMapper;

    @Override
    public UserProfit insert(UserProfit userProfit) {
        Long time = System.currentTimeMillis();
        userProfit.setCtime(time);
        userProfit.setMtime(time);
        userProfitMapper.insert(userProfit);
        return userProfit;
    }

    @Override
    public int delete(Long id) {
        UserProfit userProfit = new UserProfit();
        userProfit.setUserId(id);
        userProfit.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userProfit.setMtime(time);
        return userProfitMapper.updateByPK(userProfit);
    }

    @Override
    public UserProfit update(UserProfit userProfit) {
        Long time = System.currentTimeMillis();
        userProfit.setMtime(time);
        userProfitMapper.updateByPK(userProfit);
        return userProfit;
    }

    @Override
    public UserProfit selectByPK(Long id) {
        return userProfitMapper.selectByPK(id);
    }

    @Override
    public List<UserProfit> select(UserProfit userProfit) {
        userProfit.setRstatus((byte) 0);
        return userProfitMapper.select(userProfit);
    }

    public List<UserProfit> selectForPage(UserProfit userProfit) {
        userProfit.setRstatus((byte) 0);
        userProfit.setRecords(userProfitMapper.count(userProfit));
        return userProfitMapper.select(userProfit);
    }
    //查询返利 2018.7.27
    public List<UserProfit> selectUserProfitForPage(UserProfit userProfit) {
        userProfit.setRstatus((byte) 0);
        userProfit.setRecords(userProfitAdMapper.selectUserProfitCount(userProfit));
        return userProfitAdMapper.selectUserProfit(userProfit);
    }

    public List<UserProfit> timeForPage(UserProfit userProfit) {
        userProfit.setRstatus((byte) 0);
        userProfit.setRecords(userProfitAdMapper.count(userProfit));
        return userProfitAdMapper.select(userProfit);
    }
    /**
     * 快转总榜单
     * @param userProfit
     * @return
     */
    public List<UserProfit> selectKuaiZhuanTopForTotal(UserProfit userProfit) {
        userProfit.setRstatus((byte) 0);
        userProfit.setOrderType((byte) 7);
        userProfit.setOrderBy("user_profit desc");
        userProfit.setRecords(userProfitAdMapper.count(userProfit));
        return userProfitAdMapper.selectKuaiZhuanTopForTotal(userProfit);
    }
    /**
     * 微传媒榜单红包奖励
     * @param userProfit
     * @return
     */
    public List<UserProfit> selectRankingRewardStatus(UserProfit userProfit) {
        return userProfitAdMapper.selectRankingRewardStatus(userProfit);
    }

    /**
     * 微传媒榜单红包奖励(间推)
     * @param userProfit
     * @return
     */
    public List<UserProfit> selectRankingRewardStatusTow(UserProfit userProfit) {
        return userProfitAdMapper.selectRankingRewardStatusTwo(userProfit);
    }
    /**
     * 拆红包列表
     * @param paySn
     * @return
     */
    public List<UserProfit> selectByPaySN(String paySn) {
        UserProfit where = new UserProfit();
        where.setRstatus((byte) 0);
        where.setOrderType((byte) 19);
        where.setOrderSn(paySn);
        return userProfitMapper.select(where);
    }

    public Long count(UserProfit userProfit) {
        userProfit.setRstatus((byte)0);
        return userProfitMapper.count(userProfit);
    }

}
