package com.ruitaowang.core.domain;

public class StatCompanyUser extends Page {
    /**
     *  ,所属表字段为stat_company_user.id
     */
    private Long id;

    /**
     *  编号,所属表字段为stat_company_user.company_id
     */
    private Long companyId;

    /**
     *  商城订单数,所属表字段为stat_company_user.order_count
     */
    private Integer orderCount;

    /**
     *  创建时间,所属表字段为stat_company_user.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为stat_company_user.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常，1 删除,所属表字段为stat_company_user.rstatus
     */
    private Byte rstatus;

    /**
     *  扫码订单数,所属表字段为stat_company_user.local_order_count
     */
    private Integer localOrderCount;

    /**
     *  用户消费额,所属表字段为stat_company_user.order_consumption
     */
    private Integer orderConsumption;

    /**
     *  备注,所属表字段为stat_company_user.remark
     */
    private String remark;

    /**
     *  店面用户消费额,所属表字段为stat_company_user.local_order_consumption
     */
    private Integer localOrderConsumption;

    /**
     *  点餐订餐数量,所属表字段为stat_company_user.dish_order_count
     */
    private Integer dishOrderCount;

    /**
     *  点餐订单消费额,所属表字段为stat_company_user.dish_order_consumption
     */
    private Integer dishOrderConsumption;

    /**
     *  日期,所属表字段为stat_company_user.date
     */
    private String date;

    /**
     *  ,所属表字段为stat_company_user.user_id
     */
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getLocalOrderCount() {
        return localOrderCount;
    }

    public void setLocalOrderCount(Integer localOrderCount) {
        this.localOrderCount = localOrderCount;
    }

    public Integer getOrderConsumption() {
        return orderConsumption;
    }

    public void setOrderConsumption(Integer orderConsumption) {
        this.orderConsumption = orderConsumption;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getLocalOrderConsumption() {
        return localOrderConsumption;
    }

    public void setLocalOrderConsumption(Integer localOrderConsumption) {
        this.localOrderConsumption = localOrderConsumption;
    }

    public Integer getDishOrderCount() {
        return dishOrderCount;
    }

    public void setDishOrderCount(Integer dishOrderCount) {
        this.dishOrderCount = dishOrderCount;
    }

    public Integer getDishOrderConsumption() {
        return dishOrderConsumption;
    }

    public void setDishOrderConsumption(Integer dishOrderConsumption) {
        this.dishOrderConsumption = dishOrderConsumption;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}