package com.ruitaowang.wap;

import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.CompanyTypeService;
import com.ruitaowang.goods.service.CouponService;
import com.ruitaowang.goods.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class APIWapCouponController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private CouponService couponService;
    @Autowired
    private CompanyTypeService companyTypeService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * @api {get} /api/wap/coupon 优惠券-查询所有
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/coupon
     *
     * @apiName list
     * @apiGroup coupon
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} goodsId 商品ID.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {byte} couponType 优惠券 类别.
     * @apiParam {long} couponTime 优惠券 时间.
     * @apiParam {int} percentage 优惠折扣.
     * @apiParam {int} reach1 满.
     * @apiParam {int} score1 返.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "percentage":0,
     *                   "companyId":0,
     *                   "couponType":0,
     *                   "reach1":0,
     *                   "score1":0,
     *                   "goodsId":0,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "userId":6
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/coupon"
     *  }
     */
    @RequestMapping("/api/wap/coupon")
    public String list(Coupon coupon, @RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(coupon)) {
            return this.renderJson(MsgOut.error("no data."));
        }
        Company company=companyService.selectByCompanyStaff(Long.parseLong(userId));
        coupon.setCompanyId(company.getCompanyId());
        MsgOut o = MsgOut.success(couponService.select(coupon));
        return this.renderJson(o);
    }

    /**
     * 带有员工的优惠券
     * @param coupon
     * @param userId
     * @return
     */
    @RequestMapping("/api/wap/staffCoupon")
    public String staffCoupon(Coupon coupon, @RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(coupon)) {
            return this.renderJson(MsgOut.error("no data."));
        }
        Company company=companyService.selectByCompanyStaff(Long.parseLong(userId));
        coupon.setCompanyId(company.getCompanyId());
        List<Coupon> couponList = couponService.select(coupon);
        MsgOut o = MsgOut.success(couponList);
        return this.renderJson(o);
    }

    @RequestMapping("/api/wap/nohead/coupon")
    public String gxtList(Coupon coupon) {
        if (ObjectUtils.isEmpty(coupon)) {
            return this.renderJson(MsgOut.error("no data."));
        }
        coupon.setCompanyId(coupon.getCompanyId());
        MsgOut o = MsgOut.success(couponService.select(coupon));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/wap/coupon/page 优惠券-(分页)查询所有
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/coupon/page
     *
     * @apiName listForPage
     * @apiGroup coupon
     *
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiParam {String} remark 备注.
     * @apiParam {long} goodsId 商品ID.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {byte} couponType 优惠券 类别.
     * @apiParam {long} couponTime 优惠券 时间.
     * @apiParam {int} percentage 优惠折扣.
     * @apiParam {int} reach1 满.
     * @apiParam {int} score1 返.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "percentage":0,
     *                   "companyId":0,
     *                   "couponType":0,
     *                   "reach1":0,
     *                   "score1":0,
     *                   "goodsId":0,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "userId":6
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/coupon/page"
     *  }
     */
    @RequestMapping("/api/wap/coupon/page")
    public String listForPage(Coupon coupon, int page) {
        coupon.setPage(page);
        if (!ObjectUtils.isEmpty(coupon.getSidx())) {
            coupon.setOrderBy(coupon.getSidx() + " " + coupon.getSord() + "," + coupon.getOrderBy());
        }
        MsgOut o = MsgOut.success(couponService.selectForPage(coupon));
        o.setRecords(coupon.getRecords());
        o.setTotal(coupon.getTotal());
        return this.renderJson(o);
    }

    /**
     * 首页折扣券调取该分类优惠券及商品
     * @param id
     * @return
     */
    @RequestMapping("/api/wap/goods/coupon")
    public String goodsListOrCoupon(Long id) {
        if (!ObjectUtils.isEmpty(id)) {
            return this.renderJson(MsgOut.error("参数错误"));
        }
        List listOut=new ArrayList();
        Coupon coupon = new Coupon();
        coupon.setCompanyTypeId(id);
        coupon.setCouponType((byte)0);
        List<Coupon> coupons=couponService.select(coupon);
        if (!ObjectUtils.isEmpty(coupons)) {
            for (Coupon c:coupons){
                CouponVO couponVO=new CouponVO();
                BeanUtils.copyProperties(c,couponVO);
                couponVO.setGoods(goodsService.selectByPK(c.getGoodsId()));
                listOut.add(couponVO);
            }
        }
        MsgOut o = MsgOut.success(listOut);
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/wap/coupon/Industry 优惠券-(分页)查询所有
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/coupon/Industry
     *
     * @apiName listForPage
     * @apiGroup coupon
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} goodsId 商品ID.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {byte} couponType 优惠券 类别.
     * @apiParam {long} couponTime 优惠券 时间.
     * @apiParam {int} percentage 优惠折扣.
     * @apiParam {int} reach1 满.
     * @apiParam {int} score1 返.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "percentage":0,
     *                   "companyId":0,
     *                   "couponType":0,
     *                   "reach1":0,
     *                   "score1":0,
     *                   "goodsId":0,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "userId":6
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/coupon/Industry"
     *  }
     */
    @RequestMapping("/api/wap/coupon/Industry")
    public String listForIndustry(Long id) {
        if (!ObjectUtils.isEmpty(id)) {
            return this.renderJson(MsgOut.error("参数错误"));
        }
        List<Long> companyTypeIds= new ArrayList<>();
        List<Coupon> outList= new ArrayList<>();
        List<Coupon> coupons = new ArrayList<>();
        CompanyType companyType=new CompanyType();
        companyType.setIndustryId(id);
        List<CompanyType> companyTypes= companyTypeService.select(companyType);
        for (CompanyType ct:companyTypes){
            companyTypeIds.add(ct.getId());
        }
        for (Long l:companyTypeIds){
            Coupon cp=new Coupon();
            cp.setCompanyId(l);
            coupons=couponService.select(cp);
            for (Coupon co:coupons){
                outList.add(co);
            }
        }
        MsgOut o = MsgOut.success(outList);
        return this.renderJson(o);
    }

    /**
     * @api {post} /api/wap/coupon 优惠券-创建
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/coupon
     *
     * @apiName create
     * @apiGroup coupon
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} goodsId 商品ID.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {byte} couponType 优惠券 类别.
     * @apiParam {long} couponTime 优惠券 时间.
     * @apiParam {int} percentage 优惠折扣.
     * @apiParam {int} reach1 满.
     * @apiParam {int} score1 返.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "percentage":0,
     *                   "companyId":0,
     *                   "couponType":0,
     *                   "reach1":0,
     *                   "score1":0,
     *                   "goodsId":0,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "userId":6
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/coupon"
     *  }
     */
    @RequestMapping(value = "/api/wap/coupon", method = RequestMethod.POST)
    public String create(Coupon coupon,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        SysUser user = sysUserService.selectByPK(Long.parseLong(userId));
        Company company=companyService.selectByCompanyStaff(user.getId());
        if (ObjectUtils.isEmpty(company)) {
            return this.renderJson(MsgOut.error("no data."));
        }
        coupon.setCompanyId(company.getCompanyId());
        couponService.insert(coupon);
        MsgOut o = MsgOut.success(coupon);
        return this.renderJson(o);
    }

    /**
     * 带有员工的修改优惠券
     * @param coupon
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/staffCoupon", method = RequestMethod.POST)
    public String staffCouponCreate(Coupon coupon,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        SysUser user = sysUserService.selectByPK(Long.parseLong(userId));
        Company company=companyService.selectByCompanyStaff(user.getId());
        if (ObjectUtils.isEmpty(company)) {
            return this.renderJson(MsgOut.error("no data."));
        }
        coupon.setCompanyId(company.getCompanyId());
        couponService.insert(coupon);
        MsgOut o = MsgOut.success(coupon);
        return this.renderJson(o);
    }

    /**
     * @api {put} /api/wap/coupon 优惠券-修改
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/coupon
     *
     * @apiName update
     * @apiGroup coupon
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} goodsId 商品ID.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {byte} couponType 优惠券 类别.
     * @apiParam {long} couponTime 优惠券 时间.
     * @apiParam {int} percentage 优惠折扣.
     * @apiParam {int} reach1 满.
     * @apiParam {int} score1 返.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "percentage":0,
     *                   "companyId":0,
     *                   "couponType":0,
     *                   "reach1":0,
     *                   "score1":0,
     *                   "goodsId":0,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "userId":6
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/coupon"
     *  }
     */
    @RequestMapping(value = "/api/wap/coupon", method = RequestMethod.PUT)
    public String update(Coupon coupon,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        couponService.update(coupon);
        MsgOut o = MsgOut.success(coupon);
        return this.renderJson(o);
    }

    /**
     * @api {delete} /api/wap/coupon/{id} 优惠券-删除
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/coupon/{id}
     *
     * @apiName delete
     * @apiGroup coupon
     *
     * @apiParam {long} id 评论ID.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":1
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/coupon/{id}"
     *  }
     */
    @RequestMapping(value = "/api/wap/coupon/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id, @RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(couponService.delete(id));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/wap/coupon/{id} 优惠券-单个查询
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/coupon/{id}
     *
     * @apiName findCommentId
     * @apiGroup coupon
     *
     * @apiParam {long} id 评论ID.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "ctime":1502870415259,
     *                   "id":8,
     *                   "percentage":0,
     *                   "companyId":0,
     *                   "couponType":0,
     *                   "reach1":0,
     *                   "score1":0,
     *                   "goodsId":0,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "userId":6
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/coupon/{id}"
     *  }
     */
    @RequestMapping(value = "/api/wap/coupon/{id}", method = RequestMethod.GET)
    public String findCommentId(@PathVariable("id")Long  id) {
        MsgOut o = MsgOut.success(couponService.selectByPK(id));
        return this.renderJson(o);
    }

}
