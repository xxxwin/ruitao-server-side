package com.ruitaowang.core.domain;

/**
 * Created by xinchunting on 17-8-2.
 */
public class UserMembersVO extends UserMembers{

    private String phoneVO;
    private String memberTypeVO;

    public String getMemberTypeVO() {
        return memberTypeVO;
    }

    public void setMemberTypeVO(String memberTypeVO) {
        this.memberTypeVO = memberTypeVO;
    }

    public String getPhoneVO() {
        return phoneVO;
    }

    public void setPhoneVO(String phoneVO) {
        this.phoneVO = phoneVO;
    }
}
