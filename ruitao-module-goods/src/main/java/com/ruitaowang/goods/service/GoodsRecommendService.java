package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.GoodsRecommend;
import com.ruitaowang.core.domain.GoodsVO;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsAdMapper;
import com.ruitaowang.goods.dao.GoodsRecommendAdMapper;
import com.ruitaowang.goods.dao.GoodsRecommendMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsRecommendService implements BaseService<GoodsRecommend> {

    @Autowired
    private GoodsRecommendMapper goodsRecommendMapper;
    @Autowired
    private GoodsAdMapper goodsAdMapper;
    @Autowired
    private GoodsRecommendAdMapper goodsRecommendAdMapper;

    @Override
    public GoodsRecommend insert(GoodsRecommend goodsRecommend) {
        Long time = System.currentTimeMillis();
        goodsRecommend.setCtime(time);
        goodsRecommend.setMtime(time);
        goodsRecommendMapper.insert(goodsRecommend);
        return goodsRecommend;
    }

    @Override
    public int delete(Long id) {
        return goodsRecommendMapper.deleteByPK(id);
    }

    @Override
    public GoodsRecommend update(GoodsRecommend goodsRecommend) {
        Long time = System.currentTimeMillis();
        goodsRecommend.setMtime(time);
        goodsRecommendMapper.updateByPK(goodsRecommend);
        return goodsRecommend;
    }

    @Override
    public GoodsRecommend selectByPK(Long id) {
        return goodsRecommendMapper.selectByPK(id);
    }

    @Override
    public List<GoodsRecommend> select(GoodsRecommend goodsRecommend) {
        goodsRecommend.setRstatus((byte) 0);
        return goodsRecommendMapper.select(goodsRecommend);
    }

    public List<GoodsRecommend> selectForPage(GoodsRecommend goodsRecommend) {
        goodsRecommend.setRstatus((byte) 0);
        goodsRecommend.setRecords(goodsRecommendMapper.count(goodsRecommend));
        return goodsRecommendMapper.select(goodsRecommend);
    }

    /**
     * 根据推荐商品排序
     * @param goodsVO
     * @return
     */
    public List<GoodsVO> selectForSort(GoodsVO goodsVO) {
        goodsVO.setRstatus((byte) 0);
        goodsVO.setRecords(goodsAdMapper.selectForSortCount(goodsVO));
        return goodsAdMapper.selectForSort(goodsVO);
    }

    /**
     * 更新推荐商品
     * @param newGoodsList
     * @param companyId
     * @return
     */
    public List<GoodsRecommend> updateRecommendGoods(String[] newGoodsList,Long companyId){
        Long time = System.currentTimeMillis();
        List<GoodsRecommend> goodsRecommendList = new ArrayList<>();
        for(int i = 0;i < newGoodsList.length;i++){
            GoodsRecommend goodsRecommend = new GoodsRecommend();
            goodsRecommend.setGoodsId(Long.valueOf(newGoodsList[i]));
            goodsRecommend.setCompanyId(companyId);
            goodsRecommend.setCtime(time);
            goodsRecommend.setMtime(time);
            goodsRecommendList.add(goodsRecommend);
        }
        //goodsAdMapper.deleteForOld(oldGoodsList);
        goodsRecommendAdMapper.deleteByCompany(companyId);
        goodsRecommendAdMapper.insertNew(goodsRecommendList);
        return goodsRecommendList;
    }

    /**
     * 根据公司删除推荐上传
     * @param companyId
     * @return
     */
    public int deleteByCompany(Long companyId){
        return goodsRecommendAdMapper.deleteByCompany(companyId);
    }
}
