/**
 * Created by Administrator on 2017/3/23.
 */
var Config = {
    selectUrl: '/api/admin/order/profit'
};
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}

$(document).ready(function () {

    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        postData: {orderSn:getQueryString("id")},
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","订单编号","返利等级", "受益者编号", "受益者类型","返利类型", "受益者名称", "返利额度","订单时间"],
        colModel: [{
            name: "profitId",
            index: "profitId",
            autowidth: true,
            sorttype: "int",
            search: true
        },{
            name: "orderId",
            index: "orderId",
            autowidth: true,
            sorttype: "int",
            search: true
        },{
            name: "profitType",
            index: "profitType",
            editoptions:{value:"0:返现;1:一级;2:二级;3:一级奖励金;4:一级奖励金"},
            autowidth: true,
            edittype:'select',
            formatter:'select',
            sorttype: "int",
            search: true
        }, {
            name: "userId",
            index: "userId",
            autowidth: true
        },{
            //受益者类型
            name: "rstatus",
            index: "rstatus",
            autowidth: true,
            editoptions:{value:"0:云省-龙蛙会员;1:云转·微传媒会员;2:云点-扫码点餐会员;3:云联-跨界盈利;4:云促销-商家促销;5:云签-扫码签到;6:云飞-梦想;7:云推-城市;100:龙蛙-普通用户"},
            editable: true,
            edittype:'select',
            formatter:'select'
        }, {
            name: "orderType",
            index: "orderType",
            autowidth: true,
            editoptions:{value:"0:商城供货商订单返利;2:商城微商订单返利;4:商城微商订单返红包;7:快转（1年）;8:云省返利;9:云签返利;10:云飞返利;11:云推返利;12:云点返利;13:云联返利；14:云促销返利；15:快赚大礼包;16:云动返利;17:互动店霸屏奖励;18:互动店打赏奖励;18:互动店打赏奖励;19:互动店抢红包积分;20:区级返利;21:市级返利;22:省级返利;23:快转（1月）;24:快转（3月）;25:快转（6月）"},
            edittype:'select',
            formatter:'select'
        }, {
            name: "remark",
            index: "remark",
            autowidth: true
        }, {
            name: "userProfit",
            index: "userProfit",
            autowidth: true
        }, {
            name: "ctime",
            index: "ctime",
            autowidth: true
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            dataWrapper();
        }
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});
$('#buttonGoback').click(function () {
    history.go(-1);
    location.reload();
    return false;
});
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id);
        dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        if (Number(dataFromTheRow.orderType) === 4 || Number(dataFromTheRow.orderType) === 3) {
            jQuery("#data_list").jqGrid('setRowData', ids[i], {
                userProfit: toFixedForScore(dataFromTheRow.userProfit),
                ctime: dataFromTheRow.ctime
            });
        }else {
            jQuery("#data_list").jqGrid('setRowData', ids[i], {
                userProfit: toFixedForPrice(dataFromTheRow.userProfit),
                ctime: dataFromTheRow.ctime
            });
        }
    }
}