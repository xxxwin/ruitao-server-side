
var Config = {
    createUrl: '/api/dinner/tag',
    updateUrl: '/api/dinner/tag',
    deleteUrl: '/api/dinner/tag',
    selectUrl: '/api/dinner/tags/1'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
    //save role
    $('#submitForSave').click(function () {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
            {
                url: Config.updateUrl,
                type: "PUT",
                data: postData,
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        }
        return false;
    });

    $('#buttonGoback').click(function () {
        /*history.go(-1);
        location.reload();*/
    	document.location = "/web/dinner_tag.html";
        return false;
    })
});

//hello :)~