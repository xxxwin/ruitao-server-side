package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LsMsg;
import java.util.List;

public interface LsMsgAdMapper {

    List<LsMsg> select4New(LsMsg lsMsg);

    List<LsMsg> selectBySearch(LsMsg lsMsg);

    long countBySearch(LsMsg lsMsg);
}