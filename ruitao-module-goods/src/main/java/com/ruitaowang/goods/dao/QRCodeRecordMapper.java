package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.QRCodeRecord;
import java.util.List;

public interface QRCodeRecordMapper {
    int insert(QRCodeRecord qRCodeRecord);

    int deleteByPK(Long id);

    int updateByPK(QRCodeRecord qRCodeRecord);

    QRCodeRecord selectByPK(Long id);

    long count(QRCodeRecord qRCodeRecord);

    List<QRCodeRecord> select(QRCodeRecord qRCodeRecord);
}