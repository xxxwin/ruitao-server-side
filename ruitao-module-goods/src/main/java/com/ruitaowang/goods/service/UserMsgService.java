package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserMsg;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserMsgAdMapper;
import com.ruitaowang.goods.dao.UserMsgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMsgService implements BaseService<UserMsg> {

    @Autowired
    private UserMsgMapper userMsgMapper;
    @Autowired
    private UserMsgAdMapper userMsgAdMapper;

    @Override
    public UserMsg insert(UserMsg userMsg) {
        Long time = System.currentTimeMillis();
        userMsg.setCtime(time);
        userMsg.setMtime(time);
        userMsgMapper.insert(userMsg);
        return userMsg;
    }

    @Override
    public int delete(Long id) {
        UserMsg userMsg = new UserMsg();
        userMsg.setId(id);
        userMsg.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userMsg.setMtime(time);
        return userMsgMapper.updateByPK(userMsg);
    }

    @Override
    public UserMsg update(UserMsg userMsg) {
        Long time = System.currentTimeMillis();
        userMsg.setMtime(time);
        userMsgMapper.updateByPK(userMsg);
        return userMsg;
    }

    @Override
    public UserMsg selectByPK(Long id) {
        return userMsgMapper.selectByPK(id);
    }

    @Override
    public List<UserMsg> select(UserMsg userMsg) {
        userMsg.setRstatus((byte) 0);
        return userMsgMapper.select(userMsg);
    }

    public List<UserMsg> selectForPage(UserMsg userMsg) {
        userMsg.setRstatus((byte) 0);
        userMsg.setRecords(userMsgMapper.count(userMsg));
        return userMsgMapper.select(userMsg);
    }

    public List<UserMsg> userMsgList(UserMsg userMsg){
        userMsg.setRstatus((byte) 0);
        userMsg.setRecords(userMsgAdMapper.count(userMsg));
        return userMsgAdMapper.select(userMsg);
    }
}
