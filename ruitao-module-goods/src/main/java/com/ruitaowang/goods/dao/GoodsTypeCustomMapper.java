package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsTypeCustom;
import java.util.List;

public interface GoodsTypeCustomMapper {
    int insert(GoodsTypeCustom goodsTypeCustom);

    int deleteByPK(Long id);

    int updateByPK(GoodsTypeCustom goodsTypeCustom);

    GoodsTypeCustom selectByPK(Long id);

    long count(GoodsTypeCustom goodsTypeCustom);

    List<GoodsTypeCustom> select(GoodsTypeCustom goodsTypeCustom);
}