/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/roles',
    updateUrl: '/api/roles',
    deleteUrl: '/api/roles',
    selectUrl: '/api/roles'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    //save role
    $('#submitForSave').click(function () {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
            {
                url: Config.updateUrl,
                type: "PUT",
                data: postData,
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        }
        return false;
    });

    $('#buttonGoback').click(function () {
        history.go(-1);
        location.reload();
        return false;
    })
});

//hello :)~