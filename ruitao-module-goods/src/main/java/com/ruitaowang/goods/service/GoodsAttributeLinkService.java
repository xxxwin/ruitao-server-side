package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.GoodsAttributeLink;
import com.ruitaowang.core.domain.ShoppingCart;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsAttributeLinkAdMapper;
import com.ruitaowang.goods.dao.GoodsAttributeLinkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shaka on 2016/12/2.
 */
@Service
public class GoodsAttributeLinkService implements BaseService<GoodsAttributeLink> {


    @Autowired
    private GoodsAttributeLinkMapper goodsAttributeLinkMapper;
    @Autowired
    private GoodsAttributeLinkAdMapper goodsAttributeLinkAdMapper;


    @Override
    public GoodsAttributeLink insert(GoodsAttributeLink goodsAttributeLink) {
        Long time = System.currentTimeMillis();
        goodsAttributeLink.setCtime(time);
        goodsAttributeLink.setMtime(time);
        goodsAttributeLinkMapper.insert(goodsAttributeLink);

        return goodsAttributeLink;
    }


    public List<GoodsAttributeLink> insert(List<GoodsAttributeLink> goodsAttributeLinkList) {
        Long time = System.currentTimeMillis();

        for (GoodsAttributeLink goodsAttributeLink : goodsAttributeLinkList) {
            goodsAttributeLink.setCtime(time);
            goodsAttributeLink.setMtime(time);
            insert(goodsAttributeLink);
        }
        return goodsAttributeLinkList;
    }

    @Override
    public int delete(Long id) {
        GoodsAttributeLink goodsAttributeLink = new GoodsAttributeLink();
        goodsAttributeLink.setId(id);
        Long time = System.currentTimeMillis();
        goodsAttributeLink.setRstatus((byte) 1);
        goodsAttributeLink.setCtime(time);
        goodsAttributeLink.setMtime(time);

        return goodsAttributeLinkMapper.updateByPK(goodsAttributeLink);
    }

    @Override
    public GoodsAttributeLink update(GoodsAttributeLink goodsAttributeLink) {
        Long time = System.currentTimeMillis();
//        goodsAttributeLink.setRstatus((byte)1);
//        goodsAttributeLink.setCtime(time);
        goodsAttributeLink.setMtime(time);

        goodsAttributeLinkMapper.updateByPK(goodsAttributeLink);
        return goodsAttributeLink;
    }

    @Override
    public GoodsAttributeLink selectByPK(Long id) {
        return goodsAttributeLinkMapper.selectByPK(id);
    }

    @Override
    public List<GoodsAttributeLink> select(GoodsAttributeLink goodsAttributeLink) {
        goodsAttributeLink.setRstatus((byte) 0);
        return goodsAttributeLinkMapper.select(goodsAttributeLink);
    }

    public GoodsAttributeLink updateNoId(GoodsAttributeLink goodsAttributeLink) {
        Long time = System.currentTimeMillis();
        goodsAttributeLink.setMtime(time);
        goodsAttributeLinkAdMapper.update(goodsAttributeLink);
        return goodsAttributeLink;
    }

    public List<GoodsAttributeLink> selectAll(GoodsAttributeLink goodsAttributeLink) {
        return goodsAttributeLinkMapper.select(goodsAttributeLink);
    }

    public List<GoodsAttributeLink> selectBatch(String ids) {
        List<GoodsAttributeLink> goodsAttributeLinks = new ArrayList<>();
        if(StringUtils.isEmpty(ids)){
            return goodsAttributeLinks;
        }
        String[] attrLinks = ids.split(",");
        GoodsAttributeLink goodsAttributeLink = null;
        for (String s : attrLinks){
            goodsAttributeLink = goodsAttributeLinkMapper.selectByPK(Long.valueOf(s));
            if(goodsAttributeLink != null){
                goodsAttributeLinks.add(goodsAttributeLink);
            }
        }
        return goodsAttributeLinks;
    }
    public String getGoodsAttributeForString(List<GoodsAttributeLink> goodsAttributeLinks) {
        if(ObjectUtils.isEmpty(goodsAttributeLinks)){
            return "";
        }
        StringBuffer sb = new StringBuffer();
        for (GoodsAttributeLink s : goodsAttributeLinks){
            sb.append(s.getAttrName()).append(": ").append(s.getAttrValue()).append(" ");
        }
        return sb.toString();
    }

    public String getGoodsAttributeForString(String ids) {
        List<GoodsAttributeLink> goodsAttributeLinks = this.selectBatch(ids);
        return getGoodsAttributeForString(goodsAttributeLinks);
    }
    public int getGoodsAttributeExpraPrice(String ids) {
        List<GoodsAttributeLink> goodsAttributeLinks = this.selectBatch(ids);
        if (ObjectUtils.isEmpty(goodsAttributeLinks)){
            return 0;
        }
        int sum = 0;
        for (GoodsAttributeLink link  : goodsAttributeLinks){
            sum += link.getExtraPrice();
        }
        return sum;
    }
}
