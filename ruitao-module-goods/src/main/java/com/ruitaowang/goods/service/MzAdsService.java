package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.MzAds;
import com.ruitaowang.core.domain.Relations;
import com.ruitaowang.core.domain.RelationsVO;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.MzAdsMapper;
import com.ruitaowang.goods.dao.RelationsAdMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class MzAdsService implements BaseService<MzAds> {
    @Autowired
    private MzAdsMapper mzAdsMapper;
    @Autowired
    private RelationsAdMapper relationsAdMapper;

    @Override
    public MzAds insert(MzAds mzAds) {
        Long time = System.currentTimeMillis();
        mzAds.setCtime(time);
        mzAds.setMtime(time);
        mzAdsMapper.insert(mzAds);
        return mzAds;
    }

    @Override
    public int delete(Long id) {
        MzAds mzAds = new MzAds();
        mzAds.setAdId(id);
        mzAds.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        mzAds.setMtime(time);
        return mzAdsMapper.updateByPK(mzAds);
    }

    @Override
    public MzAds update(MzAds mzAds) {
        Long time = System.currentTimeMillis();
        mzAds.setMtime(time);
        mzAdsMapper.updateByPK(mzAds);
        return mzAds;
    }

    @Override
    public MzAds selectByPK(Long id) {

        return mzAdsMapper.selectByPK(id);
    }

    @Override
    public List<MzAds> select(MzAds mzAds) {
        mzAds.setRstatus((byte) 0);
        return mzAdsMapper.select(mzAds);
    }

    public List<MzAds> selectForPage(MzAds mzAds) {
        mzAds.setRstatus((byte) 0);
        mzAds.setRecords(mzAdsMapper.count(mzAds));
        return mzAdsMapper.select(mzAds);

    }
    //排行榜
    public List<RelationsVO> selectRanking(RelationsVO relationsVO) {
        return relationsAdMapper.selectRanking(relationsVO);

    }

    /**
     * 间推排行榜
     * @param relationsVO
     * @return
     */
    public List<RelationsVO> selectRankingTwo(RelationsVO relationsVO){
        return relationsAdMapper.selectTwoRanking(relationsVO);
    }
}

