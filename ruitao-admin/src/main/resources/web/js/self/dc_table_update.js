/**
 * 
 */

var Config = {
    createTableTypeUrl:'/api/admin/dc/tableType',
    selectTableTypeUrl:'/api/admin/dc/tableType',
    updateUrl: '/api/admin/dc/table',
    deleteUrl: '/api/admin/dc/table',
};
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {

    $.ajax(
        {
            url: "/api/configs/host/api",
            type: "GET",
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                var qrcodetable = new QRCode("qrcodetable", {
                    text: "" + data + "/wap/wx/login?fk=1-29-" + $("input[id=companyId]").val() + "-" + $("input[id=id]").val() + "",
                    width: 300,
                    height: 300,
                    colorDark : "#000000",
                    colorLight : "#ffffff",
                    correctLevel : QRCode.CorrectLevel.H
                });
                console.log(qrcodetable);
                $("#qrcodetable").attr("src",qrcodetable);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    $.ajax(
        {
            url: Config.selectTableTypeUrl,
            type: "GET",
            data: {companyId:$("input[id=companyId]").val(),page: 1, rows: 1000},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var dcTableTypes = [];
                dcTableTypes.push('<option value="" hassubinfo="true">选择分类</option>');
                data.data.map(function (dcTableType) {
                    dcTableTypes.push('<option value="');
                    dcTableTypes.push(dcTableType.id);
                    dcTableTypes.push('" ');
                    dcTableTypes.push('hassubinfo="true">');
                    dcTableTypes.push(dcTableType.name);
                    dcTableTypes.push('</option>');
                });
                $("#tableTypeId").html(dcTableTypes.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    $('#tableTypeId').val(tableTypeIdChecked);
    $('#submitForUpdate').click(function () {
        var companyId =$("input[name=companyId]").val();
        var id = $("input[name=id]").val();
        var name = $("input[name=name]").val();
        var remark = $("input[name=remark]").val();
        var tableTypeId = $("#tableTypeId").val();
        $.ajax(
            {url: Config.updateUrl,
                type: "PUT",
                data: {id:id,companyId:companyId,name:name,remark:remark,tableTypeId:tableTypeId},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        return false;
    });
});

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    
}