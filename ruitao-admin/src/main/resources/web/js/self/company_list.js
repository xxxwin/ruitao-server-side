/**
 * Created by Shaka on 12/21/16.
 */

var Config = {
    selectUrl: '/api/admin/company',
    deleteUrl: '/api/company',
    updateUrlForCellEdit: '/api/admin/company/cellEdit',
    selectIndustryUrl:'/api/admin/industry/selectAll',
    selectTypeUrl:'/api/admin/type/selectAll',
};
$("#industrySelect").change(function () {
    var industryId=$(this).val();
    $.ajax(
        {
            url: "/api/admin/type/selectByIndustry/"+industryId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="" hassubinfo="true">所有类型</option>');
                data.data.map(function (type) {
                    categories.push('<option value="');
                    categories.push(type.id);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(type.name);
                    categories.push('</option>');
                });
                $("#typeSelect").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
});
$(document).ready(function () {
    //给行业类型注入值
    $.ajax(
        {
            url: Config.selectIndustryUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="" hassubinfo="true">所有行业</option>');
                data.data.map(function (industry) {
                    categories.push('<option value="');
                    categories.push(industry.id);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(industry.name);
                    categories.push('</option>');
                });
                $("#industrySelect").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
        $.ajax(
            {
                url: Config.selectTypeUrl,
                type: "GET",
                dataType: "json",
                async: false,
                success: function (data, textStatus, jqXHR) {
                    var categories = [];
                    categories.push('<option value="" hassubinfo="true">所有类型</option>');
                    data.data.map(function (type) {
                        categories.push('<option value="');
                        categories.push(type.id);
                        categories.push('" ');
                        categories.push('hassubinfo="true">');
                        categories.push(type.name);
                        categories.push('</option>');
                    });
                    $("#typeSelect").html(categories.join(''));
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#company_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "商家店名", "联系人", "商家角色", "手机号码", "地址", "审核状态","缴纳押金", "推荐排序","时间", "操作"],
        colModel: [{
            name: "companyId",
            index: "companyId",
            width: 5,
            sorttype: "int",
            autowidth: true,
            search: true
        }, {
            name: "companyName",
            index: "companyName",
            width: 15,
            autowidth: true,
        }, {
            name: "linkman",
            index: "linkman",
            width: 10,
            autowidth: true,
        }, {
            name: "companyType",
            index: "companyType",
            width: 10,
            autowidth: true,
            sortable: true,
            editoptions: {value: "0:实体商家;1:微商;2:心理咨询;3:城市合伙人;4:梦想合伙人;5:教育培训;6:联合创始人;7:艺术家;8:供货商;9:产业合伙人;10:点餐商家;11:互动商家(至尊版);12:区域代理商;13:互动商家(基础版);14:合伙人;15:创始股东"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "mobile",
            index: "mobile",
            width: 10,
            autowidth: true,
            sorttype: "int"
        }, {
            name: "address",
            index: "address",
            width: 10,
            autowidth: true,
            sorttype: "int"
        }, {
            name: "companyStatus",
            index: "companyStatus",
            width: 10,
            autowidth: true,
            editable: true,
            sortable: true,
            editoptions: {value: "0:未审核待处理;1:已审核;2:审核未通过"},
            edittype: 'select',
            formatter: 'select'
        },  {
            name: "depositStatus",
            index: "depositStatus",
            width: 10,
            autowidth: true,
            editable: true,
            sortable: true,
            editoptions: {value: "0:未缴纳;1:已缴纳"},
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "rangeSort",
            index: "rangeSort",
            width: 5,
            sorttype: "int",
            autowidth: true,
            editable: true,
            sortable: true
        }, {
            name: "ctime",
            index: "ctime",
            width: 10
        }, {
            name: "operate",
            index: "operate",
            width: 10,
            autowidth: true,
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: '/api/admin/company/cellEdit',
        pager: "#pager_company_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "companyId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#company_list").jqGrid('navGrid', '#pager_company_list', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#company_list").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#company_list").jqGrid('delRowData', id);
}
$("#perform_search").click(function () {
    var postdata = $("#company_list").jqGrid('getGridParam', 'postData');
    postdata._search = false;
    postdata.companyType = $('#companyType option:selected').val();
    postdata.companyStatus = $('#companyStatus option:selected').val();
    postdata.companyName = $('#companyName').val();
    postdata.companyId = $('#industrySelect').val();
    postdata.companyTypeId = $('#typeSelect').val();
    jQuery("#company_list").trigger("reloadGrid", [{page: 1}]);
});
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#company_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#company_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#company_list').jqGrid ('getRowData', id);
        dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/company/update/' + id + '" data-index="0">详细</a>';
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/companyMembers/update/' + id + '" data-index="0">会员优惠</a>';
        jQuery("#company_list").jqGrid('setRowData', ids[i], {operate: html,ctime:dataFromTheRow.ctime});
    }
}
