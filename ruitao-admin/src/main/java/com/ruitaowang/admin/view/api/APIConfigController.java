package com.ruitaowang.admin.view.api;

import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by neal on 2017/07/12.
 */
@RestController
public class APIConfigController extends BaseController {

    @Value("${host.api}")
    private String hostAPI;

    @Autowired
    private CompanyService companyService;

    @RequestMapping("/api/configs/host/api")
    public String getHostAPI() {
        return this.renderJson(hostAPI);
    }

    @RequestMapping("/api/transactions/rollback")
    public String rollback() {
        companyService.rollback();
        return this.renderJson("success");
    }

    @RequestMapping("/api/transactions/norollback")
    public String norollback() {
        companyService.norollback();
        return this.renderJson("success");
    }
}
