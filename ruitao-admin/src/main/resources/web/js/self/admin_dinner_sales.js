/**
 * Created by xinchunting on 17-9-25.
 */
$(".datepicker").datepicker({
    todayBtn: "linked",
    clearBtn: true,
    autoclose: true,
    language: "zh-CN"
});

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url:"/api/admin/dc/promotions/get",
        height: 300,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["活动编号", "活动名称","活动类型", "开始时间", "结束时间", "备注", "时间","操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: true
        }, {
            name: "name",
            index: "name"
        }, {
            name: "type",
            index: "type",
            sortable: true,
            editoptions: {value: "0:回馈活动;1:支付优惠活动"},
            edittype: 'select',
            formatter: 'select'
        },{
            name: "startTime",
            index: "startTime",
            sorttype: "int"
        }, {
            name: "endTime",
            index: "endTime",
            sorttype: "int"
        }, {
            name: "remark",
            index: "remark"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        pager: "#pager_dc_sales",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
            console.log("1");
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_dc_sales', {add: false, edit: false, del: false,search: false,
        refresh: false});
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});
function dataWrapper() {
    console.log("2");
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id);
        dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        if (dataFromTheRow.startTime){
            dataFromTheRow.startTime=new Date(dataFromTheRow.startTime*1).yyyymmddhhmmss();
        }else {
            dataFromTheRow.startTime="";
        }
        if (dataFromTheRow.endTime){
            dataFromTheRow.endTime=new Date(dataFromTheRow.endTime*1).yyyymmddhhmmss();
        }else {
            dataFromTheRow.endTime="";
        }
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" data-index="0">删除</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html,ctime: dataFromTheRow.ctime,startTime: dataFromTheRow.startTime,endTime: dataFromTheRow.endTime});
    }
}
$("#perform_search").click(function () {
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.stime = dateToEpoch($("#beginDate").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    postdata.etime = dateToEpoch($("#endDate").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    jQuery("#data_list").trigger("reloadGrid", postdata);
});

$(".feedbackRelease").click(function () {
    var startTime=dateToEpoch($("#startTime").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    var endTime=dateToEpoch($("#endTime").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    $.ajax(
        {
            url:"/api/admin/dc/promotions/create",
            type: "POST",
            data:{name:$("#name").val(),content:$("#content").val(),startTime:startTime,endTime:endTime,type:0},
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                toastr.success(data.msg, data.title);
                jQuery("#data_list").trigger("reloadGrid");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        })
});

$(".discountRelease").click(function () {
    $.ajax(
        {
            url:"/api/admin/dc/promotions/create",
            type: "POST",
            data:{content:$("#discountContent").val(),type:1},
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                toastr.success(data.msg, data.title);
                jQuery("#data_list").trigger("reloadGrid");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        })
});