/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/meetings',
    updateUrl: '/api/meetings',
    deleteUrl: '/api/meetings',
    selectUrl: '/api/meetings'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
var ue = UE.getEditor('editor');
ue.ready(function() {
    ue.execCommand('serverparam', {
        'isActivity': 1
    });
});
$().ready(function () {

    $('#submitCreate').click( function() {
        $("#ajaxform").validate();
        $("#meetingContent").val(ue.getContent());
        $("#meetingPrice").val($("#meetingPrice").val()*100);

        if ($('#ajaxform').valid()){
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.createUrl,
                    type: "POST",
                    data : postData,
                    dataType: "json",
                    async: false,
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});
