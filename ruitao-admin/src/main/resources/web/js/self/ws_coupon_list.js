/**
 * Created by Shaka on 12/21/16.
 */
var Config = {
     selectUrl: '/api/ws/coupon/page',
     deleteUrl: '/api/ws/coupon'
};
$(document).ready(function () {
    $.ajax(
        {
            url:'/api/admin/companyInfo',
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data) {
                var comapnyes = [];
                // comapnyes.push('<option value="0" hassubinfo="true">顶级分类</option>');
                data.data.map(function (comapny) {
                    comapnyes.push('<option value="');
                    comapnyes.push(comapny.companyId);
                    comapnyes.push('" ');
                    comapnyes.push('hassubinfo="true">');
                    comapnyes.push(comapny.companyName);
                    comapnyes.push('</option>');
                    //console.log(comapny);
                });
                //console.log(comapnyes);
                $("#companyId").html(comapnyes.join(''));
                //console.log($("#companyId").val());
                //console.log($("#companyId option:selected").val());
                if (!data.data){
                    JqGrid(0)
                }else {
                    JqGrid($("#companyId").val())
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        }
    );
    $("#companyId").change(function () {
        //console.log($("#companyId option:selected").val());
        var postdata = $("#coupon_list").jqGrid('getGridParam', 'postData');
        postdata._search = false;
        postdata.companyId=$("#companyId option:selected").val();
        jQuery("#coupon_list").trigger("reloadGrid", [{page: 1}]);
    })
});

function JqGrid(companyId){
    console.log(companyId);
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#coupon_list").jqGrid({
        datatype: "json",
        postData:{companyId:companyId},
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","公司名称", "商品名称", "优惠券类型", "开始时间","结束时间", "满金额", "减金额", "价格(元)", "创建时间", "备注","操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: true
        }, {
            name: "companyName",
            index: "companyName"
        }, {
            name: "goodsName",
            index: "goodsName"
        }, {
            name: "couponType",
            index: "couponType"
        }, {
            name:"couponTime",
            index: "couponTime"
        }, {
            name:"couponEndtime",
            index: "couponEndtime"
        }, {
            name:"reach1",
            index: "reach1"
        }, {
            name:"score1",
            index: "score1"
        },{
            name: "price",
            index: "price"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "remark",
            index: "remark"
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        // cellEdit: true,
        // cellsubmit: 'remote',
        // cellurl: Config.updateUrlForCellEdit,
        pager: "#pager_coupon_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#coupon_list").jqGrid('navGrid', '#pager_coupon_list', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#coupon_list").setGridWidth(width);
    });
}

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#coupon_list").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#coupon_list").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#coupon_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#coupon_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        dataFromTheRow.couponTime = new Date(dataFromTheRow.couponTime * 1).yyyymmddhhmmss();
        dataFromTheRow.couponEndtime = new Date(dataFromTheRow.couponEndtime * 1).yyyymmddhhmmss();
        dataFromTheRow.price = dataFromTheRow.price / 100;
        dataFromTheRow.reach1 = dataFromTheRow.reach1 / 100;
        dataFromTheRow.score1 = dataFromTheRow.score1  / 100;
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        var couponType = "";
        switch (Number(dataFromTheRow.couponType)){
            case 0:
                couponType = "满减券";
                break;
            case 2:
                couponType = "立减券";
                break;
        }
         html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/ws/coupon/update/' + id + '" data-index="0">修改</a>';
        jQuery("#coupon_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
            couponTime: dataFromTheRow.couponTime,
            couponEndtime: dataFromTheRow.couponEndtime,
            couponType : couponType,
            price: toFixedForPrice(dataFromTheRow.price),
            reach1: toFixedForPrice(dataFromTheRow.reach1),
            score1: toFixedForPrice(dataFromTheRow.score1)
        });
    }
}

