
var Config = {
    updateUrl: '/api/admin/industry/update',
    // updateUrl: '/api/activities',
    // deleteUrl: '/api/activities',
    // selectUrl: '/api/activities'
};
//表单验证
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$.extend($.validator.messages,{
    max: $.validator.format("请输入介于 {0}到{1}之间的数值！",0,100),
    min: $.validator.format("请输入介于 {0}到{1}之间的数值！",0,100),
    number:$.validator.format("请输入有效的数字!"),
});

$().ready(function () {
    // $('#activityType').val(activityTypeChecked);
    $('#submitIndustryCreate').click(function () {

        $("#ajaxform").validate();
        //如果验证通过
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            console.log(postData);
            $.ajax(
                {
                    url: Config.updateUrl,
                    type: "PUT",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});

$('#buttonGoback').click(function() {
    history.go(-1);
    location.reload();
    return false;
});