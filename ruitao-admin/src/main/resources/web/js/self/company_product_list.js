/**
 * Created by neal on 11/22/16.
 */

var Config = {
    deleteUrl: '/api/companyProduct',
    selectDiscountUrl: '/api/localDiscount',
    selectUrl: '/api/companyProduct'
};


$(document).ready(function () {

    $.jgrid.defaults.styleUI = "Bootstrap";
    var discount;
    $.ajax(
        {
            url: Config.selectDiscountUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                discount = data.data;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
    $.ajax(
        {
            url: Config.selectUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data) {

                //add operate for data
                if (data && data.data && data.data.length > 0) {
                    for (var i = 0; i < data.data.length; i++) {
                        var id = data.data[i].productId;
                        data.data[i].id = data.data[i].productId;
                        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/companyProduct/update/' + id + '" data-index="0">编辑</a>';
                        // html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + "," + (i + 1) + ")' >删除</a>";
                        data.data[i].operate = html;
                        if(data.data[i].discountType == 1){
                            data.data[i].reach1 = "订单金额的 "+data.data[i].percentage + "%";
                            data.data[i].discountType = "百分比";
                        }else if(data.data[i].discountType == 0){
                            data.data[i].discountType = "满送";
                            data.data[i].reach1 = "1，满 ¥"+data.data[i].reach1 + " 送 "+ data.data[i].score1 + "红包；" +
                                "2，满 ¥"+data.data[i].reach2 + " 送 "+ data.data[i].score2 + "红包；" +
                                "3，满 ¥"+data.data[i].reach3 + " 送 "+ data.data[i].score3 + "红包；" +
                                "4，满 ¥"+data.data[i].reach4 + " 送 "+ data.data[i].score4 + "红包；" +
                                "5，满 ¥"+data.data[i].reach5 + " 送 "+ data.data[i].score5 + "红包";
                        }

                        data.data[i].qrcodeUrl = '<a target="_blank" href="'+data.data[i].qrcodeUrl+'">下载</a>';
                    }
                }

                $("#data_list").jqGrid({
                    datatype: "local",
                    data: data.data,
                    height: 450,
                    autowidth: true,
                    shrinkToFit: true,
                    rowNum: 12,
                    colNames: ["编号", "折扣类型", "红包情况", "备注", "操作"],
                    colModel: [{
                        name: "productId",
                        index: "productId",
                        width: 10
                    }, {
                        name: "discountType",
                        index: "discountType",
                        width: 15
                    }, {
                        name: "reach1",
                        index: "reach1",
                        width: 100
                    }, {
                        name: "remark",
                        index: "remark",
                        width: 20
                    }, {
                        name: "operate",
                        index: "operate",
                        width: 15,
                        sortable: false
                    }
                    ],
                    pager: "#pager_data_list",
                    viewrecords: true,
                    hidegrid: false,
                    gridComplete: function () {
                    }
                });
                jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {
                    add: false,
                    edit: false,
                    del: false
                });
                $(window).bind("resize", function () {
                    var width = $(".jqGrid_wrapper").width();
                    $("#data_list").setGridWidth(width);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });

});


function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}

function getDiscount(id, data) {
    console.log(data);
    console.log(data.length);
    for (var i = 0; i < data.length; i++) {
        console.log(data[i].discountId == id);
        if(data[i].discountId == id){
            return data[i];
        }
    }
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#data_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}