package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.UserCommunication;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.UserCommunicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xinchunting on 17-10-30.
 */
@RestController
public class APIUserCommunicationController extends BaseController {
    @Autowired
    private UserCommunicationService userCommunicationService;

    @RequestMapping("/api/admin/userCommunication/get")
    public String findCommunicationPage(UserCommunication userCommunication) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        userCommunication.setCompanyId(1L);
        MsgOut o = MsgOut.success(userCommunicationService.select(userCommunication));
        return this.renderJson(o);
    }
}
