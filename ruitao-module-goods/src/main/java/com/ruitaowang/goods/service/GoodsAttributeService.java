package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.GoodsAttribute;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsAttributeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class GoodsAttributeService implements BaseService<GoodsAttribute>{

    //// TODO: 2016/12/2 商品属性service,照抄未做审核

    @Autowired
    private GoodsAttributeMapper goodsAttributeMapper;

    @Override
    public GoodsAttribute insert(GoodsAttribute goodsAttribute) {

        Long time = System.currentTimeMillis();
        goodsAttribute.setCtime(time);
        goodsAttribute.setMtime(time);
        goodsAttributeMapper.insert(goodsAttribute);
        return goodsAttribute;
    }

    @Override
    public int delete(Long id) {
        GoodsAttribute goodsAttribute=new GoodsAttribute();
        goodsAttribute.setAttrId(id);
        goodsAttribute.setRstatus((byte)1);
        Long time = System.currentTimeMillis();
        goodsAttribute.setMtime(time);
        return goodsAttributeMapper.updateByPK(goodsAttribute);
    }

    @Override
    public GoodsAttribute update(GoodsAttribute goodsAttribute) {
        Long time = System.currentTimeMillis();
        goodsAttribute.setMtime(time);
        goodsAttributeMapper.updateByPK(goodsAttribute);
        return goodsAttribute;
    }

    @Override
    public GoodsAttribute selectByPK(Long id) {
        return goodsAttributeMapper.selectByPK(id);
    }

    @Override
    public List<GoodsAttribute> select(GoodsAttribute goodsAttribute)
    {
        goodsAttribute.setRstatus((byte) 0);
        return goodsAttributeMapper.select(goodsAttribute);
    }
}
