import com.ruitaowang.admin.Application;
import com.ruitaowang.admin.async.AsyncBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

/**
 * Created by neal on 12/09/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class AsyncTest {

    @Autowired
    private AsyncBean asyncBean;

    @Test
    public void testSync() throws Exception {
        asyncBean.syncVoid(1);
        asyncBean.syncVoid(2);
        asyncBean.syncVoid(3);
    }

    @Test
    public void testAsyncVoid() throws Exception {
        asyncBean.asyncVoid(1);
        asyncBean.asyncVoid(2);
        asyncBean.asyncVoid(3);
    }

    @Test
    public void testAsyncReturn() throws Exception {
        System.out.println(asyncBean.asyncReturn(1));
        System.out.println(asyncBean.asyncReturn(2));
        System.out.println(asyncBean.asyncReturn(3));
    }

    @Test
    public void testAsyncReturnFuture() throws Exception {
        long start = System.currentTimeMillis();
        Future<String> future1 = asyncBean.asyncReturnFuture(1);
        Future<String> future2 = asyncBean.asyncReturnFuture(2);
        Future<String> future3 = asyncBean.asyncReturnFuture(3);
        System.out.println("Elapsed Time : " + (System.currentTimeMillis() - start));
        System.out.println(future2.get());
        System.out.println(future1.get());
        System.out.println(future3.get());
        System.out.println("Elapsed Time : " + (System.currentTimeMillis() - start));
    }
    @Test
    public void testAsyncReturnCompleteFuture() throws Exception {
        long start = System.currentTimeMillis();
        CompletableFuture<String> future1 = asyncBean.asyncReturnCompletableFuture(1);
        CompletableFuture<String> future2 = asyncBean.asyncReturnCompletableFuture(2);
        CompletableFuture<String> future3 = asyncBean.asyncReturnCompletableFuture(3);
        CompletableFuture.allOf(future1, future2, future3).join();
        System.out.println("Elapsed Time : " + (System.currentTimeMillis() - start));
        System.out.println(future2.get());
        System.out.println(future1.get());
        System.out.println(future3.get());
        System.out.println("Elapsed Time : " + (System.currentTimeMillis() - start));
    }
}