package com.ruitaowang.core.domain;

import java.util.List;

public class ShoppingOrderVO extends ShoppingOrder{

    private Long userId;

    private String linkman;

    private List<OrderProd> goodsList;

    private List<OrderProdVO> goodsListVO;

    private String companyPhone;

    private Integer totalPrice;

    private Integer goodsNumber;

    private Integer memberPrice;

    private String IndustryCategory;

    private String address;

    private String couponPrice;

    private String orderAmount;

    private Long todayMoeny;

    private String consignee;

    private String headPhoto;

    private String orderSn;

    private String mobile;

    private String shippingAddress;

    private Long payTime;

    private String defaultLogistics;

    public Long getPayTime() {
        return payTime;
    }

    public void setPayTime(Long payTime) {
        this.payTime = payTime;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    public Long getTodayMoeny() {
        return todayMoeny;
    }

    public void setTodayMoeny(Long todayMoeny) {
        this.todayMoeny = todayMoeny;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public List<OrderProdVO> getGoodsListVO() {
        return goodsListVO;
    }

    public void setGoodsListVO(List<OrderProdVO> goodsListVO) {
        this.goodsListVO = goodsListVO;
    }

    public String getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(String couponPrice) {
        this.couponPrice = couponPrice;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIndustryCategory() {
        return IndustryCategory;
    }

    public void setIndustryCategory(String industryCategory) {
        IndustryCategory = industryCategory;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public Integer getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(Integer memberPrice) {
        this.memberPrice = memberPrice;
    }

    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<OrderProd> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<OrderProd> goodsList) {
        this.goodsList = goodsList;
    }

    public String getDefaultLogistics() {
        return defaultLogistics;
    }

    public void setDefaultLogistics(String defaultLogistics) {
        this.defaultLogistics = defaultLogistics;
    }
}
