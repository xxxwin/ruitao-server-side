package com.ruitaowang.core.domain;

public class Collection extends Page {
    /**
     *  ,所属表字段为collection.id
     */
    private Long id;

    /**
     *  用户ID,所属表字段为collection.user_id
     */
    private Long userId;

    /**
     *  商家ID,所属表字段为collection.company_id
     */
    private Long companyId;

    /**
     *  优惠券ID,所属表字段为collection.coupon_id
     */
    private Long couponId;

    /**
     *  商品ID,所属表字段为collection.goods_id
     */
    private Long goodsId;

    /**
     *  ,所属表字段为collection.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为collection.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为collection.remark
     */
    private String remark;

    /**
     *  ,所属表字段为collection.rstatus
     */
    private Byte rstatus;

    /**
     *  收藏分类
1:商品
2:商家
3:优惠券,所属表字段为collection.collection_type
     */
    private Byte collectionType;

    /**
     *  ,所属表字段为collection.price
     */
    private Integer price;


    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Byte getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(Byte collectionType) {
        this.collectionType = collectionType;
    }
}