package com.ruitaowang.core.domain;

public class GoodsRecommend extends Page {
    /**
     *  ,所属表字段为goods_recommend.id
     */
    private Long id;

    /**
     *  商家id,所属表字段为goods_recommend.company_id
     */
    private Long companyId;

    /**
     *  商品id,所属表字段为goods_recommend.goods_id
     */
    private Long goodsId;

    /**
     *  备注,所属表字段为goods_recommend.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为goods_recommend.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为goods_recommend.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为goods_recommend.rstatus
     */
    private Byte rstatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}