package com.ruitaowang.core.utils;

import java.text.DecimalFormat;

public class PriceUtile {

    public static String toFixedForPrice(int num){
        Double d=(double)num/100;
        String s=new DecimalFormat("#########0.00").format(d);
        return s;
    }

    public static String toFixedForPrice(Double num){
        Double d=num/100;
        String s=new DecimalFormat("#########0.00").format(d);
        return s;
    }
}
