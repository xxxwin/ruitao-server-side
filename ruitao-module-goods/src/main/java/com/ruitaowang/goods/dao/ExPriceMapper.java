package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ExPrice;
import java.util.List;

public interface ExPriceMapper {
    int insert(ExPrice exPrice);

    int deleteByPK(Long expriceId);

    int updateByPK(ExPrice exPrice);

    ExPrice selectByPK(Long expriceId);

    long count(ExPrice exPrice);

    List<ExPrice> select(ExPrice exPrice);
}