package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.PcUser;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.PcUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PcUserService implements BaseService<PcUser> {
    @Autowired
    private PcUserMapper pcUserMapper;


    @Override
    public PcUser insert(PcUser pcUser) {
        Long time = System.currentTimeMillis();
        pcUser.setCtime(time);
        pcUser.setMtime(time);
        pcUserMapper.insert(pcUser);
        return pcUser;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public PcUser update(PcUser pcUser) {
        pcUserMapper.updateByPK(pcUser);
        return pcUser;
    }

    @Override
    public PcUser selectByPK(Long id) {
        return pcUserMapper.selectByPK(id);
    }

    @Override
    public List<PcUser> select(PcUser pcUser) {
        pcUser.setRstatus((byte) 0);
        return pcUserMapper.select(pcUser);
    }
}
