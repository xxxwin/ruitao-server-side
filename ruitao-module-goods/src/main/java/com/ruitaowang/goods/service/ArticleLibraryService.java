package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.ArticleLibrary;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.ArticleLibraryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class ArticleLibraryService implements BaseService<ArticleLibrary> {
    @Autowired
    private ArticleLibraryMapper articleLibraryMapper;

    @Override
    public ArticleLibrary insert(ArticleLibrary articleLibrary) {
        Long time = System.currentTimeMillis();
        articleLibrary.setCtime(time);
        articleLibrary.setMtime(time);
        articleLibraryMapper.insert(articleLibrary);
        return articleLibrary;
    }

    @Override
    public int delete(Long id) {
        ArticleLibrary articleLibrary = new ArticleLibrary();
        articleLibrary.setId(id);
        articleLibrary.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        articleLibrary.setMtime(time);
        return articleLibraryMapper.updateByPK(articleLibrary);
    }

    @Override
    public ArticleLibrary update(ArticleLibrary articleLibrary) {
        Long time = System.currentTimeMillis();
        articleLibrary.setMtime(time);
        articleLibraryMapper.updateByPK(articleLibrary);
        return articleLibrary;
    }

    @Override
    public ArticleLibrary selectByPK(Long id) {
        return articleLibraryMapper.selectByPK(id);
    }

    @Override
    public List<ArticleLibrary> select(ArticleLibrary articleLibrary) {
        articleLibrary.setRstatus((byte) 0);
        return articleLibraryMapper.select(articleLibrary);
    }

    public List<ArticleLibrary> selectForPage(ArticleLibrary articleLibrary) {
        articleLibrary.setRstatus((byte) 0);
        articleLibrary.setRecords(articleLibraryMapper.count(articleLibrary));
        return articleLibraryMapper.select(articleLibrary);
    }
    public ArticleLibrary selectUrl(String articleUrl) {
        return articleLibraryMapper.selectUrl(articleUrl);
    }
}

