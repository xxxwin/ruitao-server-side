package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.RebateRatio;

import java.util.List;

public interface RebateRatioMapper {
    int insert(RebateRatio rebateRatio);

    int deleteByPK(Long id);

    int updateByPK(RebateRatio rebateRatio);

    RebateRatio selectByPK(Long id);

    long count(RebateRatio rebateRatio);

    List<RebateRatio> select(RebateRatio rebateRatio);
}