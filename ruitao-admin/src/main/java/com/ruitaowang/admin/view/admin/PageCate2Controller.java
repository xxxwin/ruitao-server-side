package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Activity;
import com.ruitaowang.core.domain.ActivityVo;
import com.ruitaowang.core.domain.Category;
import com.ruitaowang.goods.service.CategoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;
import java.util.List;

/**
 * 二级商品分类
 */
@RestController
public class PageCate2Controller {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = {"/cateList2/{cateId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView cateList2(@PathVariable("cateId") Long cateId, Model model) {
        Category category = categoryService.selectByPK(cateId);
        Category category1 = new Category();
        category1.setParentId(category.getId());
        List<Category> categoryList = categoryService.select(category1);
        model.addAttribute("categoryList", categoryList);
        return new ModelAndView("web/cate_list2");
    }

    @RequestMapping(value = {"/cateList3/{cateId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView cateList3(@PathVariable("cateId") Long cateId, Model model) {
        Category category = categoryService.selectByPK(cateId);
        Category category1 = new Category();
        category1.setParentId(category.getId());
        List<Category> categoryList = categoryService.select(category1);
        model.addAttribute("categoryList", categoryList);
        return new ModelAndView("web/cate_list3");
    }

    @RequestMapping(value = {"/cate/update2/{cateId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update2(@PathVariable("cateId") Long cateId, Model model) {
        Category category = categoryService.selectByPK(cateId);
        model.addAttribute("category", category);
        return new ModelAndView("web/cate_update1");
    }


}
