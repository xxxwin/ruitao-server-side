/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/roles',
    updateUrl: '/api/roles',
    deleteUrl: '/api/roles',
    selectUrl: '/api/roles'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax(
        {
            url: Config.selectUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                dataWrapper(data);
                $("#data_list").jqGrid({
                    datatype: "local",
                    data: data.data,
                    height: 450,
                    autowidth: true,
                    rowNum: 1000,
                    colNames: ["编号", "角色名称", "描述", "操作"],
                    colModel: [{
                        name: "id",
                        index: "id",
                        editable: false,
                        width: 20,
                        sorttype: "int",
                        search: true
                    }, {
                        name: "name",
                        index: "name",
                        editable: true,
                        width: 80
                    }, {
                        name: "remark",
                        index: "remark",
                        editable: true,
                        width: 80
                    }, {
                        name: "operate",
                        index: "operate",
                        width: 40,
                        sortable: false
                    }],
                    pager: "#pager_data_list",
                    viewrecords: true,
                    hidegrid: false,
                    gridComplete: function () {}
                });
                jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
                $(window).bind("resize", function () {
                    var width = $(".jqGrid_wrapper").width();
                    $("#data_list").setGridWidth(width);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown);
            }
        });
});

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
    {
        url: url,
        type: action,
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (action == 'DELETE') {
                if (data && data.type == "SUCCESS"){
                    jQuery("#data_list").jqGrid('delRowData', id);
                    jQuery("#data_list").trigger("reloadGrid");
                }else{
                    toastr.error(data.msg);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("该角色下有关联的权限，请先删除.");
        }
    });
}
function dataWrapper(data) {
    //add operate for data
    if(data && data.data && data.data.length>0){
        for (var i=0;i<data.data.length;i++){
            var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/permission_role_update/'+data.data[i].id+'" data-index="0">编辑</a>';
            html += "<a href='#'   style='color:#f60' onclick='Delete(" + data.data[i].id +")' >删除</a>";
            data.data[i].operate = html;
        }
    }
}

//hello :)~