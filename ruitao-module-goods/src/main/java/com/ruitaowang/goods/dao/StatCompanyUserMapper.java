package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.StatCompanyUser;
import java.util.List;

public interface StatCompanyUserMapper {
    int insert(StatCompanyUser statCompanyUser);

    int deleteByPK(Long id);

    int updateByPK(StatCompanyUser statCompanyUser);

    StatCompanyUser selectByPK(Long id);

    long count(StatCompanyUser statCompanyUser);

    List<StatCompanyUser> select(StatCompanyUser statCompanyUser);
}