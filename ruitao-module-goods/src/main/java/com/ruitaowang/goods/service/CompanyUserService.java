package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.CompanyUser;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CompanyUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyUserService implements BaseService<CompanyUser> {

    @Autowired
    private CompanyUserMapper companyUserMapper;

    @Override
    public CompanyUser insert(CompanyUser companyUser) {
        Long time = System.currentTimeMillis();
        companyUser.setCtime(time);
        companyUser.setMtime(time);
        companyUserMapper.insert(companyUser);
        return companyUser;
    }

    @Override
    public int delete(Long id) {
        CompanyUser companyUser = new CompanyUser();
        companyUser.setId(id);
        companyUser.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        companyUser.setMtime(time);
        return companyUserMapper.updateByPK(companyUser);
    }

    @Override
    public CompanyUser update(CompanyUser companyUser) {
        Long time = System.currentTimeMillis();
        companyUser.setMtime(time);
        companyUserMapper.updateByPK(companyUser);
        return companyUser;
    }

    @Override
    public CompanyUser selectByPK(Long id) {
        return companyUserMapper.selectByPK(id);
    }

    @Override
    public List<CompanyUser> select(CompanyUser companyUser) {
        companyUser.setRstatus((byte) 0);
        return companyUserMapper.select(companyUser);
    }

    public List<CompanyUser> selectForPage(CompanyUser companyUser) {
        companyUser.setRstatus((byte) 0);
        companyUser.setRecords(companyUserMapper.count(companyUser));
        return companyUserMapper.select(companyUser);
    }
}
