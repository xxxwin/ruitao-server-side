var Config = {
    updateUrl: '/api/admin/comment/update',
    getCompanyNameUrl:'/api/admin/company/selectByPk/',
    getGoodsNameUrl:'/api/goods/',
    getUserNameUrl:'/api/admin/accunts/selectByPk/'
};
$().ready(function () {

})

$(window).load(function () {
    // $('#activityType').val(activityTypeChecked);
    // var commentId=$("#comment_id").val();
    var imgs = $("#imgContent").val();
    var avatar = '';
    if(imgs.indexOf('#')>0) {
        var imglist = imgs.split('#');
        if (imglist.length > 0) {
            var count = imglist.length;
            console.log(imglist);
            for (var i = 0; i < count; i++) {
                avatar += '<img style=" height: 150px;" src="'+imglist[i]+'"/>';
            }
        }
        $("#imgs").html(avatar);
    }else{
        avatar += '<img style=" height: 150px;" src="'+imgs+'"/>';
        $("#imgs").html(avatar);
    }
    $("#commentContent").html($("#commentText").val());
    //给商家名注入值
    var companyId= $("#companyId").val();
    $.ajax(
        {
            url: Config.getCompanyNameUrl+companyId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                $("#companyName").val(data.data.companyName);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });

    //给商品名注入值
    var goodsId= $("#goodsId").val();
    $.ajax(
        {
            url: Config.getGoodsNameUrl+goodsId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                $("#goodsName").val(data.data.goodsName);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    //给商家名注入值
    var userId= $("#userId").val();
    $.ajax(
        {
            url: Config.getUserNameUrl+userId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                $("#username").val(data.data.nickname);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
});

$('#buttonGoback').click(function() {
    history.go(-1);
    location.reload();
    return false;
});