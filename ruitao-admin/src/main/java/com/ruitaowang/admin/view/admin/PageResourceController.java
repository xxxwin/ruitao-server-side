package com.ruitaowang.admin.view.admin;

import com.ruitaowang.account.service.ResourceService;
import com.ruitaowang.account.service.RoleService;
import com.ruitaowang.core.domain.CategoryRecommend;
import com.ruitaowang.core.domain.GoodsCategory;
import com.ruitaowang.core.domain.Resource;
import com.ruitaowang.core.domain.Role;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.GoodsCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class PageResourceController extends BaseController {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private RoleService roleService;
    /**
     * 资源 － 更新资源
     */
    @RequestMapping(value = {"/permission_resource_update/{resourceId}"}, method = RequestMethod.GET)
    public ModelAndView updateResource(Model model, @PathVariable("resourceId") Long resourceId) {
        Resource resource = resourceService.selectByPK(resourceId);
        model.addAttribute("resource", resource);
        return new ModelAndView("/web/permission_resource_update");
    }

    /**
     *  角色 － 更新角色
     */
    @RequestMapping(value = {"/permission_role_update/{roleId}"}, method = RequestMethod.GET)
    public ModelAndView updateRole(Model model, @PathVariable("roleId") Long roleId) {
        Role role = roleService.selectByPK(roleId);
        model.addAttribute("role", role);
        return new ModelAndView("/web/permission_role_update");
    }
}