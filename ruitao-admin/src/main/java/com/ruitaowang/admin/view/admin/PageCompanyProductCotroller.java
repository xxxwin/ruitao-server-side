package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyProduct;
import com.ruitaowang.core.domain.LocalDiscount;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.CompanyProductService;
import com.ruitaowang.goods.service.LocalDiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;
import java.util.List;

/**
 * Created by Shaka on 2017/1/12.
 */
@RestController
public class PageCompanyProductCotroller extends BaseController {

    @Autowired
    private CompanyProductService companyProductService;
    @Autowired
    private LocalDiscountService localDiscountService;

    @RequestMapping(value={"/companyProduct/update/{productId}"},method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("productId") Long productId, Model model) {

        CompanyProduct companyProduct = companyProductService.selectByPK(productId);
        List<LocalDiscount> localDiscountList = localDiscountService.select(new LocalDiscount());
        model.addAttribute("companyProduct", companyProduct);
        model.addAttribute("localDiscountList", localDiscountList);
        return new ModelAndView("web/company_product_update_score_v2");
    }
    @RequestMapping(value={"/companyMembers/update/{companyId}"},method = RequestMethod.GET)
    @PermitAll
    public ModelAndView companyMembers(@PathVariable("companyId") Long companyId, Model model) {
        CompanyProduct companyProduct=new CompanyProduct();
        companyProduct.setCompanyId(companyId);
        List<CompanyProduct> companyProducts = companyProductService.select(companyProduct);
        List<LocalDiscount> localDiscountList = localDiscountService.select(new LocalDiscount());
        model.addAttribute("companyProduct", companyProducts.get(0));
        model.addAttribute("localDiscountList", localDiscountList);
        return new ModelAndView("web/company_product_update_score_v2");
    }

}
