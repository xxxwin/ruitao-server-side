package com.ruitaowang.core.domain;

public class MzAds extends Page {
    /**
     *  自增编码,所属表字段为mz_ads.ad_id
     */
    private Long adId;

    /**
     *  活动标题,所属表字段为mz_ads.ad_title
     */
    private String adTitle;

    /**
     *  活动链接,所属表字段为mz_ads.ad_url
     */
    private String adUrl;

    /**
     *  按钮类型0 无，1 悬浮,所属表字段为mz_ads.button_type
     */
    private Byte buttonType;

    /**
     *  类型
0  名片
1   广告
2   链接,所属表字段为mz_ads.ad_type
     */
    private Byte adType;

    /**
     *  创建时间,所属表字段为mz_ads.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为mz_ads.mtime
     */
    private Long mtime;

    /**
     *  0 正常
1 删除,所属表字段为mz_ads.rstatus
     */
    private Byte rstatus;

    /**
     *  链接类型0 普通链接，1 电话， 2 电话和链接,所属表字段为mz_ads.link_type
     */
    private Byte linkType;

    /**
     *  广告位置0 顶部， 1 底部,所属表字段为mz_ads.ad_position
     */
    private Byte adPosition;

    /**
     *  广告图片,所属表字段为mz_ads.ad_image_url
     */
    private String adImageUrl;

    /**
     *  文章发布者,所属表字段为mz_ads.ad_author
     */
    private Long adAuthor;

    /**
     *  ,所属表字段为mz_ads.phone
     */
    private String phone;

    /**
     *  ,所属表字段为mz_ads.button_name
     */
    private String buttonName;

    /**
     *  ,所属表字段为mz_ads.remark
     */
    private String remark;

    /**
     *  文本,所属表字段为mz_ads.ad_content
     */
    private String adContent;

    /**
     *  公司,所属表字段为mz_ads.ad_company
     */
    private String adCompany;

    /**
     *  头像,所属表字段为mz_ads.ad_headimage_url
     */
    private String adHeadimageUrl;

    /**
     *  邮件,所属表字段为mz_ads.ad_email
     */
    private String adEmail;

    /**
     *  微信,所属表字段为mz_ads.ad_wechat
     */
    private String adWechat;

    /**
     *  地址,所属表字段为mz_ads.ad_address
     */
    private String adAddress;

    public Long getAdId() {
        return adId;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }

    public String getAdTitle() {
        return adTitle;
    }

    public void setAdTitle(String adTitle) {
        this.adTitle = adTitle == null ? null : adTitle.trim();
    }

    public String getAdUrl() {
        return adUrl;
    }

    public void setAdUrl(String adUrl) {
        this.adUrl = adUrl == null ? null : adUrl.trim();
    }

    public Byte getButtonType() {
        return buttonType;
    }

    public void setButtonType(Byte buttonType) {
        this.buttonType = buttonType;
    }

    public Byte getAdType() {
        return adType;
    }

    public void setAdType(Byte adType) {
        this.adType = adType;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Byte getLinkType() {
        return linkType;
    }

    public void setLinkType(Byte linkType) {
        this.linkType = linkType;
    }

    public Byte getAdPosition() {
        return adPosition;
    }

    public void setAdPosition(Byte adPosition) {
        this.adPosition = adPosition;
    }

    public String getAdImageUrl() {
        return adImageUrl;
    }

    public void setAdImageUrl(String adImageUrl) {
        this.adImageUrl = adImageUrl == null ? null : adImageUrl.trim();
    }

    public Long getAdAuthor() {
        return adAuthor;
    }

    public void setAdAuthor(Long adAuthor) {
        this.adAuthor = adAuthor;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getButtonName() {
        return buttonName;
    }

    public void setButtonName(String buttonName) {
        this.buttonName = buttonName == null ? null : buttonName.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getAdContent() {
        return adContent;
    }

    public void setAdContent(String adContent) {
        this.adContent = adContent == null ? null : adContent.trim();
    }

    public String getAdCompany() {
        return adCompany;
    }

    public void setAdCompany(String adCompany) {
        this.adCompany = adCompany == null ? null : adCompany.trim();
    }

    public String getAdHeadimageUrl() {
        return adHeadimageUrl;
    }

    public void setAdHeadimageUrl(String adHeadimageUrl) {
        this.adHeadimageUrl = adHeadimageUrl == null ? null : adHeadimageUrl.trim();
    }

    public String getAdEmail() {
        return adEmail;
    }

    public void setAdEmail(String adEmail) {
        this.adEmail = adEmail == null ? null : adEmail.trim();
    }

    public String getAdWechat() {
        return adWechat;
    }

    public void setAdWechat(String adWechat) {
        this.adWechat = adWechat == null ? null : adWechat.trim();
    }

    public String getAdAddress() {
        return adAddress;
    }

    public void setAdAddress(String adAddress) {
        this.adAddress = adAddress == null ? null : adAddress.trim();
    }
}