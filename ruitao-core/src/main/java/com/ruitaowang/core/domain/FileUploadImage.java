package com.ruitaowang.core.domain;

public class FileUploadImage extends Page {
    /**
     *  自增编码,所属表字段为file_upload_image.image_id
     */
    private Long imageId;

    /**
     *  图片md5,所属表字段为file_upload_image.image_md5
     */
    private String imageMd5;

    /**
     *  图片url,所属表字段为file_upload_image.image_url
     */
    private String imageUrl;

    /**
     *  创建时间,所属表字段为file_upload_image.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为file_upload_image.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为file_upload_image.remark
     */
    private String remark;

    /**
     *  0 正常
1 删除,所属表字段为file_upload_image.rstatus
     */
    private Byte rstatus;

    public Long getImageId() {
        return imageId;
    }

    public void setImageId(Long imageId) {
        this.imageId = imageId;
    }

    public String getImageMd5() {
        return imageMd5;
    }

    public void setImageMd5(String imageMd5) {
        this.imageMd5 = imageMd5 == null ? null : imageMd5.trim();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}