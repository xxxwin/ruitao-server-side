package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.District;
import com.ruitaowang.core.domain.UserMembers;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.goods.dao.DistrictAdMapper;
import com.ruitaowang.goods.dao.DistrictMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class DistrictService implements BaseService<District> {
    @Autowired
    private DistrictMapper districtMapper;
    @Autowired
    private DistrictAdMapper districtAdMapper;

    @Override
    public District insert(District district) {
        Long time = System.currentTimeMillis();
        district.setCtime(time);
        district.setMtime(time);
        districtMapper.insert(district);
        return district;
    }

    @Override
    public int delete(Long id) {
        District district = new District();
        district.setId(id);
        Long time = System.currentTimeMillis();
        district.setMtime(time);
        return districtMapper.updateByPK(district);
    }

    @Override
    public District update(District district) {
        Long time = System.currentTimeMillis();
        district.setMtime(time);
        districtMapper.updateByPK(district);
        return district;
    }

    @Override
    public District selectByPK(Long id) {
        return districtMapper.selectByPK(id);
    }

    public District selectByCityUserId(Long userId) {
        District where = new District();
        where.setCityUserId(userId);
        List<District> districts = districtMapper.select(where);
        if(ObjectUtils.isEmpty(districts)){
            return  null;
        }

        return districts.get(0);
    }

    @Override
    public List<District> select(District district) {
        return districtMapper.select(district);
    }

    public List<District> selectRepeat(District district) {
        return districtAdMapper.selectRepeat(district);
    }

    public List<District> selectByName(District district) {
        return districtAdMapper.selectByName(district);
    }
    public List<District> selectByCity(District district) {
        return districtAdMapper.selectByCity(district);
    }
    public List<District> selectForPage(District district) {
        district.setRecords(districtMapper.count(district));
        return districtMapper.select(district);
    }

    public void setUserKTMembersLimit(Long districtId, Long userId) {//7 365
        //会员期限 user members
        //1 计算会员增加的天数
        int days = 365;
        District district = selectByPK(districtId);
        String currentTime = TimeUtils.getCurrentDatetime(TimeUtils.TimeFormat.SHORT_DATE_PATTERN_LINE);
        if(ObjectUtils.isEmpty(district)){
            return;
        }
        district.setUsed((byte) 1);
        if("0".equals(district.getInitial())){
            district.setCityUserId(userId);
            district.setInitial(TimeUtils.getDateAfterNDays(currentTime, days));
            this.update(district);
        }else{
            if(Integer.valueOf(district.getInitial().replace("-", "")) >= Integer.valueOf(currentTime.replace("-", ""))){
                district.setInitial(TimeUtils.getDateAfterNDays(district.getInitial(), days));
                this.update(district);
            }else{
                district.setInitial(TimeUtils.getDateAfterNDays(currentTime, days));
                this.update(district);
            }
        }
    }

}
