package com.ruitaowang.core.domain;

public class Goods extends Page {
    /**
     *  商品分类编号,所属表字段为goods.goods_id
     */
    private Long goodsId;

    /**
     *  商品名称,所属表字段为goods.goods_name
     */
    private String goodsName;

    /**
     *  商品分类ID,所属表字段为goods.category_id
     */
    private Long categoryId;

    /**
     *  商品货号,所属表字段为goods.goods_sn
     */
    private String goodsSn;

    /**
     *  本店价格,所属表字段为goods.goods_screen_price
     */
    private Integer goodsScreenPrice;

    /**
     *  进货价格,所属表字段为goods.goods_real_price
     */
    private Integer goodsRealPrice;

    /**
     *  1 自营
0 非自营,所属表字段为goods.self_support
     */
    private Boolean selfSupport;

    /**
     *  供货商ID,所属表字段为goods.goods_provider_id
     */
    private Long goodsProviderId;

    /**
     *  创建时间,所属表字段为goods.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为goods.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常
1 删除,所属表字段为goods.rstatus
     */
    private Byte rstatus;

    /**
     *  1 上架
0 下架,所属表字段为goods.goods_online
     */
    private Byte goodsOnline;

    /**
     *  推荐排序,所属表字段为goods.goods_recommend_sort
     */
    private Integer goodsRecommendSort;

    /**
     *  库存,所属表字段为goods.goods_stock
     */
    private Integer goodsStock;

    /**
     *  ,所属表字段为goods.goods_dumy_sales
     */
    private Integer goodsDumySales;

    /**
     *  商品详情,所属表字段为goods.goods_detail
     */
    private String goodsDetail;

    /**
     *  商品列表缩略图,所属表字段为goods.goods_thum
     */
    private String goodsThum;

    /**
     *  运费,所属表字段为goods.goods_ex_price
     */
    private Integer goodsExPrice;

    /**
     *  0  纯现金
1  纯积分
2  现金+积分,所属表字段为goods.goods_pay_type
     */
    private Byte goodsPayType;

    /**
     *  积分,所属表字段为goods.goods_score
     */
    private Integer goodsScore;

    /**
     *  0 实体店
1 微商
8 供货商,所属表字段为goods.goods_provider_type
     */
    private Byte goodsProviderType;

    /**
     *  ,所属表字段为goods.give_score
     */
    private Integer giveScore;

    /**
     *  0 邮寄
1 自提,所属表字段为goods.goods_get_type
     */
    private Byte goodsGetType;

    /**
     *  商品重量,所属表字段为goods.goods_weight
     */
    private Integer goodsWeight;

    /**
     *  商家店铺  自定义商品分类ID,所属表字段为goods.custom_type_id
     */
    private Long customTypeId;

    /**
     *  ,所属表字段为goods.best
     */
    private Byte best;

    /**
     *  ,所属表字段为goods.newest
     */
    private Byte newest;

    /**
     *  ,所属表字段为goods.discount
     */
    private Byte discount;

    /**
     *  所属表字段为goods.is_material
     */
    private Byte isMaterial;

    /**
     *  所属表字段为goods.distribution_mode
     */
    private Byte distributionMode;

    /**
     *  所属表字段为goods.start_date
     */
    private String startDate;

    /**
     *  所属表字段为goods.end_date
     */
    private String endDate;

    /**
     *  所属表字段为goods.detaile_description
     */
    private String detaileDescription;

    /**
     *  所属表字段为goods.is_after_sale
     */
    private Byte isAfterSale;

    /**
     *  标准类目下的商品分类,所属表字段为goods.cate_id
     */
    private Long cateId;

    /**
     *  发货地,所属表字段为goods.delivery_place
     */
    private String deliveryPlace;

    public Byte getIsMaterial() {
        return isMaterial;
    }

    public void setIsMaterial(Byte isMaterial) {
        this.isMaterial = isMaterial;
    }

    public Byte getDistributionMode() {
        return distributionMode;
    }

    public void setDistributionMode(Byte distributionMode) {
        this.distributionMode = distributionMode;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDetaileDescription() {
        return detaileDescription;
    }

    public void setDetaileDescription(String detaileDescription) {
        this.detaileDescription = detaileDescription;
    }

    public Byte getIsAfterSale() {
        return isAfterSale;
    }

    public void setIsAfterSale(Byte isAfterSale) {
        this.isAfterSale = isAfterSale;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName == null ? null : goodsName.trim();
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getGoodsSn() {
        return goodsSn;
    }

    public void setGoodsSn(String goodsSn) {
        this.goodsSn = goodsSn == null ? null : goodsSn.trim();
    }

    public Integer getGoodsScreenPrice() {
        return goodsScreenPrice;
    }

    public void setGoodsScreenPrice(Integer goodsScreenPrice) {
        this.goodsScreenPrice = goodsScreenPrice;
    }

    public Integer getGoodsRealPrice() {
        return goodsRealPrice;
    }

    public void setGoodsRealPrice(Integer goodsRealPrice) {
        this.goodsRealPrice = goodsRealPrice;
    }

    public Boolean getSelfSupport() {
        return selfSupport;
    }

    public void setSelfSupport(Boolean selfSupport) {
        this.selfSupport = selfSupport;
    }

    public Long getGoodsProviderId() {
        return goodsProviderId;
    }

    public void setGoodsProviderId(Long goodsProviderId) {
        this.goodsProviderId = goodsProviderId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Byte getGoodsOnline() {
        return goodsOnline;
    }

    public void setGoodsOnline(Byte goodsOnline) {
        this.goodsOnline = goodsOnline;
    }

    public Integer getGoodsRecommendSort() {
        return goodsRecommendSort;
    }

    public void setGoodsRecommendSort(Integer goodsRecommendSort) {
        this.goodsRecommendSort = goodsRecommendSort;
    }

    public Integer getGoodsStock() {
        return goodsStock;
    }

    public void setGoodsStock(Integer goodsStock) {
        this.goodsStock = goodsStock;
    }

    public Integer getGoodsDumySales() {
        return goodsDumySales;
    }

    public void setGoodsDumySales(Integer goodsDumySales) {
        this.goodsDumySales = goodsDumySales;
    }

    public String getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(String goodsDetail) {
        this.goodsDetail = goodsDetail == null ? null : goodsDetail.trim();
    }

    public String getGoodsThum() {
        return goodsThum;
    }

    public void setGoodsThum(String goodsThum) {
        this.goodsThum = goodsThum == null ? null : goodsThum.trim();
    }

    public Integer getGoodsExPrice() {
        return goodsExPrice;
    }

    public void setGoodsExPrice(Integer goodsExPrice) {
        this.goodsExPrice = goodsExPrice;
    }

    public Byte getGoodsPayType() {
        return goodsPayType;
    }

    public void setGoodsPayType(Byte goodsPayType) {
        this.goodsPayType = goodsPayType;
    }

    public Integer getGoodsScore() {
        return goodsScore;
    }

    public void setGoodsScore(Integer goodsScore) {
        this.goodsScore = goodsScore;
    }

    public Byte getGoodsProviderType() {
        return goodsProviderType;
    }

    public void setGoodsProviderType(Byte goodsProviderType) {
        this.goodsProviderType = goodsProviderType;
    }

    public Integer getGiveScore() {
        return giveScore;
    }

    public void setGiveScore(Integer giveScore) {
        this.giveScore = giveScore;
    }

    public Byte getGoodsGetType() {
        return goodsGetType;
    }

    public void setGoodsGetType(Byte goodsGetType) {
        this.goodsGetType = goodsGetType;
    }

    public Integer getGoodsWeight() {
        return goodsWeight;
    }

    public void setGoodsWeight(Integer goodsWeight) {
        this.goodsWeight = goodsWeight;
    }

    public Long getCustomTypeId() {
        return customTypeId;
    }

    public void setCustomTypeId(Long customTypeId) {
        this.customTypeId = customTypeId;
    }

    public Byte getBest() {
        return best;
    }

    public void setBest(Byte best) {
        this.best = best;
    }

    public Byte getNewest() {
        return newest;
    }

    public void setNewest(Byte newest) {
        this.newest = newest;
    }

    public Byte getDiscount() {
        return discount;
    }

    public void setDiscount(Byte discount) {
        this.discount = discount;
    }

	public Long getCateId() {
		return cateId;
	}

	public void setCateId(Long cateId) {
		this.cateId = cateId;
	}

    public String getDeliveryPlace() {
        return deliveryPlace;
    }

    public void setDeliveryPlace(String deliveryPlace) {
        this.deliveryPlace = deliveryPlace;
    }
}