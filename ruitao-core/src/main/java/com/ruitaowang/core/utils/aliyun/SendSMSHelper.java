package com.ruitaowang.core.utils.aliyun;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.aliyuncs.sms.model.v20160927.SingleSendSmsRequest;
import com.aliyuncs.sms.model.v20160927.SingleSendSmsResponse;

import java.security.SecureRandom;

/**
 * Created by neal on 12/26/16.
 */
public class SendSMSHelper {
    public static void sendSMS(String recPhone, String verifyCode) throws Exception {
        IClientProfile profile = DefaultProfile.getProfile("cn-shanghai", AliyunConfig.accessKeyId, AliyunConfig.accessKeySecret);
        DefaultProfile.addEndpoint("cn-shanghai", "cn-shanghai", "Dysmsapi", "dysmsapi.aliyuncs.com");
        IAcsClient acsClient  = new DefaultAcsClient(profile);
        SendSmsRequest request = new SendSmsRequest();

        try {
            request.setSignName("首象共享");
            request.setTemplateCode("SMS_36005183");
            request.setTemplateParam("{\"checnkcode\":\"" + verifyCode + "\"}");
            request.setPhoneNumbers(recPhone);
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            if(sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
                System.out.println("请求成功");
            }
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    public static void sendOrder(String recPhone, String status) throws Exception {
        IClientProfile profile = DefaultProfile.getProfile("cn-shanghai", AliyunConfig.accessKeyId, AliyunConfig.accessKeySecret);
        DefaultProfile.addEndpoint("cn-shanghai", "cn-shanghai", "Dysmsapi", "dysmsapi.aliyuncs.com");
        IAcsClient acsClient  = new DefaultAcsClient(profile);
        SendSmsRequest request = new SendSmsRequest();

        try {
            request.setSignName("首象共享");
            request.setTemplateCode("SMS_160306426");
            request.setTemplateParam("{\"status\":\"" + status + "\"}");
            request.setPhoneNumbers(recPhone);
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            if(sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
                System.out.println("请求成功");
            }
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

//    public static int random6() {
//        SecureRandom secureRandom = new SecureRandom();
//        return secureRandom.nextInt(999999);
//    }
}
