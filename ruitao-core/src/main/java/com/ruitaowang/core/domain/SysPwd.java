package com.ruitaowang.core.domain;

public class SysPwd extends Page {
    /**
     *  用户ID,所属表字段为t_sys_pwd.user_id
     */
    private Long userId;

    /**
     *  用户交易密码,所属表字段为t_sys_pwd.user_pwd
     */
    private String userPwd;

    /**
     *  ,所属表字段为t_sys_pwd.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为t_sys_pwd.mtime
     */
    private Long mtime;

    /**
     *  ,所属表字段为t_sys_pwd.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为t_sys_pwd.remark
     */
    private String remark;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd == null ? null : userPwd.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}