package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Coupon;
import java.util.List;

public interface CouponMapper {
    int insert(Coupon coupon);

    int deleteByPK(Long id);

    int updateByPK(Coupon coupon);

    Coupon selectByPK(Long id);

    long count(Coupon coupon);

    List<Coupon> select(Coupon coupon);
}