package com.ruitaowang.core.domain;

import java.util.List;

public class OrderVO extends Order {
    private Double price;

    private String strtime;

    private Double exPriceVO;

    private Double memberPriceVO;

    private Long companyId;

    public String allOrderBegin;

    public String allOrderEnd;

    private List<ShoppingOrderVO> companyList;

    private Integer goodsNumber;

    private String nickName;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }


    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public List<ShoppingOrderVO> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<ShoppingOrderVO> companyList) {
        this.companyList = companyList;
    }

    public String getAllOrderBegin() {
        return allOrderBegin;
    }

    public void setAllOrderBegin(String allOrderBegin) {
        this.allOrderBegin = allOrderBegin;
    }

    public String getAllOrderEnd() {
        return allOrderEnd;
    }

    public void setAllOrderEnd(String allOrderEnd) {
        this.allOrderEnd = allOrderEnd;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Double getExPriceVO() {
        return exPriceVO;
    }

    public void setExPriceVO(Double exPriceVO) {
        exPriceVO=exPriceVO/100;
        this.exPriceVO = exPriceVO;
    }

    public Double getMemberPriceVO() {
        return memberPriceVO;
    }

    public void setMemberPriceVO(Double memberPriceVO) {
        memberPriceVO=memberPriceVO/100;
        this.memberPriceVO = memberPriceVO;
    }

    public Double getPrice() {

        return price;
    }

    public void setPrice(Double price) {

        price=price/100;

        this.price = price;
    }
    public String getStrtime() {
        return strtime;
    }

    public void setStrtime(String strtime) {
        this.strtime = strtime;
    }

}