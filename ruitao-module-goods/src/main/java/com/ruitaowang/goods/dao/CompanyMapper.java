package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Company;
import java.util.List;

public interface CompanyMapper {
    int insert(Company company);

    int deleteByPK(Long companyId);

    int updateByPK(Company company);

    Company selectByPK(Long companyId);

    long count(Company company);

    List<Company> select(Company company);
}