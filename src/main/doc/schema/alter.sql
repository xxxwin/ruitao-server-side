ALTER TABLE `rtshop`.`t_sys_user` add COLUMN `qrimgurl` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '二维码';
ALTER TABLE `rtshop`.`t_sys_user` add COLUMN `nickname` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '昵称';
ALTER TABLE `rtshop`.`t_sys_user` change COLUMN `username` `username` VARCHAR(191) COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT '' COMMENT '账号';
ALTER TABLE `rtshop`.`t_sys_user` change COLUMN `nickname` `nickname` VARCHAR(191) COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT '' COMMENT '昵称';

ALTER TABLE `rtshop`.`district` add COLUMN `price` INT(11) NOT NULL DEFAULT '0' COMMENT '加盟费,最低1000';
ALTER TABLE `rtshop`.`district` add COLUMN `used` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 没交费 1 以缴费';

ALTER TABLE `rtshop`.`company_product` change COLUMN `qrcode_url` `qrcode_url` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '二维码';
ALTER TABLE `rtshop`.`t_sys_resource` change COLUMN `icon` `icon` VARCHAR(105) NOT NULL DEFAULT '' COMMENT 'icon';
ALTER TABLE `rtshop`.`goods_attribute_link` add COLUMN `real_extra_price` INT(11) NOT NULL DEFAULT '0' COMMENT '进货价增量';

update rtshop.goods set goods_dumy_sales = floor(rand()*500) , goods_stock = floor(rand() * 1000);


-- ALTER TABLE `rtshop`.`t_resource` add COLUMN `real_link` VARCHAR(255) NOT NULL DEFAULT '真正链接';


update rtshop.t_sys_resource set rstatus=0;
update rtshop.t_sys_role set rstatus=0;

ALTER TABLE `rtshop`.`t_sys_resource` change COLUMN `rstatus` `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除';
ALTER TABLE `rtshop`.`t_sys_resource` change COLUMN `link`  `link` VARCHAR(100) NOT NULL DEFAULT '-1' COMMENT '资源url';
ALTER TABLE `rtshop`.`t_sys_role` change COLUMN `rstatus` `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除';

ALTER TABLE `rtshop`.`company` add COLUMN  `company_status` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 待审核\n1 审核通过\n2 未通过';
ALTER TABLE `rtshop`.`company` add COLUMN  `audit_cause` VARCHAR(255) NOT NULL DEFAULT 0 COMMENT '审核原因';
ALTER TABLE `rtshop`.`company` add COLUMN  `bank_id` VARCHAR(45) NOT NULL DEFAULT 0 COMMENT '银行卡账号';
ALTER TABLE `rtshop`.`company` add COLUMN  `co_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 红酒\n1 虫草\n2 积分\n3 保证金';



ALTER TABLE `rtshop`.`goods_order` add COLUMN  `goods_pay_type` TINYINT(4) NOT NULL DEFAULT 0;
ALTER TABLE `rtshop`.`goods_order` add COLUMN  `order_amount` INT(11) NOT NULL DEFAULT 0 COMMENT '订单总金额，不包括运费',
ALTER TABLE `rtshop`.`order_prod` add COLUMN  `goods_pay_type` TINYINT(4) NOT NULL DEFAULT 0;
ALTER TABLE `rtshop`.`order_prod` add COLUMN  `goods_score`        INT(10)      NOT NULL DEFAULT 0;

-- 2017/1/22
ALTER TABLE `rtshop`.`district`
  CHANGE COLUMN `order` `district_order` TINYINT(4) NOT NULL DEFAULT '0' ;
ALTER TABLE `rtshop`.`district`
  ADD COLUMN `biz_user_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '区代理' AFTER `used`,
  ADD COLUMN `city_user_id` BIGINT(20) NOT NULL DEFAULT 0 AFTER `biz_user_id`;

ALTER TABLE `rtshop`.`company`
  ADD COLUMN `district_id` BIGINT(20) NOT NULL DEFAULT 0 AFTER `co_type`,
  ADD COLUMN `province` VARCHAR(255) NOT NULL DEFAULT '' AFTER `district_id`,
  ADD COLUMN `city` VARCHAR(255) NOT NULL DEFAULT '' AFTER `province`,
  ADD COLUMN `district` VARCHAR(255) NOT NULL DEFAULT '' AFTER `city`;


INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `consumer_id`, `order_type`) VALUES ('2', '1483960578020', '1483960578020', '100', '1', '1', '0');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `consumer_id`, `order_type`) VALUES ('2', '1483960578020', '1483960578020', '200', '2', '1', '0');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `consumer_id`, `order_type`) VALUES ('2', '1483960578020', '1483960578020', '300', '3', '1', '0');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `consumer_id`, `order_type`) VALUES ('2', '1483960578020', '1483960578020', '400', '4', '1', '0');



ALTER TABLE `rtshop`.`local_order`
CHANGE COLUMN `product_id` `product_id` BIGINT(20) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `company_id` `company_id` BIGINT(20) NOT NULL DEFAULT 0 ,
ADD COLUMN `discount_id` BIGINT(20) NOT NULL DEFAULT 0 AFTER `company_id`;


ALTER TABLE `rtshop`.`t_sys_user`
  CHANGE COLUMN `nickname` `nickname` VARCHAR(191) COLLATE 'utf8mb4_unicode_ci' NOT NULL DEFAULT '' COMMENT '昵称' ,
  ADD COLUMN `id_card` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '省份证号' AFTER `nickname`;

ALTER TABLE `rtshop`.`order_prod`
  ADD COLUMN `goods_provider_id` BIGINT(20) NOT NULL DEFAULT 0 COMMENT '供货商编号' AFTER `goods_score`;

ALTER TABLE `rtshop`.`meetings`
  ADD COLUMN `qr_sign_up` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '会议报名二维码' AFTER `company_id`,
  ADD COLUMN `qr_sign_in` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '会议签到' AFTER `qr_sign_up`;

-- user_reward

-- company_product

ALTER TABLE `rtshop`.`company_product`
  ADD COLUMN `discount_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 满减\n1 百分比' ,
  ADD COLUMN `percentage` INT NOT NULL DEFAULT 0 COMMENT '整数 要除以100' AFTER `discount_type`,
  ADD COLUMN `reach1` INT NOT NULL DEFAULT 0 AFTER `percentage`,
  ADD COLUMN `score1` INT NOT NULL DEFAULT 0 AFTER `reach1`,
  ADD COLUMN `reach2` INT NOT NULL DEFAULT 0 AFTER `score1`,
  ADD COLUMN `score2` INT NOT NULL DEFAULT 0 AFTER `reach2`,
  ADD COLUMN `reach3` INT NOT NULL DEFAULT 0 AFTER `score2`,
  ADD COLUMN `score3` INT NOT NULL DEFAULT 0 AFTER `reach3`,
  ADD COLUMN `reach4` INT NOT NULL DEFAULT 0 AFTER `score3`,
  ADD COLUMN `score4` INT NOT NULL DEFAULT 0 AFTER `reach4`,
  ADD COLUMN `reach5` INT NOT NULL DEFAULT 0 AFTER `score4`,
  ADD COLUMN `score5` INT NOT NULL DEFAULT 0 AFTER `reach5`;

ALTER TABLE `rtshop`.`local_order`
  ADD COLUMN `score` INT NOT NULL DEFAULT 0 COMMENT '返的积分' AFTER `discount_id`;

-- 区分供货商和微商 2017-04-06

ALTER TABLE `rtshop`.`goods_order`
  ADD COLUMN `amount_score` INT NOT NULL DEFAULT 0 COMMENT '送的积分数' AFTER `order_amount`,
  ADD COLUMN `amount_money` INT NOT NULL DEFAULT 0 COMMENT '返利金额' AFTER `amount_score`;

ALTER TABLE `rtshop`.`order_prod`
  ADD COLUMN `give_score` INT NOT NULL DEFAULT 0 COMMENT '送的积分' AFTER `goods_provider_id`,
  ADD COLUMN `give_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 供货商产品\n1 微商' AFTER `give_score`;

ALTER TABLE `rtshop`.`user_profit`
  ADD COLUMN `order_sn` VARCHAR(45) NOT NULL DEFAULT 0 COMMENT '订单唯一编号' AFTER `profit_id`;


ALTER TABLE `rtshop`.`activity`
  CHANGE COLUMN `activity_content` `activity_content` LONGTEXT NOT NULL COMMENT '互动内容' ;


-- admin A  10.27.143.116
-- wwww A  139.196.250.88
-- certbot renew --force-renew
-- nginx -s reload


CREATE TABLE IF NOT EXISTS `rtshop`.`stat_company` (
  `company_id` BIGINT(20) NOT NULL COMMENT '编号',
  `order_count` INT NOT NULL DEFAULT '0' COMMENT '商城订单数',
  `ctime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `local_order_count` INT NOT NULL DEFAULT '0' COMMENT '扫码订单数',
  `order_income` INT NOT NULL DEFAULT '0' COMMENT '商城订单总收入',
  `remark` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '备注',
  `local_order_income` INT NOT NULL DEFAULT '0' COMMENT '店面订单收入',
  `dish_order_count` INT NOT NULL DEFAULT '0' COMMENT '点餐订餐数量',
  `dish_order_income` INT(11) NOT NULL DEFAULT '0' COMMENT '点餐订单收入',
  `date` VARCHAR(45) NOT NULL DEFAULT '0000-00-00' COMMENT '日期',
  PRIMARY KEY (`company_id`))
  ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARACTER SET = utf8
  COMMENT = '本地扫码订单';


CREATE TABLE IF NOT EXISTS `rtshop`.`stat_company_user` (
  `company_id` BIGINT(20) NOT NULL COMMENT '编号',
  `order_count` INT NOT NULL DEFAULT '0' COMMENT '商城订单数',
  `ctime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `local_order_count` INT NOT NULL DEFAULT '0' COMMENT '扫码订单数',
  `order_consumption` INT NOT NULL DEFAULT '0' COMMENT '用户消费额',
  `remark` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '备注',
  `local_order_consumption` INT NOT NULL DEFAULT '0' COMMENT '店面用户消费额',
  `dish_order_count` INT NOT NULL DEFAULT '0' COMMENT '点餐订餐数量',
  `dish_order_consumption` INT(11) NOT NULL DEFAULT '0' COMMENT '点餐订单消费额',
  `date` VARCHAR(45) NOT NULL DEFAULT '0000-00-00' COMMENT '日期',
  `user_id` BIGINT(20) NOT NULL DEFAULT 0,
  UNIQUE INDEX `unq_company_user` (`company_id` ASC, `user_id` ASC))
  ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARACTER SET = utf8
  COMMENT = '本地扫码订单';



ALTER TABLE `rtshop`.`stat_company`
  ADD COLUMN `id` BIGINT(20) NOT NULL AUTO_INCREMENT AFTER `date`,
  ADD PRIMARY KEY (`id`);
ALTER TABLE `rtshop`.`stat_company_user`
  ADD COLUMN `id` BIGINT(20) NOT NULL AUTO_INCREMENT AFTER `user_id`,
  ADD PRIMARY KEY (`id`);

ALTER TABLE `rtshop`.`mz_ads`
  CHANGE COLUMN `button_type` `button_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '按钮类型0 无，1 悬浮' ;

ALTER TABLE `rtshop`.`company`
  ADD COLUMN `redirect_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 官网首页\n1 微店' AFTER `bank_deposit`;


ALTER TABLE `rtshop`.`user_members`
  ADD COLUMN `member_id` BIGINT(20) NOT NULL AUTO_INCREMENT AFTER `member_type`,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`member_id`),
  ADD UNIQUE INDEX `id_type_unq` (`user_id` ASC, `member_type` ASC);


ALTER TABLE `rtshop`.`goods`
  ADD COLUMN `goods_get_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 邮寄\n1 自提' AFTER `give_score`;


ALTER TABLE `rtshop`.`order_prod`
  ADD COLUMN `goods_get_type` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 邮寄\n1 自提' AFTER `goods_provider_type`;

/**
2019.1.4
 */
ALTER TABLE `rtshop`.`ls_msg`
CHANGE COLUMN `msg_type` `msg_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '消息类型\n0 文本\n1 图片\n2 语音\n3 视频\n4 点赞' ;

/**
2019.1.7
 */
UPDATE `rtshop`.`company` SET `company_type`='1' WHERE `company_id`='317';
UPDATE `rtshop`.`user_reward` SET `rstatus`='1' WHERE `reward_id`='77';

/**
2019.1.21
 */
UPDATE `rtshop`.`goods_album` SET `url`='http://static.ruitaowang.com/attached/file/2019/01/20/20190120082427663.jpg', `original`='' WHERE `image_id`='15429';
UPDATE `rtshop`.`goods_album` SET `url`='http://static.ruitaowang.com/attached/file/2019/01/19/20190119134215780.jpg' WHERE `image_id`='15428';

INSERT INTO `rtshop`.`goods_album` (`goods_id`, `image_type`, `ctime`, `mtime`, `url`) VALUES ('2809', '1', '1547886146771', '1547886146771', 'http://static.ruitaowang.com/attached/file/2019/01/18/20190118122805719.jpg');
UPDATE `rtshop`.`goods_album` SET `url`='http://static.ruitaowang.com/attached/file/2019/01/21/20190121194128730.jpg' WHERE `image_id`='15414';

/**
2019.1.22
 */
UPDATE `rtshop`.`company` SET `rstatus`='0' WHERE `company_id`='111';

/**
2019.1.23
 */
UPDATE `rtshop`.`company` SET `company_name`='杰奥商城' WHERE `company_id`='363';
/**
2019.1.24
 */
 UPDATE `rtshop`.`t_sys_user` SET `username`='13621128967', `phone`='13621128967' WHERE `id`='67709';

 /**
 2019.1.25
  */
  UPDATE `rtshop`.`relations` SET `pid`='11701' WHERE `uesr_id`='68835';
  UPDATE `rtshop`.`relations` SET `pid`='11701' WHERE `uesr_id`='1680';

  /**
  2019.1.26
   */
UPDATE `rtshop`.`company` SET `slogan`='过春节厂家和快递都放假27号到正月初七的订单都节后发货' WHERE `company_id`='281';

  /**
  2019.2.18
   */
   UPDATE `rtshop`.`company` SET `company_name`='北京智慧云脑教育' WHERE `company_id`='335';

/**
2019.2.19
 */
 UPDATE `rtshop`.`relations` SET `ctime`='1488297600000', `mtime`='1488297600000' WHERE `uesr_id`='6700';
 UPDATE `rtshop`.`company` SET `company_name`='北京智慧 云脑教育' WHERE `company_id`='335';

 /**
2019.2.22
 */
 ALTER TABLE `rtshop`.`t_sys_user`
ADD COLUMN `subscribe` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '是否关注公众号' AFTER `wechat_id`;
/**
2019.2.25
 */
UPDATE `rtshop`.`wallet` SET `user_balance`='86' WHERE `user_id`='11701';
UPDATE `rtshop`.`wallet` SET `user_balance`='1965754' WHERE `user_id`='13';
UPDATE `rtshop`.`relations` SET `pid`='95366' WHERE `uesr_id`='225705';
UPDATE `rtshop`.`t_sys_user` SET `phone`='14715372365' WHERE `id`='209421';
UPDATE `rtshop`.`relations` SET `pid`='130444' WHERE `uesr_id`='209421';

/**
2019.2.27
 */
UPDATE `rtshop`.`company` SET `company_type`='1' WHERE `company_id`='140';

/**
2019.3.13
 */
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1172';


UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='128';


UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1230';

UPDATE `rtshop`.`wallet_company` SET `company_amount`='0', `company_balance`='0' WHERE `company_id`='150';

UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2773';

UPDATE `rtshop`.`wallet` SET `user_score`='23645' WHERE `user_id`='11700';

/**
2019.3.18
 */
UPDATE `rtshop`.`company` SET `company_type_id`='231' WHERE `company_id`='140';


UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1228';

UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='126';

UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1170';

UPDATE `rtshop`.`wallet` SET `user_score`='14600' WHERE `user_id`='1680';

UPDATE `rtshop`.`wallet` SET `user_score`='363954' WHERE `user_id`='11701';

UPDATE `rtshop`.`wallet` SET `user_score`='695866' WHERE `user_id`='13';

UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2762';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2763';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2764';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2765';

UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1229';

UPDATE `rtshop`.`wallet` SET `user_score`='29533' WHERE `user_id`='11700';

UPDATE `rtshop`.`wallet` SET `user_score`='695831' WHERE `user_id`='13';

UPDATE `rtshop`.`wallet` SET `user_score`='178338' WHERE `user_id`='6700';

UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2769';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2770';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2771';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2772';

UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1232';


UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='130';

UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1174';

UPDATE `rtshop`.`wallet` SET `user_score`='65533' WHERE `user_id`='11700';

UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2778';


UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='308';

UPDATE `rtshop`.`wallet_company` SET `company_balance`='7000' WHERE `company_id`='286';

UPDATE `rtshop`.`wallet_company` SET `company_amount`='28928', `company_balance`='28928' WHERE `company_id`='166';


UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='309';


UPDATE `rtshop`.`wallet_company` SET `company_amount`='31752', `company_balance`='31752' WHERE `company_id`='274';

/**
2019.3.25
 */
UPDATE `rtshop`.`relations` SET `pid`='68772' WHERE `uesr_id`='4337';

/**
2019.3.26
 */
UPDATE `rtshop`.`wallet` SET `user_score`='11000' WHERE `user_id`='65522';

UPDATE `rtshop`.`wallet` SET `user_balance`='0' WHERE `user_id`='64745';

UPDATE `rtshop`.`wallet` SET `user_balance`='1965857' WHERE `user_id`='13';

UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2820';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2821';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2822';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2823';

UPDATE `rtshop`.`wallet_company` SET `company_amount`='12691', `company_balance`='12691' WHERE `company_id`='135';

UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='312';

UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1254';

UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='134';


UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1196';

/**
2019.4.8
 */
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1282';


UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='140';


UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1230';

UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='2922';

UPDATE `rtshop`.`wallet` SET `user_score`='428982' WHERE `user_id`='11701';

UPDATE `rtshop`.`wallet_company` SET `company_amount`='0', `company_balance`='0' WHERE `company_id`='147';

/**
2019.4.9
 */
INSERT INTO `rtshop`.`qrcode_record` (`user_id`, `number`, `buy_num`, `buy_status`, `price`, `ctime`, `mtime`, `remark`, `rstatus`) VALUES ('299662', '0', '0', '2', '0', '1554799365000', '1554799365000', '手动添加高级合伙人', '0');

/**
2019.4.10
 */
UPDATE `rtshop`.`company` SET `company_name`='仁和旗舰店' WHERE `company_id`='346';

/**
2019.4.11
 */
INSERT INTO `rtshop`.`goods_type_custom` (`company_id`, `name`, `remark`, `ctime`, `mtime`) VALUES ('346', '美妆护肤', '手动添加', '1554965143000', '1554965143000');
INSERT INTO `rtshop`.`goods_type_custom` (`company_id`, `name`, `remark`, `ctime`, `mtime`) VALUES ('346', '护理护具', '手动添加', '1554965143000', '1554965143000');
INSERT INTO `rtshop`.`goods_type_custom` (`company_id`, `name`, `remark`, `ctime`, `mtime`) VALUES ('346', '健康保健', '手动添加', '1554965143000', '1554965143000');

/**
2019.4.13
 */
UPDATE `rtshop`.`relations` SET `pid`='87485', `remark`='手动修改' WHERE `uesr_id`='103343';

/**
2019.4.24
 */
INSERT INTO `rtshop`.`relations` VALUES (313873,139054,'手动添加',1556099735000,1556099735000);
UPDATE `rtshop`.`user_members` SET `end_time`='2019-05-24' WHERE `member_id`='74197';

/**
2019.4.25
 */
UPDATE `rtshop`.`company` SET `company_name`='云脑教育  东丽分校', `longitude`='117.301741', `latitude`='39.09101' WHERE `company_id`='335';

/**
2019.5.4
 */
UPDATE `rtshop`.`company` SET `longitude`='113.952944', `latitude`='22.549696' WHERE `company_id`='377';

/**
2019.5.6
 */
UPDATE `rtshop`.`goods` SET `goods_screen_price`='9900' WHERE `goods_id`='1592';

/**
2019.5.7
 */
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1295';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='145';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1243';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3059';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3060';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3061';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3062';
UPDATE `rtshop`.`wallet` SET `user_score`='201246' WHERE `user_id`='11700';
UPDATE `rtshop`.`wallet` SET `user_score`='1036289', `user_balance`='1966967' WHERE `user_id`='13';
UPDATE `rtshop`.`wallet` SET `user_balance`='1078492' WHERE `user_id`='6700';
UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='321';
UPDATE `rtshop`.`wallet_company` SET `company_amount`='0', `company_balance`='0' WHERE `company_id`='124';

/**
2019.5.8
 */
UPDATE `rtshop`.`user_members` SET `permanent`='1' WHERE `member_id`='234150';
UPDATE `rtshop`.`user_members` SET `permanent`='1' WHERE `member_id`='1166';
UPDATE `rtshop`.`user_members` SET `permanent`='1' WHERE `member_id`='167655';

/**
2019.5.10
 */
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='33';
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='35';
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='129';
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='136';
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='135';
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='138';
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='149';
UPDATE `rtshop`.`shopping_order` SET `order_status`='1' WHERE `id`='162';
UPDATE `rtshop`.`goods` SET `goods_online`='0' WHERE `goods_id`='2482';
UPDATE `rtshop`.`goods` SET `goods_online`='0' WHERE `goods_id`='2483';
UPDATE `rtshop`.`goods` SET `goods_online`='0' WHERE `goods_id`='2486';
UPDATE `rtshop`.`company_staff` SET `id`='1', `company_id`='281', `user_id`='17', `status`='1', `ctime`='1557455380000', `mtime`='1557455380000' WHERE `id`='2';
UPDATE `rtshop`.`company` SET `rstatus`='1' WHERE `company_id`='280';

/**
2019.5.15
 */
UPDATE `rtshop`.`t_sys_user` SET `subscribe`='1' WHERE `id`='89940';

/**
2019.5.11
 */
UPDATE `rtshop`.`qrcode_record` SET `ctime`='1384296146504', `mtime`='1384296146504' WHERE `id`='181';
UPDATE `rtshop`.`relations` SET `ctime`='1496993037200', `mtime`='1496993037200' WHERE `uesr_id`='6700';

/**
2019.5.20
 */
UPDATE `rtshop`.`relations` SET `pid`='11701' WHERE `uesr_id`='167312';

/**
2019.5.27
 */
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1352';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='178';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1313';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3245';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3246';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3247';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3248';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3249';
UPDATE `rtshop`.`wallet` SET `user_score`='40531' WHERE `user_id`='307079';
UPDATE `rtshop`.`wallet` SET `user_balance`='9093' WHERE `user_id`='47555';
UPDATE `rtshop`.`wallet` SET `user_balance`='1087639' WHERE `user_id`='6700';
UPDATE `rtshop`.`wallet` SET `user_balance`='1998666' WHERE `user_id`='13';
UPDATE `rtshop`.`wallet_company` SET `company_amount`='12389', `company_balance`='12389' WHERE `company_id`='373';
UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='346';
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1353';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='179';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1314';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3250';
UPDATE `rtshop`.`wallet` SET `user_score`='42531' WHERE `user_id`='307079';
UPDATE `rtshop`.`wallet_company` SET `company_amount`='0', `company_balance`='0' WHERE `company_id`='309';
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1354';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='180';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1315';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3251';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3252';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3253';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3254';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3255';
UPDATE `rtshop`.`wallet` SET `user_score`='44527' WHERE `user_id`='307079';
UPDATE `rtshop`.`wallet` SET `user_balance`='9081' WHERE `user_id`='47555';
UPDATE `rtshop`.`wallet` SET `user_balance`='1087638' WHERE `user_id`='6700';
UPDATE `rtshop`.`wallet` SET `user_balance`='1998660' WHERE `user_id`='13';
UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='348';


UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='383';
UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='387';
UPDATE `rtshop`.`wallet_company` SET `company_balance`='21400' WHERE `company_id`='393';

/**
2019.6.13
 */
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1433';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='211';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1405';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3606';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3607';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3608';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3609';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3610';
UPDATE `rtshop`.`wallet` SET `user_score`='139703', `user_balance`='1502' WHERE `user_id`='11700';
UPDATE `rtshop`.`wallet` SET `user_balance`='2071166' WHERE `user_id`='13';
UPDATE `rtshop`.`wallet` SET `user_balance`='123903' WHERE `user_id`='5658';
UPDATE `rtshop`.`wallet` SET `user_balance`='1120441' WHERE `user_id`='6700';


UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1453';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='217';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1426';
UPDATE `rtshop`.`wallet` SET `user_score`='53600' WHERE `user_id`='307079';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3695';
UPDATE `rtshop`.`wallet_company` SET `company_amount`='560', `company_balance`='560' WHERE `company_id`='290';
UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='379';
UPDATE `rtshop`.`wallet_company` SET `company_amount`='0', `company_balance`='0' WHERE `company_id`='144';

/**
2019.6.14
 */
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1459';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='222';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1432';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3728';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3729';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3730';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3731';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3732';
UPDATE `rtshop`.`wallet` SET `user_score`='1047' WHERE `user_id`='64022';
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1460';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='223';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1433';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3733';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3734';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3735';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3736';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3737';
UPDATE `rtshop`.`wallet` SET `user_score`='1067' WHERE `user_id`='64022';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2508';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2516';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2544';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2645';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2647';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2648';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2649';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2650';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2651';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2652';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2653';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2654';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2655';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2656';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2680';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2685';
UPDATE `rtshop`.`goods` SET `rstatus`='1' WHERE `goods_id`='2693';

/**
2019.6.17
 */
UPDATE `rtshop`.`goods_order` SET `rstatus`='1' WHERE `order_id`='1456';
UPDATE `rtshop`.`shopping_order` SET `rstatus`='1' WHERE `id`='219';
UPDATE `rtshop`.`order_prod` SET `rstatus`='1' WHERE `prod_id`='1429';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3718';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3719';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3720';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3721';
UPDATE `rtshop`.`user_profit` SET `rstatus`='1' WHERE `profit_id`='3722';
UPDATE `rtshop`.`wallet` SET `user_score`='27733' WHERE `user_id`='64022';
UPDATE `rtshop`.`wallet` SET `user_balance`='2107107' WHERE `user_id`='13';
UPDATE `rtshop`.`wallet` SET `user_balance`='127688' WHERE `user_id`='5658';
UPDATE `rtshop`.`wallet` SET `user_balance`='1129346' WHERE `user_id`='6700';
UPDATE `rtshop`.`company_profit` SET `rstatus`='1' WHERE `profit_id`='395';
UPDATE `rtshop`.`wallet_company` SET `company_amount`='12920', `company_balance`='12920' WHERE `company_id`='153';

UPDATE `rtshop`.`company` SET `company_name`='山东新大东股份有限公司' WHERE `company_id`='87';
UPDATE `rtshop`.`company` SET `company_name`='福雅日用塑料制品厂' WHERE `company_id`='139';
UPDATE `rtshop`.`company` SET `company_name`='瑞安市惠宏鞋厂' WHERE `company_id`='154';
UPDATE `rtshop`.`company` SET `company_name`='义乌市楠涛服饰有限公司' WHERE `company_id`='157';
UPDATE `rtshop`.`company` SET `company_name`='义乌市众横汽车用品有限公司' WHERE `company_id`='163';
UPDATE `rtshop`.`company` SET `company_name`='广州持高贸易有限公司(休闲食品)' WHERE `company_id`='273';

UPDATE `rtshop`.`company` SET `headimgurl`='http://static.ruitaowang.com/attached/file/2019/06/17/20190617165449901.jpg' WHERE `company_id`='389';

/**
2019.6.17
 */
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('101992', '1562461200000', '1562461200000', '5000', '5000', '28', '7000', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('102753', '1562461200000', '1562461200000', '3000', '3000', '28', '3731', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('308797', '1562461200000', '1562461200000', '2000', '2000', '28', '229000', '6', '2', '1');

UPDATE `rtshop`.`wallet` SET `user_score`='229000' WHERE `user_id`='308797';
UPDATE `rtshop`.`wallet` SET `user_score`='3731' WHERE `user_id`='102753';
UPDATE `rtshop`.`wallet` SET `user_score`='7000' WHERE `user_id`='101992';

INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('308797', '1562547600000', '1562547600000', '5000', '5000', '28', '234000', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('67126', '1562547600000', '1562547600000', '3000', '3000', '28', '34000', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('101992', '1562547600000', '1562547600000', '2000', '2000', '28', '9000', '6', '2', '1');

UPDATE `rtshop`.`wallet` SET `user_score`='234000' WHERE `user_id`='308797';
UPDATE `rtshop`.`wallet` SET `user_score`='34000' WHERE `user_id`='67126';
UPDATE `rtshop`.`wallet` SET `user_score`='9000' WHERE `user_id`='101992';

UPDATE `rtshop`.`wallet` SET `user_score`='239000' WHERE `user_id`='308797';
UPDATE `rtshop`.`wallet` SET `user_score`='8340' WHERE `user_id`='355792';
UPDATE `rtshop`.`wallet` SET `user_score`='36000' WHERE `user_id`='67126';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('308797', '1562634000000', '1562634000000', '5000', '5000', '28', '239000', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('355792', '1562634000000', '1562634000000', '3000', '3000', '28', '8340', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('67126', '1562634000000', '1562634000000', '2000', '2000', '28', '36000', '6', '2', '1');

UPDATE `rtshop`.`wallet` SET `user_score`='244000' WHERE `user_id`='308797';
UPDATE `rtshop`.`wallet` SET `user_score`='72060' WHERE `user_id`='65497';
UPDATE `rtshop`.`wallet` SET `user_score`='10340' WHERE `user_id`='355792';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('308797', '1562720400000', '1562720400000', '5000', '5000', '28', '244000', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('65497', '1562720400000', '1562720400000', '3000', '3000', '28', '72060', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('355792', '1562720400000', '1562720400000', '2000', '2000', '28', '10340', '6', '2', '1');

UPDATE `rtshop`.`wallet` SET `user_score`='555172' WHERE `user_id`='11701';
UPDATE `rtshop`.`wallet` SET `user_score`='13340' WHERE `user_id`='355792';
UPDATE `rtshop`.`wallet` SET `user_score`='38000' WHERE `user_id`='67126';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `order_id`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('11701', '1562806800000', '1562806800000', '5000', '0', '5000', '28', '555172', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `order_id`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('355792', '1562806800000', '1562806800000', '3000', '0', '3000', '28', '13340', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `order_id`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('67126', '1562806800000', '1562806800000', '2000', '0', '2000', '28', '38000', '6', '2', '1');

UPDATE `rtshop`.`wallet` SET `user_score`='87000' WHERE `user_id`='89940';
UPDATE `rtshop`.`wallet` SET `user_score`='263500' WHERE `user_id`='47555';
UPDATE `rtshop`.`wallet` SET `user_score`='33000' WHERE `user_id`='95366';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('89940', '1562461200000', '1562461200000', '5000', '5000', '28', '87000', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('47555', '1562461200000', '1562461200000', '3000', '3000', '28', '263500', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `order_id`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('95366', '1562461200000', '1562461200000', '2000', '0', '2000', '28', '33000', '6', '2', '1');

UPDATE `rtshop`.`wallet` SET `user_score`='268500' WHERE `user_id`='47555';
UPDATE `rtshop`.`wallet` SET `user_score`='558172' WHERE `user_id`='11701';
UPDATE `rtshop`.`wallet` SET `user_score`='29185' WHERE `user_id`='12720';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `order_id`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('47555', '1562547600000', '1562547600000', '5000', '0', '5000', '28', '268500', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `order_id`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('11701', '1562547600000', '1562547600000', '3000', '0', '3000', '28', '558172', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `order_id`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('12720', '1562547600000', '1562547600000', '2000', '0', '2000', '28', '29185', '6', '2', '1');


UPDATE `rtshop`.`wallet` SET `user_score`='273500' WHERE `user_id`='47555';
UPDATE `rtshop`.`wallet` SET `user_score`='561172' WHERE `user_id`='11701';
UPDATE `rtshop`.`wallet` SET `user_score`='4588' WHERE `user_id`='236925';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('47555', '1562634000000', '1562634000000', '5000', '5000', '28', '273500', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('11701', '1562634000000', '1562634000000', '3000', '3000', '28', '561172', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('236925', '1562634000000', '1562634000000', '2000', '2000', '28', '4588', '6', '2', '1');


UPDATE `rtshop`.`wallet` SET `user_score`='278500' WHERE `user_id`='47555';
UPDATE `rtshop`.`wallet` SET `user_score`='96357' WHERE `user_id`='11700';
UPDATE `rtshop`.`wallet` SET `user_score`='563172' WHERE `user_id`='11701';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('47555', '1562720400000', '1562720400000', '5000', '5000', '28', '278500', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('11700', '1562720400000', '1562720400000', '3000', '3000', '28', '96357', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('11701', '1562720400000', '1562720400000', '2000', '2000', '28', '6', '2', '1');

UPDATE `rtshop`.`wallet` SET `user_score`='1177457' WHERE `user_id`='13';
UPDATE `rtshop`.`wallet` SET `user_score`='7588' WHERE `user_id`='236925';
UPDATE `rtshop`.`wallet` SET `user_score`='31185' WHERE `user_id`='12720';
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('13', '1562806800000', '1562806800000', '5000', '5000', '28', '1177457', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('236925', '1562806800000', '1562806800000', '3000', '3000', '28', '7588', '6', '2', '1');
INSERT INTO `rtshop`.`user_profit` (`user_id`, `ctime`, `mtime`, `amount`, `user_profit`, `order_type`, `user_balance`, `profit_type`, `money_type`, `is_account_entry`) VALUES ('12720', '1562806800000', '1562806800000', '2000', '2000', '28', '31185', '6', '2', '1');











