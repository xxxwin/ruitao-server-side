package com.ruitaowang.core.domain;

public class MeetingUserLink extends Page {
    /**
     *  主键,所属表字段为meeting_user_link.link_id
     */
    private Long linkId;

    /**
     *  会议标题,所属表字段为meeting_user_link.user_id
     */
    private Long userId;

    /**
     *  手机号,所属表字段为meeting_user_link.phone
     */
    private String phone;

    /**
     *  备注,所属表字段为meeting_user_link.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为meeting_user_link.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为meeting_user_link.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为meeting_user_link.rstatus
     */
    private Byte rstatus;

    /**
     *  会议价格 单位 ： 分,所属表字段为meeting_user_link.meeting_id
     */
    private Long meetingId;

    /**
     *  ,所属表字段为meeting_user_link.locked
     */
    private Boolean locked;

    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(Long meetingId) {
        this.meetingId = meetingId;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }
}