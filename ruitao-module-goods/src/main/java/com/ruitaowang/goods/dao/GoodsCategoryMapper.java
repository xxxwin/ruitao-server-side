package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsCategory;
import java.util.List;

public interface GoodsCategoryMapper {
    int insert(GoodsCategory goodsCategory);

    int deleteByPK(Long categoryId);

    int updateByPK(GoodsCategory goodsCategory);

    GoodsCategory selectByPK(Long categoryId);

    long count(GoodsCategory goodsCategory);

    List<GoodsCategory> select(GoodsCategory goodsCategory);
}