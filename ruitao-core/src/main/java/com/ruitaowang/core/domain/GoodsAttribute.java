package com.ruitaowang.core.domain;

public class GoodsAttribute extends Page {
    /**
     *  商品重量，最小单位g,所属表字段为goods_attribute.attr_id
     */
    private Long attrId;

    /**
     *  属性名称,所属表字段为goods_attribute.attr_name
     */
    private String attrName;

    /**
     *  类型编号,所属表字段为goods_attribute.type_id
     */
    private Long typeId;

    /**
     *  属性值,所属表字段为goods_attribute.attr_values
     */
    private String attrValues;

    /**
     *  1 多选
0 单选,所属表字段为goods_attribute.attr_type
     */
    private Byte attrType;

    /**
     *  排序,所属表字段为goods_attribute.attr_sort
     */
    private Integer attrSort;

    /**
     *  促销价格,所属表字段为goods_attribute.goods_promote_price
     */
    private Integer goodsPromotePrice;
    
    private GoodsType goodsType;
    
    /**
     *  创建时间,所属表字段为goods_attribute.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为goods_attribute.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常
1 删除,所属表字段为goods_attribute.rstatus
     */
    private Byte rstatus;

    public Long getAttrId() {
        return attrId;
    }

    public void setAttrId(Long attrId) {
        this.attrId = attrId;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName == null ? null : attrName.trim();
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getAttrValues() {
        return attrValues;
    }

    public void setAttrValues(String attrValues) {
        this.attrValues = attrValues == null ? null : attrValues.trim();
    }

    public Byte getAttrType() {
        return attrType;
    }

    public void setAttrType(Byte attrType) {
        this.attrType = attrType;
    }

    public Integer getAttrSort() {
        return attrSort;
    }

    public void setAttrSort(Integer attrSort) {
        this.attrSort = attrSort;
    }

    public Integer getGoodsPromotePrice() {
        return goodsPromotePrice;
    }

    public void setGoodsPromotePrice(Integer goodsPromotePrice) {
        this.goodsPromotePrice = goodsPromotePrice;
    }
    
    public GoodsType getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(GoodsType goodsType) {
		this.goodsType = goodsType;
	}

	public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}