package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.District;
import com.ruitaowang.core.domain.UserMembers;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.goods.dao.UserMembersMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class UserMembersService implements BaseService<UserMembers> {
    @Autowired
    private UserMembersMapper userMembersMapper;
    @Autowired
    private DistrictService districtService;

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Override
    public UserMembers insert(UserMembers userMembers) {
        Long time = System.currentTimeMillis();
        userMembers.setCtime(time);
        userMembers.setMtime(time);
        userMembersMapper.insert(userMembers);
        return userMembers;
    }

    @Override
    public int delete(Long id) {
        UserMembers userMembers = new UserMembers();
        userMembers.setUserId(id);
        userMembers.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userMembers.setMtime(time);
        return userMembersMapper.updateByPK(userMembers);
    }

    @Override
    public UserMembers update(UserMembers userMembers) {
        Long time = System.currentTimeMillis();
        userMembers.setMtime(time);
        userMembersMapper.updateByPK(userMembers);
        return userMembers;
    }

    @Override
    public UserMembers selectByPK(Long id) {
        return userMembersMapper.selectByPK(id);
    }

    @Override
    public List<UserMembers> select(UserMembers userMembers) {
        userMembers.setRstatus((byte) 0);
        return userMembersMapper.select(userMembers);
    }

    public List<UserMembers> selectForPage(UserMembers userMembers) {
        userMembers.setRstatus((byte) 0);
        userMembers.setRecords(userMembersMapper.count(userMembers));
        return userMembersMapper.select(userMembers);
    }

    public void setUserMembersForKSLimit(int number, long userId) {
        //会员期限 user members
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 0);
    }

    public void setUserMembersForKZLimit(int number, long userId) {
        //会员期限 user members
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 1);
    }
    public void setUserMembersForKZ1Limit(int number, long userId) {
        //会员期限 user members
        //1 计算会员增加的天数 一个月
        int days = number * 30;
        setUserMembersLimit(days, userId, (byte) 1);
    }
    public void setUserMembersForKZ3Limit(int number, long userId) {
        //会员期限 user members
        //1 计算会员增加的天数 一季度
        int days = number * 90;
        setUserMembersLimit(days, userId, (byte) 1);
    }
    public void setUserMembersForKZ6Limit(int number, long userId) {
        //会员期限 user members
        //1 计算会员增加的天数 半年
        int days = number * 180;
        setUserMembersLimit(days, userId, (byte) 1);
    }

    public void setUserMembersForKDLimit(int number, long userId) {//一年
        //会员期限 user members
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 2);
    }
    public void setUserMembersForKLLimit(int number, long userId) {//一年
        //会员期限 user members
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 3);
    }
    public void setUserMembersForKXLimit(int number, long userId) {//一年
        //会员期限 user members
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 4);
    }
    public void setUserMembersForKQLimit(int number, long userId) {
        //会员期限 user members
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 5);
    }
    public void setUserMembersForKFLimit(int number, long userId) {
        //会员期限 快飞
        //1 计算会员增加的天数
        int days = number * 365 * 3;//类似终身
        setUserMembersLimit(days, userId, (byte) 6);
    }
    public void setUserMembersForKTLimit(int number, long userId) {
        //会员期限 快推
        //1 计算会员增加的天数
        int days = number * 365 * 3;
        setUserMembersLimit(days, userId, (byte) 7);
    }
    public void setUserMembersForHDLimit(int number, long userId) {
        //会员期限 云动
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 9);
    }
    public void setUserMembersForKZZLimit(int number, long userId) {
        //会员期限 user members
        //1 计算会员增加的天数
        int days = number * 365;
        setUserMembersLimit(days, userId, (byte) 8);

        setUserMembersLimit(days, userId, (byte) 0);
        setUserMembersLimit(days, userId, (byte) 1);
        setUserMembersLimit(days, userId, (byte) 3);
        setUserMembersLimit(days, userId, (byte) 4);
    }

    public void setUserMembersLimit(int days, long userId, Byte memberType) {
        //会员期限 user members
        //1 计算会员增加的天数
        UserMembers userMembers = selectByUserIdForOnly(userId, memberType);
        String currentTime = TimeUtils.getCurrentDatetime(TimeUtils.TimeFormat.SHORT_DATE_PATTERN_LINE);
        if(ObjectUtils.isEmpty(userMembers)){
            userMembers = new UserMembers();
            userMembers.setUserId(userId);
            userMembers.setMemberType(memberType);
            userMembers.setEndTime(TimeUtils.getDateAfterNDays(currentTime, days));
            this.insert(userMembers);
        }else{
            if(Integer.valueOf(userMembers.getEndTime().replace("-", "")) >= Integer.valueOf(currentTime.replace("-", ""))){
                userMembers.setEndTime(TimeUtils.getDateAfterNDays(userMembers.getEndTime(), days));
                this.update(userMembers);
            }else{
                userMembers.setEndTime(TimeUtils.getDateAfterNDays(currentTime, days));
                this.update(userMembers);
            }
        }
    }

    public  boolean getMemberLimit(long userId) {//判断是否在会员期内
        UserMembers where = new UserMembers();
        where.setUserId(userId);
        List<UserMembers> list = select(where);
        String currentTime = TimeUtils.getCurrentDatetime(TimeUtils.TimeFormat.SHORT_DATE_PATTERN_LINE);
        if(ObjectUtils.isEmpty(list)){
            return false;
        }else{//check every member limit
            return list.stream().anyMatch(item -> Integer.valueOf(item.getEndTime().replace("-", "")) >= Integer.valueOf(currentTime.replace("-", "")));
        }
    }
    public String getMemberForKSLimit(long userId) {//member_type 0 普通会员 快省
        UserMembers userMembers = selectMemberForKSByUserId(userId);//
        String currentTime = TimeUtils.getCurrentDatetime(TimeUtils.TimeFormat.SHORT_DATE_PATTERN_LINE);
        if(ObjectUtils.isEmpty(userMembers)){
            return "";
        }else{
            if(Integer.valueOf(userMembers.getEndTime().replace("-", "")) >= Integer.valueOf(currentTime.replace("-", ""))){
                return  userMembers.getEndTime();
            }
        }
        return "";
    }
    public String getMemberForKZLimit(long userId) {//member_type 1 快转会员
        UserMembers userMembers = selectMemberForKZByUserId(userId);//
        String currentTime = TimeUtils.getCurrentDatetime(TimeUtils.TimeFormat.SHORT_DATE_PATTERN_LINE);
        if(ObjectUtils.isEmpty(userMembers)){
            return "-2";
        }else{
            if(Integer.valueOf(userMembers.getEndTime().replace("-", "")) >= Integer.valueOf(currentTime.replace("-", "")) || userMembers.getPermanent() == 1){
                return  userMembers.getUserId().toString();
            }else {
                return  "-1";
            }
        }
    }
    public String getMemberForKTLimitV2(long userId, Long districtId) {
        District district = districtService.selectByPK(districtId);
        String currentTime = TimeUtils.getCurrentDatetime(TimeUtils.TimeFormat.SHORT_DATE_PATTERN_LINE);
        if(ObjectUtils.isEmpty(district)){
            return "";
        }
        if(district.getCityUserId() != userId){
            return "";
        }
        if(Integer.valueOf(district.getInitial().replace("-", "")) >= Integer.valueOf(currentTime.replace("-", ""))){
            return  district.getInitial();
        }
        return "";
    }
    public UserMembers selectMemberForKSByUserId(Long userId) {
        return selectByUserId(userId, (byte) 0);
    }
    public UserMembers selectMemberForKZByUserId(Long userId) {
        return selectByUserId(userId, (byte) 1);
    }
    public UserMembers selectMemberForKDByUserId(Long userId) {
        return selectByUserId(userId, (byte) 2);
    }
    public UserMembers selectMemberForKQByUserId(Long userId) {
        return selectByUserId(userId, (byte) 5);
    }
    public UserMembers selectMemberForHDByUserId(Long userId) {
        return selectByUserId(userId, (byte) 9);
    }
    public UserMembers selectMemberForKFByUserId(Long userId) {
        return selectByUserId(userId, (byte) 6);
    }
    public UserMembers selectMemberForKTByUserId(Long userId) {
        return selectByUserId(userId, (byte) 7);
    }

    public UserMembers selectByUserId(Long userId, Byte memberType) {
        UserMembers userMembers = new UserMembers();//
        userMembers.setUserId(userId);
        userMembers.setRstatus((byte)0);
        userMembers.setMemberType(memberType);
        List<UserMembers> list = userMembersMapper.select(userMembers);
        if(ObjectUtils.isEmpty(list)){
            return null;
        }
        return list.get(0);
    }

    public UserMembers selectByUserIdForOnly(Long userId, Byte memberType) {
        UserMembers userMembers = new UserMembers();//
        userMembers.setUserId(userId);
        userMembers.setRstatus((byte)0);
        userMembers.setMemberType(memberType);
        List<UserMembers> list = userMembersMapper.select(userMembers);
        if(ObjectUtils.isEmpty(list)){
            return null;
        }
        return  list.get(0);
    }


}
