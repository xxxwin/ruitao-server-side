package com.ruitaowang.core.domain;

public class CompanyType extends Page {
    /**
     * ,所属表字段为company_type.id
     */
    private Long id;

    /**
     * ,所属表字段为company_type.name
     */
    private String name;

    /**
     * ,所属表字段为company_type.Industry_id
     */
    private Long industryId;

    /**
     * ,所属表字段为company_type.rstatus
     */
    private Byte rstatus;

    /**
     * ,所属表字段为company_type.ctime
     */
    private Long ctime;

    /**
     * ,所属表字段为company_type.mtime
     */
    private Long mtime;

    /**
     * ,所属表字段为company_type.remark
     */
    private String remark;

    /**
     * ,所属表字段为company_type.img
     */
    private String img;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }
}
