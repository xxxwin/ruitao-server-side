/**
 * 
 */

var Config = {
    selectUrl: '/api/dinner/orderPeriod'
};

$(document).ready(function () {
	
    $.jgrid.defaults.styleUI = "Bootstrap";
    var companyId = $("#companyId").val();
    var beginDate = $("#beginDate").val();
    var endDate = $("#endDate").val();
    
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        postData:{companyId:companyId,beginDate:beginDate,endDate:endDate},
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","订单号", "座号","人数","点菜","应支付", "是否支付","备注","发票抬头","创建时间"],
        colModel: [{
            name: "id",
            index: "id",
            editable: false,
            width: 60,
            sorttype: "int",
            search: false
        }, {
            name: "orderNo",
            index: "orderNo",
            editable: false,
            width: 160,
            search: false
        },{
            name: "tableNo",
            index: "tableNo",
            editable: false,
            width: 60
        }, {
            name: "cusNum",
            index: "cusNum",
            editable: false,
            width: 60
        },{
            name: "dishes",
            index: "dishes",
            width: 200,
            sortable: false
        },{
            name: "totalPrice",
            index: "totalPrice",
            editable: false,
            width: 80
        }, {
            name: "payDesc",
            index: "payDesc",
            editable: false,
            width: 60
        }, {
            name: "remark",
            index: "remark",
            editable: false,
            width: 200
        }, {
            name: "fpHead",
            index: "fpHead",
            editable: false,
            width: 200
        }, {
            name: "formatCtime",
            index: "formatCtime",
            editable: false,
            width: 200
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "activityId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete:function(){  }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});
    
    function operateFmatter(cellvalue, options, rowObject){
    	var html = "<a href='#' style='color:#f60' onclick='print1("+options.rowId+")' >打印</a>";
    	return html;
    }

    $("#btn_back").click(function(){
    	document.location = '/web/dinner_statis.html';
    }); 
});

