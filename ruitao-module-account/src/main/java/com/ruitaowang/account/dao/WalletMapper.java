package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.Wallet;
import java.util.List;

public interface WalletMapper {
    int insert(Wallet wallet);

    int deleteByPK(Long userId);

    int updateByPK(Wallet wallet);

    Wallet selectByPK(Long userId);

    long count(Wallet wallet);

    List<Wallet> select(Wallet wallet);
}