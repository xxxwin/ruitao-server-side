var Config = {
    updateUrl: '/api/admin/categories/update',
    deleteUrl: '/api/admin/userHistoricalTraceList/companyBackground/delete',
    selectUrl: '/api/admin/userHistoricalTraceList/companyBackground/Page'
};
$(document).ready(function () {
    // $.jgrid.defaults.styleUI = "Bootstrap";
    // $.ajax(
    //     {
    //         url: Config.selectUrl,
    //         type: "GET",
    //         dataType: "json",
    //         async: false,
    //         success: function (data, textStatus, jqXHR) {
    //
    //             //add operate for data
    //             if(data && data.data && data.data.length>0){
    //                 for (var i=0;i<data.data.length;i++){
    //                     var id=data.data[i].categoryId;
    //                     data.data[i].id=data.data[i].categoryId;
    //                     var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/category/update/'+id+'" data-index="0">编辑</a>';
    //                     html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + ","+  (i+1) +")' >删除</a>";
    //                     data.data[i].operate = html;
    //                 }
    //             }
    //             $("#userHistoricalTrance_list").jqGrid({
    //                 datatype: "local",
    //                 data: data.data,
    //                 height: 450,
    //                 autowidth: true,
    //                 shrinkToFit: true,
    //                 rowNum: 12,
    //                 colNames: ["编号", "用户名称", "商家名称", "第一次浏览时间", "最后修改时间", "操作"],
    //                 colModel: [{
    //                     name: "id",
    //                     index: "id",
    //                     editable: false,
    //                     width: 60,
    //                     sorttype: "int",
    //                     search: true
    //                 }, {
    //                     name: "nickname",
    //                     index: "nickname",
    //                     editable: true,
    //                     width: 100
    //                 }, {
    //                     name: "companyName",
    //                     index: "companyName",
    //                     editable: true,
    //                     width: 100,
    //                     sorttype: "int"
    //                 }, {
    //                     name: "ctime",
    //                     index: "ctime",
    //                     editable: true,
    //                     width: 100
    //                 }, {
    //                     name: "mtime",
    //                     index: "mtime",
    //                     editable: true,
    //                     width: 100,
    //                 }, {
    //                     name: "operate",
    //                     index: "operate",
    //                     width: 100,
    //                     sortable: false
    //                 }],
    //                 pager: "#pager_userHistoricalTrance_list",
    //                 viewrecords: true,
    //                 hidegrid: false,
    //                 gridComplete:function(){  }
    //             });
    //             jQuery("#userHistoricalTrance_list").jqGrid('navGrid', '#pager_userHistoricalTrance_list',{ add: false, edit: false, del: false });
    //             // jQuery('#goods_list').trigger('reloadGrid');
    //             $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#goods_list").setGridWidth(width);});
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             console.error(errorThrown);
    //         }
    //     });

    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#userHistoricalTrance_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "用户名称", "商家名称", "第一次浏览时间", "最后修改时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            editable: false,
            width: 60,
            sorttype: "int",
            search: true
        }, {
            name: "nickname",
            index: "nickname",
            editable: true,
            width: 100
        }, {
            name: "companyName",
            index: "companyName",
            editable: true,
            width: 100,
            sorttype: "int"
        }, {
            name: "ctime",
            index: "ctime",
            editable: true,
            width: 100
        }, {
            name: "mtime",
            index: "mtime",
            editable: true,
            width: 100,
        }, {
            name: "operate",
            index: "operate",
            width: 100,
            sortable: false
        }],
        pager: "#pager_userHistoricalTrance_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#userHistoricalTrance_list").jqGrid('navGrid', '#pager_userHistoricalTrance_list', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#userHistoricalTrance_list").setGridWidth(width);
    });
});

function Delete(id) {
    bootbox.confirm({size:"small",message:"确认要删除id为"+id+"的这条数据吗?",callback: function (result) {
            if (result) {
                updateDialog("DELETE", Config.deleteUrl + '/' + id);
                jQuery("#userHistoricalTrance_list").jqGrid('delRowData', id);
            }
        }
    });
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#userHistoricalTrance_list").trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}

function dataWrapper() {
    var ids = jQuery("#userHistoricalTrance_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#userHistoricalTrance_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        dataFromTheRow.mtime = new Date(dataFromTheRow.mtime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        jQuery("#userHistoricalTrance_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
            mtime:dataFromTheRow.mtime,
        });
    }
}