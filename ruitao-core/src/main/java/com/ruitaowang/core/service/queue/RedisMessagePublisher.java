package com.ruitaowang.core.service.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

/**
 * Created by neal on 12/09/2017.
 */
//@Service
public class RedisMessagePublisher implements MessagePublisher {
//    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;
//    @Autowired
    private ChannelTopic channelTopic;

    public RedisMessagePublisher(final RedisTemplate<Object, Object> redisTemplate, final ChannelTopic topic) {
        this.redisTemplate = redisTemplate;
        this.channelTopic = topic;
    }

    public void publish(final String message) {
        redisTemplate.convertAndSend(channelTopic.getTopic(), message);
    }
}
