#!/usr/bin/env bash

workDir=/home/ruitao/server/source/ruitao-server-side

#activeFile1=$workDir/ruitao-rest-mobile/src/main/resources/application.properties

activeFile2=$workDir/ruitao-admin/src/main/resources/application.properties
#
#
#echo 'Before ...'
#
#cat $activeFile1
#
#cat $activeFile2
#

#sed -i 's/dev/live/1' $activeFile1

sed -i 's/dev/live/1' $activeFile2

echo "Live ..."

#cat $activeFile1

cat $activeFile2

/home/ruitao/mvn/bin/mvn clean install -DskipTests


echo 'After ...'

#sed -i 's/live/dev/1' $activeFile1

sed -i 's/live/dev/1' $activeFile2

#cat $activeFile1

cat $activeFile2

sudo /usr/bin/cp /home/ruitao/server/source/ruitao-server-side/ruitao-admin/target/ruitao-admin-1.0.0.war /home/ruitao/ROOT.war
scp /home/ruitao/server/source/ruitao-server-side/ruitao-admin/target/ruitao-admin-1.0.0.war ruitao@10.27.143.116:/home/ruitao/ROOT.war