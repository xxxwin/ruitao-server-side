package com.ruitaowang.core.utils.aliyun;

import com.aliyun.oss.OSSClient;
import com.ruitaowang.core.utils.PathFormat;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by neal on 12/26/16.
 */
public class OSSHelper {
	
//	@Value("${oss.endpoint}")
//    private static String endpoint;
	
    public static void uploadToOSS() throws Exception {
        String localFile = "/Users/neal/Desktop/WechatIMG3.jpeg";
        InputStream inputStream = new FileInputStream(localFile);
        String filePath = "test/WechatIMG3.jpeg";
        uploadToOSS(inputStream, filePath);
    }

    public static void uploadToOSS(InputStream inputStream, String filePath) throws Exception {
        // endpoint以杭州为例，其它region请按实际情况填写
//        String endpoint = "http://oss-cn-shanghai-internal.aliyuncs.com";
        String endpoint = "http://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = AliyunConfig.accessKeyId;
        String accessKeySecret = AliyunConfig.accessKeySecret;
        System.out.println(endpoint);
        if(StringUtils.isEmpty(endpoint)){
        	endpoint = "http://oss-cn-shanghai-internal.aliyuncs.com";
        }
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        // 上传文件流
        ossClient.putObject(AliyunConfig.bucketName, filePath, inputStream);
        // 关闭client
        ossClient.shutdown();
        inputStream.close();
    }

    public static String uploadToOSSV2(InputStream inputStream, String filePath) throws Exception {
        // endpoint
        String endpoint = "http://oss-cn-shanghai.aliyuncs.com";
        String accessKeyId = AliyunConfig.accessKeyId;
        String accessKeySecret = AliyunConfig.accessKeySecret;
        // 创建OSSClient实例
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        // 上传文件流
        ossClient.putObject(AliyunConfig.bucketName, filePath, inputStream);
        // 关闭client
        ossClient.shutdown();
        return filePath;
    }

    public static String uploadToOSSV2(String url, String suffix, String ext) throws Exception {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        return uploadToOSSV2(response.body().byteStream(), PathFormat.filePath(suffix, ext));
    }

    public static String uploadImageToOSS(String url, String suffix) throws Exception {
        return uploadToOSSV2(url, suffix, ".jpg");
    }
    public static String uploadVideoToOSS(String url, String suffix) throws Exception {
        return uploadToOSSV2(url, suffix, ".mp4");
    }


}
