/**
 * 
 */

var Config = {
    selectUrl: '/api/dinner/statis',
};

$(document).ready(function () {
	
	$(".datepicker").datepicker({
        language: "zh-CN",
        autoclose: true,//选中之后自动隐藏日期选择框
        clearBtn: true,//清除按钮
        todayBtn: true,//今日按钮
        format: "yyyymmdd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
	
    $.jgrid.defaults.styleUI = "Bootstrap";

    $("#data_list").jqGrid({
    	datatype: "json",
        url: Config.selectUrl,
        mtype : "post",
        postData:{},
        height: 250,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: [ "公司", "订单量", "总收入","返回红包","操作"],
        colModel: [
        {
            name: "companyName",
            index: "companyName",
            editable: false,
            width: 200
        }, {
            name: "totalCount",
            index: "totalCount",
            editable: false,
            width: 300
        }, {
            name: "totalPayYuan",
            index: "totalPayYuan",
            editable: false,
            width: 200
        },{
            name: "totalScore",
            index: "totalScore",
            editable: false,
            width: 400
        }, {
        name: "operate",
        index: "operate",
        width: 100,
        formatter:fmtOper,
        sortable: false
    }],
    	rownumbers:true,
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "activityId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete:function(){  }
    });
    $("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});
    
    $("#isearch").click(function(){
    	var begin = $("#beginDate").val();
    	var end = $("#endDate").val();
    	var params = {beginDate:begin, endDate:end};
    	$("#data_list").jqGrid('setGridParam',{postData:params});
    	setTimeout(function(){$("#data_list").trigger("reloadGrid");},2000);
    }); 
});

function fmtOper(cellvalue, options, rowObject){
	var params = 'companyId='+rowObject.companyId+'&beginDate='+$("#beginDate").val()+'&endDate='+$("#endDate").val();
	var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/dinner_statis_order?'+params+'" data-index="0">详情</a>';
    return html
} 
