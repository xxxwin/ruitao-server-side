package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Comment;

import java.util.List;

public interface CommentAdMapper {

    long count(Comment comment);

    List<Comment> select(Comment comment);

    List<Comment> selectByTwo(Comment comment);

    long selectByTwoCount(Comment comment);
}