package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.MeetingUser;
import com.ruitaowang.core.domain.Meetings;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.MeetingService;
import com.ruitaowang.goods.service.MeetingUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;
import java.util.List;

/**
 *
 * 信息
 */
@RestController
public class PageMeetingController extends BaseController {

    @Autowired
    private MeetingService meetingService;
    @Autowired
    private MeetingUserService meetingUserService;

    /**
     * 更新
     */
    @RequestMapping(value = {"/meeting/update/{meetingId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("meetingId") Long meetingId, Model model) {
        Meetings meeting = meetingService.selectByPK(meetingId);
        model.addAttribute("meeting", meeting);
        return new ModelAndView("web/meeting_update");
    }
    /**
     * 参会人员名单
     */
    @RequestMapping(value = {"/meeting/users/{meetingId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView meetingUsers(@PathVariable("meetingId") Long meetingId, Model model) {
        model.addAttribute("meetingId", meetingId);
        return new ModelAndView("web/meeting_user_list");
    }

}