/**
 *
 * Create by murong on 2018/5/14
 */

var config={
    selectUrl: '/api/admin/comment/page',
    deleteUrl: '/api/wap/comment',
    updateUrlForCellEdit: '/api/admin/company/cellEdit',
    // selectTypeUrl:'/api/admin/type/selectByIndustry/'
};

$(document).ready(function () {
    $.jgrid.defaults.styleUI="Bootstrap";
    $("#comment_list").jqGrid({
        datatype:"json",
        url:config.selectUrl,
        height:450,
        autowidth:true,
        shrinkTofit:true,
        rowNum:12,
        colNames:["编号","评论内容","商家名","产品名","用户名","备注","创建时间","操作"],
        colModel:[{
            name:"id",
            index:"id",
            sorttype:"int",
            authwidth:true,
            search:true,
            width:50
        },{
            name:"commentContent",
            index:"commentContent",
            autowidth:true,
            sortable: true,
            width:250
        },{
            name:"companyId",
            index:"companyId",
            autowidth:true,
            sortable: true,
            width:100,
            formatter:companyformatter,

        },{
            name:"goodsId",
            index:"goodsId",
            autowidth:true,
            sortable: true,
            width:150,
            formatter:goodsformatter
        },{
            name:"userId",
            index:"userId",
            autowidth:true,
            sortable: true,
            formatter:userformatter
        },{
            name:"remark",
            index:"remark",
            autowidth:true
        },{
            name: "ctime",
            index: "ctime",
            sortable: true,
        },{
            name: "operate",
            index: "operate",
            autowidth: true,
            sortable: false
        }],
        celledit:true,
        cellsubmit: 'remote',
        cellurl: '/api/admin/company/cellEdit',
        pager: "#pager_comment_list",
        jsonReader: {
            root: "data", "currPage": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });

    jQuery("#comment_list").jqGrid('navGrid',"#pager_comment_list",{
        add: false, edit: false, del: false
    });

    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#comment_list").setGridWidth(width);
    });
    $("#refresh_comment_list").click(function () {

        var curpagenum =$("#input_pager_comment_list input").val();
        $("#jqGrid").jqGrid('clearGridData');
        jQuery("#comment_list").jqGrid('setGridParam',{
            url:config.selectUrl,
            datatype:'json',
            page:Number(curpagenum)}).trigger("reloadGrid");
    });

    $("#perform_search").click(function () {
        var postdata = $("#comment_list").jqGrid('getGridParam', 'postData');
        postdata._search = true;
        postdata.comment_content = $('#commentContent').val();
        var page =$("#input_pager_comment_list input").val();
        jQuery("#comment_list").trigger("reloadGrid", [{page: page}]);
    });

});

function Delete(id) {
    bootbox.confirm({size:"small",message:"确认要删除id为"+id+"的这条数据吗?",callback: function (result) {
            if (result) {
                updateDialog("DELETE", config.deleteUrl + '/' + id);
                jQuery("#comment_list").jqGrid('delRowData', id);
            }
        }
    });
}
function commentUpdate(id) {
    var dataFromTheRow = jQuery('#comment_list').jqGrid('getRowData', id);
    layer.open({
        title: ["评论详情","font-size:20px;"],
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:"/comment/update/" + id,
    })
}
function dataWrapper() {
    var ids = jQuery("#comment_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#comment_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = '<a class="J_men uItem" style="color:#f60;padding-right: 15px" onclick="commentUpdate(' + id + ')" data-index="0">编辑</a>';
        html += '<a style="color:#f60" onclick="Delete(' + id + ')" >删除</a>';
        // var avatar = "'";
        // if( dataFromTheRow.imgContent.indexOf('#')>0) {
        //     var imglist = dataFromTheRow.imgContent.split('#');
        //     if (imglist.length > 0) {
        //         var count = imglist.length;
        //         console.log(imglist);
        //         for (var i = 0; i < count; i++) {
        //
        //             avatar += '<img style="width: 50px; height: 50px;" src="';
        //             avatar += imglist[i];
        //             avatar += '"/>';
        //
        //             console.log(avatar);
        //
        //         }
        //     }
        //     avatar += "'";
        // }
          //avatar ='<img style="width: 50px; height: 50px;" src="http://wx.qlogo.cn/mmopen/Q3auHgzwzM7BZ39EBqwgJoB59DfoyEODDYJtahyUwP3VRuqsibFju16h2PGE8GgJ7og5YRexFP67R8nyoF9ZUySGvllkibOgCgaTunibXicExsY/0"><img style="width: 50px; height: 50px;" src="http://wx.qlogo.cn/mmopen/OdHqM4a3Qk7yvJSGEDBArpxicKKq4fwo20wnK6GVjYs6bykNzbtYfgriaFT32RxcJgXxGVKhmRaN2vleFFYJlkpEgicHfsqe1WU/0">'
            //console.log(avatar);

        jQuery("#comment_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime
        });
    }
}

function companyformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax(
        {
            url: "/api/admin/company/selectByPk/" + rowObject.companyId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                name = data.data.companyName;
            }
        });
    return name;
}
function goodsformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax(
        {
            url: "/api/goods/" + rowObject.goodsId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                name = data.data.goodsName;
            }
        });
    return name;
}
function userformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax(
        {
            url: "/api/admin/accunts/selectByPk/" + rowObject.userId,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                name = data.data.nickname;
            }

        });
    return name;
}
// function commentUpdate(id) {
//     layer.open({
//         title: ["商家类型修改","font-size:20px;"],
//         type: 2,
//         scrollbar: false,
//         shadeClose: true,
//         shade: 0.8,
//         area: ['100%', '100%'],
//         content:"/comment/update/" + id,
//     })
// }