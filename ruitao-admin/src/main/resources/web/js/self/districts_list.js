/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/districts',
    updateUrl: '/api/districts',
    deleteUrl: '/api/districts',
    selectUrl: '/api/districts/page'
};
$(document).ready(function () {
    getDistrictByParentId(0, "#province");
    $.ajax({
        url: Config.selectUrl,
        data: {parentid:0 , page: 1, rows: 1000},
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            refreshGrid(data.data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
});
$.jgrid.defaults.styleUI = "Bootstrap";
$("#province").change(function(){
    var id = $(this).val();
    $.ajax({
        url: Config.selectUrl,
        data: {parentid:id , page: 1, rows: 1000},
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            getDistrictByParentId(id, "#city");
            refreshGrid(data.data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
});
$("#city").change(function(){
    var id = $(this).val();

    $.ajax({
        url: Config.selectUrl,
        data: {parentid:id , page: 1, rows: 1000},
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            getDistrictByParentId(id, "#district");
            refreshGrid(data.data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
});
function getDistrictByParentId(id, selectId) {
    $.ajax({
        url: Config.selectUrl,
        data: {parentid:id, page: 1, rows: 1000},
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            var html = [];
            data.data.forEach(function(item){
                html.push('<option value="'+item.id+'">'+item.name+'</option>');
            });
            $(selectId).html(html.join(''));
         },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
}
function refreshGrid(results) {
    $("#data_list").jqGrid("clearGridData", true).trigger("reloadGrid");
    jQuery("#data_list")
        .jqGrid('setGridParam',
            {
                datatype: 'local',
                data:results
            })
        .trigger("reloadGrid");
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var item = jQuery("#data_list").jqGrid('getRowData', id);
        var html = "<a style='color:#f60' onclick='setPrice(" + id + ")'>设定</a>";
        if (item.cityUserId != "0"){
            html += '<a style="color:#f60;padding-right: 10px" href="/user_member_update/' + item.cityUserId + '" data-index="0">归属人</a>';
        }
        var price = '<input id="qz'+id+'" value='+(item.price!=0?item.price:0)+' />';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {used:item.used==1?"YES":"NO", price: price, operate: html});
    }
}
function setPrice(id){
    $.ajax({
        url: Config.updateUrl,
        data: {id:id, price: $("#qz"+id).val()},
        type: "PUT",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            toastr.success(":)~");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
}
var results = [];
$("#data_list").jqGrid({
    datatype: "local",
    data: results,
    height: 450,
    autowidth: true,
    shrinkToFit: true,
    rowNum: 1000,
    colNames: ["编号", "父编号", "名称", "后缀", "加盟费", "是否交费","归属人ID", "操作"],
    colModel: [{
        name: "id",
        index: "id",
        editable: false,
        width: 20,
        sorttype: "int",
        search: true
    }, {
        name: "parentid",
        index: "parentid",
        editable: false,
        width: 20,
        sorttype: "int",
        search: true
    }, {
        name: "name",
        index: "name",
        editable: true,
        width: 50
    }, {
        name: "suffix",
        index: "suffix",
        editable: true,
        width: 10,
    }, {
        name: "price",
        index: "price",
        editable: true,
        width: 20
    }, {
        name: "used",
        index: "used",
        editable: true,
        width: 10,
        editoptions:{value:"0:未交费;1:已缴费"},
        edittype:'select',
        formatter:'select'
    },{
        name: "cityUserId",
        index: "cityUserId",
        width: 10
    }, {
        name: "operate",
        index: "operate",
        width: 10,
        sortable: false
    }],
    pager: "#pager_data_list",
    jsonReader: {
        root: "data", "page": "page", total: "total",
        records: "records", repeatitems: false, id: "id"
    },
    viewrecords: true,
    hidegrid: false,
    gridComplete: function () {
        //wrapper operate
        dataWrapper();
    }
});