//package com.ruitaowang.admin.config;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.web.AuthenticationEntryPoint;
//import org.springframework.stereotype.Component;
//
//import com.alibaba.fastjson.JSON;
//import com.ruitaowang.core.web.MsgOut;
//
//@Component
//public class AjaxAuthenticationEntryPoint implements AuthenticationEntryPoint {
//
//	@Override
//	public void commence(HttpServletRequest request, HttpServletResponse response,
//			AuthenticationException authException) throws IOException, ServletException {
//
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json");
//        MsgOut out = MsgOut.error("未登录");
//        response.getWriter().write(JSON.toJSONString(out));
//	}
//
//}
