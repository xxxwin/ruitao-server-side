package com.ruitaowang.admin.view.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruitaowang.core.domain.GoodsAttribute;
import com.ruitaowang.core.domain.GoodsAttributeLink;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.GoodsAttributeLinkService;
import com.ruitaowang.goods.service.GoodsAttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Shaka on 2016/12/2.
 */
@RestController
public class APIGoodsAttributeLinkController extends BaseController {

    @Autowired
    private GoodsAttributeLinkService goodsAttributeLinkService;
    @Autowired
    private GoodsAttributeService goodsAttributeService;

    @RequestMapping("/api/goodsAttributeLink")
    public String list() {
        List<GoodsAttributeLink> list;
        GoodsAttributeLink goodsAttributeLink = new GoodsAttributeLink();
        list = goodsAttributeLinkService.select(goodsAttributeLink);
        MsgOut o = MsgOut.success(list);
        return this.renderJson(o);
    }

    @RequestMapping("/api/goodsAttributeLink/{goodsAttributeLinkeId}")
    public String list(@PathVariable("goodsAttributeLinkeId") Long goodsAttributeLinkeId) {
        MsgOut o = MsgOut.success(goodsAttributeLinkService.selectByPK(goodsAttributeLinkeId));
        return this.renderJson(o);
    }

    @RequestMapping("/api/goodsAttributeLink/goodsID/{goodsID}")
    public String listByGoodsID(@PathVariable("goodsID") Long goodsID) {
        GoodsAttributeLink goodsAttributeLink = new GoodsAttributeLink();
        goodsAttributeLink.setGoodsId(goodsID);
        goodsAttributeLink.setOrderBy("id");
        goodsAttributeLink.setRstatus((byte) 0);
        goodsAttributeLink.setAttrShow((byte) 1);
        MsgOut o = MsgOut.success(goodsAttributeLinkService.select(goodsAttributeLink));
        return this.renderJson(o);
    }


    @RequestMapping(value = "/api/goodsAttributeLink", method = RequestMethod.POST)
    public String create(String attrJson) {
        JSONArray objects = JSON.parseArray(attrJson);
        for (int i = 0; i < objects.size(); i++) {
            goodsAttributeLinkService.insert(JSON.parseObject(objects.get(i).toString(), GoodsAttributeLink.class));
        }
        MsgOut o = MsgOut.success();
        return this.renderJson(o);
    }
    @RequestMapping(value = "/api/wap/goodsAttributeLink/create", method = RequestMethod.POST)
    public String wapCreateGoodsAttributeLink(String attrJson) {
        JSONArray objects = JSON.parseArray(attrJson);
        for (int i = 0; i < objects.size(); i++) {
            goodsAttributeLinkService.insert(JSON.parseObject(objects.get(i).toString(), GoodsAttributeLink.class));
        }
        MsgOut o = MsgOut.success(objects);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/goodsAttributeLink", method = RequestMethod.PUT)
    public String update(String attrJson) {
        JSONArray objects = JSON.parseArray(attrJson);

        if (objects != null && objects.size() > 0) {
            GoodsAttributeLink tmpGoodsAttrLink = new GoodsAttributeLink();
            tmpGoodsAttrLink.setGoodsId(JSON.parseObject(objects.get(0).toString(), GoodsAttributeLink.class).getGoodsId());
            List<GoodsAttributeLink> goodsAttributeLinkList = goodsAttributeLinkService.selectAll(tmpGoodsAttrLink);
            if (goodsAttributeLinkList != null && goodsAttributeLinkList.size() > 0) {
                for (GoodsAttributeLink goodsAttributeLink : goodsAttributeLinkList) {
                    goodsAttributeLink.setRstatus((byte) 1);
                    goodsAttributeLinkService.update(goodsAttributeLink);
                }
            }

            for (int i = 0; i < objects.size(); i++) {
                GoodsAttributeLink goodsAttributeLink = JSON.parseObject(objects.get(i).toString(), GoodsAttributeLink.class);
                GoodsAttributeLink newGoodsAttrLink = new GoodsAttributeLink();

                newGoodsAttrLink.setGoodsId(goodsAttributeLink.getGoodsId());
                newGoodsAttrLink.setAttrId(goodsAttributeLink.getAttrId());
                newGoodsAttrLink.setAttrValue(goodsAttributeLink.getAttrValue());

                GoodsAttribute goodsAttribute = goodsAttributeService.selectByPK(goodsAttributeLink.getAttrId());
                List<GoodsAttributeLink> select = goodsAttributeLinkService.selectAll(newGoodsAttrLink);

                goodsAttributeLink.setRstatus((byte) 0);
                if (!ObjectUtils.isEmpty(select)) {
                    if (!ObjectUtils.isEmpty(goodsAttribute)) {
                        goodsAttributeLink.setAttrName(goodsAttribute.getAttrName());
                    }
                    if(goodsAttributeLink.getId() != null && goodsAttributeLink.getId()>0){
                        goodsAttributeLinkService.update(goodsAttributeLink);
                    }else{
                        goodsAttributeLinkService.updateNoId(goodsAttributeLink);
                    }

                } else {
                    if (!ObjectUtils.isEmpty(goodsAttribute)) {
                        goodsAttributeLink.setAttrName(goodsAttribute.getAttrName());
                    }
                    GoodsAttributeLink newGoodsAttrLink1 = new GoodsAttributeLink();
                    newGoodsAttrLink1.setGoodsId(goodsAttributeLink.getGoodsId());
                    newGoodsAttrLink1.setAttrId(goodsAttributeLink.getAttrId());
                    newGoodsAttrLink1.setAttrValue(goodsAttributeLink.getAttrValue());
                    List<GoodsAttributeLink> select1 = goodsAttributeLinkService.selectAll(newGoodsAttrLink);
                    if(ObjectUtils.isEmpty(select1)){
                        goodsAttributeLinkService.insert(goodsAttributeLink);
                    }else{
                        goodsAttributeLinkService.updateNoId(goodsAttributeLink);
                    }

                }
            }
        }
        MsgOut o = MsgOut.success();
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/goodsAttrLinkUpdateByGoodsId", method = RequestMethod.PUT)
    public String update(long goodsId) {
        GoodsAttributeLink tmpGoodsAttrLink = new GoodsAttributeLink();
        tmpGoodsAttrLink.setGoodsId(goodsId);
        tmpGoodsAttrLink.setRstatus((byte) 1);
        MsgOut o = MsgOut.success(goodsAttributeLinkService.updateNoId(tmpGoodsAttrLink));
        return this.renderJson(o);
    }


    @RequestMapping(value = "/api/goodsAttributeLink/{goodsAttributeLinkeId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("goodsAttributeLinkeId") Long goodsTypeId) {
        MsgOut o = MsgOut.success(goodsAttributeLinkService.delete(goodsTypeId));
        return this.renderJson(o);
    }


}
