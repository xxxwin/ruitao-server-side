package com.ruitaowang.core.domain;

public class UserHistoricalTraceVO extends UserHistoricalTrace {
    private String userName;

    private String nickname;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
