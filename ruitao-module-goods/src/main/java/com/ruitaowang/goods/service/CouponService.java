package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Coupon;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CouponService implements BaseService<Coupon>{

    @Autowired
    private CouponMapper couponMapper;


    @Override
    public Coupon insert(Coupon coupon) {
        Long time = System.currentTimeMillis();
        coupon.setCtime(time);
        coupon.setMtime(time);
        couponMapper.insert(coupon);
        return coupon;
    }

    @Override
    public int delete(Long id) {
        Coupon coupon = new Coupon();
        coupon.setId(id);
        coupon.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        coupon.setMtime(time);
        return couponMapper.updateByPK(coupon);
    }

    @Override
    public Coupon update(Coupon coupon) {
        Long time = System.currentTimeMillis();
        coupon.setMtime(time);
        couponMapper.updateByPK(coupon);
        return coupon;
    }

    @Override
    public Coupon selectByPK(Long id) {

        return couponMapper.selectByPK(id);
    }

    @Override
    public List<Coupon> select(Coupon coupon) {
        coupon.setRstatus((byte) 0);
        return couponMapper.select(coupon);
    }

    public List<Coupon> selectForPage(Coupon coupon) {
        coupon.setRstatus((byte) 0);
        coupon.setRecords(couponMapper.count(coupon));
        return couponMapper.select(coupon);
    }

    public String couponPrice(Long id){
        Coupon coupon=couponMapper.selectByPK(id);
        return "满："+coupon.getReach1()+"————"+"返："+coupon.getScore1();
    }
}
