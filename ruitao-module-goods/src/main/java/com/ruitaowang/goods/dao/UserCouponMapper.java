package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserCoupon;
import java.util.List;

public interface UserCouponMapper {
    int insert(UserCoupon userCoupon);

    int deleteByPK(Long id);

    int updateByPK(UserCoupon userCoupon);

    UserCoupon selectByPK(Long id);

    long count(UserCoupon userCoupon);

    List<UserCoupon> select(UserCoupon userCoupon);
}