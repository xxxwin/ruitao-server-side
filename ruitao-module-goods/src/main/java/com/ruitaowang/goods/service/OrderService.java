package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.domain.Order;
import com.ruitaowang.core.domain.OrderProd;
import com.ruitaowang.core.domain.OrderVO;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.OrderAdMapper;
import com.ruitaowang.goods.dao.OrderMapper;
import com.ruitaowang.goods.dao.OrderProdMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class OrderService implements BaseService<Order>{

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    OrderProdMapper orderProdMapper;
    @Autowired
    OrderAdMapper orderAdMapper;

    @Override
    public List<Order> select(Order order) {
        order.setRstatus((byte) 0);
        return orderMapper.select(order);
    }

    @Override
    public Order insert(Order order) {
        Long time = System.currentTimeMillis();
        order.setCtime(time);
        order.setMtime(time);
        orderMapper.insert(order);
        return order;
    }

    @Override
    public int delete(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        order.setMtime(time);
        return orderMapper.updateByPK(order);
    }

    @Override
    public Order update(Order order) {
        Long time = System.currentTimeMillis();
        order.setMtime(time);
        orderMapper.updateByPK(order);
        return order;
    }

    @Override
    public Order selectByPK(Long id) {
        Order order = orderMapper.selectByPK(id);
        return order;
    }

    public Order giveBackProfit(Long id) {
        Order order = orderMapper.selectByPK(id);
        OrderProd orderProd = new OrderProd();
        orderProd.setOrderId(order.getOrderId());
        List<OrderProd> orderProds = orderProdMapper.select(orderProd);
        //单笔满100返利
        return order;
    }


    public List<Order> searchForPage(Order order) {
        order.setRstatus((byte) 0);
        order.setRecords(orderMapper.count(order));
        return orderMapper.select(order);
    }
    public List<Order> selectOrderPage(Order order) {
        order.setRecords(orderAdMapper.selectForOrderCount(order));
        return orderAdMapper.selectForOrder(order);
//        List<Order> out;
//        order.setRstatus((byte) 0);
//        if (!ObjectUtils.isEmpty(order.getPayStatus())){
//            if (order.getPayStatus() == 0){
//                order.setRecords(orderAdMapper.selectForOrderCount(order));
//                out=orderAdMapper.selectForOrder(order);
//            }else {
//                order.setRecords(orderMapper.count(order));
//                out=orderMapper.select(order);
//            }
//        } else {
//            order.setRecords(orderMapper.count(order));
//            out=orderMapper.select(order);
//        }
//        return out;
    }

    public Long selectOrderCount(Order order){
        //order.setRstatus((byte) 0);
//        if (!ObjectUtils.isEmpty(order.getPayStatus())){
//            if (order.getPayStatus() == 0){
//                return orderAdMapper.selectForOrderCount(order);
//            }else {
//                return orderMapper.count(order);
//            }
//        } else {
//            return orderMapper.count(order);
//        }
        return orderAdMapper.selectForOrderCount(order);
    }

    public List<Order> selectForOrderCompany(OrderVO orderVO){
        orderVO.setRecords(orderAdMapper.selectForOrderCompanyCount(orderVO));
        return orderAdMapper.selectForOrderCompany(orderVO);
    }

    public Long selectForOrderCompanyCount(OrderVO orderVO){
        return orderAdMapper.selectForOrderCompanyCount(orderVO);
    }

    public List<Order> fjOrder(Order order) {
        order.setRecords(orderAdMapper.fjOrderCount(order));
        return orderAdMapper.fjOrder(order);
    }

}
