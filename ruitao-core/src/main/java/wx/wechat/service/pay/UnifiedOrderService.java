package wx.wechat.service.pay;

import org.dom4j.DocumentException;
import wx.wechat.common.Configure;
import wx.wechat.common.RandomStringGenerator;
import wx.wechat.service.WXService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UnifiedOrderService extends WXService {

    /**
     * @region 必填参数
     */
    /**
     * 商品描述,必填
     */
    private String body;

    /**
     * 商户订单号
     */
    private String out_trade_no;

    /**
     * 总金额
     */
    private Integer total_fee;

    /**
     * 终端IP
     */
    private String spbill_create_ip;

    /**
     * @region 选填参数, Builder模式
     */
    /**
     * 终端设备号(门店号或收银设备ID)，注意：PC网页或公众号内支付请传"WEB"
     */
    private String device_info = "WEB";

    /**
     * 商品详情
     */
    private String detail;

    /**
     * 附加数据
     */
    private String attach;

    /**
     * 货币类型
     */
    private String fee_type;

    /**
     * 交易起始时间
     */
    private String time_start;

    /**
     * 交易结束时间
     */
    private String time_expire;

    /**
     * 交易类型
     */
    private String trade_type = "APP";

    /**
     * 商品ID
     */
    private String product_id;

    /**
     * 指定支付方式
     */
    private String limit_pay;

    /**
     * 用户标识
     */
    private String openid;

    public UnifiedOrderService(String body, String out_trade_no, Integer total_fee,
                               String spbill_create_ip, String attach) {
        this.body = body;
        this.out_trade_no = out_trade_no;
        this.total_fee = total_fee;
        this.spbill_create_ip = spbill_create_ip;
        this.attach = attach;
    }

    /**
     * @return 返回结果
     * @function 发起交易类型为APP的预下单请求, 并且进行必要的参数检查
     */
    public Map<String, Object> appOrder() throws IOException, DocumentException {

        Map requestData = requestDataGenerator();

        requestData.put("trade_type", "APP");

        //调用请求并且返回参数
        return this.postByXML("https://api.mch.weixin.qq.com/pay/unifiedorder", requestData);
    }

    /**
     * @return
     * @function 利用JSAPI在微信公众号内下单
     * @url
     */
    public Map<String, Object> jsApiOrder(String openid) throws IOException, DocumentException {

        Map requestData = requestDataGenerator();

        //设置交易类型为JSAPI
        requestData.put("trade_type", "JSAPI");

        //设置当前用户的openid
        requestData.put("openid", openid);

        System.out.println(requestData);

        //调用请求并且返回参数
        return this.postByXML("https://api.mch.weixin.qq.com/pay/unifiedorder", requestData);
    }

    /**
     * @region 私有方法区域
     */
    /**
     * @return
     * @function 生成公共的请求数据, 包含公共字段
     */
    public Map<String, String> requestDataGenerator() {

        Map<String, String> requestData = new HashMap<>();

        requestData.put("appid", Configure.appID);

        requestData.put("mch_id", Configure.mchID);

        requestData.put("nonce_str", RandomStringGenerator.getRandomStringByLength(20));

        requestData.put("notify_url", Configure.wxpay_notify_url);

        requestData.put("out_trade_no", this.out_trade_no);

        requestData.put("body", body);

        requestData.put("total_fee", total_fee.toString());

        requestData.put("spbill_create_ip", spbill_create_ip);

        requestData.put("trade_type", trade_type);

        requestData.put("device_info", "WEB");

        requestData.put("attach", attach);

        return requestData;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public Integer getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(Integer total_fee) {
        this.total_fee = total_fee;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }

    public String getDevice_info() {
        return device_info;
    }

    public void setDevice_info(String device_info) {
        this.device_info = device_info;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getFee_type() {
        return fee_type;
    }

    public void setFee_type(String fee_type) {
        this.fee_type = fee_type;
    }

    public String getTime_start() {
        return time_start;
    }

    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }

    public String getTime_expire() {
        return time_expire;
    }

    public void setTime_expire(String time_expire) {
        this.time_expire = time_expire;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getLimit_pay() {
        return limit_pay;
    }

    public void setLimit_pay(String limit_pay) {
        this.limit_pay = limit_pay;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }
}
