package com.ruitaowang.core.domain;

public class Address extends Page {
    /**
     *  自增主键,所属表字段为address.address_id
     */
    private Long addressId;

    /**
     *  收货人姓名,所属表字段为address.consignee
     */
    private String consignee;

    /**
     *  联系方式,所属表字段为address.mobile
     */
    private String mobile;

    /**
     *  备注,所属表字段为address.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为address.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为address.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为address.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为address.city
     */
    private String city;

    /**
     *  ,所属表字段为address.province
     */
    private String province;

    /**
     *  ,所属表字段为address.district
     */
    private String district;

    /**
     *  详细地址,所属表字段为address.other
     */
    private String other;

    /**
     *  国家,所属表字段为address.country
     */
    private String country;

    /**
     *  用户编号,所属表字段为address.user_id
     */
    private Long userId;

    /**
     *  ,所属表字段为address.district_id
     */
    private Long districtId;

    /**
     *  默认,所属表字段为address.defaultt
     */
    private Byte defaultt;

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee == null ? null : consignee.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other == null ? null : other.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Byte getDefaultt() {
        return defaultt;
    }

    public void setDefaultt(Byte defaultt) {
        this.defaultt = defaultt;
    }
}