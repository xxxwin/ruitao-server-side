/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/meetings',
    updateUrl: '/api/meetings',
    deleteUrl: '/api/meetings',
    selectUrl: '/api/meetings'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "会议标题", "会议时间", "费用", "报名二维码", "签到二维码", "操作"],
        colModel: [{
            name: "meetingId",
            index: "meetingId",
            width: 15
        }, {
            name: "meetingTitle",
            index: "meetingTitle",
            width: 60
        }, {
            name: "meetingDate",
            index: "meetingDate",
            width: 50
        }, {
            name: "meetingPrice",
            index: "meetingPrice",
            width: 30
        }, {
            name: "qrSignUp",
            index: "qrSignUp",
            width: 30
        }, {
            name: "qrSignIn",
            index: "qrSignIn",
            width: 30
        }, {
            name: "operate",
            index: "operate",
            width: 40
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "meetingId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {
        add: false,
        edit: false,
        del: false,
        search: false,
        refresh: false
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

});

function auditReward(id) {
    $.ajax(
    {
        url: Config.updateUrl ,
        type: "PUT",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data && data.type == "SUCCESS"){
                jQuery("#data_list").jqGrid('delRowData', id);
                jQuery("#data_list").trigger("reloadGrid");
            }else{
                toastr.error(data.msg);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络错误");
        }
    });
}

function Delete(id) {
    $.ajax(
    {
        url: Config.deleteUrl+"/"+id,
        type: "DELETE",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data && data.type == "SUCCESS"){
                jQuery("#data_list").jqGrid('delRowData', id);
                jQuery("#data_list").trigger("reloadGrid");
            }else{
                toastr.error(data.msg);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络错误");
        }
    });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', ids[i]);
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/meeting/update/' + ids[i] + '" data-index="0">编辑</a>';
            html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/meeting/users/' + ids[i] + '" data-index="0">统计</a>';
            html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="javascript:Delete(' + ids[i] + ')" data-index="0">删除</a>';
        var qrSignUp = '<a target="_blank" href="'+dataFromTheRow.qrSignUp+'">下载</a>';
        var qrSignIn = '<a target="_blank" href="'+dataFromTheRow.qrSignIn+'">下载</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {
            operate: html,
            qrSignUp: qrSignUp,
            qrSignIn: qrSignIn,
            meetingPrice: (dataFromTheRow.meetingPrice/100).toFixed(2)
        });
    }
}
