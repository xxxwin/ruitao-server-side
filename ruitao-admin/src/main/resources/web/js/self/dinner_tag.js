var Config = {
    createUrl: '/api/dinner/tag',
    updateUrl: '/api/dinner/tag',
    deleteUrl: '/api/dinner/tag',
    selectUrl: '/api/dinner/dishTags'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";

    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ['编号', '标签名称', '创建时间', '修改时间', '操作'],
        colModel: [
            {
                name: "id",
                index: "id",
                editable: false,
                width: 0,
                search: false
            },
            {
                name: "name",
                index: "name",
                editable: false,
                width: 100,
                search: false
            },
            {
                name: "ctime",
                index: "ctime",
                editable: false,
                width: 100,
                formatter: fmtTime,
                search: false
            },
            {
                name: "mtime",
                index: "mtime",
                editable: false,
                width: 100,
                formatter: fmtTime,
                search: false
            },
            {
                name: "operate",
                index: "operate",
                editable: false,
                width: 100,
                formatter: fmtOper,
                search: false
            },
        ],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "activityId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

    //create menu
    $('#submitForSave').click(function () {
        $("#ajaxform").validate({
            rules: {
                name: {
                    required: true,
                }
            }
        });
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url: Config.createUrl,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                        if (data && data.type == 'SUCCESS') {
                            $("#data_list").trigger("reloadGrid");
                            $('#ajaxform')[0].reset();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});

function fmtTime(cellvalue, options, rowObject) {
    var time = new Date(cellvalue);
    return time.yyyymmddhhmmss();
}

function fmtOper(cellvalue, options, rowObject) {
    var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/dinner_tag/' + rowObject.id + '" data-index="0">编辑</a>';
    html += "<a href='#'   style='color:#f60' onclick='Delete(" + rowObject.id + ")' >删除</a>";
    return html
}

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    $("#data_list").trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown);
            }
        });
}