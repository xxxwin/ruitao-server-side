package com.ruitaowang.core.domain;

public class CompanyProfit extends Page {
    /**
     *  主键,所属表字段为company_profit.profit_id
     */
    private Long profitId;

    /**
     *  公司编码,所属表字段为company_profit.company_id
     */
    private Long companyId;

    /**
     *  备注,所属表字段为company_profit.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为company_profit.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为company_profit.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为company_profit.rstatus
     */
    private Byte rstatus;

    /**
     *  订单金额,所属表字段为company_profit.amount
     */
    private Integer amount;

    /**
     *  ,所属表字段为company_profit.order_id
     */
    private Long orderId;

    /**
     *  收益金额,所属表字段为company_profit.company_profit
     */
    private Integer companyProfit;

    /**
     *  ,所属表字段为company_profit.consumer_id
     */
    private Long consumerId;

    /**
     *  -2 退款
-1 提现
0 商
1 地面店扫码订单返利
2 商城微商订单返利
3 地面店扫码订单返积分
4 商城微商订单返积分
5 点餐送积分
6 点餐奖励
7 快转奖励
8 快省奖励
9 快签奖励
10 快飞奖励
11 快推奖励
12 快点奖励
13 快销奖励
14 快赚奖励,所属表字段为company_profit.order_type
     */
    private Byte orderType;

    /**
     *  订单唯一编号,所属表字段为company_profit.order_sn
     */
    private String orderSn;

    /**
     *  个人账号余额,所属表字段为company_profit.company_balance
     */
    private Integer companyBalance;

    /**
     *  手续费,所属表字段为company_profit.money_reduce
     */
    private Integer moneyReduce;

    public Integer getMoneyReduce() {
        return moneyReduce;
    }

    public void setMoneyReduce(Integer moneyReduce) {
        this.moneyReduce = moneyReduce;
    }

    public Long getProfitId() {
        return profitId;
    }

    public void setProfitId(Long profitId) {
        this.profitId = profitId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getCompanyProfit() {
        return companyProfit;
    }

    public void setCompanyProfit(Integer companyProfit) {
        this.companyProfit = companyProfit;
    }

    public Long getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(Long consumerId) {
        this.consumerId = consumerId;
    }

    public Byte getOrderType() {
        return orderType;
    }

    public void setOrderType(Byte orderType) {
        this.orderType = orderType;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn == null ? null : orderSn.trim();
    }

    public Integer getCompanyBalance() {
        return companyBalance;
    }

    public void setCompanyBalance(Integer companyBalance) {
        this.companyBalance = companyBalance;
    }
}