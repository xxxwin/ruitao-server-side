package com.ruitaowang.core.domain;

public class Company extends Page {
    /**
     *  自增主键,所属表字段为company.company_id
     */
    private Long companyId;

    /**
     *  name,所属表字段为company.company_name
     */
    private String companyName;

    /**
     *  负责人,所属表字段为company.linkman
     */
    private String linkman;

    /**
     *  备注,所属表字段为company.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为company.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为company.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为company.rstatus
     */
    private Byte rstatus;

    /**
     *  联系电话,所属表字段为company.mobile
     */
    private String mobile;

    /**
     *  许可证,所属表字段为company.business_license_number
     */
    private String businessLicenseNumber;

    /**
     *  详细地址,所属表字段为company.address
     */
    private String address;

    /**
     *  身份证正面,所属表字段为company.img_id_front
     */
    private String imgIdFront;

    /**
     *  身份证反面,所属表字段为company.img_id_back
     */
    private String imgIdBack;

    /**
     *  ,所属表字段为company.img_tax_registration_certificate
     */
    private String imgTaxRegistrationCertificate;

    /**
     *  ,所属表字段为company.img_business_license
     */
    private String imgBusinessLicense;

    /**
     *  组织机构代码证,所属表字段为company.img_organization_code_certificate
     */
    private String imgOrganizationCodeCertificate;

    /**
     *  公司logo,所属表字段为company.headimgurl
     */
    private String headimgurl;

    /**
     *  用户编号,所属表字段为company.user_id
     */
    private Long userId;

    /**
     *  商家类型
0 实体店
1 微商
2 心理咨询
3 城市合伙人
4 事业合伙人
5 教育培训
6 联合创始人
7 艺术家
8 供货商
9 产业合伙人
10 餐饮
11 互动商家(至尊版)
12 区域代理商
13 互动商家(基础版)
14 合伙人
15 创始股东,所属表字段为company.company_type
     */
    private Byte companyType;

    /**
     *  经营品类,所属表字段为company.company_goods_categoty_id
     */
    private Long companyGoodsCategotyId;

    /**
     *  法人身份证,所属表字段为company.jm_id
     */
    private String jmId;

    /**
     *  合作支付方式
0 月结
1 满1000
2 实时(微信),所属表字段为company.co_pay_type
     */
    private Byte coPayType;

    /**
     *  心理咨询二级证,所属表字段为company.xl_er_cert
     */
    private String xlErCert;

    /**
     *  0 待审核
1 审核通过
2 未通过,所属表字段为company.company_status
     */
    private Byte companyStatus;

    /**
     *  审核原因,所属表字段为company.audit_cause
     */
    private String auditCause;

    /**
     *  0 红酒
1 虫草
2 积分
3 保证金,所属表字段为company.co_type
     */
    private Byte coType;

    /**
     *  区级ID,所属表字段为company.district_id
     */
    private Long districtId;

    /**
     *  省,所属表字段为company.province
     */
    private String province;

    /**
     *  市,所属表字段为company.city
     */
    private String city;

    /**
     *  区,所属表字段为company.district
     */
    private String district;

    /**
     *  银行卡账号,所属表字段为company.bank_id
     */
    private String bankId;

    /**
     *  排序顺序 降序,所属表字段为company.range_sort
     */
    private Integer rangeSort;

    /**
     *  银行名称,所属表字段为company.bank_name
     */
    private String bankName;

    /**
     *  银行卡账户,所属表字段为company.bank_account
     */
    private String bankAccount;

    /**
     *  存款,所属表字段为company.bank_deposit
     */
    private String bankDeposit;

    /**
     *  0 官网首页
1 微店,所属表字段为company.redirect_type
     */
    private Byte redirectType;

    /**
     *  店铺父级ID,所属表字段为company.pid
     */
    private Long pid;

    /**
     *  店铺口号,所属表字段为company.slogan
     */
    private String slogan;

    /**
     *  点餐二维码img URL,所属表字段为company.qr_logo_url
     */
    private String qrLogoUrl;

    /**
     *  商家分类ID,所属表字段为company.company_type_id
     */
    private Long companyTypeId;

    /**
     *  量化分级,所属表字段为company.quantitative_classification
     */
    private String quantitativeClassification;

    /**
     *  注册商家名称,所属表字段为company.business_company_name
     */
    private String businessCompanyName;

    /**
     *  法人,所属表字段为company.business_legal_person
     */
    private String businessLegalPerson;

    /**
     *  经营地址,所属表字段为company.business_address
     */
    private String businessAddress;

    /**
     *  经营范围,所属表字段为company.business_range
     */
    private String businessRange;

    /**
     *  营业许可检查时间,所属表字段为company.business_inspect_time
     */
    private Long businessInspectTime;

    /**
     *  营业许可有效时间,所属表字段为company.business_effective_time
     */
    private Long businessEffectiveTime;

    /**
     *  营业许可反面,所属表字段为company.business_img_back
     */
    private String businessImgBack;

    /**
     *  营业起始时间,所属表字段为company.business_start_time
     */
    private String businessStartTime;

    /**
     *  营业结束时间,所属表字段为company.business_end_time
     */
    private String businessEndTime;

    /**
     * 门店照片，所属表字段为img_head_photo
     */
    private String imgHeadPhoto;

    /**
     * 手持身份证正面照片，所属表字段为img_front_photo
     */
    private String imgFrontPhoto;

    /**
     * 经度,所属表字段为longitude
     */
    private String longitude;

    /**
     * 纬度,所属表字段为latitude
     */
    private String latitude;

    /**
     * 授权证明，所属表字段为authorizationCertificate
     */
    private String authorizationCertificate;
    /**
     * 店铺分类，所属表字段为storeClassification
     */
    private Byte storeClassification;
    /**
     * 押金状态，所属表字段为depositStatus
     */
    private Byte depositStatus;

    /**
     * 默认物流，所属表字段为default_logistics
     */
    private String defaultLogistics;

    /**
     * 群昵称，所属表字段为group_nickname
     */
    private String groupNickname;

    /**
     * 是否购买社群，所属表字段为pay_community
     */
    private Byte payCommunity;

    public Byte getPayCommunity() {
        return payCommunity;
    }

    public void setPayCommunity(Byte payCommunity) {
        this.payCommunity = payCommunity;
    }

    public String getGroupNickname() {
        return groupNickname;
    }

    public void setGroupNickname(String groupNickname) {
        this.groupNickname = groupNickname;
    }

    public String getAuthorizationCertificate() {
        return authorizationCertificate;
    }

    public void setAuthorizationCertificate(String authorizationCertificate) {
        this.authorizationCertificate = authorizationCertificate;
    }

    public Byte getStoreClassification() {
        return storeClassification;
    }

    public void setStoreClassification(Byte storeClassification) {
        this.storeClassification = storeClassification;
    }

    public Byte getDepositStatus() {
        return depositStatus;
    }

    public void setDepositStatus(Byte depositStatus) {
        this.depositStatus = depositStatus;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getImgFrontPhoto() {
        return imgFrontPhoto;
    }

    public void setImgFrontPhoto(String imgFrontPhoto) {
        this.imgFrontPhoto = imgFrontPhoto;
    }

    public String getImgHeadPhoto() {
        return imgHeadPhoto;
    }

    public void setImgHeadPhoto(String imgHeadPhoto) {
        this.imgHeadPhoto = imgHeadPhoto;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman == null ? null : linkman.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getBusinessLicenseNumber() {
        return businessLicenseNumber;
    }

    public void setBusinessLicenseNumber(String businessLicenseNumber) {
        this.businessLicenseNumber = businessLicenseNumber == null ? null : businessLicenseNumber.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getImgIdFront() {
        return imgIdFront;
    }

    public void setImgIdFront(String imgIdFront) {
        this.imgIdFront = imgIdFront == null ? null : imgIdFront.trim();
    }

    public String getImgIdBack() {
        return imgIdBack;
    }

    public void setImgIdBack(String imgIdBack) {
        this.imgIdBack = imgIdBack == null ? null : imgIdBack.trim();
    }

    public String getImgTaxRegistrationCertificate() {
        return imgTaxRegistrationCertificate;
    }

    public void setImgTaxRegistrationCertificate(String imgTaxRegistrationCertificate) {
        this.imgTaxRegistrationCertificate = imgTaxRegistrationCertificate == null ? null : imgTaxRegistrationCertificate.trim();
    }

    public String getImgBusinessLicense() {
        return imgBusinessLicense;
    }

    public void setImgBusinessLicense(String imgBusinessLicense) {
        this.imgBusinessLicense = imgBusinessLicense == null ? null : imgBusinessLicense.trim();
    }

    public String getImgOrganizationCodeCertificate() {
        return imgOrganizationCodeCertificate;
    }

    public void setImgOrganizationCodeCertificate(String imgOrganizationCodeCertificate) {
        this.imgOrganizationCodeCertificate = imgOrganizationCodeCertificate == null ? null : imgOrganizationCodeCertificate.trim();
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl == null ? null : headimgurl.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Byte getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Byte companyType) {
        this.companyType = companyType;
    }

    public Long getCompanyGoodsCategotyId() {
        return companyGoodsCategotyId;
    }

    public void setCompanyGoodsCategotyId(Long companyGoodsCategotyId) {
        this.companyGoodsCategotyId = companyGoodsCategotyId;
    }

    public String getJmId() {
        return jmId;
    }

    public void setJmId(String jmId) {
        this.jmId = jmId == null ? null : jmId.trim();
    }

    public Byte getCoPayType() {
        return coPayType;
    }

    public void setCoPayType(Byte coPayType) {
        this.coPayType = coPayType;
    }

    public String getXlErCert() {
        return xlErCert;
    }

    public void setXlErCert(String xlErCert) {
        this.xlErCert = xlErCert == null ? null : xlErCert.trim();
    }

    public Byte getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(Byte companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getAuditCause() {
        return auditCause;
    }

    public void setAuditCause(String auditCause) {
        this.auditCause = auditCause == null ? null : auditCause.trim();
    }

    public Byte getCoType() {
        return coType;
    }

    public void setCoType(Byte coType) {
        this.coType = coType;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId == null ? null : bankId.trim();
    }

    public Integer getRangeSort() {
        return rangeSort;
    }

    public void setRangeSort(Integer rangeSort) {
        this.rangeSort = rangeSort;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount == null ? null : bankAccount.trim();
    }

    public String getBankDeposit() {
        return bankDeposit;
    }

    public void setBankDeposit(String bankDeposit) {
        this.bankDeposit = bankDeposit == null ? null : bankDeposit.trim();
    }

    public Byte getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(Byte redirectType) {
        this.redirectType = redirectType;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan == null ? null : slogan.trim();
    }

    public String getQrLogoUrl() {
        return qrLogoUrl;
    }

    public void setQrLogoUrl(String qrLogoUrl) {
        this.qrLogoUrl = qrLogoUrl == null ? null : qrLogoUrl.trim();
    }

    public Long getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(Long companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public String getQuantitativeClassification() {
        return quantitativeClassification;
    }

    public void setQuantitativeClassification(String quantitativeClassification) {
        this.quantitativeClassification = quantitativeClassification == null ? null : quantitativeClassification.trim();
    }

    public String getBusinessCompanyName() {
        return businessCompanyName;
    }

    public void setBusinessCompanyName(String businessCompanyName) {
        this.businessCompanyName = businessCompanyName == null ? null : businessCompanyName.trim();
    }

    public String getBusinessLegalPerson() {
        return businessLegalPerson;
    }

    public void setBusinessLegalPerson(String businessLegalPerson) {
        this.businessLegalPerson = businessLegalPerson == null ? null : businessLegalPerson.trim();
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress == null ? null : businessAddress.trim();
    }

    public String getBusinessRange() {
        return businessRange;
    }

    public void setBusinessRange(String businessRange) {
        this.businessRange = businessRange == null ? null : businessRange.trim();
    }

    public Long getBusinessInspectTime() {
        return businessInspectTime;
    }

    public void setBusinessInspectTime(Long businessInspectTime) {
        this.businessInspectTime = businessInspectTime;
    }

    public Long getBusinessEffectiveTime() {
        return businessEffectiveTime;
    }

    public void setBusinessEffectiveTime(Long businessEffectiveTime) {
        this.businessEffectiveTime = businessEffectiveTime;
    }

    public String getBusinessImgBack() {
        return businessImgBack;
    }

    public void setBusinessImgBack(String businessImgBack) {
        this.businessImgBack = businessImgBack == null ? null : businessImgBack.trim();
    }

    public String getBusinessStartTime() {
        return businessStartTime;
    }

    public void setBusinessStartTime(String businessStartTime) {
        this.businessStartTime = businessStartTime == null ? null : businessStartTime.trim();
    }

    public String getBusinessEndTime() {
        return businessEndTime;
    }

    public void setBusinessEndTime(String businessEndTime) {
        this.businessEndTime = businessEndTime == null ? null : businessEndTime.trim();
    }

    public String getDefaultLogistics() {
        return defaultLogistics;
    }

    public void setDefaultLogistics(String defaultLogistics) {
        this.defaultLogistics = defaultLogistics;
    }
}