package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Order;
import java.util.List;

public interface OrderMapper {
    int insert(Order order);

    int deleteByPK(Long orderId);

    int updateByPK(Order order);

    Order selectByPK(Long orderId);

    long count(Order order);

    List<Order> select(Order order);
}