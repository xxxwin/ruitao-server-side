package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.domain.ShoppingCart;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.ShoppingCartMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class ShoppingCartService implements BaseService<ShoppingCart> {
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    @Autowired
    private GoodsService goodsService;

    @Override
    public ShoppingCart insert(ShoppingCart shoppingCart) {
        Long time = System.currentTimeMillis();
        shoppingCart.setCtime(time);
        shoppingCart.setMtime(time);
        Goods goods = goodsService.selectByPK(shoppingCart.getGoodsId());
        BeanUtils.copyProperties(goods, shoppingCart);
        shoppingCartMapper.insert(shoppingCart);
        return shoppingCart;
    }

    @Override
    public int delete(Long id) {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setCartId(id);
        shoppingCart.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        shoppingCart.setMtime(time);
        return shoppingCartMapper.updateByPK(shoppingCart);
    }

    @Override
    public ShoppingCart update(ShoppingCart shoppingCart) {
        Long time = System.currentTimeMillis();
        shoppingCart.setMtime(time);
        shoppingCartMapper.updateByPK(shoppingCart);
        return shoppingCart;
    }

    @Override
    public ShoppingCart selectByPK(Long id) {
        return shoppingCartMapper.selectByPK(id);
    }

    @Override
    public List<ShoppingCart> select(ShoppingCart shoppingCart) {
        shoppingCart.setRstatus((byte) 0);
        return shoppingCartMapper.select(shoppingCart);
    }
    public List<ShoppingCart> selectForPage(ShoppingCart shoppingCart) {
        shoppingCart.setRstatus((byte) 0);
        shoppingCart.setRecords(shoppingCartMapper.count(shoppingCart));
        return shoppingCartMapper.select(shoppingCart);
    }

    public Long count(ShoppingCart shoppingCart){
        shoppingCart.setRstatus((byte) 0);
        return shoppingCartMapper.count(shoppingCart);
    }

}
