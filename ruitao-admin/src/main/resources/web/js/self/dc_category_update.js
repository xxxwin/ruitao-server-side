/**
 *
 */

var Config = {
    createUrl: "/api/admin/dc/categories",
    updateUrl: "/api/admin/dc/categories",
    deleteUrl: "/api/admin/dc/categories",
    selectUrl: "/api/admin/dc/categories",
    selectCompanyUrl:'/api/dc/list/company'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$(document).ready(function () {
    $('#submitForUpdate').click(function () {
        var id =$("input[id=id]").val();
        var companyId =$("input[id=companyId]").val();
        var name = $("input[id=tag_name]").val();
        var remark = $("input[id=tag_remark]").val();
        $.ajax(
            {url: Config.updateUrl,
                type: "PUT",
                data: {id:id,name:name,remark:remark,companyId:companyId},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        return false;
    });
});

function fmtOper(cellvalue, options, rowObject) {
    var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/dinner_dish/' + rowObject.id + '" data-index="0">编辑</a>';
    html += "<a href='#'   style='color:#f60' onclick='Delete(" + rowObject.id + ")' >删除</a>";
    return html
}

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    $("#data_list").trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown);
            }
        });
}