package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.Role;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface RoleAdMapper {
    List<Role> selectBatch(List<Long> roleIds);

//    @Select("SELECT pid FROM rtshop.relations where pid != 0 group by pid;")
//    List<Role> selectByPid();
}