package com.ruitaowang.core.utils;

//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.URI;
import java.net.URLConnection;
import java.util.Base64;

/**
 * 图片工具类
 * <p/>
 * Created by neal on 11/27/16.
 */
public final class ImageUtils {

//    /**
//     * @Descriptionmap 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
//     * @param path 图片路径
//     * @return
//     */
//    public static String imageToBase64(String path) {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
//        byte[] data = null;
//        // 读取图片字节数组
//        try {
//            InputStream in = new FileInputStream(path);
//            data = new byte[in.available()];
//            in.read(data);
//            in.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // 对字节数组Base64编码
//        BASE64Encoder encoder = new BASE64Encoder();
//        return encoder.encode(data);// 返回Base64编码过的字节数组字符串
//    }

    /**
     * @Descriptionmap 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
     * @param destUrl 图片路径
     * @return
     */
    public static String imageToBase64FromRemote(String destUrl) {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        URI uri = null;
        byte[] data = null;
        URLConnection connection = null;// 获取URLConnection对象
        BufferedInputStream in = null;
        ByteArrayOutputStream out = null;
        try {
            uri = new URI(destUrl);// 创建URI对象
            connection = uri.toURL().openConnection();
            in = new BufferedInputStream(connection.getInputStream());
            out = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            data = out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {}
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
        // 对字节数组Base64编码
//        BASE64Encoder encoder = new BASE64Encoder();
//        return encoder.encode(data);// 返回Base64编码过的字节数组字符串
        return Base64.getEncoder().encodeToString(data);

    }

//    /**
//     * @Descriptionmap 对字节数组字符串进行Base64解码并生成图片
//     * @author temdy
//     * @param base64 图片Base64数据
//     * @param path 图片路径
//     * @return
//     */
//    public static boolean base64ToImage(String base64, String path) {// 对字节数组字符串进行Base64解码并生成图片
//        if (base64 == null){ // 图像数据为空
//            return false;
//        }
//        BASE64Decoder decoder = new BASE64Decoder();
//        try {
//            // Base64解码
//            byte[] bytes = decoder.decodeBuffer(base64);
//            for (int i = 0; i < bytes.length; ++i) {
//                if (bytes[i] < 0) {// 调整异常数据
//                    bytes[i] += 256;
//                }
//            }
//            // 生成jpeg图片
//            OutputStream out = new FileOutputStream(path);
//            out.write(bytes);
//            out.flush();
//            out.close();
//            return true;
//        } catch (Exception e) {
//            return false;
//        }
//    }

    public static void main(String[] args) {
//        String img = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCABkAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDvkso5dxcyAGQOPnIzjBHHHpjHoKslAGzvIx1wetRxxuhUGXdj72R16/lyfypJpVDquFbJ5yegH+R+dBJKyoSOQG6Z71GqhRgMSPc5qAXMZEaqGKsSvzZ7Z6/ljmmTXEcJODh5DhR6t/jj9B7UwJXb5ipPOM9arRPM6oSBhkyWzyG9MY+tL5yzqvmIpzg464I57+9VvszPIzPJKfm3AK+FwM4Hr6ZHSkBbjmjjjjWSRQ+3puyenb170q3kLKCGOCu9flOSPp+IqtDAsStGu0Z5wBjk9/xNTiP91sZtwz/Fgken+fagCzvVkDL82QMbT+tU5J5RKPlj64O58Y464xzzU6nany9B6VE7yf7IPQcE4NAD4p91qskxVCUDMDxszz39KEZGizk7fu5JPPbv/Oog5bKnDouQSRzn2/Cobjz42Bj2iEMCyhMsR3xyPb36+1AxksWlMwMgtAQMAPtz+p9c0VcTylX5Aqg88Dr78UUCLW6QZJ24PouP61A5lVMGZS2Dk7cfl/k1I0/0pn2jnmgvlIFn2uVmZTxw+QOw9/X2pTPG+VV0b1CmrBlH/wCqoXlAXnAFK4uUppMz4cwMrdB0zjjn/PpUgk3HOdo9K5nU/iFo1hK0SGS6kGeYQChOP73T8RmsY/FOJZdraQ+M8Ezc/ltoDlPQkZMd84xSvIAg5PUD9awNE8YaNr0wt4GMdwckQzqFLAenUH6Zz1re8m2bkwRn/gIphyiyStGny5OSAMc8n+Q96q/Z2uSplLIiO3yZJDjPGc9vb+laCCNVwqKB7Cn4RuoFAcpUJYSAALsYnJH6H/Oe1QyWFniTNvCyTH98rRBvMPbdxzj3rSCJ/dGfakMMR6hv++jQFiuIzgbI/lHTAxRVyKNY1IQEAnJ5zRQOxyek3TeZLGzs24bhk/h/h+VagnwQc1zmlxXlzqdvDBbylnKkFkIARjjdn+7z16Vo6hcTaYtw9zZagsEJO+f7FKUwOpzt6cdeldGMjapddTPD3cLdjV+0jgHpWJ4shvtR8N3Vrp243Em1QqsFLDcNwySMcZ/DI71oaba3mr2KXlnBI8D/AHWf92ePZsH9KimhvbNhLLbywbcgSNGep7c8dq45SaVzohFSdjxlvDWqibSokCOdRjMkADYwANxzkcYBzx+GawzK2evNe5yRWk1/a3kzSebbCQRAfw78A9/QY/GvP7jwTbRWsKo90127kvtXeioCcdBkZ4PNZ063NuazoNbHIQ3MsMySRuyOjBlZTgqRzke9fQum35vdNtbtlCGaJZCgOcZGev41xPhP4bafqsgaSV7gAjPzbUH1PX8Otev6b4Ms7K2jhYkxxrtWOPIAHbkn+laKfM9EZSjyqzephrN708TZPWull8N6fKSFSSHb3R85z25z/k1iahoc9hEZUkE0Y+8QuCPfHpWhBCsnvTxJ0rPSbpU8bknIoAu+YO+fyoqFTxRQB5OPitrKWQtrOK0sI0TaDCrbnHAAy248DPfpn2qFPEOs6rJFcXs1xviJMbtO+VJx0XgDOKybDTVjcScGUnOcYA78ADApmo61baf5kEYW4uhtwFIMa55O4g5J6DA7nkgjB05nuRypqx6NpPjiSwjMt5FbIjAoZmSSSWdgvTcWPtnGMZHGKv8Ag/xBfeN9Su7WbcunwqJCGZASxPyjBycfePy4xjB68+HfaLi5uGnlcs7/ADNsXaOewAGAPYD6V6/8LbG4sYTMLtDLqMTOkZRsoI2C7ztHOTJ03DAxw2flya5tzVclONobnV69oFjpmnTX7faGS3HmyIOjqOSo4GCR3zUekeH9Nlle+e51S006bBGnXUzLtk4OcByBwDlSNw6gjAqK/wAyFI9ZeNxcxyYMyvBFJtG7YRklflOfmPOD9KoaX4oL3hiv57ITDLRGGNMy9gyyhtpOwgkbUOG6YoUIx2QnOUt2ei2p07SonSyt0RHbexRs7mPcnuelS/2qrfdH5mvNNb8Tj7O9to9wn2k+WxnVkEUauy8ljleQTgYJOQcEVr219I0aCR180KA+0YGe5xk4+mTTCyO2+2BxkGmPNvUgHH4VzsV7hxkgKepzVo3wCfKu4+hFArIxdRt/st8wUYRvmXApITgipNQmWSUGZWiIHGeaxl8RaSs0kIvEMiEhlVGJBHHpQBvqeP8A69FZq6vYuoK3UePyooA8Fudd1G6EsdsPKhkQqyxL8xXvk8kZHXGOCR0qhb267A24Fz0UjgfUmpzbz2M+HjlgmRh95SrKwP6EEUnzM7PufexyzBjkn1qt2DTW5dEDGMwzyNEIix2YOTIcDaB2PAznoB9Aer8A+Ko/D808U8qIsrq4Usx2KFcswx3wAPVuOg5rhiCzE7iSRjkAn8yKmt7GW+nSCCJ5ZpnCqApZmY/qTQ9BJXdkeq3utLq1zHL5Uw1CaNd6S3CAwsVZWAiyFYqknXbkAKCMljWB4g8T2WkzRLJpsM1zkBoI7hmhEajAG3AAIIHGwj5fUca+qaPpnwu8MW9xIhuNYu8K43DKkqTx1G0EAe/J5xivGrieS6uJZ5W3SSOXYnuSeam9ynZaI9t0S9s9b09Li1ljZVVy0SxhSrlg2Dz7D8ed3rvR712h0Cv3Unj8G7/jXgGj6vc6LfJdW7dD8yE8MP8APevW9G8YRXlmk0wa5heRlYJGd8YHI3YznsOnf8aBXOxjlPAyFP8AdY8/5+lTiUoclHB9hiqdrJb3sKy2spZWUOFcEEg9/cfmKlYNbqMsYx09F/XigQly6OrMwkBPAyAR/OuA8UeHdDjikvCL+G6+eWTyJMo45Zid2cHPpx7V38jZQtIFI7npn+dc9qk9vGs13LsSJEJLyHcij3ABP4DrmmM8MLyZOTk+tFRbh7j6UUCPs20s7eztxDEg2gYJbkvySSx7kkkknqSfWqc/hjQLmMpNounuCCObZMj6HGR+FZ9l440C8ViL5Ydq7sTDYfoPU1pWviHR7yTZb6pZyP8A3FmXcfwzTcJLdApu1kzCufhd4QuYig0swsRgSRTOCvvySPzBqtH4e8KfDS2m1xxNuA2LLNmQqSDwuBxnuf8AJ7Oa+tLWza8uLmGK1UbmmeQBAM46184/Ezx1c+JdUlsrO736RGw2qke0Ow9Tk7un0yOAcA1FiueT3ZzHirxDdeJ9duL2SW4aFpGMEUshby1J6Adh7Dp0ycZrMlsxHbeYWwwGSOootQAScfMKW8ZmCLzsHf3pklQcnAq9Y3moaZL5tnNLCT12HhvqOh6nrVaFCz5BxjuKkN1MDgOGAPXHWgDr9P8AiFPa2xivtLW5xja8U725zxksFyD0HQDv6mu1sPip4edP3kE9qQMDzAx/Vc/0rx37XIQMxRn8Kf8AaEYfPb8fXNMD168+KGl+Sxhu1cgHCLDJuP0LcZrzrxL4vl1v9zCrx2+cnd1b8B0796xibRuCjD8On5UnlWjDhsD6n+tAFNQSO9FXhbxAYWTj/eFFAHT6pcS2wjjibaHUk/hj/GqnmvDY+arZeQ8s3J7j+lFFdtST9o9ehjFe6jtPGmpTeGPAXhiz0tYov7QtftM85TMvmMqEkN+OPoB6DHk6yyb/ADd7bx0bNFFcJsWAoGMAdKeFG4j3oooKIS3l3KqoUZ7496jmjRZAqrgY9aKKCRinDUbiScnpRRTAVAG6iiRjtC9qKKAGYyAaKKKAP//Z";
//        img = img.replace("data:image/jpeg;base64,","");
//        System.out.println(img.length());
//        base64ToImage(img, "/Users/neal/work/base.jpg");

        String url = "http://static.ruitaowang.com/qr/qr_1_83.png";
//        String url = "http://preview.quanjing.com/imageclk007/ic01700997.jpg";
        String str = imageToBase64FromRemote(url);	//读取输入流,转换为Base64字符
        System.out.println("data:image/png;base64,"+str);
    }
}