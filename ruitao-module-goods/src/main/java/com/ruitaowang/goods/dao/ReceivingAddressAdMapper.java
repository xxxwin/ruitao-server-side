package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ReceivingAddress;

import java.util.List;

public interface ReceivingAddressAdMapper {

    List<ReceivingAddress> selectDesc(ReceivingAddress receivingAddress);

    List<ReceivingAddress> selectByMtime(ReceivingAddress receivingAddress);
}
