package com.ruitaowang.wap;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.CompanyUser;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.UserOnline;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyUserService;
import com.ruitaowang.goods.service.UserOnlineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class APIWapCompanyUserController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private CompanyUserService companyUserService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserOnlineService userOnlineService;

    /**
     * 查询社群成员（分页）
     * @param page
     * @param companyUser
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/companyUser/page", method = RequestMethod.GET)
    public String companyUser(int page, CompanyUser companyUser,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        companyUser.setPage(page);
        MsgOut o = MsgOut.success(companyUserService.selectForPage(companyUser));
        o.setRecords(companyUser.getRecords());
        o.setTotal(companyUser.getTotal());
        return this.renderJson(o);
    }

    /**
     * 群成员信息
     * @param companyUser
     * @return
     */
//    @RequestMapping(value = "/api/wap/communityMembers", method = RequestMethod.GET)
//    public String communityMembers(CompanyUser companyUser){
//        SysUser user = LYSecurityUtil.currentSysUser();
//        if (ObjectUtils.isEmpty(user)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        List listOut = new ArrayList();
//        List<CompanyUser> companyUserList = companyUserService.select(companyUser);
//        for (CompanyUser c : companyUserList){
//            SysUser sysUser = sysUserService.selectByPK(c.getUserId());
//            listOut.add(sysUser);
//        }
//        MsgOut o = MsgOut.success(listOut);
//        return this.renderJson(o);
//    }

    @RequestMapping(value = "/api/wap/communityMembers", method = RequestMethod.GET)
    public String communityMembers(UserOnline userOnline,int type,int page,Long companyId){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        userOnline.setRoomId(companyId);
        List listOut = new ArrayList();
        if (type == 1){
            userOnline.setRows(15);
        } else {
            userOnline.setPage(page);
        }
        List<UserOnline> userOnlineList = userOnlineService.selectForPage(userOnline);
        for (UserOnline u : userOnlineList){
            SysUser sysUser = sysUserService.selectByPK(u.getUserId());
            if (!ObjectUtils.isEmpty(sysUser)) {
                listOut.add(sysUser);
            }
        }
        MsgOut o = MsgOut.success(listOut);
        return this.renderJson(o);
    }

    /**
     * 修改
     * @param companyUser
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/updateCompanyUser", method = RequestMethod.PUT)
    public String updateCompanyUser(CompanyUser companyUser,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(companyUserService.update(companyUser));
        return this.renderJson(o);
    }

    /**
     * 新增
     * @param companyUser
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/insertCompanyUser", method = RequestMethod.POST)
    public String insertCompanyUser(CompanyUser companyUser,@RequestHeader(value="headId",required = false) Long userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(companyUserService.insert(companyUser));
        return this.renderJson(o);
    }

    /**
     * 删除
     * @param id
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/companyUser/delete", method = RequestMethod.DELETE)
    public String wapCommentsDelete(Long id,@RequestHeader(value="headId",required = false) String userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(companyUserService.delete(id));
        return this.renderJson(o);
    }

}
