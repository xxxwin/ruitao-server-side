package com.ruitaowang.core.utils;

/**
 * Created by neal on 10/03/2017.
 */
public class StringUtils4RT {

    public static final String ruitaoMZGoodsId = "1592";
    public static final String ruitaoMZ1GoodsId = "2697";
    public static final String ruitaoMZ3GoodsId = "2698";
    public static final String ruitaoMZ6GoodsId = "2699";
    public static final String ruitaoQR6GoodsId = "2700";
    public static final String ruitaoKQGoodsId = "1831";
    public static final String ruitaoDPGoodsId = "1832";
    public static final String ruitaoKLGoodsId = "1841";
    public static final String ruitaoKXGoodsId = "1842";
    public static final String ruitaoKDGoodsId = "1843";
    public static final String ruitaoKFGoodsId = "1844";
    public static final String ruitaoKTGoodsId = "1845";
    public static final String ruitaoKZZGoodsId = "2025";//快赚
    public static final String ruitaoHDGoodsId = "2473";//云动 3000
    public static final String ruitaoHDPGoodsId = "2692";//云动 600
    public static final String ruitaoHDCommunity = "3340";//社群群贤599
    /**
     * 转换为下划线
     *
     * @param camelCaseName
     * @return
     */
    public static String underscoreName(String camelCaseName) {
        StringBuilder result = new StringBuilder();
        if (camelCaseName != null && camelCaseName.length() > 0) {
            result.append(camelCaseName.substring(0, 1).toLowerCase());
            for (int i = 1; i < camelCaseName.length(); i++) {
                char ch = camelCaseName.charAt(i);
                if (Character.isUpperCase(ch)) {
                    result.append("_");
                    result.append(Character.toLowerCase(ch));
                } else {
                    result.append(ch);
                }
            }
        }
        return result.toString();
    }

    /**
     * 转换为驼峰
     *
     * @param underscoreName
     * @return
     */
    public static String camelCaseName(String underscoreName) {
        StringBuilder result = new StringBuilder();
        if (underscoreName != null && underscoreName.length() > 0) {
            boolean flag = false;
            for (int i = 0; i < underscoreName.length(); i++) {
                char ch = underscoreName.charAt(i);
                if ("_".charAt(0) == ch) {
                    flag = true;
                } else {
                    if (flag) {
                        result.append(Character.toUpperCase(ch));
                        flag = false;
                    } else {
                        result.append(ch);
                    }
                }
            }
        }
        return result.toString();
    }

}
