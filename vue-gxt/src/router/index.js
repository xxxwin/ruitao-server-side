import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/views/personal/Index'
import { resolve } from 'url';
Vue.use(Router)
export default new Router({
  mode: "history",
  base: '/wap/gxt/',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        isUseCache: false,
        keepAlive: true
      }
    },
    {
      path: '/More_industry',
      name: 'More_industry',
      component: resolve => require(['@/views/personal/dict/more_industry'], resolve),
      meta: { title: '行业选择' }
    },
    {
      path: '/scanjoinresult',
      name: 'scanjoinresult',
      component: resolve => require(['@/views/personal/mine/scanjoinresult'], resolve),
      meta: { title: '扫码后提示' }
    },
    {
      path: '/collection',
      name: 'Collection',
      component: resolve => require(['@/views/personal/collection/collection'], resolve),
    }
    , {
      path: '/Company_sc',
      name: 'Company_sc',
      component: resolve => require(['@/views/personal/collection/company_sc'], resolve),
    },
    {
      path: "/Collecyion_card",
      name: "Collecyion_card",
      component: resolve => require(['@/views/personal/collection/collection_card'], resolve),
    },
    {
      path: '/Interaction_personal',
      name: "Interaction_personal",
      component: resolve => require(['@/views/personal/interaction/interaction_personal'], resolve),

    },
    {
      path: '/Mine',
      name: "Mine",
      component: resolve => require(['@/views/personal/mine/mine'], resolve),
    },
    {
      path: '/shenhezhong',
      name: 'shenhezhong',
      component: resolve => require(['@/views/personal/mine/shenhezhong'], resolve),
    },
    {
      path: '/personal/mine/qrcode',
      name: "Personqrcode",
      component: resolve => require(['@/views/personal/mine/qr_code'], resolve),
    },
    {
      path: '/personal/mine/qrcodetuiguang',
      name: "Personqrcodetg",
      component: resolve => require(['@/views/personal/mine/qr_codetuiguang'], resolve),
    },
    {
      path: '/personal/mine/qrsharecode',
      name: "Personshareqrcode",
      component: resolve => require(['@/views/personal/mine/qr_sharecode'], resolve),
    }, {
      path: '/Userinfo_bg',
      name: 'Userinfo_bg',
      component: resolve => require(['@/views/personal/mine/userinfo_bg'], resolve),
    },
    {
      path: '/Seach',
      name: "Seach",
      component: resolve => require(['@/views/personal/seach'], resolve),
      meta: {
        isUseCache: false,
        keepAlive: true
      }
    }, {
      path: '/Nearby',
      name: "Nearby",
      component: resolve => require(['@/views/personal/nearby/nearby'], resolve),
      meta: {
        isUseCache: false,
        keepAlive: true
      }
    }, {
      path: '/User_set',
      name: 'User_set',
      component: resolve => require(['@/views/personal/mine/user_set'], resolve),
    },
    {
      path: "/Commodity_details/",
      name: "Commodity_details",
      component: resolve => require(['@/views/personal/shop/commodity_details'], resolve),
      meta: {
        title: '商品',

      }
    },
    {
      path: '/Commodity_details_list',
      name: 'Commodity_details_list',
      component: resolve => require(['@/views/personal/shop/detailed_list'], resolve),
    },
    {
      path: '/details_list_jiesuan',
      name: 'details_list_jiesuan',
      component: resolve => require(['@/views/personal/shop/details_list_jiesuan'], resolve),
    },
    {
      path: '/addaddress',
      name: 'addaddress',
      component: resolve => require(['@/views/personal/shop/addaddress'], resolve),
    },
    {
      path: '/newaddaddress',
      name: 'newaddaddress',
      component: resolve => require(['@/views/personal/shop/newaddaddress'], resolve),
    },
    {
      path: "/Commodity_comment",
      name: 'Commodity_comment',
      component: resolve => require(['@/views/personal/shop/commodity_comment'], resolve),
    },
    {
      path: "/commodity_show",
      name: 'commodity_show',
      component: resolve => require(['@/views/personal/shop/commodity_show'], resolve),
    },
    {
      path: "/commodity_show_1",
      name: 'commodity_show_1',
      component: resolve => require(['@/views/personal/shop/commodity_show_1'], resolve),
    },
    {
      path: '/Commodity',
      name: 'Commodity',
      component: resolve => require(['@/views/personal/shop/commodity'], resolve),
      meta: {
        isUseCache: false,
        keepAlive: true
      }
    }, 
    {
      path: '/Shops',
      name: 'Shops',
      component: resolve => require(['@/views/personal/shop/shops'], resolve),
    },
    {
      path: "/Business",
      name: "Business",
      component: resolve => require(["@/views/business/business"], resolve),
    },
    {
      path: "/Business_discount",
      name: "Business_discount",
      component: resolve => require(["@/views/business/discount/business_discount"], resolve),
    },
    {
      path: "/Business_order",
      name: "Business_order",
      component: resolve => require(["@/views/business/order/business_order"], resolve),
    },
    {
      path: "/Business_settlement",
      name: "Business_settlement",
      component: resolve => require(["@/views/business/settlement/business_settlement"], resolve),
    },
    {
      path: "/Business_rz",
      name: "Business_rz",
      component: resolve => require(['@/views/business/user_center/business_rz'], resolve),
    },
    {
      path: '/headline/id/:id/qubie/:qubie',
      name: 'headline',
      component: resolve => require(['@/views/personal/headline'], resolve),
    }, {
      path: '/headline_text',
      name: 'headline_text',
      component: resolve => require(['@/views/personal/headline_text'], resolve),
    }, {
      path: '/Discount',
      name: 'Discount',
      component: resolve => require(['@/views/personal/discount/discount'], resolve)
    },
    {
      path: '/Discount_list/id/:id/subid/:subid',///
      name: "Discount_list",
      component: resolve => require(['@/views/personal/discount/discount_list'], resolve)
    }
    , {
      path: '/Business_rz_qr_code/id/:id/cmid/:cmid/companyName/:companyName',
      name: 'Business_rz_qr_code',
      component: resolve => require(['@/views/business/main/qr_code'], resolve)
    }, {
      path: '/Business_shop_set',
      name: 'Business_shop_set',
      component: resolve => require(['@/views/business/main/shop_set'], resolve)
    }, {
      path: '/business/product/add',
      name: 'Business_release_product',
      component: resolve => require(['@/views/business/main/release_product'], resolve)
    }, {
      path: '/Business_Cp_manage',
      name: 'Business_Cp_manage',
      component: resolve => require(['@/views/business/main/cp_manage'], resolve)
    }, {
      path: '/Business_discount_set/',
      name: 'Business_discount_set',
      component: resolve => require(['@/views/business/main/mj_manage'], resolve)

    }, {
      path: '/Business_add_mj',
      name: 'Business_add_mj',
      component: resolve => require(['@/views/business/main/add_mj'], resolve)
    },
    {
      path: '/Business_drainage',
      name: 'Business_drainage',
      component: resolve => require(['@/views/business/main/drainage'], resolve)
    }, {
      path: '/Commodity_Settlement',
      name: 'Commodity_Settlement',
      component: resolve => require(['@/views/personal/shop/settlement'], resolve),
    }, {
      path: "/business/cate/manage",
      name: 'Business_category_ad',
      component: resolve => require(['@/views/business/main/category_ad'], resolve),
    }, {
      path: "/business/cate/add",
      name: 'Business_category_add',
      component: resolve => require(['@/views/business/main/category_add'], resolve),
    },
    {
      path: "/business/cate/edit/id/:id",
      name: 'Business_category_edit',
      component: resolve => require(['@/views/business/main/category_edit'], resolve),

    },
    {
      path: "/lrzup",
      name: 'Lrzup',
      component: resolve => require(['@/views/personal/lrzup'], resolve),
    }, {
      path: '/personorder_r',
      name: 'personorder_r',
      component: resolve => require(['@/views/personal/mine/personorder'], resolve),

    }, {
      path: '/shibai',
      name: 'shibai',
      component: resolve => require(['@/views/personal/mine/shibai'], resolve),
    },
    {
      path: '/xw_headline',
      name: 'xw_headline',
      component: resolve => require(['@/views/personal/xw_headline'], resolve),
    },
    {
      path: '/hb_earning',
      name: 'hb_earning',
      component: resolve => require(['@/views/personal/mine/hb_earning'], resolve),
    },
    {
      path: '/earnings_mx',
      name: 'earnings_mx',
      component: resolve => require(['@/views/personal/mine/earnings_mx'], resolve),
    },
    {
      path: '/cash_earnings',
      name: 'cash_earnings',
      component: resolve => require(['@/views/personal/mine/cash_earnings'], resolve),
    },
    {
      path: '/use_rules',
      name: 'use_rules',
      component: resolve => require(['@/views/personal/mine/use_rules'], resolve),
    },
    {
      path: '/clause',
      name: 'clause',
      component: resolve => require(['@/views/business/clause'], resolve),
    },
    {
      path: '/member',
      name: 'member',
      component: resolve => require(['@/views/personal/mine/member'], resolve),

    },
    {
      path: '/cash_earnings_more',
      name: 'cash_earnings_more',
      component: resolve => require(['@/views/personal/mine/cash_earnings_more'], resolve),
    },
    {
      path: '/cash_earnings_deposit',
      name: 'cash_earnings_deposit',
      component: resolve => require(['@/views/personal/mine/cash_earnings_deposit'], resolve),
    },
    {
      path: '/cash_earnings_successful',
      name: 'cash_earnings_successful',
      component: resolve => require(['@/views/personal/mine/cash_earnings_successful'], resolve),
    },
    {
      path: '/busCash_eamings',
      name: 'busCash_eamings',
      component: resolve => require(['@/views/business/main/busCash_eamings'], resolve),
    },

    {
      path: '/pay_success',
      name: 'pay_success',
      component: resolve => require(['@/views/business/settlement/pay_success'], resolve),
    },
    {
      path: '/order_detail',
      name: 'order_detail',
      component: resolve => require(['@/views/business/settlement/order_detail'], resolve),
    }
    ,
    {
      path: '/add_bank_card',
      name: 'add_bank_card',
      component: resolve => require(['@/views/personal/mine/Add_bank_card'], resolve),
    },
    ,
    {
      path: '/Card_package',
      name: 'Card_package',
      component: resolve => require(['@/views/personal/mine/Card_package'], resolve),
    }
    ,
    {
      path: '/accounts_security',
      name: 'accounts_security',
      component: resolve => require(['@/views/personal/mine/accounts_security'], resolve),
    },
    {
      path: '/cashcard',
      name: 'cashcard',
      component: resolve => require(['@/views/personal/mine/Cashcard'], resolve),
    },
    {
      path: '/withdrawalstate',
      name: 'withdrawalstate',
      component: resolve => require(['@/views/personal/mine/withdrawalstate'], resolve),
    },
    {
      path: '/locational',
      name: 'locational',
      component: resolve => require(['@/views/personal/locational'], resolve),
    },
    {
      path: "/Commentreply",
      name: "Commentreply",
      component: resolve => require(['@/views/personal/shop/Commentreply'], resolve),
    },
    {
      path: "/redenvelopes",
      name: "redenvelopes",
      component: resolve => require(['@/views/personal/redenvelopes/redenvelopes'], resolve),
    },
    {
      path: "/setthepassword",
      name: "setthepassword",
      component: resolve => require(['@/views/personal/mine/setthepassword'], resolve),
    },
    {
      path: "/cellphoneHref",
      name: "cellphoneHref",
      component: resolve => require(['@/views/personal/mine/cellphoneHref'], resolve),
    },
    {
      path: "/VerificationTel",
      name: "VerificationTel",
      component: resolve => require(['@/views/personal/mine/VerificationTel'], resolve),
    },
    {
      path: "/orderdetails",
      name: "orderdetails",
      component: resolve => require(['@/views/personal/mine/orderdetails'], resolve),
      meta: { title: '订单详情' }
    },
    {
      path: "/arefund",
      name: "arefund",
      component: resolve => require(['@/views/personal/mine/arefund'], resolve),
      meta: { title: '退款' }
    },
    {
      path: "/comments",
      name: "comments",
      component: resolve => require(['@/views/personal/mine/comments'], resolve),
      meta: { title: '发布评论' }
    },
    {
      path: "/Business_rzxzy",//business_rzgeren
      name: "Business_rzxzy",
      component: resolve => require(['@/views/personal/mine/Business_rzxzy'], resolve),
      meta: { title: '商家入驻' }
    },
    {
      path: "/paymentsuccess",
      name: "paymentsuccess",
      component: resolve => require(['@/views/personal/mine/paymentsuccess'], resolve),
      meta: { title: '支付成功' }
    },
    {
      path: "/paymentchoice",
      name: "paymentchoice",
      component: resolve => require(['@/views/personal/mine/paymentchoice'], resolve),
      meta: { title: '付款选择页面' }
    },
    {
      path: "/help",
      name: "help",
      component: resolve => require(['@/views/personal/mine/help'], resolve),
      meta: { title: '付款选择页面' }
    },
    {
      path: "/PermissionIllustrate",
      name: "PermissionIllustrate",
      component: resolve => require(['@/views/personal/PermissionIllustrate'], resolve),
      meta: { title: "微传媒免费广告" }
    },
    {
      path: "/applyARefund",
      name: "applyARefund",
      component: resolve => require(['@/views/personal/applyrefund/applyARefund'], resolve),
      meta: { title: "申请退款" }
    },
    {
      path: "/shuffling",
      name: "shuffling",
      component: resolve => require(['@/views/ThePublic/shuffling'], resolve),
      meta: { title: "二维码" }
    },



  // 商家
  {
    path: "/me_index",
    name: "me_index",
    component: resolve => require(['@/views/merchant/me_index'], resolve),
    meta: { title: "商家中心" }
  },
  {
    path: "/me_association",
    name: "me_association",
    component: resolve => require(['@/views/merchant/me_association'], resolve),
    meta: { title: "社群" }
  },
  {
    path: "/me_indent",
    name: "me_indent",
    component: resolve => require(['@/views/merchant/me_indent'], resolve),
    meta: { title: "订单" }
  },
  {
    path: "/me_goods",
    name: "me_goods",
    component: resolve => require(['@/views/merchant/me_goods'], resolve),
    meta: {
      title:"商品"
      // isUseCache: false,
      // keepAlive: true
    }
  },
  {
    path: "/me_mine",
    name: "me_mine",
    component: resolve => require(['@/views/merchant/me_mine'], resolve),
    meta: { title: "我的" }
  },
  {
    path: "/me_account",
    name: "me_account",
    component: resolve => require(['@/views/merchant/me_account'], resolve),
    meta: { title: "员工账号" }
  },
  {
    path: "/me_actxinjian",
    name: "me_actxinjian",
    component: resolve => require(['@/views/merchant/me_actxinjian'], resolve),
    meta: { title: "新建员工账号" }
  },
  {
    path: "/me_shop",
    name: "me_shop",
    component: resolve => require(['@/views/merchant/me_shop'], resolve),
    meta: { title: "店铺设置" }
  },
  {
    path: "/me_newgoods",
    name: "me_newgoods",
    component: resolve => require(['@/views/merchant/me_newgoods'], resolve),
    meta: { title: "发布商品" }
  },
  {
    path: "/me_shopbianji",
    name: "me_shopbianji",
    component: resolve => require(['@/views/merchant/me_shopbianji'], resolve),
    meta: { title: "编辑店铺" }
  },
  {
    path: "/me_leimu",
    name: "me_leimu",
    component: resolve => require(['@/views/merchant/me_leimu'], resolve),
    meta: { title: "商品类目" }
  },
  {
    path: "/me_shoukuan",
    name: "me_shoukuan",
    component: resolve => require(['@/views/merchant/me_shoukuan'], resolve),
    meta: { title: "收款码" }
  },
  {
    path: "/me_usermsg",
    name: "me_usermsg",
    component: resolve => require(['@/views/merchant/me_usermsg'], resolve),
    meta: { title: "个人信息" }
  },
  {
    path: "/me_secure",
    name: "me_secure",
    component: resolve => require(['@/views/merchant/me_secure'], resolve),
    meta: { title: "账户与安全" }
  },
  {
    path: "/me_phonebianji",
    name: "me_phonebianji",
    component: resolve => require(['@/views/merchant/me_phonebianji'], resolve),
    meta: { title: "更改绑定手机号" }
  },
  {
    path: "/indent_fahuo",
    name: "indent_fahuo",
    component: resolve => require(['@/views/merchant/indent_fahuo'], resolve),
    meta: { title: "发货" }
  },
  {
    path: "/me_money",
    name: "me_money",
    component: resolve => require(['@/views/merchant/me_money'], resolve),
    meta: { title: "我的钱包" }
  },
  {
    path: "/money_tixian",
    name: "money_tixian",
    component: resolve => require(['@/views/merchant/money_tixian'], resolve),
    meta: { title: "提现" }
  },
  {
    path: "/me_tuikuan",
    name: "me_tuikuan",
    component: resolve => require(['@/views/merchant/me_tuikuan'], resolve),
    meta: { title: "退款详情" }
  },
  {
    path: "/me_tkjujue",
    name: "me_tkjujue",
    component: resolve => require(['@/views/merchant/me_tkjujue'], resolve),
    meta: { title: "拒绝退款" }
  },
  {
    path: "/me_suctuikuan",
    name: "/me_suctuikuan",
    component: resolve => require(['@/views/merchant/me_suctuikuan'], resolve),
    meta: { title: "退款成功" }
  },
  {
    path: "/me_zhibiao",
    name: "/me_zhibiao",
    component: resolve => require(['@/views/merchant/me_zhibiao'], resolve),
    meta: { title: "指标详情" }
  },
  {
    path: "/me_goodsmiaoshu",
    name: "/me_goodsmiaoshu",
    component: resolve => require(['@/views/merchant/me_goodsmiaoshu'], resolve),
    meta: { title: "商品描述" }
  },
  {
    path: "/me_fahuodetails",
    name: "/me_fahuodetails",
    component: resolve => require(['@/views/merchant/me_fahuodetails'], resolve),
    meta: { title: "发货详情" }
  },
  {
    path: "/me_morenwl",
    name: "/me_morenwl",
    component: resolve => require(['@/views/merchant/me_morenwl'], resolve),
    meta: { title: "默认物流" }
  },
  {
    path: "/me_goodsyulan",
    name: "/me_goodsyulan",
    component: resolve => require(['@/views/merchant/me_goodsyulan'], resolve),
    meta: { title: "预览商品" }
  },
  {
    path: "/me_favorable",
    name: "/me_favorable",
    component: resolve => require(['@/views/merchant/me_favorable'], resolve),
    meta: { title: "优惠劵" }
  },
  {
    path: "/me_juanbianji",
    name: "/me_juanbianji",
    component: resolve => require(['@/views/merchant/me_juanbianji'], resolve),
    meta: { title: "编辑优惠劵" }
  },
  {
    path: "/me_monxq",
    name: "/me_monxq",
    component: resolve => require(['@/views/merchant/me_monxq'], resolve),
    meta: { title: "收益明细" }
  },
  {
    path: "/me_wechat",
    name: "/me_wechat",
    component: resolve => require(['@/views/merchant/me_wechat'], resolve),
    meta: { title: "修改微信号" }
  },
  {
    path: "/new_product",
    name: "/new_product",
    component: resolve => require(['@/views/personal/new_product'], resolve),
    meta: { title: "新品上市" }
  },
  {
    path: "/new_tuijian",
    name: "/new_tuijian",
    component: resolve => require(['@/views/personal/new_tuijian'], resolve),
    meta: { title: "为你推荐" }
  },
  {
    path: "/new_coupon",
    name: "/new_coupon",
    component: resolve => require(['@/views/personal/new_coupon'], resolve),
    meta: { title: "领卷中心" }
  },
  {
    path: "/new_nine",
    name: "/new_nine",
    component: resolve => require(['@/views/personal/new_nine'], resolve),
    meta: { title: "9.9包邮" }
  },
  {
    path: "/new_seckill",
    name: "/new_seckill",
    component: resolve => require(['@/views/personal/new_seckill'], resolve),
    meta: { title: "每日秒杀" }
  },
  {
    path: "/new_baokuan",
    name: "/new_baokuan",
    component: resolve => require(['@/views/personal/new_baokuan'], resolve),
    meta: { title: "爆款专区" }
  },
  {
    path: "/new_classify",
    name: "/new_classify",
    component: resolve => require(['@/views/personal/new_classify'], resolve),
    meta: { title: "分类商品" }
  },
  {
    path: "/new_merchant",
    name: "/new_merchant",
    component: resolve => require(['@/views/personal/new_merchant'], resolve),
    meta: { title: "商家认证" }
  },
  {
    path: "/new_hudong",
    name: "/new_hudong",
    component: resolve => require(['@/views/personal/new_hudong'], resolve),
    meta: { title: "互动" }
  },
  {
    path: "/new_chat",
    name: "/new_chat",
    component: resolve => require(['@/views/personal/new_chat'], resolve),
    meta: { title: "客服" }
  },
  {
    path: "/me_client",
    name: "/me_client",
    component: resolve => require(['@/views/merchant/me_client'], resolve),
    meta: { title: "客户留言" }
  },
  {
    path: "/entrance",
    name: "/entrance",
    component: resolve => require(['@/views/personal/entrance'], resolve),
    meta: { title: "聊天列表" }
  },
  {
    path: "/new_sqma",
    name: "/new_sqma",
    component: resolve => require(['@/views/merchant/new_sqma'], resolve),
    meta: { title: "社群二维码名片" }
  },
  {
    path: "/new_vitacart",
    name: "/new_vitacart",
    component: resolve => require(['@/views/business/new_vitacart'], resolve),
    meta: { title: "我的卡劵" }
  },
  {
    path: "/me_drainage",
    name: "/me_drainage",
    component: resolve => require(['@/views/merchant/me_drainage'], resolve),
    meta: { title: "引流工具" }
  },
  {
    path: "/new_swim",
    name: "/new_swim",
    component: resolve => require(['@/views/business/new_swim'], resolve),
    meta: { title: "畅游购物卡" }
  },
  {
    path: "/new_enter",
    name: "/new_enter",
    component: resolve => require(['@/views/merchant/new_enter'], resolve),
    // meta: { title: "商家入驻" }
  },
  ]
})
/*PermissionIllustrate
//http://test.ruitaowang.com/wap/gxt/?fk=1-35-0-no  测试 %u963F%u8A07   https://gitee.com/xxxwin/ruitao-server-side.git
    //http://test.ruitaowang.com/wap/wx/login/?fk=1-35-0-no     共系统
    //http://test.ruitaowang.com/wap/wx/login/?fk=1-35-0-no
    //http://test.ruitaowang.com/wap/wx/login?fk=1-23-0-0       传媒
    //http://192.168.1.105:8088/wap/gxt/?fk=1-35-1-no-65366
    http://192.168.1.105:8088/wap/gxt/?fk=1-35-1-no-13
    // http://192.168.1.105:8088/wap/gxt/?fk=1-35-1-no-13
    // sh bootstrap/deploy_test.sh
    // sh bootstrap/deploy_test_push.sh
    //  window.localStorage.
    //  localStorage.headid
//    config get requirepass
//   config set requirepass Fuckyou2
//    auth Fuckyou2
    C:\Program Files\Redis
   // 百度开发密钥
   // http://api.map.baidu.com/api?v=2.0&ak=tWs61pjuk8e17zCv6E7gib6WybPmixs2
*/