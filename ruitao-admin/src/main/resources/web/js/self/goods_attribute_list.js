/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/goodsAttribute',
    updateUrl: '/api/goodsAttribute',
    deleteUrl: '/api/goodsAttribute',
    selectUrl: '/api/goodsAttribute'
};

// var ConfigType = {
//     selectUrl: "/api/admin/goodsType/get"
// };
// var dataType;

$(document).ready(function () {


    // $.ajax(
    //     {
    //         url: ConfigType.selectUrl,
    //         type: "GET",
    //         dataType: "json",
    //         async: false,
    //         success: function (data, textStatus, jqXHR) {
    //             dataType = data.data;
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             console.error(errorThrown);
    //         }
    //     }
    // );


    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#goods_attribute_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","属性名称", "所属类型","创建时间","操作"],
        colModel: [{
            name: "attrId",
            index: "attrId",
            width: 10
        },{
            name: "attrName",
            index: "attrName",
            width: 30
        }, {
            name: "goodsType.typeName",
            index: "goodsType.typeName",
            width: 25
        }, {
            name: "ctime",
            index: "ctime",
            width: 15
        }, {
            name: "operate",
            index: "operate",
            width: 20,
            sortable: false
        }
        ],
        pager: "#pager_goods_type_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "attrId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#goods_attribute_list").jqGrid('navGrid', '#pager_goods_type_list', {
        add: false,
        edit: false,
        del: false,
        search: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#goods_attribute_list").setGridWidth(width);
    });
    // $.ajax(
    //     {
    //         url: Config.selectUrl,
    //         type: "GET",
    //         dataType: "json",
    //         async: false,
    //         success: function (data, textStatus, jqXHR) {
    //
    //             //add operate for data
    //             if (data && data.data && data.data.length > 0) {
    //                 for (var i = 0; i < data.data.length; i++) {
    //                     var id = data.data[i].attrId;
    //                     data.data[i].id=data.data[i].attrId;
    //                     var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/goodsAttribute/update/' + id + '" data-index="0">编辑</a>';
    //                     html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + "," + (i + 1) + ")' >删除</a>";
    //                     data.data[i].operate = html;
    //                     // data.data[i].attrId= getTypeNameLabel(id);
    //
    //
    //                 }
    //             }
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             console.error(errorThrown);
    //         }
    //     });

});

// function getTypeNameLabel(typeId) {
//     if (dataType && dataType.length > 0) {
//         for (var i = 0; i < dataType.length; i++) {
//             if (typeId == dataType[i].typeId) {
//                 var typeHtml = "<label id='" + dataType[i].typeId + "'>" + dataType[i].typeName + "</label>";
//                 typeId = typeHtml;
//                 break;
//             }
//         }
//     }
//     return typeId;
// }
function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#goods_attribute_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#goods_attribute_list").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#goods_attribute_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#goods_attribute_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/goodsAttribute/update/'+id+'" data-index="0">编辑</a>';
        html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + ","+  (i+1) +")' >删除</a>";
        jQuery("#goods_attribute_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
            mtime: dataFromTheRow.mtime
        });
    }
}