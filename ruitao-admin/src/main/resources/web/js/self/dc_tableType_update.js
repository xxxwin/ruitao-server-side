/**
 * 
 */

var Config = {
    createUrl: '/api/admin/dc/tableType',
    updateUrl: '/api/admin/dc/tableType',
    deleteUrl: '/api/admin/dc/tableType',
    selectUrl: '/api/admin/dc/tableType',
    selectCompanyUrl:'/api/dc/list/company'
};
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
    $('#submitForUpdate').click(function () {
        var companyId =$("input[name=companyId]").val();
        var name = $("input[id=table_name]").val();
        var numMin = $("input[id=numMin]").val();
        var numMax = $("input[id=numMax]").val();
        var remark = $("input[name=remark]").val();
        $.ajax(
            {url: Config.updateUrl,
                type: "PUT",
                data: {name:name,numMin:numMin,numMax:numMax,remark:remark,companyId:companyId},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        return false;
    });

});

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    
}