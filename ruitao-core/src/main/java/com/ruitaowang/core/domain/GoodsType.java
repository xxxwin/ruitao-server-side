package com.ruitaowang.core.domain;

public class GoodsType extends Page {
    /**
     *  商品类型编号,所属表字段为goods_type.type_id
     */
    private Long typeId;

    /**
     *  商品类型名称,所属表字段为goods_type.type_name
     */
    private String typeName;

    /**
     *  商品类型组,所属表字段为goods_type.type_group
     */
    private String typeGroup;

    /**
     *  1 启用
0 不启用,所属表字段为goods_type.enabled
     */
    private Boolean enabled;

    /**
     *  创建时间,所属表字段为goods_type.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为goods_type.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常
1 删除,所属表字段为goods_type.rstatus
     */
    private Byte rstatus;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName == null ? null : typeName.trim();
    }

    public String getTypeGroup() {
        return typeGroup;
    }

    public void setTypeGroup(String typeGroup) {
        this.typeGroup = typeGroup == null ? null : typeGroup.trim();
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}