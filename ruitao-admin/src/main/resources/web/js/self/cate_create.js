var Config = {
    createUrl: "/api/admin/categories/create",
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$().ready(function () {

    $('#submitForSave').click(function () {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax({
                url: Config.createUrl,
                type: "POST",
                data: postData,
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    if (data.type ==="ERROR"){
                        toastr.success(data.msg, data.title);
                    } else {
                        // CONST_GLOBAL_COMPANY_ID = data.data.companyId;
                        toastr.success(data.msg, data.title);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        }
        return false;
    });
});





