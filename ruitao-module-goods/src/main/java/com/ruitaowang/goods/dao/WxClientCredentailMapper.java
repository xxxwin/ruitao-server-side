package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.WxClientCredentail;
import java.util.List;

public interface WxClientCredentailMapper {
    int insert(WxClientCredentail wxClientCredentail);

    int deleteByPK(Long id);

    int updateByPK(WxClientCredentail wxClientCredentail);

    WxClientCredentail selectByPK(Long id);

    long count(WxClientCredentail wxClientCredentail);

    List<WxClientCredentail> select(WxClientCredentail wxClientCredentail);
}