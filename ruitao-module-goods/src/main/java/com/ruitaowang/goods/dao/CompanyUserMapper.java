package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CompanyUser;

import java.util.List;

public interface CompanyUserMapper {
    int insert(CompanyUser companyUser);

    int deleteByPK(Long id);

    int updateByPK(CompanyUser companyUser);

    CompanyUser selectByPK(Long id);

    long count(CompanyUser companyUser);

    List<CompanyUser> select(CompanyUser companyUser);
}