package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UploadImage;
import java.util.List;

public interface UploadImageMapper {
    int insert(UploadImage uploadImage);

    int deleteByPK(Long imageId);

    int updateByPK(UploadImage uploadImage);

    UploadImage selectByPK(Long imageId);

    long count(UploadImage uploadImage);

    List<UploadImage> select(UploadImage uploadImage);
}