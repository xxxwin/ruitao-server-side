package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsExtras;
import java.util.List;

public interface GoodExtrasMapper {
    int insert(GoodsExtras goodsExtras);

    int deleteByPK(Long goodsId);

    int updateByPK(GoodsExtras goodsExtras);

    GoodsExtras selectByPK(Long goodsId);

    long count(GoodsExtras goodsExtras);

    List<GoodsExtras> select(GoodsExtras goodsExtras);
}