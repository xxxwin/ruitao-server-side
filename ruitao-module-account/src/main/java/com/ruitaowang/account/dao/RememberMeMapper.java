package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.RememberMe;

public interface RememberMeMapper {
    int insert(RememberMe param);

    int deleteByPK(String series);

    int updateByPK(RememberMe param);

    RememberMe selectByPK(String series);
}