package com.ruitaowang.core.domain;

public class LsHistoricalTrace extends Page {
    /**
     *  自增编码,所属表字段为ls_historical_trace.trace_id
     */
    private Long traceId;

    /**
     *  用户编码,所属表字段为ls_historical_trace.user_id
     */
    private Long userId;

    /**
     *  公司编码,所属表字段为ls_historical_trace.company_id
     */
    private Long companyId;

    /**
     *  创建时间,所属表字段为ls_historical_trace.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为ls_historical_trace.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为ls_historical_trace.remark
     */
    private String remark;

    /**
     *  0 正常
1 删除,所属表字段为ls_historical_trace.rstatus
     */
    private Byte rstatus;

    public Long getTraceId() {
        return traceId;
    }

    public void setTraceId(Long traceId) {
        this.traceId = traceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}