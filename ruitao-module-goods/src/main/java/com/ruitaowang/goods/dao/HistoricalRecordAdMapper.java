package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.HistoricalRecord;

public interface HistoricalRecordAdMapper {
    int update(HistoricalRecord historicalRecord);
}
