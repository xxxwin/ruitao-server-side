var Config = {
    createUrl: '/api/wallet',
    updateUrl: '/api/wallet',
    deleteUrl: '/api/wallet',
    selectUrl: '/api/admin/usersProfit',
    selectUrlSum:'/api/admin/usersProfit/sum'
};
function getQueryString(name,orderId) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var reg11 = new RegExp("(^|&)" + orderId + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
$(document).ready(function () {
    // $.ajax(
    //     {
    //         url: Config.selectUrlSum,
    //         type: "GET",
    //         data:{userId: getQueryString("id")},
    //         dataType: "json",
    //         success: function (data, textStatus, jqXHR) {
    //             console.log(data);
    //             console.log(data.data);
    //             $("input[id=sumUserProfit]").val(toFixedForPrice(data.data.userProfit));
    //             $("input[id=sumUserExpenses]").val(toFixedForPrice(data.data.userExpenses));
    //             $("input[id=sumUserScore]").val(data.data.amount);
    //             $("input[id=sumUserScoreExpenses]").val(data.data.userScoreExpenses);
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             toastr.error(errorThrown, textStatus);
    //         }
    //     });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        postData: {userId: getQueryString("id"),orderId: getQueryString("orderId")},
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "消费者ID", "消费者", "订单ID", "返利类型", "订单金额", "收入", "手续费","支出","余额", "时间"],
        colModel: [{
            name: "profitId",
            index: "profitId",
            editable: false,
            sorttype: "int",
            search: true
        }, {
            name: "consumerId",
            index: "consumerId",
            sorttype: "int",
        }, {
            name: "remark",
            index: "remark",
            editable: true,
        }, {
            name: "orderId",
            index: "orderId",
            editable: true,
        }, {
            name: "orderType",
            index: "orderType",
            editoptions: {value: "0:商城供货商订单返利;1:线下店扫码订单返利;2:商城微商订单返利;3:地面店扫码订单返红包;4:商城微商订单返红包;5:点餐订单返红包;6:点餐订单返利;7:微传媒·年;8:云省返利;9:云签返利;10:云飞返利;11:云推返利;12:云点返利;13:云联返利；14:云促销返利；15:快赚大礼包;16:云动返利;23:微传媒·月;24:微传媒·季;25:微传媒·半年;26:高级合伙人权限;-1:提现;-2:退款扣除奖励;-3:红包支出"},
            editable: true,
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "amount",
            index: "amount",
            editable: true
        }, {
            name: "userProfit",
            index: "userProfit",
            editable: true
        }, {
            name: "moneyReduce",
            index: "moneyReduce",
            editable: true
        }, {
            name: "userExpenses",
            index: "userExpenses",
            editable: true
        }, {
            name: "userBalance",
            index: "userBalance",
            editable: true
        },{
            name: "ctime",
            index: "ctime",
            editable: true
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "profitId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

});
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
};
$('#buttonGoback').click(function () {
    history.go(-1);
    location.reload();
    return false;
});


function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS") {
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    } else {
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
};

function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        console.log(dataFromTheRow.orderType);
        if (Number(dataFromTheRow.orderType)==3 || Number(dataFromTheRow.orderType) ==4 || Number(dataFromTheRow.orderType) ==5 || Number(dataFromTheRow.orderType) == -3) {
            jQuery("#data_list").jqGrid('setRowData', ids[i], {
                ctime: dataFromTheRow.ctime
            });
        } else {
            jQuery("#data_list").jqGrid('setRowData', ids[i], {
                userProfit: toFixedForPrice(dataFromTheRow.userProfit),
                userExpenses: toFixedForPrice(dataFromTheRow.userExpenses),
                amount:toFixedForPrice(dataFromTheRow.amount),
                moneyReduce:toFixedForPrice(dataFromTheRow.moneyReduce),
                ctime: dataFromTheRow.ctime
            });
        }
    };
};