/**
 * Created by Administrator on 2017/4/18.
 */
/**
 * Created by neal on 11/22/16.
 */
var CONST_GLOBAL_COMPANY_ID;
var Config = {
    createUrl: '/api/admin/company',
    updateUrl: '/api/admin/company',
    deleteUrl: '/api/company',
    selectUrl: '/api/company',
    updateAuditUrl: '/api/admin/company/audit',
    selectLogisticsNameUrl: '/api/admin/logistics/name',

    createEXUrl: "/api/admin/ws/company/exPrice"
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$().ready(function () {
    $.ajax(
        {
            url: Config.selectLogisticsNameUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var logisticsName = [];
                logisticsName.push('<option value=""></option>');
                data.data.map(function (logistics) {
                    logisticsName.push('<option value="');
                    logisticsName.push(logistics.logisticsId);
                    logisticsName.push('" ');
                    logisticsName.push('hassubinfo="true">');
                    logisticsName.push(logistics.name);
                    logisticsName.push('</option>');
                });
                $(".logisticsName").append(logisticsName.join(''));
            }
        });

    $.ajax(
        {
            url: "/api/districts",
            type: "GET",
            data: {parentid: 0, page: 1, rows: 1000},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                if (data && data.data && data.data.length > 0) {
                    var tagsDBHtml = '';
                    var tagsHBHtml='';
                    var tagsHDHtml='';
                    var tagsZNHtml='';
                    var tagsXNHtml='';
                    var tagsXBHtml='';
                    var tagsGATHtml='';
                    for (var i = 0; i < data.data.length; i++) {
                        if (data.data[i].ctime ==1){
                            tagsHBHtml += '<label><input name="tags" class="rt-hb" type="checkbox" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>    ';
                        }else if (data.data[i].ctime ==2){
                            tagsDBHtml += '<label><input name="tags" class="rt-db" type="checkbox" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>    ';
                        }else if (data.data[i].ctime == 3){
                            tagsHDHtml += '<label><input name="tags" class="rt-hd" type="checkbox" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>    ';
                        }else if (data.data[i].ctime == 4){
                            tagsZNHtml += '<label><input name="tags" class="rt-zn" type="checkbox" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>    ';
                        }else if (data.data[i].ctime == 5){
                            tagsXNHtml += '<label><input name="tags" class="rt-xn" type="checkbox" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>    ';
                        }else if (data.data[i].ctime == 6){
                            tagsXBHtml += '<label><input name="tags" class="rt-xb" type="checkbox" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>    ';
                        }else if (data.data[i].ctime == 7){
                            tagsGATHtml += '<label><input name="tags" class="rt-gat" type="checkbox" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>    ';
                        }
                    }
                    $("#tagsallHB").append(tagsHBHtml);
                    $("#tagsallDB").append(tagsDBHtml);
                    $("#tagsallHD").append(tagsHDHtml);
                    $("#tagsallZN").append(tagsZNHtml);
                    $("#tagsallXN").append(tagsXNHtml);
                    $("#tagsallXB").append(tagsXBHtml);
                    $("#tagsallGAT").append(tagsGATHtml);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });

    $('#buttonGoback').click(function () {
        history.go(-1);
        location.reload();
        return false;
    });
});
$("#tagsHB").click(function () {
    $(this).attr("checked", !$(this).attr("checked"));
    if ($(this).is(":checked")) {
        $('.rt-hb').prop("checked", true);
    }else {
        $('.rt-hb').removeAttr('checked');
    }
});
$("#tagsDB").click(function () {
    $(this).attr("checked", !$(this).attr("checked"));
    if ($(this).is(":checked")) {
        $('.rt-db').prop("checked", true);
    }else {
        $('.rt-db').removeAttr('checked');
    }
});
$("#tagsHD").click(function () {
    $(this).attr("checked", !$(this).attr("checked"));
    if ($(this).is(":checked")) {
        $('.rt-hd').prop("checked", true);
    }else {
        $('.rt-hd').removeAttr('checked');
    }
});
$("#tagsZN").click(function () {
    $(this).attr("checked", !$(this).attr("checked"));
    if ($(this).is(":checked")) {
        $('.rt-zn').prop("checked", true);
    }else {
        $('.rt-zn').removeAttr('checked');
    }
});
$("#tagsXN").click(function () {
    $(this).attr("checked", !$(this).attr("checked"));
    if ($(this).is(":checked")) {
        $('.rt-xn').prop("checked", true);
    }else {
        $('.rt-xn').removeAttr('checked');
    }
});
$("#tagsXB").click(function () {
    $(this).attr("checked", !$(this).attr("checked"));
    if ($(this).is(":checked")) {
        $('.rt-xb').prop("checked", true);
    }else {
        $('.rt-xb').removeAttr('checked');
    }
});
$("#tagsGAT").click(function () {
    $(this).attr("checked", !$(this).attr("checked"));
    if ($(this).is(":checked")) {
        $('.rt-gat').prop("checked", true);
    }else {
        $('.rt-gat').removeAttr('checked');
    }
});

$('#submitExCreate').click(function () {
    if ($('#logisticsName option:selected').val() == "" || $('#logisticsName option:selected').val() == null){
        alert("请选择快递");
        return false;
    }
    var logisticsId=$('#logisticsName option:selected').val();
    if ($('#companyId option:selected').val()==0 || $('#companyId option:selected').val() == ""){
        alert("请选择商户");
        return false;
    }
    var companyId=$('#companyId option:selected').val();
    var price = $('input[name=price]').val() * 100;
    var heightExtraPrice = $('input[name=heightExtraPrice]').val() * 100;
    var heightBase = $('input[name=heightBase]').val();
    var Ex ="";
    $('input:checkbox[name=tags]:checked').each(function () {
        if ($(this).val() != ""){
            Ex += $(this).val()+",";
        }
    });
    $.ajax({
        url: Config.createEXUrl,
        type: "POST",
        data: {companyId:companyId,logisticsId:logisticsId,price: price, provinceId: Ex,heightExtraPrice:heightExtraPrice,heightBase:heightBase},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            $(":checkbox").attr("checked",false);
            toastr.success(data.msg, data.title);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return false;
});





