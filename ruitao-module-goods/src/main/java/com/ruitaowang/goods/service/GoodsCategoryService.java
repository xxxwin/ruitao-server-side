package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.CategoryRecommend;
import com.ruitaowang.core.domain.GoodsCategory;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CategoryRecommendMapper;
import com.ruitaowang.goods.dao.GoodsCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class GoodsCategoryService implements BaseService<GoodsCategory> {
    @Autowired
    private GoodsCategoryMapper goodsCategoryMapper;
    @Autowired
    private CategoryRecommendMapper categoryRecommendMapper;

    @Override
    public GoodsCategory insert(GoodsCategory goodsCategory) {
        Long time = System.currentTimeMillis();
        goodsCategory.setCtime(time);
        goodsCategory.setMtime(time);
        goodsCategoryMapper.insert(goodsCategory);
        return goodsCategory;
    }

    public GoodsCategory insert(GoodsCategory goodsCategory, Byte[] categoryRecommend) {
        this.insert(goodsCategory);
        if(categoryRecommend==null || categoryRecommend.length == 0){
            return goodsCategory;
        }
        updateRecommend(goodsCategory,categoryRecommend);

        return goodsCategory;
    }

    private void updateRecommend(GoodsCategory goodsCategory,Byte[] categoryRecommend) {
        CategoryRecommend categoryRecommend1 = null;
        CategoryRecommend whereClause1 = new CategoryRecommend();
        whereClause1.setCategoryId(goodsCategory.getCategoryId());
        whereClause1.setRstatus((byte) 1);
        categoryRecommendMapper.updateByPK(whereClause1);
        for (int i=0; i < categoryRecommend.length; i++){
            categoryRecommend1 = new CategoryRecommend();
            categoryRecommend1.setCategoryId(goodsCategory.getCategoryId());
            categoryRecommend1.setRecommendType(categoryRecommend[i]);
            Long time = System.currentTimeMillis();

            categoryRecommend1.setMtime(time);
            //同时写入Recommend
            CategoryRecommend whereClause = new CategoryRecommend();
            whereClause.setCategoryId(goodsCategory.getCategoryId());
            whereClause.setRecommendType(categoryRecommend1.getRecommendType());
            if(categoryRecommendMapper.select(whereClause).size()>0){
                categoryRecommend1.setRstatus((byte) 0);
                categoryRecommendMapper.updateByPK(categoryRecommend1);
            }else{
                categoryRecommend1.setCtime(time);
                categoryRecommendMapper.insert(categoryRecommend1);
            }
        }
    }

    @Override
    public int delete(Long id) {
        GoodsCategory goodsCategory = new GoodsCategory();
        goodsCategory.setCategoryId(id);
        goodsCategory.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        goodsCategory.setMtime(time);
        return goodsCategoryMapper.updateByPK(goodsCategory);
    }

    @Override
    public GoodsCategory update(GoodsCategory goodsCategory) {
        Long time = System.currentTimeMillis();
        goodsCategory.setMtime(time);
        goodsCategoryMapper.updateByPK(goodsCategory);
        return goodsCategory;
    }

    public GoodsCategory update(GoodsCategory goodsCategory, Byte[] categoryRecommend) {
        this.update(goodsCategory);
        if(categoryRecommend==null || categoryRecommend.length == 0){
            return goodsCategory;
        }
        updateRecommend(goodsCategory,categoryRecommend);
        return goodsCategory;
    }

    @Override
    public GoodsCategory selectByPK(Long id) {
        return goodsCategoryMapper.selectByPK(id);
    }

    @Override
    public List<GoodsCategory> select(GoodsCategory goodsCategory) {
        goodsCategory.setRstatus((byte) 0);
        goodsCategory.setRecords(goodsCategoryMapper.count(goodsCategory));
        return goodsCategoryMapper.select(goodsCategory);
    }

    /* recommend */
    public List<CategoryRecommend> selectRecommend(Long categoryId) {
        CategoryRecommend categoryRecommend = new CategoryRecommend();
        categoryRecommend.setRstatus((byte) 0);
        categoryRecommend.setCategoryId(categoryId);
        return categoryRecommendMapper.select(categoryRecommend);
    }
}
