package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsAttributeLink;
import java.util.List;

public interface GoodsAttributeLinkMapper {
    int insert(GoodsAttributeLink goodsAttributeLink);

    int deleteByPK(Long id);

    int updateByPK(GoodsAttributeLink goodsAttributeLink);

    GoodsAttributeLink selectByPK(Long id);

    long count(GoodsAttributeLink goodsAttributeLink);

    List<GoodsAttributeLink> select(GoodsAttributeLink goodsAttributeLink);
}