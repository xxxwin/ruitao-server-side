package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.domain.GoodsTypeCustom;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.GoodsTypeCustomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class APIGoodsTypeCustomController extends BaseController {

    @Autowired
    private GoodsTypeCustomService goodsTypeCustomService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("/api/admin/goodsTypeCustom")
    public String adminList(GoodsTypeCustom goodsTypeCustom) {
        MsgOut o = MsgOut.success(goodsTypeCustomService.select(goodsTypeCustom));
        return this.renderJson(o);
    }
    /**
     * @api {get} /api/wap/goodsTypeCustom/page 自定义分类-(分页)查询所有
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom/page
     *
     * @apiName listForPage
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiParam {String} remark 备注.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {String} name 分类名称.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "name":"",
     *                   "companyId":0,
     *                   "ctime":1502870415259,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom/page"
     *  }
     */
    @RequestMapping("/api/wap/goodsTypeCustom/page")
    public String listForPage(GoodsTypeCustom goodsTypeCustom, int page,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        SysUser user = sysUserService.selectByPK(Long.parseLong(userId));
        goodsTypeCustom.setPage(page);
        if (!ObjectUtils.isEmpty(goodsTypeCustom.getSidx())) {
            goodsTypeCustom.setOrderBy(goodsTypeCustom.getSidx() + " " + goodsTypeCustom.getSord() + "," + goodsTypeCustom.getOrderBy());
        }
        Company company=companyService.selectByUserId(user.getId());
        if (ObjectUtils.isEmpty(company)){
            return this.renderJson(MsgOut.error("查询不到商户"));
        }
        goodsTypeCustom.setCompanyId(company.getCompanyId());
        MsgOut o = MsgOut.success(goodsTypeCustomService.selectForPage(goodsTypeCustom));
        o.setRecords(goodsTypeCustom.getRecords());
        o.setTotal(goodsTypeCustom.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping("/api/admin/goodsTypeCustom/page")
    public String adminListForPage(GoodsTypeCustom goodsTypeCustom, int page) {
        goodsTypeCustom.setPage(page);
        if (!ObjectUtils.isEmpty(goodsTypeCustom.getSidx())) {
            goodsTypeCustom.setOrderBy(goodsTypeCustom.getSidx() + " " + goodsTypeCustom.getSord() + "," + goodsTypeCustom.getOrderBy());
        }
        MsgOut o = MsgOut.success(goodsTypeCustomService.selectForPage(goodsTypeCustom));
        o.setRecords(goodsTypeCustom.getRecords());
        o.setTotal(goodsTypeCustom.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping("/api/ws/goodsTypeCustom/page")
    public String wsListForPage(GoodsTypeCustom goodsTypeCustom, int page) {
        goodsTypeCustom.setPage(page);
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(goodsTypeCustom.getSidx())) {
            goodsTypeCustom.setOrderBy(goodsTypeCustom.getSidx() + " " + goodsTypeCustom.getSord() + "," + goodsTypeCustom.getOrderBy());
        }
        MsgOut o = MsgOut.success(goodsTypeCustomService.selectForPage(goodsTypeCustom));
        o.setRecords(goodsTypeCustom.getRecords());
        o.setTotal(goodsTypeCustom.getTotal());
        return this.renderJson(o);
    }
    /**
     * @api {post} /api/wap/goodsTypeCustom 自定义分类-创建
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom
     *
     * @apiName create
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {String} name 分类名称.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "name":"",
     *                   "companyId":0,
     *                   "ctime":1502870415259,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom"
     *  }
     */
    @RequestMapping(value = "/api/wap/goodsTypeCustom", method = RequestMethod.POST)
    public String create(GoodsTypeCustom goodsTypeCustom,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        SysUser user = sysUserService.selectByPK(Long.parseLong(userId));
        Company company=companyService.selectByUserId(user.getId());
        if (ObjectUtils.isEmpty(company)){
            return this.renderJson(MsgOut.error("查询不到商户"));
        }
        goodsTypeCustom.setCompanyId(company.getCompanyId());
        goodsTypeCustomService.insert(goodsTypeCustom);
        MsgOut o = MsgOut.success(goodsTypeCustom);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/goodsTypeCustom", method = RequestMethod.POST)
    public String adminCreate(GoodsTypeCustom goodsTypeCustom) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company=companyService.selectByUserId(user.getId());
        if (ObjectUtils.isEmpty(company)){
            return this.renderJson(MsgOut.error("查询不到商户"));
        }
//        goodsTypeCustom.setCompanyId(company.getCompanyId());

        goodsTypeCustomService.insert(goodsTypeCustom);
        MsgOut o = MsgOut.success(goodsTypeCustom);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/ws/goodsTypeCustom", method = RequestMethod.POST)
    public String wsCreate(GoodsTypeCustom goodsTypeCustom) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company=companyService.selectByUserId(user.getId());
        if (ObjectUtils.isEmpty(company)){
            return this.renderJson(MsgOut.error("查询不到商户"));
        }
//        goodsTypeCustom.setCompanyId(company.getCompanyId());

        goodsTypeCustomService.insert(goodsTypeCustom);
        MsgOut o = MsgOut.success(goodsTypeCustom);
        return this.renderJson(o);
    }
    /**
     * @api {put} /api/wap/goodsTypeCustom 自定义分类-修改
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom
     *
     * @apiName update
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {String} name 分类名称.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "name":"",
     *                   "companyId":0,
     *                   "ctime":1502870415259,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom"
     *  }
     */
    @RequestMapping(value = "/api/wap/goodsTypeCustom", method = RequestMethod.PUT)
    public String update(GoodsTypeCustom goodsTypeCustom,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        goodsTypeCustomService.update(goodsTypeCustom);
        MsgOut o = MsgOut.success(goodsTypeCustom);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/goodsTypeCustom", method = RequestMethod.PUT)
    public String adminUpdate(GoodsTypeCustom goodsTypeCustom) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        goodsTypeCustomService.update(goodsTypeCustom);
        MsgOut o = MsgOut.success(goodsTypeCustom);
        return this.renderJson(o);
    }

    /**
     * @api {delete} /api/wap/goodsTypeCustom/{id} 自定义分类-删除
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom/{id}
     *
     * @apiName delete
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {long} id 评论ID.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":1
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom/{id}"
     *  }
     */
    @RequestMapping(value = "/api/wap/goodsTypeCustomDelete/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(goodsTypeCustomService.delete(id));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/goodsTypeCustomDelete/{id}", method = RequestMethod.DELETE)
    public String adminDelete(@PathVariable("id") Long id) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(goodsTypeCustomService.delete(id));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/ws/goodsTypeCustomDelete/{id}", method = RequestMethod.DELETE)
    public String wsDelete(@PathVariable("id") Long id) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(goodsTypeCustomService.delete(id));
        return this.renderJson(o);
    }

    /**
     * @api {get} /api/wap/goodsTypeCustom/{id} 自定义分类-单个查询
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom/{id}
     *
     * @apiName findCommentId
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {long} id 分类ID.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "name":"",
     *                   "companyId":0,
     *                   "ctime":1502870415259,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom/{id}"
     *  }
     */
    @RequestMapping(value = "/api/wap/goodsTypeCustom/{id}", method = RequestMethod.GET)
    public String findCommentId(@PathVariable("id")Long  id) {
        MsgOut o = MsgOut.success(goodsTypeCustomService.selectByPK(id));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/goodsTypeCustom/{id}", method = RequestMethod.GET)
    public String adminFindCommentId(@PathVariable("id")Long  id) {
        MsgOut o = MsgOut.success(goodsTypeCustomService.selectByPK(id));
        return this.renderJson(o);
    }
}
