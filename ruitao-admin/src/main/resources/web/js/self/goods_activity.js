/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/activities',
    updateUrl: '/api/activities',
    deleteUrl: '/api/activities',
    selectUrl: '/api/activities'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";

    $("#data_list").jqGrid({
        datatype: "json",
        url:Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "广告名称", "广告类型", "链接", "广告图片", "操作"],
        colModel: [{
            name: "activityId",
            index: "activityId",
            editable: false,
            width: 20,
            sorttype: "int",
            search: true
        }, {
            name: "activityTitle",
            index: "activityTitle",
            editable: true,
            width: 90
        }, {
            name: "activityType",
            index: "activityType",
            editable: true,
            width: 100,
        }, {
            name: "activityUrl",
            index: "activityUrl",
            editable: true,
            width: 80
        }, {
            name: "activityImageUrl",
            index: "activityImageUrl",
            editable: true,
            width: 100
        }, {
            name: "operate",
            index: "operate",
            width: 100,
            sortable: false
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "activityId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});

});
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}
$("#perform_search").click(function () {
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.activityType = $('#activityType option:selected').val();
    jQuery("#data_list").trigger("reloadGrid", [{page: 1}]);
});
function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS"){
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    }else{
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var item = jQuery("#data_list").jqGrid('getRowData', id);
        var activityType;
        switch (Number(item.activityType)){
            case 0:
                activityType = "首页-轮播广告(768x420)";
                break;
            case 1:
                activityType = "首页-通栏广告(768x164)";
                break;
            case 2:
                activityType = "首页-热门单品-左图(375x400)";
                break;
            case 3:
                activityType = "首页-热门单品-右图(375x200)";
                break;
            case 4:
                activityType = "首页-PC轮播广告(1420x400)";
                break;
            case 5:
                activityType = "官网首页-公司动态";
                break;
            case 6:
                activityType = "官网首页-公司新闻";
                break;
            case 7:
                activityType = "官网首页-行业资讯";
                break;
            case 8:
            activityType = "H5-广告";
            break;
            case 9:
                activityType = "商家联盟-轮播图(768x420)";
                break;
            case 10:
                activityType = "秒赚-广告图片(768x420)";
                break;
            case 11:
                activityType = "龙蛙理念(768x420)";
                break;
            case 12:
                activityType = "首象热点(768x420)";
                break;
            case 13:
                activityType = "生活(768x420)";
                break;
        }
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/activity/update/' + id + '" data-index="0">编辑</a>';
        html += "<a style='color:#f60' onclick='Delete(" + id + ")'>删除</a>";
        var activityImageUrl = "<img style='height: 50px;' src='"+item.activityImageUrl+"'/>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {activityType: activityType, activityImageUrl: activityImageUrl, operate: html});
    }
}