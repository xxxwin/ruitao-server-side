package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Verifycode;

import java.util.List;

public interface VerifycodeAdMapper {

    int count(Verifycode verifycode);
}