package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.MzAds;
import java.util.List;

public interface MzAdsMapper {
    int insert(MzAds mzAds);

    int deleteByPK(Long adId);

    int updateByPK(MzAds mzAds);

    MzAds selectByPK(Long adId);

    long count(MzAds mzAds);

    List<MzAds> select(MzAds mzAds);
}