/**
 * Created by neal on 12/16/16.
 */

var Config = {
    loginUrl: '/api/companyProduct/myCashier'
};

$(document).ready(function () {

$.ajax(
    {
        url : Config.loginUrl,
        dataType: "json",
        success:function(data)
        {
            if(data && data.type == 'SUCCESS'){
                if(data.data.length > 0 ){
                    $("#myCashireQrCode").attr("src", data.data[0].qrcodeUrl);
                }
            }else {
                toastr.error(data.msg, data.title);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            toastr.error(errorThrown, textStatus);
        }
    });
});