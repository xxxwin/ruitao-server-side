package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserReward;
import java.util.List;

public interface UserRewardMapper {
    int insert(UserReward userReward);

    int deleteByPK(Long rewardId);

    int updateByPK(UserReward userReward);

    UserReward selectByPK(Long rewardId);

    long count(UserReward userReward);

    List<UserReward> select(UserReward userReward);
}