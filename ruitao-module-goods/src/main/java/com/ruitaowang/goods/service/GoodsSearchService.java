package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Goods;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsSearchMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class GoodsSearchService implements BaseService<Goods>{

    @Autowired
    private GoodsSearchMapper goodsSearchMapper;

    @Override
    public List<Goods> select(Goods goods) {
        return goodsSearchMapper.select(goods);
    }

    public List<Goods> searchForPage(Goods goods) {
        goods.setRstatus((byte) 0);
        goods.setRecords(goodsSearchMapper.count(goods));
        return goodsSearchMapper.select(goods);
    }

    @Override
    public Goods insert(Goods goods) {
        return null;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public Goods update(Goods goods) {
        return null;
    }

    @Override
    public Goods selectByPK(Long id) {
        return null;
    }
}
