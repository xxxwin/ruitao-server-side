package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Collection;
import java.util.List;

public interface CollectionMapper {
    int insert(Collection collection);

    int deleteByPK(Long id);

    int updateByPK(Collection collection);

    Collection selectByPK(Long id);

    long count(Collection collection);

    List<Collection> select(Collection collection);
}