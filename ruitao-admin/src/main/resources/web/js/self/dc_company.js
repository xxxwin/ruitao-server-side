/**
 * Created by Shaka on 12/21/16.
 */

var Config = {
    selectUrl: "/api/admin/dc/company",
    deleteUrl: '/api/company',
    updateUrlForCellEdit: '/api/admin/company/cellEdit'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#company_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 550,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 3,
        colNames: ["编号", "商家店名", "联系人", "手机号码","地址", "详细地址", "审核状态", "操作"],
        colModel: [{
            name: "companyId",
            index: "companyId",
            search: true,
            width: 5
        }, {
            name: "companyName",
            index: "companyName",
            width: 15
        }, {
            name: "linkman",
            index: "linkman",
            width: 10
        }, {
            name: "mobile",
            index: "mobile",
            width: 10
        }, {
            name: "districtId",
            index: "districtId",
            formatter: myformatter,
            width: 15
        }, {
            name: "address",
            index: "address",
            width: 15
        }, {
            name: "companyStatus",
            index: "companyStatus",
            editoptions: {value: "0:未审核;1:已审核;2:审核未通过"},
            edittype: 'select',
            formatter: 'select',
            width: 10
        }, {
            name: "operate",
            index: "operate",
            width: 5
        }],
        pager: "#pager_company_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "companyId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#company_list").jqGrid('navGrid', '#pager_company_list', {add: false, edit: false, del: false,search: false,
        refresh: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#company_list").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#company_list").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#company_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#company_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var html = "<a style='color:#f60;padding-right: 15px' onclick='companyUpdate(" + id + ")' >详情</a>";
        // html += "<a style='color:#f60;padding-right: 15px' onclick='consoleThis(" + id + ")' >控制台</a>";
        // html += "<a style='color:#f60;padding-right: 15px' onclick='dcDish(" + id + ")' >菜品</a>";
        // html += "<a style='color:#f60;padding-right: 15px' onclick='dcDishCategory(" + id + ")' >菜品分类</a>";
        // html += "<a style='color:#f60;padding-right: 15px' onclick='dcTable(" + id + ")' >餐桌</a>";
        // html += "<a style='color:#f60;padding-right: 15px' onclick='dcTableType(" + id + ")' >餐桌分类</a>";
        jQuery("#company_list").jqGrid('setRowData', ids[i], {operate: html});
    }
}

function myformatter(cellvalue, options, rowObject) {
    return rowObject.province + rowObject.city + rowObject.district||'';
}
function companyUpdate(id) {
    layer.open({
        type: 2,
     //   title: '请选择区域',
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content: "/admin/dc/company/update/"+id
    })
}
// function dcDish(id) {
//     layer.open({
//         type: 2,
//         //   title: '请选择区域',
//         scrollbar: false,
//         shadeClose: true,
//         shade: 0.8,
//         area: ['100%', '100%'],
//         content: "/web/dc_dish.html?id="+id
//     })
// }
// function dcDishCategory(id) {
//     layer.open({
//         type: 2,
//         //   title: '请选择区域',
//         scrollbar: false,
//         shadeClose: true,
//         shade: 0.8,
//         area: ['100%', '100%'],
//         content: "/web/dc_dish_category.html?id="+id
//     })
// }
// function dcTable(id) {
//     layer.open({
//         type: 2,
//         //   title: '请选择区域',
//         scrollbar: false,
//         shadeClose: true,
//         shade: 0.8,
//         area: ['100%', '100%'],
//         content: "/web/dc_table.html?id="+id
//     })
// }
// function dcTableType(id) {
//     layer.open({
//         type: 2,
//         //   title: '请选择区域',
//         scrollbar: false,
//         shadeClose: true,
//         shade: 0.8,
//         area: ['100%', '100%'],
//         content: "/web/dc_tableType.html?id="+id
//     })
// }
// function consoleThis(id) {
//     layer.open({
//         type: 2,
//         //   title: '请选择区域',
//         scrollbar: false,
//         shadeClose: true,
//         shade: 0.8,
//         area: ['100%', '100%'],
//         content: "/web/dc_console.html?id="+id
//     })
// }
