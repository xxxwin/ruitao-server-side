package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Activity;
import com.ruitaowang.core.domain.ActivityVo;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.ActivityService;
import com.ruitaowang.goods.service.GoodsAttributeLinkService;
import com.ruitaowang.goods.service.GoodsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;

/**
 *
 * 商品信息
 */
@RestController
public class PageActivityController extends BaseController {

    @Autowired
    private ActivityService activityService;
    @Autowired
    private GoodsService goodsService;

    /**
     * 商品更新
     */
    @RequestMapping(value = {"/activity/update/{activityId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("activityId") Long activityId, Model model) {
        Activity activity = activityService.selectByPK(activityId);
        ActivityVo activityVo=new ActivityVo();
        BeanUtils.copyProperties(activity,activityVo);
        if (activity.getGoodsId() != 0){
            activityVo.setGoodsName(goodsService.selectByPK(activity.getGoodsId()).getGoodsName());
        }else {
            activityVo.setGoodsName("");
        }
        //Activity activity = activityService.selectByPK(activityId);
        model.addAttribute("activity", activity);
        model.addAttribute("activityVo", activityVo);
        return new ModelAndView("web/activity_update");
    }
}