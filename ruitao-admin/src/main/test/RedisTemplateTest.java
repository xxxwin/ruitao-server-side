import com.ruitaowang.admin.Application;
import com.ruitaowang.core.utils.RandomUtils;
import com.ruitaowang.goods.service.LsMsgService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;

/**
 * Created by neal on 12/09/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class RedisTemplateTest {

    @Autowired
    RedisTemplate<Object, Object> redisTemplate;
    @Autowired
    LsMsgService msgService;

    @Test
    public void testFaLunckMoney() throws Exception {
        String key = "lucky_money:4";
        ListOperations<Object, Object> operations = redisTemplate.opsForList();
        if(operations.size(key)==0){
            Arrays.stream(RandomUtils.luckyMoney(10000, 10)).forEach(item->{operations.leftPush(key, item);});
        }
    }

    @Test
    public void testChaiLunckMoney() throws Exception {
        String key = "lucky_money:4";
        ListOperations<Object, Object> operations = redisTemplate.opsForList();
        System.out.println(operations.leftPop(key));
    }

    /**判断redis数据库是否有对应的key*/
    @Test
    public void TestExist(){
        String key = "lucky_money:4";
        ListOperations<Object, Object> operations = redisTemplate.opsForList();
        System.out.println(operations.size(key));
    }
}