package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserCoupon;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserCouponMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserCouponService implements BaseService<UserCoupon>{

    @Autowired
    private UserCouponMapper userCouponMapper;
    @Override
    public UserCoupon insert(UserCoupon userCoupon) {
        Long time = System.currentTimeMillis();
        userCoupon.setCtime(time);
        userCoupon.setMtime(time);
        userCouponMapper.insert(userCoupon);
        return userCoupon;
    }

    @Override
    public int delete(Long id) {
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setId(id);
        userCoupon.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userCoupon.setMtime(time);
        return userCouponMapper.updateByPK(userCoupon);
    }

    @Override
    public UserCoupon update(UserCoupon userCoupon) {
        Long time = System.currentTimeMillis();
        userCoupon.setMtime(time);
        userCouponMapper.updateByPK(userCoupon);
        return userCoupon;
    }

    @Override
    public UserCoupon selectByPK(Long id) {

        return userCouponMapper.selectByPK(id);
    }

    @Override
    public List<UserCoupon> select(UserCoupon userCoupon) {
        userCoupon.setRstatus((byte) 0);
        return userCouponMapper.select(userCoupon);
    }

    public List<UserCoupon> selectForPage(UserCoupon userCoupon) {
        userCoupon.setRstatus((byte) 0);
        userCoupon.setRecords(userCouponMapper.count(userCoupon));
        return userCouponMapper.select(userCoupon);
    }
}
