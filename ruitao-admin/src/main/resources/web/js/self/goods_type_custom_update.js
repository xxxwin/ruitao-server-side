
var Config = {
    updateUrl: '/api/admin/goodsTypeCustom',
    selectCompanyUrl: '/api/admin/company/providers'
};

$().ready(function () {
    $.ajax(
        {
            url: Config.selectCompanyUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="" hassubinfo="true">选择商户</option>');
                data.data.map(function (company) {
                    categories.push('<option value="');
                    categories.push(company.companyId);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(company.companyName);
                    categories.push('</option>');
                });
                $("#companyId").html(categories.join(''));
                $("#companyId").val($("#goodsProviderIdTemp").val());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });

    $('#submitCustomUpdate').click(function () {

        $("#ajaxform").validate();
        //如果验证通过
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url: Config.updateUrl,
                    type: "PUT",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});

$('#buttonGoback').click(function() {
    history.go(-1);
    location.reload();
    return false;
});