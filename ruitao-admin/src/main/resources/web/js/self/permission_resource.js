/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/resources',
    updateUrl: '/api/resources',
    deleteUrl: '/api/resources',
    selectUrl: '/api/resources'
};
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";

    $.ajax(
        {
            url: Config.selectUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var resources = [];
                resources.push('<option value="0" hassubinfo="true">顶级菜单</option>');
                data.data.map(function(item){
                    resources.push('<option value="');
                    resources.push(item.id);
                    resources.push('" ');
                    if ($("#ppid").val() == item.pid){
                        resources.push('selected="selected">');
                    }else{
                        resources.push('>');
                    }
                    resources.push(item.name);
                    resources.push('</option>');
                });
                $("#pid").html(resources.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });

    $.ajax(
    {
        url: Config.selectUrl,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            //add operate for data
            if(data && data.data && data.data.length>0){
                for (var i=0;i<data.data.length;i++){
                    var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/permission_resource_update/'+data.data[i].id+'" data-index="0">编辑</a>';
                    html += "<a href='#'   style='color:#f60' onclick='Delete(" + data.data[i].id +")' >删除</a>";
                    data.data[i].operate = html;
                }
            }
            $("#data_list").jqGrid({
                datatype: "local",
                data: data.data,
                height: 450,
                autowidth: true,
                shrinkToFit: true,
                rowNum: 12,
                colNames: ["编号", "资源名称", "类型", "图标", "链接", "父编号", "操作"],
                colModel: [{
                    name: "id",
                    index: "id",
                    editable: false,
                    width: 20,
                    sorttype: "int",
                    search: false
                }, {
                    name: "name",
                    index: "name",
                    editable: false,
                    width: 100
                }, {
                    name: "type",
                    index: "type",
                    editable: false,
                    sortable: true,
                    editoptions: {value: "0:API;1:HTML"},
                    edittype: 'select',
                    formatter: 'select',
                    width: 20
                }, {
                    name: "icon",
                    index: "icon",
                    editable: false,
                    width: 20
                }, {
                    name: "link",
                    index: "link",
                    editable: false,
                    width: 100
                }, {
                    name: "pid",
                    index: "pid",
                    editable: false,
                    width: 20
                }, {
                name: "operate",
                index: "operate",
                width: 100,
                sortable: false
            }],
                pager: "#pager_data_list",
                viewrecords: true,
                hidegrid: false,
                gridComplete:function(){  }
            });
            jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
            $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.error(errorThrown);
        }
    });

    //create menu
    $('#submitForSave').click(function () {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url: Config.createUrl,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                        if (data && data.type == 'SUCCESS'){
                            location.reload();
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData',id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url : url,
            type: action,
            success:function(data, textStatus, jqXHR)
            {
                if(action == 'DELETE'){
                    jQuery("#data_list").setGridParam({url:url}).trigger("reloadGrid");
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                toastr.error(errorThrown);
            }
        });
}