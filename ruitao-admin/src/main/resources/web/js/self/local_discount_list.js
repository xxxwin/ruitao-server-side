/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/localDiscount',
    updateUrl: '/api/localDiscount',
    deleteUrl: '/api/localDiscount',
    selectUrl: '/api/localDiscount'
};


$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
        $("#goods_attribute_list").jqGrid({
            datatype: "json",
            url: Config.selectUrl,
            height: 450,
            autowidth: true,
            shrinkToFit: true,
            rowNum: 12,
            colNames: ["ID", "等级名称","折扣%", "所需积分", "备注", "操作"],
            colModel: [{
                name: "discountId",
                index: "discountId",
                width: 90
            },{
                name: "title",
                index: "title",
                width: 90
            }, {
                name: "discount",
                index: "discount",
                width: 80,
            }, {
                name: "score",
                index: "score",
                width: 80
            }, {
                name: "remark",
                index: "remark",
                sorttype: "int",
                width: 80
            }, {
                name: "operate",
                index: "operate",
                width: 100,
                sortable: false
            }
            ],
            pager: "#pager_goods_type_list",
            viewrecords: true,
            hidegrid: false,
            jsonReader: {
                root: "data", "page": "page", total: "total",
                records: "records", repeatitems: false, id: "discountId"
            },
            gridComplete: function () {
                dataWrapper();
            }
        });
        jQuery("#goods_attribute_list").jqGrid('navGrid', '#pager_goods_type_list', {
            add: false,
            edit: false,
            del: false
        });
        $(window).bind("resize", function () {
            var width = $(".jqGrid_wrapper").width();
            $("#goods_attribute_list").setGridWidth(width);
        });

});


function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS"){
                        jQuery("#goods_attribute_list").jqGrid('delRowData', id);
                        jQuery("#goods_attribute_list").trigger("reloadGrid");
                    }else{
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("删除失败，error.");
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#goods_attribute_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#goods_attribute_list').jqGrid('getRowData', id);
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/localDiscount/update/' + id + '" data-index="0">编辑</a>';
        html += "<a style='color:#f60' onclick='Delete(" + id + ")' >删除</a>";
        jQuery("#goods_attribute_list").jqGrid('setRowData', ids[i], {operate: html,});
    }
}