package com.ruitaowang.wap;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.ActivityUserLink;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.ActivityUserLinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class APIWapActivityUserController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private ActivityUserLinkService activityUserLinkService;

    /**
     * 阅读量点击进入
     * @param page
     * @param activityUserLink
     * @return
     */
    @RequestMapping(value = "/api/wap/activityForRead", method = RequestMethod.GET)
    public String wapCommentPlPage(int page, ActivityUserLink activityUserLink){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        activityUserLink.setPage(page);
        activityUserLink.setAuthor(user.getId());
        List<ActivityUserLink> activityUserLinkList = activityUserLinkService.selectForReadPage(activityUserLink);
        MsgOut o = MsgOut.success(activityUserLinkList);
        o.setRecords(activityUserLink.getRecords());
        o.setTotal(activityUserLink.getTotal());
        return this.renderJson(o);
    }

    /**
     * 阅读量个数
     * @param activityUserLink
     * @return
     */
    @RequestMapping(value = "/api/wap/activityCount", method = RequestMethod.GET)
    public String activityCount(ActivityUserLink activityUserLink){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Map map = new HashMap();
        activityUserLink.setAuthor(user.getId());
        //阅读量总数
        Long activityCount = activityUserLinkService.count(activityUserLink);
        Long stime = TimeUtils.getLongDayBegin();
        Long etime = TimeUtils.getLongDayEnd();
        activityUserLink.setStime(stime);
        activityUserLink.setEtime(etime);
        //今日阅读量
        Long todayCount = activityUserLinkService.todayCount(activityUserLink);
        map.put("activityCount",activityCount);
        map.put("todayCount",todayCount);
        MsgOut o = MsgOut.success(map);
        return this.renderJson(o);
    }
}
