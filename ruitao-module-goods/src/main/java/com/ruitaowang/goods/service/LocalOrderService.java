package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Activity;
import com.ruitaowang.core.domain.LocalOrder;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.LocalOrderAdMapper;
import com.ruitaowang.goods.dao.LocalOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 1/3/17.
 */
@Service
public class LocalOrderService implements BaseService<LocalOrder> {
    @Autowired
    private LocalOrderMapper localOrderMapper;
    @Autowired
    private LocalOrderAdMapper localOrderAdMapper;

    @Override
    public LocalOrder insert(LocalOrder localOrder) {
        Long time = System.currentTimeMillis();
        localOrder.setCtime(time);
        localOrder.setMtime(time);
        localOrderMapper.insert(localOrder);
        return localOrder;
    }

    @Override
    public int delete(Long id) {
        LocalOrder localOrder = new LocalOrder();
        localOrder.setOrderId(id);
        localOrder.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        localOrder.setMtime(time);
        return localOrderMapper.updateByPK(localOrder);
    }

    @Override
    public LocalOrder update(LocalOrder localOrder) {
        Long time = System.currentTimeMillis();
        localOrder.setMtime(time);
        localOrderMapper.updateByPK(localOrder);
        return localOrder;
    }

    @Override
    public LocalOrder selectByPK(Long id) {
        return localOrderMapper.selectByPK(id);
    }

    @Override
    public List<LocalOrder> select(LocalOrder localOrder) {
        localOrder.setRstatus((byte) 0);
        return localOrderMapper.select(localOrder);
    }

    public List<LocalOrder> selectForPage(LocalOrder localOrder) {
        localOrder.setRstatus((byte) 0);
        localOrder.setRecords(localOrderMapper.count(localOrder));
        return localOrderMapper.select(localOrder);
    }

    public List<LocalOrder> fjOrder(LocalOrder localOrder) {
        localOrder.setRecords(localOrderAdMapper.fjOrderCount(localOrder));
        return localOrderAdMapper.fjOrder(localOrder);
    }

}
