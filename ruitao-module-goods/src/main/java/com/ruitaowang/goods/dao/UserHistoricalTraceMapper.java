package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserHistoricalTrace;
import java.util.List;

public interface UserHistoricalTraceMapper {
    int insert(UserHistoricalTrace userHistoricalTrace);

    int deleteByPK(Long id);

    int updateByPK(UserHistoricalTrace userHistoricalTrace);

    UserHistoricalTrace selectByPK(Long id);

    long count(UserHistoricalTrace userHistoricalTrace);

    List<UserHistoricalTrace> select(UserHistoricalTrace userHistoricalTrace);
}