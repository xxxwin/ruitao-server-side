package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ShoppingCart;
import java.util.List;

public interface ShoppingCartMapper {
    int insert(ShoppingCart shoppingCart);

    int deleteByPK(Long cartId);

    int updateByPK(ShoppingCart shoppingCart);

    ShoppingCart selectByPK(Long cartId);

    long count(ShoppingCart shoppingCart);

    List<ShoppingCart> select(ShoppingCart shoppingCart);
}