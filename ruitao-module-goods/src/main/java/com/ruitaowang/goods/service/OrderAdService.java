package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Order;
import com.ruitaowang.core.domain.OrderVO;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.OrderAdMapper;
import com.ruitaowang.goods.dao.RuiTaoActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/5/9.
 */
@Service
public class OrderAdService implements BaseService<Order> {
    @Autowired
    private OrderAdMapper orderAdMapper;
    @Autowired
    private RuiTaoActivityMapper ruiTaoActivityMapper;

    @Override
    public Order insert(Order order) {
        return null;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public Order update(Order order) {
        return null;
    }

    @Override
    public Order selectByPK(Long id) {
        return null;
    }

    @Override
    public List<Order> select(Order order) {
        return orderAdMapper.select(order);
    }


    public List<Order> searchForPage(Order order) {
        order.setRstatus((byte) 0);
        order.setRecords(orderAdMapper.count(order));
        return orderAdMapper.select(order);
    }

    public List<OrderVO> selectByCompany(OrderVO orderVO) {
        return orderAdMapper.selectByCompany(orderVO);
    }

//    public List<Order> selectForOrder0(Order order){
//        order.setRecords(orderAdMapper.selectForOrderCount0(order));
//        return orderAdMapper.selectForOrder0(order);
//    }
//
//    public List<Order> selectForOrder1(Order order){
//        order.setRecords(orderAdMapper.selectForOrderCount1(order));
//        return orderAdMapper.selectForOrder1(order);
//    }
}