/**
 * Created by Shaka on 12/21/16.
 */

var Config = {
    selectUrl: '/api/localorders',

};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $.ajax(
        {
            url: Config.selectUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {

                // //add operate for data
                if (data && data.data && data.data.length > 0) {
                    for (var i = 0; i < data.data.length; i++) {
                        var id = data.data[i].orderId;
                        data.data[i].ctime = new Date(data.data[i].ctime).yyyymmddhhmmss();
                        data.data[i].price = (data.data[i].price / 100).toFixed(2);
                    }
                }
                $("#goods_list").jqGrid({
                    datatype: "local",
                    data: data.data,
                    height: 450,
                    autowidth: true,
                    shrinkToFit: true,
                    rowNum: 12,
                    colNames: ["订单编号", "消费者", "支付状态", "审核状态", "金额(元）", "订单时间"],
                    colModel: [{
                        name: "orderSn",
                        index: "orderSn",
                        width: 40,
                    }, {
                        name: "remark",
                        index: "remark",
                        width: 15
                    }, {
                        name: "payStatus",
                        index: "payStatus",
                        width: 15
                    }, {
                        name: "auditStatus",
                        index: "auditStatus",
                        width: 15,
                    }, {
                        name: "price",
                        index: "price",
                        width: 15
                    }, {
                        name: "ctime",
                        index: "ctime",
                        width: 30
                    }],
                    pager: "#pager_goods_list",
                    viewrecords: true,
                    hidegrid: false,
                    gridComplete: function () {
                    }
                });
                jQuery("#goods_list").jqGrid('navGrid', '#pager_goods_list', {add: false, edit: false, del: false});
                // jQuery('#goods_list').trigger('reloadGrid');
                $(window).bind("resize", function () {
                    var width = $(".jqGrid_wrapper").width();
                    $("#goods_list").setGridWidth(width);
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });

});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#goods_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#goods_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
