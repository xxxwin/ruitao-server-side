function getQueryString(id) {
    var reg = new RegExp("(^|&)" + id + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
var Config = {
    selectUrl: '/api/admin/userProfit/all'
};
$(document).ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        postData: {userId: getQueryString("id")},
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "消费者", "订单ID","收益","订单金额", "时间"],
        colModel: [{
            name: "profitId",
            index: "profitId",
            editable: false,
            sorttype: "int",
            width: 5,
            search: true
        },{
            name: "nickname",
            index: "nickname",
            // formatter:myformatter,
            width: 15,
            editable: true
        }, {
            name: "orderSn",
            index: "orderSn",
            editable: true,
            width: 15
        },{
            name: "userProfit",
            index: "userProfit",
            width: 10,
            editable: true
        }, {
            name: "amount",
            index: "amount",
            width: 10,
            editable: true
        },{
            name: "ctime",
            index: "ctime",
            width: 15,
            editable: true
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "profitId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

});
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
};
$('#buttonGoback').click(function () {
    history.go(-1);
    location.reload();
    return false;
});



$("#perform_search").click(function () {
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.stime = dateToEpoch($("#beginDate").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    postdata.etime = dateToEpoch($("#endDate").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    jQuery("#data_list").trigger("reloadGrid", postdata);
});
$("#type_search").click(function () {
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.orderType =$('#orderType option:selected').val();
    jQuery("#data_list").trigger("reloadGrid", postdata);
});

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS") {
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    } else {
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
};

function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        jQuery("#data_list").jqGrid('setRowData', ids[i], {
                ctime: dataFromTheRow.ctime,
                amount:toFixedForPrice(dataFromTheRow.amount),
                userProfit: toFixedForPrice(dataFromTheRow.userProfit),
                userBalance: toFixedForPrice(dataFromTheRow.userBalance)
            });

    };
};

function myformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax({
        url: "/api/admin/users/ByPK",
        data:{userId:rowObject.userId},
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            name = data.data.nickname;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return name;
}