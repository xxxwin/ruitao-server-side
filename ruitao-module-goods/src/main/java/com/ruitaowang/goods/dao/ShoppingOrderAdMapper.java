package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ShoppingOrder;
import com.ruitaowang.core.domain.ShoppingOrderVO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ShoppingOrderAdMapper {
    List<ShoppingOrder> selectForPj(ShoppingOrderVO shoppingOrderVO);

    long selectForPjCount(ShoppingOrderVO shoppingOrderVO);

    List<ShoppingOrder> selectForCompanyPj(ShoppingOrder shoppingOrder);

    long selectForCompanyPjCount(ShoppingOrder shoppingOrder);

    List<ShoppingOrder> selectForOrder(ShoppingOrder shoppingOrder);

    long selectForOrderCount(ShoppingOrder shoppingOrder);

    //今日成交金额
    @Select("SELECT sum(amount) as todayMoeny FROM rtshop.shopping_order where ctime > #{stime} and ctime < #{etime} and company_id = #{companyId} and pay_status = 1 and rstatus = 0")
    String todayMoeny(ShoppingOrderVO shoppingOrderVO);

    //今日成交订单
    @Select("SELECT count(1) FROM rtshop.shopping_order where ctime > #{stime} and ctime < #{etime} and company_id = #{companyId} and pay_status = 1 and rstatus = 0")
    long orderSuccess(ShoppingOrderVO shoppingOrderVO);

    List<ShoppingOrder> fjOrder(ShoppingOrder shoppingOrder);

    long fjOrderCount(ShoppingOrder shoppingOrder);

}
