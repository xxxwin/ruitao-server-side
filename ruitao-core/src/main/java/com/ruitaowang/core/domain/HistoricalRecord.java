package com.ruitaowang.core.domain;

public class HistoricalRecord extends Page {
    /**
     *  自增主键,所属表字段为historical_record.id
     */
    private Long id;

    /**
     *  用户,所属表字段为historical_record.user_id
     */
    private Long userId;

    /**
     *  创建时间,所属表字段为historical_record.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为historical_record.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为historical_record.rstatus
     */
    private Byte rstatus;

    /**
     *  搜索历史name,所属表字段为historical_record.history_name
     */
    private String historyName;

    /**
     *  搜索区域,所属表字段为historical_record.history_type
     */
    private String historyType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getHistoryName() {
        return historyName;
    }

    public void setHistoryName(String historyName) {
        this.historyName = historyName == null ? null : historyName.trim();
    }

    public String getHistoryType() {
        return historyType;
    }

    public void setHistoryType(String historyType) {
        this.historyType = historyType == null ? null : historyType.trim();
    }
}