package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.ArticleLibrary;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ArticleLibraryMapper {

    int insert(ArticleLibrary articleLibrary);

    int deleteByPK(Long id);

    int updateByPK(ArticleLibrary articleLibrary);

    ArticleLibrary selectByPK(Long id);

    long count(ArticleLibrary articleLibrary);

    List<ArticleLibrary> select(ArticleLibrary articleLibrary);

    @Select("SELECT * FROM rtshop.article_library where article_url= #{articleUrl} and rstatus = 0")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "articleClicks", column = "article_clicks"),
    })
    ArticleLibrary selectUrl(String articleUrl);
}