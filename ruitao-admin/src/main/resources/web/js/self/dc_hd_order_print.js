/**
 *
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
$(document).ready(function () {
    pt(getQueryString("id"))
});
function pt(orderid) {
    var orderData;
    var orderProvicerData;
    $.ajax({
        url: "/api/admin/dc/order/"+orderid,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            orderData=data.data.order;
            orderProvicerData=data.data.orderProvider;
            console.log(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
}

$(".dayin button").click(function () {
    window.print();
});