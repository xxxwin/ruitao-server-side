/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/company',
    updateUrl: '/api/admin/company',
    deleteUrl: '/api/company',
    selectLogisticsNameUrl: '/api/admin/logistics/name',
    updateAuditUrl: '/api/admin/company/audit',

    selectUrl: '/api/admin/ws/company/exprice',
    updateExUrl: '/api/admin/ws/company/exprice',
    updateUrlForCellEdit: '/api/admin/ws/company/exPrice/cellEdit'
};
$().ready(function () {
    $.ajax(
        {
            url: Config.selectLogisticsNameUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var logisticsName = [];
                logisticsName.push('<option value="">选择</option>');
                data.data.map(function (logistics) {
                    logisticsName.push('<option value="');
                    logisticsName.push(logistics.logisticsId);
                    logisticsName.push('" ');
                    logisticsName.push('hassubinfo="true">');
                    logisticsName.push(logistics.name);
                    logisticsName.push('</option>');
                });
                $(".logisticsName").append(logisticsName.join(''));
            }
        });
    $.ajax(
        {
            url: "/api/districts/page",
            type: "GET",
            data: {parentid: 0, page: 1, rows: 1000},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var districts = [];
                districts.push('<option value="">选择</option>');
                data.data.map(function (district) {
                    districts.push('<option value="');
                    districts.push(district.id);
                    districts.push('" ');
                    districts.push('hassubinfo="true">');
                    districts.push(district.name);
                    districts.push('</option>');
                });
                $(".district").append(districts.join(''));
            }
        });
    $('#buttonGoback').click(function () {
        history.go(-1);
        location.reload();
        return false;
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#company_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "省","快递公司", "运费","首重","续重价格", "时间"],
        colModel: [{
            name: "expriceId",
            index: "expriceId",
            editable: false,
            sorttype: "int",
            search: true
        }, {
            name: "provinceName",
            index: "provinceName",
            editable: true
        },{
            name: "logisticsName",
            index: "logisticsName",
            editable: true
        }, {
            name: "price",
            index: "price",
            editable: true
        }, {
            name: "heightBase",
            index: "heightBase",
            editable: true
        }, {
            name: "heightExtraPrice",
            index: "heightExtraPrice",
            editable: true
        },{
            name: "ctime",
            index: "ctime",
            editable: true
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        pager: "#pager_company_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "expriceId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper()
        }
    });
    jQuery("#company_list").jqGrid('navGrid', '#pager_company_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#company_list").setGridWidth(width);
    });

});
$('#submitExUpdate').click(function () {
    var logisticsId = $('#logisticsId option:selected').val();
    var price = $('input[name=price]').val() *100;
    var provinceId = $('#provinceId option:selected').val()
    $.ajax({
        url: Config.updateExUrl,
        type: "PUT",
        data: {logisticsId: logisticsId, price: price, provinceId: provinceId},
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            $('input[name=price]').val("")
            toastr.success(data.msg, data.title);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    jQuery("#company_list").trigger("reloadGrid", [{page: 1}]);
    return false;
});

$("#perform_search").click(function () {
    var postdata = $("#company_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    if ($('#logisticsName option:selected').val() != "") {
        postdata.logisticsId = $('#logisticsName option:selected').val();
    }
    if ($('#province option:selected').val() != "") {
        postdata.provinceId = $('#province option:selected').val();
    }
    jQuery("#company_list").trigger("reloadGrid", [{page: 1}]);
});
function dataWrapper() {
    var ids = jQuery("#company_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#company_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        jQuery("#company_list").jqGrid('setRowData', ids[i], {
            price: toFixedForPrice(dataFromTheRow.price),
            ctime: dataFromTheRow.ctime
        });

    }
}