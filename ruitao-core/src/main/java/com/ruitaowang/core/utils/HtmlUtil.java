package com.ruitaowang.core.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 解析html工具
 * Created by neal on 10/03/2017.
 */
public class HtmlUtil {
    public static void main(String[] args) throws IOException {
        String url = "https://mp.weixin.qq.com/s?__biz=MjM5ODIyMTE0MA==&mid=2650969421&idx=1&sn=5334af80a08f9e442db77530c27eec6e&key=c529fc820a281546e308a6651bc87904a1082e54652e5d77ba3faebf860a190009d2a8672901005d76da45d9f658500bd8870ad961b1f96653548a535465aacffb12d424b4e8f31ec66b5a7672bc3d89&ascene=0&uin=MTI4ODgxMzM1&devicetype=iMac+MacBookPro11%2C1+OSX+OSX+10.12.4+build(16E195)&version=12000410&nettype=WIFI&fontScale=100&pass_ticket=s2HnDkJvjfRhquQcuHFRBRsdBpTorm2ReucxdLWj%2BGo%3D";
        Document doc = Jsoup.connect(url).get();
        Element content = doc.getElementById("img-content");//获取id为img-content的dom节点
        System.out.println(content.toString());
    }

    public static Map<String, String> splitQuery(String imageUrl) throws Exception {
        URL url = new URL(imageUrl);
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }
}
