/**
 * Created by xinchunting on 17-9-25.
 */
/**
 * Created by xinchunting on 17-9-25.
 */
$(document).ready(function () {

});

function nowFunction(name){
    var url="";
    var title=[];
    if (name == "brandStory"){
        url="/web/admin_dinner_textBox.html?id="+name;
        title=["品牌故事","font-size:20px;"];
        skip(url,title);
    }
    if (name == "ventureHistory"){
        url="/web/admin_dinner_textBox.html?id="+name;
        title=["创业历程","font-size:20px;"];
        skip(url,title);
    }
    if (name == "foodPhilosophy"){
        url="/web/admin_dinner_textBox.html?id="+name;
        title=["美食哲学","font-size:20px;"];
        skip(url,title);
    }
    if (name == "shopSpecialty"){
        url="/web/admin_dinner_textBox.html?id="+name;
        title=["本店特色","font-size:20px;"];
        skip(url,title);
    }
    if (name == "dinnersCulture"){
        url="/web/admin_dinner_textBox.html?id="+name;
        title=["食客文化","font-size:20px;"];
        skip(url,title);
    }
    if (name == "aboutUs"){
        url="/web/admin_dinner_textBox.html?id="+name;
        title=["关于我们","font-size:20px;"];
        skip(url,title);
    }
}
function skip(url,title) {
    layer.open({
        type: 2,
        title:title,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:url
    })
}