package wx.wechat.common;

public class Configure {
    // 这个就是自己要保管好的私有Key了（切记只能放在自己的后台代码里，不能放在任何可能被看到源代码的客户端程序中）
    // 每次自己Post数据给API的时候都要用这个key来对所有字段进行签名，生成的签名会放在Sign这个字段，API收到Post数据的时候也会用同样的签名算法对Post过来的数据进行签名和验证
    // 收到API的返回的时候也要用这个key来对返回的数据算下签名，跟API的Sign数据进行比较，如果值不一致，有可能数据被第三方给篡改

    //微信登陆回调地址
    public static String  hostOSS = "http://static.ruitaowang.com";
    public static String  hostAPI = "https://www.ruitaowang.com";
    public static String  hostWap = "https://www.ruitaowang.com";
    public static String returnUri = hostWap + "/wap/enroll";//入口页面

    //微信分配的公众号ID（开通公众号之后可以获取到）d678efh567hg6787
//    public static String appID = "wx488ae3f57360b7ea";
    public static String appID = "wxf364689a02930bc7";

    //微信分配的公众号密钥
//    public static String appSecret = "8a9d7e1f721555f328808f5c11790411";
    public static String appSecret = "38eb9d398723e14d4a6a643ce1be88a7";

    //微信的内置的token
    public static String appToken = "ruitaowang";

    //微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
    public static String mchID = "1499520652";

    //微信支付分配给商户的Key
    public static String mchKey = "e10adc3949ba59abbe56e057f20f883e";

    //接收微信支付异步通知回调地址，通知url必须为直接可访问的url，不能携带参数。
    public static String wxpay_notify_url = hostAPI + "/api/wxs/notify";

    //受理模式下给子商户分配的子商户号
    public static String subMchID = "";

    //HTTPS证书的本地路径
    public static String certLocalPath = "";

    //HTTPS证书密码，默认密码等于商户号MCHID
    public static String certPassword = "";

    //是否使用异步线程的方式来上报API测速，默认为异步模式
    public static boolean useThreadToDoReport = true;

    //机器IP
    public static String ip = "";

    //以下是几个API的路径：
    //1）被扫支付API
    public static String PAY_API = "https://api.mch.weixin.qq.com/pay/micropay";

    //2）被扫支付查询API
    public static String PAY_QUERY_API = "https://api.mch.weixin.qq.com/pay/orderquery";

    //3）退款API
    public static String REFUND_API = "https://api.mch.weixin.qq.com/secapi/pay/refund";

    //4）退款查询API
    public static String REFUND_QUERY_API = "https://api.mch.weixin.qq.com/pay/refundquery";

    //5）撤销API
    public static String REVERSE_API = "https://api.mch.weixin.qq.com/secapi/pay/reverse";

    //6）下载对账单API
    public static String DOWNLOAD_BILL_API = "https://api.mch.weixin.qq.com/pay/downloadbill";

    //7) 统计上报API
    public static String REPORT_API = "https://api.mch.weixin.qq.com/payitil/report";

    public static boolean isUseThreadToDoReport() {
        return useThreadToDoReport;
    }

    public static void setUseThreadToDoReport(boolean useThreadToDoReport) {
        Configure.useThreadToDoReport = useThreadToDoReport;
    }

    public static String getAppID() {
        return appID;
    }

    public static void setAppID(String appID) {
        Configure.appID = appID;
    }

    public static String getAppSecret() {
        return appSecret;
    }

    public static void setAppSecret(String appSecret) {
        Configure.appSecret = appSecret;
    }

    public static String getAppToken() {
        return appToken;
    }

    public static void setAppToken(String appToken) {
        Configure.appToken = appToken;
    }

    public static String getMchID() {
        return mchID;
    }

    public static void setMchID(String mchID) {
        Configure.mchID = mchID;
    }

    public static String getMchKey() {
        return mchKey;
    }

    public static void setMchKey(String mchKey) {
        Configure.mchKey = mchKey;
    }

    public static String getWxpay_notify_url() {
        return wxpay_notify_url;
    }

    public static void setWxpay_notify_url(String wxpay_notify_url) {
        Configure.wxpay_notify_url = wxpay_notify_url;
    }

    public static String getSubMchID() {
        return subMchID;
    }

    public static void setSubMchID(String subMchID) {
        Configure.subMchID = subMchID;
    }

    public static String getCertLocalPath() {
        return certLocalPath;
    }

    public static void setCertLocalPath(String certLocalPath) {
        Configure.certLocalPath = certLocalPath;
    }

    public static String getCertPassword() {
        return certPassword;
    }

    public static void setCertPassword(String certPassword) {
        Configure.certPassword = certPassword;
    }

    public static String getIp() {
        return ip;
    }

    public static void setIp(String ip) {
        Configure.ip = ip;
    }

    public static String getPayApi() {
        return PAY_API;
    }

    public static void setPayApi(String payApi) {
        PAY_API = payApi;
    }

    public static String getPayQueryApi() {
        return PAY_QUERY_API;
    }

    public static void setPayQueryApi(String payQueryApi) {
        PAY_QUERY_API = payQueryApi;
    }

    public static String getRefundApi() {
        return REFUND_API;
    }

    public static void setRefundApi(String refundApi) {
        REFUND_API = refundApi;
    }

    public static String getRefundQueryApi() {
        return REFUND_QUERY_API;
    }

    public static void setRefundQueryApi(String refundQueryApi) {
        REFUND_QUERY_API = refundQueryApi;
    }

    public static String getReverseApi() {
        return REVERSE_API;
    }

    public static void setReverseApi(String reverseApi) {
        REVERSE_API = reverseApi;
    }

    public static String getDownloadBillApi() {
        return DOWNLOAD_BILL_API;
    }

    public static void setDownloadBillApi(String downloadBillApi) {
        DOWNLOAD_BILL_API = downloadBillApi;
    }

    public static String getReportApi() {
        return REPORT_API;
    }

    public static void setReportApi(String reportApi) {
        REPORT_API = reportApi;
    }
}
