package com.ruitaowang.account.service;

import com.ruitaowang.account.dao.SysPwdMapper;
import com.ruitaowang.account.dao.SysUserAdMapper;
import com.ruitaowang.account.dao.SysUserMapper;
import com.ruitaowang.core.domain.SysPwd;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by neal on 11/07/16.
 */
@Service
public class SysPwdService implements BaseService<SysPwd> {

    @Autowired
    private SysPwdMapper sysPwdMapper;

    @Override
    public SysPwd insert(SysPwd sysPwd) {
        Long time = System.currentTimeMillis();
        sysPwd.setCtime(time);
        sysPwd.setMtime(time);
        sysPwdMapper.insert(sysPwd);
        return sysPwd;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public SysPwd update(SysPwd sysPwd) {
    	Long time = System.currentTimeMillis();
        sysPwd.setCtime(time);
        sysPwd.setMtime(time);
        sysPwdMapper.updateByPK(sysPwd);
        return sysPwd;
    }
    @Override
    public SysPwd selectByPK(Long id) {
        return sysPwdMapper.selectByPK(id);
    }

    @Override
    public List<SysPwd> select(SysPwd sysPwd) {
        return sysPwdMapper.select(sysPwd);
    }

}
