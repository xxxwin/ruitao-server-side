package com.ruitaowang.core.domain;

public class Ajob extends Page {
    /**
     *  自增编码,所属表字段为ajob_controller.ajob_id
     */
    private Long ajobId;

    /**
     *  唯一编号,所属表字段为ajob_controller.ajob_sign
     */
    private String ajobSign;

    /**
     *  ip地址,所属表字段为ajob_controller.ajob_ip
     */
    private String ajobIp;

    /**
     *  是否使用：
0 使用
1 不使用,所属表字段为ajob_controller.ajob_type
     */
    private Byte ajobType;

    /**
     *  备注,所属表字段为ajob_controller.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为ajob_controller.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为ajob_controller.mtime
     */
    private Long mtime;

    /**
     *  0 正常
1 删除,所属表字段为ajob_controller.rstatus
     */
    private Byte rstatus;

    public Long getAjobId() {
        return ajobId;
    }

    public void setAjobId(Long ajobId) {
        this.ajobId = ajobId;
    }

    public Byte getAjobType() {
        return ajobType;
    }

    public void setAjobType(Byte ajobType) {
        this.ajobType = ajobType;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAjobSign() {
        return ajobSign;
    }

    public void setAjobSign(String ajobSign) {
        this.ajobSign = ajobSign;
    }

    public String getAjobIp() {
        return ajobIp;
    }

    public void setAjobIp(String ajobIp) {
        this.ajobIp = ajobIp;
    }
}