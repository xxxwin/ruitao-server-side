package com.ruitaowang.core.domain;

public class ActivityUserLink extends Page {
    /**
     *  主键,所属表字段为activity_user_link.user_link_id
     */
    private Long userLinkId;

    /**
     *  用户id,所属表字段为activity_user_link.user_id
     */
    private Long userId;

    /**
     *  文章id,所属表字段为activity_user_link.activity_id
     */
    private Long activityId;

    /**
     *  作者,所属表字段为activity_user_link.author
     */
    private Long author;

    /**
     *  备注,所属表字段为activity_user_link.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为activity_user_link.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为activity_user_link.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为activity_user_link.rstatus
     */
    private Byte rstatus;

    public Long getUserLinkId() {
        return userLinkId;
    }

    public void setUserLinkId(Long userLinkId) {
        this.userLinkId = userLinkId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}