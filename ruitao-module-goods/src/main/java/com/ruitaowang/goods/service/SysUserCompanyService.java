package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.SysUserCompany;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.SysUserCompanyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class SysUserCompanyService implements BaseService<SysUserCompany> {

    @Autowired
    private SysUserCompanyMapper sysUserCompanyMapper;

    @Override
    public SysUserCompany insert(SysUserCompany sysUserCompany) {
        Long time = System.currentTimeMillis();
        sysUserCompany.setCtime(time);
        sysUserCompany.setMtime(time);
        sysUserCompanyMapper.insert(sysUserCompany);
        return sysUserCompany;
    }

    @Override
    public int delete(Long id) {
        SysUserCompany sysUserCompany = new SysUserCompany();
        sysUserCompany.setCompanyId(id);
        sysUserCompany.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        sysUserCompany.setMtime(time);
        return sysUserCompanyMapper.updateByPK(sysUserCompany);
    }

    @Override
    public SysUserCompany update(SysUserCompany sysUserCompany) {
        Long time = System.currentTimeMillis();
        sysUserCompany.setMtime(time);
        sysUserCompanyMapper.updateByPK(sysUserCompany);
        return sysUserCompany;
    }

    @Override
    public SysUserCompany selectByPK(Long id) {
        return sysUserCompanyMapper.selectByPK(id);
    }

    @Override
    public List<SysUserCompany> select(SysUserCompany sysUserCompany) {
        sysUserCompany.setRstatus((byte) 0);
        return sysUserCompanyMapper.select(sysUserCompany);
    }

    public List<SysUserCompany> selectForPage(SysUserCompany sysUserCompany) {
        sysUserCompany.setRstatus((byte) 0);
        sysUserCompany.setRecords(sysUserCompanyMapper.count(sysUserCompany));
        return sysUserCompanyMapper.select(sysUserCompany);
    }

    public SysUserCompany selectByUserId(Long userId) {
        SysUserCompany sysUserCompany =new SysUserCompany();
        sysUserCompany.setUserId(userId);
        sysUserCompany.setRstatus((byte)0);
        List<SysUserCompany> companyList = sysUserCompanyMapper.select(sysUserCompany);
        if(ObjectUtils.isEmpty(companyList)){
            sysUserCompanyMapper.insert(sysUserCompany);
            List<SysUserCompany> sysUserCompanyList = sysUserCompanyMapper.select(sysUserCompany);
            return sysUserCompanyList.get(0);
        }
        return companyList.get(0);
    }
}
