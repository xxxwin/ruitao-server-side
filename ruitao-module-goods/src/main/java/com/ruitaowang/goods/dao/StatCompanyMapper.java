package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.StatCompany;
import java.util.List;

public interface StatCompanyMapper {
    int insert(StatCompany statCompany);

    int deleteByPK(Long id);

    int updateByPK(StatCompany statCompany);

    StatCompany selectByPK(Long id);

    long count(StatCompany statCompany);

    List<StatCompany> select(StatCompany statCompany);
}