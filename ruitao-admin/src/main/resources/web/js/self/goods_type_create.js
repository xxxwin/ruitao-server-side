/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/admin/goodsType/post',
    updateUrl: '/api/admin/goodsType/update',
    deleteUrl: '/api/admin/goodsType/delete',
    selectUrl: '/api/admin/goodsType/get'
};
$().ready(function () {
    $('#submitCreate').click( function() {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()){
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.createUrl,
                    type: "POST",
                    data : postData,
                    dataType: "json",
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});
