package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsRecommend;

import java.util.List;

public interface GoodsRecommendAdMapper {

    //int deleteForOld(Long[] goodsRecommendIds);

    int deleteByCompany(Long companyId);

    int insertNew(List<GoodsRecommend> goodsRecommends);
}
