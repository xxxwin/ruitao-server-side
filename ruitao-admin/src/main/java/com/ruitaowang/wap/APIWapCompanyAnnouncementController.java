package com.ruitaowang.wap;

import com.ruitaowang.core.domain.CompanyAnnouncement;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyAnnouncementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

@RestController
public class APIWapCompanyAnnouncementController extends BaseController{

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private CompanyAnnouncementService companyAnnouncementService;

    /**
     * 社群公告分页查询
     * @param companyAnnouncement
     * @param page
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/notice", method = RequestMethod.GET)
    public String listForWap(CompanyAnnouncement companyAnnouncement,int page,@RequestHeader(value="headId",required = false) String userId) {
        companyAnnouncement.setPage(page);
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(companyAnnouncementService.selectForPage(companyAnnouncement));
        o.setRecords(companyAnnouncement.getRecords());
        o.setTotal(companyAnnouncement.getTotal());
        return this.renderJson(o);
    }

    /**
     * 新增社群公告
     * @param companyAnnouncement
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/notice", method = RequestMethod.POST)
    public String insert(CompanyAnnouncement companyAnnouncement,@RequestHeader(value="headId",required = false) String userId){
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        companyAnnouncementService.insert(companyAnnouncement);
        MsgOut o = MsgOut.success(companyAnnouncement);
        return this.renderJson(o);
    }

    /**
     * 修改社群公告
     * @param companyAnnouncement
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/notice", method = RequestMethod.PUT)
    public String update(CompanyAnnouncement companyAnnouncement,@RequestHeader(value="headId",required = false) String userId){
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        companyAnnouncementService.update(companyAnnouncement);
        MsgOut o = MsgOut.success(companyAnnouncement);
        return this.renderJson(o);
    }

    /**
     * 删除社群公告
     * @param id
     * @param userId
     * @return
     */
    @RequestMapping(value = "/api/wap/notice/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id, @RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(companyAnnouncementService.delete(id));
        return this.renderJson(o);
    }
}
