package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Activity;
import com.ruitaowang.core.domain.ActivityVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.annotation.QueryAnnotation;

import java.util.List;

public interface ActivityAdMapper {

    @Select("select activity_content as activityContent, activity_id as activityId, activity_title as activityTitle, activity_type as activityType, ctime, rstatus from activity where rstatus = 0 and activity_id = #{activityId}")
    Activity selectByPK(Long activityId);

    @Select("select max(activity_id) from activity where rstatus = 0 and activity_type = #{activityType} and activity_id < #{activityId}")
    Long selectPreId(@Param("activityId") Long activityId, @Param("activityType") Byte activityType);

    @Select("select min(activity_id) from activity where rstatus = 0 and activity_type = #{activityType} and activity_id > #{activityId}")
    Long selectNextId(@Param("activityId") Long activityId, @Param("activityType") Byte activityType);

    @Select("SELECT count(*) FROM (SELECT activity_author FROM rtshop.activity where ctime > #{stime} and ctime < #{etime} and activity_type=10 group by activity_author) ac;")
    long countUsersWCM(@Param("stime") Long stime,@Param("etime") Long etime);

    @Select("SELECT count(*) FROM rtshop.activity where ctime > 1547886600000 and activity_author = #{activityAuthor} and activity_type = #{activityType};")
    long selectNoRstatus(Activity activity);

    @Select("SELECT activity_id as activityId, activity_title as activityTitle, ctime, activity_read as activityRead, activity_image_url as activityImageUrl FROM rtshop.activity where activity_id in(SELECT activity_id FROM rtshop.activity_user_link where ctime > #{stime} and ctime < #{etime} and user_id = #{userId} and rstatus = 0 group by activity_id);")
    List<Activity> selectDetail(ActivityVo activityVo);

}