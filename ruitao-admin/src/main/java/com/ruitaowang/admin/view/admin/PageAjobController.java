package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Ajob;
import com.ruitaowang.goods.service.AjobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;

/**
 * 定时器信息
 */
@RestController
public class PageAjobController {

    @Autowired
    private AjobService ajobService;

    @RequestMapping(value = {"/ajob/update/{activityId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("activityId") Long ajobId, Model model) {
        Ajob ajob = ajobService.selectByPK(ajobId);
        model.addAttribute("ajob", ajob);
        return new ModelAndView("web/ajob_update");
    }
}
