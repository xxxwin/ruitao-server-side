/**
 * Created by Shaka on 12/21/16.
 */
var Config = {
    selectUrl: '/api/admin/goodsTypeCustom/page',
    deleteUrl: '/api/admin/goodsTypeCustomDelete'
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#custom_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","分类名称","备注", "创建时间","最后修改时间","操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: true
        }, {
            name: "name",
            index: "name"
        },{
            name:"remark",
            index: "remark",
        },{
            name:"ctime",
            index: "ctime"
        }, {
            name:"mtime",
            index: "mtime"
        },{
            name: "operate",
            index: "operate",
            sortable: false
        }],
        pager: "#pager_custom_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
           dataWrapper();
        }
    });
    jQuery("#custom_list").jqGrid('navGrid', '#pager_custom_list', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#custom_list").setGridWidth(width);
    });
});

function Delete(id) {
    bootbox.confirm({size:"small",message:"确认要删除id为"+id+"的这条数据吗?",callback: function (result) {
            if (result) {
                updateDialog("DELETE", Config.deleteUrl + '/' + id);
                jQuery("#custom_list").jqGrid('delRowData', id);
            }
        }
    });
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#custom_list").trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}

function dataWrapper() {
    var ids = jQuery("#custom_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#custom_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        dataFromTheRow.mtime = new Date(dataFromTheRow.mtime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" onclick="industryUpdate(' + id + ')" data-index="0">修改</a>';
        jQuery("#custom_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
            mtime:dataFromTheRow.mtime,
        });
    }
}

function industryUpdate(id) {
    layer.open({
        title: ["分类修改","font-size:20px;"],
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:"/goodsTypeCustom/update/" + id,
    })
}

