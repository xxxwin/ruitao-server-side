package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.MeetingUserLink;
import com.ruitaowang.core.domain.Meetings;
import com.ruitaowang.core.properties.ConfigProperties;
import com.ruitaowang.core.utils.QRCode;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wx.wechat.common.Configure;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APIMeetingController extends BaseController {

    @Autowired
    private MeetingService meetingService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    ConfigProperties configProperties;

//    private void init() {
//        //init
//        Configure.hostAPI = configProperties.getHostAPI();
//        Configure.hostOSS = configProperties.getHostOSS();
//        Configure.hostWap = configProperties.getHostWap();
//        Configure.returnUri = configProperties.getReturnUri();
//        Configure.wxpay_notify_url = configProperties.getHostAPI() + "/api/wxs/notify";;
//    }

    @RequestMapping("/api/meetings")
    public String list(Meetings meetings, int page) {
        meetings.setPage(page);
        Company company = companyService.selectByUserId(LYSecurityUtil.currentSysUser().getId());
        if(ObjectUtils.isEmpty(company)){
            return this.renderJson(MsgOut.error());
        }
        meetings.setCompanyId(company.getCompanyId());
        MsgOut o = MsgOut.success(meetingService.selectForPage(meetings));
        o.setRecords(meetings.getRecords());
        o.setTotal(meetings.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetings", method = RequestMethod.POST)
    public String create(Meetings meetings) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
//        init();
        Company company = companyService.selectByUserId(LYSecurityUtil.currentSysUser().getId());
        if(ObjectUtils.isEmpty(company)){
            return this.renderJson(MsgOut.error("无商家信息"));
        }
        meetings.setCompanyId(company.getCompanyId());
        if(ObjectUtils.isEmpty(meetingService.insert(meetings))){
            return this.renderJson(MsgOut.error("注册不成功"));
        }
        meetings.setQrSignUp(QRCode.rulesQRForMeetingSignUp(System.getProperty("catalina.base"), meetings.getMeetingId(), LYSecurityUtil.currentSysUser().getId()));
        meetings.setQrSignIn(QRCode.rulesQRForMeetingSignIn(System.getProperty("catalina.base"), meetings.getMeetingId(), LYSecurityUtil.currentSysUser().getId()));
        meetingService.update(meetings);
        MsgOut o = MsgOut.success(meetings);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetings", method = RequestMethod.PUT)
    public String update(Meetings meetings) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        meetingService.update(meetings);
        MsgOut o = MsgOut.success(meetings);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetings/{meetingsId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("meetingsId") Long meetingsId) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        MsgOut o = MsgOut.success(meetingService.delete(meetingsId));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetings/{meetingsId}", method = RequestMethod.GET)
    public String getOne(@PathVariable("meetingsId") Long meetingsId) {

        MsgOut o = MsgOut.success(meetingService.selectByPK(meetingsId));
        return this.renderJson(o);
    }

    /**
     * 我的会议
     */

    @RequestMapping("/api/meetings/my")
    public String listForMyMeetings(Meetings meetings, int page) {
        meetings.setPage(page);
        MsgOut o = MsgOut.success(meetingService.selectMyMeetingsForPage(meetings, LYSecurityUtil.currentSysUser().getPhone()));
        o.setRecords(meetings.getRecords());
        o.setTotal(meetings.getTotal());
        return this.renderJson(o);
    }

    /**
     * 全部会议
     */

    @RequestMapping("/api/meetings/all")
    public String listForAllMeetings(Meetings meetings, int page) {
        meetings.setPage(page);
        MsgOut o = MsgOut.success(meetingService.selectForPage(meetings));
        o.setRecords(meetings.getRecords());
        o.setTotal(meetings.getTotal());
        return this.renderJson(o);
    }

}
