package com.ruitaowang.core.domain;

public class UserMsg extends Page {
    /**
     *  ,所属表字段为user_msg.id
     */
    private Long id;

    /**
     *  发送者id,所属表字段为user_msg.from_user_id
     */
    private Long fromUserId;

    /**
     *  发送者昵称,所属表字段为user_msg.from_user_name
     */
    private String fromUserName;

    /**
     *  发送者头像,所属表字段为user_msg.from_user_photo
     */
    private String fromUserPhoto;

    /**
     *  接收者id,所属表字段为user_msg.to_user_id
     */
    private Long toUserId;

    /**
     *  接收者昵称,所属表字段为user_msg.to_user_name
     */
    private String toUserName;

    /**
     *  接收者头像,所属表字段为user_msg.to_user_photo
     */
    private String toUserPhoto;

    /**
     *  互动内容,所属表字段为user_msg.msg_content
     */
    private String msgContent;

    /**
     *  创建时间,所属表字段为user_msg.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为user_msg.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为user_msg.remark
     */
    private String remark;

    /**
     *  0 正常,1 删除,所属表字段为user_msg.rstatus
     */
    private Byte rstatus;

    /**
     *  商家id,所属表字段为user_msg.company_id
     */
    private Long companyId;

    /**
     *  商家name,所属表字段为user_msg.company_name
     */
    private String companyName;

    /**
     *  商家photo,所属表字段为user_msg.company_photo
     */
    private String companyPhoto;

    /**
     *
     */
    private Byte magType;

    public Byte getMagType() {
        return magType;
    }

    public void setMagType(Byte magType) {
        this.magType = magType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyPhoto() {
        return companyPhoto;
    }

    public void setCompanyPhoto(String companyPhoto) {
        this.companyPhoto = companyPhoto;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getFromUserName() {
        return fromUserName;
    }

    public void setFromUserName(String fromUserName) {
        this.fromUserName = fromUserName == null ? null : fromUserName.trim();
    }

    public String getFromUserPhoto() {
        return fromUserPhoto;
    }

    public void setFromUserPhoto(String fromUserPhoto) {
        this.fromUserPhoto = fromUserPhoto == null ? null : fromUserPhoto.trim();
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getToUserName() {
        return toUserName;
    }

    public void setToUserName(String toUserName) {
        this.toUserName = toUserName == null ? null : toUserName.trim();
    }

    public String getToUserPhoto() {
        return toUserPhoto;
    }

    public void setToUserPhoto(String toUserPhoto) {
        this.toUserPhoto = toUserPhoto == null ? null : toUserPhoto.trim();
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent == null ? null : msgContent.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}