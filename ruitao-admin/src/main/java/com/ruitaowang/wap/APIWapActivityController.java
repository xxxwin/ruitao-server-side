package com.ruitaowang.wap;

import com.ruitaowang.core.domain.Activity;
import com.ruitaowang.core.domain.ActivityVo;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.ActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class APIWapActivityController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private ActivityService activityService;

    /**
     * 文章详情
     * @param activityVo
     * @return
     */
    @RequestMapping("/api/wap/activityForDetail")
    public String rankingList(ActivityVo activityVo) {
//        SysUser user = LYSecurityUtil.currentSysUser();
//        if (ObjectUtils.isEmpty(user)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
        //七天的毫秒数
        Long oneDay = (24L * 60L * 60L *1000L);
        //获取七天前的开始时间
        Long dayBegin = TimeUtils.getLongDayBegin();
        //开始时间结束时间
        Long stime ,etime;
        List list = new ArrayList();
        for (int i=1;i < 8;i++){
            HashMap map = new HashMap();
            stime = dayBegin;
            etime = dayBegin + oneDay;
            activityVo.setStime(stime);
            activityVo.setEtime(etime);
            //查询数据
            List<Activity> activityList = activityService.selectDetail(activityVo);
            if (!ObjectUtils.isEmpty(activityList)){
                map.put("time",dayBegin.toString());
                map.put("data",activityList);
                list.add(map);
            }
            dayBegin = dayBegin - oneDay;
        }
        MsgOut o = MsgOut.success(list);
        return this.renderJson(o);
    }

}
