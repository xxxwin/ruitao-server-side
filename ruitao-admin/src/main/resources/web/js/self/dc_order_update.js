/**
 * Created by Shaka on 12/21/16.
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
var Config = {
    selectUrl: '/api/admin/dc/orderProds',
    deleteUrl: '/api/company',
    updateUrlForCellEdit: '/api/admin/company/cellEdit',
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        postData:{orderId : parseInt(getQueryString("id"))},
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号","菜品", "菜品名称", "菜品图片", "菜品价格(元)","菜品数量", "菜品分类","商家", "备注", "时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: true
        },{
            name: "dishId",
            index: "dishId",
            search: true
        },{
            name: "name",
            index: "name",
        }, {
            name: "thum",
            index: "thum",
        },{
            name: "price",
            index: "price",
            sorttype: "int"
        }, {
            name: "num",
            index: "num",
            sorttype: "int"
        },{
            name: "categoryId",
            index: "categoryId",
            autowidth: true,
            formatter:mydateformatter,
            sorttype: "int"
        }, {
            name: "companyId",
            index: "companyId",
            search: true
        },{
            name: "remark",
            index: "remark",
        }, {
            name: "ctime",
            index: "ctime",
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false,search: false,
        refresh: false});
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#data_list").setGridParam({url: url}).trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var avatar = '<img style="width: 50px; height: 50px;" src="'+dataFromTheRow.thum+'"/>';
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {thum:avatar,operate: html,price:toFixedForPrice(dataFromTheRow.price),ctime: dataFromTheRow.ctime,price:toFixedForPrice(dataFromTheRow.price)});
    }
}
function mydateformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax({
        url: "/api/admin/dc/categories/"+rowObject.categoryId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
          name = data.data.name;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return name;
}

