package com.ruitaowang.goods.service;

import java.util.Arrays;
import java.util.List;

import com.ruitaowang.core.domain.CategoryRecommend;
import com.ruitaowang.core.domain.GoodsCategory;
import com.ruitaowang.goods.dao.CategoryRecommendMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruitaowang.core.domain.Category;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CategoryMapper;

@Service
public class CategoryService implements BaseService<Category> {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private CategoryRecommendMapper categoryRecommendMapper;

    public Category insert(Category category) {
        categoryMapper.insert(category);
        return category;
    }

    public int delete(Long id) {
        return categoryMapper.deleteByPK(id);
    }

    public Category update(Category category) {
        categoryMapper.updateByPK(category);
        return category;
    }

    public Category selectByPK(Long id) {
        return categoryMapper.selectByPK(id);
    }

    public List<Category> select(Category category) {
        //category.setRecords(categoryMapper.count(category));
        category.setResult(categoryMapper.select(category));
        return category.getResult();
    }
    
    public List<Category> selectByCompany(Long companyId,Integer onLine) {
        return categoryMapper.selectCatesByCompany(companyId,onLine);
    }
    public String selectCategoryName(Long id) {
        return categoryMapper.selectCategoryName(id);
    }

    public Category update(Category category, Byte[] categoryRecommend) {
        this.update(category);
        if(categoryRecommend==null || categoryRecommend.length == 0){
            return category;
        }
        updateRecommend(category,categoryRecommend);
        return category;
    }
    private void updateRecommend(Category category, Byte[] categoryRecommend) {
        CategoryRecommend categoryRecommend1 = null;
        CategoryRecommend whereClause1 = new CategoryRecommend();
        whereClause1.setCategoryId(category.getId());
        whereClause1.setRstatus((byte) 1);
        categoryRecommendMapper.updateByPK(whereClause1);
        for (int i=0; i < categoryRecommend.length; i++){
            categoryRecommend1 = new CategoryRecommend();
            categoryRecommend1.setCategoryId(category.getId());
            categoryRecommend1.setRecommendType(categoryRecommend[i]);
            Long time = System.currentTimeMillis();

            categoryRecommend1.setMtime(time);
            //同时写入Recommend
            CategoryRecommend whereClause = new CategoryRecommend();
            whereClause.setCategoryId(category.getId());
            whereClause.setRecommendType(categoryRecommend1.getRecommendType());
            if(categoryRecommendMapper.select(whereClause).size()>0){
                categoryRecommend1.setRstatus((byte) 0);
                categoryRecommendMapper.updateByPK(categoryRecommend1);
            }else{
                categoryRecommend1.setCtime(time);
                categoryRecommendMapper.insert(categoryRecommend1);
            }
        }
    }
    /* recommend */
    public List<CategoryRecommend> selectRecommend(Long categoryId) {
        CategoryRecommend categoryRecommend = new CategoryRecommend();
        categoryRecommend.setRstatus((byte) 0);
        categoryRecommend.setCategoryId(categoryId);
        return categoryRecommendMapper.select(categoryRecommend);
    }
}



