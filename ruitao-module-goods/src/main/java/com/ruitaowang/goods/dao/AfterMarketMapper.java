package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.AfterMarket;
import java.util.List;

public interface AfterMarketMapper {
    int insert(AfterMarket afterMarket);

    int deleteByPK(Long id);

    int updateByPK(AfterMarket afterMarket);

    AfterMarket selectByPK(Long id);

    long count(AfterMarket afterMarket);

    List<AfterMarket> select(AfterMarket afterMarket);
}