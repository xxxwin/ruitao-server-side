//package com.ruitaowang.admin.config;
//
//import com.ruitaowang.account.service.SysUserService;
//import com.ruitaowang.admin.websocket.WebSocketConnectHandler;
//import com.ruitaowang.admin.websocket.WebSocketDisconnectHandler;
//import com.ruitaowang.goods.service.UserOnlineService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.messaging.simp.SimpMessagingTemplate;
//import org.springframework.session.ExpiringSession;
//
///**
// * These handlers are separated from WebSocketConfig because they are specific to this
// * application and do not demonstrate a typical Spring Session setup.
// *
// */
//@Configuration
//public class WebSocketHandlersConfig<S extends ExpiringSession> {
//
//	@Autowired
//	private SimpMessagingTemplate messagingTemplate;
//	@Autowired
//	private UserOnlineService userOnlineService;
//	@Autowired
//	private SysUserService userService;
//
//	@Bean
//	public WebSocketConnectHandler<S> webSocketConnectHandler() {
//		return new WebSocketConnectHandler<S>(messagingTemplate, userOnlineService, userService);
//	}
//
//	@Bean
//	public WebSocketDisconnectHandler<S> webSocketDisconnectHandler() {
//		return new WebSocketDisconnectHandler<S>(messagingTemplate, userOnlineService);
//	}
//}
