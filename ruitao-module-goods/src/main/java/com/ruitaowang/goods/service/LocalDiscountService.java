package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.LocalDiscount;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.LocalDiscountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 1/3/17.
 */
@Service
public class LocalDiscountService implements BaseService<LocalDiscount> {
    @Autowired
    private LocalDiscountMapper localDiscountMapper;

    @Override
    public LocalDiscount insert(LocalDiscount localDiscount) {
        Long time = System.currentTimeMillis();
        localDiscount.setCtime(time);
        localDiscount.setMtime(time);
        localDiscountMapper.insert(localDiscount);
        return localDiscount;
    }

    @Override
    public int delete(Long id) {
        LocalDiscount localDiscount = new LocalDiscount();
        localDiscount.setDiscountId(id);
        localDiscount.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        localDiscount.setMtime(time);
        return localDiscountMapper.updateByPK(localDiscount);
    }

    @Override
    public LocalDiscount update(LocalDiscount localDiscount) {
        Long time = System.currentTimeMillis();
        localDiscount.setMtime(time);
        localDiscountMapper.updateByPK(localDiscount);
        return localDiscount;
    }

    @Override
    public LocalDiscount selectByPK(Long id) {
        return localDiscountMapper.selectByPK(id);
    }

    @Override
    public List<LocalDiscount> select(LocalDiscount localDiscount) {
        localDiscount.setRstatus((byte) 0);
        localDiscount.setOrderBy("score desc");
        return localDiscountMapper.select(localDiscount);
    }
}
