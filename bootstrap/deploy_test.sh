#!/usr/bin/env bash
ssh -t -t root@test.ruitaowang.com << EOF
cd /server/source/ruitao-server-side
echo `pwd`
/usr/bin/git checkout master
/usr/bin/git pull origin master
echo 'copy vue-gxt'
cd /server/source/ruitao-server-side/vue-gxt
npm run build
/usr/bin/cp -r /server/source/ruitao-server-side/vue-gxt/dist /server/source/ruitao-server-side/ruitao-admin/src/main/webapp/wap/vue-gxt
cd /server/source/ruitao-server-side
/opt/maven/bin/mvn clean install -DskipTests
ps aux |grep tomcat |grep -v 'grep' |awk '{print $2}'|xargs kill -9
#rm -rf /opt/tomcat/webapps/*
rm -rf /opt/admin/webapps/*

#/usr/bin/cp /server/source/ruitao-server-side/ruitao-rest-mobile/target/ruitao-rest-mobile-1.0.0.war /opt/tomcat/webapps/ROOT.war
/usr/bin/cp /server/source/ruitao-server-side/ruitao-admin/target/ruitao-admin-1.0.0.war /opt/admin/webapps/ROOT.war
#sudo sh /opt/tomcat/bin/startup.sh
sudo sh /opt/admin/bin/startup.sh
echo 'Please waiting 20s ...'
apidoc -i /server/source/ruitao-server-side/ruitao-admin/src/main/java/com/ruitaowang/admin/view/api -o /root/shihong/ruitao/ -c /server/source/ruitao-server-side/bootstrap/
apidoc -i /server/source/ruitao-server-side/ruitao-admin/src/main/java/com/ruitaowang/dinner/view/api -o /root/shihong/ruitao/dc/ -c /server/source/ruitao-server-side/bootstrap/
#sleep 20
tail -f /opt/admin/logs/catalina.out
#/opt/tomcat/logs/catalina.out
exit
EOF