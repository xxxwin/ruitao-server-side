package wx.wechat.api;

import wx.wechat.common.Configure;
import wx.wechat.common.RandomStringGenerator;
import wx.wechat.common.signature.SHA1;
import wx.wechat.common.signature.Signature;
import wx.wechat.service.mp.MPService;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MPAPI extends API {

    /**
     * @param signature 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
     * @param timestamp 时间戳
     * @param nonce     随机数
     * @param echostr   随机字符串
     * @return
     * @function 验证消息的确来自微信服务器
     */
    public String portal(
            String signature,
            String timestamp,
            String nonce,
            String echostr) {

        String[] str = {Configure.appToken, timestamp, nonce};

        Arrays.sort(str); // 字典序排序

        String bigStr = str[0] + str[1] + str[2];

        // SHA1加密
        String digest = SHA1.encode(bigStr).toLowerCase();

        // 确认请求来至微信
        if (digest.equals(signature)) {
            return echostr;
        } else {
            return "Invalid Signature";
        }
    }

}
