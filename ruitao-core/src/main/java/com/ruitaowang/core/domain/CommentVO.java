package com.ruitaowang.core.domain;

public class CommentVO extends Comment {
    private String nickName;

    private String headImgUrl;

    private Long commentTotal;

    private String thumpsUpType;

    private String goodsName;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Long getCommentTotal() {
        return commentTotal;
    }

    public void setCommentTotal(Long commentTotal) {
        this.commentTotal = commentTotal;
    }

    public String getThumpsUpType() {
        return thumpsUpType;
    }

    public void setThumpsUpType(String thumpsUpType) {
        this.thumpsUpType = thumpsUpType;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadImgUrl() {
        return headImgUrl;
    }

    public void setHeadImgUrl(String headImgUrl) {
        this.headImgUrl = headImgUrl;
    }
}
