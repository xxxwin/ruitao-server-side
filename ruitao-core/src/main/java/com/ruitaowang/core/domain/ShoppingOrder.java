package com.ruitaowang.core.domain;

public class ShoppingOrder extends Page {
    /**
     *  ,所属表字段为shopping_order.id
     */
    private Long id;

    /**
     *  订单ID,所属表字段为shopping_order.order_id
     */
    private Long orderId;

    /**
     *  商户ID,所属表字段为shopping_order.company_id
     */
    private Long companyId;

    /**
     *  商户名称,所属表字段为shopping_order.company_name
     */
    private String companyName;

    /**
     *  订单总价,所属表字段为shopping_order.amount
     */
    private Integer amount;

    /**
     *  满减券优惠价格,所属表字段为shopping_order.favorable_price
     */
    private Integer favorablePrice;

    /**
     *  满减券优惠ID,所属表字段为shopping_order.discount_id
     */
    private Long discountId;

    /**
     *  ,所属表字段为shopping_order.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为shopping_order.mtime
     */
    private Long mtime;

    /**
     *  ,所属表字段为shopping_order.remark
     */
    private String remark;

    /**
     *  ,所属表字段为shopping_order.rstatus
     */
    private Byte rstatus;

    /**
     *  商户logo,所属表字段为shopping_order.company_headImg
     */
    private String companyHeadimg;

    /**
     *  订单状态,所属表字段为order_status
     */
    private Byte orderStatus;

    /**
     * 支付状态,所属表字段为pay_status
     */
    private Byte payStatus;

    /**
     * 买家留言,所属表字段为message
     */
    private String message;

    /**
     * 发货方式,所属表字段为express_way
     */
    private Byte expressWay;

    /**
     * 物流名称,所属表字段为express_name
     */
    private String expressName;

    /**
     * 物流单号,所属表字段为express_sn
     */
    private String expressSn;

    public Byte getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Byte orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Byte getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Byte payStatus) {
        this.payStatus = payStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getFavorablePrice() {
        return favorablePrice;
    }

    public void setFavorablePrice(Integer favorablePrice) {
        this.favorablePrice = favorablePrice;
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getCompanyHeadimg() {
        return companyHeadimg;
    }

    public void setCompanyHeadimg(String companyHeadimg) {
        this.companyHeadimg = companyHeadimg == null ? null : companyHeadimg.trim();
    }

    public Byte getExpressWay() {
        return expressWay;
    }

    public void setExpressWay(Byte expressWay) {
        this.expressWay = expressWay;
    }

    public String getExpressName() {
        return expressName;
    }

    public void setExpressName(String expressName) {
        this.expressName = expressName;
    }

    public String getExpressSn() {
        return expressSn;
    }

    public void setExpressSn(String expressSn) {
        this.expressSn = expressSn;
    }
}