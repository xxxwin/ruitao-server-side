package com.ruitaowang.core.domain;

public class District extends Page {
    /**
     *  ,所属表字段为district.id
     */
    private Long id;

    /**
     *  ,所属表字段为district.name
     */
    private String name;

    /**
     *  ,所属表字段为district.parentid
     */
    private Long parentid;

    /**
     *  ,所属表字段为district.initial
     */
    private String initial;

    /**
     *  ,所属表字段为district.initials
     */
    private String initials;

    /**
     *  ,所属表字段为district.pinyin
     */
    private String pinyin;

    /**
     *  ,所属表字段为district.suffix
     */
    private String suffix;

    /**
     *  ,所属表字段为district.code
     */
    private String code;

    /**
     *  ,所属表字段为district.district_order
     */
    private Byte districtOrder;

    /**
     *  ,所属表字段为district.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为district.mtime
     */
    private Long mtime;

    /**
     *  加盟费,最低1000,所属表字段为district.price
     */
    private Integer price;

    /**
     *  0 没交费 1 以缴费,所属表字段为district.used
     */
    private Byte used;

    /**
     *  区代理,所属表字段为district.biz_user_id
     */
    private Long bizUserId;

    /**
     *  ,所属表字段为district.city_user_id
     */
    private Long cityUserId;

    /**
     * 三级联动等级,所属表字段为level_type
     */
    private Byte levelType;

    public Byte getLevelType() {
        return levelType;
    }

    public void setLevelType(Byte levelType) {
        this.levelType = levelType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial == null ? null : initial.trim();
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials == null ? null : initials.trim();
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin == null ? null : pinyin.trim();
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix == null ? null : suffix.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public Byte getDistrictOrder() {
        return districtOrder;
    }

    public void setDistrictOrder(Byte districtOrder) {
        this.districtOrder = districtOrder;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Byte getUsed() {
        return used;
    }

    public void setUsed(Byte used) {
        this.used = used;
    }

    public Long getBizUserId() {
        return bizUserId;
    }

    public void setBizUserId(Long bizUserId) {
        this.bizUserId = bizUserId;
    }

    public Long getCityUserId() {
        return cityUserId;
    }

    public void setCityUserId(Long cityUserId) {
        this.cityUserId = cityUserId;
    }
}