package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Ajob;
import java.util.List;

public interface AjobMapper {
    int insert(Ajob ajob);

    int deleteByPK(Long ajobId);

    int updateByPK(Ajob ajob);

    Ajob selectByPK(Long ajobId);

    long count(Ajob ajob);

    List<Ajob> select(Ajob ajob);
}