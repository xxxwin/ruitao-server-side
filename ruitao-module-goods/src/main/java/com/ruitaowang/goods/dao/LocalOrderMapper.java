package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LocalOrder;
import java.util.List;

public interface LocalOrderMapper {
    int insert(LocalOrder localOrder);

    int deleteByPK(Long orderId);

    int updateByPK(LocalOrder localOrder);

    LocalOrder selectByPK(Long orderId);

    long count(LocalOrder localOrder);

    List<LocalOrder> select(LocalOrder localOrder);
}