package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.CompanyAnnouncement;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CompanyAnnouncementMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyAnnouncementService implements BaseService<CompanyAnnouncement> {

    @Autowired
    private CompanyAnnouncementMapper companyAnnouncementMapper;

    @Override
    public CompanyAnnouncement insert(CompanyAnnouncement companyAnnouncement) {
        Long time = System.currentTimeMillis();
        companyAnnouncement.setCtime(time);
        companyAnnouncement.setMtime(time);
        companyAnnouncementMapper.insert(companyAnnouncement);
        return companyAnnouncement;
    }

    @Override
    public int delete(Long id) {
        CompanyAnnouncement companyAnnouncement = new CompanyAnnouncement();
        companyAnnouncement.setId(id);
        companyAnnouncement.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        companyAnnouncement.setMtime(time);
        return companyAnnouncementMapper.updateByPK(companyAnnouncement);
    }

    @Override
    public CompanyAnnouncement update(CompanyAnnouncement companyAnnouncement) {
        Long time = System.currentTimeMillis();
        companyAnnouncement.setMtime(time);
        companyAnnouncementMapper.updateByPK(companyAnnouncement);
        return companyAnnouncement;
    }

    @Override
    public CompanyAnnouncement selectByPK(Long id) {
        return companyAnnouncementMapper.selectByPK(id);
    }

    @Override
    public List<CompanyAnnouncement> select(CompanyAnnouncement companyAnnouncement) {
        companyAnnouncement.setRstatus((byte) 0);
        return companyAnnouncementMapper.select(companyAnnouncement);
    }

    public List<CompanyAnnouncement> selectForPage(CompanyAnnouncement companyAnnouncement) {
        companyAnnouncement.setRstatus((byte) 0);
        companyAnnouncement.setRecords(companyAnnouncementMapper.count(companyAnnouncement));
        return companyAnnouncementMapper.select(companyAnnouncement);
    }

}
