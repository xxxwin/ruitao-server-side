/**
 * Created by Administrator on 2017/4/28.
 */
var Config = {
    createUrl: '/api/dinner/orders',
    updateUrl: '/api/dinner/orders',
    deleteUrl: '/api/dinner/orders',
    selectUrl: '/api/admin/dc/orders/get'
};

$("#datepicker").datepicker({
    todayBtn: "linked",
    clearBtn: true,
    autoclose: true,
    language: "zh-CN"
});
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
        $("#data_list").jqGrid({
            datatype: "json",
            url: Config.selectUrl,
            height: 450,
            autowidth: true,
            shrinkToFit: true,
            rowNum: 12,
            colNames: ["下单日期","桌号","合计","客户识别码","消费次数（≧3）","消费累计（≧500）", "满足奖励","是否奖励","奖励方式","是否结","操作"],
            colModel: [{
                name: "ctime",
                index: "ctime"
            }, {
                name: "tableId",
                index: "tableId"
            },{
                name: "amount",
                index: "amount"
            },{
                name: "customer",
                index: "customer"
            },{
                name: "consumptionNum",
                index: "consumptionNum"
            },{
                name: "consumptionTotal",
                index: "consumptionTotal"
            }, {
                name: "satisfyReward",
                index: "satisfyReward",
                editoptions: {value: "0:否;1:是"},
                edittype: 'select',
                formatter: 'select'
            }, {
                name: "reward",
                index: "reward",
                editoptions: {value: "0:否;1:是"},
                edittype: 'select',
                formatter: 'select'
            },{
                name: "rewardMode",
                index: "rewardMode"
            },{
                name: "payStatus",
                index: "payStatus",
                editoptions: {value: "0:未付款;1:已付款"},
                edittype: 'select',
                formatter: 'select'
            },{
                name: "operate",
                index: "operate"
            }],
            pager: "#pager_data_list",
            jsonReader: {
                root: "data", "page": "page", total: "total",
                records: "records", repeatitems: false, id: "id"
            },
            viewrecords: true,
            hidegrid: false,
            gridComplete: function () {
                dataWrapper()
            }
        });
        jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
        $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});
    });
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData',id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url : url,
            type: action,
            success:function(data, textStatus, jqXHR)
            {
                if(action == 'DELETE'){
                    $("#data_list").trigger("reloadGrid");
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                toastr.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id);
        dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" data-index="0">打印</a>';
        // html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/dinner/profit/excel/'+dataFromTheRow.orderNo+'" >订单导出</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html,amount: toFixedForPrice(dataFromTheRow.amount),consumptionTotal: toFixedForPrice(dataFromTheRow.consumptionTotal),ctime: dataFromTheRow.ctime});
    }
}
$("#perform_search").click(function () {
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.stime = dateToEpoch($("#beginDate").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    postdata.etime = dateToEpoch($("#endDate").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    jQuery("#data_list").trigger("reloadGrid", postdata);
});
$("#search").click(function () {
        $.ajax(
        {
            url:"/api/admin/users",
            type: "GET",
            data:{phone:$("#phone").val(),page:1,rows:1000},
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                console.log(data);
                console.log(data.data[0].id);
                var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
                postdata._search = true;
                postdata.userId =data.data[0].id ;
                jQuery("#data_list").trigger("reloadGrid", postdata);
            }
        })
});