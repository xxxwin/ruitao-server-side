var Config = {
    deleteUrl: '/api/admin/cate/delete',
};

$().ready(function () {
    $(document).on('click',"#edit",function(){
        var id = $(this).parent("tr").find("td").eq(0).text();
        layer.open({
            type: 2,
            scrollbar: false,
            shadeClose: true,
            shade: 0.8,
            area: ['100%', '100%'],
            content: "/cate/update2/"+id
        })
    });

    $(document).on('click',"#delete",function(){
        var id = $(this).parent("tr").find("td").eq(0).text();
        layer.confirm("添加成功，是否继续添加", {
            btn : [ '确定', '取消' ]//按钮
        }, function() {
            $.ajax(
                {
                    url: Config.deleteUrl + '/' + id,
                    type: "DELETE",
                    success: function (data, textStatus, jqXHR) {
                        layer.msg("刪除成功")
                        // $(this).parent("tr").parent().parent().remove();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.error(errorThrown);
                    }
                });
        });
    });
});