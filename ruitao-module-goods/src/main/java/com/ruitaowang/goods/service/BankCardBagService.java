package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.BankCardBag;
import com.ruitaowang.core.domain.Comment;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.BankCardBagMapper;
import com.ruitaowang.goods.dao.CommentAdMapper;
import com.ruitaowang.goods.dao.CommentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankCardBagService implements BaseService<BankCardBag> {
    @Autowired
    private BankCardBagMapper bankCardBagMapper;

    @Override
    public BankCardBag insert(BankCardBag bankCardBag) {
        Long time = System.currentTimeMillis();
        bankCardBag.setCtime(time);
        bankCardBag.setMtime(time);
        bankCardBagMapper.insert(bankCardBag);
        return bankCardBag;
    }

    @Override
    public int delete(Long id) {
        BankCardBag bankCardBag = new BankCardBag();
        bankCardBag.setId(id);
        bankCardBag.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        bankCardBag.setMtime(time);
        return bankCardBagMapper.updateByPK(bankCardBag);
    }

    @Override
    public BankCardBag update(BankCardBag bankCardBag) {
        Long time = System.currentTimeMillis();
        bankCardBag.setMtime(time);
        bankCardBagMapper.updateByPK(bankCardBag);
        return bankCardBag;
    }

    @Override
    public BankCardBag selectByPK(Long id) {

        return bankCardBagMapper.selectByPK(id);
    }

    @Override
    public List<BankCardBag> select(BankCardBag bankCardBag) {
        bankCardBag.setRstatus((byte) 0);
        return bankCardBagMapper.select(bankCardBag);
    }

    public List<BankCardBag> selectForPage(BankCardBag bankCardBag) {
        bankCardBag.setRstatus((byte) 0);
        bankCardBag.setRecords(bankCardBagMapper.count(bankCardBag));
        return bankCardBagMapper.select(bankCardBag);
    }
}
