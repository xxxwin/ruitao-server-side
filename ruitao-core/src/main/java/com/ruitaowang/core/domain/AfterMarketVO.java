package com.ruitaowang.core.domain;

import lombok.Data;

@Data
public class AfterMarketVO{
    /**
     *  主键,所属表字段为after_market.id
     */
    private Long id;

    /**
     *  订单编号,所属表字段为after_market.order_id
     */
    private Long orderId;

    /**
     *  备注,所属表字段为after_market.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为after_market.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为after_market.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为after_market.rstatus
     */
    private Byte rstatus;

    /**
     *  订单产品编码,所属表字段为after_market.prod_id
     */
    private Long prodId;

    /**
     *  用户编号,所属表字段为after_market.user_id
     */
    private Long userId;

    /**
     *  售后类型： 0 退货， 1  退款， 3 退货退款，4 换货,所属表字段为after_market.return_type
     */
    private Byte returnType;

    /**
     *  ,所属表字段为after_market.return_cause
     */
    private String returnCause;

    /**
     *  ,所属表字段为after_market.image_id1
     */
    private Long imageId1;

    /**
     *  ,所属表字段为after_market.image_id2
     */
    private Long imageId2;

    /**
     *  ,所属表字段为after_market.image_id3
     */
    private Long imageId3;

    /**
     *  ,所属表字段为after_market.logistic_id
     */
    private Long logisticId;

    /**
     *  ,所属表字段为after_market.logistic_name
     */
    private String logisticName;

    /**
     *  ,所属表字段为after_market.logistic_sn
     */
    private String logisticSn;

    /**
     * 购买者名称 ,所属表字段为goods_order.user_name
     */
    private String userName;

    /**
     * 0 微信支付
     * 1 支付宝支付,所属表字段为goods_order.pay_way
     */
    private Byte payWay;

    /**
     * 订单总金额，不包括运费,所属表字段为goods_order.order_amount
     */
    private Integer orderAmount;

    /**
     * 订单总金额，不包括运费,所属表字段为goods_order.order_amount
     */
    private Integer AfterAmount;

}