package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Base64Img;
import java.util.List;

public interface Base64ImgMapper {
    int insert(Base64Img base64Img);

    int deleteByPK(Long imageId);

    int updateByPK(Base64Img base64Img);

    Base64Img selectByPK(Long imageId);

    long count(Base64Img base64Img);

    List<Base64Img> select(Base64Img base64Img);

    int updateByPrimaryKeyWithBLOBs(Base64Img record);
}