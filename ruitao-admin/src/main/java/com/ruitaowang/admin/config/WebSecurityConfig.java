//package com.ruitaowang.admin.config;
//
//import com.ruitaowang.account.security.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.security.SecurityProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.session.SessionRegistryImpl;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
//import org.springframework.session.web.http.HttpSessionStrategy;
//import org.springframework.web.accept.HeaderContentNegotiationStrategy;
//
///**
// * Created by neal on 11/7/16.
// */
//@Configuration
//@ComponentScan("com.ruitaowang")
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true, jsr250Enabled = true)
//@PropertySource("classpath:application.properties")
//@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 60 * 60 * 3)
//@Order( SecurityProperties.ACCESS_OVERRIDE_ORDER )
//public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//    @Autowired
//    private MyAuthenticationProvider authenticationProvider;//自定义验证
//
//    @Autowired
//    private MyUserDetailsService userDetailsService;
//
//    @Autowired
//    private MyAccessDecisionManager accessDecisionManager;
//
//    @Autowired
//    private MyAuthenticationSuccessHandler myAuthenticationSuccessHandler;
//
//    @Autowired
//    private MyAutherticationFailureHandler myAutherticationFailureHandler;
//
//    @Autowired
//    private MyLogoutSuccessHanlder myLogoutSuccessHanlder;
//
//    @Autowired
//    private MyAccessDeniedHandler accessDeniedHandler;
//
//    @Autowired
//    AjaxAuthenticationEntryPoint authenticationEntryPoint;  //  未登陆时返回 JSON 格式的数据给前端（否则为 html）
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.authenticationProvider(authenticationProvider);
//        auth.userDetailsService(userDetailsService);
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .headers()
//                .frameOptions().sameOrigin().disable()//disable X-Frame-Options
//                .authorizeRequests()
//                .accessDecisionManager(accessDecisionManager)
//                .anyRequest().authenticated()
//                .and()
//                .formLogin().loginPage("/web/login").loginProcessingUrl("/login")
//                .successHandler(myAuthenticationSuccessHandler)
//                .failureHandler(myAutherticationFailureHandler)
//                .usernameParameter("username")
//                .passwordParameter("password")
//                .and()
//                .logout().logoutUrl("/logout").deleteCookies("JSESSIONID").logoutSuccessHandler(myLogoutSuccessHanlder)
//                .and()
//                .sessionManagement()
//                .maximumSessions(1)
//                .expiredUrl("/login?expired=true")
//                .sessionRegistry(new SessionRegistryImpl());
////                    .maxSessionsPreventsLogin(true)//不允许重复登录
//
//        		http.csrf().disable();
//                http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint).accessDeniedHandler(accessDeniedHandler); // 无权访问 JSON 格式的数据
//    }
//
//    @Bean
//    public HttpSessionStrategy httpSessionStrategy() {
//        return new MyHttpSessionStrategy(new HeaderContentNegotiationStrategy());
//    }
//
//}
