package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.FileUploadImage;
import java.util.List;

public interface FileUploadImageMapper {
    int insert(FileUploadImage fileUploadImage);

    int deleteByPK(Long imageId);

    int updateByPK(FileUploadImage fileUploadImage);

    FileUploadImage selectByPK(Long imageId);

    long count(FileUploadImage fileUploadImage);

    List<FileUploadImage> select(FileUploadImage fileUploadImage);
}