package com.ruitaowang.core.domain;

public class AfterMarket extends Page {
    /**
     *  主键,所属表字段为after_market.id
     */
    private Long id;

    /**
     *  订单编号,所属表字段为after_market.order_id
     */
    private Long orderId;

    /**
     *  备注,所属表字段为after_market.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为after_market.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为after_market.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为after_market.rstatus
     */
    private Byte rstatus;

    /**
     *  订单产品编码,所属表字段为after_market.prod_id
     */
    private Long prodId;

    /**
     *  用户编号,所属表字段为after_market.user_id
     */
    private Long userId;

    /**
     *  售后类型： 0 退货， 1  退款， 3 退货退款，4 换货,所属表字段为after_market.return_type
     */
    private Byte returnType;

    /**
     *  ,所属表字段为after_market.return_cause
     */
    private String returnCause;

    /**
     *  ,所属表字段为after_market.image_id1
     */
    private Long imageId1;

    /**
     *  ,所属表字段为after_market.image_id2
     */
    private Long imageId2;

    /**
     *  ,所属表字段为after_market.image_id3
     */
    private Long imageId3;

    /**
     *  ,所属表字段为after_market.logistic_id
     */
    private Long logisticId;

    /**
     *  ,所属表字段为after_market.logistic_name
     */
    private String logisticName;

    /**
     *  ,所属表字段为after_market.logistic_sn
     */
    private String logisticSn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Byte getReturnType() {
        return returnType;
    }

    public void setReturnType(Byte returnType) {
        this.returnType = returnType;
    }

    public String getReturnCause() {
        return returnCause;
    }

    public void setReturnCause(String returnCause) {
        this.returnCause = returnCause == null ? null : returnCause.trim();
    }

    public Long getImageId1() {
        return imageId1;
    }

    public void setImageId1(Long imageId1) {
        this.imageId1 = imageId1;
    }

    public Long getImageId2() {
        return imageId2;
    }

    public void setImageId2(Long imageId2) {
        this.imageId2 = imageId2;
    }

    public Long getImageId3() {
        return imageId3;
    }

    public void setImageId3(Long imageId3) {
        this.imageId3 = imageId3;
    }

    public Long getLogisticId() {
        return logisticId;
    }

    public void setLogisticId(Long logisticId) {
        this.logisticId = logisticId;
    }

    public String getLogisticName() {
        return logisticName;
    }

    public void setLogisticName(String logisticName) {
        this.logisticName = logisticName == null ? null : logisticName.trim();
    }

    public String getLogisticSn() {
        return logisticSn;
    }

    public void setLogisticSn(String logisticSn) {
        this.logisticSn = logisticSn == null ? null : logisticSn.trim();
    }
}