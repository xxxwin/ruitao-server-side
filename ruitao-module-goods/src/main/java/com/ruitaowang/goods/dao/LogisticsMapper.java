package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Logistics;
import java.util.List;

public interface LogisticsMapper {
    int insert(Logistics logistics);

    int deleteByPK(Long logisticsId);

    int updateByPK(Logistics logistics);

    Logistics selectByPK(Long logisticsId);

    long count(Logistics logistics);

    List<Logistics> select(Logistics logistics);
}