package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Address;
import com.ruitaowang.core.domain.GoodsAlbum;
import com.ruitaowang.core.domain.GoodsAttribute;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsAlbumMapper;
import com.ruitaowang.goods.dao.GoodsAttributeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class GoodsAlbumService implements BaseService<GoodsAlbum>{

    //// TODO: 2016/12/2 商品属性service,照抄未做审核

    @Autowired
    private GoodsAlbumMapper goodsAlbumMapper;

    @Override
    public GoodsAlbum insert(GoodsAlbum goodsAlbum) {

        Long time = System.currentTimeMillis();
        goodsAlbum.setCtime(time);
        goodsAlbum.setMtime(time);
        goodsAlbumMapper.insert(goodsAlbum);
        return goodsAlbum;
    }

    @Override
    public int delete(Long id) {
        GoodsAlbum goodsAlbum=new GoodsAlbum();
        goodsAlbum.setImageId(id);
        goodsAlbum.setRstatus((byte)1);
        Long time = System.currentTimeMillis();
        goodsAlbum.setMtime(time);
        return goodsAlbumMapper.updateByPK(goodsAlbum);
    }

    public int deleteByPK(Long id) {
        return goodsAlbumMapper.deleteByPK(id);
    }

    @Override
    public GoodsAlbum update(GoodsAlbum goodsAlbum) {
        Long time = System.currentTimeMillis();
        goodsAlbum.setMtime(time);
        goodsAlbumMapper.updateByPK(goodsAlbum);
        return goodsAlbum;
    }

    @Override
    public GoodsAlbum selectByPK(Long imageId) {
        return goodsAlbumMapper.selectByPK(imageId);
    }

    @Override
    public List<GoodsAlbum> select(GoodsAlbum goodsAlbum)
    {
        goodsAlbum.setRstatus((byte) 0);
        return goodsAlbumMapper.select(goodsAlbum);
    }
    
    public List<GoodsAlbum> selectForPage(GoodsAlbum goodsAlbum) {
    	goodsAlbum.setRstatus((byte) 0);
    	goodsAlbum.setRecords(goodsAlbumMapper.count(goodsAlbum));
        return goodsAlbumMapper.select(goodsAlbum);
    }
}
