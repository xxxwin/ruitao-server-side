package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.ActivityUserLink;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.ActivityUserLinkAdMapper;
import com.ruitaowang.goods.dao.ActivityUserLinkMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityUserLinkService implements BaseService<ActivityUserLink> {

    @Autowired
    private ActivityUserLinkMapper activityUserLinkMapper;
    @Autowired
    private ActivityUserLinkAdMapper activityUserLinkAdMapper;

    @Override
    public ActivityUserLink insert(ActivityUserLink activityUserLink) {
        Long time = System.currentTimeMillis();
        activityUserLink.setCtime(time);
        activityUserLink.setMtime(time);
        activityUserLinkMapper.insert(activityUserLink);
        return activityUserLink;
    }

    @Override
    public int delete(Long id) {
        ActivityUserLink activityUserLink = new ActivityUserLink();
        activityUserLink.setUserLinkId(id);
        activityUserLink.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        activityUserLink.setMtime(time);
        return activityUserLinkMapper.updateByPK(activityUserLink);
    }

    @Override
    public ActivityUserLink update(ActivityUserLink activityUserLink) {
        Long time = System.currentTimeMillis();
        activityUserLink.setMtime(time);
        activityUserLinkMapper.updateByPK(activityUserLink);
        return activityUserLink;
    }

    @Override
    public ActivityUserLink selectByPK(Long id) {
        return activityUserLinkMapper.selectByPK(id);
    }

    @Override
    public List<ActivityUserLink> select(ActivityUserLink activityUserLink) {
        activityUserLink.setRstatus((byte) 0);
        return activityUserLinkMapper.select(activityUserLink);
    }

    public List<ActivityUserLink> selectForReadPage(ActivityUserLink activityUserLink) {
        activityUserLink.setRstatus((byte) 0);
        activityUserLink.setRecords(activityUserLinkAdMapper.countRead(activityUserLink));
        return activityUserLinkAdMapper.selectRead(activityUserLink);
    }

    public long count(ActivityUserLink activityUserLink) {
        activityUserLink.setRstatus((byte)0);
        return activityUserLinkMapper.count(activityUserLink);
    }

    public long selectForReadCount(ActivityUserLink activityUserLink){
        activityUserLink.setRstatus((byte) 0);
        return activityUserLinkAdMapper.countRead(activityUserLink);
    }

    public long todayCount(ActivityUserLink activityUserLink){
        return activityUserLinkAdMapper.todayCount(activityUserLink);
    }
}
