package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LocalOrder;

import java.util.List;

public interface LocalOrderAdMapper {

    List<LocalOrder> fjOrder(LocalOrder localOrder);

    long fjOrderCount(LocalOrder localOrder);

}