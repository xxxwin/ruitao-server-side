package com.ruitaowang.account.service;

import com.ruitaowang.core.domain.Wallet;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.account.dao.WalletMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 01/02/2017.
 */
@Service
public class WalletService implements BaseService<Wallet> {
    @Autowired
    private WalletMapper walletMapper;

    @Override
    public Wallet insert(Wallet wallet) {
        Long time = System.currentTimeMillis();
        wallet.setCtime(time);
        wallet.setMtime(time);
        walletMapper.insert(wallet);
        return wallet;
    }

    @Override
    public int delete(Long id) {
        Wallet wallet = new Wallet();
        wallet.setUserId(id);
        wallet.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        wallet.setMtime(time);
        return walletMapper.updateByPK(wallet);
    }

    @Override
    public Wallet update(Wallet wallet) {
        Long time = System.currentTimeMillis();
        wallet.setMtime(time);
        walletMapper.updateByPK(wallet);
        return wallet;
    }
    @Override
    public Wallet selectByPK(Long id) {
        return walletMapper.selectByPK(id);
    }

    @Override
    public List<Wallet> select(Wallet wallet) {
        wallet.setRstatus((byte) 0);
        return walletMapper.select(wallet);
    }

    public List<Wallet> selectForPage(Wallet wallet) {
        wallet.setRstatus((byte) 0);
        wallet.setRecords(walletMapper.count(wallet));
        return walletMapper.select(wallet);
    }

    /**
     * 获取 用户余额
     * @param userId
     * @param type
     * @return
     */
    public Integer getUserMoney(Long userId,int type) {
        int money=0;
        Wallet wallet=walletMapper.selectByPK(userId);
        if (ObjectUtils.isEmpty(wallet)){
            Wallet wallet1 = new Wallet();
            wallet1.setUserId(userId);
            Long time = System.currentTimeMillis();
            wallet1.setCtime(time);
            wallet1.setMtime(time);
            walletMapper.insert(wallet1);
            return money;
        }
        if (type==1){
           money=wallet.getUserBalance();
        }else if (type == 2){
            money=wallet.getUserScore();
        }
        return money;
    }

    public void createUserProfit(Long userId, Integer profit){
        Wallet wallet = this.selectByPK(userId);
        if(ObjectUtils.isEmpty(wallet)){
            wallet = new Wallet();
            wallet.setUserId(userId);
            wallet.setUserBalance(profit);
            wallet.setUserAmount(profit);
            this.insert(wallet);
        }else{
            wallet.setUserBalance(wallet.getUserBalance() + profit);
            wallet.setUserAmount(wallet.getUserAmount() + profit);
            this.update(wallet);
        }
    }
    public void createUserProfitForScore(Long userId, Integer profit){
        Wallet wallet = this.selectByPK(userId);
        if(ObjectUtils.isEmpty(wallet)){
            wallet = new Wallet();
            wallet.setUserId(userId);
            wallet.setUserScore(profit);
            wallet.setUserScoreTotal(profit);
            this.insert(wallet);
        }else{
            wallet.setUserScore(wallet.getUserScore() + profit);
            wallet.setUserScoreTotal(wallet.getUserScoreTotal() + profit);
            this.update(wallet);
        }
    }
}
