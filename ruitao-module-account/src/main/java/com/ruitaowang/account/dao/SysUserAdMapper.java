package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.SysUserVO;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SysUserAdMapper {
    int insert(SysUser sysUser);

    int deleteByPK(Long id);

    int updateByPK(SysUser sysUser);

    SysUser selectByPK(Long id);

    long count(SysUser sysUser);

    List<SysUser> select(SysUser sysUser);

    List<SysUser> selectUserForCommunity(SysUserVO sysUserVO);
}