package com.ruitaowang.admin.view;

import com.alibaba.fastjson.JSON;
import com.ruitaowang.account.dao.UserRoleLinkMapper;
import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.security.MyUser;
import com.ruitaowang.account.service.ResourceService;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.AfterMarket;
import com.ruitaowang.core.domain.ResourceMenu;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.UserRoleLink;
import com.ruitaowang.core.properties.ConfigProperties;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.AfterMarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import wx.wechat.common.Configure;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 入口
 *
 */
@RestController
public class PageController extends BaseController {

    @Autowired
    ConfigProperties configProperties;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private SysUserService sysUserService;

    @RequestMapping(value = {"/web/login"}, method = RequestMethod.GET)
    public ModelAndView loginForWeb() {
        return new ModelAndView("/web/login");
    }

    @RequestMapping(value = {"/logout"}, method = RequestMethod.GET)
    public ModelAndView loginForOut() {
        return new ModelAndView("/web/login");
    }

    /**
     * 登录接口
     *
     * @param username
     * @param password
     * @param request
     * @return
     */
    @RequestMapping(value = {"/web/login"}, method = RequestMethod.POST)
    public String login(String username,String password,HttpServletRequest request){
        if (ObjectUtils.isEmpty(username) && ObjectUtils.isEmpty(password)) {
            return "请填写用户名密码";
        }
        SysUser sysUser = new SysUser();
        sysUser.setUsername(username);
        List<SysUser> sysUsers=sysUserService.select(sysUser);
        if (ObjectUtils.isEmpty(sysUsers)){
            return "请关注公众号绑定手机";
        }else {
            HttpSession session = request.getSession();
            if (session.getAttribute("user") == null) {
                session.setAttribute("user", JSON.toJSONString(sysUsers.get(0)));
            }
        }
        return  JSON.toJSONString(MsgOut.success("登陆成功"));
    }

    /**
     * 跳转主页
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/web/index")
    public ModelAndView indexForWeb(Model model,HttpServletRequest request) {
        //MyUser user = LYSecurityUtil.currentUser();
        String user = (String) request.getSession().getAttribute("user");
        SysUser sysUser = JSON.parseObject(user,SysUser.class);
        //menu
        if (ObjectUtils.isEmpty(sysUser)){
            return new ModelAndView("/web/login");
        }
        List<ResourceMenu> resourceMenu = resourceService.selectMyGroupResources();
        if(ObjectUtils.isEmpty(resourceMenu)){
            return new ModelAndView("/web/login");
        }
        model.addAttribute("resources", resourceMenu);
        model.addAttribute("user", sysUser);
        model.addAttribute("countInfo", 10);
        return new ModelAndView("/web/index");
    }
}
