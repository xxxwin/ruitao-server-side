package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.Resource;

import java.util.List;

public interface ResourceAdMapper {
    List<Resource> selectBatch(List<Long> resourceIds);
    List<Resource> selectBatchByLink(List<String> links);
}