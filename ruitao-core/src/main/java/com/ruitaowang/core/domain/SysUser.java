package com.ruitaowang.core.domain;

public class SysUser extends Page {
	
	//序列号反序列化
	private static final long serialVersionUID = -4283122855186102848L;
	
	/**
     * 自增主键,所属表字段为t_sys_user.id
     */
    private Long id;

    /**
     * 账号,所属表字段为t_sys_user.username
     */
    private String username;

    /**
     * 密码,所属表字段为t_sys_user.password
     */
    private String password;

    /**
     * 部门,所属表字段为t_sys_user.department
     */
    private String department;

    /**
     * 手机号,所属表字段为t_sys_user.phone
     */
    private String phone;

    /**
     * 职位,所属表字段为t_sys_user.position
     */
    private String position;

    /**
     * 备注,所属表字段为t_sys_user.remark
     */
    private String remark;

    /**
     * 邮箱,所属表字段为t_sys_user.email
     */
    private String email;

    /**
     * 创建时间,所属表字段为t_sys_user.ctime
     */
    private Long ctime;

    /**
     * 最后更新时间,所属表字段为t_sys_user.mtime
     */
    private Long mtime;

    /**
     * 状态 0:正常 1:删除,所属表字段为t_sys_user.rstatus
     */
    private Byte rstatus;

    /**
     * 真实姓名,所属表字段为t_sys_user.real_name
     */
    private String realName;

    /**
     * 最后登录IP,所属表字段为t_sys_user.last_ip
     */
    private String lastIp;

    /**
     * 最后登录时间,所属表字段为t_sys_user.last_time
     */
    private Long lastTime;

    /**
     * 1 锁定
     * 0 正常,所属表字段为t_sys_user.locked
     */
    private Boolean locked;

    /**
     * 0： 普通会员 （消费者 两类1 网站消费，2 实体店扫码消费，没有绑定手机号）
     * 1：本地生活 商家用户
     * 2：城市合伙人
     * 4：区代理 ,所属表字段为t_sys_user.user_type
     */
    private Byte userType;

    /**
     * ,所属表字段为t_sys_user.headimgurl
     */
    private String headimgurl;

    /**
     * 二维码,所属表字段为t_sys_user.qrimgurl
     */
    private String qrimgurl;

    /**
     * 昵称,所属表字段为t_sys_user.nickname
     */
    private String nickname;

    /**
     * 身份证号,所属表字段为t_sys_user.id_card
     */
    private String idCard;

    /**
     * 微信号,所属表字段为t_sys_user.wechat_id
     */
    private String wechatId;

    /**
     * 微信二维码,所属表字段为t_sys_user.wechat_phone
     */
    private String wechatPhoto;

    /**
     * 背景图片,所属表字段为t_sys_user.background_photo
     */
    private String backgroundPhoto;

    /**
     * qq号码,所属表字段为t_sys_user.qq_number
     */
    private String qqNumber;

    /**
     * 是否关注,所属表字段为t_sys_user.subscribe
     */
    private Byte subscribe;

    /**
     * 是否是否接受推送消息,所属表字段为t_sys_user.push_message
     */
    private Byte pushMessage;

    /**
     * 是否弹框,所属表字段为t_sys_user.alert_frame
     */
    private Byte alertFrame;

    public Byte getAlertFrame() {
        return alertFrame;
    }

    public void setAlertFrame(Byte alertFrame) {
        this.alertFrame = alertFrame;
    }

    public Byte getPushMessage() {
        return pushMessage;
    }

    public void setPushMessage(Byte pushMessage) {
        this.pushMessage = pushMessage;
    }

    public Byte getSubscribe() { return subscribe; }

    public void setSubscribe(Byte subscribe) { this.subscribe = subscribe; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position == null ? null : position.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp == null ? null : lastIp.trim();
    }

    public Long getLastTime() {
        return lastTime;
    }

    public void setLastTime(Long lastTime) {
        this.lastTime = lastTime;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public Byte getUserType() {
        return userType;
    }

    public void setUserType(Byte userType) {
        this.userType = userType;
    }

    public String getHeadimgurl() {
        return headimgurl;
    }

    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl == null ? null : headimgurl.trim();
    }

    public String getQrimgurl() {
        return qrimgurl;
    }

    public void setQrimgurl(String qrimgurl) {
        this.qrimgurl = qrimgurl == null ? null : qrimgurl.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public String getWechatId() { return wechatId; }

    public void setWechatId(String wechatId) { this.wechatId = wechatId; }

    public String getWechatPhoto() {
        return wechatPhoto;
    }

    public void setWechatPhoto(String wechatPhoto) {
        this.wechatPhoto = wechatPhoto;
    }

    public String getBackgroundPhoto() {
        return backgroundPhoto;
    }

    public void setBackgroundPhoto(String backgroundPhoto) {
        this.backgroundPhoto = backgroundPhoto;
    }

    public String getQqNumber() {
        return qqNumber;
    }

    public void setQqNumber(String qqNumber) {
        this.qqNumber = qqNumber;
    }
}