package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.ExPrice;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.ExPriceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class ExPriceService implements BaseService<ExPrice> {
    @Autowired
    private ExPriceMapper ExPriceMapper;

    @Override
    public ExPrice insert(ExPrice ExPrice) {
        Long time = System.currentTimeMillis();
        ExPrice.setCtime(time);
        ExPrice.setMtime(time);
        ExPriceMapper.insert(ExPrice);
        return ExPrice;
    }

    @Override
    public int delete(Long id) {
        ExPrice ExPrice = new ExPrice();
        ExPrice.setExpriceId(id);
        ExPrice.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        ExPrice.setMtime(time);
        return ExPriceMapper.updateByPK(ExPrice);
    }

    @Override
    public ExPrice update(ExPrice ExPrice) {
        Long time = System.currentTimeMillis();
        ExPrice.setMtime(time);
        ExPriceMapper.updateByPK(ExPrice);
        return ExPrice;
    }

    @Override
    public ExPrice selectByPK(Long id) {

        return ExPriceMapper.selectByPK(id);
    }

    @Override
    public List<ExPrice> select(ExPrice ExPrice) {
        ExPrice.setRstatus((byte) 0);
        return ExPriceMapper.select(ExPrice);
    }

    public List<ExPrice> selectForPage(ExPrice ExPrice) {
        ExPrice.setRstatus((byte) 0);
        ExPrice.setRecords(ExPriceMapper.count(ExPrice));
        return ExPriceMapper.select(ExPrice);

    }
}

