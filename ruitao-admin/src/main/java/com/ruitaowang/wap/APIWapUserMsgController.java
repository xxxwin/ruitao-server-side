package com.ruitaowang.wap;

import com.alibaba.fastjson.JSON;
import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.UserMsg;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.UserMsgService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public class APIWapUserMsgController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserMsgService userMsgService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 获取聊天记录
     * @param page
     * @param userMsg
     * @return
     */
    @RequestMapping(value = "/api/wap/userMsg", method = RequestMethod.GET)
    public String userMsgForPage(int page, UserMsg userMsg){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        userMsg.setPage(page);
        if (ObjectUtils.isEmpty(userMsg.getFromUserId())) {
            userMsg.setFromUserId(user.getId());
        }
        List<UserMsg> userMsgList = userMsgService.selectForPage(userMsg);
        MsgOut o = MsgOut.success(userMsgList);
        o.setRecords(userMsg.getRecords());
        o.setTotal(userMsg.getTotal());
        return this.renderJson(o);
    }

    /**
     * 获取个人聊天列表
     * @param page
     * @param userMsg
     * @return
     */
    @RequestMapping(value = "/api/wap/userMsgList", method = RequestMethod.GET)
    public String userMsgList(int page, UserMsg userMsg,@RequestHeader(value="headId",required = false) Long userId){
//        SysUser user = LYSecurityUtil.currentSysUser();
//        if (ObjectUtils.isEmpty(user)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        userMsg.setPage(page);
        userMsg.setFromUserId(userId);
        List<UserMsg> userMsgList = userMsgService.userMsgList(userMsg);
        MsgOut o = MsgOut.success(userMsgList);
        o.setRecords(userMsg.getRecords());
        o.setTotal(userMsg.getTotal());
        return this.renderJson(o);
    }

    /**
     * 获取公司聊天列表
     * @param page
     * @param userMsg
     * @return
     */
    @RequestMapping(value = "/api/wap/companyMsgList", method = RequestMethod.GET)
    public String companyMsgList(int page, UserMsg userMsg,@RequestHeader(value="headId",required = false) Long userId){
//        SysUser user = LYSecurityUtil.currentSysUser();
//        if (ObjectUtils.isEmpty(user)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Company company = companyService.selectByCompanyStaff(userId);
        userMsg.setPage(page);
        userMsg.setCompanyId(company.getCompanyId());
        List<UserMsg> userMsgList = userMsgService.userMsgList(userMsg);
        MsgOut o = MsgOut.success(userMsgList);
        o.setRecords(userMsg.getRecords());
        o.setTotal(userMsg.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/userMsg", method = RequestMethod.PUT)
    public String updateUserMsg(UserMsg userMsg){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(userMsgService.update(userMsg));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/userMsg", method = RequestMethod.POST)
    public String createUserMsg(UserMsg userMsg){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(userMsgService.insert(userMsg));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/userMsg/delete", method = RequestMethod.DELETE)
    public String deleteUserMsg(Long id){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(userMsgService.delete(id));
        return this.renderJson(o);
    }

    @MessageMapping("/notify/1v1")
    @SendTo("/topic/notify/1v1")
    public String liaotianForUser(String json,Principal principal) {
        if (ObjectUtils.isEmpty(principal) || "anonymousUser".equalsIgnoreCase(principal.getName())) {
            return this.renderJson(MsgOut.error("parameter is null."));
        }
        UserMsg userMsg = JSON.parseObject(json, UserMsg.class);
        if (ObjectUtils.isEmpty(userMsg.getFromUserId()) || ObjectUtils.isEmpty(userMsg.getCompanyId())) {
            return this.renderJson(MsgOut.error("parameter is null."));
        }
        SysUser sysUser = sysUserService.selectByPK(userMsg.getFromUserId());
        Company company = companyService.selectByPK(userMsg.getCompanyId());
        userMsg.setFromUserName(sysUser.getNickname());
        userMsg.setFromUserPhoto(sysUser.getHeadimgurl());
        userMsg.setCompanyName(company.getCompanyName());
        userMsg.setCompanyPhoto(company.getHeadimgurl());
        userMsgService.insert(userMsg);
        return this.renderJson(MsgOut.success(userMsg));
    }

//    @MessageMapping("/notify/1v1ForCompany")
//    @SendTo("/topic/notify/1v1ForCompany")
//    public String liaotianForCompany(String json,Principal principal) {
//        SysUser user = LYSecurityUtil.currentSysUser();
//        if (ObjectUtils.isEmpty(user)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        if (ObjectUtils.isEmpty(principal) || "anonymousUser".equalsIgnoreCase(principal.getName())) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        UserMsg userMsg = JSON.parseObject(json, UserMsg.class);
//        userMsg.setFromUserId(user.getId());
//        userMsgService.insert(userMsg);
//        return this.renderJson(MsgOut.success(userMsg));
//    }

}
