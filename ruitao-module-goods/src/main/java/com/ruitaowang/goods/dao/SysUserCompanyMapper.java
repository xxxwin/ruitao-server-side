package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.SysUserCompany;
import java.util.List;

public interface SysUserCompanyMapper {
    int insert(SysUserCompany sysUserCompany);

    int deleteByPK(Long id);

    int updateByPK(SysUserCompany sysUserCompany);

    SysUserCompany selectByPK(Long id);

    long count(SysUserCompany sysUserCompany);

    List<SysUserCompany> select(SysUserCompany sysUserCompany);
}