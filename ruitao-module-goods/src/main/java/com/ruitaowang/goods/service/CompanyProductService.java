package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.CompanyProduct;
import com.ruitaowang.core.domain.LocalOrder;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CompanyProductMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 1/3/17.
 */
@Service
public class CompanyProductService implements BaseService<CompanyProduct> {
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private CompanyProductMapper companyProductMapper;
    @Autowired
    private CompanyProductService companyProductService;
    @Autowired
    private LocalOrderService localOrderService;
    @Autowired
    private OrderProdService orderProdService;

    @Override
    public CompanyProduct insert(CompanyProduct companyProduct) {
        Long time = System.currentTimeMillis();
        companyProduct.setCtime(time);
        companyProduct.setMtime(time);
        companyProductMapper.insert(companyProduct);
        return companyProduct;
    }

    @Override
    public int delete(Long id) {
        CompanyProduct companyProduct = new CompanyProduct();
        companyProduct.setCompanyId(id);
        companyProduct.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        companyProduct.setMtime(time);
        return companyProductMapper.updateByPK(companyProduct);
    }

    @Override
    public CompanyProduct update(CompanyProduct companyProduct) {
        Long time = System.currentTimeMillis();
        companyProduct.setMtime(time);
        companyProductMapper.updateByPK(companyProduct);
        return companyProduct;
    }

    @Override
    public CompanyProduct selectByPK(Long id) {
        return companyProductMapper.selectByPK(id);
    }

    @Override
    public List<CompanyProduct> select(CompanyProduct companyProduct) {
        companyProduct.setRstatus((byte) 0);
        return companyProductMapper.select(companyProduct);
    }

    public CompanyProduct selectFistDiscount(Long companyId) {
        CompanyProduct where = new CompanyProduct();
        where.setCompanyId(companyId);
        where.setOrderBy("product_id desc");
        List<CompanyProduct> companyProducts = companyProductMapper.select(where);
        if(ObjectUtils.isEmpty(companyProducts)){
            return null;
        }
        return companyProducts.get(0);
    }

    public LocalOrder updateLocalOrderDiscount(Long productId, LocalOrder localOrder) {
        CompanyProduct companyProduct = companyProductService.selectByPK(productId);
        if(ObjectUtils.isEmpty(companyProduct)) {
            return null;
        }

        Byte discountType = companyProduct.getDiscountType();
        LOGGER.info("discountType  -> {}, ObjectUtils.isEmpty(discountType) -> {}", discountType, ObjectUtils.isEmpty(discountType));
        if (ObjectUtils.isEmpty(discountType)) {
            return null;
        }
        int score = 0;
        StringBuffer remark = new StringBuffer();
        int amountMoney = localOrder.getPrice();
        if (discountType.intValue() == 0) {
            int reach5 = companyProduct.getReach5();
            int reach4 = companyProduct.getReach4();
            int reach3 = companyProduct.getReach3();
            int reach2 = companyProduct.getReach2();
            int reach1 = companyProduct.getReach1();

            if (reach5 > 0 && amountMoney >= reach5) {
                score = companyProduct.getScore5();
            } else if (reach4 > 0 && amountMoney >= reach4) {
                score = companyProduct.getScore4();
            } else if (reach3 > 0 && amountMoney >= reach3) {
                score = companyProduct.getScore3();
            } else if (reach2 > 0 && amountMoney >= reach2) {
                score = companyProduct.getScore2();
            } else if (reach1 > 0 && amountMoney >= reach1) {
                score = companyProduct.getScore1();
            }
            remark.append("reach1=").append(reach1/100).append(", score1=").append(companyProduct.getScore1()).append("|");
            remark.append("reach2=").append(reach2/100).append(", score2=").append(companyProduct.getScore2()).append("|");
            remark.append("reach3=").append(reach3/100).append(", score3=").append(companyProduct.getScore3()).append("|");
            remark.append("reach4=").append(reach4/100).append(", score4=").append(companyProduct.getScore4()).append("|");
            remark.append("reach5=").append(reach5/100).append(", score5=").append(companyProduct.getScore5()).append("|");
        }
        if (discountType.intValue() == 1) {
            score = localOrder.getPrice()-(companyProduct.getPercentage() * localOrder.getPrice() / 10000);
            remark.append("percentage=").append(companyProduct.getPercentage());
        }
        LOGGER.info("满减优惠价格: score={}, 订单总额：price={}, discountType={}, fan={}", score, localOrder.getPrice(), discountType, remark);
        //update local_order
        localOrder.setScore(score);
        localOrder.setRemark(remark.toString());
        localOrderService.update(localOrder);
        return localOrder;
    }

}
