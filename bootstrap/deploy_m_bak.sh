#!/usr/bin/env bash

sudo /usr/bin/ssh -t -t ruitao@int.bak.bmw.ruitaowang.com /bin/bash << EOF

cd /home/ruitao

ps aux |grep tomcat |grep -v 'grep' |awk '{print $2}'|xargs kill -9

rm -rf /home/ruitao/tomcat/webapps/*

cp /home/ruitao/ROOT.war /home/ruitao/tomcat/webapps/ROOT.war

sh /home/ruitao/tomcat/bin/startup.sh

sleep 10

sed -i "s/test.ruitaowang/www.ruitaowang/1" /home/ruitao/tomcat/webapps/ROOT/wap/js/commons.js
sed -i '0,/http:/{s/http:/https:/}' /home/ruitao/tomcat/webapps/ROOT/wap/js/commons.js

sed -i "s/test.ruitaowang/www.ruitaowang/1" /home/ruitao/tomcat/webapps/ROOT/pc/js/commons.js
sed -i 's/http:/https:/1' /home/ruitao/tomcat/webapps/ROOT/pc/js/commons.js

sed -i "s/test.ruitaowang/www.ruitaowang/1" /home/ruitao/tomcat/webapps/ROOT/wap/orderapp/js/dccommon.js
sed -i 's/http:/https:/1' /home/ruitao/tomcat/webapps/ROOT/wap/orderapp/js/dccommon.js


tail -f /home/ruitao/tomcat/logs/catalina.out

EOF

echo 'Uploaded...'