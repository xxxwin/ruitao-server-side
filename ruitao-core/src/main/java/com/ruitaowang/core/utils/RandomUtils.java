package com.ruitaowang.core.utils;

import com.alibaba.fastjson.JSONArray;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.utils.aliyun.SendSMSHelper;
import org.hashids.Hashids;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.ObjectUtils;
import org.springframework.web.util.WebUtils;
import tech.lingyi.wx.utils.DESUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static java.time.DayOfWeek.MONDAY;

/**
 * Created by neal on 12/4/16.
 */
public class RandomUtils {
    private static final String KEY_ = "12******Pa$$w0rd";
    private  static  Hashids hashids = new Hashids(KEY_, 8);
    public static int random3(){
        SecureRandom secureRandom = new SecureRandom();
        return secureRandom.nextInt(999);
    }

    public static String randomString(int len){
        StringBuffer base = new StringBuffer();
        SecureRandom secureRandom = new SecureRandom();
        for (int i = 0; i < len; i++) {
            if(i == 0){
                int random = secureRandom.nextInt(9);
                random = random == 0 ? 1 : random;
                base.append(random);
                continue;
            }
            base.append(secureRandom.nextInt(9));
        }
        return base.toString();
    }

    public static String genOrderSN(Long buyerId){
        return TimeUtils.getCurrentDatetime(TimeUtils.TimeFormat.LONG_DATE_PATTERN_WITH_MILSEC_NONE_ORDER) + "l" + buyerId;
    }


    public static String encodeToken(Long userId){
        return hashids.encode(userId);
    }

    public static Long decodeToken(String token) throws Exception{
        return hashids.decode(token)[0];
    }

     public static Long getUserId(String token) throws Exception{
         return decodeToken(token);
    }


    public static String hashPassword(String passwrodPlaint){
        return MD5Util.encryptMD5(passwrodPlaint);
    }

    /**
     * 发红包 1 积分余额>0,没有积分发不了
     */
    public static int[] luckyMoney(int total, int num){
        int min = 1; //每个人最少0.01元
//        int total = 10000;//总金额
//        int num = 10;//10个红包
        int result[] = new int[num];
        for (int i=1; i<num; i++){
//        for (int i=0; i<num; i++){
            int safeTotal = (total - ( num - i ) * min)/(num -i);
            int money = min + (int) (Math.random() * (safeTotal - min + 1));
            total = total - money;
            System.out.println("第 " + (i + 1) + " 个红包，金额 " + money);
            result[i-1] = money;
        }
        result[num-1] = total;
        System.out.println("第 " + num + " 个红包，金额 " + total);
        return result;
    }

//    /**
//     * 发红包 1 积分余额>0,没有积分发不了
//     */
//    public static int[] luckyMoneyPT(int total, int num){
//        int min = 1; //每个人最少0.01元
////        int total = 10000;//总金额
////        int num = 10;//10个红包
//        int result[] = new int[num];
//        for (int i=1; i<num; i++){
//            int money= total / num;
//            total = total - money;
//            System.out.println("第 " + i + " 个红包，金额 " + money);
//            result[i-1] = money;
//        }
//        result[num-1] = total;
//        System.out.println("第 " + num + " 个红包，金额 " + total);
//        return result;
//    }
    public static String luckyMoneyS(int total, int num){
        int[] result = luckyMoney(total, num);
        StringBuilder sb = new StringBuilder();
        Arrays.stream(result).forEach(item->{sb.append(item).append("-");});
        return sb.toString();
    }
//     /   volatile
    int i = 0;
    static int ii = 0;
public static void main(String[] args) throws Exception {
//    System.out.println(235*100/356);
//    List<Integer> list = Arrays.asList(1,2,3,4);
//    System.out.println(list);
//    Collections.reverse(list);
//    System.out.println(list);
//    System.out.println(hashPassword("ruitao_123456"));
//    System.out.println(TimeUtils.getDateAfterNDays("2017-03-19", 32));
//    System.out.println(TimeUtils.dateToEpoch("20170319000"));
//    Long time = System.currentTimeMillis();

//    System.out.println(TimeUtils.getDatetime(TimeUtils.TimeFormat.LONG_DATE_PATTERN_LINE, time));
//    Long l = 1L;
//    System.out.println(l.longValue() == 1l);
//
//    Document doc = null;
//    String url = "https://mp.weixin.qq.com/s/UUO-I0TTssLO6dGweQJNfQ";
//    try {
//        doc = Jsoup.connect(url).get();
//        Elements elements = doc.select("div.profile_inner").remove();
//        System.out.println(elements.toString());
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println(doc);
//    } catch (IOException e) {
//        e.printStackTrace();
//    }

//    Element content = doc.getElementById("img-content");//获取id为img-content的dom节点
//    System.out.println(content.toString());

//    LocalDate today = LocalDate.now();
//
//    // Go backward to get Monday
//    LocalDate monday = today;
//    while (monday.getDayOfWeek() != DayOfWeek.MONDAY)
//    {
//        monday = monday.minusDays(1);
//    }
//
//    // Go forward to get Sunday
//    LocalDate sunday = today;
//    while (sunday.getDayOfWeek() != DayOfWeek.SUNDAY)
//    {
//        sunday = sunday.plusDays(1);
//    }
//    ZoneId zoneId = ZoneId.systemDefault(); // or: ZoneId.of("Europe/Oslo");
//    System.out.println(zoneId);
//    System.out.println("Today: " + today);
//    System.out.println("Monday of the Week: " + monday + " , " + monday.atStartOfDay(zoneId).toEpochSecond());
//    System.out.println("Sunday of the Week: " + sunday + " , " + (sunday.atStartOfDay(zoneId).toEpochSecond() + 24 * 60 * 60));

//    StringBuffer sb = new StringBuffer("18612452056");
//    sb.replace(3, 7, "****");
//    System.out.println(sb.toString());
//
//    SysUser u1 = new SysUser(); u1.setId(1L);
//    SysUser u2 = new SysUser(); u2.setId(1L);
//    System.out.println(u1.getId() == u2.getId());

//    SendSMSHelper.sendSMS("955355789", "123456");


//    JSONArray a = JSONArray.parseArray("[{\"id\":72,\"name\":\"猪棒骨\",\"price\":2000,\"categoryId\":6},{\"id\":69,\"name\":\"猪肝\",\"price\":2000,\"categoryId\":6},{\"id\":23,\"name\":\"珍珠翡翠白玉汤\",\"price\":1000,\"categoryId\":2},{\"id\":21,\"name\":\"白玉粥\",\"price\":1000,\"categoryId\":2},{\"id\":19,\"name\":\"糖油饼\",\"price\":800,\"categoryId\":2}]");
//    System.out.println(a.toString());

//    luckyMoney(10000, 10);

//    System.out.println(encodeToken(1L));

//    System.out.println(hashPassword("13331066836"));

//    new RandomUtils().runi();

    System.out.println(System.nanoTime());
    System.out.println(System.currentTimeMillis());
}
void runi(){
    Thread t1 = new Thread(){
        @Override
        public void run() {
            for (int j = 0; j < 10000; j++) {
                i++;
                ii++;
//                System.out.println("t1 i -> "+i);
//                System.out.println("t1 ii -> "+ii);

//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
            }
//            System.out.println("t1 i -> "+i);
            System.out.println("t1 ii -> "+ii);
        }
    };
    Thread t2 = new Thread(){
        @Override
        public void run() {
            for (int j = 0; j < 10000; j++) {
//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                i++;
                ii++;
            }

//            System.out.println("t2 i -> "+i);
            System.out.println("t2 ii -> "+ii);
        }
    };

    Thread t3 = new Thread(){
        @Override
        public void run() {
            for (int j = 0; j < 10000; j++) {
//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                i++;
                ii++;
            }
//            System.out.println("t3 i -> "+i);
            System.out.println("t3 ii -> "+ii);
        }
    };

    t1.start();
    t2.start();
    t3.start();
}

}
