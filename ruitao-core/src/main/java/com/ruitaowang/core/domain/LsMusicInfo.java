package com.ruitaowang.core.domain;

public class LsMusicInfo extends Page {
    /**
     *  自增编码,所属表字段为ls_music_info.music_id
     */
    private Long musicId;

    /**
     *  歌手名,所属表字段为ls_music_info.music_name
     */
    private String musicName;

    /**
     *  歌手缩略图,所属表字段为ls_music_info.singer_name
     */
    private String singerName;

    /**
     *  歌手编码,所属表字段为ls_music_info.singer_id
     */
    private Long singerId;

    /**
     *  金额,所属表字段为ls_music_info.music_price
     */
    private Integer musicPrice;

    /**
     *  创建时间,所属表字段为ls_music_info.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为ls_music_info.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为ls_music_info.remark
     */
    private String remark;

    /**
     *  0 正常
1 删除,所属表字段为ls_music_info.rstatus
     */
    private Byte rstatus;

    public Long getMusicId() {
        return musicId;
    }

    public void setMusicId(Long musicId) {
        this.musicId = musicId;
    }

    public String getMusicName() {
        return musicName;
    }

    public void setMusicName(String musicName) {
        this.musicName = musicName == null ? null : musicName.trim();
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName == null ? null : singerName.trim();
    }

    public Long getSingerId() {
        return singerId;
    }

    public void setSingerId(Long singerId) {
        this.singerId = singerId;
    }

    public Integer getMusicPrice() {
        return musicPrice;
    }

    public void setMusicPrice(Integer musicPrice) {
        this.musicPrice = musicPrice;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}