package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserMembers;
import java.util.List;

public interface UserMembersMapper {
    int insert(UserMembers userMembers);

    int deleteByPK(Long memberId);

    int updateByPK(UserMembers userMembers);

    UserMembers selectByPK(Long memberId);

    long count(UserMembers userMembers);

    List<UserMembers> select(UserMembers userMembers);
}