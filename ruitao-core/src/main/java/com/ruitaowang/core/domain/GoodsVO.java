package com.ruitaowang.core.domain;

import java.util.List;

public class GoodsVO extends Goods {


    public long getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(long collectionId) {
        this.collectionId = collectionId;
    }

    private long collectionId;

    public List<Coupon> getCouponList() {
        return couponList;
    }

    public void setCouponList(List<Coupon> couponList) {
        this.couponList = couponList;
    }

    private List<Coupon> couponList;

    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    private Integer goodsNumber;

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    private Long cartId;

    /**
     * 排序类型（1、销量。2、最新。）
     */
    private byte sortType;

    //省
    private String province;

    //市
    private String city;

    //区
    private String district;

    //销量
    private int xiaoliang;

    public int getXiaoliang() {
        return xiaoliang;
    }

    public void setXiaoliang(int xiaoliang) {
        this.xiaoliang = xiaoliang;
    }

    //收藏数量
    private Long collectCount;

    /**
     * 自定义分类
     */
    private String customType;

    private Long companyId;

    private Long goodsRecommendId;

    private String cateName;

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public Long getGoodsRecommendId() {
        return goodsRecommendId;
    }

    public void setGoodsRecommendId(Long goodsRecommendId) {
        this.goodsRecommendId = goodsRecommendId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCustomType() {
        return customType;
    }

    public void setCustomType(String customType) {
        this.customType = customType;
    }

    public Long getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(Long collectCount) {
        this.collectCount = collectCount;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public byte getSortType() {
        return sortType;
    }

    public void setSortType(byte sortType) {
        this.sortType = sortType;
    }
}
