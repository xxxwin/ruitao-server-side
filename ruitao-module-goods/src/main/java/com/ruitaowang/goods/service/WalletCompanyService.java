package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Wallet;
import com.ruitaowang.core.domain.WalletCompany;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.WalletCompanyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class WalletCompanyService implements BaseService<WalletCompany> {
    @Autowired
    private WalletCompanyMapper walletCompanyMapper;

    @Override
    public WalletCompany insert(WalletCompany walletCompany) {
        Long time = System.currentTimeMillis();
        walletCompany.setCtime(time);
        walletCompany.setMtime(time);
        walletCompanyMapper.insert(walletCompany);
        return walletCompany;
    }

    @Override
    public int delete(Long id) {
        WalletCompany walletCompany = new WalletCompany();
        walletCompany.setCompanyId(id);
        walletCompany.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        walletCompany.setMtime(time);
        return walletCompanyMapper.updateByPK(walletCompany);
    }

    @Override
    public WalletCompany update(WalletCompany walletCompany) {
        Long time = System.currentTimeMillis();
        walletCompany.setMtime(time);
        walletCompanyMapper.updateByPK(walletCompany);
        return walletCompany;
    }

    @Override
    public WalletCompany selectByPK(Long id) {
        return walletCompanyMapper.selectByPK(id);
    }

    @Override
    public List<WalletCompany> select(WalletCompany walletCompany) {
        walletCompany.setRstatus((byte) 0);
        return walletCompanyMapper.select(walletCompany);
    }

    public List<WalletCompany> selectForPage(WalletCompany walletCompany) {
        walletCompany.setRstatus((byte) 0);
        walletCompany.setRecords(walletCompanyMapper.count(walletCompany));
        return walletCompanyMapper.select(walletCompany);
    }

    public void createUserProfit(Long companyId, Integer profit){
        WalletCompany walletCompany = this.selectByPK(companyId);
        if(ObjectUtils.isEmpty(walletCompany)){
            walletCompany = new WalletCompany();
            walletCompany.setCompanyId(companyId);
            walletCompany.setCompanyAmount(profit);
            walletCompany.setCompanyBalance(profit);
            this.insert(walletCompany);
        }else{
            walletCompany.setCompanyAmount(walletCompany.getCompanyAmount() + profit);
            walletCompany.setCompanyBalance(walletCompany.getCompanyBalance() + profit);
            this.update(walletCompany);
        }
    }
}
