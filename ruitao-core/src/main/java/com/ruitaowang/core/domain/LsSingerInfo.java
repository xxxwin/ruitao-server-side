package com.ruitaowang.core.domain;

public class LsSingerInfo extends Page {
    /**
     *  自增编码,所属表字段为ls_singer_info.singer_id
     */
    private Long singerId;

    /**
     *  歌手名,所属表字段为ls_singer_info.singer_name
     */
    private String singerName;

    /**
     *  歌手缩略图,所属表字段为ls_singer_info.singer_thum
     */
    private String singerThum;

    /**
     *  公司编码,所属表字段为ls_singer_info.company_id
     */
    private Long companyId;

    /**
     *  创建时间,所属表字段为ls_singer_info.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为ls_singer_info.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为ls_singer_info.remark
     */
    private String remark;

    /**
     *  0 正常
1 删除,所属表字段为ls_singer_info.rstatus
     */
    private Byte rstatus;

    public Long getSingerId() {
        return singerId;
    }

    public void setSingerId(Long singerId) {
        this.singerId = singerId;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName == null ? null : singerName.trim();
    }

    public String getSingerThum() {
        return singerThum;
    }

    public void setSingerThum(String singerThum) {
        this.singerThum = singerThum == null ? null : singerThum.trim();
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}