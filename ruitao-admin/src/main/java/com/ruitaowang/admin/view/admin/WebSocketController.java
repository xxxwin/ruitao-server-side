package com.ruitaowang.admin.view.admin;

import com.alibaba.fastjson.JSON;
import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.LsMsg;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.LsMsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RestController;

import javax.rmi.CORBA.Util;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * 实时推送
 */
@RestController
public class WebSocketController extends BaseController {

    @Autowired
    private SysUserService userService;

    @Autowired
    private LsMsgService lsMsgService;

    @MessageMapping("/topic/hi/{companyId}")
    @SendTo("/topic/hi/{companyId}")
    public String handle(@DestinationVariable Long companyId, String greeting, Principal principal) {
        return TimeUtils.getCurrentDatetime() + " : " + greeting;
    }

}
