package com.ruitaowang.core.domain;

public class Logistics extends Page {
    /**
     *  自增主键,所属表字段为logistics.logistics_id
     */
    private Long logisticsId;

    /**
     *  name,所属表字段为logistics.name
     */
    private String name;

    /**
     *  备注,所属表字段为logistics.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为logistics.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为logistics.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为logistics.rstatus
     */
    private Byte rstatus;

    public Long getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(Long logisticsId) {
        this.logisticsId = logisticsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}