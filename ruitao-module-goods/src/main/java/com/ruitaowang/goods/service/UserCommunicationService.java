package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserCommunication;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserCommunicationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by xinchunting on 17-10-30.
 */
@Service
public class UserCommunicationService implements BaseService<UserCommunication> {
    @Autowired
    private UserCommunicationMapper userCommunicationMapper;


    @Override
    public UserCommunication insert(UserCommunication userCommunication) {
        Long time = System.currentTimeMillis();
        userCommunication.setCtime(time);
        userCommunication.setMtime(time);
        userCommunicationMapper.insert(userCommunication);
        return userCommunication;
    }

    @Override
    public int delete(Long id) {
        UserCommunication userCommunication = new UserCommunication();
        userCommunication.setId(id);
        userCommunication.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userCommunication.setMtime(time);
        return userCommunicationMapper.updateByPK(userCommunication);
    }

    @Override
    public UserCommunication update(UserCommunication userCommunication) {
        Long time = System.currentTimeMillis();
        userCommunication.setMtime(time);
        userCommunicationMapper.updateByPK(userCommunication);
        return userCommunication;
    }

    @Override
    public UserCommunication selectByPK(Long id) {

        return userCommunicationMapper.selectByPK(id);
    }

    @Override
    public List<UserCommunication> select(UserCommunication userCommunication) {
        userCommunication.setRstatus((byte) 0);
        return userCommunicationMapper.select(userCommunication);
    }

    public List<UserCommunication> selectForPage(UserCommunication userCommunication) {
        userCommunication.setRstatus((byte) 0);
        userCommunication.setRecords(userCommunicationMapper.count(userCommunication));
        return userCommunicationMapper.select(userCommunication);
    }
}
