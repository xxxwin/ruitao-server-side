package com.ruitaowang.core.domain;

/**
 * Created by Administrator on 2017/5/6.
 */
public class OrderProdVO extends OrderProd {
    private String strcompanyname;
    private String strlinkman;
    private String strmobile;
    private String goodsScreenPriceVO;
    private String goodsRealPriceVO;
    private String strnickname;
    private Byte best;
    private Byte newest;
    private Byte discount;

    public String getGoodsScreenPriceVO() {
        return goodsScreenPriceVO;
    }

    public void setGoodsScreenPriceVO(String goodsScreenPriceVO) {
        this.goodsScreenPriceVO = goodsScreenPriceVO;
    }

    public String getGoodsRealPriceVO() {
        return goodsRealPriceVO;
    }

    public void setGoodsRealPriceVO(String goodsRealPriceVO) {
        this.goodsRealPriceVO = goodsRealPriceVO;
    }
    public Byte getBest() {
        return best;
    }

    public void setBest(Byte best) {
        this.best = best;
    }

    public Byte getNewest() {
        return newest;
    }

    public void setNewest(Byte newest) {
        this.newest = newest;
    }

    public Byte getDiscount() {
        return discount;
    }

    public void setDiscount(Byte discount) {
        this.discount = discount;
    }
    public String getStrnickname() {
        return strnickname;
    }

    public void setStrnickname(String strnickname) {
        this.strnickname = strnickname;
    }

    public void setStrcompanyname(String strcompanyname) {
        this.strcompanyname = strcompanyname;
    }

    public void setStrlinkman(String strlinkman) {
        this.strlinkman = strlinkman;
    }

    public void setStrmobile(String strmobile) {
        this.strmobile = strmobile;
    }

    public String getStrcompanyname() {
        return strcompanyname;
    }

    public String getStrlinkman() {
        return strlinkman;
    }

    public String getStrmobile() {
        return strmobile;
    }
}
