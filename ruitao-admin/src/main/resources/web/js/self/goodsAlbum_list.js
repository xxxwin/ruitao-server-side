var Config = {
     selectUrl: '/api/goodsAlbums',
     deleteUrl: '/api/admin/coupon'
    // updateUrlForCellEdit: '/api/admin/dc/dish/cellEdit',
};
var layerIndex;
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["相册编号","商品编号","缩略图","图片链接地址", "上传时间","操作"],
        colModel: [{
            name: "imageId",
            index: "imageId",
            search: true
        }, {
            name: "goodsId",
            index: "goodsId"
        }, {
            name: "imgSh",
            index: "imgSh"
        },{
            name: "url",
            index: "url"
        },{
            name:"ctime",
            index: "ctime"
        },{
            name: "operate",
            index: "operate",
            sortable: false
        }],
        // cellEdit: true,
        // cellsubmit: 'remote',
        // cellurl: Config.updateUrlForCellEdit,
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_coupon_list', {
        add: false, edit: false, del: false, search: false,
        refresh: false
    });
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
    
	$('#layerClose').click(function () {//#ajaxform 
		layer.close(layerIndex);    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#data_list").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}

function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var avatar = '<img style="width: 50px; height: 50px;" src="'+dataFromTheRow.url+'"/>';
        var html = "";
         html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="javascript:edit(' + id + ')" data-index="0">查看</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime,
            imgSh:avatar
        });
    }
}

function edit(id){
	openLayer("查看图片");
    var row = jQuery('#data_list').jqGrid('getRowData', id);
	$("#url").val(row.url);
	$("#imageId").val(row.imageId);
	$("#goodsId").val(row.goodsId);
	$("#imageType").val(row.imageType);
	$("#imgShow").attr("src",row.url);
	$("#original").val(row.original);
}

function openLayer(title,form){
	var	layer_width = '700px';
	var	layer_heigth = '500px';
	var	form = $('#ajaxform')
	
	layerIndex = layer.open({  
        type: 1,  
        title: [title, 'background-color: #00bb9d;text-align:center;font-size:18px;'],  
        shadeClose: true,  
        shade: false,  
        area: [layer_width, layer_heigth],
        content: form
    });
}

