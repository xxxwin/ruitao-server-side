package com.ruitaowang.core.domain;

public class CompanyIndustry extends Page {
    /**
     *  ,所属表字段为company_industry.id
     */
    private Long id;

    /**
     *  ,所属表字段为company_industry.name
     */
    private String name;

    /**
     *  ,所属表字段为company_industry.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为company_industry.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为company_industry.mtime
     */
    private Long mtime;

    /**
     *  ,所属表字段为company_industry.remark
     */
    private String remark;

    /**
     *  ,所属表字段为company_industry.company_commission
     */
    private String companyCommission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getCompanyCommission() {
        return companyCommission;
    }

    public void setCompanyCommission(String companyCommission) {
        this.companyCommission = companyCommission;
    }
}