package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.GoodsTypeCustom;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.GoodsTypeCustomMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsTypeCustomService implements BaseService<GoodsTypeCustom> {

    @Autowired
    private GoodsTypeCustomMapper goodsTypeCustomMapper;

    @Override
    public GoodsTypeCustom insert(GoodsTypeCustom goodsTypeCustom) {
        Long time = System.currentTimeMillis();
        goodsTypeCustom.setCtime(time);
        goodsTypeCustom.setMtime(time);
        goodsTypeCustomMapper.insert(goodsTypeCustom);
        return goodsTypeCustom;
    }

    @Override
    public int delete(Long id) {
        GoodsTypeCustom goodsTypeCustom = new GoodsTypeCustom();
        goodsTypeCustom.setId(id);
        goodsTypeCustom.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        goodsTypeCustom.setMtime(time);
        return goodsTypeCustomMapper.updateByPK(goodsTypeCustom);
    }

    @Override
    public GoodsTypeCustom update(GoodsTypeCustom goodsTypeCustom) {
        Long time = System.currentTimeMillis();
        goodsTypeCustom.setMtime(time);
        goodsTypeCustomMapper.updateByPK(goodsTypeCustom);
        return goodsTypeCustom;
    }

    @Override
    public GoodsTypeCustom selectByPK(Long id) {

        return goodsTypeCustomMapper.selectByPK(id);
    }

    @Override
    public List<GoodsTypeCustom> select(GoodsTypeCustom goodsTypeCustom) {
        goodsTypeCustom.setRstatus((byte) 0);
        return goodsTypeCustomMapper.select(goodsTypeCustom);
    }

    public List<GoodsTypeCustom> selectForPage(GoodsTypeCustom goodsTypeCustom) {
        goodsTypeCustom.setRstatus((byte) 0);
        goodsTypeCustom.setRecords(goodsTypeCustomMapper.count(goodsTypeCustom));
        return goodsTypeCustomMapper.select(goodsTypeCustom);
    }
}
