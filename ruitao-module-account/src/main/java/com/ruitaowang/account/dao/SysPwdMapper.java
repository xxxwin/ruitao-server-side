package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.SysPwd;
import java.util.List;

public interface SysPwdMapper {
    int insert(SysPwd sysPwd);

    int deleteByPK(Long userId);

    int updateByPK(SysPwd sysPwd);

    SysPwd selectByPK(Long userId);

    long count(SysPwd sysPwd);

    List<SysPwd> select(SysPwd sysPwd);
}