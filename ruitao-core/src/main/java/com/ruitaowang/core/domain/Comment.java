package com.ruitaowang.core.domain;

public class Comment extends Page {
    /**
     *  自增,所属表字段为comment.id
     */
    private Long id;

    /**
     *  评论内容,所属表字段为comment.comment_content
     */
    private String commentContent;

    /**
     *  评论类型,所属表字段为comment.comment_type
     */
    private Byte commentType;

    /**
     *  图片json,所属表字段为comment.img_content
     */
    private String imgContent;

    /**
     *  商家ID,所属表字段为comment.company_id
     */
    private Long companyId;

    /**
     *  商品ID,所属表字段为comment.goods_id
     */
    private Long goodsId;

    /**
     *  备注,所属表字段为comment.remark
     */
    private String remark;

    /**
     *  ,所属表字段为comment.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为comment.mtime
     */
    private Long mtime;

    /**
     *  ,所属表字段为comment.rstatus
     */
    private Byte rstatus;

    /**
     *  用户ID,所属表字段为comment.user_id
     */
    private Long userId;

    /**
     * 对评论进行评论,所属表字段为comment.parent_id
     */
    private Long parentId;

    /**
     * 点赞数量,所属表字段为comment.thumbs_up
     */
    private Long thumbsUp;

    /**
     * 是否匿名,所属表字段为comment.anonymous
     */
    private Byte anonymous;

    public Byte getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Byte anonymous) {
        this.anonymous = anonymous;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getThumbsUp() {
        return thumbsUp;
    }

    public void setThumbsUp(Long thumbsUp) {
        this.thumbsUp = thumbsUp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent == null ? null : commentContent.trim();
    }

    public Byte getCommentType() {
        return commentType;
    }

    public void setCommentType(Byte commentType) {
        this.commentType = commentType;
    }

    public String getImgContent() {
        return imgContent;
    }

    public void setImgContent(String imgContent) {
        this.imgContent = imgContent == null ? null : imgContent.trim();
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}