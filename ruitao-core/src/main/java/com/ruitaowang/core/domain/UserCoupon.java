package com.ruitaowang.core.domain;

public class UserCoupon extends Page {
    /**
     *  ,所属表字段为user_coupon.id
     */
    private Long id;

    /**
     *  商户ID,所属表字段为user_coupon.company_id
     */
    private Long companyId;

    /**
     *  用户ID,所属表字段为user_coupon.user_id
     */
    private Long userId;

    /**
     *  优惠券时间
1天 3天 7天 15天 30天 60天 90天,所属表字段为user_coupon.coupon_time
     */
    private Long couponTime;

    /**
     *  优惠券结束时间,所属表字段为user_coupon.end_time
     */
    private String endTime;

    /**
     *  折扣比例,所属表字段为user_coupon.percentage
     */
    private Integer percentage;

    /**
     *  过期状态
0 正常
1 过期,所属表字段为user_coupon.overdue_rstatus
     */
    private Byte overdueRstatus;

    /**
     *  状态 
0 正常
1 删除,所属表字段为user_coupon.rstatus
     */
    private Byte rstatus;

    /**
     *  创建时间,所属表字段为user_coupon.ctime
     */
    private Long ctime;

    /**
     *  修改时间,所属表字段为user_coupon.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为user_coupon.remark
     */
    private String remark;

    /**
     *  ,所属表字段为user_coupon.reach1
     */
    private Integer reach1;

    /**
     *  ,所属表字段为user_coupon.score1
     */
    private Integer score1;

    /**
     *  ,所属表字段为user_coupon.coupon_type
     */
    private Byte couponType;

    /**
     *  ,所属表字段为user_coupon.goods_id
     */
    private Long goodsId;

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    /**
     *  ,所属表字段为user_coupon.coupon_id
     */
    private Long couponId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCouponTime() {
        return couponTime;
    }

    public void setCouponTime(Long couponTime) {
        this.couponTime = couponTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime == null ? null : endTime.trim();
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Byte getOverdueRstatus() {
        return overdueRstatus;
    }

    public void setOverdueRstatus(Byte overdueRstatus) {
        this.overdueRstatus = overdueRstatus;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getReach1() {
        return reach1;
    }

    public void setReach1(Integer reach1) {
        this.reach1 = reach1;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Byte getCouponType() {
        return couponType;
    }

    public void setCouponType(Byte couponType) {
        this.couponType = couponType;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }
}