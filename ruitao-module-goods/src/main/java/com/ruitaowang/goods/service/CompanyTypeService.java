package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.CompanyIndustry;
import com.ruitaowang.core.domain.CompanyType;
import com.ruitaowang.goods.dao.CompanyIndustryMapper;
import com.ruitaowang.goods.dao.CompanyTypeAdMapper;
import com.ruitaowang.goods.dao.CompanyTypeMapper;
import com.ruitaowang.goods.utils.RStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 商家类型Service
 */
@Service
public class CompanyTypeService {
    @Autowired
    private CompanyTypeAdMapper companyTypeAdMapper;
    @Autowired
    private CompanyTypeMapper companyTypeMapper;
    @Autowired
    private CompanyIndustryMapper companyIndustryMapper;

    public List<CompanyType> select(CompanyType companyType) {
        companyType.setRstatus(RStatus.Exist);
        return companyTypeAdMapper.select(companyType);
    }
    public List<CompanyType> selectForPage(CompanyType companyType) {
        companyType.setRstatus(RStatus.Exist);
        companyType.setRecords(companyTypeAdMapper.count(companyType));
        return companyTypeAdMapper.select(companyType);
    }
    public CompanyType selectByPK(Long id) {
        return companyTypeAdMapper.selectByPK(id);
    }

    public CompanyType insert(CompanyType companyType) {
        Long time = System.currentTimeMillis();
        companyType.setCtime(time);
        companyType.setMtime(time);
        companyTypeAdMapper.insert(companyType);
        return companyType;
    }

    public CompanyType update(CompanyType companyType) {
        Long time = System.currentTimeMillis();
        companyType.setMtime(time);
        companyTypeAdMapper.updateByPK(companyType);
        return companyTypeAdMapper.selectByPK(companyType.getId());
    }

    public int delete(Long id) {
        CompanyType companyType = new CompanyType();
        companyType.setMtime(System.currentTimeMillis());
        companyType.setRstatus(RStatus.Deleted);
        companyType.setId(id);
        return companyTypeAdMapper.updateByPK(companyType);
    }
    public String industryCategoryi(Long id){
         CompanyType companyType=companyTypeMapper.selectByPK(id);
         CompanyIndustry companyIndustry=companyIndustryMapper.selectByPK(companyType.getIndustryId());
        return companyIndustry.getName()+"/"+companyType.getName()+"【"+companyIndustry.getCompanyCommission()+"%】";
    }
}
