package com.ruitaowang.admin.view.admin;

import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Base64Img;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyProduct;
import com.ruitaowang.core.utils.QRCode;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.dao.Base64ImgMapper;
import com.ruitaowang.goods.service.CompanyProductService;
import com.ruitaowang.goods.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;

/**
 *
 * 商品信息
 */
@RestController
public class PageCompanyController extends BaseController {

    @Autowired
    private CompanyService companyService;
    @Autowired
    private Base64ImgMapper base64ImgMapper;
    @Autowired
    private CompanyProductService companyProductService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * 商家更新
     */
    @RequestMapping(value = {"/company/update/{companyId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(@PathVariable("companyId") Long companyId, Model model) {
        CompanyProduct companyProduct=companyProductService.selectFistDiscount(companyId);
        Company company = companyService.selectByPK(companyId);
        if(!ObjectUtils.isEmpty(company.getImgIdFront()) && !company.getImgIdFront().startsWith("http")){
            company.setImgIdFront(base64ImgMapper.selectByPK(Long.valueOf(company.getImgIdFront())).getBase64());
        }
        if(!ObjectUtils.isEmpty(company.getImgIdBack()) && !company.getImgIdBack().startsWith("http")){
            company.setImgIdBack(base64ImgMapper.selectByPK(Long.valueOf(company.getImgIdBack())).getBase64());
        }
        if(!ObjectUtils.isEmpty(company.getImgOrganizationCodeCertificate()) && !company.getImgOrganizationCodeCertificate().startsWith("http")){
            company.setImgOrganizationCodeCertificate(base64ImgMapper.selectByPK(Long.valueOf(company.getImgOrganizationCodeCertificate())).getBase64());
        }
        if(!ObjectUtils.isEmpty(company.getXlErCert()) && !company.getXlErCert().startsWith("http")){
            company.setXlErCert(base64ImgMapper.selectByPK(Long.valueOf(company.getXlErCert())).getBase64());
        }
        if(!ObjectUtils.isEmpty(company.getImgBusinessLicense()) && !company.getImgBusinessLicense().startsWith("http")){
            company.setImgBusinessLicense(base64ImgMapper.selectByPK(Long.valueOf(company.getImgBusinessLicense())).getBase64());
        }
        if(!ObjectUtils.isEmpty(company.getHeadimgurl()) && !company.getHeadimgurl().startsWith("http")){
            Base64Img base64Img =  base64ImgMapper.selectByPK(Long.valueOf(company.getHeadimgurl()));
            if(!ObjectUtils.isEmpty(base64Img)){
                company.setHeadimgurl(base64Img.getBase64());
            }

        }
        String companyQrCodeUrl;
        if (!ObjectUtils.isEmpty(companyProduct)){
            if (ObjectUtils.isEmpty(companyProduct.getQrcodeUrl())){
                if (ObjectUtils.isEmpty(sysUserService.selectByPK(company.getUserId()))){
                    companyProduct.setQrcodeUrl("无效用户Id 无法注册二维码");
                    companyQrCodeUrl = companyProduct.getQrcodeUrl();
                }else {
                    companyProduct.setQrcodeUrl(QRCode.rulesQRForSMPay(System.getProperty("catalina.base"), company.getUserId(), companyProduct.getProductId()));
                    companyProduct=companyProductService.update(companyProduct);
                    companyQrCodeUrl = companyProduct.getQrcodeUrl();
                }
            }else {
                companyQrCodeUrl = companyProduct.getQrcodeUrl();
            }
        }else {
            CompanyProduct companyProduct1=new CompanyProduct();
            companyProduct1.setCompanyId(company.getCompanyId());
            CompanyProduct companyProductNew = companyProductService.insert(companyProduct1);
            if (ObjectUtils.isEmpty(sysUserService.selectByPK(company.getUserId()))){
                companyProductNew.setQrcodeUrl("注册二维码无效 无效用户ID");
                companyQrCodeUrl = companyProductNew.getQrcodeUrl();
            }else {
                companyProductNew.setQrcodeUrl(QRCode.rulesQRForSMPay(System.getProperty("catalina.base"), company.getUserId(), companyProductNew.getProductId()));
                companyProductNew=companyProductService.update(companyProductNew);
                companyQrCodeUrl = companyProductNew.getQrcodeUrl();
            }
        }
        model.addAttribute("companyQrCodeUrl" , companyQrCodeUrl);
        model.addAttribute("companyProduct" , companyProduct);
        model.addAttribute("company", company);
        return new ModelAndView("web/company_update");
    }

}