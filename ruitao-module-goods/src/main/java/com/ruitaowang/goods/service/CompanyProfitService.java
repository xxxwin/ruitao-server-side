package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyProduct;
import com.ruitaowang.core.domain.CompanyProfit;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CompanyAdMapper;
import com.ruitaowang.goods.dao.CompanyMapper;
import com.ruitaowang.goods.dao.CompanyProfitAdMapper;
import com.ruitaowang.goods.dao.CompanyProfitMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by Shaka on 2017/1/4.
 */
@Service
public class CompanyProfitService implements BaseService<CompanyProfit> {
    @Autowired
    private CompanyProfitMapper companyProfitMapper;
    @Autowired
    private CompanyProfitAdMapper companyProfitAdMapper;

    @Override
    public CompanyProfit insert(CompanyProfit companyProfit) {
        Long time = System.currentTimeMillis();
        companyProfit.setCtime(time);
        companyProfit.setMtime(time);
        companyProfitMapper.insert(companyProfit);
        return companyProfit;
    }

    @Override
    public int delete(Long id) {
        CompanyProfit CompanyProfit = new CompanyProfit();
        CompanyProfit.setCompanyId(id);
        CompanyProfit.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        CompanyProfit.setMtime(time);
        return companyProfitMapper.updateByPK(CompanyProfit);
    }

    @Override
    public CompanyProfit update(CompanyProfit companyProfit) {
        Long time = System.currentTimeMillis();
        companyProfit.setMtime(time);
        companyProfitMapper.updateByPK(companyProfit);
        return companyProfitMapper.selectByPK(companyProfit.getCompanyId());
    }

    @Override
    public CompanyProfit selectByPK(Long id) {
        return companyProfitMapper.selectByPK(id);
    }

    @Override
    public List<CompanyProfit> select(CompanyProfit companyProfit) {
        companyProfit.setRstatus((byte) 0);
        return companyProfitMapper.select(companyProfit);
    }

    public List<CompanyProfit> selectForPage(CompanyProfit companyProfit) {
        companyProfit.setRstatus((byte) 0);
        companyProfit.setRecords(companyProfitMapper.count(companyProfit));
        return companyProfitMapper.select(companyProfit);
    }

    //昨日净收入
    public Long beforeMoney(CompanyProfit companyProfit){
        String beforeMoney = companyProfitAdMapper.selectMoney(companyProfit);
        if (!ObjectUtils.isEmpty(beforeMoney)) {
            return Long.valueOf(beforeMoney);
        } else {
            return (long)0;
        }
    }

}
