package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Activity;
import java.util.List;

public interface ActivityMapper {
    int insert(Activity activity);

    int deleteByPK(Long activityId);

    int updateByPK(Activity activity);

    Activity selectByPK(Long activityId);

    long count(Activity activity);

    List<Activity> select(Activity activity);

    int updateByPrimaryKeyWithBLOBs(Activity record);
}