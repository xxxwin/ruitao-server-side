#!/usr/bin/env bash

workDir=/home/ruitao/server/source/ruitao-server-side

sudo rm -rf $workDir/ruitao-admin/src/main/webapp/wap/vue-gxt/*

/usr/bin/git checkout master

/usr/bin/git pull origin master

cd $workDir/vue-gxt
npm run build
# /usr/bin/cp -r $workDir/vue-gxt/dist /server/source/ruitao-server-side/ruitao-admin/src/main/webapp/wap/vue-gxt
sudo /usr/bin/cp -r $workDir/vue-gxt/dist $workDir/ruitao-admin/src/main/webapp/wap/vue-gxt

#activeFile1=$workDir/ruitao-rest-mobile/src/main/resources/application.properties

activeFile2=$workDir/ruitao-admin/src/main/resources/application.properties
#
#
#echo 'Before ...'
#
#cat $activeFile1
#
#cat $activeFile2
#

#sed -i 's/dev/live/1' $activeFile1

sed -i 's/dev/live/1' $activeFile2

echo "Live ..."

#cat $activeFile1

cat $activeFile2

cd $workDir

/home/ruitao/mvn/bin/mvn clean install -DskipTests


echo 'After ...'

#sed -i 's/live/dev/1' $activeFile1

sed -i 's/live/dev/1' $activeFile2

#cat $activeFile1

cat $activeFile2

sudo /usr/bin/cp /home/ruitao/server/source/ruitao-server-side/ruitao-admin/target/ruitao-admin-1.0.0.war /home/ruitao/ROOT.war
scp /home/ruitao/server/source/ruitao-server-side/ruitao-admin/target/ruitao-admin-1.0.0.war ruitao@10.27.143.116:/home/ruitao/ROOT.war
