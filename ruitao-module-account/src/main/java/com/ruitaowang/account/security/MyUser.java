package com.ruitaowang.account.security;

import com.ruitaowang.core.domain.Role;
import com.ruitaowang.core.domain.SysUser;


import java.util.Collection;
import java.util.List;

/**
 * Created by neal on 23/08/2017.
 */
public class MyUser{
    private com.ruitaowang.core.domain.SysUser user;
    private List<Role> roles;

    public MyUser(String username, String password, Collection<?> authorities, com.ruitaowang.core.domain.SysUser user) {
        this.user = user;
    }

    public SysUser getUser() {
        return user;
    }

    public void setUser(SysUser user) {
        this.user = user;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
