/**
 * Created by neal on 12/16/16.
 */

var Config = {
    loginUrl: '/web/login'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
   $("#login").click(function () {
       if ($('#ajaxform').valid()) {
           $.ajax(
           {
               url : Config.loginUrl,
               type: "POST",
               data: $("#ajaxform").serializeArray(),
               dataType: "json",
               success:function(data, textStatus, jqXHR)
               {
                   if(data && data.type == 'SUCCESS'){
                       toastr.success(data.msg, data.title);
                       //switch
                       window.location.href="/web/index";
                   }else {
                       toastr.error(data.msg, data.title);
                   }
               },
               error: function(jqXHR, textStatus, errorThrown)
               {
                   console.log(errorThrown);
                   toastr.error(errorThrown, textStatus);
               }
           });
           return false;
       }
   });
});