package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CompanyProduct;
import java.util.List;

public interface CompanyProductMapper {
    int insert(CompanyProduct companyProduct);

    int deleteByPK(Long productId);

    int updateByPK(CompanyProduct companyProduct);

    CompanyProduct selectByPK(Long productId);

    long count(CompanyProduct companyProduct);

    List<CompanyProduct> select(CompanyProduct companyProduct);
}