/**
 * Created by Administrator on 2017/4/25.
 */
/**
 * Created by Administrator on 2017/4/25.
 */
/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by neal on 11/22/16.
 */
var Config = {
    createUrl: '/api/wallet',
    updateUrl: '/api/wallet',
    deleteUrl: '/api/wallet',
    selectUrl: "/api/admin/companyProfit/get"
};
$(document).ready(function () {
    defaultOrderList();
});

$(".datepicker").datepicker({
    language: "zh-CN",
    clearBtn: true,//清除按钮
    todayBtn: "linked",//今日按钮
    changeMonth: true,
    changeYear: true,
    autoclose: true,//选中之后自动隐藏日期选择框
    format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
});
function defaultOrderList() {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "订单ID", "订单SN", "订单类型", "商户ID","商家名称", "消费者名称", "总金额", "收益金额", "创建时间", "操作"],
        colModel: [{
            name: "profitId",
            index: "profitId",
            editable: false,
            sorttype: "int",
            search: true
        }, {
            name: "orderId",
            index: "orderId"
        }, {
            name: "orderSn",
            index: "orderSn"
        }, {
            name: "orderType",
            index: "orderType",
            // editoptions: {value: "0:商城供货商订单返利;2:商城微商订单返利;4:商城微商订单返红包;7:云转返利;8:云省返利;9:云签返利;10:云飞返利;11:云推返利;12:云点返利;13:云联返利；14:云促销返利；15:快赚大礼包;16:云动返利;17:互动店霸屏奖励;18:互动店打赏奖励;18:互动店打赏奖励;19:互动店抢红包积分;20:区级返利;21:市级返利;22:省级返利;-1:提现;-2:退款扣除奖励;-3:红包支出"},
            editoptions: {value: "0:商城供货商订单返利;1:线下店扫码订单返利;2:商城微商订单返利;3:地面店扫码订单返红包;4:商城微商订单返红包;5:点餐订单返红包;6:点餐订单返利;7:微传媒·年;8:云省返利;9:云签返利;10:云飞返利;11:云推返利;12:云点返利;13:云联返利；14:云促销返利；15:快赚大礼包;16:云动返利;23:微传媒·月;24:微传媒·季;25:微传媒·半年;26:高级合伙人权限;-1:提现;-2:退款扣除奖励;-3:红包支出"},
            editable: true,
            edittype: 'select',
            formatter: 'select'
        }, {
            name: "companyId",
            index: "companyId"
        }, {
            name: "companyName",
            index: "companyName"
        }, {
            name: "nickName",
            index: "nickName"
        }, {
            name: "amount",
            index: "amount"
        }, {
            name: "companyProfit",
            index: "companyProfit"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "profitId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
          dataWrapper();
        }
    });
}
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS"){
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    }else{
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/api/admin/companyProfit/excel/' + id + '" >订单导出</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {
            operate: html,
            //companyBalance: toFixedForPrice(dataFromTheRow.companyBalance),
            companyProfit: toFixedForPrice(dataFromTheRow.companyProfit),
            amount: toFixedForPrice(dataFromTheRow.amount),
            ctime: dataFromTheRow.ctime
        });
    }
}
// $('#buttonGoback').click(function () {
//     history.go(-1);
//     location.reload();
//     return false;
// });