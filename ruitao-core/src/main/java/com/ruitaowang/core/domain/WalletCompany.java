package com.ruitaowang.core.domain;

public class WalletCompany extends Page {
    /**
     *  主键,用户编号,所属表字段为wallet_company.company_id
     */
    private Long companyId;

    /**
     *  总额,所属表字段为wallet_company.company_amount
     */
    private Integer companyAmount;

    /**
     *  余额,所属表字段为wallet_company.company_balance
     */
    private Integer companyBalance;

    /**
     *  备注,所属表字段为wallet_company.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为wallet_company.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为wallet_company.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为wallet_company.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为wallet_company.company_yestoday
     */
    private Integer companyYestoday;

    /**
     *  公司类型,所属表字段为wallet_company.company_type
     */
    private Byte companyType;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Integer getCompanyAmount() {
        return companyAmount;
    }

    public void setCompanyAmount(Integer companyAmount) {
        this.companyAmount = companyAmount;
    }

    public Integer getCompanyBalance() {
        return companyBalance;
    }

    public void setCompanyBalance(Integer companyBalance) {
        this.companyBalance = companyBalance;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getCompanyYestoday() {
        return companyYestoday;
    }

    public void setCompanyYestoday(Integer companyYestoday) {
        this.companyYestoday = companyYestoday;
    }

    public Byte getCompanyType() {
        return companyType;
    }

    public void setCompanyType(Byte companyType) {
        this.companyType = companyType;
    }
}