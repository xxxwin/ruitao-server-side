var config = {
    createUrl: '/api/admin/type/insert',
    selectIndustryUrl:'/api/admin/industry/selectAll',
    // updateUrl: '/api/activities',
    // deleteUrl: '/api/activities',
    // selectUrl: '/api/activities'
};

//表单验证
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$().ready(function () {
    //给行业类型注入值
	
    $.ajax(
        {
            url: config.selectIndustryUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                data.data.map(function (industry) {
                    categories.push('<option value="');
                    categories.push(industry.id);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(industry.name);
                    categories.push('</option>');
                });
                $("#industrySelect").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    $('#submitTypeCreate').click(function () {
        $("#typeForm").validate();
        if ($('#typeForm').valid()) {
            var postData = $('#typeForm').serializeArray();
            $.ajax(
                {
                    url: config.createUrl,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
});