var Config = {
    selectUserCount: '/api/admin/user/count',
    selectCompanyCount: '/api/admin/company/count',
    selectCountUser:'/api/admin/stat/userAll',
    selectDayData:'/api/admin/day/dayData'
};

// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('users'));

var userCount;          //会员数量
var companyCount;       //店铺数量
$().ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd",//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
        onSelect:dateTime
    }).on('changeDate',dateTime);
// $("#dateTime").onselect(function (dateText, inst) {
//    console.log(dateText);
// });
    function dateTime() {
       var stime = dateToEpoch($("#dateTime").val())* 1 - 8 * 60 * 60 * 1000 + 1;
       var etime = dateToEpoch($("#dateTime").val())* 1 + 16 * 60 * 60 * 1000 - 1;
        $.ajax(
            {
                url: Config.selectDayData,
                type: "GET",
                data:{stime:stime,etime:etime},
                dataType: "json",
                async: false,
                success: function (data, textStatus, jqXHR) {
                    console.log(data.data);
                    $("#usersAll").val(data.data.dayAll+"人");
                    $("#usersDay").val(data.data.dataAll+"人");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
    }
    refreshCount();
    window.setInterval(refreshCount, 1000*3);
    function refreshCount() {
        $.ajax(
            {
                url: Config.selectCountUser,
                type: "GET",
                dataType: "json",
                async: false,
                success: function (data, textStatus, jqXHR) {
                    console.log(data.data);
                    $("#userTimeAll").val(data.data+"人");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
    }
    $.ajax(
    {
        url: Config.selectUserCount,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            userCount = data.data.userCount;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });

    $.ajax(
        {
            url: Config.selectCompanyCount,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                companyCount = data.data.companyCount;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });

    myData();
});
function myData() {
    var option = {
        tooltip : {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            data: ['会员数量','店铺数量']
        },
        series : [
            {
                name: '统计基数',
                type: 'pie',
                radius : '55%',
                center: ['50%', '60%'],
                data:[
                    {value:userCount, name:'会员数量'},
                    {value:companyCount, name:'店铺数量'},
                ],
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }
        ]
    };
    myChart.setOption(option);
}
