package com.ruitaowang.core.domain;

public class LocalDiscount extends Page {
    /**
     *  自增主键,所属表字段为local_discount.discount_id
     */
    private Long discountId;

    /**
     *  折扣,所属表字段为local_discount.discount
     */
    private Integer discount;

    /**
     *  送积分,所属表字段为local_discount.score
     */
    private Integer score;

    /**
     *  备注,所属表字段为local_discount.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为local_discount.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为local_discount.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为local_discount.rstatus
     */
    private Byte rstatus;

    /**
     *  单笔消费达到的金额才送积分,所属表字段为local_discount.limit_price
     */
    private Integer limitPrice;

    /**
     *  ,所属表字段为local_discount.title
     */
    private String title;

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getLimitPrice() {
        return limitPrice;
    }

    public void setLimitPrice(Integer limitPrice) {
        this.limitPrice = limitPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }
}