package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Order;
import com.ruitaowang.core.domain.OrderProd;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.OrderProdAdMapper;
import com.ruitaowang.goods.dao.OrderProdMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class OrderProdService implements BaseService<OrderProd>{

    @Autowired
    private OrderProdMapper orderProdMapper;
    @Autowired
    private OrderProdAdMapper orderProdAdMapper;

    @Override
    public List<OrderProd> select(OrderProd orderProd) {
        orderProd.setRstatus((byte) 0);
        return orderProdMapper.select(orderProd);
    }

    @Override
    public OrderProd insert(OrderProd orderProd) {
        Long time = System.currentTimeMillis();
        orderProd.setCtime(time);
        orderProd.setMtime(time);
        orderProdMapper.insert(orderProd);
        return orderProd;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public OrderProd update(OrderProd orderProd) {
        orderProdMapper.updateByPK(orderProd);
        return orderProd;
    }

    @Override
    public OrderProd selectByPK(Long id) {
        return orderProdMapper.selectByPK(id);
    }


    public List<OrderProd> selectForPage(OrderProd orderProd) {
        orderProd.setRstatus((byte) 0);
        orderProd.setRecords(orderProdMapper.count(orderProd));
        return orderProdMapper.select(orderProd);
    }
    public List<OrderProd> timeForPage(OrderProd orderProd) {
        orderProd.setRstatus((byte) 0);
        orderProd.setRecords(orderProdAdMapper.count(orderProd));
        return orderProdAdMapper.select(orderProd);
    }
}
