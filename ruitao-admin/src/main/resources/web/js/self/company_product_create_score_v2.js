/**
 * Created by Sahaka on 12/01/16.
 */

var Config = {
    createUrl: '/api/companyProduct',
};

var ConfigDiscount = {
    selectUrl: '/api/localDiscount'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
$().ready(function () {
    $('#percentagefan').hide();
    $('#reachfan').show();
    $('#submitCreate').click(function () {

        $("#ajaxform").validate();

        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            postData.reach1=postData.reach1 * 100;
            postData.score1=postData.reach1 * 100;
            postData.reach2=postData.reach1 * 100;
            postData.score2=postData.reach1 * 100;
            postData.reach3=postData.reach1 * 100;
            postData.score3=postData.reach1 * 100;
            postData.reach4=postData.reach1 * 100;
            postData.score4=postData.reach1 * 100;
            postData.reach5=postData.reach1 * 100;
            postData.score5=postData.reach1 * 100;
            $.ajax(
                {
                    url: Config.createUrl,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });

    $('select').change(function () {
        if($(this).val() == 0){
            $('#percentagefan').hide();
            $('#reachfan').show();
        }

        if($(this).val() == 1){
            $('#percentagefan').show();
            $('#reachfan').hide();
        }
    });


});
