package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LsMsg;
import java.util.List;

public interface LsMsgMapper {
    int insert(LsMsg lsMsg);

    int deleteByPK(Long msgId);

    int updateByPK(LsMsg lsMsg);

    LsMsg selectByPK(Long msgId);

    long count(LsMsg lsMsg);

    List<LsMsg> select(LsMsg lsMsg);
}