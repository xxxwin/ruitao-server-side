package com.ruitaowang.core.utils.crypt;

import com.ruitaowang.core.utils.MD5Util;

/**
 * Created by neal on 11/22/16.
 */
public class EncodePassword {
    public static String encodePasswd(String origin) {
        return MD5Util.encryptMD5(origin);
    }
}
