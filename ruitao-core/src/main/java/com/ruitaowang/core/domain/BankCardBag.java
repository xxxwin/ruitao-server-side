package com.ruitaowang.core.domain;

public class BankCardBag extends Page {
    /**
     *  ,所属表字段为bank_card_bag.id
     */
    private Long id;

    /**
     *  银行卡账号,所属表字段为bank_card_bag.bank_id
     */
    private String bankId;

    /**
     *  开户行名称,所属表字段为bank_card_bag.bank_name
     */
    private String bankName;

    /**
     *  银行预留手机号,所属表字段为bank_card_bag.bank_user_phone
     */
    private String bankUserPhone;

    /**
     *  开户人身份证,所属表字段为bank_card_bag.bank_user_idcard
     */
    private String bankUserIdcard;

    /**
     *  开户人姓名,所属表字段为bank_card_bag.bank_user_name
     */
    private String bankUserName;

    /**
     *  归属user,所属表字段为bank_card_bag.user_id
     */
    private Long userId;

    /**
     *  备注,所属表字段为bank_card_bag.remark
     */
    private String remark;

    /**
     *  状态

0:正常
1:删除,所属表字段为bank_card_bag.rstatus
     */
    private Byte rstatus;

    /**
     *  创建时间,所属表字段为bank_card_bag.ctime
     */
    private Long ctime;

    /**
     *  修改时间,所属表字段为bank_card_bag.mtime
     */
    private Long mtime;

    /**
     *  开户行缩写简称,所属表字段为bank_card_bag.bank_abbreviation
     */
    private String bankAbbreviation;

    /**
     * 开户行支行,所属表字段为bank_card_bag.bank_branch
     */
    private String bankBranch;

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankAbbreviation() {
        return bankAbbreviation;
    }

    public void setBankAbbreviation(String bankAbbreviation) {
        this.bankAbbreviation = bankAbbreviation;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId == null ? null : bankId.trim();
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? null : bankName.trim();
    }

    public String getBankUserPhone() {
        return bankUserPhone;
    }

    public void setBankUserPhone(String bankUserPhone) {
        this.bankUserPhone = bankUserPhone == null ? null : bankUserPhone.trim();
    }

    public String getBankUserIdcard() {
        return bankUserIdcard;
    }

    public void setBankUserIdcard(String bankUserIdcard) {
        this.bankUserIdcard = bankUserIdcard == null ? null : bankUserIdcard.trim();
    }

    public String getBankUserName() {
        return bankUserName;
    }

    public void setBankUserName(String bankUserName) {
        this.bankUserName = bankUserName == null ? null : bankUserName.trim();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }
}