package com.ruitaowang.goods.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;

import com.ruitaowang.core.domain.Category;
import org.apache.ibatis.annotations.Select;

public interface CategoryMapper {
	
	public static String SELECTBYPK = "select * from category where id=#{id}";
	 
    Category selectByPK(Long id);

    List<Category> select(Category category); 
    
    List<Category> selectCatesByCompany(@Param("companyId")Long companyId,@Param("onLine")Integer onLine);
    
    long count(Category category);
	
	int insert(Category category);

	@Delete("delete from category where id = #{id}")
    int deleteByPK(Long id);

    int updateByPK(Category category);

    @Select("SELECT GROUP_CONCAT(T2.name) as name FROM (SELECT@r AS _id,(SELECT @r := parent_id FROM category WHERE id = _id) AS parent_id,@l := @l + 1 AS lvl FROM (SELECT @r := #{id}, @l := 0) vars,category h WHERE @r > 0 AND parent_id > 0) T1 JOIN category T2 ON T1._id = T2.id ORDER BY T1.lvl desc")
    String selectCategoryName(@Param("id")Long id);



}


