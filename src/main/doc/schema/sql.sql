-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema rtshop
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema rtshop
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rtshop` DEFAULT CHARACTER SET utf8 ;
USE `rtshop` ;

-- -----------------------------------------------------
-- Table `rtshop`.`activity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`activity` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`activity` (
  `activity_id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `activity_title` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '活动标题',
  `activity_url` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '活动链接',
  `activity_content` LONGTEXT NOT NULL COMMENT '互动内容',
  `activity_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '活动类型\n0 轮播图\n1 二级banner',
  `ctime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `goods_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '商品编码',
  `activity_sort` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `activity_image_url` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '广告图片',
  `activity_author` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '文章发布者',
  PRIMARY KEY (`activity_id`))
  ENGINE = InnoDB
  AUTO_INCREMENT = 124
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`t_sys_user` ;

CREATE TABLE `t_sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '账号',
  `password` varchar(512) NOT NULL DEFAULT '' COMMENT '密码',
  `department` varchar(45) NOT NULL DEFAULT '' COMMENT '部门',
  `phone` varchar(45) NOT NULL DEFAULT '' COMMENT '手机号',
  `position` varchar(45) NOT NULL DEFAULT '' COMMENT '职位',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `real_name` varchar(45) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `last_ip` varchar(45) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `last_time` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: 公开       个人隐私信息 \n0 :不公开   个人隐私信息',
  `user_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0： 普通会员 （消费者 两类1 网站消费，2 实体店扫码消费，没有绑定手机号）\n1：本地生活 商家用户\n2：城市合伙人\n4：区代理 ',
  `headimgurl` varchar(500) NOT NULL DEFAULT '',
  `qrimgurl` varchar(255) NOT NULL DEFAULT '' COMMENT '二维码',
  `nickname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '昵称',
  `id_card` varchar(100) NOT NULL DEFAULT '' COMMENT '省份证号',
  `wechat_id` varchar(50) NOT NULL DEFAULT '' COMMENT '微信号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=65794 DEFAULT CHARSET=utf8 COMMENT='系统用户表'


-- -----------------------------------------------------
-- Table `rtshop`.`address`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`address` ;

CREATE TABLE `address` (
  `address_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `consignee` varchar(255) NOT NULL DEFAULT '' COMMENT '收货人姓名',
  `mobile` varchar(15) NOT NULL DEFAULT '' COMMENT '联系方式',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `city` varchar(45) NOT NULL DEFAULT '',
  `province` varchar(45) NOT NULL DEFAULT '',
  `district` varchar(45) NOT NULL DEFAULT '',
  `other` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `country` varchar(45) NOT NULL DEFAULT 'cn' COMMENT '国家',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编号',
  `district_id` bigint(20) NOT NULL DEFAULT '0',
  `defaultt` tinyint(4) NOT NULL DEFAULT '0' COMMENT '默认',
  PRIMARY KEY (`address_id`),
  KEY `fk_address_user_id_idx` (`user_id`),
  CONSTRAINT `fk_address_user_id` FOREIGN KEY (`user_id`) REFERENCES `t_sys_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=297 DEFAULT CHARSET=utf8 COMMENT='地址列表'


-- -----------------------------------------------------
-- Table `rtshop`.`after_market`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`after_market` ;

CREATE TABLE `after_market` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '订单编号',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `prod_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '订单产品编码',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编号',
  `return_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '售后类型： 0 退货退款， 1  退款， 2 换货',
  `return_cause` varchar(500) NOT NULL DEFAULT '',
  `image_id1` bigint(20) NOT NULL DEFAULT '0',
  `image_id2` bigint(20) NOT NULL DEFAULT '0',
  `image_id3` bigint(20) NOT NULL DEFAULT '0',
  `logistic_id` bigint(20) NOT NULL DEFAULT '0',
  `logistic_name` varchar(45) NOT NULL DEFAULT '',
  `logistic_sn` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='售后'


-- -----------------------------------------------------
-- Table `rtshop`.`base64_img`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`base64_img` ;

CREATE TABLE `base64_img` (
  `image_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `base64` longtext NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8 COMMENT='base64图片'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_category` ;

CREATE TABLE `goods_category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品分类编号',
  `category_name` varchar(100) NOT NULL DEFAULT '' COMMENT '商品分类名称',
  `parent_category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品上级分类ID',
  `category_sort` int(5) NOT NULL DEFAULT '0' COMMENT '分类排序',
  `category_hidden` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1， 隐藏不显示\n0， 前端显示',
  `category_unit` varchar(10) NOT NULL DEFAULT '' COMMENT '分类重量单位',
  `nav_show` tinyint(4) NOT NULL DEFAULT '0' COMMENT '显示在导航栏\n1 是\n0 否',
  `category_amount` int(11) NOT NULL DEFAULT '0' COMMENT '商品数量',
  `category_small_icon` varchar(45) NOT NULL DEFAULT '',
  `category_desc` varchar(200) NOT NULL DEFAULT '',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='商品分类信息'


-- -----------------------------------------------------
-- Table `rtshop`.`category_recommend`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`category_recommend` ;

CREATE TABLE `category_recommend` (
  `recommend_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '推荐编号',
  `category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品分类编号',
  `recommend_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1，精品\n2，最新\n3，最热',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  PRIMARY KEY (`recommend_id`),
  UNIQUE KEY `unq_category_recommend_type` (`category_id`,`recommend_type`),
  KEY `fk_category_recommend_category_id_idx` (`category_id`),
  CONSTRAINT `fk_category_recommend_category_id` FOREIGN KEY (`category_id`) REFERENCES `goods_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='商品分类信息'


-- -----------------------------------------------------
-- Table `rtshop`.`company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`company` ;

CREATE TABLE `company` (
  `company_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `company_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'name',
  `linkman` varchar(10) NOT NULL DEFAULT '' COMMENT '负责人',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `mobile` varchar(45) NOT NULL DEFAULT '' COMMENT '联系电话',
  `business_license_number` varchar(50) NOT NULL DEFAULT '' COMMENT '许可证',
  `address` varchar(255) NOT NULL DEFAULT '' COMMENT '详细地址',
  `img_id_front` varchar(255) NOT NULL DEFAULT '' COMMENT '身份证正面',
  `img_id_back` varchar(255) NOT NULL DEFAULT '' COMMENT '身份证反面',
  `img_tax_registration_certificate` varchar(255) NOT NULL DEFAULT '',
  `img_business_license` varchar(255) NOT NULL DEFAULT '',
  `img_organization_code_certificate` varchar(255) NOT NULL DEFAULT '' COMMENT '组织机构代码证',
  `headimgurl` varchar(255) NOT NULL DEFAULT '' COMMENT '公司logo',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编号',
  `company_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '商家类型\n0 实体店\n1 微商\n2 心理咨询\n3 城市合伙人\n4 事业合伙人\n5 教育培训\n6 联合创始人\n7 艺术家\n8 供货商\n9 产业合伙人\n10 餐饮\n11 互动商家(至尊版)\n12 区域代理商\n13 互动商家(基础版)\n14 合伙人\n15 创始股东',
  `company_goods_categoty_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '经营品类',
  `jm_id` varchar(45) NOT NULL DEFAULT '‘’' COMMENT '法人身份证',
  `co_pay_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '合作支付方式\n0 月结\n1 满1000\n2 实时(微信)',
  `xl_er_cert` varchar(255) NOT NULL DEFAULT '' COMMENT '心理咨询二级证',
  `company_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 待审核\n1 审核通过\n2 未通过',
  `audit_cause` varchar(255) NOT NULL DEFAULT '0' COMMENT '审核原因',
  `co_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 红酒\n1 虫草\n2 积分\n3 保证金',
  `district_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '区级ID',
  `province` varchar(255) NOT NULL DEFAULT '' COMMENT '省',
  `city` varchar(255) NOT NULL DEFAULT '' COMMENT '市',
  `district` varchar(255) NOT NULL DEFAULT '' COMMENT '区',
  `bank_id` varchar(45) NOT NULL DEFAULT '' COMMENT '银行卡账号',
  `range_sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序顺序 降序',
  `bank_name` varchar(100) NOT NULL DEFAULT '0' COMMENT '银行名称',
  `bank_account` varchar(100) NOT NULL DEFAULT '0' COMMENT '银行卡账户',
  `bank_deposit` varchar(100) NOT NULL DEFAULT '0' COMMENT '存款',
  `redirect_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 官网首页\n1 微店',
  `pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '店铺父级ID',
  `slogan` varchar(255) NOT NULL DEFAULT '0' COMMENT '店铺口号',
  `qr_logo_url` varchar(100) NOT NULL DEFAULT '0' COMMENT '点餐二维码img URL',
  `company_type_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商家分类ID',
  `quantitative_classification` varchar(100) NOT NULL DEFAULT '' COMMENT '量化分级',
  `business_company_name` varchar(225) NOT NULL DEFAULT '' COMMENT '注册商家名称',
  `business_legal_person` varchar(225) NOT NULL DEFAULT '' COMMENT '法人',
  `business_address` varchar(225) NOT NULL DEFAULT '' COMMENT '经营地址',
  `business_range` varchar(500) NOT NULL DEFAULT '' COMMENT '经营范围',
  `business_inspect_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '营业许可检查时间',
  `business_effective_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '营业许可有效时间',
  `business_img_back` varchar(225) NOT NULL DEFAULT '' COMMENT '营业许可反面',
  `business_start_time` varchar(20) NOT NULL DEFAULT '' COMMENT '营业起始时间',
  `business_end_time` varchar(20) NOT NULL DEFAULT '' COMMENT '营业结束时间',
  `img_head_photo` varchar(225) NOT NULL DEFAULT '' COMMENT '门店照片',
  `img_front_photo` varchar(255) NOT NULL DEFAULT '' COMMENT '手持身份证正面照片',
  `longitude` varchar(50) NOT NULL DEFAULT '' COMMENT '经度',
  `latitude` varchar(50) NOT NULL DEFAULT '' COMMENT '纬度',
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8 COMMENT='商家'

-- -----------------------------------------------------
-- Table `rtshop`.`company_product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`company_product` ;

CREATE TABLE `company_product` (
  `product_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '本地商家编号',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `remark` varchar(500) NOT NULL DEFAULT '',
  `discount_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '折扣编码',
  `qrcode_url` varchar(255) NOT NULL DEFAULT '' COMMENT '二维码',
  `discount_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 满减\n1 百分比',
  `percentage` int(11) NOT NULL DEFAULT '0' COMMENT '整数 要除以100',
  `reach1` int(11) NOT NULL DEFAULT '0',
  `score1` int(11) NOT NULL DEFAULT '0',
  `reach2` int(11) NOT NULL DEFAULT '0',
  `score2` int(11) NOT NULL DEFAULT '0',
  `reach3` int(11) NOT NULL DEFAULT '0',
  `score3` int(11) NOT NULL DEFAULT '0',
  `reach4` int(11) NOT NULL DEFAULT '0',
  `score4` int(11) NOT NULL DEFAULT '0',
  `reach5` int(11) NOT NULL DEFAULT '0',
  `score5` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8 COMMENT='商家本地扫码'


-- -----------------------------------------------------
-- Table `rtshop`.`dinner_dish`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`dinner_dish` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`dinner_dish` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `company_id` BIGINT(20) NOT NULL DEFAULT '0',
  `name` VARCHAR(20) NOT NULL,
  `price` INT(11) NOT NULL DEFAULT '0',
  `discount_id` BIGINT(20) NOT NULL DEFAULT '0',
  `remark` VARCHAR(100) NOT NULL DEFAULT '',
  `img_url` VARCHAR(100) NOT NULL DEFAULT '',
  `ctime` BIGINT(15) NOT NULL DEFAULT '0',
  `mtime` BIGINT(15) NOT NULL DEFAULT '0',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  AUTO_INCREMENT = 26
  DEFAULT CHARACTER SET = utf8
  COMMENT = '菜品';


-- -----------------------------------------------------
-- Table `rtshop`.`dinner_dish_tag_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`dinner_dish_tag_link` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`dinner_dish_tag_link` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `tag_id` BIGINT(20) NOT NULL DEFAULT '0',
  `dish_id` BIGINT(20) NOT NULL DEFAULT '0',
  `ctime` BIGINT(20) NOT NULL DEFAULT '0',
  `mtime` BIGINT(20) NOT NULL DEFAULT '0',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rtshop`.`dinner_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`dinner_order` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`dinner_order` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `cus_num` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT '就餐人数',
  `order_no` VARCHAR(30) NOT NULL DEFAULT '0' COMMENT '点餐号',
  `company_id` BIGINT(20) NOT NULL DEFAULT '0',
  `table_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '就餐餐座',
  `real_pay` INT(11) NOT NULL DEFAULT '0' COMMENT '实际支付',
  `rtn_score` INT(11) NOT NULL DEFAULT '0' COMMENT '返积分',
  `discount` INT(11) NOT NULL DEFAULT '0' COMMENT '折扣',
  `pay_status` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 未支付\n1 已支付',
  `remark` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '订单备注',
  `fp_head` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '发票抬头',
  `user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '点餐用户',
  `ctime` BIGINT(15) NOT NULL DEFAULT '0',
  `mtime` BIGINT(15) NOT NULL DEFAULT '0',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rtshop`.`dinner_order_dish_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`dinner_order_dish_link` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`dinner_order_dish_link` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `order_id` BIGINT(20) NOT NULL,
  `dish_id` BIGINT(20) NOT NULL,
  `num` SMALLINT(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rtshop`.`dinner_table`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`dinner_table` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`dinner_table` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `company_id` BIGINT(20) NOT NULL DEFAULT '0',
  `table_number` VARCHAR(20) NOT NULL DEFAULT '0' COMMENT '桌号',
  `remark` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '备注',
  `qr_url` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '二维码url',
  `ctime` BIGINT(15) NOT NULL DEFAULT '0',
  `mtime` BIGINT(15) NOT NULL DEFAULT '0',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  INDEX `company_id` (`company_id` ASC),
  CONSTRAINT `dinner_table_ibfk_1`
  FOREIGN KEY (`company_id`)
  REFERENCES `rtshop`.`company` (`company_id`))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rtshop`.`dinner_tag`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`dinner_tag` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`dinner_tag` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `company_id` BIGINT(20) NOT NULL DEFAULT '0',
  `type` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT '标签分类:1-菜品;2-备注选择',
  `name` VARCHAR(30) NOT NULL COMMENT '标签名称',
  `ctime` BIGINT(20) NOT NULL DEFAULT '0',
  `mtime` BIGINT(20) NOT NULL DEFAULT '0',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rtshop`.`district`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`district` ;

CREATE TABLE `district` (
  `id` bigint(20) NOT NULL,
  `name` varchar(90) NOT NULL DEFAULT '',
  `parentid` bigint(20) NOT NULL DEFAULT '0',
  `initial` char(10) NOT NULL DEFAULT '',
  `initials` char(90) NOT NULL DEFAULT '',
  `pinyin` varchar(270) NOT NULL DEFAULT '',
  `suffix` varchar(45) NOT NULL DEFAULT '',
  `code` char(63) NOT NULL DEFAULT '',
  `district_order` tinyint(4) NOT NULL DEFAULT '0',
  `ctime` bigint(20) NOT NULL DEFAULT '0',
  `mtime` bigint(20) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '加盟费,最低1000',
  `used` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 没交费 1 以缴费',
  `biz_user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '区代理',
  `city_user_id` bigint(20) NOT NULL DEFAULT '0',
  `level_type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


-- -----------------------------------------------------
-- Table `rtshop`.`goods`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods` ;

CREATE TABLE `goods` (
  `goods_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品分类编号',
  `goods_name` varchar(100) NOT NULL DEFAULT '' COMMENT '商品名称',
  `category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品分类ID',
  `goods_sn` varchar(50) NOT NULL DEFAULT '' COMMENT '商品货号',
  `goods_screen_price` int(10) NOT NULL DEFAULT '0' COMMENT '本店价格',
  `goods_real_price` int(10) NOT NULL DEFAULT '0' COMMENT '进货价格',
  `self_support` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 自营\n0 非自营',
  `goods_provider_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '供货商ID',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常\n1 删除',
  `goods_online` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 上架\n0 下架',
  `goods_recommend_sort` int(11) NOT NULL DEFAULT '0' COMMENT '推荐排序',
  `goods_stock` int(11) NOT NULL DEFAULT '0' COMMENT '库存',
  `goods_dumy_sales` int(11) NOT NULL DEFAULT '0',
  `goods_detail` varchar(3000) NOT NULL DEFAULT '' COMMENT '商品详情',
  `goods_thum` varchar(100) NOT NULL DEFAULT '' COMMENT '商品列表缩略图',
  `goods_ex_price` int(11) NOT NULL DEFAULT '0' COMMENT '运费',
  `goods_pay_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0  纯现金\n1  纯积分\n2  现金+积分',
  `goods_score` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `goods_provider_type` tinyint(4) NOT NULL DEFAULT '8' COMMENT '0 实体店\n1 微商\n8 供货商',
  `give_score` int(11) NOT NULL DEFAULT '0',
  `goods_get_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 邮寄\n1 自提',
  `goods_weight` int(11) NOT NULL DEFAULT '0' COMMENT '商品重量',
  `custom_type_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商家店铺  自定义商品分类ID',
  `best` tinyint(4) NOT NULL DEFAULT '0' COMMENT '热卖',
  `newest` tinyint(4) NOT NULL DEFAULT '0' COMMENT '最新',
  `discount` tinyint(4) NOT NULL DEFAULT '0' COMMENT '折扣',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2739 DEFAULT CHARSET=utf8 COMMENT='商品基本信息'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_album`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_album` ;

CREATE TABLE `goods_album` (
  `image_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '相册编号',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品编号',
  `image_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 相册\n0 详情',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常\n1 删除',
  `url` varchar(200) NOT NULL DEFAULT '' COMMENT 'image url',
  `original` varchar(200) NOT NULL DEFAULT '' COMMENT '原名',
  `state` varchar(45) NOT NULL DEFAULT '' COMMENT 'upload type',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15126 DEFAULT CHARSET=utf8 COMMENT='商品扩展信息'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_type` ;

CREATE TABLE `goods_type` (
  `type_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品类型编号',
  `type_name` varchar(100) NOT NULL DEFAULT '' COMMENT '商品类型名称',
  `type_group` varchar(500) NOT NULL DEFAULT '' COMMENT '商品类型组',
  `enabled` tinyint(1) DEFAULT '0' COMMENT '1 启用\n0 不启用',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常\n1 删除',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COMMENT='商品类型'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_attribute`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_attribute` ;

CREATE TABLE `goods_attribute` (
  `attr_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品重量，最小单位g',
  `attr_name` varchar(50) NOT NULL DEFAULT '' COMMENT '属性名称',
  `type_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '类型编号',
  `attr_values` varchar(1000) NOT NULL DEFAULT '0' COMMENT '属性值',
  `attr_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '1 多选\n0 单选',
  `attr_sort` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `goods_promote_price` int(10) NOT NULL DEFAULT '0' COMMENT '促销价格',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常\n1 删除',
  PRIMARY KEY (`attr_id`),
  KEY `fk_type_id_idx` (`type_id`),
  CONSTRAINT `fk_type_id` FOREIGN KEY (`type_id`) REFERENCES `goods_type` (`type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COMMENT='商品属性'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_attribute_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_attribute_link` ;

CREATE TABLE `goods_attribute_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goods_id` bigint(20) NOT NULL COMMENT '商品编号',
  `attr_id` bigint(20) NOT NULL COMMENT '属性编号',
  `extra_price` int(10) NOT NULL DEFAULT '0' COMMENT '额外加的钱',
  `attr_value` varchar(50) NOT NULL DEFAULT '' COMMENT '1， 隐藏不显示\n2， 前端显示',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `attr_count` int(10) NOT NULL DEFAULT '0' COMMENT '此属性拥有的商品数量',
  `attr_show` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否显示在 商品详情页的“请选择”\n0 不显示\n1 显示',
  `attr_name` varchar(50) NOT NULL DEFAULT '' COMMENT '商品属性名',
  `real_extra_price` int(11) NOT NULL DEFAULT '0' COMMENT '进货价增量',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_goods_attr_value` (`goods_id`,`attr_id`,`attr_value`),
  KEY `fk_goods_id_idx` (`goods_id`),
  KEY `fk_attr_id_idx` (`attr_id`),
  CONSTRAINT `fk_g_a_attr_id` FOREIGN KEY (`attr_id`) REFERENCES `goods_attribute` (`attr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_g_a_goods_id` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`goods_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5820 DEFAULT CHARSET=utf8 COMMENT='商品属性中间表'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_extras`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_extras` ;

CREATE TABLE `goods_extras` (
  `goods_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `goods_weight` int(10) NOT NULL DEFAULT '0' COMMENT '商品重量，最小单位g',
  `goods_xp_free` tinyint(2) NOT NULL DEFAULT '1' COMMENT '1 免运费\n0 要快递费',
  `goods_xp_price` int(10) NOT NULL DEFAULT '0' COMMENT '快递费用',
  `goods_promote_price` int(10) unsigned zerofill NOT NULL DEFAULT '0000000000' COMMENT '促销价格',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常\n1 删除',
  PRIMARY KEY (`goods_id`),
  CONSTRAINT `fk_goods_id` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`goods_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品扩展信息'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_order` ;

CREATE TABLE `goods_order` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '购买者编号',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `pay_way` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 微信支付\n\n1 支付宝支付\n\n2 龙蛙红包支付\n\n3 龙蛙红包与现金支付\n\n4 龙蛙软件购买',
  `pay_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 未支付\n1 已支付',
  `audit_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态\n0 未审核\n1 审核通过\n2 审核不通过 （原因）',
  `order_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单状态\n0 （买家：等待卖家发货；商家：待发货）\n1  （买家：商家已发货；商家：已发货）\n2 （买家：确认已收获，；商家：已完成）\n3 （买家：申请退货，原因；商家：买家申请退货）\n4 (买家：申请退款，原因；）\n5 交易关闭\n6 以配货（龙蛙已经联系过供货商）',
  `remark` varchar(500) NOT NULL DEFAULT '',
  `audit_user_id` bigint(20) NOT NULL DEFAULT '0',
  `order_sn` varchar(45) NOT NULL DEFAULT '0' COMMENT '订单唯一编号',
  `shipping_address` varchar(200) NOT NULL DEFAULT '' COMMENT '收货地址',
  `ex_way` tinyint(4) NOT NULL DEFAULT '0' COMMENT '快递方式\n0 在线支付\n1 到付',
  `ex_price` int(11) NOT NULL DEFAULT '0',
  `ex_text` varchar(200) NOT NULL DEFAULT '',
  `ex_sn` varchar(45) NOT NULL DEFAULT '' COMMENT '物流快递单号',
  `mobile` varchar(45) NOT NULL DEFAULT '' COMMENT '收货人联系电话',
  `consignee` varchar(45) NOT NULL DEFAULT '' COMMENT '收货人',
  `goods_pay_type` tinyint(4) NOT NULL DEFAULT '0',
  `order_amount` int(11) NOT NULL DEFAULT '0' COMMENT '订单总金额，不包括运费',
  `amount_score` int(11) NOT NULL DEFAULT '0' COMMENT '送的积分数',
  `amount_money` int(11) NOT NULL DEFAULT '0' COMMENT '返利金额',
  `member_price` int(11) NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付类型\n\n1:现金\n2:积分\n3:积分+现金\n4:现金+积分+优惠券\n5;现金+优惠券\n6:积分+优惠券',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=848 DEFAULT CHARSET=utf8 COMMENT='订单信息'


-- -----------------------------------------------------
-- Table `rtshop`.`goods_type_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`goods_type_link` ;

CREATE TABLE `goods_type_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品类型名称',
  `type_id` bigint(20) NOT NULL DEFAULT '0',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常\n1 删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_goods_type_link` (`goods_id`,`type_id`),
  KEY `fk_type_id_idx` (`type_id`),
  CONSTRAINT `fk_g_t_goods_id` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`goods_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_g_t_type_id` FOREIGN KEY (`type_id`) REFERENCES `goods_type` (`type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品类型中间表'


-- -----------------------------------------------------
-- Table `rtshop`.`local_discount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`local_discount` ;

CREATE TABLE `local_discount` (
  `discount_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `discount` int(11) NOT NULL DEFAULT '0' COMMENT '折扣',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '送积分',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `limit_price` int(11) NOT NULL DEFAULT '0' COMMENT '单笔消费达到的金额才送积分',
  `title` varchar(255) DEFAULT '',
  PRIMARY KEY (`discount_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='商家入住信息'


-- -----------------------------------------------------
-- Table `rtshop`.`local_order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`local_order` ;

CREATE TABLE `local_order` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '购买者编号',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `pay_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 未支付\n1 已支付',
  `audit_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态\n0 未审核\n1 审核通过\n2 审核不通过 （原因）',
  `remark` varchar(500) NOT NULL DEFAULT '',
  `audit_user_id` bigint(20) NOT NULL DEFAULT '0',
  `order_sn` varchar(45) NOT NULL DEFAULT '0' COMMENT '订单唯一编号',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '订单消费金额',
  `product_id` bigint(20) NOT NULL DEFAULT '0',
  `company_id` bigint(20) NOT NULL DEFAULT '0',
  `discount_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '优惠折扣Id (Discarded)',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '订单消费积分',
  `favorable_price` int(11) NOT NULL DEFAULT '0' COMMENT '满减券价格',
  `local_order_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付类型\n\n1:现金\n2:积分\n3:积分+现金\n4:现金+积分+优惠券\n5;现金+优惠券\n6:积分+优惠券',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '订单总金额',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='本地扫码订单'


-- -----------------------------------------------------
-- Table `rtshop`.`logistics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`logistics` ;

CREATE TABLE `logistics` (
  `logistics_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'name',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`logistics_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='物流表'


-- -----------------------------------------------------
-- Table `rtshop`.`meeting_user_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`meeting_user_link` ;

CREATE TABLE `meeting_user_link` (
  `link_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '会议标题',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `meeting_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '会议价格 单位 ： 分',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `meeting_user_uq` (`phone`,`meeting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=631 DEFAULT CHARSET=utf8 COMMENT='用户参会关联表'


-- -----------------------------------------------------
-- Table `rtshop`.`meeting_users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`meeting_users` ;

CREATE TABLE `meeting_users` (
  `meeting_user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `department` varchar(45) NOT NULL DEFAULT '' COMMENT '部门',
  `phone` varchar(45) NOT NULL DEFAULT '' COMMENT '手机号',
  `position` varchar(45) NOT NULL DEFAULT '' COMMENT '职位',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `email` varchar(50) NOT NULL DEFAULT '' COMMENT '邮箱',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `real_name` varchar(45) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `locked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 已签到 \n0 未签到',
  `id_card` varchar(100) NOT NULL DEFAULT '' COMMENT '身份证号',
  `meeting_id` bigint(20) NOT NULL DEFAULT '0',
  `company_name` varchar(100) NOT NULL DEFAULT '' COMMENT '公司名字',
  PRIMARY KEY (`meeting_user_id`),
  UNIQUE KEY `meeting_users_UNIQUE` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=944 DEFAULT CHARSET=utf8 COMMENT='会议用户表'


-- -----------------------------------------------------
-- Table `rtshop`.`meetings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`meetings` ;

CREATE TABLE `meetings` (
  `meeting_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `meeting_title` varchar(100) NOT NULL DEFAULT '0' COMMENT '会议标题',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `meeting_price` int(11) NOT NULL DEFAULT '0' COMMENT '会议价格 单位 ： 分',
  `meeting_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 免费\n1 收费',
  `meeting_content` varchar(10000) NOT NULL DEFAULT '' COMMENT '会议内容，富文本',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编号',
  `qr_sign_up` varchar(100) NOT NULL DEFAULT '' COMMENT '会议报名二维码',
  `qr_sign_in` varchar(100) NOT NULL DEFAULT '' COMMENT '会议签到',
  `meeting_address` varchar(100) NOT NULL DEFAULT '',
  `meeting_date` varchar(45) NOT NULL DEFAULT '',
  `meeting_holder` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`meeting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='会议信息'


-- -----------------------------------------------------
-- Table `rtshop`.`order_prod`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`order_prod` ;

CREATE TABLE `order_prod` (
  `prod_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '订单编号',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `goods_name` varchar(100) NOT NULL DEFAULT '',
  `goods_screen_price` int(10) NOT NULL DEFAULT '0',
  `goods_real_price` int(10) NOT NULL DEFAULT '0',
  `goods_attr_values` varchar(100) NOT NULL DEFAULT '',
  `goods_attr_link_id` varchar(100) NOT NULL DEFAULT '' COMMENT '属性ID，以逗号分隔.eg: 1，2，3',
  `goods_number` int(10) NOT NULL DEFAULT '0' COMMENT '购买数量',
  `goods_thum` varchar(100) NOT NULL DEFAULT '',
  `self_support` tinyint(1) NOT NULL DEFAULT '0',
  `remark` varchar(500) NOT NULL DEFAULT '',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品编号',
  `logistics_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '物流公司ID',
  `logistics_name` varchar(45) NOT NULL DEFAULT '' COMMENT '物流公司名称',
  `logistics_sn` varchar(50) NOT NULL DEFAULT '' COMMENT '物流单号',
  `xp_price` int(10) NOT NULL DEFAULT '0' COMMENT '运费',
  `goods_pay_type` tinyint(4) NOT NULL DEFAULT '0',
  `goods_score` int(10) NOT NULL DEFAULT '0',
  `goods_provider_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '供货商编号',
  `give_score` int(11) NOT NULL DEFAULT '0' COMMENT '送的积分',
  `give_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 供货商产品\n1 微商',
  `goods_provider_type` tinyint(4) NOT NULL DEFAULT '0',
  `goods_get_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 邮寄\n1 自提',
  `member_price` int(11) NOT NULL DEFAULT '0',
  `company_balance` int(11) NOT NULL DEFAULT '0' COMMENT '公司余额',
  `company_coupon_id` bigint(20) NOT NULL DEFAULT '0',
  `company_coupon_price` int(10) NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '订单分类\n\n1：购物订单\n2：龙蛙支付',
  PRIMARY KEY (`prod_id`),
  KEY `fk_category_recommend_category_id_idx` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=810 DEFAULT CHARSET=utf8 COMMENT='订单产品信息'


-- -----------------------------------------------------
-- Table `rtshop`.`persistent_logins`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`persistent_logins` ;

CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


-- -----------------------------------------------------
-- Table `rtshop`.`relations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`relations` ;

CREATE TABLE `relations` (
  `uesr_id` bigint(20) NOT NULL COMMENT '用户编号',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`uesr_id`),
  UNIQUE KEY `unq_user_pid_key` (`uesr_id`,`pid`),
  CONSTRAINT `fk_user_relation_id` FOREIGN KEY (`uesr_id`) REFERENCES `t_sys_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='层级关系，每个用户只有一个父级ID'


-- -----------------------------------------------------
-- Table `rtshop`.`shopping_cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`shopping_cart` ;

CREATE TABLE `shopping_cart` (
  `cart_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品分类编号',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编号',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `goods_attr_link_id` varchar(45) NOT NULL DEFAULT '' COMMENT '商品属性ID，以逗号分隔.eg: 1，2，3',
  `goods_number` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cart_id`),
  KEY `fk_category_recommend_category_id_idx` (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=utf8 COMMENT='购物车'


-- -----------------------------------------------------
-- Table `rtshop`.`sns_wx_userinfo`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`sns_wx_userinfo` ;

CREATE TABLE `sns_wx_userinfo` (
  `user_id` bigint(20) NOT NULL COMMENT '用户编码',
  `nickname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '',
  `openid` varchar(45) NOT NULL DEFAULT '' COMMENT '活动链接',
  `headimgurl` varchar(500) NOT NULL DEFAULT '' COMMENT '互动内容',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '活动类型\n0 轮播图\n1 二级banner',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `province` varchar(255) NOT NULL DEFAULT '0' COMMENT '商品编码',
  `city` varchar(255) NOT NULL DEFAULT '0' COMMENT '排序',
  `activity_image_url` varchar(100) NOT NULL DEFAULT '' COMMENT '广告图片',
  `language` varchar(45) NOT NULL DEFAULT '',
  `country` varchar(255) NOT NULL DEFAULT '',
  `unionid` varchar(45) NOT NULL DEFAULT '',
  `access_token` varchar(200) NOT NULL DEFAULT '',
  `expires_in` int(11) NOT NULL DEFAULT '0',
  `refresh_token` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信信息'


-- -----------------------------------------------------
-- Table `rtshop`.`stat_company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`stat_company` ;

CREATE TABLE `stat_company` (
  `company_id` bigint(20) NOT NULL COMMENT '编号',
  `order_count` int(11) NOT NULL DEFAULT '0' COMMENT '商城订单数',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `local_order_count` int(11) NOT NULL DEFAULT '0' COMMENT '扫码订单数',
  `order_income` int(11) NOT NULL DEFAULT '0' COMMENT '商城订单总收入',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `local_order_income` int(11) NOT NULL DEFAULT '0' COMMENT '店面订单收入',
  `dish_order_count` int(11) NOT NULL DEFAULT '0' COMMENT '点餐订餐数量',
  `dish_order_income` int(11) NOT NULL DEFAULT '0' COMMENT '点餐订单收入',
  `date` varchar(45) NOT NULL DEFAULT '0000-00-00' COMMENT '日期',
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_company_day` (`company_id`,`date`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8 COMMENT='本地扫码订单'


-- -----------------------------------------------------
-- Table `rtshop`.`stat_company_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`stat_company_user` ;

CREATE TABLE `stat_company_user` (
  `company_id` bigint(20) NOT NULL COMMENT '编号',
  `order_count` int(11) NOT NULL DEFAULT '0' COMMENT '商城订单数',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后修改时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '行状态\n0 正常，1 删除',
  `local_order_count` int(11) NOT NULL DEFAULT '0' COMMENT '扫码订单数',
  `order_consumption` int(11) NOT NULL DEFAULT '0' COMMENT '用户消费额',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `local_order_consumption` int(11) NOT NULL DEFAULT '0' COMMENT '店面用户消费额',
  `dish_order_count` int(11) NOT NULL DEFAULT '0' COMMENT '点餐订餐数量',
  `dish_order_consumption` int(11) NOT NULL DEFAULT '0' COMMENT '点餐订单消费额',
  `date` varchar(45) NOT NULL DEFAULT '0000-00-00' COMMENT '日期',
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_company_user` (`company_id`,`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=323 DEFAULT CHARSET=utf8 COMMENT='本地扫码订单'


-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_param`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`t_sys_param` ;

CREATE TABLE `t_sys_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'name',
  `value` varchar(255) NOT NULL DEFAULT '' COMMENT 'value',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='参数表'


-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_resource`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`t_sys_resource` ;

CREATE TABLE `t_sys_resource` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `icon` varchar(105) NOT NULL DEFAULT '' COMMENT 'icon',
  `link` varchar(100) NOT NULL DEFAULT '-1' COMMENT '资源url',
  `pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '父资源id',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `ctime` bigint(15) DEFAULT NULL COMMENT '创建时间',
  `mtime` bigint(15) DEFAULT NULL COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 api, 1 html',
  `is_show` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 显示，1 隐藏',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='资源表'


-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`t_sys_role` ;

CREATE TABLE `t_sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '父角色id',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `ctime` bigint(15) DEFAULT NULL COMMENT '创建时间',
  `mtime` bigint(15) DEFAULT NULL COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='角色表'


-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_role_resource_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`t_sys_role_resource_link` ;

CREATE TABLE `t_sys_role_resource_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `resource_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '资源id',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fk_link_role_resource_role_id` (`role_id`,`resource_id`),
  KEY `fk_role_resource_resource_id_idx` (`resource_id`),
  CONSTRAINT `fk_role_resource_resource_id` FOREIGN KEY (`resource_id`) REFERENCES `t_sys_resource` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_resource_role_id` FOREIGN KEY (`role_id`) REFERENCES `t_sys_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=965 DEFAULT CHARSET=utf8 COMMENT='角色、资源关系表'


-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_user_role_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`t_sys_user_role_link` ;

CREATE TABLE `t_sys_user_role_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户id',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_user_role_id` (`role_id`,`user_id`),
  KEY `fk_link_user_role_user_id_idx` (`user_id`),
  KEY `fk_link_user_role_role_id_idx` (`role_id`),
  CONSTRAINT `fk_link_user_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `t_sys_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_link_user_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `t_sys_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=utf8 COMMENT='用户、角色关系表'


-- -----------------------------------------------------
-- Table `rtshop`.`user_lock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`user_lock` ;

CREATE TABLE `user_lock` (
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '主键',
  `district_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '申请次数',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `province` varchar(100) NOT NULL DEFAULT '‘’',
  `city` varchar(100) NOT NULL DEFAULT '‘’',
  `district` varchar(100) NOT NULL DEFAULT '‘’',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='锁定用户区域'


-- -----------------------------------------------------
-- Table `rtshop`.`user_meetings_link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`user_meetings_link` ;

CREATE TABLE `user_meetings_link` (
  `meeting_link_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '会议标题',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `meeting_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '会议价格 单位 ： 分',
  PRIMARY KEY (`meeting_link_id`),
  UNIQUE KEY `meeting_user_uq` (`user_id`,`meeting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户参会关联表'


-- -----------------------------------------------------
-- Table `rtshop`.`user_members`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`user_members` ;

CREATE TABLE `user_members` (
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '主键',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `start_time` varchar(10) NOT NULL DEFAULT '',
  `end_time` varchar(10) NOT NULL DEFAULT '',
  `consumer` bigint(15) NOT NULL DEFAULT '0' COMMENT '消费金额',
  `comsumer_id` bigint(15) NOT NULL DEFAULT '0',
  `member_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 快省-龙蛙会员\n1 快转-微传媒会员\n2 快点-扫码点餐会员\n3 快联-跨界盈利\n4 快销-商家促销\n5 快签-扫码签到\n6 快飞-梦想\n7 快推-城市\n8 快赚-打包（快省、快转、快销、快联）\n9 云动\n',
  `member_id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`member_id`),
  UNIQUE KEY `id_type_unq` (`user_id`,`member_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1142 DEFAULT CHARSET=utf8 COMMENT='会员期限'


-- -----------------------------------------------------
-- Table `rtshop`.`user_profit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`user_profit` ;

CREATE TABLE `user_profit` (
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '主键',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `order_id` bigint(15) NOT NULL DEFAULT '0',
  `user_profit` int(11) NOT NULL DEFAULT '0' COMMENT '收益金额',
  `consumer_id` bigint(15) NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-4 发积分红包\n-3 扣除积分\n-2 退款\n-1  提现\n0 供货商订单奖励\n1 地面店扫码奖励\n2 微商订单奖励\n3 地面店扫码返积分\n4 微商订单返积分\n5 点餐送积分\n6 点餐奖励\n7 云转奖励\n8 云省奖励\n9 云签奖励\n10 云飞奖励\n11 云推奖励\n12 云点奖励\n13 云联奖励\n14 云销奖励\n15 云赚奖励\n16 互动店商品奖励\n17 互动店霸屏奖励\n18 互动店打赏奖励\n19 互动店抢红包积分\n20 互动店区级返利\n21 互动店市级返利\n22 互动店省级返利',
  `profit_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_sn` varchar(45) NOT NULL DEFAULT '0' COMMENT '订单唯一编号',
  `user_balance` int(11) NOT NULL DEFAULT '0' COMMENT '个人账号余额',
  `profit_type` tinyint(4) NOT NULL DEFAULT '0',
  `money_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '返利金额类型\n\n1 现金\n2 积分',
  PRIMARY KEY (`profit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2365 DEFAULT CHARSET=utf8 COMMENT='会员收益'


-- -----------------------------------------------------
-- Table `rtshop`.`user_reward`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`user_reward` ;

CREATE TABLE `user_reward` (
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '主键',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `reward_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 个人账户， \n1 商家账户',
  `reward_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `real_name` varchar(100) NOT NULL DEFAULT '' COMMENT '微信对应的真实姓名',
  `id_card` varchar(100) NOT NULL DEFAULT '' COMMENT '微信对应的真实身份证号码',
  `payment_no` varchar(45) NOT NULL DEFAULT '0' COMMENT '微信订单号',
  `bank_id` varchar(45) NOT NULL DEFAULT '0' COMMENT '银行卡卡号',
  `bank_card_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '银行卡包ID唯一ID',
  `reward_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '提现账户类型\n\n1:微信\n2:银行卡',
  PRIMARY KEY (`reward_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COMMENT='会员提现申请表'


-- -----------------------------------------------------
-- Table `rtshop`.`verifycode`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`verifycode` ;

CREATE TABLE `verifycode` (
  `verifycode_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `invite_count` int(11) NOT NULL DEFAULT '0' COMMENT '申请次数',
  `verifycode` varchar(45) NOT NULL DEFAULT '' COMMENT '验证码',
  `mobile` varchar(45) NOT NULL DEFAULT '' COMMENT '手机号',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `reg_ip` varchar(45) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `reg_deadline` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后失效时间',
  PRIMARY KEY (`verifycode_id`),
  UNIQUE KEY `phone_UNIQUE` (`mobile`)
) ENGINE=InnoDB AUTO_INCREMENT=2770 DEFAULT CHARSET=utf8 COMMENT='验证码'


-- -----------------------------------------------------
-- Table `rtshop`.`wallet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`wallet` ;

CREATE TABLE `wallet` (
  `user_id` bigint(20) NOT NULL COMMENT '主键,用户编号',
  `user_score` int(11) NOT NULL DEFAULT '0' COMMENT '剩余积分 (分)',
  `user_balance` int(11) NOT NULL DEFAULT '0' COMMENT '余额',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `user_amount` int(11) NOT NULL DEFAULT '0' COMMENT '总额',
  `user_score_total` int(11) NOT NULL DEFAULT '0' COMMENT '总积分',
  `history_balance` int(11) NOT NULL DEFAULT '0',
  `history_score` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_wallet_user_id` FOREIGN KEY (`user_id`) REFERENCES `t_sys_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='钱包'


-- -----------------------------------------------------
-- Table `rtshop`.`wallet_company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`wallet_company` ;

CREATE TABLE `wallet_company` (
  `company_id` bigint(20) NOT NULL COMMENT '主键,用户编号',
  `company_amount` int(11) NOT NULL DEFAULT '0' COMMENT '总额',
  `company_balance` int(11) NOT NULL DEFAULT '0' COMMENT '余额',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `company_yestoday` int(11) NOT NULL DEFAULT '0',
  `company_type` tinyint(4) DEFAULT '0' COMMENT '公司类型',
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='钱包'


-- -----------------------------------------------------
-- Table `rtshop`.`web_user_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`web_user_list` ;

CREATE TABLE IF NOT EXISTS `rtshop`.`web_user_list` (
  `UpperUserID` VARCHAR(255) NULL DEFAULT NULL,
  `WXopenid` VARCHAR(255) NULL DEFAULT NULL,
  `WXnickname` VARCHAR(255) COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT '' COMMENT '账号',
  `WXheadimgurl` VARCHAR(255) NULL DEFAULT NULL,
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  AUTO_INCREMENT = 12202
  DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `rtshop`.`wx_client_credentail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`wx_client_credentail` ;

CREATE TABLE `wx_client_credentail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(255) NOT NULL DEFAULT '',
  `expires_in` int(11) NOT NULL DEFAULT '0',
  `ctime` bigint(20) NOT NULL DEFAULT '0',
  `mtime` bigint(20) NOT NULL DEFAULT '0',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8


-- -----------------------------------------------------
-- Table `rtshop`.`mz_ads`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`mz_ads` ;

CREATE TABLE `mz_ads` (
  `ad_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `ad_title` varchar(200) NOT NULL DEFAULT '' COMMENT '活动标题',
  `ad_url` varchar(100) NOT NULL DEFAULT '' COMMENT '活动链接',
  `button_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '按钮类型0 无，1 悬浮',
  `ad_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '类型\n0  名片\n1   广告\n2   链接',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `link_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '链接类型0 普通链接，1 电话， 2 电话和链接',
  `ad_position` tinyint(4) NOT NULL DEFAULT '0' COMMENT '广告位置0 顶部， 1 底部',
  `ad_image_url` varchar(100) NOT NULL DEFAULT '' COMMENT '广告图片',
  `ad_author` bigint(20) NOT NULL DEFAULT '0' COMMENT '文章发布者',
  `phone` varchar(45) NOT NULL DEFAULT '0',
  `button_name` varchar(100) NOT NULL DEFAULT '',
  `remark` varchar(100) NOT NULL DEFAULT '',
  `ad_content` varchar(5000) NOT NULL DEFAULT '' COMMENT '文本',
  `ad_company` varchar(45) NOT NULL DEFAULT '' COMMENT '公司',
  `ad_headimage_url` varchar(100) NOT NULL DEFAULT '' COMMENT '头像',
  `ad_email` varchar(100) NOT NULL DEFAULT '' COMMENT '邮件',
  `ad_wechat` varchar(45) NOT NULL DEFAULT '' COMMENT '微信',
  `ad_address` varchar(100) NOT NULL DEFAULT '' COMMENT '地址',
  PRIMARY KEY (`ad_id`)
) ENGINE=InnoDB AUTO_INCREMENT=346 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`msg`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`events` ;

CREATE TABLE `events` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `who_id` varchar(200) NOT NULL DEFAULT '0' COMMENT '主语',
  `who_txt` varchar(100) NOT NULL DEFAULT '' COMMENT '昵称',
  `who_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '主语类型 0 user',
  `pred_txt` varchar(100) NOT NULL DEFAULT '' COMMENT '描述',
  `pred_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '宾语类型 0 申请售后',
  `whom_id` varchar(200) NOT NULL DEFAULT '' COMMENT '谓语 退货产品ID',
  `whom_txt` varchar(100) NOT NULL DEFAULT '' COMMENT '昵称',
  `whom_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '谓语类型 0 退货产品ID',
  `related_id` varchar(200) NOT NULL DEFAULT '0' COMMENT '关联者ID 退货订单ID，',
  `related_txt` varchar(100) NOT NULL DEFAULT '' COMMENT '描述',
  `related_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '关联者类型 0 商城退货订单',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `version` tinyint(4) NOT NULL DEFAULT '0' COMMENT '版本号',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


-- -----------------------------------------------------
-- Table `rtshop`.`activity`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rtshop`.`ex_price` ;

CREATE TABLE `ex_price` (
  `exprice_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `province_id` varchar(200) NOT NULL DEFAULT '' COMMENT '省',
  `city_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '市',
  `logistics_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '快递公司编号',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '运费',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `height_extra_price` int(11) NOT NULL DEFAULT '0' COMMENT '超过1kg额外多少钱',
  `height_base` int(11) NOT NULL DEFAULT '0' COMMENT '基准重量',
  PRIMARY KEY (`exprice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8


USE `rtshop` ;

-- -----------------------------------------------------
-- procedure rt_rollback_data
-- -----------------------------------------------------

USE `rtshop`;
DROP procedure IF EXISTS `rtshop`.`rt_rollback_data`;

DELIMITER $$
USE `rtshop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `rt_rollback_data`()
  begin
    declare v_max int unsigned default 0;
    declare v_user_id int unsigned default 0;
    declare v_counter int unsigned default 0;
    declare v_id int unsigned default 0;
    declare v_temp_num int unsigned default 0;
    declare v_temp_uuid varchar(45) default '0';


    -- start transaction;

    -- select count(*)  from `rt-win`.Web_User_List where wxopenid is not null and flag = 1;
    set v_max = 500;
    while v_counter < v_max do

      select count(*) into v_temp_num from (select * from `rt-win`.Web_User_List as o where o.flag = 0 and o.wxopenid is not null limit 1) temp;

      -- select count(*) into v_temp_num from `rt-win`.Web_User_List as o where o.flag = 0 and o.wxopenid is not null limit 1;
      select o.webuserid into v_id from `rt-win`.Web_User_List as o where o.flag = 0 and o.wxopenid is not null limit 1;
      -- log
      -- select v_temp_num as 'v_temp_num1';
      -- select v_id as 'v_id';

      if v_temp_num = 1 then

        select count(*) into v_temp_num from `rtshop`.sns_wx_userinfo as n,`rt-win`.Web_User_List as o where n.openid = o.wxopenid and o.webuserid = v_id;

        -- log
        -- select v_temp_num as 'v_temp_num2';

        -- 已经恢复
        if v_temp_num = 1 then

          update `rt-win`.Web_User_List set flag = 1 where webuserid = v_id;

        end if;

        -- 没有恢复

        if v_temp_num = 0 then

          set v_temp_uuid = UUID();
          -- select v_temp_uuid as 'v_temp_uuid';

          insert into `rtshop`.t_sys_user (username,headimgurl,nickname) select v_temp_uuid as username,wxheadimgurl,wxnickname from `rt-win`.Web_User_List where webuserid=v_id;
          select id into v_user_id from `rtshop`.t_sys_user where username = v_temp_uuid;
          insert into `rtshop`.sns_wx_userinfo (user_id,openid,unionid,headimgurl,nickname) select v_user_id as user_id ,wxopenid,v_temp_uuid as 'unionid', wxheadimgurl,wxnickname from `rt-win`.Web_User_List where webuserid=v_id;
          update `rt-win`.Web_User_List set flag = 1 where webuserid = v_id;

        -- insert into `rtshop`.relations  (uesr_id,pid) values ((select max(id) from t_sys_user),151);
        -- select v_temp_num as 'v_temp_num3';
        end if;

      end if;

      set v_counter=v_counter+1;
    end while;
    -- commit;
  end$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure showChildLst
-- -----------------------------------------------------

USE `rtshop`;
DROP procedure IF EXISTS `rtshop`.`showChildLst`;

DELIMITER $$
USE `rtshop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `showChildLst`(IN rootid INT)
  BEGIN
    DECLARE Level int ;
    drop TABLE IF EXISTS tmpLst;
    CREATE TABLE tmpLst (
      id int,
      nLevel int,
      sCort varchar(8000)
    );

    Set Level=0 ;
    INSERT into tmpLst SELECT id,Level,ID FROM crm_presona_dk WHERE presona_pid=rootid;
    WHILE ROW_COUNT()>0 DO
      SET Level=Level+1 ;
      INSERT into tmpLst
        SELECT A.id,Level,concat(B.sCort,'-',A.id) FROM crm_presona_dk A,tmpLst B
        WHERE  A.presona_pid=B.id AND B.nLevel=Level-1  ;
    END WHILE;

  END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


-- 来秀
-- history
DROP TABLE IF EXISTS `rtshop`.`ls_historical_trace` ;
CREATE TABLE `ls_historical_trace` (
  `trace_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  PRIMARY KEY (`trace_id`),
  UNIQUE KEY `unq_ls_historical_trace` (`user_id`,`company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`ls_msg`
-- -----------------------------------------------------
-- room msg
DROP TABLE IF EXISTS `rtshop`.`ls_msg` ;
CREATE TABLE `ls_msg` (
  `msg_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `from_user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '发送者编码',
  `from_user_text` varchar(500) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0' COMMENT '发送者昵称',
  `from_user_avatar` varchar(500) NOT NULL DEFAULT '0' COMMENT '发送者头像',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `to_user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '接收者编码',
  `to_user_text` varchar(500) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0' COMMENT '接收者昵称',
  `to_user_avatar` varchar(500) NOT NULL DEFAULT '0' COMMENT '接收者头像',
  `pred` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户行为\n0 普通消息\n1 购分享\n2 我耀评\n3 我耀赞\n4 我耀秀\n5 趣竞价\n6 积分红包\n7 赏\n8 耀霸屏\n9 商家促销\n10 商家秀\n11 品牌故事\n12 后厨通知菜做好了\n13 用户延后上菜\n14 用户呼叫服务员\n15 商家趣竞价',
  `pred_text` varchar(100) NOT NULL DEFAULT '' COMMENT 'pred说明',
  `msg_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '消息类型\n0 文本\n1 图片\n2 语音\n3 视频',
  `msg_content` varchar(2000) NOT NULL DEFAULT '0' COMMENT '互动内容,富文本',
  `usurp_screen_num` int(11) NOT NULL DEFAULT '1' COMMENT '霸屏数量',
  `usurp_screen_theme_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '霸屏主题编码',
  `usurp_screen_theme_text` varchar(500) NOT NULL DEFAULT '0' COMMENT '霸屏主题说明',
  `usurp_screen_time` int(11) NOT NULL DEFAULT '0' COMMENT '霸屏时长',
  `usurp_screen_price` int(11) NOT NULL DEFAULT '0' COMMENT '霸屏单价',
  `usurp_screen_table_no` int(11) NOT NULL DEFAULT '0' COMMENT '霸屏桌号',
  `reward_to` bigint(20) NOT NULL DEFAULT '0' COMMENT '打赏对象编码',
  `reward_text` varchar(100) NOT NULL DEFAULT '' COMMENT '打赏说明说明',
  `reward_gift` int(11) NOT NULL DEFAULT '0' COMMENT '打赏礼物',
  `reward_price` int(11) NOT NULL DEFAULT '0' COMMENT '打赏礼物单价',
  `reward_gift_num` int(11) NOT NULL DEFAULT '1' COMMENT '打赏礼物数量',
  `play_singer` varchar(100) NOT NULL DEFAULT '0' COMMENT '乐队或歌手名',
  `play_singer_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '乐队或歌手编码',
  `play_music_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '点播音乐编码',
  `play_music_text` varchar(100) NOT NULL DEFAULT '' COMMENT '点播音乐名字',
  `play_music_price` int(11) NOT NULL DEFAULT '0' COMMENT '点播音乐单价',
  `play_music_num` int(11) NOT NULL DEFAULT '1' COMMENT '点播音乐数量',
  `lucky_money_price` int(11) NOT NULL DEFAULT '0' COMMENT '红包金额',
  `lucky_money_num` int(11) NOT NULL DEFAULT '1' COMMENT '红包数量',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `pay_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付状态1 未支付，0 已支付',
  `pay_sn` varchar(100) NOT NULL DEFAULT '0' COMMENT '支付单号',
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1937 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`ls_singer_info`
-- -----------------------------------------------------
-- 歌手信息
CREATE TABLE `ls_singer_info` (
  `singer_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `singer_name` varchar(100) NOT NULL DEFAULT '' COMMENT '歌手名',
  `singer_thum` varchar(100) NOT NULL DEFAULT '' COMMENT '歌手缩略图',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  PRIMARY KEY (`singer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`ls_music_info`
-- -----------------------------------------------------
-- 歌曲信息
CREATE TABLE `ls_music_info` (
  `music_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `music_name` varchar(100) NOT NULL DEFAULT '' COMMENT '歌手名',
  `singer_name` varchar(100) NOT NULL DEFAULT '' COMMENT '歌手缩略图',
  `singer_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '歌手编码',
  `music_price` int(11) NOT NULL DEFAULT '0' COMMENT '金额',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  PRIMARY KEY (`music_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


ALTER TABLE `rtshop`.`user_reward`
  ADD COLUMN `payment_no` VARCHAR(45) NOT NULL DEFAULT 0 COMMENT '微信订单号' AFTER `id_card`;

-- -----------------------------------------------------
-- Table `rtshop`.`rebate_ratio`
-- -----------------------------------------------------
--返利比例
CREATE TABLE `rebate_ratio` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '返利比例自增ID',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `one_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '一级返利',
  `two_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '二级返利',
  `area_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '区返利',
  `city_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '市返利',
  `province_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '省返利',
  `rt_level` tinyint(4) NOT NULL DEFAULT '0' COMMENT '龙蛙返利',
  `remark` varchar(100) NOT NULL DEFAULT '',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `ctime` bigint(20) NOT NULL DEFAULT '0',
  `mtime` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`user_coupon`
-- -----------------------------------------------------
--用户领取优惠券
CREATE TABLE `user_coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商户ID',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `coupon_time` bigint(15) NOT NULL DEFAULT '0' COMMENT '优惠券起始时间\n',
  `end_time` varchar(20) NOT NULL DEFAULT '' COMMENT '优惠券结束时间',
  `percentage` int(11) NOT NULL DEFAULT '0' COMMENT '折扣比例',
  `overdue_rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '过期状态\n0 正常\n1 过期\n2 已使用',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 \n0 正常\n1 删除',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `reach1` int(11) NOT NULL DEFAULT '0' COMMENT '满',
  `score1` int(11) NOT NULL DEFAULT '0' COMMENT '返',
  `coupon_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '优惠券类型',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `coupon_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '优惠券ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`comment`
-- -----------------------------------------------------
CREATE TABLE `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增',
  `comment_content` varchar(500) NOT NULL DEFAULT '' COMMENT '评论内容',
  `comment_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '评论类型\n1：评论\n2：买家秀\n3：二级评论\n4：是否点赞',
  `img_content` varchar(5000) NOT NULL DEFAULT '' COMMENT '图片json',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商家ID',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '对评论进行评论',
  `thumbs_up` bigint(20) NOT NULL DEFAULT '0' COMMENT '点赞数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COMMENT='评论'

-- -----------------------------------------------------
-- Table `rtshop`.`collection`
-- -----------------------------------------------------
CREATE TABLE `collection` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商家ID',
  `coupon_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '优惠券ID',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `collection_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '收藏分类\n1:商品\n2:商家\n3:优惠券',
  `price` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8 COMMENT='收藏'

-- -----------------------------------------------------
-- Table `rtshop`.`coupon`
-- -----------------------------------------------------
CREATE TABLE `coupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商家ID',
  `coupon_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '优惠券类型\n\n0=满减\n1=折扣\n2=立减',
  `coupon_time` bigint(15) NOT NULL DEFAULT '0' COMMENT '优惠券起始时间',
  `percentage` int(11) NOT NULL DEFAULT '0' COMMENT '优惠券折扣比例',
  `reach1` int(11) NOT NULL DEFAULT '0' COMMENT '满',
  `score1` int(11) NOT NULL DEFAULT '0' COMMENT '返',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `goods_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商品ID',
  `coupon_endtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '过期时间',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '立减价格',
  `company_type_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商家类型ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='折扣券'

-- -----------------------------------------------------
-- Table `rtshop`.`qrcode_record`
-- -----------------------------------------------------
CREATE TABLE `qrcode_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '推广次数',
  `buy_num` int(11) NOT NULL DEFAULT '0' COMMENT '购买次数',
  `buy_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '购买状态\n\n0：未购买\n1：以购买\n2：不限推广',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '购买价格',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='二维码 购买次数记录'

-- -----------------------------------------------------
-- Table `rtshop`.`goods_type_custom`
-- -----------------------------------------------------
CREATE TABLE `goods_type_custom` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商家ID',
  `name` varchar(100) NOT NULL DEFAULT '' COMMENT '分类名称',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COMMENT='商家店铺的商品分类自定义'


-- -----------------------------------------------------
-- Table `rtshop`.`user_historical_trace`
-- -----------------------------------------------------
CREATE TABLE `user_historical_trace` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '房间编码，即店铺companyId',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `company_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '店铺名称',
  `company_background` varchar(500) NOT NULL DEFAULT '' COMMENT '聊天背景',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_room_UNIQUE` (`user_id`,`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2730 DEFAULT CHARSET=utf8 COMMENT='用户历史足迹表'

-- -----------------------------------------------------
-- Table `rtshop`.`bank_card_bag`
-- -----------------------------------------------------
CREATE TABLE `bank_card_bag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bank_id` varchar(45) NOT NULL DEFAULT '' COMMENT '银行卡账号',
  `bank_name` varchar(100) NOT NULL DEFAULT '' COMMENT '开户行名称',
  `bank_user_phone` varchar(45) NOT NULL DEFAULT '' COMMENT '银行预留手机号',
  `bank_user_idcard` varchar(100) NOT NULL DEFAULT '' COMMENT '开户人身份证',
  `bank_user_name` varchar(45) NOT NULL DEFAULT '' COMMENT '开户人姓名',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '归属user',
  `remark` varchar(500) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态\n\n0:正常\n1:删除',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `bank_abbreviation` varchar(45) NOT NULL DEFAULT '' COMMENT '开户行缩写简称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='银行卡包'

-- -----------------------------------------------------
-- Table `rtshop`.`historical_record`
-- -----------------------------------------------------
CREATE TABLE `historical_record` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `user_id` bigint(15) NOT NULL DEFAULT '0' COMMENT '用户',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `history_name` varchar(100) NOT NULL DEFAULT '' COMMENT '搜索历史name',
  `history_type` varchar(100) NOT NULL DEFAULT '' COMMENT '搜索区域',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='搜索历史'

-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_pwd`
-- -----------------------------------------------------
CREATE TABLE `t_sys_pwd` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `user_pwd` varchar(512) NOT NULL DEFAULT '' COMMENT '用户交易密码',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `remark` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户交易密码'

-- -----------------------------------------------------
-- Table `rtshop`.`receiving_address`
-- -----------------------------------------------------
CREATE TABLE `receiving_address` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户ID',
  `address_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '收货地址状态\n\n0：正常收货地址\n1：默认收货地址',
  `province` varchar(225) NOT NULL DEFAULT '' COMMENT '省',
  `city` varchar(225) NOT NULL DEFAULT '' COMMENT '市',
  `district` varchar(225) NOT NULL DEFAULT '' COMMENT '区',
  `district_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '地区级联所属ID',
  `address` varchar(500) NOT NULL DEFAULT '' COMMENT '收货详细地址',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `remark` varchar(500) NOT NULL DEFAULT '',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='收货地址'

-- -----------------------------------------------------
-- Table `rtshop`.`shopping_order`
-- -----------------------------------------------------
CREATE TABLE `shopping_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '订单ID',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '商户ID',
  `company_name` varchar(500) NOT NULL DEFAULT '' COMMENT '商户名称',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '订单总价',
  `favorable_price` int(11) NOT NULL DEFAULT '0' COMMENT '满减券优惠价格',
  `discount_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '满减券优惠ID',
  `ctime` bigint(20) NOT NULL DEFAULT '0',
  `mtime` bigint(20) NOT NULL DEFAULT '0',
  `remark` varchar(500) NOT NULL DEFAULT '',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `company_headImg` varchar(500) NOT NULL DEFAULT '' COMMENT '商户logo',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='购物订单'

-- -----------------------------------------------------
-- Table `rtshop`.`company_industry`
-- -----------------------------------------------------
CREATE TABLE `company_industry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `remark` varchar(100) NOT NULL DEFAULT '',
  `company_commission` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`company_profit`
-- -----------------------------------------------------
CREATE TABLE `company_profit` (
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '订单金额',
  `order_id` bigint(15) NOT NULL DEFAULT '0',
  `company_profit` int(11) NOT NULL DEFAULT '0' COMMENT '收益金额',
  `consumer_id` bigint(15) NOT NULL DEFAULT '0',
  `order_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-2 退款\n-1 提现\n0 商\n1 地面店扫码订单返利\n2 商城微商订单返利\n3 地面店扫码订单返积分\n4 商城微商订单返积分\n5 点餐送积分\n6 点餐奖励\n7 快转奖励\n8 快省奖励\n9 快签奖励\n10 快飞奖励\n11 快推奖励\n12 快点奖励\n13 快销奖励\n14 快赚奖励',
  `profit_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `order_sn` varchar(45) NOT NULL DEFAULT '0' COMMENT '订单唯一编号',
  `company_balance` int(11) NOT NULL DEFAULT '0' COMMENT '个人账号余额',
  PRIMARY KEY (`profit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8 COMMENT='店铺收益'

-- -----------------------------------------------------
-- Table `rtshop`.`company_type`
-- -----------------------------------------------------
CREATE TABLE `company_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `Industry_id` bigint(20) NOT NULL DEFAULT '0',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0',
  `ctime` bigint(15) NOT NULL DEFAULT '0',
  `mtime` bigint(15) NOT NULL DEFAULT '0',
  `remark` varchar(100) NOT NULL DEFAULT '',
  `img` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_bookkeeping`
-- -----------------------------------------------------
CREATE TABLE `dc_bookkeeping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `item` varchar(200) NOT NULL DEFAULT '' COMMENT '促销活动内容',
  `time` varchar(20) NOT NULL DEFAULT '' COMMENT '记账时间',
  `unit_price` int(11) NOT NULL DEFAULT '0' COMMENT '单价',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT '数量',
  `actual_price` int(11) NOT NULL DEFAULT '0' COMMENT '实际价格',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_category`
-- -----------------------------------------------------
CREATE TABLE `dc_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '菜品名称',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=727 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_company_comment`
-- -----------------------------------------------------
CREATE TABLE `dc_company_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '点餐订单编码',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '评分，满分10分，每个星2分',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '评论内容',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '餐厅编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_dish`
-- -----------------------------------------------------
CREATE TABLE `dc_dish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '菜名',
  `thum` varchar(200) NOT NULL DEFAULT '' COMMENT '图片',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜品编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `is_tuijian` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 不推荐 1 推荐',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=975 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_dish_comment`
-- -----------------------------------------------------
CREATE TABLE `dc_dish_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `dish_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜品编码',
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '点餐订单编码',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `dish_name` varchar(200) NOT NULL DEFAULT '' COMMENT '菜名',
  `content` varchar(500) NOT NULL DEFAULT '' COMMENT '评论内容',
  `up_down` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 赞\n1 踩',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_flavor`
-- -----------------------------------------------------
CREATE TABLE `dc_flavor` (
  `flavor_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `flavor` varchar(100) NOT NULL DEFAULT '' COMMENT '口味',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `dish_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '单品编码',
  `flavor_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '属性组类型',
  PRIMARY KEY (`flavor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_order`
-- -----------------------------------------------------
CREATE TABLE `dc_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `order_sn` varchar(50) NOT NULL DEFAULT '' COMMENT '订单号',
  `day` varchar(10) NOT NULL DEFAULT '' COMMENT '天',
  `day_index` int(11) NOT NULL DEFAULT '0' COMMENT '每天订单流水号',
  `amount` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '赠送积分',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `pay_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付状态',
  `pay_way` tinyint(4) NOT NULL DEFAULT '0' COMMENT '支付方式',
  `order_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态',
  `table_id` bigint(20) NOT NULL DEFAULT '0',
  `serving_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '上菜时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_order_prod`
-- -----------------------------------------------------
CREATE TABLE `dc_order_prod` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '菜名',
  `thum` varchar(200) NOT NULL DEFAULT '' COMMENT '图片',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜品编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `order_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '点餐订单编码',
  `num` int(11) NOT NULL DEFAULT '0',
  `dish_id` bigint(20) NOT NULL DEFAULT '0',
  `is_print` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否打印 0 否，1 打印',
  `flavors` varchar(255) NOT NULL DEFAULT '' COMMENT '口味',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_prepare_dish`
-- -----------------------------------------------------
CREATE TABLE `dc_prepare_dish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `dish_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜品编码',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '菜名',
  `flavors` varchar(200) NOT NULL DEFAULT '' COMMENT '口味',
  `thum` varchar(200) NOT NULL DEFAULT '' COMMENT '图片',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '价格',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '份数',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `category_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜品编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `is_tuijian` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 不推荐 1 推荐',
  `table_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_promotions`
-- -----------------------------------------------------
CREATE TABLE `dc_promotions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `content` varchar(2000) NOT NULL DEFAULT '' COMMENT '促销活动内容',
  `start_time` varchar(20) NOT NULL DEFAULT '' COMMENT '开始时间',
  `end_time` varchar(20) NOT NULL DEFAULT '' COMMENT '结束时间',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '活动内容 0 回馈管理区；1 支付区优惠活动',
  `name` varchar(200) NOT NULL DEFAULT '0' COMMENT '活动名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_queue`
-- -----------------------------------------------------
CREATE TABLE `dc_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '就餐人数',
  `day` varchar(10) NOT NULL DEFAULT '' COMMENT '天',
  `day_index` int(11) NOT NULL DEFAULT '0' COMMENT '每天排号流水号',
  `table_type_id` bigint(20) NOT NULL DEFAULT '0',
  `table_type_name` varchar(45) NOT NULL DEFAULT '0',
  `notify_count` int(11) NOT NULL DEFAULT '0' COMMENT '微信通知用户次数',
  `queuing` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否需要排队0 不需要， 1 需要',
  `table_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '分配的桌子编号',
  `table_type_max_num` int(11) NOT NULL DEFAULT '0' COMMENT '桌子最大容量',
  `table_name` varchar(45) NOT NULL DEFAULT '' COMMENT '桌号名称',
  `delay_time` bigint(20) NOT NULL DEFAULT '0' COMMENT '延后上菜时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_restaurant`
-- -----------------------------------------------------
CREATE TABLE `dc_restaurant` (
  `company_id` bigint(20) NOT NULL COMMENT '公司编码',
  `about_us` varchar(3000) NOT NULL DEFAULT '0' COMMENT '关于我们',
  `brand_story` varchar(3000) NOT NULL DEFAULT '0' COMMENT '品牌故事',
  `venture_history` varchar(3000) NOT NULL DEFAULT '0' COMMENT '创业历程',
  `food_philosophy` varchar(3000) NOT NULL DEFAULT '0' COMMENT '美食哲学',
  `shop_specialty` varchar(3000) NOT NULL DEFAULT '0' COMMENT '本店特色',
  `dinners_culture` varchar(3000) NOT NULL DEFAULT '0' COMMENT '食客文化',
  `customers_feedback` varchar(3000) NOT NULL DEFAULT '0' COMMENT '食客回馈',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `company_id_UNIQUE` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_table`
-- -----------------------------------------------------
CREATE TABLE `dc_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '名称',
  `qr_url` varchar(200) NOT NULL DEFAULT '' COMMENT '二维码地址',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `table_type_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '菜品编码',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `busy` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 可用， 1 使用中',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_table_type`
-- -----------------------------------------------------
CREATE TABLE `dc_table_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `name` varchar(200) NOT NULL DEFAULT '' COMMENT '名称',
  `num_min` int(11) NOT NULL DEFAULT '0' COMMENT '最少人数',
  `num_max` int(11) NOT NULL DEFAULT '0' COMMENT '最多人数',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`dc_user_award`
-- -----------------------------------------------------
CREATE TABLE `dc_user_award` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `award_name` varchar(200) NOT NULL DEFAULT '' COMMENT '礼物名称',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `company_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '公司编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`file_upload_image`
-- -----------------------------------------------------
CREATE TABLE `file_upload_image` (
  `image_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增编码',
  `image_md5` varchar(32) NOT NULL DEFAULT '' COMMENT '图片md5',
  `image_url` varchar(100) NOT NULL DEFAULT '' COMMENT '图片url',
  `ctime` bigint(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2392 DEFAULT CHARSET=utf8

-- -----------------------------------------------------
-- Table `rtshop`.`file_upload_image`
-- -----------------------------------------------------
CREATE TABLE `user_online` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `user_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '用户编码',
  `room_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '房间编码，即店铺companyId',
  `remark` varchar(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` bigint(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` bigint(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  `last_ip` varchar(45) NOT NULL DEFAULT '' COMMENT '最后登录IP',
  `headimgurl` varchar(500) NOT NULL DEFAULT '',
  `nickname` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '昵称',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_room_UNIQUE` (`user_id`,`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19513 DEFAULT CHARSET=utf8 COMMENT='在线用户表'

-- -----------------------------------------------------
-- Table `rtshop`.`activity_user_link`
-- -----------------------------------------------------
CREATE TABLE `rtshop`.`activity_user_link` (
  `user_link_id` BIGINT(20) NOT NULL COMMENT '主键',
  `user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '用户id',
  `activity_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '文章id',
  `link_type` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '状态：\n1、粉丝。\n2、阅读量。',
  `remark` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` BIGINT(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` BIGINT(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`user_link_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '用户和文章的粉丝和阅读量的关系表';

ALTER TABLE `rtshop`.`activity_user_link`
CHANGE COLUMN `link_type` `link_type` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态：\n1、粉丝。\n2、阅读量。' ,
ADD COLUMN `author` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '作者' AFTER `activity_id`;

ALTER TABLE `rtshop`.`t_sys_user`
ADD COLUMN `wechat_photo` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '微信照片' AFTER `wechat_id`;

ALTER TABLE `rtshop`.`t_sys_user`
ADD COLUMN `background_photo` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '背景图' AFTER `subscribe`;
ALTER TABLE `rtshop`.`t_sys_user`
CHANGE COLUMN `background_photo` `background_photo` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '背景图' AFTER `wechat_photo`;

ALTER TABLE `rtshop`.`t_sys_user`
ADD COLUMN `qq_number` VARCHAR(45) NOT NULL DEFAULT '0' COMMENT 'qq号码' AFTER `background_photo`,
ADD COLUMN `e_mail` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '邮箱' AFTER `qq_number`;

ALTER TABLE `rtshop`.`t_sys_user`
DROP COLUMN `e_mail`;

ALTER TABLE `rtshop`.`t_sys_user`
ADD COLUMN `company_name` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '公司名称' AFTER `qq_number`,
ADD COLUMN `company_adress` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '公司地址' AFTER `company_name`,
ADD COLUMN `comany_describe` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '公司简介' AFTER `company_adress`;

-- -----------------------------------------------------
-- Table `rtshop`.`t_sys_user_company`
-- -----------------------------------------------------
CREATE TABLE `rtshop`.`t_sys_user_company` (
  `id` BIGINT(20) NOT NULL,
  `company_name` VARCHAR(45) NOT NULL DEFAULT '',
  `company_address` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '公司地址',
  `comany_describe` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '公司简介',
  `remark` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` BIGINT(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` BIGINT(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '公司';

ALTER TABLE `rtshop`.`t_sys_user`
DROP COLUMN `comany_describe`,
DROP COLUMN `company_adress`,
DROP COLUMN `company_name`;

-- -----------------------------------------------------
-- Table `rtshop`.`goods_recommend`
-- -----------------------------------------------------
CREATE TABLE `rtshop`.`goods_recommend` (
  `id` BIGINT(20) NOT NULL,
  `user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '用户id',
  `goods_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '商品id',
  `remark` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '备注',
  `ctime` BIGINT(15) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` BIGINT(15) NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '状态 0:正常 1:删除',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '微传媒推荐排序';

ALTER TABLE `rtshop`.`t_sys_user_company`
ADD COLUMN `user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '用户id' AFTER `id`;

ALTER TABLE `rtshop`.`t_sys_user_company`
CHANGE COLUMN `company_name` `company_name` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '公司名称' ;

ALTER TABLE `rtshop`.`t_sys_user_company`
CHANGE COLUMN `id` `company_id` BIGINT(20) NOT NULL ;

ALTER TABLE `rtshop`.`goods_recommend`
CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `rtshop`.`activity`
ADD COLUMN `activity_read` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '阅读量' AFTER `activity_author`;

ALTER TABLE `rtshop`.`t_sys_user_company`
CHANGE COLUMN `company_id` `company_id` BIGINT(20) NOT NULL AUTO_INCREMENT ;

ALTER TABLE `rtshop`.`goods_recommend`
CHANGE COLUMN `user_id` `company_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '公司id' ;

CREATE TABLE `rtshop`.`user_msg` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `from_user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '发送者id',
  `from_user_name` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '发送者昵称',
  `from_user_photo` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '发送者头像',
  `to_user_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '接收者id',
  `to_user_name` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '接收者昵称',
  `to_user_photo` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '接收者头像',
  `msg_content` VARCHAR(2000) NOT NULL DEFAULT '' COMMENT '互动内容',
  `ctime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `mtime` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '最后编辑时间',
  `remark` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '备注',
  `rstatus` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '0 正常\n1 删除',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COMMENT = '1对1聊天消息';

ALTER TABLE `rtshop`.`bank_card_bag`
  ADD COLUMN `bank_branch` VARCHAR(45) NOT NULL DEFAULT '' COMMENT '开户行支行' AFTER `bank_abbreviation`;

ALTER TABLE `rtshop`.`user_msg`
  ADD COLUMN `company_id` BIGINT(20) NOT NULL DEFAULT '0' COMMENT '商家id' AFTER `rstatus`;