package com.ruitaowang.core.domain;

public class UserHistoricalTrace extends Page {
    /**
     *  自增主键,所属表字段为user_historical_trace.id
     */
    private Long id;

    /**
     *  用户编码,所属表字段为user_historical_trace.user_id
     */
    private Long userId;

    /**
     *  房间编码，即店铺companyId,所属表字段为user_historical_trace.company_id
     */
    private Long companyId;

    /**
     *  备注,所属表字段为user_historical_trace.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为user_historical_trace.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为user_historical_trace.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为user_historical_trace.rstatus
     */
    private Byte rstatus;

    /**
     *  店铺名称,所属表字段为user_historical_trace.company_name
     */
    private String companyName;

    /**
     *  ,所属表字段为user_historical_trace.company_background
     */
    private String companyBackground;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getCompanyBackground() {
        return companyBackground;
    }

    public void setCompanyBackground(String companyBackground) {
        this.companyBackground = companyBackground == null ? null : companyBackground.trim();
    }
}