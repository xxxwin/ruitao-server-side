package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserMsg;

import java.util.List;

public interface UserMsgAdMapper {

    long count(UserMsg userMsg);

    List<UserMsg> select(UserMsg userMsg);
}