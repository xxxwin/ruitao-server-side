package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Relations;
import java.util.List;

public interface RelationsMapper {
    int insert(Relations relations);

    int deleteByPK(Long uesrId);

    int updateByPK(Relations relations);

    Relations selectByPK(Long uesrId);

    long count(Relations relations);

    List<Relations> select(Relations relations);
}