package com.ruitaowang.goods.utils;

public class RStatus {
    //未删除状态
    public static final byte Exist = 0;
    //已删除
    public static final byte Deleted = 1;
}
