/**
 * Created by Shaka on 12/21/16.
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
var Config = {
    selectUrl: '/api/admin/dc/categories',
    deleteUrl: '/api/admin/dc/categories',
    updateUrlForCellEdit: '/api/admin/company/cellEdit',
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#dc_category").jqGrid({
        datatype: "json",
        postData:{companyId:parseInt(getQueryString("id"))},
        url: Config.selectUrl,
        height: 550,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["分类ID", "分类名称", "备注", "时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            sorttype: "int",
            search: true
        }, {
            name: "name",
            index: "name",
        }, {
            name: "remark",
            index: "remark",
            autowidth: true
        },{
            name: "ctime",
            index: "ctime",
            autowidth: true,
            sorttype: "int"
        }, {
            name: "operate",
            index: "operate",
            autowidth: true,
            sortable: false
        }],
        pager: "#pager_dc_category",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#dc_category").jqGrid('navGrid', '#pager_dc_category', {add: false, edit: false, del: false,search: false,
        refresh: false});
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#dc_category").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#dc_category").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#dc_category").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#dc_category").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#dc_category').jqGrid ('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/admin/dc/category/update/'+ id +'" data-index="0">修改</a>';
        jQuery("#dc_category").jqGrid('setRowData', ids[i], {operate: html,ctime:dataFromTheRow.ctime});
    }
}
