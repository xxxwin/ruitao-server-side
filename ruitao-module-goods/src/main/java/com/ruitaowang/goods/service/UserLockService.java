package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserLock;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserLockMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 1/19/2017.
 */
@Service
public class UserLockService implements BaseService<UserLock> {
    @Autowired
    private UserLockMapper userLockMapper;

    @Override
    public UserLock insert(UserLock userLock) {
        Long time = System.currentTimeMillis();
        userLock.setCtime(time);
        userLock.setMtime(time);
        userLockMapper.insert(userLock);
        return userLock;
    }

    @Override
    public int delete(Long id) {
        UserLock userLock = new UserLock();
        userLock.setUserId(id);
        userLock.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userLock.setMtime(time);
        return userLockMapper.updateByPK(userLock);
    }

    @Override
    public UserLock update(UserLock userLock) {
        Long time = System.currentTimeMillis();
        userLock.setMtime(time);
        userLockMapper.updateByPK(userLock);
        return userLock;
    }

    @Override
    public UserLock selectByPK(Long id) {
        return userLockMapper.selectByPK(id);
    }

    @Override
    public List<UserLock> select(UserLock userLock) {
        userLock.setRstatus((byte) 0);
        return userLockMapper.select(userLock);
    }

    public List<UserLock> selectForPage(UserLock userLock) {
        userLock.setRstatus((byte) 0);
        userLock.setRecords(userLockMapper.count(userLock));
        return userLockMapper.select(userLock);
    }

}
