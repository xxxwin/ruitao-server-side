
var Config = {
    selectCompanyUrl: '/api/providers',
    submitCreate:'/api/ws/goodsTypeCustom',

};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$().ready(function () {
    $.ajax(
        {
            url: Config.selectCompanyUrl,
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                var categories = [];
                categories.push('<option value="0" hassubinfo="true">选择商户</option>');
                data.data.map(function (company) {
                    categories.push('<option value="');
                    categories.push(company.companyId);
                    categories.push('" ');
                    categories.push('hassubinfo="true">');
                    categories.push(company.companyName);
                    categories.push('</option>');
                });
                $("#companyId").html(categories.join(''));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown, textStatus);
            }
        });
    jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '0');
    }, "请选择分类");

    $("#submitCreate").click(function () {
        $("#ajaxform").validate({
            rules: {
                companyId: {
                    selectcheck: true
                }
            }
        });
        if ($('#ajaxform').valid()) {
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url: Config.submitCreate,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    success: function (data, textStatus, jqXHR) {
                        toastr.success(data.msg, data.title);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
    });
});

