package com.ruitaowang.admin.view.api;

import com.ruitaowang.core.domain.ReceivingAddress;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.ReceivingAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class APIReceivingAddressController extends BaseController {

    @Autowired
    private ReceivingAddressService receivingAddressService;


//    @RequestMapping(value = "/api/wap/receivingAddress", method = RequestMethod.GET)
//    public String list(ReceivingAddress receivingAddress, int page, @RequestHeader(value = "headId", required = false) String userId) {
//        if (ObjectUtils.isEmpty(userId)) {
//            return this.renderJson(MsgOut.error("Plz login."));
//        }
//        receivingAddress.setPage(page);
//        receivingAddress.setUserId(Long.parseLong(userId));
//        List<ReceivingAddress> receivingAddresses = receivingAddressService.selectForPage(receivingAddress);
//        MsgOut o = MsgOut.success(receivingAddresses);
//        return this.renderJson(o);
//    }

    @RequestMapping(value = "/api/wap/receivingAddress", method = RequestMethod.GET)
    public String list(ReceivingAddress receivingAddress, @RequestHeader(value = "headId", required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        receivingAddress.setUserId(Long.parseLong(userId));
        List<ReceivingAddress> receivingAddresses = receivingAddressService.selectDesc(receivingAddress);
//        String sign = "no hava";
//        for(ReceivingAddress r : receivingAddresses){
//            if(r.getAddressType() == 1){
//                sign = "hava";
//            }
//        }
//        if(sign.equals("no hava") && !ObjectUtils.isEmpty(receivingAddresses)){
//            List<ReceivingAddress> receivingAddresses1 = receivingAddressService.selectByMtime(receivingAddress);
//            ReceivingAddress receivingAddress1 = receivingAddresses1.get(0);
//            receivingAddress1.setUserId(Long.parseLong(userId));
//            receivingAddress1.setAddressType((byte)1);
//            receivingAddressService.update(receivingAddress1);
//        }
        MsgOut o = MsgOut.success(receivingAddresses);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/receivingAddressById", method = RequestMethod.GET)
    public String selectById(Long id, @RequestHeader(value = "headId", required = false) String userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        ReceivingAddress receivingAddressList = receivingAddressService.selectByPK(id);
        MsgOut o = MsgOut.success(receivingAddressList);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/receivingAddress", method = RequestMethod.POST)
    public String create(ReceivingAddress receivingAddress, @RequestHeader(value = "headId", required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        receivingAddress.setUserId(Long.parseLong(userId));
        ReceivingAddress receivingAddress1 = new ReceivingAddress();
        receivingAddress1.setUserId(Long.parseLong(userId));
        receivingAddress1.setAddressType((byte)1);
        List<ReceivingAddress> receivingAddressList = receivingAddressService.select(receivingAddress1);
        receivingAddressService.insert(receivingAddress);
        if (!ObjectUtils.isEmpty(receivingAddressList) && receivingAddress.getAddressType() == 1) {
            Long id = receivingAddressList.get(0).getId();
            receivingAddress1.setId(id);
            receivingAddress1.setAddressType((byte)0);
            receivingAddressService.update(receivingAddress1);
        }
        MsgOut o = MsgOut.success("操作成功了！");
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/receivingAddress", method = RequestMethod.PUT)
    public String update(ReceivingAddress receivingAddress, @RequestHeader(value = "headId", required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if(receivingAddress.getAddressType() == 1){
            ReceivingAddress receivingAddress1 = new ReceivingAddress();
            receivingAddress1.setUserId(Long.parseLong(userId));
            receivingAddress1.setAddressType((byte)1);
            List<ReceivingAddress> receivingAddressList = receivingAddressService.select(receivingAddress1);
            if (!ObjectUtils.isEmpty(receivingAddressList)) {
                Long id = receivingAddressList.get(0).getId();
                receivingAddress1.setId(id);
                receivingAddress1.setAddressType((byte)0);
                receivingAddressService.update(receivingAddress1);
            }
        }
        receivingAddress.setUserId(Long.parseLong(userId));
        receivingAddressService.update(receivingAddress);
        MsgOut o = MsgOut.success(receivingAddress);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/wap/receivingAddress/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id, @RequestHeader(value = "headId", required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        receivingAddressService.delete(id);
        ReceivingAddress receivingAddress = new ReceivingAddress();
        receivingAddress.setUserId(Long.parseLong(userId));
        List<ReceivingAddress> receivingAddresses = receivingAddressService.select(receivingAddress);
        String sign = "no hava";
        for(ReceivingAddress r : receivingAddresses){
            if(r.getAddressType() == 1){
                sign = "hava";
            }
        }
        if(sign.equals("no hava") && !ObjectUtils.isEmpty(receivingAddresses)){
            List<ReceivingAddress> receivingAddresses1 = receivingAddressService.selectByMtime(receivingAddress);
            ReceivingAddress receivingAddress1 = receivingAddresses1.get(0);
            receivingAddress1.setUserId(Long.parseLong(userId));
            receivingAddress1.setAddressType((byte)1);
            receivingAddressService.update(receivingAddress1);
        }
        MsgOut o = MsgOut.success("操作成功了！");
        return this.renderJson(o);
    }
}
