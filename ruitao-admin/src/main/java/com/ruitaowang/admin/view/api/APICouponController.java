package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.CompanyTypeService;
import com.ruitaowang.goods.service.CouponService;
import com.ruitaowang.goods.service.GoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class APICouponController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private CouponService couponService;
    @Autowired
    private CompanyTypeService companyTypeService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private SysUserService sysUserService;

    @RequestMapping("/api/ws/coupon/page")
    public String wsListForPage(Coupon coupon, int page) {
        coupon.setPage(page);
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(coupon.getSidx())) {
            coupon.setOrderBy(coupon.getSidx() + " " + coupon.getSord() + "," + coupon.getOrderBy());
        }
        List<CouponVO> couponVOS = new ArrayList<>();
        List<Coupon> coupons = couponService.selectForPage(coupon);
        for(Coupon c : coupons){
            CouponVO couponVO = new CouponVO();
            BeanUtils.copyProperties(c,couponVO);
            if (!ObjectUtils.isEmpty(c.getGoodsId()) && c.getGoodsId()!= 0){
                couponVO.setGoodsName(goodsService.selectByPK(c.getGoodsId()).getGoodsName());
            }
            if(!ObjectUtils.isEmpty(c.getCompanyId())  && c.getCompanyId()!= 0){
                couponVO.setCompanyName(companyService.selectByPK(c.getCompanyId()).getCompanyName());
            }
            couponVOS.add(couponVO);
        }
        MsgOut o = MsgOut.success(couponVOS);
        o.setRecords(coupon.getRecords());
        o.setTotal(coupon.getTotal());
        return this.renderJson(o);
    }


    @RequestMapping("/api/admin/coupon/page")
    public String adminCouponForPage(Coupon coupon, int page) {
        coupon.setPage(page);
        if (!ObjectUtils.isEmpty(coupon.getSidx())) {
            coupon.setOrderBy(coupon.getSidx() + " " + coupon.getSord() + "," + coupon.getOrderBy());
        }
        List<CouponVO> couponVOS=new ArrayList<>();
        List<Coupon> coupons=couponService.selectForPage(coupon);
        for (Coupon c:coupons){
            CouponVO couponVO=new CouponVO();
            BeanUtils.copyProperties(c,couponVO);
            if (!ObjectUtils.isEmpty(c.getGoodsId()) && c.getGoodsId()!= 0){
                couponVO.setGoodsName(goodsService.selectByPK(c.getGoodsId()).getGoodsName());
            }
            if (!ObjectUtils.isEmpty(c.getCompanyId())  && c.getCompanyId()!= 0){
                couponVO.setCompanyName(companyService.selectByPK(c.getCompanyId()).getCompanyName());
            }
            couponVOS.add(couponVO);
        }
        MsgOut o = MsgOut.success(couponVOS);
        o.setRecords(coupon.getRecords());
        o.setTotal(coupon.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/coupon", method = RequestMethod.POST)
    public String adminCreate(Coupon coupon) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        //一个商家的每个商品的优惠券  一种类型只能创建一个
//        Coupon c=new Coupon();
//        c.setGoodsId(coupon.getGoodsId());
//        c.setCompanyId(coupon.getCompanyId());
//        c.setCouponType(coupon.getCouponType());
//        List<Coupon> coupons=couponService.select(c);
//        if (!ObjectUtils.isEmpty(coupons)){
//            return this.renderJson(MsgOut.error("该商品以创建该类型优惠券"));
//        }
        couponService.insert(coupon);
        MsgOut o = MsgOut.success(coupon);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/ws/coupon", method = RequestMethod.POST)
    public String wsCreate(Coupon coupon) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        //一个商家的每个商品的优惠券  一种类型只能创建一个
//        Coupon c=new Coupon();
//        c.setGoodsId(coupon.getGoodsId());
//        c.setCompanyId(coupon.getCompanyId());
//        c.setCouponType(coupon.getCouponType());
//        List<Coupon> coupons=couponService.select(c);
//        if (!ObjectUtils.isEmpty(coupons)){
//            return this.renderJson(MsgOut.error("该商品以创建该类型优惠券"));
//        }
        couponService.insert(coupon);
        MsgOut o = MsgOut.success(coupon);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/coupon", method = RequestMethod.PUT)
    public String adminUpdate(Coupon coupon) {
        MsgOut o = MsgOut.success(couponService.update(coupon));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/ws/coupon", method = RequestMethod.PUT)
    public String wsUpdate(Coupon coupon) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        couponService.update(coupon);
        MsgOut o = MsgOut.success(coupon);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/coupon/{id}", method = RequestMethod.DELETE)
    public String adminDelete(@PathVariable("id") Long id) {
        MsgOut o = MsgOut.success(couponService.delete(id));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/ws/coupon/{id}", method = RequestMethod.DELETE)
    public String wsDelete(@PathVariable("id") Long id) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(couponService.delete(id));
        return this.renderJson(o);
    }
}
