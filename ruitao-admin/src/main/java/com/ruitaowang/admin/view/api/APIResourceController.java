package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.service.ResourceService;
import com.ruitaowang.core.domain.Resource;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 菜单相关
 */
@RestController
public class APIResourceController extends BaseController {

    @Autowired
    private ResourceService resourceService;

    /**
     *
     * @api {get} /api/resources 获取菜单列表
     * @apiSampleRequest /api/resources
     * @apiExample {curl} Example usage:
     *     curl -i http://localhost:8080/api/resources
     * @apiPermission admin
     *
     * @apiName list
     * @apiGroup Menu
     * @apiVersion 0.1.0
     * @apiDescription 当前登录用户拥有的菜单
     *
     * @apiParam   {int}   page    页码
     * @apiParam   {int}   rows    页大小
     *
     * @apiSuccess {String} code    结果码
     * @apiSuccess {String} msg     消息说明
     * @apiSuccess {String} type    结果类型
     * @apiSuccess {String} title   提示标题
     * @apiSuccess {Object} data    分页数据
     * @apiSuccess {int}    total   总记录数
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     code: '200',
     *     msg:  'success',
     *     total: 1,
     *     data:  {},
     *     type:  'SUCCESS',
     *     title: '成功'
     * }
     *
     * @apiError Menu 对应<code>ID</code>的菜单没有数据
     * @apiErrorExample Error-Response:
     * HTTP/1.1 404 Not Found
     * {
     *     code: '404',
     *     msg:  'User Not Found',
     *     type: 'ERROR',
     *     title: '错误'
     * }
     */
    @RequestMapping(value = "/api/resources", method = RequestMethod.GET)
    public String list() {
        List<Resource> list = resourceService.select(new Resource());
        MsgOut o = MsgOut.success(list);
        return this.renderJson(o);
    }

    /**
     *
     * @api {post} /api/resources 创建新菜单
     * @apiName  create
     * @apiHeader {String} access-key Users unique access-key.
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "Accept-Encoding": "Accept-Encoding: gzip, deflate"
     *     }
     * @apiGroup Menu
     * @apiVersion 0.1.0
     * @apiDescription 创建一个新菜单
     *
     * @apiParam   {String}  name   名称
     * @apiParam   {String}  link   菜单url
     * @apiParam   {Long}    pid    父级菜单ID
     *
     * @apiSuccess {String} code    结果码
     * @apiSuccess {String} msg     消息说明
     * @apiSuccess {String} type    结果类型
     * @apiSuccess {String} title   提示标题
     * @apiSuccess {Object} data    分页数据
     * @apiSuccess {int}    total   总记录数
     *
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     code: '200',
     *     msg:  'success',
     *     total: 1,
     *     data:  {},
     *     type:  'SUCCESS',
     *     title: '成功'
     * }
     *
     * @apiError Menu 对应<code>ID</code>的菜单没有数据
     * @apiErrorExample Error-Response:
     * HTTP/1.1 404 Not Found
     * {
     *     code: '404',
     *     msg:  'User Not Found',
     *     type: 'ERROR',
     *     title: '错误'
     *
     * }
     */
    @RequestMapping(value = "/api/resources", method = RequestMethod.POST)
//    @RolesAllowed({"ROLE_menus:create", "ROLE_root"})
    public String create(Resource resource) {
        resource = resourceService.insert(resource);
        return this.renderJson(MsgOut.success(resource));
    }

    @RequestMapping(value = "/api/resources", method = RequestMethod.PUT)
//    @RolesAllowed({"ROLE_menus:update", "ROLE_root"})
    public String update(Resource resource) {//@Valid
        resourceService.update(resource);
        return this.renderJson(MsgOut.success(resource));
    }
//    @RolesAllowed({"ROLE_menus:delete", "ROLE_root"})
    @RequestMapping(value = "/api/resources/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id) {
        resourceService.delete(id);
        return this.renderJson(MsgOut.success());
    }

    @RequestMapping(value = "/api/resources/{roleId}", method = RequestMethod.GET)
//    @RolesAllowed({"ROLE_menus:view", "ROLE_root"})
    public String getMenuByRoleId(@PathVariable("roleId") Long id) {
        List<Resource> list;
        MsgOut o;
        list = resourceService.selectByRoleId(id);
        o = MsgOut.success(list);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/resources/grant")
//    @RolesAllowed({"ROLE_menus:grant", "ROLE_root"})
    public String grant(Long roleId, String mids) {
        MsgOut o;
        resourceService.grant(roleId, mids);
        o = MsgOut.success();
        return this.renderJson(o);
    }
}
