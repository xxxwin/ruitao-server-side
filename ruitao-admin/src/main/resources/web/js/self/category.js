/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/admin/categories/post',
    updateUrl: '/admin/categories/update',
    deleteUrl: '/admin/categories/delete',
    selectUrl: '/admin/categories/getList'
};
$(document).ready(function () {
    jqGrid()

});
function jqGrid() {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#goods_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "分类名称","创建时间", "操作"],
        colModel: [{
            name: "categoryId",
            index: "categoryId",
            editable: false,
            width: 10,
            sorttype: "int",
            search: true
        }, {
            name: "categoryName",
            index: "categoryName",
            editable: true,
            width: 60
        }, {
            name: "ctime",
            index: "ctime",
            editable: true,
            width: 20
        }, {
            name: "operate",
            index: "operate",
            width: 20,
            sortable: false
        }],
        pager: "#pager_goods_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "categoryId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            //wrapper operate
            dataWrapper();
        }
    });
    jQuery("#goods_list").jqGrid('navGrid', '#pager_goods_list', {
        add: false,
        edit: false,
        del: false,
        search: false
    });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#goods_list").setGridWidth(width);});

    // $.ajax(
    //     {
    //         url: Config.selectUrl,
    //         type: "GET",
    //         dataType: "json",
    //         async: false,
    //         success: function (data, textStatus, jqXHR) {
    //
    //             //add operate for data
    //             if(data && data.data && data.data.length>0){
    //                 for (var i=0;i<data.data.length;i++){
    //                     var id=data.data[i].categoryId;
    //                     data.data[i].id=data.data[i].categoryId;
    //                     var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/goodsCategory/update/'+id+'" data-index="0">编辑</a>';
    //                     html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + ","+  (i+1) +")' >删除</a>";
    //                     data.data[i].operate = html;
    //                 }
    //             }
    //             // jQuery('#goods_list').trigger('reloadGrid');
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             console.error(errorThrown);
    //         }
    //     });
}


function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#goods_list").jqGrid('delRowData',id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url : url,
            type: action,
            success:function(data, textStatus, jqXHR)
            {
                if(action == 'DELETE'){
                    jQuery("#goods_list").trigger("reloadGrid");

                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                console.error(errorThrown);
            }
        });
}

function dataWrapper() {
    var ids = jQuery("#goods_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#goods_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/goodsCategory/update/'+id+'" data-index="0">编辑</a>';
        html += "<a href='#'   style='color:#f60' onclick='Delete(" + id + ","+  (i+1) +")' >删除</a>";
        jQuery("#goods_list").jqGrid('setRowData', ids[i], {
            operate: html,
            ctime: dataFromTheRow.ctime
        });
    }
}