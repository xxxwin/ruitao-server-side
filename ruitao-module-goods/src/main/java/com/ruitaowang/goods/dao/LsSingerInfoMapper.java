package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.LsSingerInfo;
import java.util.List;

public interface LsSingerInfoMapper {
    int insert(LsSingerInfo lsSingerInfo);

    int deleteByPK(Long singerId);

    int updateByPK(LsSingerInfo lsSingerInfo);

    LsSingerInfo selectByPK(Long singerId);

    long count(LsSingerInfo lsSingerInfo);

    List<LsSingerInfo> select(LsSingerInfo lsSingerInfo);
}