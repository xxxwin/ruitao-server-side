package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.MeetingUserLink;
import java.util.List;

public interface MeetingUserLinkMapper {
    int insert(MeetingUserLink meetingUserLink);

    int deleteByPK(Long linkId);

    int updateByPK(MeetingUserLink meetingUserLink);

    MeetingUserLink selectByPK(Long linkId);

    long count(MeetingUserLink meetingUserLink);

    List<MeetingUserLink> select(MeetingUserLink meetingUserLink);
}