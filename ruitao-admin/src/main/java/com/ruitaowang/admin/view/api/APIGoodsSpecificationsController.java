package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.GoodsSpecifications;
import com.ruitaowang.core.domain.GoodsTypeCustom;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.dao.GoodsSpecificationsMapper;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.GoodsSpecificationsService;
import com.ruitaowang.goods.service.GoodsTypeCustomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

@RestController
public class APIGoodsSpecificationsController extends BaseController {

    @Autowired
    private GoodsSpecificationsService goodsSpecificationsService;

    @RequestMapping("/api/admin/goodsSpecifications")
    public String adminList(GoodsSpecifications goodsSpecifications) {
        MsgOut o = MsgOut.success(goodsSpecificationsService.select(goodsSpecifications));
        return this.renderJson(o);
    }
    /**
     * @api {get} /api/wap/goodsTypeCustom/page 自定义分类-(分页)查询所有
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom/page
     *
     * @apiName listForPage
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     * @apiParam {String} remark 备注.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {String} name 分类名称.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "name":"",
     *                   "companyId":0,
     *                   "ctime":1502870415259,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom/page"
     *  }
     */
    @RequestMapping("/api/wap/goodsSpecifications/page")
    public String listForPage(GoodsSpecifications goodsSpecifications, int page, @RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        goodsSpecifications.setPage(page);
        MsgOut o = MsgOut.success(goodsSpecificationsService.selectForPage(goodsSpecifications));
        o.setRecords(goodsSpecifications.getRecords());
        o.setTotal(goodsSpecifications.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping("/api/admin/goodsSpecifications/page")
    public String adminListForPage(GoodsSpecifications goodsSpecifications, int page) {
        goodsSpecifications.setPage(page);
        if (!ObjectUtils.isEmpty(goodsSpecifications.getSidx())) {
            goodsSpecifications.setOrderBy(goodsSpecifications.getSidx() + " " + goodsSpecifications.getSord() + "," + goodsSpecifications.getOrderBy());
        }
        MsgOut o = MsgOut.success(goodsSpecificationsService.selectForPage(goodsSpecifications));
        o.setRecords(goodsSpecifications.getRecords());
        o.setTotal(goodsSpecifications.getTotal());
        return this.renderJson(o);
    }
    /**
     * @api {post} /api/wap/goodsTypeCustom 自定义分类-创建
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom
     *
     * @apiName create
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {String} name 分类名称.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "name":"",
     *                   "companyId":0,
     *                   "ctime":1502870415259,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom"
     *  }
     */
    @RequestMapping(value = "/api/wap/goodsSpecifications", method = RequestMethod.POST)
    public String create(GoodsSpecifications goodsSpecifications,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(goodsSpecifications)){
            goodsSpecificationsService.insert(goodsSpecifications);
        }
        MsgOut o = MsgOut.success(goodsSpecifications);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/goodsSpecifications", method = RequestMethod.POST)
    public String adminCreate(GoodsSpecifications goodsSpecifications) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (!ObjectUtils.isEmpty(goodsSpecifications)){
            goodsSpecificationsService.insert(goodsSpecifications);
        }
        MsgOut o = MsgOut.success(goodsSpecifications);
        return this.renderJson(o);
    }
    /**
     * @api {put} /api/wap/goodsTypeCustom 自定义分类-修改
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom
     *
     * @apiName update
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {String} remark 备注.
     * @apiParam {long} companyId 商户ID.
     * @apiParam {String} name 分类名称.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "id":8,
     *                   "name":"",
     *                   "companyId":0,
     *                   "ctime":1502870415259,
     *                   "mtime":1502870415259,
     *                   "remark":"",
     *                   "rstatus":0,
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom"
     *  }
     */
    @RequestMapping(value = "/api/wap/goodsSpecifications", method = RequestMethod.PUT)
    public String update(GoodsSpecifications goodsSpecifications,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        goodsSpecificationsService.update(goodsSpecifications);
        MsgOut o = MsgOut.success(goodsSpecifications);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/goodsSpecifications", method = RequestMethod.PUT)
    public String adminUpdate(GoodsSpecifications goodsSpecifications) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        goodsSpecificationsService.update(goodsSpecifications);
        MsgOut o = MsgOut.success(goodsSpecifications);
        return this.renderJson(o);
    }

    /**
     * @api {delete} /api/wap/goodsTypeCustom/{id} 自定义分类-删除
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/wap/goodsTypeCustom/{id}
     *
     * @apiName delete
     * @apiGroup GoodsTypeCustom
     *
     * @apiParam {long} id 评论ID.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":1
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/wap/goodsTypeCustom/{id}"
     *  }
     */
    @RequestMapping(value = "/api/wap/goodsSpecificationsDelete/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("id") Long id,@RequestHeader(value="headId",required = false) String userId) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(goodsSpecificationsService.delete(id));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/goodsSpecificationsDelete/{id}", method = RequestMethod.DELETE)
    public String adminDelete(@PathVariable("id") Long id) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(goodsSpecificationsService.delete(id));
        return this.renderJson(o);
    }
}
