/**
 * 
 */


function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
var Config = {
    createTableTypeUrl:'/api/admin/dc/tableType',
    selectTableTypeUrl:'/api/admin/dc/tableType',
    createUrl: '/api/admin/dc/table',
    updateUrl: '/api/admin/dc/table',
    deleteUrl: '/api/admin/dc/table',
    selectUrl: '/api/admin/dc/table',
    selectCompanyUrl:'/api/dc/list/company'
};
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
        $.ajax(
            {
                url: Config.selectTableTypeUrl,
                type: "GET",
                data: {companyId:getQueryString("id"),page:1,rows:1000},
                dataType: "json",
                async: false,
                success: function (data, textStatus, jqXHR) {
                    var dcTableTypes = [];
                    dcTableTypes.push('<option value="" hassubinfo="true">选择分类</option>');
                    data.data.map(function (dcTableType) {
                        dcTableTypes.push('<option value="');
                        dcTableTypes.push(dcTableType.id);
                        dcTableTypes.push('" ');
                        dcTableTypes.push('hassubinfo="true">');
                        dcTableTypes.push(dcTableType.name);
                        dcTableTypes.push('</option>');
                    });
                    $("#tableTypeId").html(dcTableTypes.join(''));
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
    //create menu
    $('#submitForSave').click(function () {
        var name = $("input[name=name]").val();
        var remark = $("input[name=remark]").val();
        var tableTypeId = $("#tableTypeId").val();
        $.ajax(
            {url: Config.createUrl,
                type: "POST",
                data: {companyId:getQueryString("id"),name:name,remark:remark,tableTypeId:tableTypeId},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        return false;
    });
    $('#createTableTag').click(function () {
        var name = $("input[id=table_name]").val();
        var numMax = $("input[id=numMax]").val();
        $.ajax(
            {url: Config.createTableTypeUrl,
                type: "POST",
                data: {name:name,numMax:numMax,companyId:getQueryString("id")},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                    $("input[id=table_name]").val("");
                    $("input[id=numMax]").val("");
                    $.ajax(
                        {
                            url: Config.selectTableTypeUrl,
                            type: "GET",
                            data: {companyId:getQueryString("id"),page:1,rows:1000},
                            dataType: "json",
                            async: false,
                            success: function (data, textStatus, jqXHR) {
                                var dcTableTypes = [];
                                dcTableTypes.push('<option value="" hassubinfo="true">选择分类</option>');
                                data.data.map(function (dcTableType) {
                                    dcTableTypes.push('<option value="');
                                    dcTableTypes.push(dcTableType.id);
                                    dcTableTypes.push('" ');
                                    dcTableTypes.push('hassubinfo="true">');
                                    dcTableTypes.push(dcTableType.name);
                                    dcTableTypes.push('</option>');
                                });
                                $("#tableTypeId").html(dcTableTypes.join(''));
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                toastr.error(errorThrown, textStatus);
                            }
                        });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        return false;
    });

});

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    
}