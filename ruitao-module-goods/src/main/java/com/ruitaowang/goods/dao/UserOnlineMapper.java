package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserOnline;
import java.util.List;

public interface UserOnlineMapper {
	
    int insert(UserOnline userOnline);

    int deleteByPK(Long id);

    int updateByPK(UserOnline userOnline);

    UserOnline selectByPK(Long id);

    long count(UserOnline userOnline);

    List<UserOnline> select(UserOnline userOnline);
}