/**
 *
 */
var num =0;
var Config = {
    createUrl: '/api/admin/dc/orders',
    updateUrl: '/api/admin/dc/orders',
    deleteUrl: '/api/admin/dc/orders',
    selectUrl: '/api/admin/dc/hd/order/get'
};
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 550,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["订单ID", "日期", "流水号","桌号", "总价(元)", "返红包", "公司ID","消费者", "备注", "时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: false
        }, {
            name: "day",
            index: "day"
        }, {
            name: "dayIndex",
            index: "dayIndex"
        }, {
            name: "tableId",
            index: "tableId"
  //          formatter:mydateformatter,
        },{
            name: "amount",
            index: "amount"
        }, {
            name: "score",
            index: "score"
        }, {
            name: "companyId",
            index: "companyId"
        }, {
            name: "userId",
            index: "userId"
        },{
            name: "remark",
            index: "remark"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate"
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false,id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });
   // setInterval(show,5000);
});
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='orderUpdate(" + id + ")' >详情</a>";
        html += "<a style='color:#f60;padding-right: 15px' onclick='pt(" + dataFromTheRow.id + ")' >打印</a>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {operate: html,ctime: dataFromTheRow.ctime,amount:toFixedForPrice(dataFromTheRow.amount)});
    }
}
function pt(orderid) {
    layer.open({
        type: 2,
        //   title: '请选择区域',
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content: "/web/dc_hd_order_print.html?id=" + orderid
    })
}
function orderUpdate(id) {
    layer.open({
        type: 2,
        //   title: '请选择区域',
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content: "/web/dc_order_update.html?id=" + id
    })
}
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#data_list").jqGrid('delRowData', id);
}

function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    $("#data_list").trigger("reloadGrid");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error(errorThrown);
            }
        });
}
function mydateformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax({
        url: "/api/admin/dc/table/"+rowObject.tableId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            if (data.data){
                name = data.data.name;
            }else {
                name=""
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return name;
}