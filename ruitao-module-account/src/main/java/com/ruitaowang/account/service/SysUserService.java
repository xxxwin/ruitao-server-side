package com.ruitaowang.account.service;

import com.ruitaowang.account.dao.SysUserAdMapper;
import com.ruitaowang.account.dao.SysUserMapper;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.SysUserVO;
import com.ruitaowang.core.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by neal on 11/07/16.
 */
@Service
public class SysUserService implements BaseService<SysUser> {

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserAdMapper sysUserAdMapper;


    @Override
    public SysUser insert(SysUser sysUser) {
        Long time = System.currentTimeMillis();
        sysUser.setCtime(time);
        sysUser.setMtime(time);
        sysUserMapper.insert(sysUser);
        return sysUser;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public SysUser update(SysUser sysUser) {
        Long time = System.currentTimeMillis();
        sysUser.setMtime(time);
        if (!ObjectUtils.isEmpty(sysUser.getPhone())){
            sysUser.setLocked(true);
        }
        sysUserMapper.updateByPK(sysUser);
        return sysUser;
    }
    @Override
    public SysUser selectByPK(Long id) {
        return sysUserMapper.selectByPK(id);
    }
    public SysUser selectByUsername(String username) {
        if(StringUtils.isEmpty(username)){
            return null;
        }
        SysUser user = new SysUser();
        user.setUsername(username);
        List<SysUser> users = this.select(user);
        if(ObjectUtils.isEmpty(users)){
            return null;
        }
        return users.get(0);
    }

    @Override
    public List<SysUser> select(SysUser sysUser) {
        return sysUserMapper.select(sysUser);
    }

    public List<SysUser> search(SysUser sysUser) {
        return sysUserAdMapper.select(sysUser);
    }

    public List<SysUser> selectUserForCommunity(SysUserVO sysUserVO){
        return sysUserAdMapper.selectUserForCommunity(sysUserVO);
    }


    public List<SysUser> selectForPage(SysUser address) {
        address.setRstatus((byte) 0);
        address.setRecords(sysUserMapper.count(address));
        return sysUserMapper.select(address);
    }
    public List<SysUser> searchForPage(SysUser address) {
        address.setRstatus((byte) 0);
        address.setRecords(sysUserAdMapper.count(address));
        return sysUserAdMapper.select(address);
    }

    public Long userCount(SysUser sysUser){
        sysUser.setRstatus((byte) 0);
        return sysUserMapper.count(sysUser);
    }
    public Long countDayData(Long stime,Long etime){
        return sysUserMapper.countDayData(stime,etime);
    }
    public Long countDayAll(Long etime){
        return sysUserMapper.countDayAll(etime);
    }
}
