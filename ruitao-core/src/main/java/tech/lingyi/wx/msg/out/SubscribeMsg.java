package tech.lingyi.wx.msg.out;

/**
 * Created by neal on 09/08/2017.
 */
public class SubscribeMsg {
    private String touser;
    private String template_id;
    private String url;
    private String scene;
    private String title;
}