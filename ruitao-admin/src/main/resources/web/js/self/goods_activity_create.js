/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/activities',
    updateUrl: '/api/activities',
    deleteUrl: '/api/activities',
    selectUrl: '/api/activities',
    selectGoods: '/api/admin/goods'
};

$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
var ue = UE.getEditor('editor');
ue.ready(function() {
    ue.execCommand('serverparam', {
        'isActivity': 1
    });
    ue.setContent($("input[name=activityContent]").val());
});
$().ready(function () {
    $('#submitCategoryCreate').click( function() {
        var activityImageUrl = $("input[name=activityImageUrl]").val().indexOf("/ueditor/jsp/upload/image");
        if(activityImageUrl != -1){
            alert("图片上传未成功")
            return false;
        }

        // if($("p").attr("name") == "2"){
        //     toastr.error("商品不存在", "ERROR");
        //     return false;
        // }

        $("#ajaxform").validate();
        if ($('#ajaxform').valid()){
            $("input[name=activityContent]").val(ue.getContent());
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.createUrl,
                    type: "POST",
                    data : postData,
                    dataType: "json",
                    async: false,
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }
        return false;
    });
    //uedit监听事件
    ue.addListener("contentChange",function(){
        console.log('内容改变:'+ue.getContent());
        if(ue.getContent().indexOf('/ueditor/jsp/') != -1) {
            toastr.error("改图片存在问题，请重新删除后重新上传.", "ERROR");
        }
    });
});
var input;
$("input:file").change(function(){
    input = this;
    lrz(this.files[0], {width: 640})
        .then(function (rst) {
            // 把处理的好的图片给用户看看呗
            var img = new Image();
            img.src = rst.base64;
            img.onload = function () {
                $(".div_temporaryImage").html(img);
            };
            return rst;
        })
        .then(function (rst) {
            // 这里该上传给后端啦
            // 额外添加参数
            rst.formData.append('fileLen', rst.fileLen);
            $.ajax({
                url: '/uploadForCertificate', // 这个地址做了跨域处理，可以用于实际调试
                data: rst.formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    console.log(data+"data");
                    if(data.type=="SUCCESS"){
                        $(".activityImageUrl").val(data.msg);
                        $(input).parent().siblings("span").html("上传成功");
                        // $(input).parent("div").parent("div").next("div").html('<img src="'+data.msg+'">');
                    }else{
                        alert("上传失败，请稍后重试");
                    }
                }
            });
            return rst;
        })
        .catch(function (err) {
            alert("上传失败，请稍后重试");
        })
        .always(function () {

        });
});
