package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.District;
import java.util.List;

public interface DistrictMapper {
    int insert(District district);

    int deleteByPK(Long id);

    int updateByPK(District district);

    District selectByPK(Long id);

    long count(District district);

    List<District> select(District district);
}