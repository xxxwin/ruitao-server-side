package com.ruitaowang.admin.view.api;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.ruitaowang.core.domain.GoodsAttribute;
import com.ruitaowang.core.domain.GoodsAttributeLink;
import com.ruitaowang.core.domain.GoodsType;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.GoodsAttributeLinkService;
import com.ruitaowang.goods.service.GoodsAttributeService;
import com.ruitaowang.goods.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Shaka on 2016/11/23.
 */
@RestController
public class APIGoodsAttributeController extends BaseController {

    @Autowired
    private GoodsAttributeService goodsAttributeService;


    /**
     * @api {get} /api/goodsAttribute 分页获取活动或者广告列表
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/goodsAttribute
     *
     * @apiName findGoodsAttributePage
     * @apiGroup GoodsAttribute
     *
     * @apiParam {int} page 第几页.
     * @apiParam {int} rows 每页条数.
     *
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *
     *    {
     *       "code":0,
     *       "data":[
     *           {
     *                   "attrId": 108,
     *                   "attrName": "口味",
     *                   "attrSort": 1,
     *                   "attrType": 0,
     *                   "attrValues": "香辣虾味\r\n烧烤盐味\r\n冬荫功味\r\n披萨风味\r\n玉米味\r\n沙律风味\r\n牛奶原味\r\n蔓越莓味\r\n提子味\r\n辣味鸡肉松味\r\n紫菜鸡肉松味\r\n泰式酸辣味\r\n紫菜味\r\n虾丝味\r\n鸡肉松味\r\n巧克力味\r\n抹茶味\r\n奶油味\r\n香草味\r\n双层巧克力口味\r\n白巧克力口味\r\n经典原味\r\n焦糖夹心口味\r\n招牌原味\r\n碳烤味\r\n香辣味\r\n海苔味\r\n椒盐味\r\n牛奶味\r\n可可味\r\n酸辣虾味汤面\r\n青咖喱汤面\r\n香辣海鲜味\r\n冬荫功浓汤\r\n榛子\r\n巧克力\r\n黑巧克力\r\n香草\r\n椰子\r\n柠檬\r\n黑加仑\r\n提拉米苏\r\n卡布奇诺\r\n扁桃仁\r\n咖啡\r\n覆盆子酸奶\r\n京都原味奶茶\r\n宇治抹茶奶绿\r\n函馆恋人巧克力奶茶\r\n浅草樱、莓风味\r\n北海道札幌奶茶\r\n烧烤味\r\n黄油味\r\n芥末味\r\n原味\r\n番茄味\r\n芝士味",
     *                  "ctime": 1503458128768,
     *                  "goodsPromotePrice": 0,
     *                  "mtime": 1504233263154,
     *                  "rstatus": 0,
     *                   "typeId": 80
     *           }
     *       ],
     *       "msg":"操作成功啦",
     *       "records":5,
     *       "title":"成功",
     *       "total":1,
     *       "type":"SUCCESS"
     *    }
     *
     * @apiError InternalServerError.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     *  {
     *      "timestamp":1504064947603,
     *      "status":500,
     *      "error":"Internal Server Error",
     *      "exception":"java.lang.ArithmeticException",
     *      "message":"/ by zero",
     *      "path":"/api/goodsAttribute"
     *  }
     */
    @RequestMapping("/api/goodsAttribute")
    public String findGoodsAttributePage(GoodsAttribute goodsAttribute) {
        List<GoodsAttribute> list;
        if(null==goodsAttribute)
            goodsAttribute = new GoodsAttribute();
        list = goodsAttributeService.select(goodsAttribute);
        MsgOut o = MsgOut.success(list);
        return JSON.toJSONStringWithDateFormat(o, "yyyy-MM-dd HH:mm:ss", SerializerFeature.DisableCircularReferenceDetect);
    }
    @RequestMapping("/api/wap/goodsAttribute/get")
    public String wapFindGoodsAttributePage(GoodsAttribute goodsAttribute) {
        List<GoodsAttribute> list;
        if(null==goodsAttribute)
            goodsAttribute = new GoodsAttribute();
        list = goodsAttributeService.select(goodsAttribute);
        MsgOut o = MsgOut.success(list);
        return this.renderJson(o);
    }

    @RequestMapping("/api/goodsAttribute/{goodsAttributeId}")
    public String list(@PathVariable("goodsAttributeId") Long goodsTypeId){
        MsgOut o=MsgOut.success(goodsAttributeService.selectByPK(goodsTypeId));
        return  this.renderJson(o);
    }

    @RequestMapping(value = "/api/goodsAttribute",method = RequestMethod.POST)
    public String create(GoodsAttribute goodsAttribute){
        goodsAttributeService.insert(goodsAttribute);
        MsgOut o=MsgOut.success(goodsAttribute);
        return this.renderJson(o);
    }

    @RequestMapping(value="/api/goodsAttribute",method =RequestMethod.PUT)
    public String update(GoodsAttribute goodsAttribute){
        goodsAttributeService.update(goodsAttribute);
        MsgOut o=MsgOut.success(goodsAttribute);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/goodsAttribute/{goodsAttributeId}" ,method = RequestMethod.DELETE)
    public String delete(@PathVariable("goodsAttributeId") Long goodsTypeId){
        MsgOut o=MsgOut.success(goodsAttributeService.delete(goodsTypeId));
        return this .renderJson(o);
    }






}
