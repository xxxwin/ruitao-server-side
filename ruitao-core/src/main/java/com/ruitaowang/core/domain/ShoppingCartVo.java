package com.ruitaowang.core.domain;

public class ShoppingCartVo extends Goods {
    /**
     *  编号,所属表字段为shopping_cart.cart_id
     */
    private Long cartId;

    /**
     *  用户编号,所属表字段为shopping_cart.user_id
     */
    private Long userId;

    /**
     *  商品属性ID，以逗号分隔.eg: 1，2，3,所属表字段为shopping_cart.goods_attr_link_id
     */
    private String goodsAttrLinkId;

    private String goodsAttrValues;

    /**
     *  ,所属表字段为shopping_cart.goods_number
     */
    private Integer goodsNumber;

    public Long getCartId() {
        return cartId;
    }

    public void setCartId(Long cartId) {
        this.cartId = cartId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getGoodsAttrLinkId() {
        return goodsAttrLinkId;
    }

    public void setGoodsAttrLinkId(String goodsAttrLinkId) {
        this.goodsAttrLinkId = goodsAttrLinkId == null ? null : goodsAttrLinkId.trim();
    }

    public String getGoodsAttrValues() {
        return goodsAttrValues;
    }

    public void setGoodsAttrValues(String goodsAttrValues) {
        this.goodsAttrValues = goodsAttrValues;
    }

    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
}