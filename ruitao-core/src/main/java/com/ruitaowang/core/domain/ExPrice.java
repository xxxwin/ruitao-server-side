package com.ruitaowang.core.domain;

public class ExPrice extends Page {
    /**
     *  自增编码,所属表字段为ex_price.exprice_id
     */
    private Long expriceId;

    /**
     *  省,所属表字段为ex_price.province_id
     */
    private String provinceId;

    /**
     *  市,所属表字段为ex_price.city_id
     */
    private Long cityId;

    /**
     *  快递公司编号,所属表字段为ex_price.logistics_id
     */
    private Long logisticsId;

    /**
     *  公司编码,所属表字段为ex_price.company_id
     */
    private Long companyId;

    /**
     *  运费,所属表字段为ex_price.price
     */
    private Integer price;

    /**
     *  创建时间,所属表字段为ex_price.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为ex_price.mtime
     */
    private Long mtime;

    /**
     *  0 正常
1 删除,所属表字段为ex_price.rstatus
     */
    private Byte rstatus;

    /**
     *  超过1kg额外多少钱,所属表字段为ex_price.height_extra_price
     */
    private Integer heightExtraPrice;

    /**
     *  基准重量,所属表字段为ex_price.height_base
     */
    private Integer heightBase;

    public Long getExpriceId() {
        return expriceId;
    }

    public void setExpriceId(Long expriceId) {
        this.expriceId = expriceId;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId == null ? null : provinceId.trim();
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(Long logisticsId) {
        this.logisticsId = logisticsId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getHeightExtraPrice() {
        return heightExtraPrice;
    }

    public void setHeightExtraPrice(Integer heightExtraPrice) {
        this.heightExtraPrice = heightExtraPrice;
    }

    public Integer getHeightBase() {
        return heightBase;
    }

    public void setHeightBase(Integer heightBase) {
        this.heightBase = heightBase;
    }
}