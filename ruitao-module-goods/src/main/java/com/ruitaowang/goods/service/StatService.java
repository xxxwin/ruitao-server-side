package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.StatCompany;
import com.ruitaowang.core.domain.StatCompanyUser;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.StatAdMapper;
import com.ruitaowang.goods.dao.StatCompanyMapper;
import com.ruitaowang.goods.dao.StatCompanyUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class StatService implements BaseService<StatCompany> {
    @Autowired
    private StatCompanyMapper statCompanyMapper;
    @Autowired
    private StatCompanyUserMapper statCompanyUserMapper;
    @Autowired
    private StatAdMapper statAdMapper;
    @Override
    public StatCompany insert(StatCompany statCompany) {
        Long time = System.currentTimeMillis();
        statCompany.setCtime(time);
        statCompany.setMtime(time);
        statCompanyMapper.insert(statCompany);
        return statCompany;
    }

    @Override
    public int delete(Long id) {
        StatCompany statCompany = new StatCompany();
        statCompany.setId(id);
        statCompany.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        statCompany.setMtime(time);
        return statCompanyMapper.updateByPK(statCompany);
    }

    @Override
    public StatCompany update(StatCompany statCompany) {
        Long time = System.currentTimeMillis();
        statCompany.setMtime(time);
        statCompanyMapper.updateByPK(statCompany);
        return statCompany;
    }

    @Override
    public StatCompany selectByPK(Long id) {

        return statCompanyMapper.selectByPK(id);
    }

    @Override
    public List<StatCompany> select(StatCompany statCompany) {
        statCompany.setRstatus((byte) 0);
        return statCompanyMapper.select(statCompany);
    }

    public List<StatCompany> selectForPage(StatCompany statCompany) {
        statCompany.setRstatus((byte) 0);
        statCompany.setRecords(statCompanyMapper.count(statCompany));
        return statCompanyMapper.select(statCompany);
    }

    public List<StatCompanyUser> selectStatCompanyUserForPage(StatCompanyUser statCompanyUser) {
        statCompanyUser.setRstatus((byte) 0);
        statCompanyUser.setRecords(statCompanyUserMapper.count(statCompanyUser));
        return statCompanyUserMapper.select(statCompanyUser);
    }

    public void updateStatCompany() {
        //order count
        List<StatCompany> shopOrderCounts = statAdMapper.selectAllShopOrderCountGroupByHTime();
        if(!ObjectUtils.isEmpty(shopOrderCounts)){

            for (StatCompany statCompany : shopOrderCounts){
                StatCompany where = new StatCompany();
                where.setCompanyId(statCompany.getCompanyId());
                where.setDate(statCompany.getDate());
                List<StatCompany> wheres = statCompanyMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                        statCompanyMapper.insert(statCompany);
                }else{
                    StatCompany update = wheres.get(0);
                    update.setOrderCount(statCompany.getOrderCount());
                    statCompanyMapper.updateByPK(update);
                }
            }
        }
        //local order count
        List<StatCompany> localOrderCounts = statAdMapper.selectAllLocalOrderCountByGroupHTime();
        if(!ObjectUtils.isEmpty(localOrderCounts)){

            for (StatCompany statCompany : localOrderCounts){
                StatCompany where = new StatCompany();
                where.setCompanyId(statCompany.getCompanyId());
                where.setDate(statCompany.getDate());
                List<StatCompany> wheres = statCompanyMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                    statCompanyMapper.insert(statCompany);
                }else{
                    StatCompany update = wheres.get(0);
                    update.setLocalOrderCount(statCompany.getLocalOrderCount());
                    statCompanyMapper.updateByPK(update);
                }
            }
        }
        //order income
        List<StatCompany> orderIncomes = statAdMapper.selectAllOrderIncomeByGroupHTime();
        if(!ObjectUtils.isEmpty(orderIncomes)){

            for (StatCompany statCompany : orderIncomes){
                StatCompany where = new StatCompany();
                where.setCompanyId(statCompany.getCompanyId());
                where.setDate(statCompany.getDate());
                List<StatCompany> wheres = statCompanyMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                    statCompanyMapper.insert(statCompany);
                }else{
                    StatCompany update = wheres.get(0);
                    update.setOrderIncome(statCompany.getOrderIncome());
                    statCompanyMapper.updateByPK(update);
                }
            }
        }
        //local order income
        List<StatCompany> localOrderIncomes = statAdMapper.selectAllLocalOrderIncomeByGroupHTime();
        if(!ObjectUtils.isEmpty(localOrderIncomes)){

            for (StatCompany statCompany : localOrderIncomes){
                StatCompany where = new StatCompany();
                where.setCompanyId(statCompany.getCompanyId());
                where.setDate(statCompany.getDate());
                List<StatCompany> wheres = statCompanyMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                    statCompanyMapper.insert(statCompany);
                }else{
                    StatCompany update = wheres.get(0);
                    update.setLocalOrderIncome(statCompany.getLocalOrderIncome());
                    statCompanyMapper.updateByPK(update);
                }
            }
        }
    }


    public void updateStatCompanyUser() {
        //order count
        List<StatCompanyUser> shopOrderCounts = statAdMapper.selectAllUserOrderCountGroupByHTime();
        if(!ObjectUtils.isEmpty(shopOrderCounts)){

            for (StatCompanyUser statCompany : shopOrderCounts){
                StatCompanyUser where = new StatCompanyUser();
                where.setCompanyId(statCompany.getCompanyId());
                where.setUserId(statCompany.getUserId());
                where.setDate(statCompany.getDate());
                List<StatCompanyUser> wheres = statCompanyUserMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                    statCompanyUserMapper.insert(statCompany);
                }else{
                    StatCompanyUser update = wheres.get(0);
                    update.setOrderCount(statCompany.getOrderCount());
                    statCompanyUserMapper.updateByPK(update);
                }
            }
        }
        //local order count
        List<StatCompanyUser> localOrderCounts = statAdMapper.selectAllUserLocalOrderCountByGroupHTime();
        if(!ObjectUtils.isEmpty(localOrderCounts)){

            for (StatCompanyUser statCompany : localOrderCounts){
                StatCompanyUser where = new StatCompanyUser();
                where.setCompanyId(statCompany.getCompanyId());
                where.setUserId(statCompany.getUserId());
                where.setDate(statCompany.getDate());
                List<StatCompanyUser> wheres = statCompanyUserMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                    statCompanyUserMapper.insert(statCompany);
                }else{
                    StatCompanyUser update = wheres.get(0);
                    update.setLocalOrderCount(statCompany.getLocalOrderCount());
                    statCompanyUserMapper.updateByPK(update);
                }
            }
        }
        //order income
        List<StatCompanyUser> shopOrderConsumption = statAdMapper.selectAllUserOrderConsumptionByGroupHTime();
        if(!ObjectUtils.isEmpty(shopOrderCounts)){

            for (StatCompanyUser statCompany : shopOrderConsumption){
                StatCompanyUser where = new StatCompanyUser();
                where.setCompanyId(statCompany.getCompanyId());
                where.setUserId(statCompany.getUserId());
                where.setDate(statCompany.getDate());
                List<StatCompanyUser> wheres = statCompanyUserMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                    statCompanyUserMapper.insert(statCompany);
                }else{
                    StatCompanyUser update = wheres.get(0);
                    update.setOrderConsumption(statCompany.getOrderConsumption());
                    statCompanyUserMapper.updateByPK(update);
                }
            }
        }
        //local order income
        List<StatCompanyUser> localOrderConsumption = statAdMapper.selectAllUserLocalOrderConsumptionByGroupHTime();
        if(!ObjectUtils.isEmpty(shopOrderCounts)){

            for (StatCompanyUser statCompany : localOrderConsumption){
                StatCompanyUser where = new StatCompanyUser();
                where.setCompanyId(statCompany.getCompanyId());
                where.setUserId(statCompany.getUserId());
                where.setDate(statCompany.getDate());
                List<StatCompanyUser> wheres = statCompanyUserMapper.select(where);
                if(ObjectUtils.isEmpty(wheres)){
                    statCompanyUserMapper.insert(statCompany);
                }else{
                    StatCompanyUser update = wheres.get(0);
                    update.setLocalOrderConsumption(statCompany.getLocalOrderConsumption());
                    statCompanyUserMapper.updateByPK(update);
                }
            }
        }
    }
}

