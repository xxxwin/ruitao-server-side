package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.UserCommunication;

import java.util.List;

/**
 * Created by xinchunting on 17-10-30.
 */
public interface UserCommunicationMapper {
    int insert(UserCommunication userCommunication);

    int deleteByPK(Long id);

    int updateByPK(UserCommunication userCommunication);

    UserCommunication selectByPK(Long id);

    long count(UserCommunication userCommunication);

    List<UserCommunication> select(UserCommunication userCommunication);
}
