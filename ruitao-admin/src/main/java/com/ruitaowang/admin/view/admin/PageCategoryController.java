package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Category;
import com.ruitaowang.core.domain.CategoryRecommend;
import com.ruitaowang.core.domain.GoodsCategory;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.goods.service.CategoryService;
import com.ruitaowang.goods.service.GoodsCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.security.PermitAll;
import java.util.*;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class PageCategoryController extends BaseController {

    @Autowired
    private GoodsCategoryService goodsCategoryService;
    @Autowired
    private CategoryService categoryService;

    /**
     * 商品分类 － 编辑
     */
    @RequestMapping(value = {"/category/update/{categoryId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView update(Model model, @PathVariable("categoryId") Long categoryId) {
        Category category = categoryService.selectByPK(categoryId);
        model.addAttribute("category", category);
        if (category != null) {
            List<CategoryRecommend> categoryRecommendList = categoryService.selectRecommend(category.getId());
            Byte[] b = new Byte[]{1,2,3};
            Byte[] a = new Byte[3];
            if (categoryRecommendList != null && categoryRecommendList.size() > 0){
                for(int j=0;j< categoryRecommendList.size();j++){
                    if(categoryRecommendList.get(j).getRecommendType() == 1){
                        model.addAttribute("best", "true");
                        a[0] = 1;
                    }

                    if(categoryRecommendList.get(j).getRecommendType() == 2){
                        model.addAttribute("newest", "true");
                        a[1] = 2;
                    }

                    if(categoryRecommendList.get(j).getRecommendType() == 3){
                        model.addAttribute("hotest", "true");
                        a[2] = 3;
                    }
                }
            }

            Byte[] c = substract(b,a);
            if ( c != null && c.length > 0){
                for(int j=0;j< c.length;j++){
                    if(c[j] == 1){
                        model.addAttribute("best", "false");
                    }

                    if(c[j] == 2){
                        model.addAttribute("newest", "false");
                    }

                    if(c[j] == 3){
                        model.addAttribute("hotest", "false");
                    }
                }
            }
        }
        return new ModelAndView("/web/goods_category_update");
    }

    /**
     * 商品分类 － 编辑
     */
    @RequestMapping(value = {"/goodsCategory/update/{categoryId}"}, method = RequestMethod.GET)
    @PermitAll
    public ModelAndView goodsCategoryUpdate(Model model, @PathVariable("categoryId") Long categoryId) {
        GoodsCategory goodsCategory = goodsCategoryService.selectByPK(categoryId);
        model.addAttribute("category", goodsCategory);
        return new ModelAndView("/web/goodsCategory_update");
    }
    //求两个数组的差集
    public static Byte[] substract(Byte[] arr1, Byte[] arr2) {
        LinkedList<Byte> list = new LinkedList<Byte>();
        for (Byte str : arr1) {
            if(!list.contains(str)) {
                list.add(str);
            }
        }
        for (Byte str : arr2) {
            if (list.contains(str)) {
                list.remove(str);
            }
        }
        Byte[] result = {};
        return list.toArray(result);
    }
}