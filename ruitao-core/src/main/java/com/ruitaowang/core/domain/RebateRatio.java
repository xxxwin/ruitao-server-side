package com.ruitaowang.core.domain;

public class RebateRatio extends Page {
    /**
     *  返利比例自增ID,所属表字段为rebate_ratio.id
     */
    private Long id;

    /**
     *  商品ID,所属表字段为rebate_ratio.goods_id
     */
    private Long goodsId;

    /**
     *  一级返利,所属表字段为rebate_ratio.one_level
     */
    private Byte oneLevel;

    /**
     *  二级返利,所属表字段为rebate_ratio.two_level
     */
    private Byte twoLevel;

    /**
     *  区返利,所属表字段为rebate_ratio.area_level
     */
    private Byte areaLevel;

    /**
     *  市返利,所属表字段为rebate_ratio.city_level
     */
    private Byte cityLevel;

    /**
     *  省返利,所属表字段为rebate_ratio.province_level
     */
    private Byte provinceLevel;

    /**
     *  龙蛙返利,所属表字段为rebate_ratio.rt_level
     */
    private Byte rtLevel;

    /**
     *  ,所属表字段为rebate_ratio.remark
     */
    private String remark;

    /**
     *  ,所属表字段为rebate_ratio.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为rebate_ratio.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为rebate_ratio.mtime
     */
    private Long mtime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Byte getOneLevel() {
        return oneLevel;
    }

    public void setOneLevel(Byte oneLevel) {
        this.oneLevel = oneLevel;
    }

    public Byte getTwoLevel() {
        return twoLevel;
    }

    public void setTwoLevel(Byte twoLevel) {
        this.twoLevel = twoLevel;
    }

    public Byte getAreaLevel() {
        return areaLevel;
    }

    public void setAreaLevel(Byte areaLevel) {
        this.areaLevel = areaLevel;
    }

    public Byte getCityLevel() {
        return cityLevel;
    }

    public void setCityLevel(Byte cityLevel) {
        this.cityLevel = cityLevel;
    }

    public Byte getProvinceLevel() {
        return provinceLevel;
    }

    public void setProvinceLevel(Byte provinceLevel) {
        this.provinceLevel = provinceLevel;
    }

    public Byte getRtLevel() {
        return rtLevel;
    }

    public void setRtLevel(Byte rtLevel) {
        this.rtLevel = rtLevel;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }
}