package com.ruitaowang.core.domain;

public class CategoryRecommend extends Page {
    /**
     *  推荐编号,所属表字段为category_recommend.recommend_id
     */
    private Long recommendId;

    /**
     *  商品分类编号,所属表字段为category_recommend.category_id
     */
    private Long categoryId;

    /**
     *  1，精品
2，最新
3，最热,所属表字段为category_recommend.recommend_type
     */
    private Byte recommendType;

    /**
     *  创建时间,所属表字段为category_recommend.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为category_recommend.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常，1 删除,所属表字段为category_recommend.rstatus
     */
    private Byte rstatus;

    public Long getRecommendId() {
        return recommendId;
    }

    public void setRecommendId(Long recommendId) {
        this.recommendId = recommendId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Byte getRecommendType() {
        return recommendType;
    }

    public void setRecommendType(Byte recommendType) {
        this.recommendType = recommendType;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}