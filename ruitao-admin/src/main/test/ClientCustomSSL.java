/*
 * ====================================================================
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 */

import wx.wechat.service.mp.MPService;

import java.util.Map;

/**
 * This example demonstrates how to create secure connections with a custom SSL
 * context.
 */
public class ClientCustomSSL {

    public final static void main(String[] args) throws Exception {
        String path = "/Users/neal/work/bitbucket.org/ruitao-server-side/ruitao-admin/src/main/resources/wxpaycert/apiclient_cert.p12";
        MPService mp = new MPService();
//        Map<String, Object> s = mp.transfers("2","o7E6KuHcQAEpTe-ljP6IIhLK15Gg", "马英乘", 100, "192.168.1.1", path);
//        System.out.println(s);

//        Map<String, Object> s1 = mp.gettransferinfo("1000018301201708189227021205", path);
//        Map<String, Object> s1 = mp.gettransferinfo("1000018301201708189217388003", path);
//        Map<String, Object> s1 = mp.gettransferinfo("1000018301201708189223134713", path);
        Map<String, Object> s1 = mp.gettransferinfo("reward3", path);
        System.out.println(s1);
    }
}
