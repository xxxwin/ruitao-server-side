package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.MeetingUser;
import com.ruitaowang.core.domain.MeetingUserLink;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.MeetingUserLinkService;
import com.ruitaowang.goods.service.MeetingUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@RestController
public class APIMeetingUserController extends BaseController {

    @Autowired
    private MeetingUserService meetingUserService;
    @Autowired
    private MeetingUserLinkService meetingUserLinkService;

    @RequestMapping("/api/meetingusers")
    public String list(MeetingUserLink meetingUserLink, int page) {
        //link
        meetingUserLink.setPage(page);
        List<MeetingUserLink> meetingUserLinks = meetingUserLinkService.selectForPage(meetingUserLink);
        List<MeetingUser> meetingUsers = new ArrayList<>();
        if(!ObjectUtils.isEmpty(meetingUserLinks)){
            meetingUserLinks.forEach(item -> {
                MeetingUser meetingUser = meetingUserService.selectByPhone(item.getPhone());
                if(!ObjectUtils.isEmpty(meetingUser)){
                    //是否签到
                    meetingUser.setLocked(item.getLocked());
                    meetingUser.setRstatus(item.getRstatus());
                    meetingUsers.add(meetingUser);
                }
            });
        }
        MsgOut o = MsgOut.success(meetingUsers);
        o.setRecords(meetingUserLink.getRecords());
        o.setTotal(meetingUserLink.getTotal());
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetingusers", method = RequestMethod.POST)
    public String create(MeetingUser meetingUser) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        meetingUserService.insert(meetingUser);
        MsgOut o = MsgOut.success(meetingUser);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetingusers", method = RequestMethod.PUT)
    public String update(MeetingUser meetingUser) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        meetingUserService.update(meetingUser);
        MsgOut o = MsgOut.success(meetingUser);
        return this.renderJson(o);
    }


    @RequestMapping(value = "/api/meetingusers/{meetingUserId}", method = RequestMethod.DELETE)
    public String delete(@PathVariable("meetingUserId") Long meetingUserId) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        MsgOut o = MsgOut.success(meetingUserService.delete(meetingUserId));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetingusers/auditSignIn", method = RequestMethod.GET)
    public String auditSignIn(String phone, int rstatus) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        MeetingUserLink temp = meetingUserLinkService.selectByPhone(phone);
        temp.setRstatus((byte) rstatus);
        meetingUserLinkService.update(temp);
        MsgOut o = MsgOut.success(temp);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetingusers/auditCost", method = RequestMethod.GET)
    public String auditCost(String phone, String locked) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        MeetingUserLink temp = meetingUserLinkService.selectByPhone(phone);
        temp.setLocked(Boolean.valueOf(locked));
        meetingUserLinkService.update(temp);
        MsgOut o = MsgOut.success(temp);
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/meetingusers/showData", method = RequestMethod.GET)
    public String showData(Long meetingId) {
        if (ObjectUtils.isEmpty(LYSecurityUtil.currentSysUser())) {
            return this.renderJson(MsgOut.error("plz login."));
        }
        MeetingUserLink where = new MeetingUserLink();
        where.setMeetingId(meetingId);
        where.setRstatus(null);
        long signIn = meetingUserLinkService.select(where).size();

        where.setRstatus((byte) 1);
        long signUp = meetingUserLinkService.select(where).size();
        where.setCtime(signIn);
        where.setMtime(signUp);
        MsgOut o = MsgOut.success(where);
        return this.renderJson(o);
    }

}
