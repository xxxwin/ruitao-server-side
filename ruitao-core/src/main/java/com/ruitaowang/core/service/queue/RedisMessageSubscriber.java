package com.ruitaowang.core.service.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neal on 12/09/2017.
 */
public class RedisMessageSubscriber implements MessageListener{
    public static final Logger LOGGER = LoggerFactory.getLogger(RedisMessageSubscriber.class);
    public static List<String> messageList = new ArrayList();

    public void onMessage(final Message message, final byte[] pattern) {
        messageList.add(message.toString());
        LOGGER.debug("Message received: " + new String(message.getBody()));
    }
}
