package com.ruitaowang.wap;

import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyProfit;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.WalletCompany;
import com.ruitaowang.core.utils.TimeUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyProfitService;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.WalletCompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class APIWapWalletController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private WalletCompanyService walletCompanyService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private CompanyProfitService companyProfitService;

    /**
     * 商家钱包（旧版）
     *
     * @param walletCompany
     * @return
     */
    @RequestMapping("/api/wap/company/wallet")
    public String wapFindWalletCompanyForPage(WalletCompany walletCompany, @RequestHeader(value = "headId", required = false) String userId) {
        if (!ObjectUtils.isEmpty(walletCompany.getSidx())) {
            walletCompany.setOrderBy(walletCompany.getSidx() + " " + walletCompany.getSord() + "," + walletCompany.getOrderBy());
        }
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        SysUser user = sysUserService.selectByPK(Long.parseLong(userId));
        List listOut = new ArrayList();
        Company company = companyService.selectByUserId(user.getId());
        if (!ObjectUtils.isEmpty(company)) {
            walletCompany.setCompanyId(company.getCompanyId());
            List<WalletCompany> list = walletCompanyService.selectForPage(walletCompany);
            if (!ObjectUtils.isEmpty(list)) {
                listOut.add(list.get(0));
            } else {
                WalletCompany newWalletCompany = new WalletCompany();
                newWalletCompany.setCompanyId(company.getCompanyId());
                newWalletCompany.setCompanyType(company.getCompanyType());
                walletCompanyService.insert(newWalletCompany);
                listOut.add(newWalletCompany);
            }
        }
        MsgOut o = MsgOut.success(listOut);
        return this.renderJson(o);
    }

    /**
     * 我的钱包（商家版新版）
     * @param userId
     * @return
     */
    @RequestMapping("/api/wap/myWallet")
    public String myWallet(@RequestHeader(value = "headId", required = false) String userId){
        if (ObjectUtils.isEmpty(userId)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        Map map = new HashMap();
        Company company = companyService.selectByCompanyStaff(Long.parseLong(userId));
        List<WalletCompany> walletOut = new ArrayList();
        if (!ObjectUtils.isEmpty(company)) {
            WalletCompany walletCompany = new WalletCompany();
            walletCompany.setCompanyId(company.getCompanyId());
            List<WalletCompany> list = walletCompanyService.selectForPage(walletCompany);
            if (!ObjectUtils.isEmpty(list)) {
                walletOut.add(list.get(0));
            } else {
                walletCompany.setCompanyType(company.getCompanyType());
                walletCompanyService.insert(walletCompany);
                walletOut.add(walletCompany);
            }
        }
        //昨天开始时间
        Long stime = TimeUtils.getLongDayBegin() - 86400000L;
        //昨天结束时间
        Long etime = TimeUtils.getLongDayEnd() - 86400000L;
        CompanyProfit companyProfit = new CompanyProfit();
        companyProfit.setStime(stime);
        companyProfit.setEtime(etime);
        companyProfit.setOrderType((byte)1);
        companyProfit.setCompanyId(company.getCompanyId());
        Long beforeMoney = companyProfitService.beforeMoney(companyProfit);

        //今天开始时间
        Long todayStime = TimeUtils.getLongDayBegin();
        //今天结束时间
        Long todayEtime = TimeUtils.getLongDayEnd();
        companyProfit.setStime(todayStime);
        companyProfit.setEtime(todayEtime);
        Long todayMoney = companyProfitService.beforeMoney(companyProfit);
        companyProfit.setOrderType((byte)-1);
        Long todayTxMoney = companyProfitService.beforeMoney(companyProfit);

        //今日总资产
        map.put("companyBalance",walletOut.get(0).getCompanyBalance());
        //昨日净收入
        map.put("beforeMoney",beforeMoney);
        //在途订单
        map.put("downMoney",0);
        //今日收入
        map.put("todayMoney",todayMoney);
        //今日支出金额
        map.put("todayTxMoney",todayTxMoney);
        MsgOut o = MsgOut.success(map);
        return this.renderJson(o);
    }
}
