package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.UserHistoricalTrace;
import com.ruitaowang.core.domain.UserLock;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.UserCommunicationMapper;
import com.ruitaowang.goods.dao.UserHistoricalTraceMapper;
import com.ruitaowang.goods.dao.UserLockMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 1/19/2017.
 */
@Service
public class UserHistoricalTraceService implements BaseService<UserHistoricalTrace> {
    @Autowired
    private UserHistoricalTraceMapper userHistoricalTraceMapper;

    @Override
    public UserHistoricalTrace insert(UserHistoricalTrace userHistoricalTrace) {
        Long time = System.currentTimeMillis();
        userHistoricalTrace.setCtime(time);
        userHistoricalTrace.setMtime(time);
        userHistoricalTraceMapper.insert(userHistoricalTrace);
        return userHistoricalTrace;
    }

    @Override
    public int delete(Long id) {
        UserHistoricalTrace userHistoricalTrace = new UserHistoricalTrace();
        userHistoricalTrace.setId(id);
        userHistoricalTrace.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        userHistoricalTrace.setMtime(time);
        return userHistoricalTraceMapper.updateByPK(userHistoricalTrace);
    }

    @Override
    public UserHistoricalTrace update(UserHistoricalTrace userHistoricalTrace) {
        Long time = System.currentTimeMillis();
        userHistoricalTrace.setMtime(time);
        userHistoricalTraceMapper.updateByPK(userHistoricalTrace);
        return userHistoricalTrace;
    }

    @Override
    public UserHistoricalTrace selectByPK(Long id) {
        return userHistoricalTraceMapper.selectByPK(id);
    }

    @Override
    public List<UserHistoricalTrace> select(UserHistoricalTrace userHistoricalTrace) {
        userHistoricalTrace.setRstatus((byte) 0);
        return userHistoricalTraceMapper.select(userHistoricalTrace);
    }

    public List<UserHistoricalTrace> selectForPage(UserHistoricalTrace userHistoricalTrace) {
        userHistoricalTrace.setRstatus((byte) 0);
        userHistoricalTrace.setRecords(userHistoricalTraceMapper.count(userHistoricalTrace));
        return userHistoricalTraceMapper.select(userHistoricalTrace);
    }

    public void createUserHistoricalTrace(UserHistoricalTrace userHistoricalTrace) {
        List<UserHistoricalTrace> userHistoricalTraces=userHistoricalTraceMapper.select(userHistoricalTrace);
        Long time = System.currentTimeMillis();
        if(ObjectUtils.isEmpty(userHistoricalTraces)){
            userHistoricalTrace.setCtime(time);
            userHistoricalTrace.setMtime(time);
            userHistoricalTraceMapper.insert(userHistoricalTrace);
        }else {
            userHistoricalTrace.setMtime(time);
            userHistoricalTrace.setId(userHistoricalTraces.get(0).getId());
            userHistoricalTraceMapper.updateByPK(userHistoricalTrace);
        }
    }
}
