package com.ruitaowang.core.domain;

public class OrderProd extends Page {
    /**
     *  编号,所属表字段为order_prod.prod_id
     */
    private Long prodId;

    /**
     *  订单编号,所属表字段为order_prod.order_id
     */
    private Long orderId;

    /**
     *  创建时间,所属表字段为order_prod.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为order_prod.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常，1 删除,所属表字段为order_prod.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为order_prod.goods_name
     */
    private String goodsName;

    /**
     *  ,所属表字段为order_prod.goods_screen_price
     */
    private Integer goodsScreenPrice;

    /**
     *  ,所属表字段为order_prod.goods_real_price
     */
    private Integer goodsRealPrice;

    /**
     *  ,所属表字段为order_prod.goods_attr_values
     */
    private String goodsAttrValues;

    /**
     *  属性ID，以逗号分隔.eg: 1，2，3,所属表字段为order_prod.goods_attr_link_id
     */
    private String goodsAttrLinkId;

    /**
     *  购买数量,所属表字段为order_prod.goods_number
     */
    private Integer goodsNumber;

    /**
     *  ,所属表字段为order_prod.goods_thum
     */
    private String goodsThum;

    /**
     *  ,所属表字段为order_prod.self_support
     */
    private Boolean selfSupport;

    /**
     *  ,所属表字段为order_prod.remark
     */
    private String remark;

    /**
     *  商品编号,所属表字段为order_prod.goods_id
     */
    private Long goodsId;

    /**
     *  物流公司ID,所属表字段为order_prod.logistics_id
     */
    private Long logisticsId;

    /**
     *  物流公司名称,所属表字段为order_prod.logistics_name
     */
    private String logisticsName;

    /**
     *  物流单号,所属表字段为order_prod.logistics_sn
     */
    private String logisticsSn;

    /**
     *  运费,所属表字段为order_prod.xp_price
     */
    private Integer xpPrice;

    /**
     *  ,所属表字段为order_prod.goods_pay_type
     */
    private Byte goodsPayType;

    /**
     *  ,所属表字段为order_prod.goods_score
     */
    private Integer goodsScore;

    /**
     *  供货商编号,所属表字段为order_prod.goods_provider_id
     */
    private Long goodsProviderId;

    /**
     *  送的积分,所属表字段为order_prod.give_score
     */
    private Integer giveScore;

    /**
     *  0 供货商产品
1 微商,所属表字段为order_prod.give_type
     */
    private Byte giveType;

    /**
     *  ,所属表字段为order_prod.goods_provider_type
     *  8.供应商
     */
    private Byte goodsProviderType;

    /**
     *  0 邮寄
1 自提,所属表字段为order_prod.goods_get_type
     */
    private Byte goodsGetType;

    /**
     *  ,所属表字段为order_prod.member_price
     */
    private Integer memberPrice;

    /**
     *  公司余额,所属表字段为order_prod.company_balance
     */
    private Integer companyBalance;

    /**
     *  ,所属表字段为order_prod.company_coupon_id
     */
    private Long companyCouponId;

    /**
     *  ,所属表字段为order_prod.company_coupon_price
     */
    private Integer companyCouponPrice;

    /**
     1 自提,所属表字段为order_prod.order_type
     */
    private Byte orderType;

    /**
     *  是否评价，所属表字段为order_prod.evaluate
     */
    private Byte evaluate;

    public Byte getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(Byte evaluate) {
        this.evaluate = evaluate;
    }

    public Byte getOrderType() {
        return orderType;
    }

    public void setOrderType(Byte orderType) {
        this.orderType = orderType;
    }

    public Long getProdId() {
        return prodId;
    }

    public void setProdId(Long prodId) {
        this.prodId = prodId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName == null ? null : goodsName.trim();
    }

    public Integer getGoodsScreenPrice() {
        return goodsScreenPrice;
    }

    public void setGoodsScreenPrice(Integer goodsScreenPrice) {
        this.goodsScreenPrice = goodsScreenPrice;
    }

    public Integer getGoodsRealPrice() {
        return goodsRealPrice;
    }

    public void setGoodsRealPrice(Integer goodsRealPrice) {
        this.goodsRealPrice = goodsRealPrice;
    }

    public String getGoodsAttrValues() {
        return goodsAttrValues;
    }

    public void setGoodsAttrValues(String goodsAttrValues) {
        this.goodsAttrValues = goodsAttrValues == null ? null : goodsAttrValues.trim();
    }

    public String getGoodsAttrLinkId() {
        return goodsAttrLinkId;
    }

    public void setGoodsAttrLinkId(String goodsAttrLinkId) {
        this.goodsAttrLinkId = goodsAttrLinkId == null ? null : goodsAttrLinkId.trim();
    }

    public Integer getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Integer goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public String getGoodsThum() {
        return goodsThum;
    }

    public void setGoodsThum(String goodsThum) {
        this.goodsThum = goodsThum == null ? null : goodsThum.trim();
    }

    public Boolean getSelfSupport() {
        return selfSupport;
    }

    public void setSelfSupport(Boolean selfSupport) {
        this.selfSupport = selfSupport;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getLogisticsId() {
        return logisticsId;
    }

    public void setLogisticsId(Long logisticsId) {
        this.logisticsId = logisticsId;
    }

    public String getLogisticsName() {
        return logisticsName;
    }

    public void setLogisticsName(String logisticsName) {
        this.logisticsName = logisticsName == null ? null : logisticsName.trim();
    }

    public String getLogisticsSn() {
        return logisticsSn;
    }

    public void setLogisticsSn(String logisticsSn) {
        this.logisticsSn = logisticsSn == null ? null : logisticsSn.trim();
    }

    public Integer getXpPrice() {
        return xpPrice;
    }

    public void setXpPrice(Integer xpPrice) {
        this.xpPrice = xpPrice;
    }

    public Byte getGoodsPayType() {
        return goodsPayType;
    }

    public void setGoodsPayType(Byte goodsPayType) {
        this.goodsPayType = goodsPayType;
    }

    public Integer getGoodsScore() {
        return goodsScore;
    }

    public void setGoodsScore(Integer goodsScore) {
        this.goodsScore = goodsScore;
    }

    public Long getGoodsProviderId() {
        return goodsProviderId;
    }

    public void setGoodsProviderId(Long goodsProviderId) {
        this.goodsProviderId = goodsProviderId;
    }

    public Integer getGiveScore() {
        return giveScore;
    }

    public void setGiveScore(Integer giveScore) {
        this.giveScore = giveScore;
    }

    public Byte getGiveType() {
        return giveType;
    }

    public void setGiveType(Byte giveType) {
        this.giveType = giveType;
    }

    public Byte getGoodsProviderType() {
        return goodsProviderType;
    }

    public void setGoodsProviderType(Byte goodsProviderType) {
        this.goodsProviderType = goodsProviderType;
    }

    public Byte getGoodsGetType() {
        return goodsGetType;
    }

    public void setGoodsGetType(Byte goodsGetType) {
        this.goodsGetType = goodsGetType;
    }

    public Integer getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(Integer memberPrice) {
        this.memberPrice = memberPrice;
    }

    public Integer getCompanyBalance() {
        return companyBalance;
    }

    public void setCompanyBalance(Integer companyBalance) {
        this.companyBalance = companyBalance;
    }

    public Long getCompanyCouponId() {
        return companyCouponId;
    }

    public void setCompanyCouponId(Long companyCouponId) {
        this.companyCouponId = companyCouponId;
    }

    public Integer getCompanyCouponPrice() {
        return companyCouponPrice;
    }

    public void setCompanyCouponPrice(Integer companyCouponPrice) {
        this.companyCouponPrice = companyCouponPrice;
    }
}