package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.RebateRatio;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.RebateRatioMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class RebateRatioService implements BaseService<RebateRatio>{

    @Autowired
    private RebateRatioMapper rebateRatioMapper;


    @Override
    public List<RebateRatio> select(RebateRatio rebateRatio) {
        rebateRatio.setRstatus((byte) 0);
        return rebateRatioMapper.select(rebateRatio);
    }

    @Override
    public RebateRatio insert(RebateRatio rebateRatio) {
        Long time = System.currentTimeMillis();
        rebateRatio.setCtime(time);
        rebateRatio.setMtime(time);
        rebateRatioMapper.insert(rebateRatio);
        return rebateRatio;
    }

    @Override
    public int delete(Long id) {
        return 0;
    }

    @Override
    public RebateRatio update(RebateRatio rebateRatio) {
        rebateRatioMapper.updateByPK(rebateRatio);
        return rebateRatio;
    }

    @Override
    public RebateRatio selectByPK(Long id) {
        return rebateRatioMapper.selectByPK(id);
    }


    public List<RebateRatio> selectForPage(RebateRatio rebateRatio) {
        rebateRatio.setRstatus((byte) 0);
        rebateRatio.setRecords(rebateRatioMapper.count(rebateRatio));
        return rebateRatioMapper.select(rebateRatio);
    }
}
