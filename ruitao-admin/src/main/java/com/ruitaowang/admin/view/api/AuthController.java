/*
 * Copyright 2014-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.utils.RandomUtils;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController extends BaseController{

	@RequestMapping("/auth/token")
	public String token(){
		SysUser user = LYSecurityUtil.currentSysUser();
		if(ObjectUtils.isEmpty(user)){
			return this.renderJson(MsgOut.error("Plz login."));
		}
		String token = null;
		try {
			token = RandomUtils.encodeToken(user.getId());
		} catch (Exception e) {
			return this.renderJson(MsgOut.error("Token Error."));
		}
		return this.renderJson(MsgOut.success(token));
	}
}
