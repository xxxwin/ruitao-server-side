package com.ruitaowang.core.domain;

import lombok.Data;

@Data
public class GoodsSpecifications extends Page {
    /**
     *  ,所属表字段为goods_type_custom.id
     */
    private Long id;

    /**
     *  分类名称,所属表字段为goods_type_custom.name
     */
    private String name;

    /**
     *  ,所属表字段为goods_type_custom.rstatus
     */
    private Byte status;

    /**
     *  备注,所属表字段为goods_type_custom.remark
     */
    private String remark;

    /**
     *  ,所属表字段为goods_type_custom.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为goods_type_custom.mtime
     */
    private Long mtime;
}