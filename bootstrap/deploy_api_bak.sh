#!/usr/bin/env bash
/usr/bin/ssh -t -t ruitao@int.bak.admin.ruitaowang.com /bin/bash << EOF

cd /home/ruitao

ps aux |grep tomcat |grep -v 'grep' |awk '{print $2}'|xargs kill -9

rm -rf /home/ruitao/tomcat/webapps/*

cp /home/ruitao/ROOT.war /home/ruitao/tomcat/webapps/ROOT.war

sh /home/ruitao/tomcat/bin/startup.sh

tail -f /home/ruitao/tomcat/logs/catalina.out

EOF

echo 'Uploaded...'