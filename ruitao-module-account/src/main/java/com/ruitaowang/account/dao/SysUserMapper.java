package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.domain.SysUserVO;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface SysUserMapper {
    int insert(SysUser sysUser);

    int deleteByPK(Long id);

    int updateByPK(SysUser sysUser);

    SysUser selectByPK(Long id);

    long count(SysUser sysUser);

    List<SysUser> select(SysUser sysUser);

    @Select("SELECT count(*) FROM rtshop.t_sys_user where ctime > #{stime} and ctime < #{etime}")
    long countDayData(@Param("stime") Long stime,@Param("etime") Long etime);

    @Select("SELECT count(*) FROM rtshop.t_sys_user where ctime < #{etime}")
    long countDayAll(@Param("etime") Long etime);
}