package com.ruitaowang.account.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ruitaowang.account.dao.CompanyStaffMapper;
import com.ruitaowang.core.domain.CompanyStaff;
import com.ruitaowang.core.service.BaseService;

@Service
public class CompanyStaffService implements BaseService<CompanyStaff> {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    
    @Autowired
    private CompanyStaffMapper companyStaffMapper;

    public CompanyStaff insert(CompanyStaff companyStaff) {
        Long time = System.currentTimeMillis();
        companyStaff.setCtime(time);
        companyStaff.setMtime(time);
        companyStaffMapper.insert(companyStaff);
        return companyStaff;
    }

    public int delete(Long id) {
        return companyStaffMapper.deleteByPK(id);
    }

    public CompanyStaff update(CompanyStaff companyStaff) {
        Long time = System.currentTimeMillis();
        companyStaff.setMtime(time);
        companyStaffMapper.updateByPK(companyStaff);
        return companyStaff;
    }

    public CompanyStaff selectByPK(Long id) {
        return companyStaffMapper.selectByPK(id);
    }

    public List<CompanyStaff> select(CompanyStaff companyStaff) {
        return companyStaffMapper.select(companyStaff);
    }
    
}



