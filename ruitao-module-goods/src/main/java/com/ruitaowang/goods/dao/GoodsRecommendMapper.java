package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.GoodsRecommend;
import java.util.List;

public interface GoodsRecommendMapper {
    int insert(GoodsRecommend goodsRecommend);

    int deleteByPK(Long id);

    int updateByPK(GoodsRecommend goodsRecommend);

    GoodsRecommend selectByPK(Long id);

    long count(GoodsRecommend goodsRecommend);

    List<GoodsRecommend> select(GoodsRecommend goodsRecommend);
}