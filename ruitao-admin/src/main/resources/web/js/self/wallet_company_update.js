/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by neal on 11/22/16.
 */

var Config = {
    // createUrl: '/api/admin/company/wallet',
    // updateUrl: '/api/admin/company/wallet',
    // deleteUrl: '/api/admin/company/wallet',
    selectUrl: '/api/admin/companyProfit/get'
};
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
};
$(document).ready(function () {
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url:Config.selectUrl,
        postData: {companyId: getQueryString("id"),orderId:getQueryString("orderId")},
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "订单ID","订单SN","订单类型","商户ID", "消费者名称","总金额","收益金额","手续费","创建时间","操作"],
        colModel: [{
            name: "profitId",
            index: "profitId",
            editable: false,
            sorttype: "int",
            search: true
        }, {
            name: "orderId",
            index: "orderId"
        }, {
            name: "orderSn",
            index: "orderSn"
        },{
            name: "orderType",
            index: "orderType",
            editoptions:{value:"0:商城供货商订单返利;2:商城微商订单返利;4:商城微商订单返红包;7:云转返利;8:云省返利;9:云签返利;10:云飞返利;11:云推返利;12:云点返利;13:云联返利；14:云促销返利；15:快赚大礼包;16:云动返利;17:互动店霸屏奖励;18:互动店打赏奖励;18:互动店打赏奖励;19:互动店抢红包积分;20:区级返利;21:市级返利;22:省级返利;-1:提现;-2:退款扣除奖励;-3:红包支出"},
            editable: true,
            edittype: 'select',
            formatter: 'select'
        },{
            name: "companyId",
            index: "companyId"
        },{
            name: "remark",
            index: "remark"
        },{
            name: "amount",
            index: "amount"
        },{
            name: "companyProfit",
            index: "companyProfit"
        },{
            name: "moneyReduce",
            index: "moneyReduce",
            editable: true
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "profitId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list',{ add: false, edit: false, del: false });
    $(window).bind("resize",function(){var width=$(".jqGrid_wrapper").width();$("#data_list").setGridWidth(width);});

});
function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS"){
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    }else{
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
}
// $("#perform_search").click(function() {
//     var postdata = $("#data_list").jqGrid('getGridParam','postData');
//     postdata._search = false;
//     postdata.companyType = $('#companyType option:selected') .val();
//     postdata.companyName=$('#companyName').val();
//     jQuery("#data_list").trigger("reloadGrid",[{page:1}]);
// });
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid ('getRowData', id);
        dataFromTheRow.ctime=new Date(dataFromTheRow.ctime*1).yyyymmddhhmmss();
        var html = '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/api/admin/companyProfit/excel/'+id+'" >订单导出</a>';
        jQuery("#data_list").jqGrid('setRowData', ids[i], {
            operate:html,
            //companyBalance: toFixedForPrice(dataFromTheRow.companyBalance),
            companyProfit: toFixedForPrice(dataFromTheRow.companyProfit),
            amount: toFixedForPrice(dataFromTheRow.amount),
            moneyReduce:toFixedForPrice(dataFromTheRow.moneyReduce),
            ctime: dataFromTheRow.ctime});
    }
}