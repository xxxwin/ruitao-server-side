package tech.lingyi.wx.msg.out;

import java.util.HashMap;

/**
 * Created by neal on 18/07/2017.
 */
public class TemplateItem extends HashMap<String, Item> {

    public TemplateItem() {}

    public TemplateItem(String key, Item item) {
        this.put(key, item);
    }
}
