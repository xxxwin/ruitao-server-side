package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.Collection;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.CollectionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CollectionService implements BaseService<Collection>{

    @Autowired
    private CollectionMapper collectionMapper;


    @Override
    public Collection insert(Collection collection) {
        Long time = System.currentTimeMillis();
        collection.setCtime(time);
        collection.setMtime(time);
        collectionMapper.insert(collection);
        return collection;
    }

    @Override
    public int delete(Long id) {
        Collection collection = new Collection();
        collection.setId(id);
        collection.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        collection.setMtime(time);
        return collectionMapper.updateByPK(collection);
    }

    @Override
    public Collection update(Collection collection) {
        Long time = System.currentTimeMillis();
        collection.setMtime(time);
        collectionMapper.updateByPK(collection);
        return collection;
    }

    @Override
    public Collection selectByPK(Long id) {

        return collectionMapper.selectByPK(id);
    }

    @Override
    public List<Collection> select(Collection collection) {
        collection.setRstatus((byte) 0);
        return collectionMapper.select(collection);
    }

    public List<Collection> selectForPage(Collection collection) {
        collection.setRstatus((byte) 0);
        collection.setRecords(collectionMapper.count(collection));
        return collectionMapper.select(collection);
    }

    public Long companyCount(Collection collection){
        collection.setRstatus((byte) 0);
        return collectionMapper.count(collection);
    }

}
