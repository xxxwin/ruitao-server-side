/**
 * Created by Shaka on 11/22/16.
 */

var Config = {
    updateUrl: '/api/admin/orders',
    selectProviderInfoUrl: '/api/admin/company/goodsProviders',
    selectCompanyUrl: '/api/admin/company/providers',
    updateProviderUrl:'/api/goods',
    companyNameUrl: '/api/admin/orderProd/companyName',
    selectLogisticsNameUrl: '/api/admin/logistics/name',
    updateLogisticsUrl:'/api/admin/orderProd/logistics',
    logisticsSnUrl:'/api/admin/orderProd/logistics',
    logisticsNameUrl:"/api/admin/orderProd/ByPK"
};
/***/
$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});
function set() {
    $("#orderAmount").text(toFixedForPrice($("#orderAmount").text()));
    var payStatus;
    switch (Number($("#payStatus").text())){
        case 0:
            payStatus = "未支付";
            break;
        case 1:
            payStatus = "已支付";
            break;
    }
    $("#payStatus").text(payStatus);
    var auditStatus;
    switch (Number($("#auditStatus").text())){
        case 0:
            auditStatus = "未审核";
            break;
        case 1:
            auditStatus = "审核通过";
            break;
        case 2:
            auditStatus = "审核未通过";
            break;
    }
    $("#auditStatus").text(auditStatus);
    var orderStatus;
    switch (Number($("#orderStatus").text())){
        case 0:
            orderStatus = "待发货";
            break;
        case 1:
            orderStatus = "已发货";
            break;
        case 2:
            orderStatus = "已完成";
            break;
        case 3:
            orderStatus = "买家申请退货";
            break;
        case 4:
            orderStatus = "已申请退款";
            break;
        case 5:
            orderStatus = "交易关闭";
            break;
        case 6:
            orderStatus = "已配货";
            break;
    }
    $("#orderStatus").text(orderStatus);

}
$().ready(function () {
    set();
    $('#submitUpdate').click( function() {
        $("#ajaxform").validate();
        if ($('#ajaxform').valid()){
            var postData = $('#ajaxform').serializeArray();
            $.ajax(
                {
                    url : Config.updateUrl,
                    type: "PUT",
                    data : postData,
                    dataType: "json",
                    success:function(data, textStatus, jqXHR)
                    {
                        toastr.success(data.msg, data.title);
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        toastr.error(errorThrown, textStatus);
                    }
                });
        }

        return false;
    });
    // $.ajax(
    //     {
    //         url: Config.selectCompanyUrl,
    //         type: "GET",
    //         //data: {goodsId: goodsId,goodsProviderId: providId},
    //         dataType: "json",
    //         async: false,
    //         success: function (data, textStatus, jqXHR) {
    //             var categories = [];
    //             categories.push('<option value=""></option>');
    //             data.data.map(function (company) {
    //                 categories.push('<option value="');
    //                 categories.push(company.companyId);
    //                 categories.push('" ');
    //                 categories.push('hassubinfo="true">');
    //                 categories.push(company.companyName);
    //                 categories.push('</option>');
    //             });
    //             $(".goodsProviderId").append(categories.join(''));
    //             var trs = $(".shop").find("tbody tr");
    //             $.each(trs,function(i,v){
    //                 var prodId = $(v).find("td.goods input[class=prodId]").val();
    //                 var logisticsName = $(v).find("td.selectLogisticsName select[name=logisticsName]");
    //                 var sle = $(v).find("td.selectContent select[name=ProviderId]");
    //                 var sle1 = $(sle).find("option:nth-child(1)");
    //                 var logisticsName1 = $(logisticsName).find("option:nth-child(1)");
    //                 var sle2 = $(v).find("td.selectContent1 input[name=goodsProviderId]");
    //                 $(v).find("td.goodsScreenPrice").text(toFixedForPrice($(v).find("td.goodsScreenPrice").text()));
    //                 $(v).find("td.goodsRealPrice").text(toFixedForPrice($(v).find("td.goodsRealPrice").text()));
    //                 $(v).find("td.goodsScore").text(toFixedForPrice($(v).find("td.goodsScore").text()));
    //                 $(v).find("td.ctime").text(new Date(($(v).find("td.ctime").text())*1).yyyymmddhhmmss());
    //                 var goodsProviderId1 = $(v).find("td button input.goodsProviderId1");
    //                 var returnType;
    //                 switch (Number($(v).find("td.thisReturnType").html())){
    //                     case 0:
    //                         returnType = "退货退款";
    //                         break;
    //                     case 1:
    //                         returnType = "退款";
    //                         break;
    //                     case 2:
    //                         returnType = "换货";
    //                         break;
    //                     case 4:
    //                         returnType = "正常";
    //                         break;
    //                 }
    //                 $(v).find("td.thisReturnType").html(returnType);
    //                 $.ajax({
    //                     url: Config.companyNameUrl,
    //                     type: "GET",
    //                     dataType: "json",
    //                     data:{prodId: prodId},
    //                     success:function(txt){
    //                         $(sle1).text(txt.data.companyName);
    //                         $(sle2).val(txt.data.companyId);
    //                         if(txt.data.companyName = ""){
    //                             $(sle1).html("请选择  ======");
    //                         }else{
    //                             $(goodsProviderId1).val(txt.data.companyId);
    //                         }
    //                     }
    //                 });
    //                 $.ajax(
    //                     {
    //                         url: Config.selectLogisticsNameUrl,
    //                         type: "GET",
    //                         dataType: "json",
    //                         async: false,
    //                         success: function (data, textStatus, jqXHR) {
    //                             var logisticsName = [];
    //                             logisticsName.push('<option value=""></option>');
    //                             data.data.map(function (logistics) {
    //                                 logisticsName.push('<option value="');
    //                                 logisticsName.push(logistics.logisticsId);
    //                                 logisticsName.push('" ');
    //                                 logisticsName.push('hassubinfo="true">');
    //                                 logisticsName.push(logistics.name);
    //                                 logisticsName.push('</option>');
    //                             });
    //                             $(".logisticsName").append(logisticsName.join(''));
    //                             $.ajax({
    //                                 url: Config.logisticsNameUrl,
    //                                 type: "GET",
    //                                 dataType: "json",
    //                                 data:{prodId: prodId},
    //                                 success:function(txt){
    //
    //                                     $(".logisticsName").val(txt.data.logisticsId);
    //                                 }
    //                             });
    //
    //                         }  }  );
    //             });
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             toastr.error(errorThrown, textStatus);
    //         }
    //     });
    //
    // var providId = "";
    // $(".goodsProviderId").change(function () {
    //     var goodsId=$(this).parent().prev().prev().find("input[class=goodsId]").val();
    //     var prodId=$(this).parent().prev().prev().find("input[class=prodId]").val();
    //     providId = $(this).find("option:selected").val();
    //     var orderId=$("#orderId").text()
    //     $.ajax({
    //         url: Config.updateProviderUrl,
    //         type: "PUT",
    //         dataType: "json",
    //         data: { prodId:prodId,orderId:orderId,goodsId: goodsId, goodsProviderId: providId },
    //         success:function(data, textStatus, jqXHR)
    //         {
    //             toastr.success(data.msg, data.title);
    //         },
    //         error: function(jqXHR, textStatus, errorThrown)
    //         {
    //             toastr.error(errorThrown, textStatus);
    //         }
    //     });
    // });
    // $(".logisticsName").change(function (){
    //     var prodId=$(this).parent().prev().prev().prev().prev().prev().prev().prev().find("input[class=prodId]").val();
    //     var goodsId=$(this).parent().prev().prev().prev().prev().prev().prev().prev().find("input[class=goodsId]").val();
    //     var logisticsId = $(this).find("option:selected").val();
    //     $.ajax({
    //         url: Config.updateLogisticsUrl,
    //         type: "PUT",
    //         dataType: "json",
    //         data: { prodId:prodId,goodsId: goodsId, logisticsId: logisticsId },
    //         success:function(data, textStatus, jqXHR)
    //         {
    //             toastr.success(data.msg, data.title);
    //         },
    //         error: function(jqXHR, textStatus, errorThrown)
    //         {
    //             toastr.error(errorThrown, textStatus);
    //         }
    //     });
    // });
    // $(".logisticsSnA").change(function (){
    //     var prodId=$(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().find("input[class=prodId]").val();
    //     var goodsId=$(this).parent().prev().prev().prev().prev().prev().prev().prev().prev().find("input[class=goodsId]").val();
    //     var logisticsSn = $(this).val();
    //     $.ajax({
    //         url: Config.logisticsSnUrl,
    //         type: "PUT",
    //         dataType: "json",
    //         data: { prodId:prodId,goodsId: goodsId, logisticsSn: logisticsSn },
    //         success:function(data, textStatus, jqXHR)
    //         {
    //             toastr.success(data.msg, data.title);
    //         },
    //         error: function(jqXHR, textStatus, errorThrown)
    //         {
    //             toastr.error(errorThrown, textStatus);
    //         }
    //     });
    // });
    //
    // $('.showProviderInfo').click( function() {
    //     var id=$(this).parent().prev().find("input[class=goodsId]").val();
    //     $.ajax(
    //         {url: Config.selectProviderInfoUrl,
    //             data:{goodsId:id},
    //             type: "GET",
    //             dataType: "json",
    //             success: function (data, textStatus, jqXHR) {
    //                 $("#companyId").text(data.data.companyId);
    //                 $("#companyName").text(data.data.companyName);
    //                 $("#linkman").text(data.data.linkman);
    //                 $("#mobile").text(data.data.mobile);
    //                 $("#address").text(data.data.address);
    //                 $("#remark").text(data.data.remark);
    //             },
    //             error: function (jqXHR, textStatus, errorThrown) {
    //                 toastr.error(errorThrown, textStatus);
    //             }
    //         });
    // });
    // $("#query").click(function () {
    //     console.log(Number($(".logisticsSnA").val()));
    //     var logSn;
    //     switch (Number($(".logisticsName").val())){
    //         case 1:
    //               logSn = "shunfeng";
    //             break;
    //         case 2:
    //             logSn = "yuantong";
    //             break;
    //         case 3:
    //             logSn ="zhongtong";
    //             break;
    //         case 4:
    //             logSn ="shentong";
    //             break;
    //         case 5:
    //             logSn ="zhaijisong";
    //             break;
    //         case 6:
    //             logSn ="ems";
    //             break;
    //         case 7:
    //             logSn ="tiantian";
    //             break;
    //         case 8:
    //             logSn ="baishihuitong";
    //             break;
    //         case 9:
    //             logSn ="kuaijie";
    //             break;
    //     }
    //     if($(".logisticsSnA").val()  && $(".logisticsName").find("option:selected").val()){
    //         console.log(logSn);
    //         var url="http://www.kuaidi100.com/query?type="+logSn+"&postid="+$(".logisticsSnA").val();
    //         var url="http://www.kuaidi100.com/query?type="+logSn+"&postid="+$(".logisticsSnA").val();
    //
    //         layer.open({
    //             type: 2,
    //             title: '物流跟踪',
    //             scrollbar: false,
    //             shadeClose: true,
    //             shade: 0.8,
    //             area: ['100%', '100%'],
    //             content: "/web/express_inquiry.html?url="+url
    //         })
    //     }else {
    //
    //     }
    // });
    $('#buttonGoback').click(function() {
        history.go(-1);
        location.reload();
        return false;
    });


});

$("#fahuo").click(function () {
    if($(this).text() == "已发货"){
        toastr.error("已发货，请不要重复点击！");
        return false;
    }
    var shoppingId = $(this).attr("name");
    $.ajax({
        url: "/api/admin/updateShoppingOrder",
        type: "POST",
        dataType: "json",
        data: { id:shoppingId,orderStatus:1},
        success:function(data, textStatus, jqXHR)
        {
            toastr.success(data.msg, data.title);
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            toastr.error(errorThrown, textStatus);
        }
    });
});

function fahuo(shoppingId) {
    console.log("shoppingId == " + shoppingId)
    
}