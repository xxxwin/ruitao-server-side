package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.WxClientCredentail;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.WxClientCredentailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class WxClientCredentialService implements BaseService<WxClientCredentail> {
    @Autowired
    private WxClientCredentailMapper wxClientCredentailMapper;

    @Override
    public WxClientCredentail insert(WxClientCredentail wxClientCredentail) {

        List<WxClientCredentail> list = wxClientCredentailMapper.select(new WxClientCredentail());
        if(ObjectUtils.isEmpty(list)){
            wxClientCredentailMapper.insert(wxClientCredentail);
        }else{
            WxClientCredentail temp = list.get(0);
            temp.setAccessToken(wxClientCredentail.getAccessToken());
            temp.setExpiresIn(wxClientCredentail.getExpiresIn());
            wxClientCredentailMapper.updateByPK(temp);
        }
        return wxClientCredentail;
    }

    @Override
    public int delete(Long id) {
        WxClientCredentail wxClientCredentail = new WxClientCredentail();
        wxClientCredentail.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        wxClientCredentail.setMtime(time);
        return wxClientCredentailMapper.updateByPK(wxClientCredentail);
    }

    @Override
    public WxClientCredentail update(WxClientCredentail wxClientCredentail) {
        Long time = System.currentTimeMillis();
        wxClientCredentail.setMtime(time);
        wxClientCredentailMapper.updateByPK(wxClientCredentail);
        return wxClientCredentail;
    }

    @Override
    public WxClientCredentail selectByPK(Long id) {
        return wxClientCredentailMapper.selectByPK(id);
    }

    @Override
    public List<WxClientCredentail> select(WxClientCredentail wxClientCredentail) {
        wxClientCredentail.setRstatus((byte) 0);
        return wxClientCredentailMapper.select(wxClientCredentail);
    }


}
