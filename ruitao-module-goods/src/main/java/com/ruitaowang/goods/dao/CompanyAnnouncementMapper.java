package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CompanyAnnouncement;

import java.util.List;

public interface CompanyAnnouncementMapper {
    int insert(CompanyAnnouncement companyAnnouncement);

    int deleteByPK(Long id);

    int updateByPK(CompanyAnnouncement companyAnnouncement);

    CompanyAnnouncement selectByPK(Long id);

    long count(CompanyAnnouncement companyAnnouncement);

    List<CompanyAnnouncement> select(CompanyAnnouncement companyAnnouncement);
}