package com.ruitaowang.account.dao;

import com.ruitaowang.core.domain.UserRoleLink;

import java.util.List;

public interface UserRoleLinkAdMapper {

    int deleteByUserId(Long userId);

    List<UserRoleLink> selectBatch(List<Long> userId);
}