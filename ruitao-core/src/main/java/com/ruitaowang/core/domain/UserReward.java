package com.ruitaowang.core.domain;

public class UserReward extends Page {
    /**
     *  主键,所属表字段为user_reward.reward_id
     */
    private Long rewardId;

    /**
     *  主键,所属表字段为user_reward.user_id
     */
    private Long userId;

    /**
     *  备注,所属表字段为user_reward.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为user_reward.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为user_reward.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为user_reward.rstatus
     */
    private Byte rstatus;

    /**
     *  订单金额,所属表字段为user_reward.amount
     */
    private Integer amount;

    /**
     *  0 个人账户， 1 商家账户,所属表字段为user_reward.reward_type
     */
    private Byte rewardType;

    /**
     *  微信对应的真实姓名,所属表字段为user_reward.real_name
     */
    private String realName;

    /**
     *  微信对应的真实身份证号码,所属表字段为user_reward.id_card
     */
    private String idCard;

    /**
     *  微信订单号,所属表字段为user_reward.payment_no
     */
    private String paymentNo;

    /**
     *  银行卡卡号,所属表字段为user_reward.bank_id
     */
    private String bankId;

    /**
     *  银行卡包ID唯一ID,所属表字段为user_reward.bank_card_id
     */
    private Long bankCardId;
    /**
     *  状态 1:微信 2:银行,所属表字段为user_reward.rstatus
     */
    private Byte rewardStatus;

    public Byte getRewardStatus() {
        return rewardStatus;
    }

    public void setRewardStatus(Byte rewardStatus) {
        this.rewardStatus = rewardStatus;
    }

    public Long getBankCardId() {
        return bankCardId;
    }

    public void setBankCardId(Long bankCardId) {
        this.bankCardId = bankCardId;
    }

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public Long getRewardId() {
        return rewardId;
    }

    public void setRewardId(Long rewardId) {
        this.rewardId = rewardId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Byte getRewardType() {
        return rewardType;
    }

    public void setRewardType(Byte rewardType) {
        this.rewardType = rewardType;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public String getPaymentNo() {
        return paymentNo;
    }

    public void setPaymentNo(String paymentNo) {
        this.paymentNo = paymentNo == null ? null : paymentNo.trim();
    }
}