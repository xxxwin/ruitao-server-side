package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Order;
import com.ruitaowang.core.domain.OrderVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2017/5/9.
 */
public interface OrderAdMapper {
    List<Order> select(Order order);

    long count(Order order);

    List<OrderVO> selectByCompany(OrderVO orderVO);

    List<Order> selectForOrder(Order order);

    long selectForOrderCount(Order order);

    List<Order> selectForOrderCompany(OrderVO orderVO);

    long selectForOrderCompanyCount(OrderVO orderVO);

    List<Order> fjOrder(Order order);

    long fjOrderCount(Order order);
}
