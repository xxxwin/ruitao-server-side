/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/dinner/dishes',
    updateUrl: '/api/dinner/dishes',
    deleteUrl: '/api/dinner/dish',
    dishTagsUrl: '/api/dinner/dishTags',
    selectUrl: '/api/dinner/dishs/1'
};


$.validator.setDefaults({
    highlight: function (e) {
        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
    }, success: function (e) {
        e.closest(".form-group").removeClass("has-error").addClass("has-success");
    }, errorElement: "span", errorPlacement: function (e, r) {
        e.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
    }, errorClass: "help-block m-b-none", validClass: "help-block m-b-none"
});

$(document).ready(function () {

    var discountId;

    $.ajax(
        {
            url: '/api/dinner/dish/' + $("#id").val(),
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data, textStatus, jqXHR) {
                if (data && data.data) {
                    discountId = data.data.discountId;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });

    $.ajax(
        {
            url: Config.dishTagsUrl,
            type: "GET",
            dataType: "json",
            // async: false,
            success: function (data, textStatus, jqXHR) {
                if (data && data.data && data.data.length > 0) {
                    var tagsHtml = '';
                    for (var i = 0; i < data.data.length; i++) {
                        tagsHtml += '<label><input name="tags" type="radio" value="' + data.data[i].id + '" />' + data.data[i].name + '</label>';
                    }
                    $("#dishTags").append(tagsHtml);
                    console.log($("#dinnerTagId").val());
                    $('input:radio[name="tags"]').filter('[value="'+$("#dinnerTagId").val()+'"]').attr('checked', true);

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // console.error(errorThrown);
            }
        });
    //
    // $.ajax(
    //     {
    //         url: '/api/localDiscount',
    //         type: "GET",
    //         dataType: "json",
    //         async: false,
    //         success: function (data, textStatus, jqXHR) {
    //             if (data && data.data && data.data.length > 0) {
    //                 var tagsHtml = '<select name="discountId">';
    //                 for (var i = 0; i < data.data.length; i++) {
    //                     var selectTmp = '';
    //                     if (data.data[i].discountId == discountId) {
    //                         selectTmp = 'selected';
    //                     }
    //                     tagsHtml += '<option value="' + data.data[i].discountId + '" ' + selectTmp + '>' + data.data[i].title + ' ' + data.data[i].remark + '</option>';
    //                 }
    //                 tagsHtml += '</select>';
    //                 $("#discount").append(tagsHtml);
    //             }
    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             console.error(errorThrown);
    //         }
    //     });

    //save role
    $('#submitForSave').click(function () {
        var name = $("input[name=name]").val();
        var price = Math.floor($("input[name=price]").val() * 100);
        var remark = $("input[name=remark]").val();
        var tags = $("input[name=tags]:checked").val();
        var imgUrl = $("input[name=imgUrl]").val();
        $.ajax(
            {
                url: Config.updateUrl,
                type: "PUT",
                data: {id:$("#id").val(), name: name, price:Number( price), remark: remark, tags: tags, imgUrl: imgUrl},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        return false;
    });

    $('#buttonGoback').click(function () {
        /*history.go(-1);
         location.reload();*/
        document.location = "/web/admin_dinner_dishCreate.html";
        return false;
    })
});
$('#btn_temporary').click(function () {
    //判断上传控件中是否选择了图片
    var image = $("#file_temporaryImage").val();
    if ($.trim(image) == "") {
        alert("请选择图片！");
        return;
    }
    //提交请求处理的url
    var actionUrl = "/uploadForDish";
    //开始ajax操作
    $("#uploadForm").ajaxSubmit({
        type: "POST",//提交类型
        dataType: "json",//返回结果格式
        url: actionUrl,//请求地址
        success: function (data) {//请求成功后的函数
            if (data.status == "warning") {//返回警告
                alert("上传图片：警告");
            } else if (data.type == "SUCCESS") {//返回成功
                $("#div_temporaryImage").find("img").remove();
                $("#div_temporaryImage").append("<img style='width:300px;height:300px;' src='" + data.msg + "' />");
                $("#imgUrl").val("");
                $("#imgUrl").val(data.msg);
            }
        },
        error: function (data) { alert("上传失败原因：可能图片大于1M"); },//请求失败的函数
        async: true
    });
});

//hello :)~