package com.ruitaowang.core.domain;

public class StatCompany extends Page {
    /**
     *  ,所属表字段为stat_company.id
     */
    private Long id;

    /**
     *  编号,所属表字段为stat_company.company_id
     */
    private Long companyId;

    /**
     *  商城订单数,所属表字段为stat_company.order_count
     */
    private Integer orderCount;

    /**
     *  创建时间,所属表字段为stat_company.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为stat_company.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常，1 删除,所属表字段为stat_company.rstatus
     */
    private Byte rstatus;

    /**
     *  扫码订单数,所属表字段为stat_company.local_order_count
     */
    private Integer localOrderCount;

    /**
     *  商城订单总收入,所属表字段为stat_company.order_income
     */
    private Integer orderIncome;

    /**
     *  备注,所属表字段为stat_company.remark
     */
    private String remark;

    /**
     *  店面订单收入,所属表字段为stat_company.local_order_income
     */
    private Integer localOrderIncome;

    /**
     *  点餐订餐数量,所属表字段为stat_company.dish_order_count
     */
    private Integer dishOrderCount;

    /**
     *  点餐订单收入,所属表字段为stat_company.dish_order_income
     */
    private Integer dishOrderIncome;

    /**
     *  日期,所属表字段为stat_company.date
     */
    private String date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getLocalOrderCount() {
        return localOrderCount;
    }

    public void setLocalOrderCount(Integer localOrderCount) {
        this.localOrderCount = localOrderCount;
    }

    public Integer getOrderIncome() {
        return orderIncome;
    }

    public void setOrderIncome(Integer orderIncome) {
        this.orderIncome = orderIncome;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getLocalOrderIncome() {
        return localOrderIncome;
    }

    public void setLocalOrderIncome(Integer localOrderIncome) {
        this.localOrderIncome = localOrderIncome;
    }

    public Integer getDishOrderCount() {
        return dishOrderCount;
    }

    public void setDishOrderCount(Integer dishOrderCount) {
        this.dishOrderCount = dishOrderCount;
    }

    public Integer getDishOrderIncome() {
        return dishOrderIncome;
    }

    public void setDishOrderIncome(Integer dishOrderIncome) {
        this.dishOrderIncome = dishOrderIncome;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date == null ? null : date.trim();
    }
}