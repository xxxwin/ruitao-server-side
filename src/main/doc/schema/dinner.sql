/*
SQLyog Ultimate v12.09 (32 bit)
MySQL - 5.6.24 : Database - rtshop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rtshop` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `rtshop`;

/*Table structure for table `dinner_dish` */

DROP TABLE IF EXISTS `dinner_dish`;

CREATE TABLE `dinner_dish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `remark` varchar(100) DEFAULT NULL,
  `img_url` varchar(100) DEFAULT NULL,
  `ctime` bigint(15) DEFAULT '0',
  `mtime` bigint(15) DEFAULT '0',
  `rstatus` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='菜品';

/*Table structure for table `dinner_dish_tag_link` */

DROP TABLE IF EXISTS `dinner_dish_tag_link`;

CREATE TABLE `dinner_dish_tag_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(20) NOT NULL,
  `dish_id` bigint(20) NOT NULL,
  `ctime` bigint(20) DEFAULT NULL,
  `mtime` bigint(20) DEFAULT NULL,
  `rstatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Table structure for table `dinner_order` */

DROP TABLE IF EXISTS `dinner_order`;

CREATE TABLE `dinner_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cus_num` smallint(6) DEFAULT NULL COMMENT '就餐人数',
  `order_no` varchar(30) DEFAULT NULL COMMENT '点餐号',
  `company_id` bigint(20) DEFAULT NULL,
  `table_id` bigint(20) NOT NULL COMMENT '就餐餐座',
  `real_pay` decimal(10,2) DEFAULT NULL COMMENT '实际支付',
  `pay_status` tinyint(4) DEFAULT '0' COMMENT '0 未支付\n1 已支付',
  `remark` varchar(50) DEFAULT NULL COMMENT '订单备注',
  `fp_head` varchar(50) DEFAULT NULL COMMENT '发票抬头',
  `user_id` bigint(20) DEFAULT NULL COMMENT '点餐用户',
  `ctime` bigint(15) DEFAULT '0',
  `mtime` bigint(15) DEFAULT '0',
  `rstatus` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

/*Table structure for table `dinner_order_dish_link` */

DROP TABLE IF EXISTS `dinner_order_dish_link`;

CREATE TABLE `dinner_order_dish_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) NOT NULL,
  `dish_id` bigint(20) NOT NULL,
  `num` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `dish_id` (`dish_id`),
  CONSTRAINT `dinner_order_dish_link_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `dinner_order` (`id`),
  CONSTRAINT `dinner_order_dish_link_ibfk_2` FOREIGN KEY (`dish_id`) REFERENCES `dinner_dish` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;

/*Table structure for table `dinner_table` */

DROP TABLE IF EXISTS `dinner_table`;

CREATE TABLE `dinner_table` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `table_number` varchar(20) NOT NULL COMMENT '桌号',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `qr_url` varchar(100) DEFAULT NULL COMMENT '二维码url',
  `ctime` bigint(15) DEFAULT '0',
  `mtime` bigint(15) DEFAULT '0',
  `rstatus` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  CONSTRAINT `dinner_table_ibfk_1` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `dinner_tag` */

DROP TABLE IF EXISTS `dinner_tag`;

CREATE TABLE `dinner_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) NOT NULL,
  `type` smallint(6) NOT NULL COMMENT '标签分类:1-菜品;2-备注选择',
  `name` varchar(30) NOT NULL COMMENT '标签名称',
  `ctime` bigint(20) DEFAULT NULL,
  `mtime` bigint(20) DEFAULT NULL,
  `rstatus` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/* Procedure structure for procedure `showChildLst` */

/*!50003 DROP PROCEDURE IF EXISTS  `showChildLst` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `showChildLst`(IN rootid INT)
BEGIN
    DECLARE Level int ;
    drop TABLE IF EXISTS tmpLst;
    CREATE TABLE tmpLst (
      id int,
      nLevel int,
      sCort varchar(8000)
    );
    Set Level=0 ;
    INSERT into tmpLst SELECT id,Level,ID FROM crm_presona_dk WHERE presona_pid=rootid;
    WHILE ROW_COUNT()>0 DO
      SET Level=Level+1 ;
      INSERT into tmpLst
        SELECT A.id,Level,concat(B.sCort,'-',A.id) FROM crm_presona_dk A,tmpLst B
        WHERE  A.presona_pid=B.id AND B.nLevel=Level-1  ;
    END WHILE;
  END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
