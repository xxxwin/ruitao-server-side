package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.ShoppingOrder;
import com.ruitaowang.core.domain.ShoppingOrderVO;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.ShoppingOrderAdMapper;
import com.ruitaowang.goods.dao.ShoppingOrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

/**
 * Created by neal on 11/07/16.
 */
@Service
public class ShoppingOrderService implements BaseService<ShoppingOrder> {

    @Autowired
    private ShoppingOrderMapper shoppingOrderMapper;
    @Autowired
    private ShoppingOrderAdMapper shoppingOrderAdMapper;

    @Override
    public ShoppingOrder insert(ShoppingOrder shoppingOrder) {
        Long time = System.currentTimeMillis();
        shoppingOrder.setCtime(time);
        shoppingOrder.setMtime(time);
        shoppingOrderMapper.insert(shoppingOrder);
        return shoppingOrder;
    }

    @Override
    public int delete(Long id) {
        ShoppingOrder shoppingOrder = new ShoppingOrder();
        shoppingOrder.setId(id);
        shoppingOrder.setRstatus((byte)1);
        Long time = System.currentTimeMillis();
        shoppingOrder.setMtime(time);
        return shoppingOrderMapper.updateByPK(shoppingOrder);
    }

    @Override
    public ShoppingOrder update(ShoppingOrder shoppingOrder) {
        shoppingOrderMapper.updateByPK(shoppingOrder);
        return shoppingOrder;
    }
    @Override
    public ShoppingOrder selectByPK(Long id) {
        return shoppingOrderMapper.selectByPK(id);
    }

    @Override
    public List<ShoppingOrder> select(ShoppingOrder shoppingOrder) {
        shoppingOrder.setRstatus((byte) 0);
        return shoppingOrderMapper.select(shoppingOrder);
    }
    public List<ShoppingOrder> selectForPage(ShoppingOrder shoppingOrder) {
        shoppingOrder.setRstatus((byte) 0);
        shoppingOrder.setRecords(shoppingOrderMapper.count(shoppingOrder));
        return shoppingOrderMapper.select(shoppingOrder);
    }

    public List<ShoppingOrder> selectForPj(ShoppingOrderVO shoppingOrderVO){
        shoppingOrderVO.setRstatus((byte)0);
        shoppingOrderVO.setRecords(shoppingOrderAdMapper.selectForPjCount(shoppingOrderVO));
        return shoppingOrderAdMapper.selectForPj(shoppingOrderVO);
    }

    public Long selectForPjCount(ShoppingOrderVO shoppingOrderVO){
        shoppingOrderVO.setRstatus((byte)0);
        return shoppingOrderAdMapper.selectForPjCount(shoppingOrderVO);
    }

    public List<ShoppingOrder> selectForCompanyPj(ShoppingOrder shoppingOrder){
        shoppingOrder.setRecords(shoppingOrderAdMapper.selectForCompanyPjCount(shoppingOrder));
        return shoppingOrderAdMapper.selectForCompanyPj(shoppingOrder);
    }

    public Long selectForCompanyPjCount(ShoppingOrder shoppingOrder){
        return shoppingOrderAdMapper.selectForCompanyPjCount(shoppingOrder);
    }
    //查询商家订单
    public List<ShoppingOrder> selectForOrder(ShoppingOrder shoppingOrder){
        shoppingOrder.setRecords(shoppingOrderAdMapper.selectForOrderCount(shoppingOrder));
        return shoppingOrderAdMapper.selectForOrder(shoppingOrder);
    }

    /**
     * 订单个数
     * @param shoppingOrder
     * @return
     */
    public Long selectForOrderCount(ShoppingOrder shoppingOrder){
        return shoppingOrderAdMapper.selectForOrderCount(shoppingOrder);
    }

    /**
     * 今日成交金额
     * @param shoppingOrderVO
     * @return
     */
    public Long todayMoeny(ShoppingOrderVO shoppingOrderVO){
        String todayMoeny = shoppingOrderAdMapper.todayMoeny(shoppingOrderVO);
        if (!ObjectUtils.isEmpty(todayMoeny)) {
            return Long.parseLong(todayMoeny);
        } else {
            return (long)0;
        }
    }

    /**
     * 今日成交金额
     * @param shoppingOrderVO
     * @return
     */
    public Long orderSuccess(ShoppingOrderVO shoppingOrderVO){
        return shoppingOrderAdMapper.orderSuccess(shoppingOrderVO);
    }

    public List<ShoppingOrder> fjOrder(ShoppingOrder shoppingOrder) {
        shoppingOrder.setRecords(shoppingOrderAdMapper.fjOrderCount(shoppingOrder));
        return shoppingOrderAdMapper.fjOrder(shoppingOrder);
    }

}
