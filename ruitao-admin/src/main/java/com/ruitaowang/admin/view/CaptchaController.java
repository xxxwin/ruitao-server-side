package com.ruitaowang.admin.view;

import com.ruitaowang.core.utils.captcha.Captcha;
import com.ruitaowang.core.utils.captcha.GifCaptcha;
import com.ruitaowang.core.web.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Captcha
 */
@RestController
public class CaptchaController extends BaseController {

    /**
     * 动态验证码
     */
    @RequestMapping(value = {"/captcha"}, method = RequestMethod.GET)
    @PermitAll
    public void captcha() throws IOException {
        LOGGER.info("|--> {}", this.getRequest());
        LOGGER.info("|--> {}", this.getResponse());
        OutputStream outputStream = this.getResponse().getOutputStream();
        Captcha captcha = new GifCaptcha(150,40,5);
        captcha.out(outputStream);
        outputStream.flush();
        outputStream.close();
    }
}