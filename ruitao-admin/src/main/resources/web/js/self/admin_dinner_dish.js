/**
 * Created by xinchunting on 17-9-25.
 */
/**
 * Created by Shaka on 12/21/16.
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
function dishUpdate() {
    layer.open({
        title: ["加菜管理","font-size:20px;"],
        type: 2,
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:"/web/admin_dinner_dishCreate.html?id="+getQueryString("id")
    })
}
var Config = {
    selectUrl: '/api/admin/dc/dish',
    deleteUrl: '/api/admin/dc/dish',
    updateUrlForCellEdit: '/api/admin/dc/dish/cellEdit',
};
var companyId;
$(document).ready(function () {
    $.ajax({
        url: "/api/admin/dc/index/get",
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data) {
                companyId = data.data[0].companyId;
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#dc_dish").jqGrid({
        datatype: "json",
        postData:{companyId :getQueryString("id")},
        url: Config.selectUrl,
        height: 300,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["菜品编号", "菜品名称", "菜品图片", "菜品价格(元)", "菜品分类","菜品描述", "时间", "操作"],
        colModel: [{
            name: "id",
            index: "id",
            search: true
        }, {
            name: "name",
            index: "name"
        }, {
            name: "thum",
            index: "thum"
        },{
            name: "price",
            index: "price",
            sorttype: "int"
        }, {
            name: "categoryId",
            index: "categoryId",
            formatter:mydateformatter,
            autowidth: true,
            sorttype: "int"
        }, {
            name: "remark",
            index: "remark"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate",
            sortable: false
        }],
        cellEdit: true,
        cellsubmit: 'remote',
        cellurl: Config.updateUrlForCellEdit,
        pager: "#pager_dc_dish",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "id"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#dc_dish").jqGrid('navGrid', '#pager_dc_dish', {add: false, edit: false, del: false,search: false,
        refresh: false});
    // jQuery('#goods_list').trigger('reloadGrid');
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#dc_dish").setGridWidth(width);
    });
});

function Delete(id, index) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id);
    jQuery("#dc_dish").jqGrid('delRowData', id);
}
function updateDialog(action, url) {
    $.ajax(
        {
            url: url,
            type: action,
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    jQuery("#dc_dish").trigger("reloadGrid");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.error(errorThrown);
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#dc_dish").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#dc_dish').jqGrid ('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='Delete(" + id + ")' >删除</a>";
        html += '<a class="J_menuItem" style="color:#f60;padding-right: 15px" href="/admin/dc/dish/update/'+ id +'" data-index="0">修改</a>';
        var avatar = '<img style="width: 50px; height: 50px;" src="'+dataFromTheRow.thum+'"/>';
        jQuery("#dc_dish").jqGrid('setRowData', ids[i], {operate: html,ctime: dataFromTheRow.ctime,thum:avatar,price:toFixedForPrice(dataFromTheRow.price)});
    }
}
function mydateformatter(cellvalue, options, rowObject) {
    var name;
    $.ajax({
        url: "/api/admin/dc/categories/"+rowObject.categoryId,
        type: "GET",
        dataType: "json",
        async: false,
        success: function (data, textStatus, jqXHR) {
            name = data.data.name;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error(errorThrown, textStatus);
        }
    });
    return name;
}

