package com.ruitaowang.goods.dao;

import com.beust.jcommander.Parameter;
import com.ruitaowang.core.domain.Company;
import com.ruitaowang.core.domain.CompanyVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface CompanyAdMapper {

    int updateByPK(Company company);

    long count(Company company);

    List<Company> select(Company company);

    List<CompanyVO> findCompanyGoods(Company company);

    List<CompanyVO> findCompanyGoodsNearBy(Company company);

    long findCompanyGoodsCount(Company company);

    long findCompanyGoodsNearByCount(Company company);

    List<Company> selectCompanyIndustryIdForPage(CompanyVO companyVO);

    long selectCompanyIndustryIdCount(CompanyVO companyVO);

    List<Company> searchCompany(Company company);

    long searchCompanyCount(Company company);

    List<Company> searchCompanyByNear(CompanyVO companyVO);

    long searchCompanyByNearCount(Company company);

    List<CompanyVO> selectForInteractive(Company company);

    long selectForInteractiveCount(Company company);

    List<CompanyVO> selectForMyInteractive(Company company);

    long selectForMyInteractiveCount(Company company);

    List<Company> selectByCZ(Company company);

    long selectByCZCount(Company company);


//    @Select("select bean.* from ( select company_id, company_name, linkman, remark, ctime, mtime, rstatus, mobile, business_license_number, address, img_id_front," +
//            " img_id_back, img_tax_registration_certificate, img_business_license, img_organization_code_certificate, headimgurl, user_id, company_type, " +
//            "company_goods_categoty_id, jm_id, co_pay_type, xl_er_cert, company_status, audit_cause, co_type, district_id, province, city, district, bank_id, " +
//            "range_sort, bank_name, bank_account, bank_deposit, redirect_type, pid, slogan, qr_logo_url, company_type_id, quantitative_classification," +
//            " business_company_name, business_legal_person, business_address, business_range, business_inspect_time, business_effective_time," +
//            " business_img_back from company where rstatus = 0 and company_type_id in(SELECT id FROM rtshop.company_type where Industry_id=#{companyTypeId}) ) bean order by ctime asc limit #{page}, #{rows}")
//
//    @Results({
//            @Result(property = "companyId", column = "company_id"),
//            @Result(property = "companyName", column = "company_name"),
//            @Result(property = "businessLicenseNumber", column = "business_license_number"),
//            @Result(property = "imgIdFront", column = "img_id_front"),
//            @Result(property = "imgIdBack", column = "img_id_back"),
//            @Result(property = "imgTaxRegistrationCertificate", column = "img_tax_registration_certificate"),
//            @Result(property = "imgBusinessLicense", column = "img_business_license"),
//            @Result(property = "imgOrganizationCodeCertificate", column = "img_organization_code_certificate"),
//            @Result(property = "userId", column = "user_id"),
//            @Result(property = "companyType", column = "company_type"),
//            @Result(property = "companyGoodsCategotyId", column = "company_goods_categoty_id"),
//            @Result(property = "jmId", column = "jm_id"),
//            @Result(property = "coPayType", column = "co_pay_type"),
//            @Result(property = "xlErCert", column = "xl_er_cert"),
//            @Result(property = "auditCause", column = "audit_cause"),
//            @Result(property = "companyStatus", column = "company_status"),
//            @Result(property = "coType", column = "co_type"),
//            @Result(property = "districtId", column = "district_id"),
//            @Result(property = "bankId", column = "bank_id"),
//            @Result(property = "rangeSort", column = "range_sort"),
//            @Result(property = "bankName", column = "bank_name"),
//            @Result(property = "bankAccount", column = "bank_account"),
//            @Result(property = "bankDeposit", column = "bank_deposit"),
//            @Result(property = "redirectType", column = "redirect_type"),
//            @Result(property = "qrLogoUrl", column = "qr_logo_url"),
//            @Result(property = "companyTypeId", column = "company_type_id"),
//            @Result(property = "quantitativeClassification", column = "quantitative_classification"),
//            @Result(property = "businessCompanyName", column = "business_company_name"),
//            @Result(property = "businessLegalPerson", column = "business_legal_person"),
//            @Result(property = "businessAddress", column = "business_address"),
//            @Result(property = "businessRange", column = "business_range"),
//            @Result(property = "businessInspectTime", column = "business_inspect_time"),
//            @Result(property = "businessEffectiveTime", column = "business_effective_time"),
//            @Result(property = "businessImgBack", column = "business_img_back"),
//
//
//    })
//    List<Company> selectCompanyType(@Param("companyTypeId") Long companyTypeId , @Param("page") int page , @Param("rows") int rows);

//    @Select("select bean.* from (select company_id from rtshop.company rc inner join rtshop.goods rg on rc.company_id = rg.goods_provider_id where rg.goods_online = 1 group by rc.company_id order by rc.ctime) bean limit #{page}, #{rows}")
//    @ResultMap("com.ruitaowang.goods.dao.CompanyMapper.resultMap")
//    List<Company> findCompanyGoods(@Param("page") int page , @Param("rows") int rows);
}