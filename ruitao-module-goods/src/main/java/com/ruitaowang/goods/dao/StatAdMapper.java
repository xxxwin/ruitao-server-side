package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.StatCompany;
import com.ruitaowang.core.domain.StatCompanyUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface StatAdMapper {

    //company
    @Select("select goods_provider_id as company_id, count(order_id) as order_count, htime as date from (" +
            "select order_id, l.goods_provider_id, count(order_id), from_unixtime(floor(ctime/1000), '%Y-%m-%d') htime from `rtshop`.order_prod l where rstatus=0 and goods_provider_id!=0 group by htime, goods_provider_id, order_id" +
            ") xxx group by htime")
    @Results({
            @Result(property = "companyId", column = "company_id"),
            @Result(property = "orderCount", column = "order_count")
    })
    List<StatCompany> selectAllShopOrderCountGroupByHTime();

    @Select("select company_id, count(order_id) as local_order_count, from_unixtime(floor(ctime/1000), '%Y-%m-%d') as date from `rtshop`.local_order where rstatus=0 and company_id!=0 group by date, company_id;")
    @Results({
            @Result(property = "companyId", column = "company_id"),
            @Result(property = "localOrderCount", column = "local_order_count")
    })
    List<StatCompany> selectAllLocalOrderCountByGroupHTime();

    @Select("select goods_provider_id as company_id, sum(goods_real_price) as order_income, from_unixtime(floor(ctime/1000), '%Y-%m-%d') as date from `rtshop`.order_prod where rstatus=0 and goods_provider_id !=0 group by date, goods_provider_id, order_id")
    @Results({
            @Result(property = "companyId", column = "company_id"),
            @Result(property = "orderIncome", column = "order_income")
    })
    List<StatCompany> selectAllOrderIncomeByGroupHTime();

    @Select("select company_id, sum(price) as local_order_income, from_unixtime(floor(ctime/1000), '%Y-%m-%d') as date from `rtshop`.local_order where rstatus=0 and company_id !=0 group by date, company_id")
    @Results({
            @Result(property = "companyId", column = "company_id"),
            @Result(property = "localOrderIncome", column = "local_order_income")
    })
    List<StatCompany> selectAllLocalOrderIncomeByGroupHTime();

    //company user
    @Select("select p.goods_provider_id as company_id, o.user_id, count(o.order_id) as order_count from `rtshop`.goods_order o, `rtshop`.order_prod p where o.order_id=p.order_id and o.rstatus=0 and p.goods_provider_id group by p.goods_provider_id,o.user_id;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "companyId", column = "company_id"),
            @Result(property = "orderCount", column = "order_count")
    })
    List<StatCompanyUser> selectAllUserOrderCountGroupByHTime();

    @Select("select company_id, user_id, count(order_id) as local_order_count from `rtshop`.local_order group by company_id,user_id")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "companyId", column = "company_id"),
            @Result(property = "localOrderCount", column = "local_order_count")
    })
    List<StatCompanyUser> selectAllUserLocalOrderCountByGroupHTime();

    @Select("select p.goods_provider_id as company_id, o.user_id, sum(o.amount_score),sum(o.order_amount) as order_consumption from `rtshop`.goods_order o, `rtshop`.order_prod p where o.order_id=p.order_id and o.rstatus=0 and p.goods_provider_id group by p.goods_provider_id,o.user_id;")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "orderConsumption", column = "order_consumption")
    })
    List<StatCompanyUser> selectAllUserOrderConsumptionByGroupHTime();

    @Select("select company_id, user_id, sum(price) as local_order_consumption from `rtshop`.local_order where rstatus=0 group by company_id,user_id")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "localOrderConsumption", column = "local_order_consumption")
    })
    List<StatCompanyUser> selectAllUserLocalOrderConsumptionByGroupHTime();
}