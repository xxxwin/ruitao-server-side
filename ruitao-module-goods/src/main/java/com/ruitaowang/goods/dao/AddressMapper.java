package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Address;
import java.util.List;

public interface AddressMapper {
    int insert(Address address);

    int deleteByPK(Long addressId);

    int updateByPK(Address address);

    Address selectByPK(Long addressId);

    long count(Address address);

    List<Address> select(Address address);
}