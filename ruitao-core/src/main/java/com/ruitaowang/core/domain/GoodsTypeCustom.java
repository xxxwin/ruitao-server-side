package com.ruitaowang.core.domain;

public class GoodsTypeCustom extends Page {
    /**
     *  ,所属表字段为goods_type_custom.id
     */
    private Long id;

    /**
     *  商家ID,所属表字段为goods_type_custom.company_id
     */
    private Long companyId;

    /**
     *  分类名称,所属表字段为goods_type_custom.name
     */
    private String name;

    /**
     *  ,所属表字段为goods_type_custom.rstatus
     */
    private Byte rstatus;

    /**
     *  备注,所属表字段为goods_type_custom.remark
     */
    private String remark;

    /**
     *  ,所属表字段为goods_type_custom.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为goods_type_custom.mtime
     */
    private Long mtime;

    private Long cateId;

    public Long getCateId() {
        return cateId;
    }

    public void setCateId(Long cateId) {
        this.cateId = cateId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }
}