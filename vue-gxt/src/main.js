// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import moment from 'moment'
import App from './App'
import router from './router'
import VeeValidate,{ Validator } from 'vee-validate'
import zh_CN from 'vee-validate/dist/locale/zh_CN'
import "./assets/personal/js/jquery-1.10.1.min.js"
import "./assets/personal/css/mui.css"
import "./assets/personal/css/style.css"
import "./assets/personal/css/fonts.css"
import VueAwesomeSwiper from 'vue-awesome-swiper'
import infiniteScroll from 'vue-infinite-scroll'
import lrz from 'lrz'
import { DatetimePicker } from 'mint-ui'

import echarts from 'echarts'
 
Vue.prototype.$echarts = echarts 
Vue.component(DatetimePicker.name, DatetimePicker)
//设置TiTle
import VueWechatTitle from 'vue-wechat-title'
Vue.use(VueWechatTitle)
// Vue.config.devtools = true

Vue.use(VueAwesomeSwiper)
Vue.use(VeeValidate);
Vue.use(infiniteScroll);

axios.interceptors.request.use(
  config => {
    // window.localStorage.headid = 13
    var headId = window.localStorage.headid;
    if(!!headId){
      config.headers["headid"] = headId;
    }
    else{
      // window.location.href="http://"+document.domain+"/wap/wx/login?fk=1-35-1-"+encodeURIComponent(window.location.href);
      window.location.href="https://"+document.domain+"/wap/wx/login?fk=1-35-0-no";
    }
    return config
  },function(error){
    return Promise.reject(error)
  });

// axios.defaults.withCredentials=true;
Vue.prototype.$http = axios

Validator.addLocale(zh_CN);
Vue.use(VeeValidate,{
    local:'zh_CN',
    delay:'100',
  }
)

// import "./assets/personal/js/layer/mobile/need/layer.css"
// import "./assets/personal/css/swiper.css"
import "./assets/personal/js/common.js"
import "./assets/common/js/layer.js"

process.env.MOCK && require('@/mock')
export default layer

if (process.env.NODE_ENV !== 'development') {
  Vue.prototype.APIURL_PREFIX = ''
} else {
   Vue.prototype.APIURL_PREFIX = '/apigxt'
  //  Vue.prototype.APIURL_PREFIX = 'http://test.ruitaowang.com'
  // Vue.prototype.APIURL_PREFIX = 'http://localhost:8081'
   //Vue.prototype.APIURL_PREFIX = 'http://192.168.199.137:8081'
  // Vue.prototype.APIURL_PREFIX = 'http://192.168.199.131:8081'
}

//公用样式 +common判断当前宽高
Vue.config.productionTip = false


Vue.filter('formatDate', function(value,format,isuinx) {
  if (value) {
    return isuinx? moment.unix(String(value)).format(format||'YYYY-MM-DD'): moment(value).format(format||'YYYY-MM-DD');
  }
});
Vue.filter('formatDateTIME', function(value,format,isuinx) {
  if (value) {
    return isuinx? moment.unix(String(value)).format(format||'YYYY-MM-DD HH:mm:ss'): moment(value).format(format||'YYYY-MM-DD HH:mm:ss');
  }
});
Vue.filter('capitalize4', function (value) {//过滤器取最后四个
 if(value){
    var str=value;
   return  str.substr(str.length-4);
    
 }
})
Vue.filter('capitalize6', function (value) {//过滤器取最后1个
  if(value){
     var str=value;
    return  str.substr(str.length-6);
     
  }
 })
 Vue.filter('capitalize1', function (value) {//过滤器取第一个1个
  if(value){
     var str=value;
    return  str.substring(0,1);
     
  }
 })
Vue.filter('amountMin100', function(number,fixnum) {
  return isNaN(number) ? 0.00 : (number/100).toFixed(0==fixnum?0:(fixnum||2));
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App},
  template: '<App/>',

})
