package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.MeetingUser;
import java.util.List;

public interface MeetingUserMapper {
    int insert(MeetingUser meetingUser);

    int deleteByPK(Long meetingUserId);

    int updateByPK(MeetingUser meetingUser);

    MeetingUser selectByPK(Long meetingUserId);

    long count(MeetingUser meetingUser);

    List<MeetingUser> select(MeetingUser meetingUser);
}