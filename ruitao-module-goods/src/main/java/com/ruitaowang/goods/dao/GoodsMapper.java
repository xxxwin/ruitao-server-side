package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.Goods;
import java.util.List;

public interface GoodsMapper {
    int insert(Goods goods);

    int deleteByPK(Long goodsId);

    int updateByPK(Goods goods);

    Goods selectByPK(Long goodsId);

    long count(Goods goods);

    List<Goods> select(Goods goods);
}