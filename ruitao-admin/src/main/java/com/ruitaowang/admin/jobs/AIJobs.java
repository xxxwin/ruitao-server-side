//package com.ruitaowang.admin.jobs;
//
//import com.ruitaowang.account.service.SysUserService;
//import com.ruitaowang.core.domain.Ajob;
//import com.ruitaowang.core.domain.SNSWXUserinfo;
//import com.ruitaowang.goods.service.ActivityService;
//import com.ruitaowang.goods.service.AjobService;
//import com.ruitaowang.goods.service.WXUserinfoService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//import org.springframework.util.ObjectUtils;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//import tech.lingyi.wx.msg.out.TemplateData;
//import tech.lingyi.wx.msg.out.TemplateItem;
//import wx.wechat.service.mp.MPService;
//
//import javax.servlet.http.HttpServletRequest;
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * Created by neal on 21/09/2017.
// */
//@Component
//public class AIJobs {
//
//    @Autowired
//    private SysUserService sysUserService;
//    @Autowired
//    private WXUserinfoService wxUserinfoService;
//    @Autowired
//    private AjobService ajobService;
//    @Autowired
//    private ActivityService activityService;
//    @Value("${host.api}")
//    private String host;
//    private Byte isStart;
//
//
//    InetAddress addr = InetAddress.getLocalHost();
//    private String ip=addr.getHostAddress().toString(); //获取本机ip
//    private String hostName=addr.getHostName().toString(); //获取本机计算机名称
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(AIJobs.class);
//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
//    private static final int STIME = 1000;
//    private static  int DELAY_COUNT = 1;
//    private static  int RATE_COUNT = 1;
//    private static  int CRON_COUNT = 1;
//
//    public AIJobs() throws UnknownHostException {
//        this.ip = ip;
//        this.hostName = hostName;
//    }
//
////    @Scheduled(fixedDelay = STIME*5) //task执行结束后延迟STIME秒再执行下一次，每次总计用时: task执行时间(3)+延后时间(5)=8秒
////    public void fixedDelay() throws InterruptedException {
////        Thread.sleep(3*1000);
////       LOGGER.debug("count {}, delay {}", DELAY_COUNT++, dateFormat.format(new Date()));
////    }
//
////    @Scheduled(fixedRate = STIME*2)//每隔STIME秒就执行一次，每次总计用时: 延后时间(5)=2秒（即不考虑task执行时间）
////    public void fixedRate() throws InterruptedException {
////        Thread.sleep(2*1000);
////        LOGGER.debug("count {}, rate {}", RATE_COUNT++, dateFormat.format(new Date()));
////    }
//
//    //     cron 用过Linux的crontab都知道怎么回事，他们是一样的
//    //     第一位，表示秒，取值[0-59]
//    //     第二位，表示分，取值[0-59]
//    //     第三位，表示小时，取值[0-23]
//    //     第四位，日期天/日，取值[1-31]
//    //     第五位，日期月份，取值[1-12]
//    //     第六位，周几，1:周一, 2:周二, 3:周三, 4:周四, 5:周五, 6:周六 7:周日, 1/3 周一到周三; 1#1 第一周的周一;
//    //     第7位，年份, 但是在这里我们只能使用前六位，Spring只支持6位字段
//
//    //    (*)星号：每秒，每分，每天，每月，每年...
//    //    (?)问号：问号只能出现在日期和周这两个位置，表示这个位置的值不确定。
//    //    (-)减号：一个范围，如“1-3”，即1,2,3
//    //    (,)逗号：一个散列值，如第一位“1,2,4”，则表示1s,2s,3s执行task
//    //    (/)斜杠：如：x/y，x是开始值，y是步长，比如在第一位（秒） 1/30就是，从1秒开始，每隔30秒，最后就是每分钟的1，31，  另：*/y，等同于0/y
//    //    (#) 出现在周的位置，1#1，Spring中不支持
//
//    //  0 0 3 * * ?      每天3点执行
//    //  0 5 3 * * ?      每天3点5分执行
//    //  0 5 3 ? * *      每天3点5分执行，与上面作用相同
//    //  0 5/10 3 * * ?   每天3点的 5分，15分，25分，35分，45分，55分这几个时间点执行
//    //  */1 * * * * 7    星期日的每秒，也可以 1 * * * * 7
//    //  0 10 3 ? * 1 #3  每个月的第三个周，周天 执行，#号只能出现在周的位置
////    @Scheduled(cron = "0/1 * * * * 1")
////    public void cron(){
////        LOGGER.debug("count {}, cron {}", CRON_COUNT++, dateFormat.format(new Date()));
////    }
//
////    public Boolean isUrl(){
////        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
////        HttpServletRequest request = attributes.getRequest();
////        StringBuffer url = request.getRequestURL();
////        String tempContextUrl = url.delete(url.length() - request.getRequestURI().length(),
////                url.length()).append(request.getServletContext().getContextPath()).append("/").toString();
////        LOGGER.debug("tempContextUrl的值为" + tempContextUrl);
////        if (tempContextUrl.indexOf("www")!=-1){
////            //包含www，说明是线上环境
////            LOGGER.debug("线上环境");
////            return true;
////        } else {
////            //不包含www，说明不是线上环境
////            LOGGER.debug("不是线上环境");
////            return false;
////        }
////    }
//
//    public List<Ajob> getAjob(Ajob ajob){
//        return ajobService.select(ajob);
//    }
//
//    /**
//     *  每天晚上10点发送消息模板（测试）
//     */
//    @Scheduled(cron = "0 0 22 * * ?")
//    public void ceshiwcmSendTotal(){
//
//        LOGGER.info("ip地址为" + ip);
//        Ajob ajob = new Ajob();
//        //测试环境
//        ajob.setAjobSign("testwcm1000");
//        List<Ajob> ajobListcs = getAjob(ajob);
//        if (!ObjectUtils.isEmpty(ajobListcs) && ip.equals("10.171.55.220")) {
//            isStart = ajobListcs.get(0).getAjobType();
//            if (isStart == 1){
//                buildQRNotify();
//            }
//        }
//
//        //bmw线上服务器
//        ajob.setAjobSign("bmwwcm1000");
//        List<Ajob> ajobListbmw = getAjob(ajob);
//        if (!ObjectUtils.isEmpty(ajobListbmw) && ip.equals("10.29.24.137")) {
//            isStart = ajobListbmw.get(0).getAjobType();
//            if (isStart == 1){
//                buildQRNotify();
//            }
//        }
//
//        //admin线上服务器
//        ajob.setAjobSign("adminwcm1000");
//        List<Ajob> ajobListadmin = getAjob(ajob);
//        if (!ObjectUtils.isEmpty(ajobListadmin) && ip.equals("10.27.143.116")) {
//            isStart = ajobListadmin.get(0).getAjobType();
//            if (isStart == 1){
//                buildQRNotify();
//            }
//        }
//    }
//
//    /**
//     * 每天早上8点发送新入驻人数
//     * @throws ParseException
//     */
//    @Scheduled(cron = "0 0 8 * * ?")
//    public void cron1() throws ParseException {
////        if(isUrl){
////            sendUserTotal();
////        }
//
//        LOGGER.info("ip地址为" + ip);
//        Ajob ajob = new Ajob();
//        //测试环境
//        ajob.setAjobSign("testuser800");
//        List<Ajob> ajobListcs = getAjob(ajob);
//        if (!ObjectUtils.isEmpty(ajobListcs) && ip.equals("10.171.55.220")) {
//            isStart = ajobListcs.get(0).getAjobType();
//            if (isStart == 1){
//                sendUserTotal();
//            }
//        }
//
//        //bmw线上服务器
//        ajob.setAjobSign("bmwuser800");
//        List<Ajob> ajobListbmw = getAjob(ajob);
//        if (!ObjectUtils.isEmpty(ajobListbmw) && ip.equals("10.29.24.137")) {
//            isStart = ajobListbmw.get(0).getAjobType();
//            if (isStart == 1){
//                sendUserTotal();
//            }
//        }
//
//        //admin线上服务器
//        ajob.setAjobSign("adminuser800");
//        List<Ajob> ajobListadmin = getAjob(ajob);
//        if (!ObjectUtils.isEmpty(ajobListadmin) && ip.equals("10.27.143.116")) {
//            isStart = ajobListadmin.get(0).getAjobType();
//            if (isStart == 1){
//                sendUserTotal();
//            }
//        }
//    }
//
//    /**
//     * 发送每天微传媒的使用统计（模板消息）
//     */
//    public void buildQRNotify() {
//        String huiYuan = getRandom(60000,80000);
//        String guangGao = getRandom(200000,300000);
//        String zyxKeHu = getRandom(1000000,2000000);
//        int[] users = {65446,64970,65155,64956,13};
//        //int[] users = {65446};
//        for(int i = 0;i < users.length;i++){
//            TemplateData templateData = TemplateData.New();
//            SNSWXUserinfo snswxUserinfo = wxUserinfoService.selectByPK((long)users[i]);
//            if (ObjectUtils.isEmpty(snswxUserinfo)) {
//                return;
//            }
//            templateData.setTouser(snswxUserinfo.getOpenid());
//            templateData.setTemplate_id("h6w9MwtIjx_hu_ln3-NPLNnCkKZo-5Vqcbm_wpOvsSs");
//            templateData.setTopcolor("#FF3333");
//            templateData.setData(new TemplateItem());
//            templateData.add("first", "\t\t\t\t会员数据播报\r" +
//                    "今天共有 "+ huiYuan +" 位会员通过微传媒广告引流系统发表了 "+ guangGao +" 篇广告文案，获取了 "+ zyxKeHu +" 位准意向客户\r\r" +
//                    "客户是有限的，尽早获取尽早收益，继续加油\r\r" +
//                    "此功能还在测试阶段，只能公司内部员工可以看到，数据为虚拟数据\r\r" +
//                    "今天也要元气满满哦！！！" , "#173177");
//            templateData.setUrl(host + "/wap/mz_v2/mz.html?fk=1-23-0-0-0");
//            templateData.add("keyword1", "免费消息", "#0044BB");
//            templateData.add("keyword2", huiYuan, "#0044BB");
//            templateData.add("keyword3", dateFormat.format(new Date()), "#0044BB");
//            templateData.add("remark", "感谢您使用微传媒", "#FF3333");
//            try {
//                LOGGER.info("templateData {}", templateData);
//                new MPService().sendAPIEnter4TemplateMsg(templateData);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    /**
//     * 发送每天的会员数据统计（模板消息）
//     */
//    public void sendUserTotal() throws ParseException{
//        Map map = getUserCount();
//        String dateAll = (String)map.get("dataAll");
//        String dayAll = (String)map.get("dayAll");
//        String usersWCM = (String)map.get("usersWCM");
//        String nowDate = (String)map.get("nowDate");
//        String afterDate = (String)map.get("afterDate");
//        String content = afterDate + "0点   到 "+ nowDate +"0点   新增会员"+
//                dateAll +"人  微传媒使用人数"+ usersWCM +"人    "+ nowDate +"0点   截止总会员人数"+ dayAll +"人";
//        int[] users = {65446,13};
//        //int[] users = {65446};
//        for(int i = 0;i < users.length;i++){
//            TemplateData templateData = TemplateData.New();
//            SNSWXUserinfo snswxUserinfo = wxUserinfoService.selectByPK((long)users[i]);
//            if (ObjectUtils.isEmpty(snswxUserinfo)) {
//                return;
//            }
//            templateData.setTouser(snswxUserinfo.getOpenid());
//            templateData.setTemplate_id("5WeumKFY35cupXrf7pNQJWA-Osi15eDDECFsnZMSnJY");
//            templateData.setTopcolor("#FF3333");
//            templateData.setData(new TemplateItem());
//            templateData.add("first", "会员数据统计","#173177");
//            templateData.setUrl(host + "/wap/gxt/?fk=1-35-0-0-0");
//            templateData.add("keyword1", "首象共享", "#0044BB");
//            templateData.add("keyword2", dateFormat.format(new Date()), "#0044BB");
//            templateData.add("keyword3",content , "#0044BB");
//            templateData.add("remark", "今天也要元气满满哦", "#FF3333");
//            try {
//                LOGGER.info("templateData {}", templateData);
//                new MPService().sendAPIEnter4TemplateMsg(templateData);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
////    /**
////     *  发送每天微传媒的使用统计（客服消息）
////     */
////    public void buildWxHuiHua(){
////        String huiYuan = getRandom(60000,80000);
////        String guangGao = getRandom(200000,300000);
////        String zyxKeHu = getRandom(1000000,2000000);
////        //int[] users = {65446,64970,65155,64956,13};
////        int[] users = {65446};
//////        for(int i = 0;i < users.length;i++){
////            SNSWXUserinfo snswxUserinfo = wxUserinfoService.selectByPK((long)65446);
////            LOGGER.info("users[i]：进入正确方法");
////            if (ObjectUtils.isEmpty(snswxUserinfo)) {
////                LOGGER.info("定时器进入return");
////                return;
////            }
////            String content = "\t\t  会员数据播报\r" +
////                    "今天共有 "+ huiYuan +" 位会员通过微传媒广告引流系统发表了 "+ guangGao +" 篇广告文案，获取了 "+ zyxKeHu +" 位准意向客户\r\r" +
////                    "客户是有限的，尽早获取尽早收益，继续加油\r\r" +
////                    "此功能还在测试阶段，只能公司内部员工可以看到，数据为虚拟数据，这个为客服消息\r\r" +
////                    "测试阶段时，每晚10点会发送这个客服消息！！！";
////            try {
////                Map map = new HashMap();
////                map.put("openId",snswxUserinfo.getOpenid());
////                map.put("content",content);
////                new MPService().buildWxHuiHua(map);
////            } catch (Exception e){
////                e.printStackTrace();
////            }
////        //}
////    }
//
//    /**
//     * 获取某一个范围的随机数
//     * @param min
//     * @param max
//     * @return
//     */
//    public static String getRandom(int min, int max){
//        Random random = new Random();
//        int s = random.nextInt(max) % (max - min + 1) + min;
//        return String.valueOf(s);
//
//    }
//
////    /**
////     * 发送用户统计方法
////     * @throws ParseException
////     */
////    public void sendUserCount() throws ParseException {
////        Map map = getUserCount();
////        String dateAll = (String)map.get("dataAll");
////        String dayAll = (String)map.get("dayAll");
////        String nowDate = (String)map.get("nowDate");
////        String afterDate = (String)map.get("afterDate");
////        //int[] users = {65446,13};
////        int[] users = {65446};
////        for(int i = 0;i < users.length;i++){
////            SNSWXUserinfo snswxUserinfo = wxUserinfoService.selectByPK((long)users[i]);
////            LOGGER.info("users[i]：" + users[i]);
////            if (ObjectUtils.isEmpty(snswxUserinfo)) {
////                LOGGER.info("进入return");
////                return;
////            }
////            String content = afterDate + "0点   到 "+ nowDate +"0点   新增会员"+
////                    dateAll +"人  "+ nowDate +"0点   截止总会员人数"+ dayAll +"人";
////            try {
////                map.put("openId",snswxUserinfo.getOpenid());
////                map.put("content",content);
////                new MPService().buildWxHuiHua(map);
////            } catch (Exception e){
////                e.printStackTrace();
////            }
////        }
////    }
//
//    /**
//     * 获得用户统计个数
//     * @return
//     * @throws ParseException
//     */
//    public Map getUserCount() throws ParseException {
//        //设置日期格式
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        //获取当前是日期
//        Date date = simpleDateFormat.parse(simpleDateFormat.format(new Date()));
//        //转换为时间
//        long stime = date.getTime() - 86400000L,
//                etime = date.getTime();
//        //将上一天的时间戳转成时间格式
//        Date afterDate = new Date(stime);
//        //将当前日期按照“-”进行分割
//        String[] strNow = simpleDateFormat.format(new Date()).toString().split("-");
//        //将上一天日期按照“-”进行分割
//        String[] strAfter = simpleDateFormat.format(afterDate).toString().split("-");
//        Long dataAll= sysUserService.countDayData(stime,etime);
//        Long dayAll= sysUserService.countDayAll(etime);
//        Long usersWCM = activityService.countUsersWCM(stime,etime);
//        Map map = new HashMap();
//        map.put("dataAll",String.valueOf(dataAll));
//        map.put("dayAll",String.valueOf(dayAll));
//        map.put("usersWCM",String.valueOf(usersWCM));
//        map.put("nowDate", String.valueOf(strNow[1] + "月" + strNow[2] + "号"));
//        map.put("afterDate", String.valueOf(strAfter[1] + "月" + strAfter[2] + "号"));
//        return map;
//    }
//}
