package com.ruitaowang.core.domain;

public class CompanyProduct extends Page {
    /**
     *  编号,所属表字段为company_product.product_id
     */
    private Long productId;

    /**
     *  本地商家编号,所属表字段为company_product.company_id
     */
    private Long companyId;

    /**
     *  创建时间,所属表字段为company_product.ctime
     */
    private Long ctime;

    /**
     *  最后修改时间,所属表字段为company_product.mtime
     */
    private Long mtime;

    /**
     *  行状态
0 正常，1 删除,所属表字段为company_product.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为company_product.remark
     */
    private String remark;

    /**
     *  折扣编码,所属表字段为company_product.discount_id
     */
    private Long discountId;

    /**
     *  二维码,所属表字段为company_product.qrcode_url
     */
    private String qrcodeUrl;

    /**
     *  0 满减
1 百分比,所属表字段为company_product.discount_type
     */
    private Byte discountType;

    /**
     *  整数 要除以100,所属表字段为company_product.percentage
     */
    private Integer percentage;

    /**
     *  ,所属表字段为company_product.reach1
     */
    private Integer reach1;

    /**
     *  ,所属表字段为company_product.score1
     */
    private Integer score1;

    /**
     *  ,所属表字段为company_product.reach2
     */
    private Integer reach2;

    /**
     *  ,所属表字段为company_product.score2
     */
    private Integer score2;

    /**
     *  ,所属表字段为company_product.reach3
     */
    private Integer reach3;

    /**
     *  ,所属表字段为company_product.score3
     */
    private Integer score3;

    /**
     *  ,所属表字段为company_product.reach4
     */
    private Integer reach4;

    /**
     *  ,所属表字段为company_product.score4
     */
    private Integer score4;

    /**
     *  ,所属表字段为company_product.reach5
     */
    private Integer reach5;

    /**
     *  ,所属表字段为company_product.score5
     */
    private Integer score5;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public String getQrcodeUrl() {
        return qrcodeUrl;
    }

    public void setQrcodeUrl(String qrcodeUrl) {
        this.qrcodeUrl = qrcodeUrl == null ? null : qrcodeUrl.trim();
    }

    public Byte getDiscountType() {
        return discountType;
    }

    public void setDiscountType(Byte discountType) {
        this.discountType = discountType;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Integer getReach1() {
        return reach1;
    }

    public void setReach1(Integer reach1) {
        this.reach1 = reach1;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Integer getReach2() {
        return reach2;
    }

    public void setReach2(Integer reach2) {
        this.reach2 = reach2;
    }

    public Integer getScore2() {
        return score2;
    }

    public void setScore2(Integer score2) {
        this.score2 = score2;
    }

    public Integer getReach3() {
        return reach3;
    }

    public void setReach3(Integer reach3) {
        this.reach3 = reach3;
    }

    public Integer getScore3() {
        return score3;
    }

    public void setScore3(Integer score3) {
        this.score3 = score3;
    }

    public Integer getReach4() {
        return reach4;
    }

    public void setReach4(Integer reach4) {
        this.reach4 = reach4;
    }

    public Integer getScore4() {
        return score4;
    }

    public void setScore4(Integer score4) {
        this.score4 = score4;
    }

    public Integer getReach5() {
        return reach5;
    }

    public void setReach5(Integer reach5) {
        this.reach5 = reach5;
    }

    public Integer getScore5() {
        return score5;
    }

    public void setScore5(Integer score5) {
        this.score5 = score5;
    }
}