package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.ReceivingAddress;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.ReceivingAddressAdMapper;
import com.ruitaowang.goods.dao.ReceivingAddressMapper;
import com.ruitaowang.goods.utils.RStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/07/16.
 */
@Service
public class ReceivingAddressService implements BaseService<ReceivingAddress> {

    @Autowired
    private ReceivingAddressMapper receivingAddressMapper;
    @Autowired
    private ReceivingAddressAdMapper receivingAddressAdMapper;

    @Override
    public ReceivingAddress insert(ReceivingAddress receivingAddress) {
        Long time = System.currentTimeMillis();
        receivingAddress.setCtime(time);
        receivingAddress.setMtime(time);
        receivingAddressMapper.insert(receivingAddress);
        return receivingAddress;
    }

    @Override
    public int delete(Long id) {
        ReceivingAddress receivingAddress = new ReceivingAddress();
        receivingAddress.setCtime(System.currentTimeMillis());
        receivingAddress.setRstatus(RStatus.Deleted);
        receivingAddress.setId(id);
        return receivingAddressMapper.updateByPK(receivingAddress);
    }

    @Override
    public ReceivingAddress update(ReceivingAddress receivingAddress) {
        receivingAddressMapper.updateByPK(receivingAddress);
        return receivingAddress;
    }
    @Override
    public ReceivingAddress selectByPK(Long id) {
        return receivingAddressMapper.selectByPK(id);
    }

    @Override
    public List<ReceivingAddress> select(ReceivingAddress receivingAddress) {
        receivingAddress.setRstatus((byte) 0);
        return receivingAddressMapper.select(receivingAddress);
    }
    public List<ReceivingAddress> selectForPage(ReceivingAddress receivingAddress) {
        receivingAddress.setRstatus((byte) 0);
        receivingAddress.setRecords(receivingAddressMapper.count(receivingAddress));
        return receivingAddressMapper.select(receivingAddress);
    }

    public List<ReceivingAddress> selectDesc(ReceivingAddress receivingAddress) {
        receivingAddress.setRstatus((byte) 0);
        return receivingAddressAdMapper.selectDesc(receivingAddress);
    }

    public List<ReceivingAddress> selectByMtime(ReceivingAddress receivingAddress) {
        receivingAddress.setRstatus((byte) 0);
        return receivingAddressAdMapper.selectByMtime(receivingAddress);
    }

}
