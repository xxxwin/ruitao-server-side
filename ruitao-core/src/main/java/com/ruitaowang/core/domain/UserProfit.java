package com.ruitaowang.core.domain;

public class UserProfit extends Page {
    /**
     *  主键,所属表字段为user_profit.profit_id
     */
    private Long profitId;

    /**
     *  主键,所属表字段为user_profit.user_id
     */
    private Long userId;

    /**
     *  备注,所属表字段为user_profit.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为user_profit.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为user_profit.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为user_profit.rstatus
     */
    private Byte rstatus;

    /**
     *  订单金额,所属表字段为user_profit.amount
     */
    private Integer amount;

    /**
     *  ,所属表字段为user_profit.order_id
     */
    private Long orderId;

    /**
     *  收益金额,所属表字段为user_profit.user_profit
     */
    private Integer userProfit;

    /**
     *  ,所属表字段为user_profit.consumer_id
     */
    private Long consumerId;

    /**
     *  -4 发积分红包
-3 扣除积分
-2 退款
-1  提现
0 供货商订单奖励
1 地面店扫码奖励
2 微商订单奖励
3 地面店扫码返积分
4 微商订单返积分
5 点餐送积分
6 点餐奖励
7 云转奖励
8 云省奖励
9 云签奖励
10 云飞奖励
11 云推奖励
12 云点奖励
13 云联奖励
14 云销奖励
15 云赚奖励
16 互动店商品奖励
17 互动店霸屏奖励
18 互动店打赏奖励
19 互动店抢红包积分
20 互动店区级返利
21 互动店市级返利
22 互动店省级返利,所属表字段为user_profit.order_type
     */
    private Byte orderType;

    /**
     *  订单唯一编号,所属表字段为user_profit.order_sn
     */
    private String orderSn;

    /**
     *  个人账号余额,所属表字段为user_profit.user_balance
     */
    private Integer userBalance;

    /**
     *  ,所属表字段为user_profit.profit_type
     */
    private Byte profitType;

    /**
     *  ,所属表字段为user_profit.money_type
     */
    private Byte moneyType;

    /**
     *  手续费,所属表字段为user_profit.money_reduce
     */
    private Integer moneyReduce;

    /**
     *  是否入账，所属表字段为user_profit.is_account_entry
     */
    private Byte isAccountEntry;

    public Byte getIsAccountEntry() {
        return isAccountEntry;
    }

    public void setIsAccountEntry(Byte isAccountEntry) {
        this.isAccountEntry = isAccountEntry;
    }

    public Integer getMoneyReduce() {
        return moneyReduce;
    }

    public void setMoneyReduce(Integer moneyReduce) {
        this.moneyReduce = moneyReduce;
    }

    public Byte getMoneyType() {
        return moneyType;
    }

    public void setMoneyType(Byte moneyType) {
        this.moneyType = moneyType;
    }

    public Long getProfitId() {
        return profitId;
    }

    public void setProfitId(Long profitId) {
        this.profitId = profitId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getUserProfit() {
        return userProfit;
    }

    public void setUserProfit(Integer userProfit) {
        this.userProfit = userProfit;
    }

    public Long getConsumerId() {
        return consumerId;
    }

    public void setConsumerId(Long consumerId) {
        this.consumerId = consumerId;
    }

    public Byte getOrderType() {
        return orderType;
    }

    public void setOrderType(Byte orderType) {
        this.orderType = orderType;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn == null ? null : orderSn.trim();
    }

    public Integer getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(Integer userBalance) {
        this.userBalance = userBalance;
    }

    public Byte getProfitType() {
        return profitType;
    }

    public void setProfitType(Byte profitType) {
        this.profitType = profitType;
    }
}