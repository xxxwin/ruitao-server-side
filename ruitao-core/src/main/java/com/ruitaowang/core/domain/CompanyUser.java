package com.ruitaowang.core.domain;

public class CompanyUser extends Page {
    /**
     *  id,所属表字段为company_user.id
     */
    private Long id;

    /**
     *  社群（店铺）id,所属表字段为company_user.company_id
     */
    private Long companyId;

    /**
     *  用户id,所属表字段为company_user.user_id
     */
    private Long userId;

    /**
     *  备注,所属表字段为company_user.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为company_user.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为company_user.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为company_user.rstatus
     */
    private Byte rstatus;

    /**
     *  0:正常1:被投诉,所属表字段为company_user.complaint
     */
    private Byte complaint;

    /**
     *  被投诉次数,所属表字段为company_user.complaint_count
     */
    private Byte complaintCount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Byte getComplaint() {
        return complaint;
    }

    public void setComplaint(Byte complaint) {
        this.complaint = complaint;
    }

    public Byte getComplaintCount() {
        return complaintCount;
    }

    public void setComplaintCount(Byte complaintCount) {
        this.complaintCount = complaintCount;
    }
}