package com.ruitaowang.admin.view.admin;

import com.ruitaowang.core.domain.Comment;
import com.ruitaowang.goods.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class PageCommentController {
    @Autowired
    private CommentService commentService;
    @RequestMapping("/comment/update/{id}")
    public ModelAndView update(@PathVariable("id") long id,Model model){
        Comment comment=commentService.selectByPK(id);
        model.addAttribute("comment", comment);
        return new ModelAndView("web/comment_update");
    }
}
