package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.CategoryRecommend;
import java.util.List;

public interface CategoryRecommendMapper {
    int insert(CategoryRecommend categoryRecommend);

    int deleteByPK(Long recommendId);

    int updateByPK(CategoryRecommend categoryRecommend);

    CategoryRecommend selectByPK(Long recommendId);

    long count(CategoryRecommend categoryRecommend);

    List<CategoryRecommend> select(CategoryRecommend categoryRecommend);
}