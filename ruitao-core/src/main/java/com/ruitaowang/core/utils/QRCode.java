package com.ruitaowang.core.utils;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.ruitaowang.core.utils.aliyun.OSSHelper;
import wx.wechat.common.Configure;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * Created by neal on 12/19/16.
 */
public class QRCode {
	
	public static void main(String[] args) throws IOException, WriterException, NotFoundException {
//		 Encode("http://test.ruitaowang.com/wap/wx/login?fk=0-2-61760-117",
//		 "/Users/neal/work/qr_61760_117_123.png", "/Users/neal/work/favicon.jpg");
//
		Encode("https://www.ruitaowang.com/wap/wx/login?fk=0-2-0-147",
		 "/Users/neal/work/qr_0_147_123.png", "/Users/neal/work/favicon.jpg");

//		 File file = new File("/Users/neal/work/qr_biz_co.png");
//		 InputStream inputStream = new FileInputStream(file);
//		 try {
//		 OSSHelper.uploadToOSS(inputStream, "qr/qr_biz_co.png");
//		 } catch (Exception e) {
//		 e.printStackTrace();
//		 }
//		 Decode("/Users/neal/work/qr_1.png");
	}

	private static String qr(String workPath, String url, String fileName) {

		try {
			Encode(url, workPath + "/" + fileName, workPath + "/favicon.jpg");
			File file = new File(workPath + "/" + fileName);
			InputStream inputStream = new FileInputStream(file);
			OSSHelper.uploadToOSS(inputStream, fileName);
		} catch (WriterException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Configure.hostOSS + "/" + fileName;
	}

	public static String rulesQRForMeetingSignUp(String workPath, Long meetingId, Long userId) {// 会议报名
		String url = Configure.hostAPI + "/wap/wx/login?fk=1-12-" + userId + "-" + meetingId;
		String qr_img_url = "";
		if (Configure.hostAPI.indexOf("www.ruitaowang.com") > -1) {
			qr_img_url = "qr/qr_sign_up_" + meetingId + ".png";
		} else {
			qr_img_url = "qr/qr_sign_up_" + meetingId + "_test.png";
		}
		return qr(workPath, url, qr_img_url);
	}

	public static String rulesQRForMeetingSignIn(String workPath, Long meetingId, Long userId) {// 会议签到
		String url = Configure.hostAPI + "/wap/wx/login?fk=1-13-" + userId + "-" + meetingId;
		String qr_img_url = "";
		if (Configure.hostAPI.indexOf("www.ruitaowang.com") > -1) {
			qr_img_url = "qr/qr_sign_in_" + meetingId + ".png";
		} else {
			qr_img_url = "qr/qr_sign_in_" + meetingId + "_test.png";
		}
		return qr(workPath, url, qr_img_url);
	}

	public static String rulesQRForPCGoodsDetail(String workPath, Long goodsId) {// PC端
																				// 商品详情QR
		String url = Configure.hostAPI + "/wap/wx/login?fk=1-9-0-" + goodsId;
		String qr_img_url = "";
		if (Configure.hostAPI.indexOf("www.ruitaowang.com") > -1) {
			qr_img_url = "qr/qr_goods_" + goodsId + ".png";
		} else {
			qr_img_url = "qr/qr_goods_" + goodsId + "_test.png";
		}
		return qr(workPath, url, qr_img_url);
	}

	public static String rulesQRForUserId(String workPath, Long userId) {// 个人二维码
		String url = Configure.hostAPI + "/wap/wx/login?fk=1-1-" + userId + "-0";
		String qr_img_url = "qr/qr_" + userId + ".png";
		return qr(workPath, url, qr_img_url);
	}

	public static String rulesQRForSMPay(String workPath, Long userId, Long productId) {// 扫码支付
		String url = Configure.hostAPI + "/wap/wx/login?fk=0-2-" + userId + "-" + productId;
		String qr_img_url = "qr/qr_" + userId + "_" + productId + "_" + RandomUtils.random3() + ".png";
		return qr(workPath, url, qr_img_url);
	}

	private static void Encode(String content, String path) throws WriterException, IOException {
		int x = 500;
		int y = 500;
		int onColor = 0xFF008AC9;
		int offColor = 0xFFCAE7F7;
		String format = "png";
		File out = new File(path);

		HashMap<EncodeHintType, Object> map = new HashMap<>();
		map.put(EncodeHintType.CHARACTER_SET, "utf-8");
		map.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);// Decode
																			// 出错，可以适当调节排错率
		map.put(EncodeHintType.MARGIN, 1);
		BitMatrix encode = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, x, y, map);

		MatrixToImageWriter.writeToPath(encode, format, out.toPath(), new MatrixToImageConfig(onColor, offColor));
	}

	private static void Encode(String content, String path, String logoPath) throws WriterException, IOException {
		int x = 500;
		int y = 500;
		int onColor = 0xFF008AC9;
		int offColor = 0xFFCAE7F7;
		// int onColor = 0xFF008AC9;
		// int offColor = 0xFFFFFFFF;
		String format = "png";
		File out = new File(path);

		HashMap<EncodeHintType, Object> map = new HashMap<>();
		map.put(EncodeHintType.CHARACTER_SET, "utf-8");
		map.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);// Decode
																			// 出错，可以适当调节排错率
		map.put(EncodeHintType.MARGIN, 1);
		BitMatrix encode = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, x, y, map);

		BufferedImage qrCode = MatrixToImageWriter.toBufferedImage(encode, new MatrixToImageConfig(onColor, offColor));
		Graphics2D graphics = qrCode.createGraphics();

		BufferedImage logo = ImageIO.read(new File(logoPath));
		graphics.drawImage(logo, (qrCode.getWidth() - logo.getWidth()) / 2, (qrCode.getHeight() - logo.getHeight()) / 2, null);
		graphics.dispose();
		logo.flush();
		ImageIO.write(qrCode, format, out);
	}

	private static void Decode(String path) throws IOException, NotFoundException {
		File in = new File(path);
		BufferedImage qrCode = ImageIO.read(in);

		BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(qrCode)));

		HashMap<DecodeHintType, Object> map = new HashMap<>();
		map.put(DecodeHintType.CHARACTER_SET, "utf-8");
		Result decode = new MultiFormatReader().decode(binaryBitmap, map);

		System.out.println(decode);
	}
	
	public static String rulesQRForDiancan(String workPath, Long tableId,String tableNo) {// 扫码点餐
		String url = Configure.hostAPI + "/wap/wx/login?fk=1-14-0-" + tableId;
		String qr_img_url = "";
		if (Configure.hostAPI.indexOf("www.ruitaowang.com") > -1) {
			qr_img_url = "qr/qr_dinner_" + tableId + "_" + tableNo + "_" + RandomUtils.random3() + ".png";
		} else {
			qr_img_url = "qr/qr_dinner_" + tableId + "_" + tableNo + "_" + RandomUtils.random3() + "_test.png";
		}
		return qr(workPath, url, qr_img_url);
	}
}