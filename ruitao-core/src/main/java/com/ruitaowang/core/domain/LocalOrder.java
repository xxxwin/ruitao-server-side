package com.ruitaowang.core.domain;

public class LocalOrder extends Page {
    /**
     * 编号,所属表字段为local_order.order_id
     */
    private Long orderId;

    /**
     * 购买者编号,所属表字段为local_order.user_id
     */
    private Long userId;

    /**
     * 创建时间,所属表字段为local_order.ctime
     */
    private Long ctime;

    /**
     * 最后修改时间,所属表字段为local_order.mtime
     */
    private Long mtime;

    /**
     * 行状态
     * 0 正常，1 删除,所属表字段为local_order.rstatus
     */
    private Byte rstatus;

    /**
     * 0 未支付
     * 1 已支付,所属表字段为local_order.pay_status
     */
    private Byte payStatus;

    /**
     * 审核状态
     * 0 未审核
     * 1 审核通过
     * 2 审核不通过 （原因）,所属表字段为local_order.audit_status
     */
    private Byte auditStatus;

    /**
     * ,所属表字段为local_order.remark
     */
    private String remark;

    /**
     * ,所属表字段为local_order.audit_user_id
     */
    private Long auditUserId;

    /**
     * 订单唯一编号,所属表字段为local_order.order_sn
     */
    private String orderSn;

    /**
     * ,所属表字段为local_order.price
     */
    private Integer price;

    /**
     * ,所属表字段为local_order.product_id
     */
    private Long productId;

    /**
     * ,所属表字段为local_order.company_id
     */
    private Long companyId;

    /**
     * ,所属表字段为local_order.discount_id
     */
    private Long discountId;

    /**
     * 返的积分,所属表字段为local_order.score
     */
    private Integer score;

    /**
     * 满减券优惠价格,所属表字段为local_order.favorable_price
     */
    private Integer favorablePrice;

    /**
     * 扫码订单类型,所属表字段为local_order.local_order_type
     */
    private Byte localOrderType;

    /**
     * 扫码订单总金额,所属表字段为local_order.amount
     */
    private Integer amount;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Byte getLocalOrderType() {
        return localOrderType;
    }

    public void setLocalOrderType(Byte localOrderType) {
        this.localOrderType = localOrderType;
    }

    public Integer getFavorablePrice() {
        return favorablePrice;
    }

    public void setFavorablePrice(Integer favorablePrice) {
        this.favorablePrice = favorablePrice;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Byte getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Byte payStatus) {
        this.payStatus = payStatus;
    }

    public Byte getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(Byte auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getAuditUserId() {
        return auditUserId;
    }

    public void setAuditUserId(Long auditUserId) {
        this.auditUserId = auditUserId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn == null ? null : orderSn.trim();
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getDiscountId() {
        return discountId;
    }

    public void setDiscountId(Long discountId) {
        this.discountId = discountId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}