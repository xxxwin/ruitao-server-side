package com.ruitaowang.goods.dao;

import com.ruitaowang.core.domain.OrderProd;
import java.util.List;

public interface OrderProdMapper {
    int insert(OrderProd orderProd);

    int deleteByPK(Long prodId);

    int updateByPK(OrderProd orderProd);

    OrderProd selectByPK(Long prodId);

    long count(OrderProd orderProd);

    List<OrderProd> select(OrderProd orderProd);
}