import com.ruitaowang.account.service.RoleService;
import com.ruitaowang.admin.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by neal on 12/09/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class UserServiceTest {

    @Autowired
    RoleService roleService;

    @Test
    public void testGrantRoles(){
        roleService.grantUserRoleForAppend(1L, "4");
    }
}