package com.ruitaowang.core.domain;

public class Coupon extends Page {
    /**
     *  ,所属表字段为coupon.id
     */
    private Long id;

    /**
     *  商家ID,所属表字段为coupon.company_id
     */
    private Long companyId;

    /**
     *  商家ID,所属表字段为coupon.coupon_name
     */
    private String couponName;

    /**
     *  优惠券类型,所属表字段为coupon.coupon_type
     */
    private Byte couponType;

    /**
     *  优惠券有效时间,所属表字段为coupon.coupon_time
     */
    private Long couponTime;

    /**
     *  优惠券折扣比例,所属表字段为coupon.percentage
     */
    private Integer percentage;

    /**
     *  满,所属表字段为coupon.reach1
     */
    private Integer reach1;

    /**
     *  返,所属表字段为coupon.score1
     */
    private Integer score1;

    /**
     *  ,所属表字段为coupon.ctime
     */
    private Long ctime;

    /**
     *  ,所属表字段为coupon.mtime
     */
    private Long mtime;

    /**
     *  备注,所属表字段为coupon.remark
     */
    private String remark;

    /**
     *  ,所属表字段为coupon.rstatus
     */
    private Byte rstatus;

    /**
     *  ,所属表字段为coupon.goods_id
     */
    private Long goodsId;

    /**
     *  ,所属表字段为coupon.coupon_endtime
     */
    private Long couponEndtime;

    /**
     *  ,所属表字段为coupon.price
     */
    private Integer price;

    /**
     *  商家类型ID,所属表字段为coupon.company_type_id
     */
    private Long companyTypeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Byte getCouponType() {
        return couponType;
    }

    public void setCouponType(Byte couponType) {
        this.couponType = couponType;
    }

    public Long getCouponTime() {
        return couponTime;
    }

    public void setCouponTime(Long couponTime) {
        this.couponTime = couponTime;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public Integer getReach1() {
        return reach1;
    }

    public void setReach1(Integer reach1) {
        this.reach1 = reach1;
    }

    public Integer getScore1() {
        return score1;
    }

    public void setScore1(Integer score1) {
        this.score1 = score1;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getCouponEndtime() {
        return couponEndtime;
    }

    public void setCouponEndtime(Long couponEndtime) {
        this.couponEndtime = couponEndtime;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Long getCompanyTypeId() {
        return companyTypeId;
    }

    public void setCompanyTypeId(Long companyTypeId) {
        this.companyTypeId = companyTypeId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }
}