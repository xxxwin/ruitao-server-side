/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by Administrator on 2017/4/14.
 */
/**
 * Created by neal on 11/22/16.
 */

var Config = {
    createUrl: '/api/wallet',
    updateUrl: '/api/wallet',
    deleteUrl: '/api/wallet',
    selectUrl: '/api/admin/wallet'
};
$(document).ready(function () {
    $(".datepicker").datepicker({
        language: "zh-CN",
        clearBtn: true,//清除按钮
        todayBtn: "linked",//今日按钮
        changeMonth: true,
        changeYear: true,
        autoclose: true,//选中之后自动隐藏日期选择框
        format: "yyyy-mm-dd"//日期格式，详见 http://bootstrap-datepicker.readthedocs.org/en/release/options.html#format
    });
    $.jgrid.defaults.styleUI = "Bootstrap";
    $("#data_list").jqGrid({
        datatype: "json",
        url: Config.selectUrl,
        height: 450,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 12,
        colNames: ["编号", "名字", "购物积分", "日期","操作"],
        colModel: [{
            name: "userId",
            index: "userId",
            sorttype: "int",
            search: true
        }, {
            name: "userName",
            index: "userName"
        }, {
            name: "userScore",
            index: "userScore"
        }, {
            name: "ctime",
            index: "ctime"
        }, {
            name: "operate",
            index: "operate"
        }],
        pager: "#pager_data_list",
        jsonReader: {
            root: "data", "page": "page", total: "total",
            records: "records", repeatitems: false, id: "userId"
        },
        viewrecords: true,
        hidegrid: false,
        gridComplete: function () {
            dataWrapper();
        }
    });
    jQuery("#data_list").jqGrid('navGrid', '#pager_data_list', {add: false, edit: false, del: false});
    $(window).bind("resize", function () {
        var width = $(".jqGrid_wrapper").width();
        $("#data_list").setGridWidth(width);
    });

});

function Delete(id) {
    updateDialog("DELETE", Config.deleteUrl + '/' + id, id);
}

function updateDialog(action, url, id) {
    $.ajax(
        {
            url: url,
            type: action,
            dataType: "json",
            success: function (data, textStatus, jqXHR) {
                if (action == 'DELETE') {
                    if (data && data.type == "SUCCESS") {
                        jQuery("#data_list").jqGrid('delRowData', id);
                        jQuery("#data_list").trigger("reloadGrid");
                    } else {
                        toastr.error(data.msg);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toastr.error("网络故障");
            }
        });
}
function dataWrapper() {
    var ids = jQuery("#data_list").jqGrid('getDataIDs');
    for (var i = 0; i < ids.length; i++) {
        var id = ids[i];
        var dataFromTheRow = jQuery('#data_list').jqGrid('getRowData', id);
        dataFromTheRow.ctime = new Date(dataFromTheRow.ctime * 1).yyyymmddhhmmss();
        var html = "<a style='color:#f60;padding-right: 15px' onclick='orderUpdate(" + id + ")'>积分详情</a>";
        jQuery("#data_list").jqGrid('setRowData', ids[i], {
            operate:html,
            userScore:dataFromTheRow.userScore/100,
            ctime: dataFromTheRow.ctime
        });
    }
}
function orderUpdate(id) {
    layer.open({
        type: 2,
        title:"详情",
        scrollbar: false,
        shadeClose: true,
        shade: 0.8,
        area: ['100%', '100%'],
        content:"/web/wallet_all.html?id="+id
    })
}
// function clean() {
//     var postData = $('#data_list').jqGrid("getGridParam", "postData");
//     if(postData){
//         delete postData.userName;
//         delete postData.stime;
//         delete postData.etime;
//     }
// }
$("#perform_search").click(function () {
    clean();
    var postdata = $("#data_list").jqGrid('getGridParam', 'postData');
    postdata._search = true;
    postdata.userName = $('#userName').val();
    if($("#beginDate").val() != ""){
        postdata.stime = dateToEpoch($("#beginDate").val()) * 1 - 8 * 60 * 60 * 1000 + 1;
    }
    if($("#endDate").val() != ""){
        postdata.etime = dateToEpoch($("#endDate").val()) * 1 + 16 * 60 * 60 * 1000 - 1;
    }
    jQuery("#data_list").trigger("reloadGrid",[{page: 1}]);
});
function clean() {
    var postData = $('#data_list').jqGrid("getGridParam", "postData");
    console.log(postData);
    if(postData.stime){
        console.log(postData.stime);
        delete postData.stime;
    }
    if (postData.etime){
        console.log(postData.etime);
        delete postData.etime;
    }
    if (postData.userName){
        console.log(postData.userName);
        delete postData.userName;
    }
}