/**
 * Created by xinchunting on 17-9-25.
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
    var context = "";
    if (r != null)
        context = r[2];
    reg = null;
    r = null;
    return context == null || context == "" || context == "undefined" ? "" : context;
}
var ue = UE.getEditor('editor');

    var companyId;
    var createCompanyId;
    $.ajax({
        url: "/api/admin/dc/index/get",
        type: "GET",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            if (data) {
                companyId = data.data[0].companyId;
            }
            update();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            toastr.error("网络故障");
        }
    });

    function update() {
        ue.ready(function () {
            ue.execCommand('serverparam', {
                'isActivity': 1
            });
            $.ajax({
                url: "/api/admin/dc/company/DcRestaurant/get",
                type: "GET",
                data: {companyId: companyId},
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    createCompanyId = data.data[0].companyId;
                    if (getQueryString("id") == "brandStory") {
                        console.log("2");
                        if (data.data[0].brandStory) {
                            console.log("1");
                            console.log("brandStory");
                            ue.setContent(data.data[0].brandStory);
                        }
                        $("input[name=id]").val(data.data[0].id);
                    }
                    if (getQueryString("id") == "ventureHistory") {
                        if (data.data[0].ventureHistory) {
                            console.log("ventureHistory");
                            ue.setContent(data.data[0].ventureHistory);
                        }
                        $("input[name=id]").val(data.data[0].id);
                    }
                    if (getQueryString("id") == "foodPhilosophy") {
                        if (data.data[0].foodPhilosophy) {
                            console.log("foodPhilosophy");
                            ue.setContent(data.data[0].foodPhilosophy);
                        }
                        $("input[name=id]").val(data.data[0].id);
                    }
                    if (getQueryString("id") == "shopSpecialty") {
                        if (data.data[0].shopSpecialty) {
                            console.log("shopSpecialty");
                            ue.setContent(data.data[0].shopSpecialty);
                        }
                        $("input[name=id]").val(data.data[0].id);
                    }
                    if (getQueryString("id") == "dinnersCulture") {
                        if (data.data[0].dinnersCulture) {
                            console.log("dinnersCulture");
                            ue.setContent(data.data[0].dinnersCulture);
                        }
                        $("input[name=id]").val(data.data[0].id);
                    }
                    if (getQueryString("id") == "aboutUs") {
                        if (data.data[0].aboutUs) {

                            console.log("aboutUs");
                            ue.setContent(data.data[0].aboutUs);

                        }
                        $("input[name=id]").val(data.data[0].id);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error("网络故障");
                }
            });
        });

    }

    $(document).ready(function () {
    });
    $('#submitCreate').click(function () {
        var data = {};
        if (createCompanyId) {
            if (getQueryString("id") == "brandStory") {
                data = {brandStory: ue.getContent(), id: $("input[name=id]").val()};
            }
            if (getQueryString("id") == "ventureHistory") {
                data = {ventureHistory: ue.getContent(), id: $("input[name=id]").val()};
            }
            if (getQueryString("id") == "foodPhilosophy") {
                data = {foodPhilosophy: ue.getContent(), id: $("input[name=id]").val()};
            }
            if (getQueryString("id") == "shopSpecialty") {
                data = {shopSpecialty: ue.getContent(), id: $("input[name=id]").val()};
            }
            if (getQueryString("id") == "dinnersCulture") {
                data = {dinnersCulture: ue.getContent(), id: $("input[name=id]").val()};
            }
            if (getQueryString("id") == "aboutUs") {
                data = {aboutUs: ue.getContent(), id: $("input[name=id]").val()};
            }
        } else {
            if (getQueryString("id") == "brandStory") {
                data = {companyId: companyId, brandStory: ue.getContent()};
            }
            if (getQueryString("id") == "ventureHistory") {
                data = {companyId: companyId, ventureHistory: ue.getContent()};
            }
            if (getQueryString("id") == "foodPhilosophy") {
                data = {companyId: companyId, foodPhilosophy: ue.getContent()};
            }
            if (getQueryString("id") == "shopSpecialty") {
                data = {companyId: companyId, shopSpecialty: ue.getContent()};
            }
            if (getQueryString("id") == "dinnersCulture") {
                data = {companyId: companyId, dinnersCulture: ue.getContent()};
            }
            if (getQueryString("id") == "aboutUs") {
                data = {companyId: companyId, aboutUs: ue.getContent()};
            }
        }
        $.ajax(
            {
                url: "/api/admin/dc/company/textBox/update",
                type: "PUT",
                data: data,
                dataType: "json",
                success: function (data, textStatus, jqXHR) {
                    toastr.success(data.msg, data.title);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    toastr.error(errorThrown, textStatus);
                }
            });
        return false;
    });