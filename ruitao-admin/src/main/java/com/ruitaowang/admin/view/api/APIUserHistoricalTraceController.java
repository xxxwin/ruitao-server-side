package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.account.service.SysUserService;
import com.ruitaowang.core.domain.*;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.CompanyService;
import com.ruitaowang.goods.service.UserHistoricalTraceService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xinchunting on 17-10-30.
 */
@RestController
public class APIUserHistoricalTraceController extends BaseController {

    @Autowired
    private UserHistoricalTraceService userHistoricalTraceService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private SysUserService sysUserService;


    @RequestMapping("/api/wap/userHistoricalTraceList/get")
    public String findUserHistoricalTrace(UserHistoricalTrace userHistoricalTrace,@RequestHeader(value="headId",required = false) String userId,int page) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        SysUser user = sysUserService.selectByPK(Long.parseLong("13"));
        userHistoricalTrace.setPage(page);
        userHistoricalTrace.setUserId(user.getId());
        List out=new ArrayList<>();
        UserHistoricalTrace uht=new UserHistoricalTrace();
        Company company=companyService.selectByPK(281L);
        uht.setCompanyName(company.getCompanyName()+"(龙蛙体验店)");
        uht.setRemark("http://static.ruitaowang.com/attached/file/2018/07/09/20180709155656054.jpg");
        uht.setCompanyId(company.getCompanyId());
        if (page == 1){
            out.add(uht);
        }
//        userHistoricalTrace.setPage(page);
        List<UserHistoricalTrace> userHistoricalTracesList=userHistoricalTraceService.selectForPage(userHistoricalTrace);
        if (!ObjectUtils.isEmpty(userHistoricalTracesList)){
            for (UserHistoricalTrace where:userHistoricalTracesList){
                if (where.getCompanyId() != 281){
                    if (ObjectUtils.isEmpty(companyService.selectByPK(where.getCompanyId()).getHeadimgurl())){
                        where.setRemark("http://static.ruitaowang.com/attached/file/2018/07/18/20180718153138439.jpg");
                    }else {
                        where.setRemark(companyService.selectByPK(where.getCompanyId()).getHeadimgurl());
                    }
                        where.setCompanyName(companyService.selectByPK(where.getCompanyId()).getCompanyName());
                    out.add(where);
                }
            }
        }
        MsgOut o = MsgOut.success(out);
        o.setRecords(userHistoricalTrace.getRecords());
        o.setTotal(userHistoricalTrace.getTotal());
        return this.renderJson(o);
    }
    @RequestMapping("/api/wep/userHistoricalTraceList/companyBackground/get")
    public String findUserHistoricalTraceBackground(UserHistoricalTrace userHistoricalTrace) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(userHistoricalTrace)){
            return this.renderJson(MsgOut.error("参数错误"));
        }
        userHistoricalTrace.setUserId(user.getId());
        MsgOut o = MsgOut.success(userHistoricalTraceService.select(userHistoricalTrace));
        return this.renderJson(o);
    }
    @RequestMapping("/api/admin/userHistoricalTraceList/companyBackground/get")
    public String adminFindUserHistoricalTrace(UserHistoricalTrace userHistoricalTrace) {
        if (ObjectUtils.isEmpty(userHistoricalTrace)){
            return this.renderJson(MsgOut.error("参数错误"));
        }
        List<UserHistoricalTraceVO> userHistoricalTraceVOS = new ArrayList<>();
        List<UserHistoricalTrace> userHistoricalTraces = userHistoricalTraceService.select(userHistoricalTrace);
        for(UserHistoricalTrace u : userHistoricalTraces){
            UserHistoricalTraceVO userHistoricalTraceVO = new UserHistoricalTraceVO();
            BeanUtils.copyProperties(u,userHistoricalTraceVO);
            if(u.getUserId() != 0){
                userHistoricalTraceVO.setNickname(sysUserService.selectByPK(u.getUserId()).getNickname());
            }
            userHistoricalTraceVOS.add(userHistoricalTraceVO);
        }
        MsgOut o = MsgOut.success(userHistoricalTraceVOS);
        return this.renderJson(o);
    }

    @RequestMapping("/api/admin/userHistoricalTraceList/companyBackground/Page")
    public String adminFindUserHistoricalTracePage(UserHistoricalTrace userHistoricalTrace , int page) {
        userHistoricalTrace.setPage(page);
        if (!ObjectUtils.isEmpty(userHistoricalTrace.getSidx())) {
            userHistoricalTrace.setOrderBy(userHistoricalTrace.getSidx() + " " + userHistoricalTrace.getSord() + "," + userHistoricalTrace.getOrderBy());
        }
        if (ObjectUtils.isEmpty(userHistoricalTrace)){
            return this.renderJson(MsgOut.error("参数错误"));
        }
        List<UserHistoricalTraceVO> userHistoricalTraceVOS = new ArrayList<>();
        List<UserHistoricalTrace> userHistoricalTraces = userHistoricalTraceService.selectForPage(userHistoricalTrace);
        for(UserHistoricalTrace u : userHistoricalTraces){
            UserHistoricalTraceVO userHistoricalTraceVO = new UserHistoricalTraceVO();
            BeanUtils.copyProperties(u,userHistoricalTraceVO);
            if(u.getUserId() != 0){
                userHistoricalTraceVO.setNickname(sysUserService.selectByPK(u.getUserId()).getNickname());
            }
            userHistoricalTraceVOS.add(userHistoricalTraceVO);
        }
        MsgOut o = MsgOut.success(userHistoricalTraceVOS);
        o.setRecords(userHistoricalTrace.getRecords());
        o.setTotal(userHistoricalTrace.getTotal());
        return this.renderJson(o);
    }


    @RequestMapping(value ="/api/wep/userHistoricalTraceList/companyBackground/create", method = RequestMethod.POST)
    public String createUserHistoricalTraceBackground(UserHistoricalTrace userHistoricalTrace) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(userHistoricalTrace)){
            return this.renderJson(MsgOut.error("参数错误"));
        }
        userHistoricalTrace.setUserId(user.getId());
        MsgOut o = MsgOut.success(userHistoricalTraceService.insert(userHistoricalTrace));
        return this.renderJson(o);
    }
    @RequestMapping(value ="/api/wap/userHistoricalTraceList/create", method = RequestMethod.POST)
    public String createUserHistoricalTrace(@RequestHeader(value="headId",required = false) String userId,UserHistoricalTrace userHistoricalTrace) {
        if (ObjectUtils.isEmpty(userId)){
            return this.renderJson(MsgOut.error("Plz login."));
        }
        SysUser user = sysUserService.selectByPK(Long.parseLong(userId));
        if (ObjectUtils.isEmpty(userHistoricalTrace)){
            return this.renderJson(MsgOut.error("参数错误"));
        }
        userHistoricalTrace.setUserId(user.getId());
        List<UserHistoricalTrace> userHistoricalTraces=userHistoricalTraceService.select(userHistoricalTrace);
        if (ObjectUtils.isEmpty(userHistoricalTraces)){
            userHistoricalTrace.setCompanyName(companyService.selectByPK(userHistoricalTrace.getCompanyId()).getCompanyName());
            userHistoricalTraceService.insert(userHistoricalTrace);
        }else {
            userHistoricalTraceService.update(userHistoricalTraces.get(0));
        }
        MsgOut o = MsgOut.success(userHistoricalTrace);
        return this.renderJson(o);
    }
    @RequestMapping(value ="/api/wep/userHistoricalTraceList/companyBackground/update", method = RequestMethod.PUT)
    public String updateUserHistoricalTraceBackground(UserHistoricalTrace userHistoricalTrace) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        if (ObjectUtils.isEmpty(userHistoricalTrace)){
            return this.renderJson(MsgOut.error("参数错误"));
        }
        userHistoricalTrace.setUserId(user.getId());
        MsgOut o = MsgOut.success(userHistoricalTraceService.update(userHistoricalTrace));
        return this.renderJson(o);
    }

    @RequestMapping(value = "/api/admin/userHistoricalTraceList/companyBackground/delete/{id}", method = RequestMethod.DELETE)
    public String deleteUserHistoricalTraceBackground(@PathVariable("id") Long id) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        MsgOut o = MsgOut.success(userHistoricalTraceService.delete(id));
        return this.renderJson(o);
    }
}
