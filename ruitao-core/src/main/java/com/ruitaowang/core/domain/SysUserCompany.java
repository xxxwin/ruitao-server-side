package com.ruitaowang.core.domain;

public class SysUserCompany extends Page {
    /**
     *  ,所属表字段为t_sys_user_company.id
     */
    private Long companyId;

    /**
     *  ,所属表字段为t_sys_user_company.id
     */
    private Long userId;

    /**
     *  公司名称,所属表字段为t_sys_user_company.company_name
     */
    private String companyName;

    /**
     *  公司地址,所属表字段为t_sys_user_company.company_address
     */
    private String companyAddress;

    /**
     *  公司简介,所属表字段为t_sys_user_company.comany_describe
     */
    private String comanyDescribe;

    /**
     *  备注,所属表字段为t_sys_user_company.remark
     */
    private String remark;

    /**
     *  创建时间,所属表字段为t_sys_user_company.ctime
     */
    private Long ctime;

    /**
     *  最后更新时间,所属表字段为t_sys_user_company.mtime
     */
    private Long mtime;

    /**
     *  状态 0:正常 1:删除,所属表字段为t_sys_user_company.rstatus
     */
    private Byte rstatus;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress == null ? null : companyAddress.trim();
    }

    public String getComanyDescribe() {
        return comanyDescribe;
    }

    public void setComanyDescribe(String comanyDescribe) {
        this.comanyDescribe = comanyDescribe == null ? null : comanyDescribe.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}