package com.ruitaowang.core.domain;

/**
 * Created by xinchunting on 17-10-30.
 */
public class UserCommunication extends Page {

    /**
     *  自增主键,所属表字段为user_communication.id
     */
    private Long id;

    /**
     *  公司ID,所属表字段为user_communication.company_id
     */
    private Long companyId;

    /**
     *  用户手机号,所属表字段为user_communication.user_phone
     */
    private String userPhone;

    /**
     *  通讯状态,所属表字段为user_communication.communication
     */
    private Byte communication;
    /**
     *  创建时间,所属表字段为user_communication.company_id
     */
    private Long ctime;
    /**
     *  修改时间,所属表字段为user_communication.company_id
     */
    private Long mtime;
    /**
     *  状态,所属表字段为user_communication.rstatus
     */
    private Byte rstatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Byte getCommunication() {
        return communication;
    }

    public void setCommunication(Byte communication) {
        this.communication = communication;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }
}
