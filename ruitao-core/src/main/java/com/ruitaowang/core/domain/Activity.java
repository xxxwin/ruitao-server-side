package com.ruitaowang.core.domain;

public class Activity extends Page {
    /**
     *  自增编码,所属表字段为activity.activity_id
     */
    private Long activityId;

    /**
     *  活动标题,所属表字段为activity.activity_title
     */
    private String activityTitle;

    /**
     *  活动链接,所属表字段为activity.activity_url
     */
    private String activityUrl;

    /**
     *  活动类型
0 轮播图
1 二级banner,所属表字段为activity.activity_type
     */
    private Byte activityType;

    /**
     *  创建时间,所属表字段为activity.ctime
     */
    private Long ctime;

    /**
     *  最后编辑时间,所属表字段为activity.mtime
     */
    private Long mtime;

    /**
     *  0 正常
1 删除,所属表字段为activity.rstatus
     */
    private Byte rstatus;

    /**
     *  商品编码,所属表字段为activity.goods_id
     */
    private Long goodsId;

    /**
     *  排序,所属表字段为activity.activity_sort
     */
    private Byte activitySort;

    /**
     *  广告图片,所属表字段为activity.activity_image_url
     */
    private String activityImageUrl;

    /**
     *  文章发布者,所属表字段为activity.activity_author
     */
    private Long activityAuthor;

    /**
     *  互动内容,所属表字段为activity.activity_content
     */
    private String activityContent;

    /**
     *  阅读量,所属表字段为activity.activity_read
     */
    private Long activityRead;

    public Long getActivityRead() {
        return activityRead;
    }

    public void setActivityRead(Long activityRead) {
        this.activityRead = activityRead;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getActivityTitle() {
        return activityTitle;
    }

    public void setActivityTitle(String activityTitle) {
        this.activityTitle = activityTitle == null ? null : activityTitle.trim();
    }

    public String getActivityUrl() {
        return activityUrl;
    }

    public void setActivityUrl(String activityUrl) {
        this.activityUrl = activityUrl == null ? null : activityUrl.trim();
    }

    public Byte getActivityType() {
        return activityType;
    }

    public void setActivityType(Byte activityType) {
        this.activityType = activityType;
    }

    public Long getCtime() {
        return ctime;
    }

    public void setCtime(Long ctime) {
        this.ctime = ctime;
    }

    public Long getMtime() {
        return mtime;
    }

    public void setMtime(Long mtime) {
        this.mtime = mtime;
    }

    public Byte getRstatus() {
        return rstatus;
    }

    public void setRstatus(Byte rstatus) {
        this.rstatus = rstatus;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Byte getActivitySort() {
        return activitySort;
    }

    public void setActivitySort(Byte activitySort) {
        this.activitySort = activitySort;
    }

    public String getActivityImageUrl() {
        return activityImageUrl;
    }

    public void setActivityImageUrl(String activityImageUrl) {
        this.activityImageUrl = activityImageUrl == null ? null : activityImageUrl.trim();
    }

    public Long getActivityAuthor() {
        return activityAuthor;
    }

    public void setActivityAuthor(Long activityAuthor) {
        this.activityAuthor = activityAuthor;
    }

    public String getActivityContent() {
        return activityContent;
    }

    public void setActivityContent(String activityContent) {
        this.activityContent = activityContent == null ? null : activityContent.trim();
    }
}