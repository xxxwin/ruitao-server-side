package com.ruitaowang.admin.view.api;

import com.ruitaowang.account.security.LYSecurityUtil;
import com.ruitaowang.core.domain.Ajob;
import com.ruitaowang.core.domain.SysUser;
import com.ruitaowang.core.web.BaseController;
import com.ruitaowang.core.web.MsgOut;
import com.ruitaowang.goods.service.AjobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class APIAjobController extends BaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private AjobService ajobService;

    /**
     * @api {get} /api/ajobs/page 获取定时任务配置
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/ajobs/page
     * @apiPermission ajob
     * @apiName ajobs
     * @apiGroup Ajob
     * @apiParam {int} page 页数.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code": 0,
     * "data": [
     * {
     * "ajobId": 2,
     * "ajobIp": "10.171.55.220",
     * "ajobSign": "testuser800",
     * "ajobType": 2,
     * "ctime": 1547546371004,
     * "mtime": 1547623289026,
     * "remark": "每早8点",
     * "rstatus": 0
     * },
     * {
     * "ajobId": 1,
     * "ajobIp": "10.171.55.220",
     * "ajobSign": "testwcm1000",
     * "ajobType": 2,
     * "ctime": 1547542513395,
     * "mtime": 1547623313896,
     * "remark": "每晚10点",
     * "rstatus": 0
     * }
     * ],
     * "msg": "操作成功啦",
     * "records": 2,
     * "title": "成功",
     * "total": 1,
     * "type": "SUCCESS"
     * }
     */
    @RequestMapping(value = "/api/ajobs/page", method = RequestMethod.GET)
    public String ajobs(Ajob ajob,int page){
        ajob.setPage(page);
        if (!ObjectUtils.isEmpty(ajob.getSidx())) {
            ajob.setOrderBy(ajob.getSidx() + " " + ajob.getSord() + "," + ajob.getOrderBy());
        }
        List<Ajob> ajobList = ajobService.selectForPage(ajob);
        MsgOut o = MsgOut.success(ajobList);
        o.setRecords(ajob.getRecords());
        o.setTotal(ajob.getTotal());
        return this.renderJson(o);
    }

    /**
     * @api {put} /api/ajobs/update 修改定时任务配置
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/ajobs/update
     * @apiPermission ajob
     * @apiName updateAjob
     * @apiGroup Ajob
     * @apiParam {String} ajobSign 唯一标识.
     * @apiParam {String} ajobIp 服务器.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code": 0,
     * "data": {
     * "ajobSign": "测试标识",
     * "ajobIp": "10.29.24.137",
     * "mtime": 1547716521410
     * },
     * "msg": "操作成功啦",
     * "title": "成功",
     * "type": "SUCCESS"
     * }
     */
    @RequestMapping(value = "/api/ajobs/update", method = RequestMethod.PUT)
    public String updateAjob(Ajob ajob) {
        ajobService.update(ajob);
        MsgOut o = MsgOut.success(ajob);
        return this.renderJson(o);
    }

    /**
     * @api {post} /api/ajobs/create 新增定时任务
     * @apiVersion 0.1.0
     * @apiSampleRequest /api/ajobs/create
     * @apiPermission ajob
     * @apiName createAjob
     * @apiGroup Ajob
     * @apiParam {String} ajobSign 唯一标识.
     * @apiParam {String} ajobIp 服务器.
     * @apiSuccess {int} code 接口返回状态码.
     * @apiSuccess {String} msg  接口返回信息.
     * @apiSuccess {Object} data  接口返回数据.
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * <p>
     * {
     * "code": 0,
     * "data": {
     * "ajobSign": "测试标识",
     * "ajobIp": "10.29.24.137",
     * "mtime": 1547716521410
     * },
     * "msg": "操作成功啦",
     * "title": "成功",
     * "type": "SUCCESS"
     * }
     */
    @RequestMapping(value = "/api/ajobs/create", method = RequestMethod.POST)
    public String createAjob(Ajob ajob){
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        ajobService.insert(ajob);
        MsgOut o = MsgOut.success(ajob);
        return this.renderJson(o);
    }

    /**
     * 删除定时任务
     * @param ajobId
     * @return
     */
    @RequestMapping(value = "/api/ajobs/{ajobId}", method = RequestMethod.DELETE)
    public String deleteActivity(@PathVariable("ajobId") Long ajobId) {
        MsgOut o = MsgOut.success(ajobService.delete(ajobId));
        return this.renderJson(o);
    }

    /**
     * 列表页修改定时器状态
     * @param ajob
     * @param id
     * @return
     */
    @RequestMapping(value = "/api/ajobs/cellEdit", method = RequestMethod.POST)
    public String updateGoodsForCellEdit(Ajob ajob, long id) {
        SysUser user = LYSecurityUtil.currentSysUser();
        if (ObjectUtils.isEmpty(user)) {
            return this.renderJson(MsgOut.error("Plz login."));
        }
        ajob.setAjobId(id);
        ajobService.update(ajob);
        MsgOut o = MsgOut.success(ajob);
        return this.renderJson(o);
    }

}
