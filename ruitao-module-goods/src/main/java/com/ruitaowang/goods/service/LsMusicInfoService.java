package com.ruitaowang.goods.service;

import com.ruitaowang.core.domain.LsMusicInfo;
import com.ruitaowang.core.service.BaseService;
import com.ruitaowang.goods.dao.LsMusicInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by neal on 11/17/16.
 */
@Service
public class LsMusicInfoService implements BaseService<LsMusicInfo> {
    @Autowired
    private LsMusicInfoMapper LsMusicInfoMapper;

    @Override
    public LsMusicInfo insert(LsMusicInfo LsMusicInfo) {
        Long time = System.currentTimeMillis();
        LsMusicInfo.setCtime(time);
        LsMusicInfo.setMtime(time);
        LsMusicInfoMapper.insert(LsMusicInfo);
        return LsMusicInfo;
    }

    @Override
    public int delete(Long id) {
        LsMusicInfo LsMusicInfo = new LsMusicInfo();
        LsMusicInfo.setMusicId(id);
        LsMusicInfo.setRstatus((byte) 1);
        Long time = System.currentTimeMillis();
        LsMusicInfo.setMtime(time);
        return LsMusicInfoMapper.updateByPK(LsMusicInfo);
    }

    @Override
    public LsMusicInfo update(LsMusicInfo LsMusicInfo) {
        Long time = System.currentTimeMillis();
        LsMusicInfo.setMtime(time);
        LsMusicInfoMapper.updateByPK(LsMusicInfo);
        return LsMusicInfo;
    }

    @Override
    public LsMusicInfo selectByPK(Long id) {

        return LsMusicInfoMapper.selectByPK(id);
    }

    @Override
    public List<LsMusicInfo> select(LsMusicInfo LsMusicInfo) {
        LsMusicInfo.setRstatus((byte) 0);
        return LsMusicInfoMapper.select(LsMusicInfo);
    }

    public List<LsMusicInfo> selectForPage(LsMusicInfo LsMusicInfo) {
        LsMusicInfo.setRstatus((byte) 0);
        LsMusicInfo.setRecords(LsMusicInfoMapper.count(LsMusicInfo));
        return LsMusicInfoMapper.select(LsMusicInfo);

    }
}

